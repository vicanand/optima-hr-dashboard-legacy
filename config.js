let path = require('path');
const rootPath = path.normalize(__dirname);
const env = process.env.NODE_ENV || 'development_stage';

const config = {
    development_stage: {
        platformIFI: {
            RBL: 140793,
            IDFC: 140827,
            KOTAK: 141618,
            SODEXO: 141617,
            HDFC: 147340
        },
        root: rootPath,
        app: {
            name: 'OPTIMA',
            domain: 'localhost:4000',
            protocol: 'http'
        },
        port: process.env.PORT || 4000,
        ssoUrl: 'http://sso-stage.zetaapps.in/',
        ssoPhUrl: 'https://sso-stage-ph.zetaapps.in/',
        corpBaseUrl: 'https://mv-stage.dash.zeta.in/',
        omsBaseUrl: 'https://api.stage.zeta.in/zeta.in/',
        esBaseUrl: 'https://api.stage.zeta.in/zeta.in/analytics-datacollector/1.0/recordEvents',
        loginBaseUrl: 'https://mv-stage.dash.zeta.in/',
        redis: {
            host: 'localhost',
            port: 6379,
            secret: '4d0b191d40d8db87562ba2b306560a2bdb97ba8941c3bcd4b96792b0f3af7aef',
            ttl: (30 * 24 * 60 * 60)
        },
        logger: {
            accessKeyId: 'AKIAJKH4UDH6TAYDINOA',
            secretAccessKey: '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',
            region: 'ap-southeast-1',
            streamName: 'logs-staging',
            partitionKey: "CorpBenService",
            bunyanLogLevel : 'debug'
        },
        corpBenAPIToken:'XuD8KIHgiHb4ZvbSmjMPEhraDtkqVirgexCnDTqOpYJBYsKwtU3jGOU3AvYknLOeWok4YrlR2L0JFcCjJSQI10hgjLdfcUIJ7fvB'
    },
    development_production: {
        platformIFI: {
            RBL: 156699,
            IDFC: 158326,
            KOTAK: 163924,
            SODEXO: 163925,
            HDFC: 233203
        },
        root: rootPath,
        app: {
            name: 'OPTIMA',
            domain: 'localhost:2700',
            protocol: 'http'
        },
        port: process.env.PORT || 2700,
        ssoUrl: 'https://sso.zetaapps.in/',
        ssoPhUrl: 'https://sso-ph.zetaapps.in/',
        corpBaseUrl: 'https://corp.zetaapps.in/',
        omsBaseUrl: 'https://api.gw.zetapay.in/zeta.in/',
        esBaseUrl: 'https://api.gw.zetapay.in/zeta.in/analytics-datacollector/1.0/recordEvents',
        loginBaseUrl: 'https://corp.zetaapps.in/',
        redis: {
            host: 'localhost',
            port: 6379,
            secret: '4d0b441d40d4db67582ba2b308562a2bdb37ba894143bcd4b56792b0f3af7aef',
            ttl: (30 * 24 * 60 * 60)
        },
        logger: {
            accessKeyId: 'AKIAJKH4UDH6TAYDINOA',
            secretAccessKey: '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',
            region: 'ap-southeast-1',
            streamName: 'logs-staging',
            partitionKey: "CorpBenService",
            bunyanLogLevel : 'debug'
        },
        corpBenAPIToken: 'maZJZdW6NUePHvpVF8V4g0W9rQAO9LWrX7g3JemqULP0BdkMYhioGrwIG8GyNwdT3W7FMl0V8q9hocFeFLAz6wn98C1vUK5DReju'
    },
    development_preprod: {
        platformIFI: {
            RBL: 140793,
            IDFC: 140827,
            KOTAK: 141618,
            SODEXO: 141617,
            HDFC: 151613
        },
        root: rootPath,
        app: {
            name: 'OPTIMA',
            domain: 'localhost:3000',
            protocol: 'https'
        },
        port: process.env.PORT || 3000,
        ssoUrl: 'http://sso-pp.zetaapps.in/',
        ssoPhUrl: 'https://sso-pp-ph.zetaapps.in/',
        corpBaseUrl: 'https://corp-pp.zetaapps.in/',
        omsBaseUrl: 'https://api.preprod.zeta.in/zeta.in/',
        esBaseUrl: 'https://api.preprod.zeta.in/zeta.in/analytics-datacollector/1.0/recordEvents',
        loginBaseUrl: 'https://mv-preprod.dash.zeta.in/',
        redis: {
            host: 'localhost',
            port: 6379,
            secret: '4d0b191d40d8db87562ba2b306560a2bdb97ba8941c3bcd4b96792b0f3af7aef',
            ttl: (30 * 24 * 60 * 60)
        },
        logger: {
            accessKeyId: 'AKIAJKH4UDH6TAYDINOA',
            secretAccessKey: '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',
            region: 'ap-southeast-1',
            streamName: 'logs-preprod',
            partitionKey: "CorpBenService",
            bunyanLogLevel : 'debug'
        }
    },
    preprod: {
        root: rootPath,
        app: {
            name: 'OPTIMA',
            domain: 'optima-pp.zetaapps.in',
            protocol: 'https'
        },
        port: process.env.PORT || 3000,
        ssoUrl: 'http://sso-pp.zetaapps.in/',
        ssoPhUrl: 'https://sso-pp-ph.zetaapps.in/',
        corpBaseUrl: 'https://corp-pp.zetaapps.in/',
        omsBaseUrl: 'https://api.preprod.zeta.in/zeta.in/',
        esBaseUrl: 'https://api.preprod.zeta.in/zeta.in/analytics-datacollector/1.0/recordEvents',
        loginBaseUrl: 'https://mv-preprod.dash.zeta.in/',
        redis: {
            host: '10.4.10.154',
            port: 6379,
            secret: '4d0b191d40d8db87562ba2b306560a2bdb97ba8941c3bcd4b96792b0f3af7aef',
            ttl: (30 * 24 * 60 * 60)
        },
        logger: {
            accessKeyId: 'AKIAJKH4UDH6TAYDINOA',
            secretAccessKey: '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',
            region: 'ap-southeast-1',
            streamName: 'logs-preprod',
            partitionKey: "CorpBenService",
            bunyanLogLevel : 'debug'
        }
    },
    stage: {
        root: rootPath,
        app: {
            name: 'OPTIMA',
            domain: 'optima-stage.zetaapps.in',
            protocol: 'https'
        },
        port: process.env.PORT || 9000,
        ssoUrl: 'http://sso-stage.zetaapps.in/',
        ssoPhUrl: 'https://sso-stage-ph.zetaapps.in/',
        corpBaseUrl: 'https://mv-stage.dash.zeta.in/',
        omsBaseUrl: 'https://api.stage.zeta.in/zeta.in/',
        esBaseUrl: 'https://api.stage.zeta.in/zeta.in/analytics-datacollector/1.0/recordEvents',
        loginBaseUrl: 'https://mv-stage.dash.zeta.in/',
        redis: {
            host: 'localhost',
            port: 6379,
            secret: '4d0b191d40d8db87562ba2b306560a2bdb97ba8941c3bcd4b96792b0f3af7aef',
            ttl: (30 * 24 * 60 * 60)
        },
        corpBenAPIToken:'XuD8KIHgiHb4ZvbSmjMPEhraDtkqVirgexCnDTqOpYJBYsKwtU3jGOU3AvYknLOeWok4YrlR2L0JFcCjJSQI10hgjLdfcUIJ7fvB',
        logger: {
            accessKeyId: 'AKIAJKH4UDH6TAYDINOA',
            secretAccessKey: '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',
            region: 'ap-southeast-1',
            streamName: 'logs-staging',
            partitionKey: "CorpBenService",
            bunyanLogLevel : 'debug'
        }
    },

    production: {
        root: rootPath,
        app: {
            name: 'OPTIMA',
            domain: 'corporates.zetaapps.in',
            protocol: 'https'
        },
        port: process.env.PORT || 2700,
        ssoUrl: 'https://sso.zetaapps.in/',
        ssoPhUrl: 'https://sso-ph.zetaapps.in/',
        corpBaseUrl: 'https://corp.zetaapps.in/',
        omsBaseUrl: 'https://api.gw.zetapay.in/zeta.in/',
        esBaseUrl: 'https://api.gw.zetapay.in/zeta.in/analytics-datacollector/1.0/recordEvents',
        loginBaseUrl: 'https://corp.zetaapps.in/',
        redis: {
            host: 'localhost',
            port: 6379,
            secret: '4d0b441d40d4db67582ba2b308562a2bdb37ba894143bcd4b56792b0f3af7aef',
            ttl: (30 * 24 * 60 * 60)
        },
        logger: {
            accessKeyId: 'AKIAJKH4UDH6TAYDINOA',
            secretAccessKey: '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',
            region: 'ap-southeast-1',
            streamName: 'logs-staging',
            partitionKey: "CorpBenService",
            bunyanLogLevel : 'debug'
        }
    }
};

module.exports = config[env];

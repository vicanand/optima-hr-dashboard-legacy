const webpack = require('webpack'),
      CleanWebpackPlugin = require('clean-webpack-plugin'),
      ExtractTextPlugin = require("extract-text-webpack-plugin"),
      path = require('path');

var glob = require("glob");

const mainCss = new ExtractTextPlugin({
    filename: 'css/main.css',
});

let pathsToClean = [
    'dist'
];

let cleanOptions = {
    root:     __dirname + "/public/",
    verbose:  true,
    dry:      false
};

const vendorCss = new ExtractTextPlugin({
    filename: 'css/vendor.css',
});

const jsPath1 = glob.sync(__dirname + "/public/scripts/**/*.js");

const entryObject = jsPath1.reduce((acc, item) => {
    const name = item.substring(item.lastIndexOf('/')+1, (item.length-3));
    acc[name] = item;
    return acc;
}, {});

const config = {
    node: {
        dns: "mock",
        fs: "empty",
        net: "empty",
        tls: "empty",
        path: true,
        url: false
    },
    resolve: {
        extensions: ['.js', '.scss'],
        modules: ['node_modules']
    },
    module: {

        rules: [
            {
                test: /\.ejs$/,
                use: 'raw-loader'
            },
            {
                test: /\.js$/,
                exclude: /node_modules\/(?!(aws-kinesis-writable)\/).*/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ['es2015', {
                                modules: false
                            }]
                        ]
                    }
                }]
            },
            {
                test: /\.(scss)$/,
                use: mainCss.extract({
                    fallback: 'style-loader',
                    use: [{
                        loader: "css-loader"
                    }, {
                        loader: "sass-loader"
                    }]
                })
            },
            {
                test: /\.(css)$/,
                use: vendorCss.extract({
                    fallback: 'style-loader',
                    use: [{
                        loader: "css-loader"
                    }, {
                        loader: "sass-loader"
                    }]
                })
            },
            {
                test: /\.(eot|ttf|woff|woff2)$/,
                loader: 'file-loader?name=/fonts/[name].[ext]'
            },

            {
                test: /\.(png|jpg|svg|jpeg|gif)$/,
                loader: 'file-loader?name=/images/[name].[ext]'
            }
        ]
    },
    plugins: [
        mainCss,
        vendorCss,
        new CleanWebpackPlugin(pathsToClean, cleanOptions),
        new webpack.ProvidePlugin({
            pdfMake: 'pdfmake'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks(module, count) {
                var context = module.context;
                return (context && context.indexOf('node_modules') >= 0) || ( context && context.indexOf('/public/lib/') >= 0);
            }
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'manifest',
            chunks: ['vendor']
        })
    ]
};

var barConfig = Object.assign({}, config, {
    name: "b",
    entry: entryObject,
    output: {
        path: __dirname + '/public/dist',
        filename: 'scripts/[name].js',
        chunkFilename: 'scripts/[name].js'
    },
    watchOptions: {
        poll: true
    }
});

module.exports = [
    barConfig
];

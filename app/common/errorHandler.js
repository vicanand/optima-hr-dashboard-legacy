var Constant = require('../common/constant');
//var Constant = require('../common/constant');

let path = require('path');
const rootPath = path.normalize(__dirname);
const env = process.env.NODE_ENV || 'development_stage';

let logErrors = function(err, req, res, next) {
    next();
};

let notFoundHandler = function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
};

let errorHandler = function(err, req, res, next) {                          
    let devErrMsg = err.toString().replace('<%-','').replace('<%','').replace('%>','');
    let errorStatus = err.status || 500;
    let errorTemplate;
    if (errorStatus === 500) {
        errorTemplate = '500-error-page';
        if(env == 'development_stage'){
            err.message = devErrMsg;
        }
        else{
            err.message = Constant.ERROR_MEESAGE;
        }
        err.mailto = res.locals.country_details.supportEmail;
    } else if (errorStatus === 404) {
        errorTemplate = 'notFound';
    } else if (res.locals.showErrorWitHdr) {
        errorTemplate = 'error-page';
    } else {
        errorTemplate = 'errorFound';
    }
    res.status(errorStatus);
    res.render(errorTemplate, {
        message: err.message,
        mail: err.mailto,
        error: {},
        title: 'Error'
    });
};

module.exports = function(app) {
    //  app.use(logErrors);
    app.use(notFoundHandler);
    app.use(errorHandler);
}

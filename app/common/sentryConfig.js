/**
 * This file is responsible to configure sentry in SSO.
 * Check integration of sentry to express js at https://docs.sentry.io/clients/node/integrations/express/
 * To check sentry dashboard visit https://sentry.io/zeta-tech/sso/
 */
var Raven =  require('raven');

/**
 * Initialize sentry [Raven is JS lib for sentry to integrate]
 * Sentry DNS Key('https://f7f4c0c362ae440e95d9045e99b644a6@sentry.io/1227935') is specific to SSO
 * @param {Object} app - Express app object
 */
const _initialize = (app) => {

    console.log(process.env.NODE_ENV);

    if (process.env && process.env.NODE_ENV && process.env.NODE_ENV.indexOf('stage') < 0 && app ) {        
        Raven.config('https://fa7e5db56f1744c78a0c85e9e3a00d4f@sentry.io/1251214', {
            environment: process.env.NODE_ENV
        }).install();
        app.use(Raven.requestHandler());
        _initialized = true;    
}
};

/**
 * Use sentry error handler to log client error in dashboard.
 * This must be used after routeHandler. See usage in express.js
 * @param {Object} app - Express app object
 */
const _errorHandler = (app) => {
    app.use(Raven.errorHandler());
}

/**
 * To log custom errors in sentry dashboard
 * @param {*} message this can be Error object or message string
 * @param {*} type values are "error/warning"
 * @param {*} data data to be send along with custom error
 * @param {*} logger can be current function, class, file name to help in debugging
 */
const _logException = (logObject) => {
    if (logObject.message) {
        let message = logObject.message;
        let type = logObject.type || 'error';
        let data = logObject.data || {};
        let logger = logObject.logger || 'javascript';
        Raven.captureException(message, {
            level: type || 'error',
            logger: logger,
            extra: {data}
        });
    }
}

module.exports = {
    initialize : _initialize,
    errorHandler: _errorHandler,
    logException: _logException
}
"use strict";
const _ = require("underscore");
var Constant = require("../common/constant");
var AuthSvc = require("../service/authSvc");
var Util = require("../common/util.js");
var UtilsUser = require("../common/utils/user.js");
const configSetter = require("../common/countryConfig/configSetter");

let loginCheck = function(req, res, next) {
  if (!req.session["requestedSource"]) {
    req.session["requestedSource"] = req.query.source || "";
  }
  if (req.path == "/genie") {
    req.session["genie"] = req.query.corpId;
  }
  if (!res.locals.country_details) {
    res.locals.country_details = {
      currencySymbol: "₹",
      currencyName: "INR",
      supportEmail: "support@zeta.in"
    };
  }
  if (
    req.originalUrl != "/email" &&
    req.originalUrl != "/verifyNewCorporateDetails" &&
    !req.originalUrl.includes("/setSelectedCorporatesID") &&
    !req.originalUrl.includes("/checkAuthTokenExist") &&
    !req.originalUrl.includes("/verifyEmail") &&
    !req.originalUrl.includes("/signup") &&
    !req.originalUrl.includes("/hdfcSignup") &&
    !req.originalUrl.includes("/vn-signup") &&
    !req.originalUrl.includes("/signin") &&
    !req.originalUrl.includes("/account/link") &&
    !req.originalUrl.includes("/account/corporateSwitch") &&
    !req.originalUrl.includes("/account/noAccount") &&
    !req.originalUrl.includes("/account/inactiveAccount") &&
    !req.originalUrl.includes("/fetchinggift") &&
    !req.originalUrl.includes("/session") &&
    !req.originalUrl.includes("/logout") &&
    !req.originalUrl.includes("/login/") &&
    !req.hostname.includes("signup-vt") &&
    !req.hostname.includes("signup.sodexo.vn")
  ) {
    AuthSvc.redirectToLogin(req, res).then(
      function(respData) {
        var retailCorp = "RETAIL_CORPORATE";
        if (respData) {
          // if(req.query.corpID){
          //     res.redirect('/setSelectedCorporatesID?corpID='+req.query.corpID);
          // }
          // if(req.originalUrl.indexOf('dashboard') > -1){
          if (respData.corporateCompanies.length == 0) {
            res.redirect("/account/noAccount");
          } else if (
            respData.corporateCompanies.length == 1 &&
            respData.corporateCompanies[0].corporate &&
            respData.corporateCompanies[0].corporate.type.indexOf(
              retailCorp
            ) !== -1
          ) {
            res.redirect("/account/noAccount");
          } else if (respData.corporateCompanies.length == 1) {
            if (req.session["requestedSource"]) {
              res.redirect("/documents-drive?source=remote");
            } else {
              AuthSvc.goToCorporate(req, res, next);
            }
          } else {
            res.redirect("/account/corporateSwitch");
          }
          // }
          // else{
          //     let url = req.originalUrl.substr(0, req.originalUrl.indexOf('?')?req.originalUrl.indexOf('?'):req.originalUrl.length);
          //     res.redirect('https://' + req.headers.host + url || '/dashboard');
          // }
        } else {
          next();
        }
      },
      function(error) {
        var noAccErr;
        if (error && error.body) {
          try {
            noAccErr = JSON.parse(error.body);
          } catch (err) {}
        }
        if (noAccErr && noAccErr.type) {
          if (noAccErr.type == "AccountNotVerifiedException") {
            req.session[Constant.SESSION_INACTIVE_EMAILS] = noAccErr.message;
            res.redirect("/account/inactiveAccount");
          } else if (
            (req.headers.host.indexOf("smdigitalportal") != -1 &&
            noAccErr.type == "CorporateNotPresentException")  || 
            ((req.headers.host.indexOf('sodexo.ph') != -1 || req.headers.host.indexOf('sodexoph') != -1) && 
            noAccErr.type == 'CorporateNotPresentException')
          ) {
            req.session[Constant.SESSION_INACTIVE_EMAILS] = noAccErr.message;
            res.redirect("/account/inactiveAccount");
          }
        }
        if (!Util.checkIfAuthErrorAndRedirect(error, req, res)) {
          var errorMessage = getErrorMessage(error);
          // req.flash('errors', errorMessage);
          // url = '/email';
          // res.redirect(url);
          // res.redirect(Config.ssoUrl + '?redirectUrl=' + 'https://' + req.headers.host + '/dashboard');
        }
      }
    );
  } else {
    next();
  }
};

let getErrorMessage = function(error) {
  if (error && error.body) {
    try {
      error = JSON.parse(error.body);
      return error.error;
    } catch (e) {
      return error.body;
    }
  }
  return "";
};

let sortCompany = function(req, res, next) {
  let companiesJSON = req.session[Constant.SESSION_COMP_INFO];

  function compare(a, b) {
    const genreA = a.company.companyName.toUpperCase();
    const genreB = b.company.companyName.toUpperCase();

    let comparison = 0;
    if (genreA > genreB) {
      comparison = 1;
    } else if (genreA < genreB) {
      comparison = -1;
    }
    return comparison;
  }
  return companiesJSON.sort(compare);
};

let localData = function(req, res, next) {
  // if(req.originalUrl.indexOf('renew') === -1 && req.originalUrl.indexOf('authToken') === -1){
  //     let url = req.originalUrl;
  //     if(url.indexOf('authToken') > -1){
  //         url = req.originalUrl.substr(0, req.originalUrl.indexOf('?'));
  //     }
  //     req.session.redirect = url;
  // }
  // console.log(req.session);

  /**
   * HOST_NAME setter
   */
  configSetter.setHost(req, res, next);

  let companyIDs = [];
  if (
    req.session &&
    req.session[Constant.SESSION_SELECTED_COMP_INFO] &&
    req.session[Constant.SESSION_ACC_INFO] &&
    req.session[Constant.SESSION_CORP_INFO] &&
    req.session[Constant.SESSION_COMP_INFO]
  ) {
    if (req.session["genie"] != 0) {
      res.locals.checkGenie = req.session["genie"];
    }
    res.locals.user = req.session[Constant.SESSION_ACC_INFO];
    res.locals.corpList = req.session[Constant.SESSION_CORP_LIST];
    res.locals.corp = req.session[Constant.SESSION_CORP_INFO];
    res.locals.companies = req.session[Constant.SESSION_COMP_INFO];
    res.locals.companies = Util.sortArrayOfObject(
      res.locals.companies,
      "company.companyName",
      "ASC"
    );
    req.session[Constant.SESSION_COMP_INFO].map(function(company) {
      companyIDs.push(company.company.companyID);
    });
    res.locals.companyInfo = req.session[Constant.SESSION_COMPANY_SHORT_CODE];
    res.locals.companyIds = companyIDs.join(",");
    res.locals.selectedCompany =
      req.session[Constant.SESSION_SELECTED_COMP_INFO];
    res.locals.authToken = req.session[Constant.SESSION_AUTH].authToken;
    res.locals.sessionID = req.sessionID;
    res.locals.userID = req.session.auth.userId;
    res.locals.expenseActive = UtilsUser.checkGKFlag(
      req,
      res.locals.selectedCompany.company.companyID,
      Constant.EXPENSE_GK_FLAG
    );
    res.locals.isExpense = UtilsUser.checkGKFlag(
      req,
      res.locals.selectedCompany.company.companyID,
      Constant.IS_EXPENSE_CORP
    );

    res.locals.bulkOrderActive = UtilsUser.checkGKFlag(
      req,
      res.locals.selectedCompany.company.companyID,
      Constant.BULK_ORDER_GK_FLAG
    );
    res.locals.bulkCloseActive = UtilsUser.checkGKFlag(
      req,
      res.locals.selectedCompany.company.companyID,
      Constant.BULK_CLOSE_GK_FLAG
    );
    res.locals.expressDashboardActive = UtilsUser.checkGKFlag(
      req,
      res.locals.selectedCompany.company.companyID,
      Constant.EXPRESS_DASHBOARD
    );
    res.locals.restrict_to_express_insights = UtilsUser.checkGKFlag(
      req,
      res.locals.selectedCompany.company.companyID,
      Constant.EXPRESS_RESTRICT
    );
    res.locals.surveyLaunch = UtilsUser.checkGKFlag(
      req,
      res.locals.selectedCompany.company.companyID,
      Constant.SURVEY_LAUNCH
    );
    res.locals.reportCentreActive = 1;
    res.locals.powerCenterEnabled = UtilsUser.checkGKFlag(
      req,
      res.locals.selectedCompany.company.companyID,
      Constant.POWER_CENTER_ENABLED
    );
    res.locals.programSetupEnabled = UtilsUser.checkGKFlag(
      req,
      res.locals.selectedCompany.company.companyID,
      Constant.PROGRAM_SETUP_ENABLED
    );
    res.locals.invoices_commercials = UtilsUser.checkGKFlag(
      req,
      res.locals.selectedCompany.company.companyID,
      Constant.INVOICES_COMMERCIALS
    );
    res.locals.powerCenterUserEnabled = UtilsUser.checkGKFlag(
      req,
      res.locals.selectedCompany.company.companyID,
      Constant.POWER_CENTER_USER_ENABLED
    );
    res.locals.lastLoginTime = req.session[Constant.LAST_LOGIN_TIME];
    res.locals.ifiBankName = req.session[Constant.SESSION_IFIS_BANK_NAME];
    res.locals.showGeneratedReports = true;
    res.locals.activateCloseCards = UtilsUser.checkGKFlag(
      req,
      res.locals.selectedCompany.company.companyID,
      Constant.ACTIVATE_CLOSE_CARD
    );

    res.locals.isVNReportCenterEnabled = UtilsUser.checkGKFlag(
      req,
      res.locals.selectedCompany.company.companyID,
      Constant.VN_REPORT_CENTER_ENABLED
    );
    //Gate Keeper not required for this problem
    // UtilsUser.checkGKFlag(req, res.locals.selectedCompany.company.companyID, Constant.REPORT_CENTER_GK_FLAG);

    /**
     * 1. Country Configurations Setter
     */
    configSetter.setCountryConfigs(req, res, function() {
      next();
    });

    // next();
  } else {
    next();
  }
};

let billingAddressCheck = function(req, res, next) {
  let waitingForData = false;
  if (req.session && req.session[Constant.SESSION_COMP_INFO]) {
    if (
      !req.originalUrl.includes("/setSelectedCorporatesID") &&
      !req.originalUrl.includes("/verifyNewCorporateDetails") &&
      req.originalUrl != "/checkAuthTokenExist" &&
      req.originalUrl != "/updateCompDtls" &&
      req.originalUrl != "/verifyEmail" &&
      req.originalUrl != "/account/corpDetails" &&
      req.originalUrl.split("?")[0] != "/account/link" &&
      req.originalUrl != "/email" &&
      req.originalUrl != "/logout" &&
      !req.originalUrl.includes("/account/corporateSwitch") &&
      req.originalUrl != "/" &&
      !req.originalUrl.includes("/login/")
    ) {
      let companyResp = req.session[Constant.SESSION_COMP_INFO];
      if (
        companyResp[0].company.attributes &&
        companyResp[0].company.attributes.reviewBillingAddress
      ) {
        res.redirect("https://" + req.headers.host + "/account/corpDetails");
      } else {
        if (!waitingForData) {
          next();
        }
      }
    } else {
      if (!waitingForData) {
        next();
      }
    }
  } else {
    if (!waitingForData) {
      next();
    }
  }
};

let privilegeCheck = function(req, res, next) {
  res.locals.rbac = null; // Default full access
  const configData = req.session[Constant.SESSION_CONFIG_DATA];
  if (
    configData &&
    configData.generalConfigs &&
    configData.generalConfigs.enableRBAC
  ) {
    AuthSvc.privilegeCheck(req, res, next).finally(function() {
      if (req.session[Constant.SESSION_RBAC_RESOURCES]) {
        // RBAC RESOURCES
        res.locals.rbac = req.session[Constant.SESSION_RBAC_RESOURCES];
      }
      next();
    });
  } else {
    next();
  }
};

module.exports = function(app) {
  app.use(loginCheck);
  app.use(localData);
  app.use(privilegeCheck);
  app.use(billingAddressCheck);
};

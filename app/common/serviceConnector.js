const request = require('request');
const queryString = require('query-string');
const Promise = require('bluebird');
const escapeHtmlInJson = require('escape-html-in-json');
const userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36';
var Util = require('../common/util');
var logger = require('./logger').createLogger({ name: 'utilAppFolder' });

let config = require('../../config');

let post = function(endPoint, params, headers, req) {
    console.log("post endpoint",endPoint);
    console.log("post params",params);
    console.log("post headers",headers);
    headers = headers || {};
    headers['User-Agent'] = userAgent;
    copyRequiredHeaders(req, headers);

    return new Promise(function(resolve, reject) {
        return request.post(
            endPoint, {
                json: params,
                headers: headers
            },
            function(error, response, body) {

                if (!error && response.statusCode == 200) {
                    let data = JSON.parse(JSON.stringify(body, escapeHtmlInJson));
                    console.log("data===============================",data);                     
                    resolve(data);
                } else {
                    if(response){
                        let serviceCal = response ? response.request.href : '';
                        let serviceCall = serviceCal ? serviceCal.substr(0,serviceCal.indexOf('?')) : '';
                        delete logger.fields.pid;
                        logger.info({"body": response.body, "status Code": response.statusCode,
                            "status Message": response.statusMessage, "method": response.req.method, "service Call" : serviceCall,
                        }, 'API Failed');
                    }
                    reject(response);
                }
            }
        );
    });
};

function copyRequiredHeaders(req, headers) {
    if (req && req.headers) {
        let xff = req.headers['x-forwarded-for'];
        if (xff) {
            headers['zeta-x-forwarded-for'] = xff;
        }
    } else {
        headers['zeta-x-forwarded-for'] = 'PASS_THROUGH';
    }
}

var get = function(endPoint, params, headers, req) {
    console.log("get endpoint",endPoint);
    console.log("get params",params);
    console.log("get headers",headers);
    headers = headers || {};
    headers['User-Agent'] = userAgent;
    copyRequiredHeaders(req, headers);
    return new Promise(function(resolve, reject) {
        var currentTime = new Date().getTime();
        return request.get(
            endPoint, {
                qs: params,
                headers: headers
            },
            function(error, response, body) {
                if (!error && response.statusCode == 200) {
                    resolve(JSON.stringify(JSON.parse(body), escapeHtmlInJson));
                    console.log("data===============================",body);                                                                                                   
                } else {
                    if(response){
                        let serviceCal = response.request.href;
                        let serviceCall = serviceCal.substr(0,serviceCal.indexOf('?'));
                        delete logger.fields.pid;
                        logger.info({"body": response.body, "status Code": response.statusCode,
                            "status Message": response.statusMessage, "method": response.req.method, "service Call" : serviceCall,
                        }, 'API Failed');    
                    }                                          
                    reject(response);
                }
            }
        );
    });
};

module.exports = {
    post: post,
    get: get
};
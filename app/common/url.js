const url = {
    LOGIN_BASE_URL: "?redirecturl=",
    LOGIN: 'account/login', //Not Used
    LOGOUT: 'account/logout', //Not Used
    GET_BENEFIT_PROGRAM_PAGINATION_POST: 'corpben/1.0/getBenefitPrograms',
    GET_BENEFIT_PROGRAM_PAGINATION: 'corpben/1.0/getBenefitPrograms?token=:authToken:&corpID=:corpID:&companyID=:companyID:&programStatus=:programStatus:&pageNumber=:pageNumber:&pageSize=:pageSize:', // with pagination
    GET_BENEFIT_PROGRAM_PAGINATION_ADMIN: 'corpben/1.0/getBenefitPrograms?token=:authToken:&corpID=:corpID:&companyID=:companyID:&pageNumber=:pageNumber:&pageSize=:pageSize:', // with pagination
    GET_BENEFIT_PROGRAM: 'corpben/1.0/getBenefitPrograms?token=:authToken:&corpID=:corpID:&companyID=:companyID:&programStatus=:programStatus:',
    GET_RECOMMENDED_PROGRAMS: 'corpben/1.0/getRecommendedProgramTypes?token=:authToken:&corpId=:corpID:&companyId=:companyID:',
    GET_BENEFIT_DETAILS_PROGRAM: 'corpben/1.0/getBenefitProgramDetails?token=:authToken:&corpID=:corpID:&companyID=:companyID:&programID=:programID:',
    GET_CORPORATE: 'corporate', //Not Used
    GET_CORP_ACCOUNT_DETAILS: 'accountByZetaUserID?userID=:userID:&token=:authToken:', //Not Used
    GET_COMPANIES: 'account/companies', //Not Used
    GET_ACCOUNT: 'account', //Not Used
    GET_FUNDING_ACCOUNTS: 'corpben/1.0/getFundingAccounts?corpID=:corpID:&pageNo=:pageNo:&pageSize=:pageSize:&token=:authToken:',
    GET_FUNDING_ACCOUNT: 'corpben/1.0/getFundingAccountDetails?corpID=:corpID:&accountID=:accountID:&token=:authToken:',
    ADD_FUND_ACCOUNT: 'corpben/1.0/addFundingAccount?token=:authToken:',
    GET_DOCS: 'corpben/1.0/getDocGroups',
    GET_DOC_DETAILS: 'corpben/1.0/getDocGroup',
    GET_BILL_DETAILS: 'biller/1.0/getBill',
    GET_DASHBOARD_REPORTS: 'corpben/1.0/getDashboardReports?token=:authToken:&corpID=:corpID:&hidden=:status:',
    GET_DASHBOARD_REPORTS_ON_PRODUCTS: 'corpben/1.0/getDashboardReportsForProduct?token=:authToken:&corpID=:corpID:&hidden=:status:&corpProductType=:product:',
    GET_CORPORATE_DETAIL: 'corpben/1.0/getCorpProgramDetails?corpID=:corpID:&token=:token:',
    GET_EMPLOYEE_DETAILS: 'corpben/1.0/getEmployeeDetails?token=:authToken:&corpID=:corpID:&employeeID=:employeeID:',
    CREATE_PROGRAM: 'corpben/1.0/createCardProgram?token=:authToken:',
    UPDATE_CARD_FUNDING_ACC: 'corpben/1.0/updateCardProgramFundingAccount?token=:authToken:',
    GET_EMPLOYEESV2: 'corpben/1.0/getEmployeesV2?corpID=:corpID:&token=:authToken:',
    GET_PROGRAMDETAILSV2: 'corpben/1.0/getProgramDetails?token=:authToken:',
    GET_TRANSFER_DETAILS: 'account/companies/:companyID:/orders/:orderID:?token=:authToken:&corpID=:corpID:',
    DOWNLOAD_UPLOADED_FILE: 'account/:authToken:/orders/:orderId:/downloadUploadedFile',
    GET_PROFORMA_INVOICE: 'account/:authToken:/orders/:orderId:/proforma-invoice',
    CHECK_AGREEMENT_SIGNUP: 'instagift/1.0/checkAgreementSignup?token=:authToken:&companyID=:companyID:&corpID=:corpID:&programType=INSTAGIFT',
    GET_INSTAGIFT_PAYOUTS: 'instagift/1.0/getPayouts?token=:authToken:&companyID=:companyID:&corpID=:corpID:&orderID=:orderID:',
    GET_INSTAGIFT_ORDERS: 'instagift/1.0/getOrders?token=:authToken:&companyID=:companyID:&corpID=:corpID:&programID=:programID:&orderID=:orderID:',
    GET_FILE_UPLOAD_DETAILS: 'getFileUploadDetails',
    GET_ASSET_UPLOAD_ENDPOINT: 'locker/1.0/getAssetUploadEndpoint',
    GET_GATEKEEPERS: 'gatekeeper/1.0/getListGKsWithResult?token=:authToken:',
    ASSOCIATE_ZETA_USER_WITH_CORP: 'corpben/1.0/associateZetaUserWithCorp?token=:authToken:',
    DOWNLOAD_PAYOUT_REPORT: 'account/orders/:orderID:/payouts/download?token=:authToken:&corpID=:corpID:',
    CARD_DETAILS_SYNC: 'corpben/1.0/triggerReportSync?token=:authToken:',
    GET_CORP_CARD_PROGRAM: 'corpben/1.0/getCorpCardProgram?token=:authToken:',
    NEW_SIGNUP_CORPORATE: 'corpben/1.0/corporateSignUpRequest',
    GET_SIGNED_DOCUMENT: 'corpben/1.0/signPDF?token=:authToken:&pdfURL=:s3FileUrl:&apiKey=:apiKey:&x=:x:&y=:y:&width=:width:&height=:height:',
    GET_CORP_COMPANY_DETAILS: 'corpben/1.0/getCorpCompanyDetails?token=:authToken:&corpID=:corpID:',
    GET_PRIVILEGIES: 'certstore/1.0/getPrivileges?token=:authToken:&subjectJID=:subjectJID:',
    GET_CORP_ACCOUNTSV2: 'corpben/1.0/getCorpAccountsV2?corpID=:corpID:&token=:authToken:',

    //report centre
    GET_ARCHIVED_REPORTS: 'corpben/1.0/getArchivedReports?corpID=:corpID:&token=:authToken:&pageNo=:pageNo:&pageSize=:pageSize:&latestFirst=true&corpProductType=:product:',

    GET_IFIS_ACCOUNTS: 'corpben/1.0/getIFIDetails?corpID=:corpID:&ifiID=:ifiID:&type=:type:&token=:authToken:',
    GET_LAST_LOGIN: 'corpben/1.0/recordUserLogin?token=:token:&corpID=:corpID:',
    GET_CORPORATE_LIST: 'corpben/1.0/listCorporatesForAccount?token=:authToken:&corporateID=:corporateID:',
    GET_USER_PROFILE: 'zetauser/1.0/getUserProfile?userJID=:userJID:',
    GET_GENIE_FOR_USER: 'corpben/1.0/getGenieForUser?userId=:userId:&corpId=:corpId:',
    GET_COMPANY_DETAILS: 'corpben/1.0/getCorpCompanyDetails?corpID=:corpID:&token=:authToken:',
    UPDATE_CORPORATE_DETAILS: 'corpben/1.0/verifyNewCorporateDetails?token=:authToken:',

    CREATE_SELECT_GITFTING_ORDER: 'partner-gifting/1.0/createProgram?',
    GET_SELECT_GITFTING_PROGRAM: 'partner-gifting/1.0/getProgram?corporateId=:corporateId:&companyId=:companyId:&spotlightType=SELECT_GIFTING&token=:token:',
    GET_SELECT_GITFTING_DETAILS: 'partner-gifting/1.0/listProduct?corporateId=:corporateId:&companyId=:companyId:&spotlightType=SELECT_GIFTING&token=:token:',
    GET_SELECT_GITFTING_ORDERLIST: 'partner-gifting/1.0/listOrder?',
    GET_SELECT_GITFTING_GENERATE: 'partner-gifting/1.0/generateGiftCard?claimResource=:claimResource:',
    GET_PROGRAM_SHORT_CODES: 'corpben/1.0/getProgramShortCodes?token=:authToken:&corpID=:corpID:',
    GET_COMPANIES: 'corpben/1.0/getCompanies?token=:authToken:&corpID=:corpID:',
    CARD_TEMPLATE_TYPE: 'corpben/1.0/getCardProgramTemplatesByType?token=:token:&corporateID=:corporateID:&companyID=:companyID:&cardProgramType=:cardProgramType:',
    //Virtual
    VIRTUAL_CARD_TEMP:'instagift/1.0/listTemplates?corpID=:corpID:&companyID=:companyID:&pageSize=:pageSize:&pageNumber=:pageNumber:&type=:type:',
    GET_VIRTUAL_INVOICE:'instagift/1.0/getOrderInvoice?token=:token:&corporateID=:corpID:&companyID=:companyID:&orderID=:order:',
    GET_VIRTUAL_PROFORMA_INVOICE:'instagift/1.0/getOrderProformaInvoice',
    GET_PHYSICAL_CARD_PROFORMA_INVOICE:'instagift/1.0/getCardOrderProformaInvoice',
    // TRIGGER_REPORT_SYNC: 'corpben/1.0/triggerReportSync?token=:authToken:',

    //RnR
    LIST_PROGRAM_DETAILS:'sirius/1.0/listProgramDetails?companyID=:companyID:&corpID=:corpID:&token=:token:',
    GET_POINT_BASED_PERFORMA_INVOICE:'sirius/1.0/getFileOrderPerformaInvoice?token=:token:',


    //Express
    EXPRESS_GET_QUICKSHOP_INFO: 'quickshop/1.0/getQuickshopInfo',
    EXPRESS_GET_QUICKSHOP_INFO_DATE: 'quickshop/1.0/getQuickshopForDate',
    GET_CORP_CAFE_INSIGHTS: 'zetamerchant/1.0/getCorporateCafeteriasInsights?token=:authToken:',
    GET_CAFETERIA_INSIGHTS_SNAPSHOT: 'zetamerchant/1.0/getCafeteriaInsightsSnapshot?token=:authToken:',
    GET_EMPLOYEE_INSIGHTS_SNAPSHOT: 'zetamerchant/1.0/getEmployeeInsightsSnapshot?token=:authToken:',
    GET_VENDOR_TRENDS: 'zetamerchant/1.0/getVendorTrends',
    GET_STORE_TRENDS: 'zetamerchant/1.0/getStoreTrends',
    GET_ALL_STORES: 'zetamerchant/1.0/getAllStores',
    GET_PAYMENT_PREFERENCE: 'zetamerchant/1.0/getPaymentPreference',
    GET_EMPLOYEE_TRANSACTION_DETAILS: 'zetamerchant/1.0/getEmployeeInsightsTransactionDetails',
    GET_SALES_TRANSACTION: 'zetamerchant/1.0/getEmployeeInsightsTransactionDetails',
    GET_VENDOR_INSIGHTS_SNAPSHOT: 'quickshop/1.0/getVendorInsightsSnapshot',
    GET_FOOT_FALL: 'zetamerchant/1.0/getFootfall',
    GET_SETTLEMENT_FOR_COUNTER: 'settlement/1.0/getSettlements',
    GET_SETTLEMENT_METRICS: 'settlement/1.0/getSettlementAggregates',
    GET_VENDORS: 'zetamerchant/1.0/getVendors',
    GET_BUSINESS_PROFILE: 'business/1.0/getBusinessProfile',
    GET_STORES_IN_BUSINESS_FOR_CORPORATE: 'zetamerchant/1.0/getStoresInBusinessForCorporate',
    GET_ITEM_TRENDS: 'quickshop/1.0/getItemTrends',
    GET_ALL_ITEMS: 'quickshop/1.0/getAllItems',
    GET_LIST_OF_UPLOAD_MENUS: 'quickshop/1.0/getListOfUploadedMenus',
    GET_SELECT_GITFTING_INVOICEURL: 'partner-gifting/1.0/getOrderInvoice?',
    GET_SELECT_GITFTING_PINVOICEURL: 'partner-gifting/1.0/getOrderProformaInvoice?',


    //Feedback
    GET_CUSTOMER_RATINGS: 'quickshop/1.0/getCustomerRatings',
    GET_CUSTOMER_FEEDBACK: 'quickshop/1.0/getCustomerFeedback',
    GET_CUSTOMER_COMMENTS: 'quickshop/1.0/getCustomerComments',
    GET_TRANSACTIONS: 'corpben/1.0/getTransactionsForCardID?token=:token:&cardID=:cardID:&corpID=:corpID:&offset=0&order=OLDEST_FIRST&pageSize=:pageSize:&recordTypes=DEBIT&recordTypes=CREDIT&zetaUserID=:zetaUserID:&after=:after:&before=:before:',
    GET_BILL_FOR_CARDPROGRAM: 'biller/1.0/getBillsForCardprogram?token=:token:',
    GET_CARD_ELIGIBILITY: 'corpben/1.0/getCorpConfig?token=:authToken:&corpID=:corpID:&cardID=:cardID:',
    GET_REPORT_BY_ID: 'corpben/1.0/downloadReportByID?token=:authToken:&corporateID=:corporateId:&reportID=:reportId:',


    // card order apis
    CARD_ORDER_LIST: 'instagift/1.0/getApplicableCards?corpID=:corpID:&companyID=:companyID:',
    CARD_ORDER_DETAILS: 'instagift/1.0/getSuperCardDetails?token=:token:',

    GET_CHECKLIST_BY_RULE: 'biller/1.0/getChecklistItems?token=:authToken:',
};

module.exports = url;
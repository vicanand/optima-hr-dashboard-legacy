/**
 * Country Configurations Setter
 */
const _ = require('lodash');
const env = process.env.NODE_ENV || 'development_stage';
const configLoader = require('./configLoader');
const Constant = require('../constant');
const {
  COUNTRY_CONFIGS, 
  HOST_NAME_MAPPINGS, 
  COUNTRY_VAR_MAPPINGS, 
  CONFIGS_MAPPINGS
} = require('./configMappings');

/**
 * HOST_NAME setter
 * @param {Object} req request object
 * @param {Object} res response object
 * [TO_ADD]: Add new host name in HOST_NAME_MAPPINGS
 * Key Name => ** hostName **
 */
const setHost = function(req, res) {
  if (!res.locals.hostName) {
    const foundHost = _.findKey(HOST_NAME_MAPPINGS, function(domains) { 
      return new RegExp(domains.join("|")).test(req.headers.host);
    });
    if (foundHost) {
      res.locals.hostName = foundHost;
    }
  }
}

/**
 * Load country configs file
 * @param {Object} req request object
 * @param {String} countryVar Country Variable
 * [IMPORTANT]: Functions won't be supported as config, so please avoid using functions in config file for app/
 */
const initConfigurations = function(req, countryVar) {
  return new Promise((resolve, reject) => {
    if (CONFIGS_MAPPINGS[countryVar] && CONFIGS_MAPPINGS[countryVar].CONFIG_FILE_PATH && !req.session[Constant.SESSION_CONFIG_DATA]) {
      configLoader.getConfig(CONFIGS_MAPPINGS[countryVar].CONFIG_FILE_PATH, function(error, data) {
        if (error) {
          console.log('error', error);
        } else {
          // Set config data to session
          req.session[Constant.SESSION_CONFIG_DATA] = data;
        }
        resolve();
      });
    } else {
      resolve();
    }
  });
}

/**
 * Details setter
 * @param {Object} req request object
 * @param {Object} res response object
 * [IMPORTANT]: Required to call after res.locals.countryVar set
 * [TO_ADD]: Add new mapping/details in CONFIGS_MAPPINGS =>
 * Key Name => ** country_details **
 */
const setDetails = function (req, res) {
  let details = { // Default details
    currencySymbol: "₹",
    currencyName: "INR",
    supportEmail: "support@zeta.in"
  };
  if (CONFIGS_MAPPINGS[res.locals.countryVar] && CONFIGS_MAPPINGS[res.locals.countryVar].DETAILS) {
    details = CONFIGS_MAPPINGS[res.locals.countryVar].DETAILS;
  }

  res.locals.country_details = details;
}

/**
 * Country Variable Setter
 * @param {Object} req request object
 * @param {Object} res response object
 * @param {Function} next next callback
 * [TO_ADD]: Add condition and set key = "YOUR COUNTRY VAR"
 * Key Name => ** [isSodexoPhpSM, isSodexoVn, ...] **
 */
const setCountryConfigs = function(req, res, next) {
  let key = "isIndia"; // Default country
  if (COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.SM_CORP_IDS[env].indexOf(parseInt(res.locals.corp.id)) > -1) {
    key = COUNTRY_VAR_MAPPINGS.PHILIPPINES_SM;
  } else if (COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.CORP_IFI[env] == res.locals.corp.ifi) {
    key = COUNTRY_VAR_MAPPINGS.PHILIPPINES;
  } else if (COUNTRY_CONFIGS.SODEXO_VIETNAM_CONFIGS.CORP_IFI[env] == res.locals.corp.ifi) {
    key = COUNTRY_VAR_MAPPINGS.VIETNAM;
  }
  // Set variabel to countryVar property on res.locals
  res.locals.countryVar = key;

  // Set Details [Currency Code, Currency Symbol, Support Email, ...]
  setDetails(req, res);

  // Init Configs
  initConfigurations(req, key).then(() => {
    next();
  });
}

module.exports = {
  setHost,
  setCountryConfigs
}
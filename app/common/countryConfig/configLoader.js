/**
 * Country Config Loader
 */

const fs = require("fs");

/**
 * 
 * @param {String} path - Config file path Example: /Philippines/ph.js
 * @param {Function} cb - Callback function
 */
const getConfig = function(path, cb) {
  if (!path) {
    cb('Invalid argument');
    return;
  }
  fs.access(__dirname + path, fs.F_OK, function(err) {
    if (err) {
      cb(err);
      return;
    }
    const finalPath = "." + path;
    const configData = require(finalPath);
    if (configData) {
      cb(null, configData);
    } else {
      cb('No config data found');
    }
  });
};

module.exports = {getConfig};
const COUNTRY_CONFIGS = {
  // Sodexo Philippines Configs
  SODEXO_PHILIPPINES_CONFIGS: {
      APP_ID: 2002,
      COUNTRY_CODE: '63',
      SM_CORP_IDS: {
        'development_stage': [11660],
        'development_production': [57546, 57863],
        'development_preprod': [16183, 16378, 16623],
        'stage': [11660],
        'production': [57546, 57863],
        'preprod': [16183, 16378, 16623],
      },
      CORP_IFI: {
        'development_stage': 329,
        'development_preprod': 308,
        'development_production': 181,
        'stage': 329,
        'preprod': 308,
        'production': 181,
      },
      PLATFORM_IFI: {
        'development_stage': 151287,
        'development_production': 155103,
        'development_preprod': 285134,
        'stage': 151287,
        'production': 285134,
        'preprod': 155103,
      },
      SSO_URL: {
        'development_stage': 'https://sso-stage-ph.zetaapps.in/',
        'development_production': 'https://sso-ph.zetaapps.in/',
        'development_preprod': 'https://sso-pp-ph.zetaapps.in/',
        'stage': 'https://sso-stage-ph.zetaapps.in/',
        'production': 'https://sso-ph.zetaapps.in/',
        'preprod': 'https://sso-pp-ph.zetaapps.in/',
      }
  },
  // Sodexo Vietnam Configs
  SODEXO_VIETNAM_CONFIGS: {
      COUNTRY_CODE: '84',
      CORP_IFI: {
        'development_stage': 333,
        'development_preprod': 309,
        'development_production': 178,
        'stage': 333,
        'preprod': 309,
        'production': 178,
      },
      PLATFORM_IFI: {
        'development_stage': 151515,
        'development_preprod': 156924,
        'development_production': 290831,
        'stage': 151515,
        'preprod': 156924,
        'production': 290831,
      },
      URL : {
        'development_stage': 'http://corporates-stage.zetaapps.in/?ifi=151515',
        'development_preprod': 'https://sodexovn-corporates-pp.zetaapps.in/',
        'development_production': 'https://client.sodexo.vn',
        'stage': 'http://corporates-stage.zetaapps.in/?ifi=151515',
        'preprod': 'https://sodexovn-corporates-pp.zetaapps.in/',
        'production': 'https://client.sodexo.vn',
      },
      VN_BASE_URL: {
        'development_stage': 'http://sodexo-vn.stage.esg.zeta.in',
        'development_preprod': 'https://sodexo-vn.preprod.zeta.in',
        'development_production': 'https://sodexovn.gw.zetapay.in',
        'stage': 'http://sodexo-vn.stage.esg.zeta.in',
        'preprod': 'https://sodexo-vn.preprod.zeta.in',
        'production': 'https://sodexovn.gw.zetapay.in'
      },
      CATALOG_ROOT_ID: {
        'development_stage' : '',
        'stage':'',
        'preprod' : '1089504876052751034',
        'development_preprod' : '1089504876052751034',
        'production': '3258696887230378116',
        'development_production': '3258696887230378116'
    }
  }
};

/**
 * KEY = Currency
 * VALUE = Currency Config
 */
const CURRENCY_CONFIGS = {
    USD: {
      symbol: '$',
      languages: {'en-US': {displaySymbolPosition: 'PREFIX'}},
    }, // US Dollar
    EUR: {
      symbol: '€',
      languages: {'en-US': {displaySymbolPosition: 'PREFIX'}}
    }, // Euro
    INR: {
      symbol: '₹',
      languages: {'en-IN': {displaySymbolPosition: 'PREFIX'}}
    }, // Indian Rupee
    PHP: {
      symbol: '₱',
      languages: {'en-PH': {displaySymbolPosition: 'PREFIX'}},
      minimumFractionDigits: 2
    }, // Philippine Peso
    VND: {
      symbol: '₫',
      languages: {'en-VN': {displaySymbolPosition: 'PREFIX'}}
    }, // Vietnamese Dong
}

/**
 * KEY = Host Name
 * VALUE = Domain Name to decide host
 */
const HOST_NAME_MAPPINGS = {
  PHSM: ['smdigitalportal'], // Philippines SM,
  PH: ['sodexo.ph', 'sodexoph-'], //Phillipines
  VN: ['sodexovn', 'sodexo.vn'] // Vietnam
}

/**
 * KEY = Country Name
 * VALUE = Variable Value
 */
const COUNTRY_VAR_MAPPINGS = {
  PHILIPPINES_SM: "isSodexoPhpSm", // Philippines SM
  PHILIPPINES: "isSodexoPh", // Philippines
  VIETNAM: "isSodexoVn"
}

/**
 * KEY = Country Name as per Country Var
 * VALUE = Configs file path, as per current file relative (with no dot(.)), DETAILS, etc..
 */
const CONFIGS_MAPPINGS = {
  "isIndia": {
    DETAILS: {
      currencySymbol: "₹",
      currencyName: "INR",
      supportEmail: "support@zeta.in",
    }
  },
  "isSodexoPhpSm": {
    CONFIG_FILE_PATH: "/Philippines/phSm.js",
    DETAILS: {
      currencySymbol: "₱",
      currencyName: "PHP",
      supportEmail: "customercare.svc.ph@sodexo.com",
    }
  }, // Philippines SM
  "isSodexoPh": {
    CONFIG_FILE_PATH: "/Philippines/ph.js",
    DETAILS: {
      currencySymbol: "₱",
      currencyName: "PHP",
      supportEmail: "customercare.svc.ph@sodexo.com",
    }
  }, // Philippines
  "isSodexoVn": {
    CONFIG_FILE_PATH: "/Vietnam/vn.js",
    DETAILS: {
      currencySymbol: "₫",
      currencyName: "VND",
      supportEmail: "spv.info@sodexo.com",
    }
  } // Vietnam
}


module.exports = {
  COUNTRY_CONFIGS,
  CURRENCY_CONFIGS,
  HOST_NAME_MAPPINGS,
  COUNTRY_VAR_MAPPINGS,
  CONFIGS_MAPPINGS
};
const data = {
  // General Config
  "generalConfigs": {
    "enableRBAC": true, // Enable role based access
  },
  // app/controller/adminCtrl.js
  "adminCtrl": {
    "fundingAccHome": {
      "templateData": {
        bannerBgClass: "royal-blue-sodexo",
        breadcrumbs: null,
        headingText: "Funding Accounts",
        subHeadingText: "Funding accounts are similar to your bank’s current account. They can fund one or more programs across your companies. Use them to transfer funds to your beneficiaries.",
        subHeadingLearnMoreLink: null,
        addAccountHeading: 'With funding accounts your funds are instantly updated when you transfer money via Credit Term/Advance.'
      }
    },
    "funding-history": {
      "templateData": {
        bannerBgClass: "royal-blue-sodexo",
        breadcrumbs: [{href: "/admin/funding-accounts", text: "Funding Accounts"}]
      },
      "pageName": "fundingAccounts"
    },
    "addFund": {
      "templateData": {
        bannerBgClass: 'royal-blue-sodexo',
        breadcrumbs: [{href: '/admin/funding-accounts', text: 'Funding Account'}],
        headingText: 'Fund request details',
        disableFooterNote: true,
        transferTypes: [
          {value: 'CREDIT_TERM', text: 'Credit Term', tranferNumberLabel: 'OrderID'}, 
          {value: 'ADVANCE', text: 'Advance', tranferNumberLabel: 'OrderID'}],
        addFundClass: 'ph-add-fund',
        addFundFields: [
          {
              wrapperClass: 'pd-top-twenty quotationNumber',
              fieldLabel: 'SF Quotation Number',
              labelFor: 'transactionRefNumber',
              inputClass: 'width-xlg',
              inputId: 'transactionRefNumber',
              name: '',
              isCalendarInput: false,
              isRequired: true
          },
          {
              wrapperClass: 'orderId',
              fieldLabel: 'OrderID',
              labelFor: 'orderId',
              inputClass: 'width-xlg',
              inputId: 'orderId',
              name: '',
              isCalendarInput: false,
              isRequired: true
          },
          {
              wrapperClass: 'payment-proof-wrap hide',
              fieldLabel: 'Proof of Payment (attachment)',
              labelFor: 'paymentProof',
              inputClass: 'width-xlg',
              inputId: 'paymentProof',
              name: '',
              isCalendarInput: false,
              inputType: 'file',
              isRequired: false,
              isAttachment: true,
          },
          {
              wrapperClass: 'date-picker-xlg due-date-wrap',
              fieldLabel: 'Due Date',
              labelFor: 'dueDate',
              inputClass: 'bdr-radius datepicker-img',
              inputId: 'transactionDate',
              name: '',
              isCalendarInput: true,
              isRequired: true
          },
          {
              wrapperClass: 'chequeAmount',
              fieldLabel: `Credit Amount ( ₱ )`,
              labelFor: 'chequeAmount',
              inputClass: 'width-xlg',
              inputId: 'transactionAmount',
              name: '',
              isCalendarInput: false,
              isRequired: true
          },
          {
              wrapperClass: 'POText',
              fieldLabel: 'PO text field',
              labelFor: 'POText',
              inputClass: 'width-xlg',
              inputId: 'POText',
              name: '',
              isCalendarInput: false,
              isRequired: true
          },
          {
              wrapperClass: 'POAttachment',
              fieldLabel: 'PO Attachment',
              labelFor: 'POAttachment',
              inputClass: 'width-xlg',
              inputId: 'POAttachment',
              name: '',
              isCalendarInput: false,
              inputType: 'file',
              isRequired: true,
              isAttachment: true,
          }
      ],
      },
      "pageName": "fundingAccounts"
    },
    "companyDetails": {
      "templateData": {
        showTaxStatus: false
      }
    },
    "admin-companies": {
      "templateData": {
        pageHeading: 'Create companies in Sodexo if your business has multiple legal entities and you wish to seperate their benefits, rewards and expenses programs accordingly.'
      }
    },
    "accessCtrl": {
      addUserWithRoles: true
    }
  },
  //benefitsvc
  "benefitSvc": {
    programTypeArray: 'PROGRAM_TYPES_ARRAY_PH'
  },
  // app/controller/benefitCtrl.js
  "benefitCtrl": {
    "init": {
      "benefitHomePageData": {
        "templateData": {
          enableSpotlightPromotionModal: false,
          enableMopinion: false,
          showTaxStatus: false,
          documentQuoteText: "Manage disbursement and monitor performance of your programs."
        }
      },
    },
    "newTransfer": {
      TRANSFER_EXCEL_DOWNLOAD_FILE: {
        php_meal: 'download-samples/philippines/sample',
        php_gift: 'download-samples/philippines/sample',
        citymall_gift: 'download-samples/philippines/sample',
        wellcom_gift: 'download-samples/philippines/sample',
        smgp_gift: 'download-samples/philippines/sample',
        premiumpass_gift: 'download-samples/philippines/sample',
        rustan_gift: 'download-samples/philippines/sample',
        waltermart_gift: 'download-samples/philippines/sample',
        miniso_gift: 'download-samples/philippines/sample',

      },
      TRANSFER_PREVIEW_IMAGES: {
        php_meal: 'images/previews/philippines/meal-preview.png',
        php_gift: 'images/previews/philippines/meal-preview.png',
        citymall_gift: 'images/previews/philippines/meal-preview.png',
        wellcom_gift: 'images/previews/philippines/meal-preview.png',
        smgp_gift: 'images/previews/philippines/meal-preview.png',
        premiumpass_gift: 'images/previews/philippines/meal-preview.png',
        rustan_gift: 'images/previews/philippines/meal-preview.png',
        waltermart_gift: 'images/previews/philippines/meal-preview.png',
        miniso_gift: 'images/previews/philippines/meal-preview.png',
      }
    },
    "getBenefitDetails": {
      "templateData": {
        "overview": {
          showCardProgramLogo: true,
          enableCardLogoInput: false,
          showTaxAndExpiry: false
        }
      }
    }
  },
  // app/controller/employeeStetmentCtrl.js
  "employeeStetmentCtrl": {
    "init": {
      downloadSodexoPDF: true
    }
  },
  // app/controller/homeCtrl.js
  "homeCtrl": {
    "corpDetails": {
      "templateData": {
        logoClassName: "sodexo-logo",
        showSodexoHelpSupport: {
          speakToUsText: "Call us on +63 (02)6894700, Mon-Friday 9-6pm only except public holidays"
        },
        showCinPanGst: false
      }
    }
  },
  // app/controller/employeeCtrl.js
  "employeeCtrl": {
    "employeeDetailsV2": {
      "templateData": {
        showKYCStatus: false,
        showOverviewTab: false
      }
    }
  }
}

module.exports = data;
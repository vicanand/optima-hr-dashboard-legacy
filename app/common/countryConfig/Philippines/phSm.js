const data = {
  // General Config
  "generalConfigs": {
    "enableRBAC": true, // Enable role based access
  },
  // app/controller/adminCtrl.js
  "adminCtrl": {
    "fundingAccHome": {
      "templateData": {
        bannerBgClass: 'royal-blue-sodexo',
        breadcrumbs: null,
        headingText: 'Funding Accounts',
        subHeadingText: 'Create a funding account (similar to your Bank’s current account) to add funds and transfer vouchers to your employees.',
        subHeadingLearnMoreLink: null,
        disableOpenFundingAcc: true,
        addAccountHeading: 'With funding accounts your funds are instantly updated when you transfer money via IMPS/NEFT.'
      }
    },
    "funding-history": {
      "templateData": {
        bannerBgClass: "royal-blue-sodexo",
        breadcrumbs: [{href: "/admin/funding-accounts", text: "Funding Accounts"}],
        accountStmt: {
          enableDateRangeDownload: true
        },
        fundAdditions: {
          enableDateRangeDownload: true
        }
      },
      "pageName": "fundingAccounts"
    },
    "addFund": {
      "templateData": {
        bannerBgClass: 'royal-blue-sodexo',
        breadcrumbs: [{href: '/admin/funding-accounts', text: 'Funding Account'}],
        headingText: 'Raise a fund addition request',
        transferTypes: [{value: 'CHEQUE', text: 'Cheque', tranferNumberLabel: 'Reference Number', trasferDateLabel: 'Cheque Date'}],
        disableFooterNote: true,
        addFundFields: [
          {
              wrapperClass: 'pd-top-twenty',
              fieldLabel: 'Transaction Number',
              labelFor: 'chequeNumber',
              inputClass: 'width-xlg',
              inputId: 'transactionRefNumber',
              name: '',
              isCalendarInput: false,
          },
          {
              wrapperClass: 'date-picker-xlg',
              fieldLabel: 'Transfer Date',
              labelFor: 'transferDate',
              inputClass: 'bdr-radius datepicker-img',
              inputId: 'transactionDate',
              name: '',
              isCalendarInput: true,
          },
          {
              wrapperClass: '',
              fieldLabel: `Amount ( ₱ )`,
              labelFor: 'chequeAmount',
              inputClass: 'width-xlg',
              inputId: 'transactionAmount',
              name: '',
              isCalendarInput: false,
          }
      ],
      },
      "pageName": "fundingAccounts"
    }
  },
  // app/controller/dashboardCtrl.js
  "dashboardCtrl": {
    "init": {
      "activeProduct": "sm-corp/gift-pass"
    }
  }
}

module.exports = data;
const data = {
  // app/controller/adminCtrl.js
  "adminCtrl": {
    "fundingAccHome": {
      "templateData": {
        bannerBgClass: 'royal-blue-sodexo',
        breadcrumbs: null,
        headingText: 'Funding Accounts',
        subHeadingText: 'Funding accounts are similar to your bank’s current account. They can fund one or more programs across your companies. Use them to transfer funds to your beneficiaries.',
        subHeadingLearnMoreLink: null,
        addAccountHeading: 'With funding accounts your funds are instantly updated when you transfer money via IMPS/NEFT.'
      }
    },
    "funding-history": {
      "templateData": {
        bannerBgClass: "royal-blue-sodexo",
        breadcrumbs: [{href: "/admin/funding-accounts", text: "Funding Accounts"}]
      },
      "pageName": "fundingAccounts"
    },
    "addFund": {
      "templateData": {
        bannerBgClass: 'royal-blue-sodexo',
        breadcrumbs: [{href: '/admin/funding-accounts', text: 'Funding Account'}],
        headingText: 'Details of your bank transfer to Sodexo’s Bank Account',
        transferTypes: [{tranferNumberLabel: 'Transaction Number', trasferDateLabel: 'Transfer Date'}],
        disableFooterNote: true,
        addFundFields: [
          {
              wrapperClass: 'pd-top-twenty',
              fieldLabel: 'Transaction Number',
              labelFor: 'chequeNumber',
              inputClass: 'width-xlg',
              inputId: 'transactionRefNumber',
              name: '',
              isCalendarInput: false,
          },
          {
              wrapperClass: 'date-picker-xlg',
              fieldLabel: 'Transfer Date',
              labelFor: 'transferDate',
              inputClass: 'bdr-radius datepicker-img',
              inputId: 'transactionDate',
              name: '',
              isCalendarInput: true,
          },
          {
              wrapperClass: '',
              fieldLabel: `Amount ( ₫ )`,
              labelFor: 'chequeAmount',
              inputClass: 'width-xlg',
              inputId: 'transactionAmount',
              name: '',
              isCalendarInput: false,
          }
      ],
      },
      "pageName": "fundingAccounts"
    },
    "companyDetails": {
      "templateData": {
        showTaxStatus: false
      }
    },
    "admin-companies": {
      "templateData": {
        pageHeading: 'Create companies in Sodexo if your business has multiple legal entities and you wish to seperate their benefits, rewards and expenses programs accordingly.'
      }
    }
  },
  // app/controller/benefitCtrl.js
  "benefitCtrl": {
    "init": {
      "benefitHomePageData": {
        "templateData": {
          enableSpotlightPromotionModal: false,
          enableMopinion: false,
          showTaxStatus: false,
          documentQuoteText: "Manage disbursement and monitor performance of your programs."
        }
      }
    },
    "newTransfer": {
      TRANSFER_EXCEL_DOWNLOAD_FILE: {
        meal: 'download-samples/sodexo-vietnam/meal-sample',
        sodexo_cafeteria: 'download-samples/sodexo-vietnam/meal-sample'
      },
      TRANSFER_PREVIEW_IMAGES: {
        meal: 'images/previews/sodexo-vietnam/meal-preview.png',
        sodexo_cafeteria: 'images/previews/sodexo-vietnam/meal-preview.png'
      }
    },
    "getBenefitDetails": {
      "templateData": {
        "overview": {
          showCardProgramLogo: true,
          enableCardLogoInput: true,
          showTaxAndExpiry: false
        }
      }
    }
  },
  // app/controller/employeeStetmentCtrl.js
  "employeeStetmentCtrl": {
    "init": {
      downloadSodexoPDF: true
    }
  },
  // app/controller/homeCtrl.js
  "homeCtrl": {
    "corpDetails": {
      "templateData": {
        logoClassName: "sodexo-logo",
        showSodexoHelpSupport: {
          speakToUsText: "Call us on (+84) (28) 7305 0667, Mon-Friday 9-6pm only except public holidays"
        },
        showCinPanGst: false
      }
    }
  },
  // app/controller/employeeCtrl.js
  "employeeCtrl": {
    "employeeDetailsV2": {
      "templateData": {
        showKYCStatus: false,
        showOverviewTab: false
      }
    }
  }
}

module.exports = data;
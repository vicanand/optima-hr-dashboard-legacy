var Config = require('../../config');
var Constant = require('../common/constant');
var Util = require('../common/util');
var Endpoint = require('../common/url');
var ServiceConnector = require('../common/serviceConnector');
var DashboardController = require('../controller/dashboardCtrl');
var GateKeeperService = require('../service/gatekeeper.js');
var AdminService = require('../service/adminSvc.js');
var BulkActionSvc = require('../service/bulkActionSvc');
var moment = require('moment');
const { COUNTRY_CONFIGS } = require('./countryConfig/configMappings');
const env = process.env.NODE_ENV || 'development_stage';

let setSelectedCompanyID = function(req, res, next) {
    if (!req.query.companyID && req.query.companyID != "") {
        res.sendStatus(500);
    } else {
        let companies = req.session[Constant.SESSION_COMP_INFO];
        let selectedCompany = companies.find(function(item) {
            return item.company.companyID == req.query.companyID;
        });
        req.session[Constant.SESSION_SELECTED_COMP_INFO] = selectedCompany;
        let landingProductList = req.session[Constant.SESSION_CORP_INFO].allowedPrograms;
        let activeProduct='';
        if(landingProductList.indexOf("OPTIMA") > -1){   
            activeProduct = req.session.selectedProgram || 'optima';
        } else if(landingProductList.indexOf("SPOTLIGHT") > -1){
            activeProduct = "spotlight";
        } else if(landingProductList.indexOf("EXPRESS") > -1){
            activeProduct = "EXPRESS";
        } else if(landingProductList.allowedPrograms.indexOf("EXPENSE") > -1) {
            activeProduct = "EXPENSE";
        } else{
            activeProduct = req.session.selectedProgram || 'optima';
        }
        res.setHeader('Content-Type', 'application/json');
        res.status(200).json({
            "id": selectedCompany.company.companyID,
            "selectedCompany": selectedCompany,
            "product": activeProduct
        });
    }
    res.end();
};


// switch corporates create API for Selected corporates

let setSelectedCorporateID = function(req, res, next) {
    delete req.session[Constant.SESSION_ACC_INFO];
    delete req.session[Constant.SESSION_SELECTED_COMP_INFO];
    let selectedCorpID = req.query.corpID;
    if (!selectedCorpID && selectedCorpID != "") {
        res.sendStatus(500);
    } else {
        let corpList = req.session[Constant.SESSION_CORP_LIST];
        let currCorp = corpList.corporateCompanies.filter(function(corp) {
            return corp.corporate.id == selectedCorpID;
        });
        let corpDetails = {
            "id": selectedCorpID,
            "ifi": currCorp[0].corporate.defaultIFI,
            "name": currCorp[0].corporate.name,
            "accountHolderName": "",
            "allowedPrograms": currCorp[0].corporate.attributes.allowedProducts
        }
        let getLastLogin = Config.omsBaseUrl + Endpoint.GET_LAST_LOGIN.replace(':token:', req.session[Constant.SESSION_AUTH].authToken).replace(':corpID:', selectedCorpID);

        lastLoginRecord(req, res, next, getLastLogin);

        let getUserProfileURL = Config.omsBaseUrl + Endpoint.GET_USER_PROFILE.replace(':userJID:', req.session[Constant.SESSION_AUTH].userJID);
        let headers = {
            'X-Zeta-AuthToken': req.session[Constant.SESSION_AUTH].authToken
        }
        AdminService.getUserProfile(getUserProfileURL, '', headers).then(function(userProfile) {
            userProfile = JSON.parse(userProfile);
            let accountName = userProfile.name ? userProfile.name.firstName : '';
            corpDetails.accountHolderName = accountName;
            req.session[Constant.SESSION_USER_PROFILE] = userProfile;
            req.session[Constant.SESSION_CORP_INFO] = corpDetails;
            if (userProfile.name && userProfile.name.lastName) {
                accountName = accountName + " " + userProfile.name.lastName;
            }
            let accResp = {
                "account": {
                    "accountDetails": {
                        "id": userProfile.userID,
                        "name": accountName,
                        "email": userProfile.emailList[0].email
                    }
                }
            };
            req.session[Constant.SESSION_ACC_INFO] = accResp;
            let companyDetailsUrl = Config.omsBaseUrl + Endpoint.GET_COMPANY_DETAILS.replace(':corpID:', selectedCorpID)
                .replace(':authToken:', req.session[Constant.SESSION_AUTH].authToken)
            ServiceConnector.get(companyDetailsUrl, {}, undefined, req).then(function(companyResp) {
                companyResp = JSON.parse(companyResp);
                req.session[Constant.SESSION_COMP_INFO] = companyResp.companySummaries;
                req.session[Constant.SESSION_SELECTED_COMP_INFO] = companyResp.companySummaries[0];
                let companyIDs = companyResp.companySummaries.map(function(company) {
                    return company.company.companyID;
                });
                let authToken = req.session[Constant.SESSION_AUTH].authToken;
                let userId = req.session[Constant.SESSION_AUTH].userId;
                getGKFlagsForCorporate(req, authToken, userId, selectedCorpID, companyIDs).then(function(respData) {
                    DashboardController.goToHome(req, res, next);
                }).catch(function(error) {
                    res.sendStatus(500);
                });
            }).catch(function(error) {
                res.sendStatus(401);
                res.locals.showErrorWitHdr = true;
                Util.errorFoundHandler(error, req, res, next);
            });
        }).catch(function(error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });
    }
};

let getGKFlagsForCorporate = function(req, authToken, userId, corpID, companyIDs) {
    let promises = [];
    for (var i = 0; i < companyIDs.length; i++) {
        promises.push(GateKeeperService.getFlagsForCompany(authToken, userId, corpID, companyIDs[i]));
    }
    return Promise.all(promises).then(function(response) {
        req.session[Constant.SESSION_GK] = {
            companies: {}
        };
        for (var i = 0; i < response.length; i++) {
            req.session[Constant.SESSION_GK].companies[companyIDs[i]] = response[i];
        }
    }, function(err) {

    });
};

let lastLoginRecord = function(req, res, next, url) {
    ServiceConnector.get(url).then(function(respData) {
        let lastLoginRecord = JSON.parse(respData);
        let getDay = moment(lastLoginRecord.loginRecord.epochTime).utcOffset('+0530').format("DD");
        let getMonth = moment(lastLoginRecord.loginRecord.epochTime).utcOffset('+0530').format(" MMM YY hh:mm A");
        req.session[Constant.LAST_LOGIN_TIME] = getDay + "th" + getMonth;
    }, function(respErr) {
        req.session[Constant.LAST_LOGIN_TIME] = false;
    });
}


let setSelectedFundingAcc = function(req, res, next) {
    if (!req.query.accountID && req.query.accountID == "") {
        res.sendStatus(500);
    } else {
        let fundAcc = req.session[Constant.SESSION_ALL_FUNDING_ACCOUNTS].fundingAccounts;
        let selectedFundAcc;
        selectedFundAcc = fundAcc.find(function(fund) {
            return fund.accountID == req.query.accountID;
        });
        if (selectedFundAcc) {
            selectedFundAcc["ifiDetails"] = req.session[Constant.SESSION_IFIS_DETAILS];
            selectedFundAcc['name'] = req.query.name;
            selectedFundAcc['ifiId'] = parseInt(req.query.ifiId);
        } else {
            selectedFundAcc = {
                "ifiDetails": req.session[Constant.SESSION_IFIS_DETAILS],
                "name": req.query.name,
                "ifiId": parseInt(req.query.ifiId)
            }
        }
        res.setHeader('Content-Type', 'application/json');
        res.status(200).json(selectedFundAcc);
    }
    res.end();
};

let createProgram = function(req, res, next) {
    let productType = req.query.productType.toLowerCase();
    productType = Constant.PROGRAM_TRANSFER_ENUM[productType];
    let fundingAccID = req.query.fundingAccID == 'createNew' ? req.session[Constant.SESSION_NEW_FUNDING_ACC_ID] : req.query.fundingAccID;
    let inputObj = {
        "companyID": req.session[Constant.SESSION_SELECTED_COMP_INFO].company.companyID,
        "cardProgramDetails": {
            "programName": req.headers.programname,
            "financialYear": req.query.financialYear,
            "productType": productType,
            "currency": req.query.currency ? req.query.currency : "INR"
        },
        "fundingAccountID": fundingAccID,
        "corpID": res.locals.corp.id
    }
    let headers = {
        "Content-Type": "application/json",
    };

    let url='';
    const country = req.query.country;
    if(country == 'isSodexoVn'){
        url = COUNTRY_CONFIGS.SODEXO_VIETNAM_CONFIGS.VN_BASE_URL[env] + req.query.url + '?token=' + req.session[Constant.SESSION_AUTH].authToken;
    } else {
        url = (Config.omsBaseUrl + Endpoint['CREATE_PROGRAM']).
            replace(':authToken:', req.session[Constant.SESSION_AUTH].authToken);
    }
    ServiceConnector.post(url, inputObj, headers).then(function(respData) {
        req.session[Constant.SESSION_NEW_FUNDING_ID] = respData.id;
        req.session['responseData'] = respData;

        update(req, res, next);

    }, function(respErr) {
        res.setHeader('Content-Type', 'application/json');
        res.status(500).json(respErr);
        res.end();
    });
};


let updateCardFundingAccount = function(req, res, next) {
    let url = (Config.omsBaseUrl + Endpoint['ADD_FUND_ACCOUNT'].replace(':authToken:', req.session[Constant.SESSION_AUTH].authToken))
    let inputObj = {
        'corpID': res.locals.corp.id,
        'name': req.query.newFundAccount
    }
    let headers = {
        "Content-Type": "application/json",
    };
    ServiceConnector.post(url, inputObj, headers).then(function(respData) {
        let newFundingAccountID = respData.accountID;
        req.session[Constant.SESSION_NEW_FUNDING_ACC_ID] = newFundingAccountID;
        if(req.query.isNewAccount == 'false'){
              update(req, res, next);
        }else{
             createProgram(req, res, next);
        }
    }, function(respErr) {
    });
};

let update = function(req, res, next) {

    let url = (Config.omsBaseUrl + Endpoint['UPDATE_CARD_FUNDING_ACC'].replace(':authToken:', req.session[Constant.SESSION_AUTH].authToken))
    let fundingAccID = req.query.fundingAccID == 'createNew' ? req.session[Constant.SESSION_NEW_FUNDING_ACC_ID] : req.query.fundingAccID;
    let programID = req.query.isNewAccount == 'false' ? req.query.programID : req.session[Constant.SESSION_NEW_FUNDING_ID];
    let inputObj = {
        "corpID": res.locals.corp.id,
        "fundingAccountID": fundingAccID,
        "programID": programID
    };
    let headers = {
        "Content-Type": "application/json",
    };
    ServiceConnector.post(url, inputObj, headers).then(function(respData) {
        let respDataSend;
        if(req.query.isNewAccount == 'false'){
             respDataSend = req.query.isNewAccount == 'false'  ? req.session['responseData'] = { 'id':req.query.programID} : respData;
        } else if(req.query.isNewAccount == 'frombenefit'){
            respDataSend =  req.session['responseData'];
        }else{
            respDataSend = req.query.fundingAccID == 'createNew' ? req.session['responseData'] : respData;
        }
        req.session[Constant.SESSION_NEW_FUNDING_ID] = '';
        req.session['responseData'] = '';
        req.session[Constant.SESSION_NEW_FUNDING_ACC_ID] = '';
        req.query.isNewAccount = '';
        res.setHeader('Content-Type', 'application/json');
        res.status(200).json(respDataSend);
        res.end();
    }, function(respErr) {
        res.setHeader('Content-Type', 'application/json');
        res.status(500).json(respErr);
        res.end();
    });
};


let createCashIncentiveProgram = function(req, res, next) {
    let productType = req.query.productType.toLowerCase();
    productType = Constant.PROGRAM_TRANSFER_ENUM[productType];
    let inputObj = {
        "companyID": req.session[Constant.SESSION_SELECTED_COMP_INFO].company.companyID,
        "cardProgramDetails": {
            "programName": "Cash Incentive",
            "productType": productType,
            "currency": "INR"
        },
        "fundingAccountID": req.query.fundingAccID,
        "corpID": res.locals.corp.id
    }
    let headers = {
        "Content-Type": "application/json",
    };
    let url = (Config.omsBaseUrl + Endpoint['CREATE_PROGRAM']).
    replace(':authToken:', req.session[Constant.SESSION_AUTH].authToken);
    ServiceConnector.post(url, inputObj, headers).then(function(respData) {
        res.setHeader('Content-Type', 'application/json');
        res.status(200).json(respData);
        res.end();
    }, function(respErr) {
        res.setHeader('Content-Type', 'application/json');
        res.status(500).json(respErr);
        res.end();
    });
};

let createSelectGiftingProgram = function(req, res, next) {
    let headers = {
        "Content-Type": "application/json",
    };
    let fundingAccountID = req.query.fundingAccountId;
    let url = (Config.omsBaseUrl + Endpoint['CREATE_SELECT_GITFTING_ORDER']);
    let inputObj = {
        "corporateId": res.locals.corp.id,
        "companyId": req.session[Constant.SESSION_SELECTED_COMP_INFO].company.companyID,
        "fundingAccountId": fundingAccountID,
        "token": req.session[Constant.SESSION_AUTH].authToken,
        "spotlightType": "SELECT_GIFTING"
    };

    ServiceConnector.post(url, inputObj, headers).then(function(respData) {
        res.setHeader('Content-Type', 'application/json');
        res.status(200).json(respData);
        res.end();
    }, function(respErr) {
        res.setHeader('Content-Type', 'application/json');
        res.status(500).json(respErr);
        res.end();
    });
};

let createCashlessProgram = function(req, res, next) {
    let productType = req.query.productType.toLowerCase();
    productType = Constant.PROGRAM_TRANSFER_ENUM[productType];
    let inputObj = {
        "companyID": req.session[Constant.SESSION_SELECTED_COMP_INFO].company.companyID,
        "cardProgramDetails": {
            "programName": req.headers.programname,
            "productType": productType,
            "currency": req.query.currency
        },
        "corpID": res.locals.corp.id
    }
    let headers = {
        "Content-Type": "application/json",
    };
    let url = (Config.omsBaseUrl + Endpoint['CREATE_PROGRAM']).replace(':authToken:', req.session[Constant.SESSION_AUTH].authToken);
    ServiceConnector.post(url, inputObj, headers).then(function(respData) {
        res.setHeader('Content-Type', 'application/json');
        res.status(200).json(respData);
        res.end();
    }, function(respErr) {
        res.setHeader('Content-Type', 'application/json');
        res.status(500).json(respErr);
        res.end();
    });
};

let programsFunded = function(req, res, next) {
    if (!req.query.fundingAccID && req.query.fundingAccID != "") {
        res.sendStatus(500);
    } else {
        let fundingAccounts = req.session[Constant.SESSION_ALL_FUNDING_ACCOUNTS];
        let selectedFundingAccount = fundingAccounts.fundingAccounts.find(function(fundingAccount) {
            return fundingAccount.accountID == req.query.fundingAccID;
        });
        res.setHeader('Content-Type', 'application/json');
        res.status(200).json(selectedFundingAccount);
    }
    res.end();
};

let companyCodes = function(req, res, next) {
    let corpID = req.session.corp_info && req.session.corp_info.id;
    let corpName = req.session.corp_info && req.session.corp_info.name;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    BulkActionSvc.getCompanies(authToken, corpID).then(function(companies) {
        companies = JSON.parse(companies);
        let respData = {
            "corpName": corpName,
            "companies": companies
        }
        res.setHeader('Content-Type', 'application/json');
        res.status(200).json(respData);
        res.end();
    }, function(error) {
        res.sendStatus(500);
        res.end();
    });
};

let checkAuthTokenExist = function(req, res, next) {
    if (req.session[Constant.SESSION_AUTH]) {
        res.sendStatus(200);
    } else {
        res.sendStatus(500);
    }
    res.end();
};

let verifyNewCorporateDetails = function(req, res, next) {
    let url = Config.omsBaseUrl + Endpoint.UPDATE_CORPORATE_DETAILS.replace(':authToken:', req.session[Constant.SESSION_AUTH].authToken);
    let headers = {
        "Content-Type": "application/json"
    };
    ServiceConnector.post(url, req.body, headers).then(function(respData) {
        let corpList = req.session[Constant.SESSION_CORP_LIST];
        let currCorp = corpList.corporateCompanies.filter(function(corp) {
            return corp.corporate.id == req.body.corpID;
        });
        let selectedCorpID = currCorp[0].corporate.id;
        let selectedCorpIFI = currCorp[0].corporate.defaultIFI;
        let companyDetailsUrl = Config.omsBaseUrl + Endpoint.GET_COMPANY_DETAILS.replace(':corpID:', selectedCorpID)
            .replace(':authToken:', req.session[Constant.SESSION_AUTH].authToken)
        ServiceConnector.get(companyDetailsUrl).then(function(companyResp) {
            let corpInfo = req.session[Constant.SESSION_CORP_INFO];
            corpInfo.name = req.body.corporateName;
            corpInfo.ifi = selectedCorpIFI;
            req.session[Constant.SESSION_CORP_INFO] = corpInfo;
            companyResp = JSON.parse(companyResp);
            req.session[Constant.SESSION_COMP_INFO] = companyResp.companySummaries;
            req.session[Constant.SESSION_SELECTED_COMP_INFO] = companyResp.companySummaries[0];
            let companyIDs = companyResp.companySummaries.map(function(company) {
                return company.companyID;
            });
            let goToCorpDetails = false;
            if (companyResp.companySummaries[0].company.attributes && companyResp.companySummaries[0].company.attributes.reviewBillingAddress) {
                goToCorpDetails = true;
            }
            res.setHeader('Content-Type', 'application/json');
            res.status(200).json({
                "goToCorpDetails": goToCorpDetails
            });
            res.end();
        }).catch(function(error) {
            res.sendStatus(500);
        });
    }).catch(function(error) {
        if(error.body.message == "Cannot modify corporate details. Action allowed only for new corporates"){
            res.status(500).send(error.body.message);
        }
        else{
        res.sendStatus(500);
        }
    });
};

module.exports = {
    setSelectedFundingAcc: setSelectedFundingAcc,
    setSelectedCompanyID: setSelectedCompanyID,
    createProgram: createProgram,
    createCashlessProgram: createCashlessProgram,
    createCashIncentiveProgram: createCashIncentiveProgram,
    createSelectGiftingProgram: createSelectGiftingProgram,
    programsFunded: programsFunded,
    checkAuthTokenExist: checkAuthTokenExist,
    setSelectedCorporateID: setSelectedCorporateID,
    verifyNewCorporateDetails: verifyNewCorporateDetails,
    companyCodes: companyCodes,
    lastLoginRecord: lastLoginRecord,
    updateCardFundingAccount: updateCardFundingAccount,
    updateFundAcc: update
};
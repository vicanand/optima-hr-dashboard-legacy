formatINR = function (number) {
    var totalAmtSplitArr = Number(number).toString().split('.');
    var totalAmtINR = '';
    if (totalAmtSplitArr && totalAmtSplitArr.length) {
        totalAmtINR += totalAmtSplitArr[0].replace(/(\d)(?=(\d\d)+\d$)/g, '$1,');
        // Not considering the decimals i.e. paise
        /*if (totalAmtSplitArr.length === 2) {
            totalAmtINR += '.' + totalAmtSplitArr[1];
        }*/
    }
    return totalAmtINR;
};
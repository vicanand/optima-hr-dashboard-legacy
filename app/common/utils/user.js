let checkGKFlag = function(req, companyID, productStatus) {
    let gkCheck = req.session.gk;
    if (gkCheck && gkCheck.companies[companyID] && gkCheck.companies[companyID].GKsWithResults) {
        let compProductsInfoCheck = gkCheck.companies[companyID].GKsWithResults[productStatus];
        return compProductsInfoCheck;
    }
    return false;
};

module.exports = {
    checkGKFlag: checkGKFlag
};

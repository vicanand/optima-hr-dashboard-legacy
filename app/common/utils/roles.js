class RBAC {
    constructor(config) {
        // resources = null has full access, resources = {} has no access
        this.resources = {};
        this.currentRole = '';
        this.fundManagerReadAction = ['getFundingAccounts','getFundingAccountDetails','getFundingAccountDetailsWithCardProgram','getFundingAccountTransactions','getWhitelistedBankAccounts']

        this.programManagerReadAction = ['getBenefitProgramDetails','getAggregatePayouts','getPayouts','getPayoutStats','getAggregations','listOrders','getCorpProgramDetails']

        this.programManagerWriteAction = [
            'updateCardProgram',
            'updateCardProgramFundingAccount',
            'createCardProgram',
            'createCustomCardProgram',
            'createOrder',
            'addCommunicationNumber',
            'addDependent',
            'deleteDependent',
            'deleteCommunicationNumber'
        ]

        this.fundManagerWriteAction = [
            'addFundingAccount','moveFundsBetweenFAs'
        ]

        // Data
        // Supported Resources
        this.resourcesName = ['changeCorpAccountPassword', 'addAccountForEmployee', 'addFunds', 'changeCorpAccountRole', 'createOrder', 'listOrders', 'moveFunds', 'getFundingAccounts', 'getCorpAccounts', ...this.fundManagerReadAction, ...this.fundManagerWriteAction, ...this.programManagerReadAction, ...this.programManagerWriteAction];
        this.actions = ['edit', 'view', 'create'];
        this.initResourceBased(config);
    }

    initResourceBased(config) {
        this.resources = config.privileges.reduce((accumulator, item) => {
            if (item.action && item.action.indexOf('api.corpben') !== -1) {
                if (!accumulator) { accumulator = {}; };
                const foundResource = this.resourcesName.find((rItem) => item.action.indexOf(rItem) !== -1); 
                const foundAction = this.actions.find((rItem) => item.action.indexOf(rItem) !== -1);
                if (foundResource) {
                    // const allowedTo = item.objectJID.indexOf('/comp~'+config.companyID) !== -1 ? 'own' : 'any';
                    const allowedTo = item.objectJID.indexOf('/comp~') !== -1 ? 'own:'+item.objectJID.split('~')[1] : 'any';
                    accumulator[foundResource + (item.objectJID.indexOf('/comp~') !== -1 ? "_" + item.objectJID.split('~')[1] : '')] = foundAction ? foundAction+':'+allowedTo : 'create:'+allowedTo;
                }
            }
            return accumulator;
        }, null);
        if (this.resources && Object.keys(this.resources).length === 0) {
            const isFullAccess = config.privileges.some((item) => item.action == 'api.corpben.hr' || item.action == 'api.corpben.hr.*');
            this.resources = isFullAccess ? null : {};
        }

        if (config.corpAccountRole) {
            this.currentRole = config.corpAccountRole;
        }
        
        if (config.countryVar && config.countryVar === 'isSodexoPhpSm') {
            // FIXME - Add extra resources [SM] - Should be set from backend certstore
            this.addLocalResourcesSM(); // For PH SM
        }
    }

    addLocalResourcesSM() {
        if (this.currentRole && this.resources && Object.keys(this.resources).length) {
            if (this.currentRole === 'OPERATOR') {
                this.resources['ICshowOwnOnly'] = 'view:own'; // Issuance Conversion Portal - Show only own records
            } else if (this.currentRole === 'TREASURY' || this.currentRole === 'ACCOUNTANT') {
                this.resources['ICshowHistoryOnly'] = 'view:own'; // Issuance Conversion Portal - Show only history
            }
        }
    }
}

const craeteResourceConfigs = function(data) {
    if (data && data.companyID && data.privilegeData && data.privilegeData.privileges && data.privilegeData.privileges.length) {
        const parseData = {};
        parseData.privileges = data.privilegeData.privileges;
        // parseData.companyID = data.companyID;
        if (data.corpAccountData && data.corpAccountData.entries
            && data.corpAccountData.entries.length && data.corpAccountData.entries[0]
            && data.corpAccountData.entries[0].companyUsers && data.corpAccountData.entries[0].companyUsers.length) { // Get user's role
            parseData.corpAccountRole = data.corpAccountData.entries[0].companyUsers[0].attributes && data.corpAccountData.entries[0].companyUsers[0].attributes.role;
        }
        if (data.countryVar) { parseData.countryVar = data.countryVar; }
        const rbac = new RBAC(parseData);
        return rbac.resources;
    } else {
        return {}; // No Access
    }
}

module.exports = { craeteResourceConfigs };
var programStaticData = {
    "communication": {
        "companyID": "",
        "corpID": "",
        "productType": "communication",
        "fundingAccountID": "",
        "message": "",
        "greeting": "",
        "corpProductType": "OPTIMA",
        "cardProgram": {
            "businessName": "",
            "businessDetails": {
                "billingAddress": {
                    "line1": "",
                    "city": "",
                    "region": "",
                    "countryID": "",
                    "postCode": ""
                }
            },
            "cardName": "",
            "active": true,
            "productType": "Communication",
            "cardDesign": {
                "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                "bg": "https://card-program-files.s3.amazonaws.com/communication_new.jpeg",
                "bgColor": "#FA7800",
                "offerColor": "#FFFFFF",
                "textColor": "#FFFFFF",
                "cardName": ""
            },
            "cashBackProgram": {
                "maxCashBackProportion": 100,
                "headers": {}
            },
            "voucherProgram": {
                "voucherName": "",
                "voucherDesign": "http://x.y.z",
                "voucherValidity": "CURRENT_FINANCIAL_YEAR"
            },
            "reimbursementProgram": {
                "printedBillRequired": false,
                "lienSupported": true,
                "mandatorySpendThroughZeta": false,
                "maxValuePerBill": 10000000,
                "maxValuePerMonth": 10000000,
                "approval": "ZETA",
                "userDeclarationText": "By proceeding, you declare that this connection is in your name and  you have used this for official purposes. You authorize Zeta to approve an amount up to the claimed value based on the company policy.",
                "extraReimbursementProgramConfig": {
                    "allowRules": [{
                            "rule": "biller.allowBlurredBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowLastFinancialYearBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowBillDatePastUploadDate",
                            "value": false
                        },
                        {
                            "rule": "biller.allowTamperedBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowOtherLanguageBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowHandWrittenBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowAppScreenshots",
                            "value": false
                        },
                        {
                            "rule": "biller.allowSMSreceipts",
                            "value": false
                        },
                        {
                            "rule": "biller.allowEBills",
                            "value": true
                        },
                        {
                            "rule": "biller.allowLatePaymentFees",
                            "value": false
                        },
                        {
                            "rule": "biller.includeLatePaymentFees",
                            "value": false
                        }
                    ],
                    "requireRules": [{
                            "rule": "biller.requireBillNumber",
                            "value": true
                        },
                        {
                            "rule": "biller.requireBillDate",
                            "value": true
                        },
                        {
                            "rule": "biller.requireMerchantName",
                            "value": true
                        },
                        {
                            "rule": "biller.requireBillPeriod",
                            "value": true
                        },
                        {
                            "rule": "biller.requireConnectionInEmployeesName",
                            "value": true
                        }
                    ],
                    "uploadBillURL": "zeta://zapp/?zappID=multiBillUpload"
                }
            },
            "issuanceConfiguration": {
                "types": [
                    "VOUCHER"
                ],
                "issuer": "",
                "requiresExclusiveVirtualSuperCard": true,
                "expiryBehaviour": "moveToIssuer",
                "revokeBehaviour": "moveToIssuer",
                "closeCardBehaviour": "moveToIssuer"
            },
            "spendConfiguration": {
                "allowedTimeSlices": [],
                "spendDeclaration": "By proceeding, you declare that this connection is in your name and  you have used this for official purposes",
                "allowedMerchants": [
                    "business:130915"
                ],
                "channels": {
                    "allowSuperCard": true,
                    "allowReimbursement": true,
                    "allowZetaApp": true
                },
                "limits": {
                    "transactionLimit": 10000000,
                    "dailyLimit": 10000000,
                    "monthlyLimit": 10000000,
                    "dailyTxnCount": 10000000,
                    "monthlyTxnCount": 10000000
                },
                "transactionConstraints": {
                    "minTransactionValue": 0,
                    "maxTransactionValue": 10000000,
                    "allowedParticipation": {
                        "type": "PERCENT_OF_VALUE",
                        "value": 100,
                        "valueCap": 1000000
                    }
                }
            },
            "notificationConfiguration": {
                "templateGroupSuffix": "COMMUNICATION",
                "issueNotificationEnabled": true,
                "issueNotificationForRegisteredUser": true,
                "claimNotificationEnabled": true
            },
            "templateID": "iosCommunicationCardProgram_EN_INBOX",
            "extraCardProgramConfig": {
                "settlementConfig": {
                    "settler": "ZETA"
                }
            },
            "assetConfiguration": {},
            "currency": {
                "currencyCode": "INR"
            },
            "timezone": {
                "totalSeconds": 19800
            },
            "ifi": "",
            "ifiName": "",
            "businessID": "",
            "headers": {}
        },
        "closeCardConfiguration": {
            "cardCloseFrequency": "CURRENT_FINANCIAL_YEAR",
            "closeBehaviour": "",
            "closeCardDate": ""
        }
    },
    "driver_salary": {
        "companyID": "",
        "corpID": "",
        "productType": "driver_salary",
        "fundingAccountID": "",
        "message": "",
        "greeting": "",
        "corpProductType": "OPTIMA",
        "cardProgram": {
            "businessID": "",
            "businessName": "",
            "businessDetails": {
                "billingAddress": {
                    "line1": "",
                    "city": "",
                    "region": "",
                    "countryID": "",
                    "postCode": ""
                }
            },
            "cardName": "",
            "active": true,
            "productType": "DriverSalary",
            "cardDesign": {
                "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                "bg": "https://card-program-files.s3.amazonaws.com/driver_salary_card.jpg",
                "bgColor": "#617cff",
                "offerColor": "#FFFFFF",
                "textColor": "#FFFFFF",
                "cardName": ""
            },
            "cashBackProgram": {
                "maxCashBackProportion": 100,
                "headers": {}
            },
            "voucherProgram": {
                "voucherName": "",
                "voucherDesign": "http://x.y.z",
                "voucherValidity": "CURRENT_FINANCIAL_YEAR"
            },
            "reimbursementProgram": {
                "printedBillRequired": false,
                "lienSupported": true,
                "mandatorySpendThroughZeta": false,
                "maxValuePerBill": 10000000,
                "maxValuePerMonth": 10000000,
                "approval": "ZETA",
                "userDeclarationText": "You declare that the amount spent from your Optima Driver's Salary Card is spent by you and on the salary for your driver. You authorize Zeta to approve an amount up to the claimed value based on the company policy.",
                "extraReimbursementProgramConfig": {
                    "allowRules": [{
                            "rule": "biller.allowBlurredBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowLastFinancialYearBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowBillDatePastUploadDate",
                            "value": false
                        },
                        {
                            "rule": "biller.allowTamperedBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowOtherLanguageBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowHandWrittenBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowDriversFoodExpense",
                            "value": false
                        },
                        {
                            "rule": "biller.allowDriversMobileExpense",
                            "value": false
                        },
                        {
                            "rule": "biller.allowOvernightExpense",
                            "value": false
                        },
                        {
                            "rule": "biller.allowOtherCabBills",
                            "value": false
                        }
                    ],
                    "requireRules": [{
                            "rule": "biller.requireBillDate",
                            "value": true
                        },
                        {
                            "rule": "biller.requireBillNumber",
                            "value": true
                        },
                        {
                            "rule": "biller.requireMonth",
                            "value": true
                        },
                        {
                            "rule": "biller.requireRevenueStamp",
                            "value": true
                        },
                        {
                            "rule": "biller.requireDriverSignature",
                            "value": true
                        },
                        {
                            "rule": "biller.requireDLNumber",
                            "value": true
                        },
                        {
                            "rule": "biller.requireDLNumberMatch",
                            "value": true
                        },
                        {
                            "rule": "biller.requireEmployeeSignature",
                            "value": true
                        },
                        {
                            "rule": "biller.requireDriverDLCopy",
                            "value": true
                        },
                        {
                            "rule": "biller.requireCompanyFormat",
                            "value": true
                        }
                    ],
                    "uploadBillURL": "zeta://zapp/?zappID=multiBillUpload"
                }
            },
            "issuanceConfiguration": {
                "types": [
                    "VOUCHER"
                ],
                "issuer": "147494@business.zeta.in",
                "requiresExclusiveVirtualSuperCard": true,
                "expiryBehaviour": "moveToIssuer",
                "revokeBehaviour": "moveToIssuer",
                "closeCardBehaviour": "moveToIssuer"
            },
            "spendConfiguration": {
                "allowedTimeSlices": [],
                "spendDeclaration": "You declare that the amount spent from your Optima Driver's Salary Card is spent by you and on the salary for your driver.",
                "allowedMerchants": [
                    "mid:REIMBURSEMENT-ONLY"
                ],
                "channels": {
                    "allowSuperCard": false,
                    "allowSuperTag": true,
                    "allowReimbursement": true,
                    "allowZetaApp": true
                },
                "limits": {
                    "transactionLimit": 10000000,
                    "dailyLimit": 10000000,
                    "monthlyLimit": 10000000,
                    "dailyTxnCount": 10000000,
                    "monthlyTxnCount": 10000000
                },
                "transactionConstraints": {
                    "minTransactionValue": 0,
                    "maxTransactionValue": 10000000
                }
            },
            "notificationConfiguration": {
                "templateGroupSuffix": "DRIVERSALARY",
                "issueNotificationEnabled": true,
                "issueNotificationForRegisteredUser": true,
                "claimNotificationEnabled": true
            },
            "templateID": "iosDriverSalaryCardProgram_EN_INBOX",
            "androidTemplateID": "androidDriversSalaryCard_EN_INBOX",
            "extraCardProgramConfig": {
                "settlementConfig": {
                    "settler": "ZETA"
                }
            },
            "assetConfiguration": {},
            "currency": {
                "currencyCode": "INR"
            },
            "timezone": {
                "totalSeconds": 19800
            },
            "ifi": "",
            "ifiName": "",
            "headers": {}
        },
        "closeCardConfiguration": {
            "cardCloseFrequency": "CURRENT_FINANCIAL_YEAR",
            "closeBehaviour": "",
            "closeCardDate": ""
        }
    },
    "fuel": {
        "companyID": "",
        "corpID": "",
        "productType": "fuel",
        "fundingAccountID": "",
        "message": "",
        "greeting": "",
        "corpProductType": "OPTIMA",
        "cardProgram": {
            "businessName": "",
            "businessDetails": {
                "billingAddress": {
                    "line1": "",
                    "city": "",
                    "region": "",
                    "countryID": "",
                    "postCode": ""
                }
            },
            "cardName": "",
            "active": true,
            "productType": "Fuel",
            "cardDesign": {
                "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                "bg": "https://card-program-files.s3.amazonaws.com/fuel_new.jpeg",
                "bgColor": "#E62C2C",
                "offerColor": "#FFFFFF",
                "textColor": "#FFFFFF",
                "cardName": ""
            },
            "cashBackProgram": {
                "maxCashBackProportion": 100,
                "headers": {}
            },
            "voucherProgram": {
                "voucherName": "",
                "voucherDesign": "http://x.y.z",
                "voucherValidity": "CURRENT_FINANCIAL_YEAR"
            },
            "reimbursementProgram": {
                "printedBillRequired": false,
                "lienSupported": true,
                "mandatorySpendThroughZeta": false,
                "maxValuePerBill": 5000000,
                "maxValuePerMonth": 50000000,
                "approval": "ZETA",
                "userDeclarationText": "You declare that the amount spent from your Zeta Fuel Card is spent by you and on Retail Fuel, Oil and Lubricants only. You authorize Zeta to approve an amount up to the claimed value based on the company policy.",
                "extraReimbursementProgramConfig": {
                    "allowRules": [{
                            "rule": "biller.allowBlurredBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowLastFinancialYearBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowBillDatePastUploadDate",
                            "value": false
                        },
                        {
                            "rule": "biller.allowTamperedBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowFuelPumpBills",
                            "value": true
                        },
                        {
                            "rule": "biller.allowWithoutSTN",
                            "value": true
                        },
                        {
                            "rule": "biller.allowOtherLanguageBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowHandWrittenBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowUberBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowOlaBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowMeruBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowOtherCabBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowBusTickets",
                            "value": false
                        },
                        {
                            "rule": "biller.allowBusPasses",
                            "value": false
                        },
                        {
                            "rule": "biller.allowRailwayTickets",
                            "value": false
                        },
                        {
                            "rule": "biller.allowRailwayPasses",
                            "value": false
                        },
                        {
                            "rule": "biller.allowMetroReceipts",
                            "value": false
                        },
                        {
                            "rule": "biller.allowMetroPasses",
                            "value": false
                        },
                        {
                            "rule": "biller.allowMaintenanceRepairBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowAutoInsuranceReceipts",
                            "value": false
                        }
                    ],
                    "requireRules": [{
                            "rule": "biller.requireMerchantName",
                            "value": true
                        },
                        {
                            "rule": "biller.requireBillDate",
                            "value": true
                        },
                        {
                            "rule": "biller.requireBillNumber",
                            "value": true
                        }
                    ],
                    "uploadBillURL": "zeta://zapp/?zappID=multiBillUpload"
                }
            },
            "issuanceConfiguration": {
                "types": [
                    "VOUCHER"
                ],
                "issuer": "",
                "requiresExclusiveVirtualSuperCard": true,
                "expiryBehaviour": "moveToUserCashCard",
                "revokeBehaviour": "moveToIssuer",
                "closeCardBehaviour": "moveToUserCashCard",
                "escrowAllowed": true
            },
            "spendConfiguration": {
                "allowedTimeSlices": [],
                "spendDeclaration": "You declare that the amount spent from your Zeta Fuel Card is spent by you and on Retail Fuel, Oil and Lubricants only.",
                "allowedMerchants": [
                    "mcc:5172",
                    "mcc:5983",
                    "mcc:5542",
                    "mcc:5541",
                    "amc:5983"
                ],
                "channels": {
                    "allowSuperCard": true,
                    "allowSuperTag": true,
                    "allowReimbursement": false,
                    "allowZetaApp": true
                },
                "limits": {
                    "transactionLimit": 10000000,
                    "dailyLimit": 10000000,
                    "monthlyLimit": 10000000,
                    "dailyTxnCount": 10000000,
                    "monthlyTxnCount": 10000000
                },
                "transactionConstraints": {
                    "minTransactionValue": 0,
                    "maxTransactionValue": 10000000
                }
            },
            "notificationConfiguration": {
                "templateGroupSuffix": "FUEL",
                "issueNotificationEnabled": true,
                "issueNotificationForRegisteredUser": true,
                "claimNotificationEnabled": true
            },
            "extraCardProgramConfig": {
                "settlementConfig": {
                    "settler": "ZETA"
                }
            },
            "assetConfiguration": {},
            "currency": {
                "currencyCode": "INR"
            },
            "timezone": {
                "totalSeconds": 19800
            },
            "ifi": "",
            "ifiName": "",
            "businessID": "",
            "headers": {}
        },
        "closeCardConfiguration": {
            "cardCloseFrequency": "CURRENT_FINANCIAL_YEAR",
            "closeBehaviour": "",
            "closeCardDate": ""
        }
    },
    "asset": {
        "companyID": "",
        "corpID": "",
        "productType": "asset",
        "fundingAccountID": "",
        "message": "",
        "greeting": "",
        "corpProductType": "OPTIMA",
        "cardProgram": {
            "businessName": "",
            "businessDetails": {
                "billingAddress": {
                    "line1": "",
                    "city": "",
                    "region": "",
                    "countryID": "",
                    "postCode": ""
                }
            },
            "cardName": "",
            "active": true,
            "productType": "Asset",
            "cardDesign": {
                "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                "bg": "https://card-program-files.s3.amazonaws.com/gadget_new.jpeg",
                "bgColor": "#00C29B",
                "offerColor": "#FFFFFF",
                "textColor": "#FFFFFF",
                "cardName": ""
            },
            "cashBackProgram": {
                "maxCashBackProportion": 100,
                "headers": {}
            },
            "voucherProgram": {
                "voucherName": "",
                "voucherDesign": "http://x.y.z",
                "voucherValidity": "CURRENT_FINANCIAL_YEAR"
            },
            "reimbursementProgram": {
                "printedBillRequired": false,
                "lienSupported": true,
                "mandatorySpendThroughZeta": false,
                "maxValuePerBill": 10000000,
                "maxValuePerMonth": 10000000,
                "approval": "ZETA",
                "userDeclarationText": "You declare that the amount spent from your Zeta Asset Purchase Card is spent by you and on laptops, mobiles and tablets only. You authorize Zeta to approve an amount up to the claimed value based on the company policy.",
                "extraReimbursementProgramConfig": {}
            },
            "issuanceConfiguration": {
                "types": [
                    "VOUCHER"
                ],
                "issuer": "",
                "requiresExclusiveVirtualSuperCard": true,
                "expiryBehaviour": "moveToUserCashCard",
                "revokeBehaviour": "moveToIssuer",
                "closeCardBehaviour": "moveToUserCashCard"
            },
            "spendConfiguration": {
                "allowedTimeSlices": [],
                "spendDeclaration": "You declare that the amount spent from your Zeta Asset Purchase Card is spent by you and on laptops, mobiles and tablets only.",
                "allowedMerchants": [
                    "business:139639",
                    "business:131597",
                    "mid:99010226",
                    "mid:99010126",
                    "mid:99010194",
                    "mid:AMAZON SELLER",
                    "mid:AMAZON SELLER,",
                    "business:139639",
                    "business:131597",
                    "mid:Amazon Internet Services",
                    "mid:99010226",
                    "mid:99010126",
                    "mid:AMAZON SELLER",
                    "business:139639",
                    "business:131597",
                    "mid:Amazon Internet Services",
                    "mid:99010226",
                    "mid:99010126",
                    "mid:AMAZON SELLER",
                    "mid:AMAZON SELLER,"
                ],
                "channels": {
                    "allowSuperCard": false,
                    "allowSuperTag": true,
                    "allowReimbursement": true,
                    "allowZetaApp": true
                },
                "limits": {
                    "transactionLimit": 10000000,
                    "dailyLimit": 10000000,
                    "monthlyLimit": 10000000,
                    "dailyTxnCount": 10000000,
                    "monthlyTxnCount": 10000000
                },
                "transactionConstraints": {
                    "minTransactionValue": 0,
                    "maxTransactionValue": 10000000
                }
            },
            "notificationConfiguration": {
                "templateGroupSuffix": "ASSET",
                "issueNotificationEnabled": true,
                "issueNotificationForRegisteredUser": true,
                "claimNotificationEnabled": true
            },
            "templateID": "iosAssetCard_EN_INBOX",
            "androidTemplateID": "assetCardExpandableAndroid_EN_INBOX",
            "extraCardProgramConfig": {
                "settlementConfig": {
                    "settler": "ZETA"
                }
            },
            "assetConfiguration": {
                "providers": {
                    "qwikcilver": {
                        "type": "qwikcilver",
                        "creditAddress": "131917:1:1",
                        "endPoint": "https://cardreseller.zetaapps.in/generateCouponCode"
                    }
                }
            },
            "currency": {
                "currencyCode": "INR"
            },
            "timezone": {
                "totalSeconds": 19800
            },
            "ifi": "",
            "ifiName": "",
            "businessID": "",
            "headers": {}
        },
        "closeCardConfiguration": {
            "cardCloseFrequency": "CURRENT_FINANCIAL_YEAR",
            "closeBehaviour": "",
            "closeCardDate": ""
        }
    },
    "gift": {
        "companyID": "",
        "corpID": "",
        "productType": "gift",
        "fundingAccountID": "",
        "message": "",
        "greeting": "",
        "corpProductType": "OPTIMA",
        "cardProgram": {
            "businessName": "",
            "businessDetails": {
                "billingAddress": {
                    "firstName": "Zeta",
                    "lastName": "User",
                    "email": "qwikcilver@zeta.in",
                    "telephone": "02261966300",
                    "line1": "Zeta Office, Ground Floor, Tower C",
                    "line2": "Diamond District, Domlur",
                    "city": "Bangalore",
                    "region": "Karnataka",
                    "countryID": "IN",
                    "postCode": "560008"
                }
            },
            "cardName": "",
            "active": true,
            "productType": "Gift",
            "cardDesign": {
                "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                "bg": "https://card-program-files.s3.amazonaws.com/gift_Card_new.jpeg",
                "bgColor": "#e616c6",
                "offerColor": "#FFFFFF",
                "textColor": "#FFFFFF",
                "cardName": ""
            },
            "cashBackProgram": {
                "maxCashBackProportion": 100,
                "headers": {}
            },
            "voucherProgram": {
                "voucherName": "",
                "voucherDesign": "http://x.y.z",
                "voucherValidity": "THREE_YEARS"
            },
            "reimbursementProgram": {
                "printedBillRequired": false,
                "lienSupported": false,
                "mandatorySpendThroughZeta": false,
                "maxValuePerBill": 500000,
                "maxValuePerMonth": 500000,
                "approval": "ZETA",
                "userDeclarationText": "",
                "extraReimbursementProgramConfig": {}
            },
            "issuanceConfiguration": {
                "types": [
                    "VOUCHER"
                ],
                "issuer": "",
                "requiresExclusiveVirtualSuperCard": true,
                "expiryBehaviour": "moveToIssuer",
                "revokeBehaviour": "moveToIssuer",
                "closeCardBehaviour": "moveToIssuer",
                "isCardReloadable": false
            },
            "spendConfiguration": {
                "allowedTimeSlices": [],
                "allowedMerchants": [],
                "disallowedMerchants": [],
                "channels": {
                    "allowSuperCard": true,
                    "allowSuperTag": true,
                    "allowReimbursement": false,
                    "allowZetaApp": true
                },
                "limits": {
                    "transactionLimit": 5000000,
                    "dailyTxnCount": 10000000,
                    "monthlyTxnCount": 10000000,
                    "dailyLimit": 5000000,
                    "monthlyLimit": 5000000
                },
                "transactionConstraints": {
                    "minTransactionValue": 0,
                    "maxTransactionValue": 5000000
                }
            },
            "notificationConfiguration": {
                "templateGroupSuffix": "GIFT",
                "issueNotificationEnabled": true,
                "issueNotificationForRegisteredUser": true,
                "claimNotificationEnabled": true
            },
            "templateID": "iosGiftCardQCCardProgram_EN_INBOX",
            "androidTemplateID": "androidGiftCardV2_EN_INBOX",
            "extraCardProgramConfig": {
                "settlementConfig": {
                    "settler": "ZETA"
                }
            },
            "assetConfiguration": {
                "providers": {
                    "qwikcilver": {
                        "type": "qwikcilver",
                        "creditAddress": "140810:1:1",
                        "endPoint": "https://cardreseller.zetaapps.in/generateCouponCode"
                    }
                }
            },
            "currency": {
                "currencyCode": "INR"
            },
            "timezone": {
                "totalSeconds": 19800
            },
            "ifi": "",
            "ifiName": "",
            "businessID": "",
            "headers": {}
        },
        "closeCardConfiguration": {
            "cardCloseFrequency": "CURRENT_FINANCIAL_YEAR",
            "closeBehaviour": "",
            "closeCardDate": ""
        }
    },
    "meal": {
        "companyID": "",
        "corpID": "",
        "productType": "meal",
        "fundingAccountID": "",
        "message": "",
        "greeting": "",
        "corpProductType": "OPTIMA",
        "cardProgram": {
            "businessName": "",
            "businessDetails": {
                "billingAddress": {
                    "line1": "",
                    "city": "",
                    "region": "",
                    "countryID": "",
                    "postCode": ""
                }
            },
            "cardName": "",
            "active": true,
            "productType": "Meal",
            "cardDesign": {
                "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                "bg": "https://card-program-files.s3.amazonaws.com/meal_new.jpeg",
                "bgColor": "#FAA300",
                "offerColor": "#FFFFFF",
                "textColor": "#FFFFFF",
                "cardName": ""
            },
            "cashBackProgram": {
                "maxCashBackProportion": 100,
                "headers": {}
            },
            "voucherProgram": {
                "voucherName": "",
                "voucherDesign": "http://x.y.z",
                "voucherValidity": "THREE_YEARS"
            },
            "reimbursementProgram": {
                "printedBillRequired": false,
                "lienSupported": false,
                "mandatorySpendThroughZeta": false,
                "maxValuePerBill": 500000,
                "maxValuePerMonth": 500000,
                "approval": "ZETA",
                "userDeclarationText": "You declare that the value you are claiming is spent by you and for food and non-alcoholic beverages only. You authorize Zeta to approve an amount up to the claimed value based on the company policy.",
                "extraReimbursementProgramConfig": {
                    "allowRules": [{
                            "rule": "biller.allowBlurredBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowLastFinancialYearBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowBillDatePastUploadDate",
                            "value": false
                        },
                        {
                            "rule": "biller.allowTamperedBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowOtherLanguageBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowHandWrittenBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowGroceryBills",
                            "value": true
                        },
                        {
                            "rule": "biller.allowSupermarketBills",
                            "value": true
                        },
                        {
                            "rule": "biller.allowNonFoodBill",
                            "value": false
                        },
                        {
                            "rule": "biller.allowAlcoholicBeverages",
                            "value": false
                        },
                        {
                            "rule": "biller.allowMultipleBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowWithoutSTN",
                            "value": false
                        }
                    ],
                    "requireRules": [{
                            "rule": "biller.requireBillDate",
                            "value": true
                        },
                        {
                            "rule": "biller.requireBillNumber",
                            "value": true
                        },
                        {
                            "rule": "biller.requireMerchantName",
                            "value": true
                        }
                    ]
                }
            },
            "issuanceConfiguration": {
                "types": [
                    "VOUCHER"
                ],
                "issuer": "",
                "requiresExclusiveVirtualSuperCard": true,
                "expiryBehaviour": "moveToIssuer",
                "revokeBehaviour": "moveToIssuer",
                "closeCardBehaviour": "moveToIssuer"
            },
            "spendConfiguration": {
                "allowedTimeSlices": [],
                "spendDeclaration": "You declare that you are buying food and non-alcoholic beverages only",
                "allowedMerchants": [
                    "mcc:5411",
                    "mcc:5422",
                    "mcc:5441",
                    "mcc:5451",
                    "mcc:5462",
                    "mcc:5499",
                    "mcc:5811",
                    "mcc:5812",
                    "mcc:5814",
                    "amc:5812",
                    "amc:5499",
                    "mcc:5311"
                ],
                "disallowedMerchants": [
                    "mcc:5813",
                    "mcc:5921",
                    "amc:5813"
                ],
                "channels": {
                    "allowSuperCard": true,
                    "allowSuperTag": true,
                    "allowReimbursement": false,
                    "allowZetaApp": true
                },
                "limits": {
                    "transactionLimit": 1000000,
                    "dailyTxnCount": 10000000,
                    "monthlyTxnCount": 10000000,
                    "dailyLimit": 1000000,
                    "monthlyLimit": 2000000
                },
                "transactionConstraints": {
                    "minTransactionValue": 0,
                    "maxTransactionValue": 10000000
                }
            },
            "notificationConfiguration": {
                "issueNotificationEnabled": false,
                "issueNotificationForRegisteredUser": true,
                "claimNotificationEnabled": true
            },
            "extraCardProgramConfig": {
                "settlementConfig": {
                    "settler": "ZETA"
                }
            },
            "assetConfiguration": {},
            "currency": {
                "currencyCode": "INR"
            },
            "timezone": {
                "totalSeconds": 19800
            },
            "ifi": "",
            "ifiName": "",
            "businessID": "",
            "headers": {}
        },
        "closeCardConfiguration": {
            "cardCloseFrequency": "CURRENT_FINANCIAL_YEAR",
            "closeBehaviour": "",
            "closeCardDate": ""
        }
    },
    "medical": {
        "companyID": "",
        "corpID": "",
        "productType": "medical",
        "fundingAccountID": "",
        "message": "",
        "greeting": "",
        "corpProductType": "OPTIMA",
        "cardProgram": {
            "businessName": "",
            "businessDetails": {
                "billingAddress": {
                    "line1": "",
                    "city": "",
                    "region": "",
                    "countryID": "",
                    "postCode": ""
                }
            },
            "cardName": "",
            "active": true,
            "productType": "Medical",
            "cardDesign": {
                "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                "bg": "https://card-program-files.s3.amazonaws.com/medical_new.jpeg",
                "bgColor": "#1282E3",
                "offerColor": "#FFFFFF",
                "textColor": "#FFFFFF",
                "cardName": ""
            },
            "cashBackProgram": {
                "maxCashBackProportion": 100,
                "headers": {}
            },
            "voucherProgram": {
                "voucherName": "",
                "voucherDesign": "http://x.y.z",
                "voucherValidity": "CURRENT_FINANCIAL_YEAR"
            },
            "reimbursementProgram": {
                "printedBillRequired": false,
                "lienSupported": true,
                "mandatorySpendThroughZeta": false,
                "maxValuePerBill": 10000000,
                "maxValuePerMonth": 10000000,
                "approval": "ZETA",
                "userDeclarationText": "You declare that the amount entered only includes the part of your bill spent on Medical Expenses. You authorize Zeta to approve an amount up to the claimed value based on the company policy.",
                "extraReimbursementProgramConfig": {
                    "allowRules": [{
                            "rule": "biller.allowBlurredBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowLastFinancialYearBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowBillDatePastUploadDate",
                            "value": false
                        },
                        {
                            "rule": "biller.allowTamperedBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowNonMedicalItems",
                            "value": false
                        },
                        {
                            "rule": "biller.allowWithoutSTN",
                            "value": true
                        },
                        {
                            "rule": "biller.allowOtherLanguageBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowHandWrittenBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowDoctorConsultationBills",
                            "value": true
                        },
                        {
                            "rule": "biller.allowDentalSurgeryBills",
                            "value": true
                        },
                        {
                            "rule": "biller.allowDentalConsultationBills",
                            "value": true
                        },
                        {
                            "rule": "biller.allowOpticianBills",
                            "value": true
                        },
                        {
                            "rule": "biller.allowCosmeticTreatmentBills",
                            "value": true
                        },
                        {
                            "rule": "biller.allowFoodSupplementBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowOPDConsultationBills",
                            "value": true
                        },
                        {
                            "rule": "biller.allowMedicalEquipment",
                            "value": true
                        },
                        {
                            "rule": "biller.allowAyurvedaMedicines",
                            "value": true
                        },
                        {
                            "rule": "biller.allowHomeopathyMedicines",
                            "value": true
                        }
                    ],
                    "requireRules": [{
                            "rule": "biller.requireBillDate",
                            "value": true
                        },
                        {
                            "rule": "biller.requireBillNumber",
                            "value": true
                        },
                        {
                            "rule": "biller.requireMerchantName",
                            "value": true
                        },
                        {
                            "rule": "biller.requirePrescription",
                            "value": false
                        }
                    ],
                    "uploadBillURL": "zeta://zapp/?zappID=multiBillUpload"
                }
            },
            "issuanceConfiguration": {
                "types": [
                    "VOUCHER"
                ],
                "issuer": "",
                "requiresExclusiveVirtualSuperCard": true,
                "expiryBehaviour": "moveToUserCashCard",
                "revokeBehaviour": "moveToIssuer",
                "closeCardBehaviour": "moveToUserCashCard"
            },
            "spendConfiguration": {
                "allowedTimeSlices": [],
                "spendDeclaration": "You declare that the amount entered only includes the part of your bill spent on Medical Expenses.",
                "allowedMerchants": [
                    "mcc:5122",
                    "mcc:5912",
                    "mcc:8071",
                    "mcc:8099",
                    "mcc:8011",
                    "mcc:5047",
                    "mcc:8021",
                    "mcc:8031",
                    "mcc:8041",
                    "mcc:8042",
                    "mcc:8043",
                    "mcc:8044",
                    "mcc:8049",
                    "mcc:8050",
                    "mcc:8062",
                    "amc:5047"
                ],
                "channels": {
                    "allowSuperCard": true,
                    "allowReimbursement": true,
                    "allowZetaApp": true
                },
                "limits": {
                    "transactionLimit": 10000000,
                    "dailyTxnCount": 10000000,
                    "monthlyTxnCount": 10000000,
                    "dailyLimit": 50000000,
                    "monthlyLimit": 50000000
                },
                "transactionConstraints": {
                    "minTransactionValue": 0,
                    "maxTransactionValue": 10000000,
                    "allowedParticipation": {
                        "type": "PERCENT_OF_VALUE",
                        "value": 100,
                        "valueCap": 1000000
                    }
                }
            },
            "notificationConfiguration": {
                "templateGroupSuffix": "MEDICAL",
                "issueNotificationEnabled": true,
                "issueNotificationForRegisteredUser": true,
                "claimNotificationEnabled": true
            },
            "templateID": "iosMedicalCardProgram_EN_INBOX",
            "extraCardProgramConfig": {
                "settlementConfig": {
                    "settler": "ZETA"
                }
            },
            "assetConfiguration": {},
            "currency": {
                "currencyCode": "INR"
            },
            "timezone": {
                "totalSeconds": 19800
            },
            "ifi": "",
            "ifiName": "",
            "businessID": "",
            "headers": {}
        },
        "closeCardConfiguration": {
            "cardCloseFrequency": "CURRENT_FINANCIAL_YEAR",
            "closeBehaviour": "",
            "closeCardDate": ""
        }
    },
    "LTA": {
        "companyID": "",
        "corpID": "",
        "productType": "LTA",
        "fundingAccountID": "",
        "message": "",
        "greeting": "",
        "corpProductType": "OPTIMA",
        "cardProgram": {
            "businessName": "",
            "businessDetails": {
                "billingAddress": {
                    "line1": "",
                    "city": "",
                    "region": "",
                    "countryID": "",
                    "postCode": ""
                }
            },
            "cardName": "",
            "active": true,
            "productType": "LTA",
            "cardDesign": {
                "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                "bg": "https://card-program-files.s3.amazonaws.com/LTA_card_new.jpeg",
                "bgColor": "#ffcc00",
                "offerColor": "#FFFFFF",
                "textColor": "#FFFFFF",
                "cardName": ""
            },
            "cashBackProgram": {
                "maxCashBackProportion": 100,
                "headers": {}
            },
            "voucherProgram": {
                "voucherName": "",
                "voucherDesign": "http://x.y.z",
                "voucherValidity": "FOUR_YEARS"
            },
            "reimbursementProgram": {
                "printedBillRequired": false,
                "lienSupported": true,
                "mandatorySpendThroughZeta": false,
                "maxValuePerBill": 10000000,
                "maxValuePerMonth": 10000000,
                "approval": "ZETA",
                "userDeclarationText": "You declare that you are claiming for travel expenses spent on yourself and your family. You authorize Zeta to approve an amount up to the claimed value based on the company policy.",
                "extraReimbursementProgramConfig": {
                    "allowRules": [{
                            "rule": "biller.allowBlurredBills",
                            "value": false
                        },
                        {
                            "rule": "biller.requireBillDate",
                            "value": true
                        },
                        {
                            "rule": "biller.requireMerchantName",
                            "value": true
                        },
                        {
                            "rule": "biller.allowBillDatePastUploadDate",
                            "value": false
                        },
                        {
                            "rule": "biller.allowLastCalendarYearBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowBillAmtLessThanClaimedAmt",
                            "value": false
                        },
                        {
                            "rule": "biller.allowOtherCurrency",
                            "value": false
                        },
                        {
                            "rule": "biller.allowTamperedBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowHandwrittenBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowPrivateAirlines",
                            "value": true
                        },
                        {
                            "rule": "biller.allowMultiplePNR",
                            "value": true
                        },
                        {
                            "rule": "biller.allowLocalCabBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowCabBills",
                            "value": true
                        },
                        {
                            "rule": "biller.allowTravelAgentBill",
                            "value": true
                        },
                        {
                            "rule": "biller.allowApprovalEmail",
                            "value": true
                        },
                        {
                            "rule": "biller.allowHotelBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowFoodBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowInternationalTravel",
                            "value": false
                        },
                        {
                            "rule": "biller.allowMoreThanTwoChildren",
                            "value": true
                        }
                    ],
                    "requireRules": [{
                            "rule": "biller.requireLeaveApplication",
                            "value": true
                        },
                        {
                            "rule": "biller.requireBoardingPass",
                            "value": true
                        },
                        {
                            "rule": "biller.requireUserOnLeave",
                            "value": true
                        },
                        {
                            "rule": "biller.requireEmployeeAsTraveler",
                            "value": true
                        },
                        {
                            "rule": "biller.requireBillDate",
                            "value": true
                        },
                        {
                            "rule": "biller.requireMerchantName",
                            "value": false
                        },
                        {
                            "rule": "biller.requireMerchantName",
                            "value": false
                        }
                    ],
                    "preProcessingRules": [{
                        "cardFields": [],
                        // "condition": "(cardConfig['laimsAllowedToCarryForward'] == 'no limit'  || cardConfig['claimsAllowedPerLTABlock'] == 'no limit') || (billAggregate.approvedBillCountY0 < parseInt(cardConfig['claimsAllowedToCarryForward']) + parseInt(cardConfig['claimsAllowedPerLTABlock']) && billAggregate.approvedBillCountY1 < parseInt(cardConfig['claimsAllowedPerLTABlock']) - Math.max(billAggregate.approvedBillCountY0 - parseInt(cardConfig['claimsAllowedToCarryForward']), 0) && billAggregate.approvedBillCountY2 < parseInt(cardConfig['claimsAllowedPerLTABlock']) - billAggregate.approvedBillCountY1 - Math.max(billAggregate.approvedBillCountY0  - parseInt(cardConfig['claimsAllowedToCarryForward']), 0) && billAggregate.approvedBillCountY3 < parseInt(cardConfig['claimsAllowedPerLTABlock']) - billAggregate.approvedBillCountY1 - billAggregate.approvedBillCountY2 - Math.max(billAggregate.approvedBillCountY0 - parseInt(cardConfig['claimsAllowedToCarryForward']), 0))",
                        "passThrough": false,
                        "errorMessage": "You have exhausted the limit for number of LTA claims allowed by your corporate for the current LTA block (2018-2021)"
                    }]
                }
            },
            "issuanceConfiguration": {
                "types": [
                    "VOUCHER"
                ],
                "issuer": "",
                "requiresExclusiveVirtualSuperCard": true,
                "expiryBehaviour": "moveToIssuer",
                "revokeBehaviour": "moveToIssuer",
                "closeCardBehaviour": "moveToIssuer"
            },
            "spendConfiguration": {
                "allowedTimeSlices": [],
                "spendDeclaration": "You declare that you are spending for travel expenses for yourself and your family.",
                "allowedMerchants": [
                    "mcc:4722",
                    "mid:SOTC53615",
                    "mid:470000000002448",
                    "mid:470000000001811",
                    "mid:470000000001811",
                    "mid:GOIBIBO60179",
                    "mid:YATRADCC2016",
                    "mid:YATRADCC2016",
                    "mid:MAKEMYTRIP",
                    "mid:99010027",
                    "mid:470000097015044"
                ],
                "channels": {
                    "allowSuperCard": true,
                    "allowReimbursement": true,
                    "allowZetaApp": true
                },
                "limits": {
                    "transactionLimit": 10000000,
                    "dailyTxnCount": 10000000,
                    "monthlyTxnCount": 10000000,
                    "dailyLimit": 50000000,
                    "monthlyLimit": 50000000
                },
                "transactionConstraints": {
                    "minTransactionValue": 0,
                    "maxTransactionValue": 10000000,
                    "allowedParticipation": {
                        "type": "PERCENT_OF_VALUE",
                        "value": 100,
                        "valueCap": 1000000
                    }
                }
            },
            "notificationConfiguration": {
                "templateGroupSuffix": "LTA",
                "issueNotificationEnabled": true,
                "issueNotificationForRegisteredUser": true,
                "claimNotificationEnabled": true
            },
            "templateID": "iosIDFCReimbursementCardProgram_EN_INBOX",
            "androidTemplateID": "androidIdfcCard_EN_INBOX",
            "extraCardProgramConfig": {
                "settlementConfig": {
                    "settler": "ZETA"
                }
            },
            "assetConfiguration": {},
            "currency": {
                "currencyCode": "INR"
            },
            "timezone": {
                "totalSeconds": 19800
            },
            "ifi": "",
            "ifiName": "",
            "businessID": "",
            "headers": {}
        },
        "closeCardConfiguration": {
            "cardCloseFrequency": "CURRENT_FINANCIAL_YEAR",
            "closeBehaviour": "",
            "closeCardDate": ""
        }
    },
    "uniform": {
        "companyID": "",
        "corpID": "",
        "productType": "uniform",
        "fundingAccountID": "",
        "message": "",
        "greeting": "",
        "corpProductType": "OPTIMA",
        "cardProgram": {
            "businessName": "",
            "businessDetails": {
                "billingAddress": {
                    "line1": "",
                    "city": "",
                    "region": "",
                    "countryID": "",
                    "postCode": ""
                }
            },
            "cardName": "",
            "active": true,
            "productType": "Uniform",
            "cardDesign": {
                "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                "bg": "https://corp-ben-hr-files.s3.amazonaws.com/app_uniformCard.png",
                "bgColor": "#FAA300",
                "offerColor": "#FFFFFF",
                "textColor": "#FFFFFF",
                "cardName": ""
            },
            "cashBackProgram": {
                "maxCashBackProportion": 100,
                "headers": {}
            },
            "voucherProgram": {
                "voucherName": "",
                "voucherDesign": "http://x.y.z",
                "voucherValidity": "CURRENT_FINANCIAL_YEAR"
            },
            "reimbursementProgram": {
                "printedBillRequired": false,
                "lienSupported": true,
                "mandatorySpendThroughZeta": false,
                "maxValuePerBill": 50000000,
                "maxValuePerMonth": 50000000,
                "approval": "ZETA",
                "userDeclarationText": "You declare that the value you are claiming for is spent by you on uniform/clothing expenses purchased for official purposes only.You authorize Zeta to approve an amount up to the claimed value based on the company policy.",
                "extraReimbursementProgramConfig": {
                    "allowRules": [{
                            "rule": "biller.allowBlurredBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowLastFinancialYearBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowTamperedBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowHandwrittenBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowStitchingCharges",
                            "value": false
                        }
                    ],
                    "requireRules": [{
                            "rule": "biller.requireMerchantName",
                            "value": true
                        },
                        {
                            "rule": "biller.requireBillDate",
                            "value": true
                        },
                        {
                            "rule": "biller.requireBillNumber",
                            "value": true
                        },
                        {
                            "rule": "biller.requireEmployeeName",
                            "value": true
                        }
                    ],
                    "uniformCategories": [
                        "Formal Shirts",
                        "Trousers",
                        "T-Shirts",
                        "Collar/Polo T-Shirts",
                        "Jeans",
                        "Formal Shoes",
                        "Casual Shoes/Sports Shoes",
                        "Blazers",
                        "Formal Business Suit",
                        "Kurtas",
                        "Tie",
                        "Scarves",
                        "Salwar Kameez",
                        "Sarees",
                        "Dupatta",
                        "Skirts",
                        "Waistcoat",
                        "Socks",
                        "Sweater"
                    ]
                }
            },
            "issuanceConfiguration": {
                "types": [
                    "VOUCHER"
                ],
                "issuer": "",
                "requiresExclusiveVirtualSuperCard": true,
                "expiryBehaviour": "moveToIssuer",
                "revokeBehaviour": "moveToIssuer",
                "closeCardBehaviour": "moveToIssuer",
                "escrowAllowed": true
            },
            "spendConfiguration": {
                "allowedTimeSlices": [],
                "spendDeclaration": "You declare that you are buying clothing for official purpose only",
                "allowedMerchants": [
                    "mid:reimbursement-only"
                ],
                "channels": {
                    "allowSuperCard": true,
                    "allowSuperTag": true,
                    "allowReimbursement": true,
                    "allowZetaApp": true
                },
                "limits": {
                    "transactionLimit": 10000000,
                    "dailyLimit": 10000000,
                    "monthlyLimit": 10000000,
                    "dailyTxnCount": 10000000,
                    "monthlyTxnCount": 10000000
                },
                "transactionConstraints": {
                    "minTransactionValue": 0,
                    "maxTransactionValue": 10000000
                }
            },
            "notificationConfiguration": {
                "templateGroupSuffix": "UNIFORM",
                "issueNotificationEnabled": true,
                "issueNotificationForRegisteredUser": true,
                "claimNotificationEnabled": true
            },
            "templateID": "iosRblIfiAllowanceCard_EN_INBOX",
            "androidTemplateID": "androidRblIfiAllowanceCard_EN_INBOX",
            "extraCardProgramConfig": {
                "settlementConfig": {
                    "settler": "ZETA"
                }
            },
            "assetConfiguration": {},
            "currency": {
                "currencyCode": "INR"
            },
            "timezone": {
                "totalSeconds": 19800
            },
            "ifi": "",
            "ifiName": "",
            "businessID": "",
            "headers": {}
        },
        "closeCardConfiguration": {
            "cardCloseFrequency": "CURRENT_FINANCIAL_YEAR",
            "closeBehaviour": "",
            "closeCardDate": ""
        }
    },
    "car_maintenance_and_driver_salary": {
        "companyID": "",
        "corpID": "",
        "productType": "car_maintenance_and_driver_salary",
        "fundingAccountID": "",
        "message": "",
        "greeting": "",
        "corpProductType": "OPTIMA",
        "cardProgram": {
            "businessName": "",
            "businessDetails": {
                "billingAddress": {
                    "line1": "",
                    "city": "",
                    "region": "",
                    "countryID": "",
                    "postCode": ""
                }
            },
            "cardName": "",
            "active": true,
            "productType": "CarMaintenanceAndDriverSalary",
            "cardDesign": {
                "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                "bg": "https://card-program-files.s3.amazonaws.com/carmaintenance.png",
                "bgColor": "#617cff",
                "offerColor": "#FFFFFF",
                "textColor": "#151515",
                "cardName": "",
                "showReimbursementDetails": true
            },
            "cashBackProgram": {
                "maxCashBackProportion": 100,
                "headers": {}
            },
            "voucherProgram": {
                "voucherName": "",
                "voucherDesign": "http://x.y.z",
                "voucherValidity": "CURRENT_FINANCIAL_YEAR"
            },
            "reimbursementProgram": {
                "printedBillRequired": false,
                "lienSupported": true,
                "mandatorySpendThroughZeta": false,
                "maxValuePerBill": 10000000,
                "maxValuePerMonth": 10000000,
                "approval": "ZETA",
                "userDeclarationText": "You declare that the amount spent from your Optima Car Maintenance & Driver Salary Card is spent by you and on spends for car maintenance, toll, parking fee and salary for your driver. You authorize Zeta to approve an amount up to the claimed value based on the company policy.",
                "extraReimbursementProgramConfig": {
                    "allowRules": [{
                            "rule": "biller.allowOtherCabBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowAdvanceDriverSalary",
                            "value": false
                        },
                        {
                            "rule": "biller.allowAutoInsuranceReceipts",
                            "value": false
                        },
                        {
                            "rule": "biller.allowBillDatePastUploadDate",
                            "value": false
                        },
                        {
                            "rule": "biller.allowBlurredBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowDriversFoodExpense",
                            "value": true
                        },
                        {
                            "rule": "biller.allowDriversMobileExpense",
                            "value": true
                        },
                        {
                            "rule": "biller.allowHandWrittenBills",
                            "value": true
                        },
                        {
                            "rule": "biller.allowLastFinancialYearBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowOtherLanguageBills",
                            "value": true
                        },
                        {
                            "rule": "biller.allowOvernightExpense",
                            "value": true
                        },
                        {
                            "rule": "biller.allowPaperTollReceipt",
                            "value": true
                        },
                        {
                            "rule": "biller.allowPurchaseOfAccessories",
                            "value": false
                        },
                        {
                            "rule": "biller.allowPurchaseOfTyres",
                            "value": true
                        },
                        {
                            "rule": "biller.allowTamperedBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowTollReceiptOutsideIndia",
                            "value": false
                        }
                    ],
                    "requireRules": [{
                            "rule": "biller.requireBillDate",
                            "value": true
                        },
                        {
                            "rule": "biller.requireBillNumber",
                            "value": true
                        },
                        {
                            "rule": "biller.requireCompanyFormat",
                            "value": false
                        },
                        {
                            "rule": "biller.requireDLNumber",
                            "value": false
                        },
                        {
                            "rule": "biller.requireDLNumberMatch",
                            "value": false
                        },
                        {
                            "rule": "biller.requireDriverDLCopy",
                            "value": false
                        },
                        {
                            "rule": "biller.requireDriverSignature",
                            "value": true
                        },
                        {
                            "rule": "biller.requireEmployeeSignature",
                            "value": false
                        },
                        {
                            "rule": "biller.requireMerchantName",
                            "value": true
                        },
                        {
                            "rule": "biller.requireRevenueStamp",
                            "value": false
                        }
                    ],
                    "categories": [
                        "Car maintenance",
                        "Driver Salary",
                        "Toll",
                        "Parking Fee"
                    ],
                    "uploadBillURL": "zeta://zapp/?zappID=multiBillUpload"
                }
            },
            "issuanceConfiguration": {
                "types": [
                    "VOUCHER"
                ],
                "issuer": "",
                "requiresExclusiveVirtualSuperCard": true,
                "expiryBehaviour": "moveToIssuer",
                "revokeBehaviour": "moveToIssuer",
                "closeCardBehaviour": "moveToIssuer"
            },
            "spendConfiguration": {
                "allowedTimeSlices": [],
                "spendDeclaration": "You declare that the amount spent from your Optima Car Maintenance & Driver Salary Card is spent by you and on spends for car maintenance, toll, parking fee and salary for your driver.",
                "allowedMerchants": [
                    "mid:REIMBURSEMENT-ONLY"
                ],
                "channels": {
                    "allowSuperCard": false,
                    "allowReimbursement": true,
                    "allowZetaApp": true
                },
                "limits": {
                    "transactionLimit": 10000000,
                    "dailyLimit": 10000000,
                    "monthlyLimit": 10000000,
                    "dailyTxnCount": 10000000,
                    "monthlyTxnCount": 10000000
                },
                "transactionConstraints": {
                    "minTransactionValue": 0,
                    "maxTransactionValue": 10000000,
                    "allowedParticipation": {
                        "type": "PERCENT_OF_VALUE",
                        "value": 100,
                        "valueCap": 1000000
                    }
                }
            },
            "notificationConfiguration": {
                "templateGroupSuffix": "CARMAINTENANCEANDDRIVERSALARY",
                "issueNotificationEnabled": true,
                "issueNotificationForRegisteredUser": true,
                "claimNotificationEnabled": true
            },
            "templateID": "iosIDFCReimbursementCardProgram_EN_INBOX",
            "androidTemplateID": "androidIdfcCard_EN_INBOX",
            "extraCardProgramConfig": {
                "settlementConfig": {
                    "settler": "ZETA"
                }
            },
            "assetConfiguration": {},
            "currency": {
                "currencyCode": "INR"
            },
            "timezone": {
                "totalSeconds": 19800
            },
            "ifi": "",
            "ifiName": "",
            "businessID": "",
            "headers": {}
        },
        "closeCardConfiguration": {
            "cardCloseFrequency": "CURRENT_FINANCIAL_YEAR",
            "closeBehaviour": "",
            "closeCardDate": ""
        }
    },
    "self_education": {
        "companyID": "",
        "corpID": "",
        "productType": "self_education",
        "fundingAccountID": "",
        "message": "",
        "greeting": "",
        "corpProductType": "OPTIMA",
        "cardProgram": {
            "businessName": "",
            "businessDetails": {
                "billingAddress": {
                    "line1": "",
                    "city": "",
                    "region": "",
                    "countryID": "",
                    "postCode": ""
                }
            },
            "cardName": "",
            "active": true,
            "productType": "SelfEducation",
            "cardDesign": {
                "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                "bg": "https://card-program-files.s3.amazonaws.com/selfeducation_card.png",
                "bgColor": "#FB3F7E",
                "offerColor": "#FFFFFF",
                "textColor": "#FFFFFF",
                "cardName": ""
            },
            "cashBackProgram": {
                "maxCashBackProportion": 100,
                "headers": {}
            },
            "voucherProgram": {
                "voucherName": "",
                "voucherDesign": "http://x.y.z",
                "voucherValidity": "THREE_YEARS"
            },
            "reimbursementProgram": {
                "printedBillRequired": false,
                "lienSupported": true,
                "mandatorySpendThroughZeta": false,
                "maxValuePerBill": 5000000,
                "maxValuePerMonth": 50000000,
                "approval": "ZETA",
                "userDeclarationText": "You declare that the value you are claiming is spent by you to enroll for courses as per your company policies only",
                "extraReimbursementProgramConfig": {
                    "allowRules": [{
                            "rule": "biller.allowBlurredBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowBillDatePastUploadDate",
                            "value": true
                        },
                        {
                            "rule": "biller.allowLastFinancialYearBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowBillDatePastUploadDate",
                            "value": false
                        },
                        {
                            "rule": "biller.allowOtherLanguageBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowTamperedBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowHandwrittenBills",
                            "value": false
                        }
                    ],
                    "requireRules": [{
                            "rule": "biller.requireBillDate",
                            "value": true
                        },
                        {
                            "rule": "biller.requireBillNumber",
                            "value": true
                        },
                        {
                            "rule": "biller.requireMerchantName",
                            "value": true
                        },
                        {
                            "rule": "biller.requireCourseInEmployeeName",
                            "value": true
                        },
                        {
                            "rule": "biller.requireCourseCompleted",
                            "value": true
                        },
                        {
                            "rule": "biller.requireCourseCertificateInEmployeeName",
                            "value": true
                        },
                        {
                            "rule": "biller.requireSameNameOnReceiptAndCertificate",
                            "value": true
                        }
                    ],
                    "uploadBillURL": "zeta://zapp/?zappID=multiBillUpload"
                }
            },
            "issuanceConfiguration": {
                "types": [
                    "VOUCHER"
                ],
                "issuer": "",
                "requiresExclusiveVirtualSuperCard": true,
                "expiryBehaviour": "moveToUserCashCard",
                "revokeBehaviour": "moveToIssuer",
                "closeCardBehaviour": "moveToUserCashCard",
                "escrowAllowed": true
            },
            "spendConfiguration": {
                "allowedTimeSlices": [],
                "spendDeclaration": "You declare that you are enrolling for courses which adhere to your company policies.",
                "channels": {
                    "allowSuperCard": false,
                    "allowSuperTag": true,
                    "allowReimbursement": true,
                    "allowZetaApp": true
                },
                "limits": {
                    "transactionLimit": 10000000,
                    "dailyLimit": 10000000,
                    "monthlyLimit": 10000000,
                    "dailyTxnCount": 10000000,
                    "monthlyTxnCount": 10000000
                },
                "transactionConstraints": {
                    "minTransactionValue": 0,
                    "maxTransactionValue": 10000000
                }
            },
            "notificationConfiguration": {
                "templateGroupSuffix": "SELFEDUCATION",
                "issueNotificationEnabled": true,
                "issueNotificationForRegisteredUser": true,
                "claimNotificationEnabled": true
            },
            "templateID": "iosSelfEducationCardProgram_EN_INBOX",
            "androidTemplateID": "androidSelfEducationCard_EN_INBOX",
            "extraCardProgramConfig": {
                "settlementConfig": {
                    "settler": "ZETA"
                }
            },
            "assetConfiguration": {},
            "currency": {
                "currencyCode": "INR"
            },
            "timezone": {
                "totalSeconds": 19800
            },
            "ifi": "",
            "ifiName": "",
            "businessID": "",
            "headers": {}
        },
        "closeCardConfiguration": {
            "cardCloseFrequency": "CURRENT_FINANCIAL_YEAR",
            "closeBehaviour": "",
            "closeCardDate": ""
        }
    },
    "shift": {
        "companyID": "",
        "corpID": "",
        "productType": "shift",
        "fundingAccountID": "",
        "message": "",
        "greeting": "",
        "corpProductType": "OPTIMA",
        "cardProgram": {
            "businessName": "",
            "businessDetails": {
                "billingAddress": {
                    "line1": "",
                    "city": "",
                    "region": "",
                    "countryID": "",
                    "postCode": ""
                }
            },
            "cardName": "",
            "active": true,
            "productType": "Shift",
            "cardDesign": {
                "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                "bg": "https://card-program-files.s3.amazonaws.com/shiftallowance.png",
                "offerColor": "#FFFFFF",
                "textColor": "#FFFFFF",
                "cardName": ""
            },
            "cashBackProgram": {
                "maxCashBackProportion": 100,
                "headers": {}
            },
            "voucherProgram": {
                "voucherName": "",
                "voucherDesign": "http://x.y.z",
                "voucherValidity": "THREE_YEARS"
            },
            "reimbursementProgram": {
                "printedBillRequired": false,
                "lienSupported": false,
                "mandatorySpendThroughZeta": false,
                "maxValuePerBill": 500000,
                "maxValuePerMonth": 10000000,
                "approval": "ZETA",
                "userDeclarationText": "You declare that the value you are claiming is spent by you and for food and non-alcoholic beverages only. You authorize Zeta to approve an amount up to the claimed value based on the company policy.",
                "extraReimbursementProgramConfig": {
                    "allowRules": [{
                            "rule": "biller.allowBlurredBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowLastFinancialYearBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowBillDatePastUploadDate",
                            "value": false
                        },
                        {
                            "rule": "biller.allowTamperedBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowOtherLanguageBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowHandWrittenBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowGroceryBills",
                            "value": true
                        },
                        {
                            "rule": "biller.allowSupermarketBills",
                            "value": true
                        },
                        {
                            "rule": "biller.allowNonFoodBill",
                            "value": false
                        },
                        {
                            "rule": "biller.allowAlcoholicBeverages",
                            "value": false
                        },
                        {
                            "rule": "biller.allowMultipleBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowWithoutSTN",
                            "value": false
                        }
                    ],
                    "requireRules": [{
                            "rule": "biller.requireBillDate",
                            "value": true
                        },
                        {
                            "rule": "biller.requireBillNumber",
                            "value": true
                        },
                        {
                            "rule": "biller.requireMerchantName",
                            "value": true
                        }
                    ]
                }
            },
            "issuanceConfiguration": {
                "types": [
                    "VOUCHER"
                ],
                "issuer": "",
                "requiresExclusiveVirtualSuperCard": true,
                "expiryBehaviour": "moveToIssuer",
                "revokeBehaviour": "moveToIssuer",
                "closeCardBehaviour": "moveToIssuer",
                "escrowAllowed": true
            },
            "spendConfiguration": {
                "allowedTimeSlices": [],
                "spendDeclaration": "You declare that you are buying food and non-alcoholic beverages only",
                "allowedMerchants": [
                    "mcc:5411",
                    "mcc:5422",
                    "mcc:5441",
                    "mcc:5451",
                    "mcc:5462",
                    "mcc:5499",
                    "mcc:5811",
                    "mcc:5812",
                    "mcc:5814",
                    "amc:5812",
                    "amc:5499",
                    "mcc:5311"
                ],
                "disallowedMerchants": [
                    "mcc:5813",
                    "mcc:5921",
                    "amc:5813"
                ],
                "channels": {
                    "allowSuperCard": true,
                    "allowSuperTag": true,
                    "allowReimbursement": false,
                    "allowZetaApp": true
                },
                "limits": {
                    "transactionLimit": 10000000,
                    "dailyLimit": 10000000,
                    "monthlyLimit": 10000000,
                    "dailyTxnCount": 10000000,
                    "monthlyTxnCount": 10000000
                },
                "transactionConstraints": {
                    "minTransactionValue": 0,
                    "maxTransactionValue": 10000000
                }
            },
            "notificationConfiguration": {
                "templateGroupSuffix": "SHIFT",
                "issueNotificationEnabled": true,
                "issueNotificationForRegisteredUser": true,
                "claimNotificationEnabled": true
            },
            "extraCardProgramConfig": {
                "settlementConfig": {
                    "settler": "ZETA"
                }
            },
            "assetConfiguration": {},
            "currency": {
                "currencyCode": "INR"
            },
            "timezone": {
                "totalSeconds": 19800
            },
            "ifi": "",
            "ifiName": "",
            "businessID": "",
            "headers": {}
        },
        "closeCardConfiguration": {
            "cardCloseFrequency": "CURRENT_FINANCIAL_YEAR",
            "closeBehaviour": "",
            "closeCardDate": ""
        }
    },
    "books": {
        "companyID": "",
        "corpID": "",
        "productType": "books",
        "fundingAccountID": "",
        "message": "",
        "greeting": "",
        "corpProductType": "OPTIMA",
        "cardProgram": {
            "businessName": "",
            "businessDetails": {
                "billingAddress": {
                    "line1": "",
                    "city": "",
                    "region": "",
                    "countryID": "",
                    "postCode": ""
                }
            },
            "cardName": "",
            "active": true,
            "productType": "Books",
            "cardDesign": {
                "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                "bg": "https://card-program-files.s3.amazonaws.com/books_card.png",
                "bgColor": "#FA7800",
                "offerColor": "#FFFFFF",
                "textColor": "#FFFFFF",
                "cardName": ""
            },
            "cashBackProgram": {
                "maxCashBackProportion": 100,
                "headers": {}
            },
            "voucherProgram": {
                "voucherName": "",
                "voucherDesign": "http://x.y.z",
                "voucherValidity": "CURRENT_FINANCIAL_YEAR"
            },
            "reimbursementProgram": {
                "printedBillRequired": false,
                "lienSupported": true,
                "mandatorySpendThroughZeta": false,
                "maxValuePerBill": 10000000,
                "maxValuePerMonth": 10000000,
                "approval": "ZETA",
                "userDeclarationText": "You declare that that the value you are claiming is spent by you for purchase of books, periodicals and journals required for discharge of work only",
                "extraReimbursementProgramConfig": {
                    "allowRules": [{
                            "rule": "biller.allowBlurredBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowBillDatePastUploadDate",
                            "value": true
                        },
                        {
                            "rule": "biller.allowLastFinancialYearBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowBillDatePastUploadDate",
                            "value": false
                        },
                        {
                            "rule": "biller.allowOtherLanguageBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowTamperedBills",
                            "value": false
                        },
                        {
                            "rule": "biller.allowHandwrittenBills",
                            "value": false
                        }
                    ],
                    "requireRules": [{
                        "rule": "biller.requireBillNumber",
                        "value": true
                    }],
                    "bookCategories": [
                        "Business & Economics",
                        "Computing, Internet & Digital Media",
                        "Health, Family & Personal Development",
                        "Language, Linguistics & Writing",
                        "Law",
                        "Literature & Fiction",
                        "Politics",
                        "Reference",
                        "Sciences, Technology & Medicine",
                        "Society & Social Sciences",
                        "Travel",
                        "Finance & Accounting",
                        "Business & Finance"
                    ],
                    "uploadBillURL": "zeta://zapp/?zappID=multiBillUpload"
                }
            },
            "issuanceConfiguration": {
                "types": [
                    "VOUCHER"
                ],
                "issuer": "",
                "requiresExclusiveVirtualSuperCard": true,
                "expiryBehaviour": "moveToUserCashCard",
                "revokeBehaviour": "moveToIssuer",
                "closeCardBehaviour": "moveToUserCashCard"
            },
            "spendConfiguration": {
                "allowedTimeSlices": [],
                "spendDeclaration": "You declare that you are buying books, periodicals and journals required for discharge of work only",
                "allowedMerchants": [
                    "mid:REIMBURSEMENT-ONLY"
                ],
                "channels": {
                    "allowSuperCard": false,
                    "allowReimbursement": true,
                    "allowZetaApp": true
                },
                "limits": {
                    "transactionLimit": 10000000,
                    "dailyTxnCount": 10000000,
                    "monthlyTxnCount": 10000000,
                    "dailyLimit": 50000000,
                    "monthlyLimit": 50000000
                },
                "transactionConstraints": {
                    "minTransactionValue": 0,
                    "maxTransactionValue": 10000000,
                    "allowedParticipation": {
                        "type": "PERCENT_OF_VALUE",
                        "value": 100,
                        "valueCap": 1000000
                    }
                }
            },
            "notificationConfiguration": {
                "templateGroupSuffix": "BOOKS",
                "issueNotificationEnabled": true,
                "issueNotificationForRegisteredUser": true,
                "claimNotificationEnabled": true
            },
            "templateID": "iosBooksAndPeriodicalsCardProgram_EN_INBOX",
            "androidTemplateID": "androidBooksAndPeriodicalsCard_EN_INBOX",
            "extraCardProgramConfig": {
                "settlementConfig": {
                    "settler": "ZETA"
                }
            },
            "assetConfiguration": {},
            "currency": {
                "currencyCode": "INR"
            },
            "timezone": {
                "totalSeconds": 19800
            },
            "ifi": "",
            "ifiName": "",
            "businessID": "",
            "headers": {}
        },
        "closeCardConfiguration": {
            "cardCloseFrequency": "CURRENT_FINANCIAL_YEAR",
            "closeBehaviour": "",
            "closeCardDate": ""
        }
    },
    uniformCategories: [
        "Formal Shirts",
        "Trousers",
        "T-Shirts",
        "Collar/Polo T-Shirts",
        "Jeans",
        "Formal Shoes",
        "Casual Shoes/Sports Shoes",
        "Blazers",
        "Formal Business Suit",
        "Kurtas",
        "Tie",
        "Scarves",
        "Salwar Kameez",
        "Sarees",
        "Dupatta",
        "Skirts",
        "Waistcoat",
        "Socks",
        "Sweater"
    ],
    booksCategories: {
        "Action & Adventure": ["Action & Adventure"],
        "Arts, Film & Photography": [
            "Architecture",
            "Cinema & Broadcast",
            "Dance",
            "Design & Fashion",
            "History, Theory & Criticism",
            "Museums & Museology",
            "Music",
            "Painting",
            "Photography",
            "Sculpture",
            "Theatre & Spectacles"
        ],
        "Biographies, Diaries & True Accounts": [
            "Biographies & Autobiographies",
            "Diaries, Letters & Journals",
            "True Accounts"
        ],
        "Business & Economics": [
            "Business Self-Help",
            "Business, Strategy & Management",
            "Industries & Business Sectors",
            "Business & Economics"
        ],
        "Children's & Young Adult": [
            "Adventure",
            "Comics & Mangas",
            "Crafts, Hobbies & Practical Interests",
            "Crime & Thriller",
            "Early Learning",
            "Family, Personal & Social Issues",
            "Fantasy, Science Fiction & Horror",
            "Games, Toys & Activities",
            "Historical Fiction",
            "History",
            "Humour",
            "Interactive & Activity Books",
            "Language Learning",
            "Literature & Fiction",
            "Money & Jobs",
            "Mysteries & Curiosities",
            "Painting, Arts & Music",
            "Picture Books",
            "Reference",
            "Religion",
            "Science, Nature & Technology",
            "Sport",
            "Traditional Stories"
        ],
        "Comics & Mangas": [
            "Comics & Mangas"
        ],
        "Computing, Internet & Digital Media": [
            "Computer & Video Games",
            "Computer Science",
            "Computer Security",
            "Databases",
            "Digital Media & Graphic Design",
            "Hardware & Handheld Devices",
            "Internet & Web",
            "Networks & System Administration",
            "Operating Systems",
            "Programming & Software Development",
            "Software & Business Applications",
            "Computing, Internet & Digital Media"
        ],
        "Crafts, Home & Lifestyle": [
            "Antiques & Collectables",
            "Food, Drink & Entertaining",
            "Games & Quizzes",
            "Gardening",
            "Handicrafts, Decorative Arts & Crafts",
            "Home & House Maintenance",
            "Lifestyle & Personal Style Guides",
            "Models & Model Railroading",
            "Pets"
        ],
        "Crime, Thriller & Mystery": [
            "Crime, Thriller & Mystery"
        ],
        "Exam Preparation": [
            "Banking & Insurance",
            "Defence",
            "Engineering Entrance",
            "Exams by UPSC",
            "Finance & Accounting",
            "Government Exams",
            "IGNOU Entrance Exam",
            "International Entrance Exams",
            "Interview Preparation",
            "Law Entrance Exams",
            "Management Entrance",
            "Medical Entrance",
            "Polytechnics & ITI",
            "Professional Certification Exams"
        ],
        "Fantasy, Horror & Science Fiction": [
            "Fantasy",
            "Horror",
            "Science Fiction"
        ],
        "Health, Family & Personal Development": [
            "Family & Relationships",
            "Healthy Living & Wellness",
            "Mind, Body & Spirit",
            "Personal Development & Self-Help",
            "Health, Family & Personal Development"
        ],
        "Historical Fiction": ["Historical Fiction"],
        "History": ["History"],
        "Humour": ["Humour"],
        "Language, Linguistics & Writing": [
            "Dictionaries",
            "Grammar",
            "Journalism",
            "Language Learning & Teaching",
            "Linguistics",
            "Rhetoric & Speech",
            "Writing Guides",
            "Language, Linguistics & Writing"
        ],
        "Law": ["Law"],
        "Maps & Atlases": ["Maps & Atlases"],
        "Reference": ["Reference"],
        "Literature & Fiction": [
            "Anthologies",
            "Classic Fiction",
            "Contemporary Fiction",
            "Essays",
            "Indian Writing",
            "Literary Theory, History & Criticism",
            "Myths, Legends & Sagas",
            "Plays",
            "Poetry",
            "Religious & Spiritual Fiction",
            "Short Stories",
            "Travel Writing"
        ],
        "Politics": [
            "multiSelectOpts",
            "Freedom & Security",
            "International Relations & Globalization",
            "Political Ideologies",
            "Political Parties",
            "Political Theory",
            "Political Structure & Processes",
            "Public Administration",
            "Politics"
        ],
        "Romance": ["Romance"],
        "Sciences, Technology & Medicine": [
            "Agriculture & Farming",
            "Astronomy",
            "Biology & Life Sciences",
            "Chemistry",
            "Earth Sciences",
            "Engineering & Technology",
            "Environment",
            "Geography",
            "Mathematics",
            "Medicine",
            "Physics",
            "Transportation & Automotive",
            "Sciences, Technology & Medicine"
        ],
        "Society & Social Sciences": ["Humour", "Society & Social Sciences"],
        "Sports": [
            "Active Outdoor Pursuits", "Air Sports", "American Football", "Baseball", "Basketball", "Bodybuilding & Weightlifting", "Combat Sports & Self-Defence", "Cricket", "Cycling", "Equestrian & Animal Sports", "Field Sports", "Golf", "Gymnastics", "Marathon & Running", "Motor Sports", "Pool, Billiards & Snooker", "Rugby", "Skateboarding & Rollerblading", "Soccer", "Sporting Events & Organisations", "Tennis", "Training & Coaching", "Triathlon", "Water Sports", "Winter Sports"
        ],
        "Textbooks": [
            "Business & Finance", "Communication & Journalism", "Computer Science", "Teaching & Education", "Engineering", "Humanities", "Law", "Medicine & Health Sciences", "Science & Mathematics", "Social Sciences", "School Texts"
        ],
        "Travel": [
            "Illustrated Books", "Travel & Holiday Guides", "Travel Writing", "Travel"
        ],
        "Newspapers": ["Newspapers"]
    },
    expiryDropdownData: {
        "CURRENT_FINANCIAL_YEAR": "Current Financial Year",
        "CURENT_CALANDER_YEAR": "Current Calendar Year",
        "CURRENT_MONTH": "Current Month",
        "ONE_DAY": "1 Day",
        "THREE_DAYS": "3 Days",
        "THIRTY_DAYS": "30 Days",
        "NINTY_DAYS": "90 Days",
        "ONE_TWENTY_DAYS": "120 Days",
        "THREE_SIXTY_FIVE_DAYS": "365 Days",
        "THREE_YEARS": "3 Years",
        "FOUR_YEARS": "4 Years"
    }
}

module.exports = programStaticData;
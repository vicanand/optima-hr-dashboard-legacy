const Bunyan = require('bunyan');
const Util = require('util');
const env = process.env.NODE_ENV || 'development_stage';
const config = require('../../config');
const KinesisWritable = require('aws-kinesis-writable');
const stream = new KinesisWritable(config.logger).on('error', function(err) {
    console.error('this is kinesisWritable config',JSON.stringify({
        message: err.message,
        records: err.records,
        stack: err.stack
    }));
});

var Logger = function(options) {
    options.streams = [{
        stream: stream,
        level: config.logger.bunyanLogLevel,
        count: 1
    }];
    if (config.logger.debug) {
        options.streams[1] = {
            stream: process.stdout,
            level: 'debug'
        }
    } 
    this._emit = (rec, noemit) => {
        delete rec.v;
        delete rec.time;
        delete rec.name; 
        Bunyan.prototype._emit.call(this, rec, noemit);
      };
    Bunyan.call(this, options);
};

Util.inherits(Logger, Bunyan);

const wrapper = function(level) {
    return function() {
        if (arguments.length === 0) {
            return;
        }   
        var customArguments = [];
        if (arguments[0] instanceof Error) {
            customArguments[0] = {
                error: Bunyan.stdSerializers.err(arguments[0])
            };
            customArguments = customArguments.concat(Array.prototype.slice.call(arguments, 1));
        } else if (arguments[0] instanceof Object) {
            customArguments[0] = objCopy(arguments[0]);
            customArguments = customArguments.concat(Array.prototype.slice.call(arguments, 1));
        } else {
            customArguments[0] = {};
            customArguments = customArguments.concat(Array.prototype.slice.call(arguments));
        }

        customArguments[0].timestamp = new Date().toISOString();
        customArguments[0].app = 'CorpBenService';
        customArguments[0].subApp = 'UI';
        customArguments[0].logger = this.fields.name;
        customArguments[0].host = env;
        customArguments[0].log = level;
        Logger.super_.prototype[level].apply(this, customArguments);
    }
};

function objCopy(obj) {
    if (obj === null) {
        return null;
    } else if (Array.isArray(obj)) {
        return { inputArray: obj.slice() };
    } else {
        var copy = {};
        Object.keys(obj).forEach(function(k) {
            copy[k] = obj[k];
        });
        return copy;
    }
}

Logger.prototype.trace = wrapper('trace');
Logger.prototype.info = wrapper('info');
Logger.prototype.debug = wrapper('debug');
Logger.prototype.warn = wrapper('warn');
Logger.prototype.error = wrapper('error');
Logger.prototype.fatal = wrapper('fatal');

module.exports = Logger;
module.exports.createLogger = function(options) {
    return new Logger(options);
};
const _ = require('lodash');
var Config = require('../../config');
var Constant = require('../common/constant');
const env = process.env.NODE_ENV || 'development';
const {CURRENCY_CONFIGS, COUNTRY_CONFIGS} = require('../common/countryConfig/configMappings');


let formatINRWithPaise = function (number) {
    var totalAmtSplitArr = Number(number).toString().split('.');
    var totalAmtINR = '';
    if (totalAmtSplitArr && totalAmtSplitArr.length) {
        totalAmtINR += totalAmtSplitArr[0].replace(/(\d)(?=(\d\d)+\d$)/g, '$1,');
        if (totalAmtSplitArr.length === 2) {
            totalAmtINR += '.' + totalAmtSplitArr[1];
        }
    }
    return totalAmtINR;
};

let formatINR = function (number) {
    var totalAmtSplitArr = Number(number).toString().split('.');
    var totalAmtINR = '';
    if (totalAmtSplitArr && totalAmtSplitArr.length) {
        totalAmtINR += totalAmtSplitArr[0].replace(/(\d)(?=(\d\d)+\d$)/g, '$1,');
        // Not considering the decimals i.e. paise
        /*if (totalAmtSplitArr.length === 2) {
            totalAmtINR += '.' + totalAmtSplitArr[1];
        }*/
    }
    return totalAmtINR;
};
let formatNumber = function (number) {
    var totalValSplitArr = Number(number).toString().split('.');
    var totalVal = '';
    if (totalValSplitArr && totalValSplitArr.length) {
        totalVal += totalValSplitArr[0].replace(/(\d)(?=(\d\d)+\d$)/g, '$1,');
    }
    return totalVal;
};
let dateFormat = function (date, month, year) {
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    return date + ' ' + months[month - 1] + ' ' + year;
};

let dateFormatWithDay = function (date) {
    let dateNew = new Date(date);
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][dateNew.getMonth()];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][dateNew.getDay()];
    return days + ', ' + dateNew.getDate() + ' ' + months + ' ' + dateNew.getFullYear();
};

let timestampToDate = function (timestamp) {
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let currDate = new Date(timestamp);
    return currDate.getDate() + ' ' + months[currDate.getMonth()] + ' ' + currDate.getFullYear();
};

let timeStampToDate1 = function (timestamp) {
    let currDate = new Date(timestamp);
    return this.dateFormat(currDate.getDate(), currDate.getMonth() + 1, currDate.getFullYear());
}

let timestampToMonthYear = function (timestamp) {
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let currDate = new Date(timestamp);
    return months[currDate.getMonth()] + '-' + currDate.getFullYear();
};

let getMonthFullNameMap = function (timestamp) {
    return {
        'jan': 'January',
        'feb': 'February',
        'mar': 'March',
        'apr': 'April',
        'may': 'May',
        'jun': 'June',
        'jul': 'July',
        'aug': 'August',
        'sep': 'September',
        'oct': 'October',
        'nov': 'November',
        'dec': 'December'
    };
};


var checkIfAuthErrorAndRedirect = function(error, req, res) {
    let checkGenie = 0; let ssoUrl = Config.ssoUrl; let queryParams; let status = false; let logout = false;
    if(req.session && req.session.genie){
        checkGenie = req.session.genie;
    }
    var errorMessage = error && error.statusMessage && error.statusMessage.toLowerCase();
    if (error && (checkGenie === 0) && error.statusCode === 500 && error.body && typeof(error.body) === 'string' && (error.body.indexOf("auth") > -1)) {
        // res.redirect(Config.ssoUrl + 'logout?redirectUrl=' + req.protocol +'://' + req.headers.host + (req.originalUrl || '/dashboard'));
        queryParams = '?redirectUrl=' + req.protocol +'://' + req.headers.host + (req.originalUrl || '/dashboard');
        logout = true;
    }
    if (error && typeof (error.body) === 'string') {
        errorMessage = errorMessage.concat(error.body.toLowerCase());
    } else if (error && error.body && typeof (error.body.message) === 'string') {
        errorMessage = errorMessage.concat(error.body.message.toLowerCase());
    }
    if (error && (checkGenie === 0) && ((error.statusCode === 401) || (errorMessage && errorMessage.match('token')))) {
        if (req.originalUrl.indexOf('/dashboard') > -1) {
            req.session.destroy();
            // res.redirect(Config.ssoUrl + 'logout?redirectUrl=' + req.protocol +'://' + req.headers.host + '/dashboard');
            queryParams = '?redirectUrl=' + req.protocol +'://' + req.headers.host + '/dashboard';
            logout = true;
        }
        else{
            // res.redirect(Config.ssoUrl + 'logout?redirectUrl=' + req.protocol +'://' + req.headers.host + (req.originalUrl || '/dashboard'));
            queryParams = '?redirectUrl=' + req.protocol +'://' + req.headers.host + (req.originalUrl || '/dashboard');
            logout = true;
        }
        status = true;
    }
    if (logout) {
        if (req.headers.host.indexOf('smdigitalportal') != -1 ) {
            ssoUrl = Config.ssoPhUrl || COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.SSO_URL[env];
            let ifi = COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.PLATFORM_IFI[env];
            queryParams += '&appID=' + COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.APP_ID + '&ifi=' + ifi;
        } else if (req.headers.host.indexOf('sodexo.ph') != -1 || req.headers.host.indexOf('sodexoph-') != -1) {
            queryParams += '&countryCode=' + COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.COUNTRY_CODE + '&ifi=' + COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.PLATFORM_IFI[env];
        } else if (req.headers.host.indexOf('sodexo.vn') != -1 || req.headers.host.indexOf('sodexovn') != -1) {
            queryParams += '&countryCode=' + COUNTRY_CONFIGS.SODEXO_VIETNAM_CONFIGS.COUNTRY_CODE + '&ifi=' + COUNTRY_CONFIGS.SODEXO_VIETNAM_CONFIGS.PLATFORM_IFI[env];
        }
        res.redirect(ssoUrl + 'logout' + queryParams);
    }
    return status;
};

let errorFoundHandler = function (error, req, res, next) {
    let checkGenie = 0;
    if (req.session.genie) {
        checkGenie = req.session.genie;
    }
    if (checkIfAuthErrorAndRedirect(error, req, res)) {
        return;
    }
    // if (env == 'development') {
    //     var err = new Error(JSON.stringify(error));
    // } else {
    if (checkGenie == 0) {
        var err = new Error(Constant.ERROR_MEESAGE);
    } else {
        var err = new Error(Constant.ERROR_MEESAGE_GENIE);
    }
    // }
    err.status = 500;
    err.mailto = res.locals.country_details.supportEmail;
    next(err);
};

let filterRecommondedPrograms = function (passedArray, passedFilter) {
    var filteredArray = passedArray.filter(
        function (el) {
            for (var i = 0; i < passedFilter.length; i++) {
                if (el.type == passedFilter[i].cardProgramObject.productType) {
                    return false;
                }
            }
            return true;
        }
    );
    return filteredArray;
}


let epochToIstDate = function (epoch) {
    var date = new Date(epoch);
    var dateFormat;
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var monthIndex = date.getMonth();
    dateFormat = date.getDate() + " " + months[monthIndex] + " " + date.getFullYear();
    return dateFormat;
}

let compareLastIssuanceToCurrentDate = function (lastIssuanceDate) {
    var lastIssuanceDate = new Date(lastIssuanceDate);
    var currentDate = new Date();
    return (lastIssuanceDate < currentDate);
}

let compareProductTypeToPrograms = function (productType) {
    return (Constant.NON_REIMBURSABLE_PROGRAMS.indexOf(productType) !== -1);
}

let compareTenDaysBefore = function (date) {
    var lastIssuanceDate = new Date(date);
    var tenDaysBeforeDate = new Date(lastIssuanceDate.getTime() - (10 * 24 * 60 * 60 * 1000));
    var currentDate = new Date();
    return (currentDate >= tenDaysBeforeDate && currentDate <= lastIssuanceDate);
}

let maskAccountNumber = function (number) {
    return number.replace(/.(?=.{4})/g, '*');
}

let sortArrayOfObject = function (obj = [], field = "", type = 'ASC') {
    // debugger;
    let sorted;
    if (type == 'ASC') {
        sorted = _.sortBy(obj, field);
    } else {
        sorted = _.sortBy(obj, field).reverse();
    }
    return sorted;
}

let htmlDecoder = function (encodedStr) {
    return _.unescape(encodedStr);
}

let epochToIstTime = function (epoch) {
    var date = new Date(epoch);
    var timeFormat;
    timeFormat = date.getHours() + ":" + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    return timeFormat;
}

let dateToepochTime = function (date, separate, beforeAfter) {
    var myDate = date;
    myDate = myDate.split(separate);
    let currenthours = new Date().getHours();
    let currentMinutes = new Date().getMinutes();
    var newDate = myDate[1] + "/" + myDate[2] + "/" + myDate[0]
    if (beforeAfter == 'before') {
        return new Date(newDate + ' ' + currenthours + ':' + currentMinutes).getTime();
    }
    if (beforeAfter == 'after') {
        return new Date(newDate).getTime();
    }
}

let htmlEntities = function (str) {
    let encodedStr = str.replace(/[\u00A0-\u9999<>\&]/gim, function (i) {
        return '&#' + i.charCodeAt(0) + ';';
    });
    return encodedStr.replace(/&/gim, '&amp;');
};

let getCompBussinessId = function(comp_info, companyId) {
    let bussinessId = ''
    comp_info.forEach(function (company) {
        if (company.company.companyID == companyId) {
            bussinessId = company.company.businessID
        }
    })

    return bussinessId;
}

let currencyNameToSymbol = function(currency_name) {
    let name = currency_name || 'INR';
    if(CURRENCY_CONFIGS[name] !== undefined) {
        return CURRENCY_CONFIGS[name].symbol;
    }
}

let getLanguagesByCurrency = function(currency_name) {
    let name = currency_name || 'INR';
    if(CURRENCY_CONFIGS[name] !== undefined) {
        return CURRENCY_CONFIGS[name].languages;
    }
}

let getUrlParameter = function (sParam, origin) {
    if (!origin)
        return false;
    var sPageURL = decodeURIComponent(origin.replace('/','').replace('?','')),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
    return false;
}
let formatCurrency = function(number, currency_name, language, currencyConfig) {
    currency_name = currency_name && CURRENCY_CONFIGS[currency_name] !== undefined ? currency_name : 'INR'; // get currency name
    currencyConfig = currency_name && CURRENCY_CONFIGS[currency_name];
    const languages = getLanguagesByCurrency(currency_name); // get languages object
    language = languages && Object.keys(languages).length ? language && languages[language] && language || Object.keys(languages)[0] : 'en-IN'; // select language
    let config = {currency: currency_name, minimumFractionDigits: 0}; // default config
    // checkout more about config options: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toLocaleString#Parameters
    if (currencyConfig && Object.keys(currencyConfig).length) { // extend default config
        config = _.extend(config, currencyConfig);
    }
    let amountValue = parseFloat(number).toLocaleString(language, config);
    if (!config.style || (config.style && config.style === 'decimal') && languages[language].displaySymbolPosition) {
        amountValue = (languages[language].displaySymbolPosition === 'PREFIX' ? currencyNameToSymbol(currency_name) : '') + amountValue + (languages[language].displaySymbolPosition === 'POSTFIX' ? currencyNameToSymbol(currency_name): '');
    }
    return amountValue;
};

let extractEmailFirstPart = function(emailAddress) {
    return emailAddress.substring(0, emailAddress.indexOf("@"));
}

let getDataFromObject = function(object, propertyPath, defaultValue) {
    if (defaultValue) {
        return _.get(object, propertyPath, defaultValue);
    }
    return _.get(object, propertyPath);
}

let checkPriviledge = function(rbac, action, companyID){
    let hasPriviledge = true;
    let newAction = action + "_" + companyID
    let reverseMapAction = Constant.reverseMapActions[action] + "_" + companyID
    if (rbac && Object.keys(rbac).length > 1){
        if(!rbac[newAction] && rbac[reverseMapAction]){
            hasPriviledge = false;
        }
    }

    return hasPriviledge;
}

let isProgramManagerRead = function(rbac, companyID) {
    let isProgramManagerRead = false
    let action = "listOrders_"+companyID
    if (rbac && Object.keys(rbac).length > 1){
        if(rbac[action]){
            isProgramManagerRead = true;
        }
    }

    return isProgramManagerRead
}

let isProgramManagerWrite = function(rbac, companyID) {
    let isProgramManagerWrite = false
    let action = "createOrder_"+companyID
    if (rbac && Object.keys(rbac).length > 1){
        if(rbac[action]){
            isProgramManagerWrite = true;
        }
    }

    return isProgramManagerWrite
}

module.exports = {
    dateFormat: dateFormat,
    errorFoundHandler: errorFoundHandler,
    formatINR: formatINR,
    formatINRWithPaise: formatINRWithPaise,
    filterRecommondedPrograms: filterRecommondedPrograms,
    timestampToDate: timestampToDate,
    timestampToMonthYear: timestampToMonthYear,
    formatNumber: formatNumber,
    epochToIstDate: epochToIstDate,
    compareLastIssuanceToCurrentDate: compareLastIssuanceToCurrentDate,
    compareProductTypeToPrograms: compareProductTypeToPrograms,
    compareTenDaysBefore: compareTenDaysBefore,
    checkIfAuthErrorAndRedirect: checkIfAuthErrorAndRedirect,
    maskAccountNumber: maskAccountNumber,
    sortArrayOfObject: sortArrayOfObject,
    htmlDecoder: htmlDecoder,
    epochToIstTime : epochToIstTime,
    dateToepochTime : dateToepochTime,
    timestampToDate1: timeStampToDate1,
    htmlEntities:htmlEntities,
    getCompBussinessId: getCompBussinessId,
    currencyNameToSymbol: currencyNameToSymbol,
    getUrlParameter: getUrlParameter,
    extractEmailFirstPart: extractEmailFirstPart,
    formatCurrency: formatCurrency,
    getDataFromObject: getDataFromObject,
    checkPriviledge: checkPriviledge,
    isProgramManagerWrite: isProgramManagerWrite,
    isProgramManagerRead: isProgramManagerRead
};
var express = require("express");
var glob = require("glob");

var favicon = require("serve-favicon");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var compress = require("compression");
var methodOverride = require("method-override");
var flash = require("connect-flash");
var session = require("express-session");
var middleware = require("./common/middleware");
var errorHandler = require("./common/errorHandler");
var RedisStore = require("connect-redis")(session);
var redis = require("redis");
var _ = require("underscore");
// var xFrameOptions = require("x-frame-options");

var sentryConfig = require("./common/sentryConfig");

module.exports = function(app, config) {
  var env = process.env.NODE_ENV || "development";
  var client = redis.createClient(config.redis.port, config.redis.host);
  client.on("error", function(e) {
    console.log("error", e);
  });

  app.locals.ENV = env;
  app.locals.ENV_DEVELOPMENT = env == "development";

  app.set("views", config.root + "/public/template");
  app.set("view engine", "ejs");
  app.set("X-Frame-Options", "SAMEORIGIN");
  app.disable("x-powered-by");

  sentryConfig.initialize(app);
  // app.use(favicon(config.root + '/public/image/favicon.ico'));
  app.use(logger("dev"));
  app.use(bodyParser.json());
  app.use(
    bodyParser.urlencoded({
      extended: true
    })
  );
  app.use(cookieParser());
  app.use(
    session({
      store: new RedisStore({
        host: config.redis.host,
        port: config.redis.port,
        client: client,
        ttl: config.redis.ttl,
        prefix: config.app.domain,
        logErrors: true
      }),
      secret: config.redis.secret,
      resave: false,
      saveUninitialized: true
    })
  );
  app.use(compress());
  // app.use(xFrameOptions());
  // app.locals.deployVersion = Math.ceil((new Date).getTime()/300000)*300000;

  app.locals.deployVersion = Math.ceil(new Date().getTime() / 300000) * 300000;
  var cacheTime = 86400000 * 7;

  app.use(
    "/css",
    express.static(config.root + "/public/dist/css", {
      maxAge: cacheTime
    })
  );

  app.use(
    "/fonts",
    express.static(config.root + "/public/dist/fonts", {
      maxAge: cacheTime
    })
  );

  app.use(
    "/cached",
    express.static(config.root + "/public/cached", {
      maxAge: cacheTime
    })
  );

  app.use(
    "/images",
    express.static(config.root + "/public/dist/images", {
      maxAge: cacheTime
    })
  );

  app.use(
    "/scripts",
    express.static(config.root + "/public/dist/scripts", {
      maxAge: cacheTime
    })
  );

  app.use(
    "/",
    express.static(config.root + "/public", {
      maxAge: 0
    })
  );
  // app.get("/", function(req, res) {
  //   res.writeHead(200, { "X-Frame-Options": "SAMEORIGIN" });
  //   res.get("X-Frame-Options");
  // });

  app.use(methodOverride());
  app.use(flash());

  middleware(app);

  var routers = glob.sync(config.root + "/app/router/*.js");
  routers.forEach(function(router) {
    require(router)(app);
  });

  sentryConfig.errorHandler(app);
  errorHandler(app);
};

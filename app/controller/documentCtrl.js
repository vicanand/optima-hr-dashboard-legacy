var BenefitSvc = require('../service/benefitSvc');
var DocumentSvc = require('../service/documentSvc');
var Constant = require('../common/constant');
var DocStoreSvc = require('../service/documentSvc');
var Util = require('../common/util');
var UtilsUser = require('../common/utils/user.js');
htmlDecoder = Util.htmlDecoder;

let init = function(req, res, next) {
    let allowDownload = UtilsUser.checkGKFlag(req, res.locals.selectedCompany.company.companyID, Constant.SHOW_DOCUMENT_DOWNLOAD);
    let allowOtherDocuments = UtilsUser.checkGKFlag(req, res.locals.selectedCompany.company.companyID, Constant.SHOW_OTHER_DOCUMENTS);
    let pageData = {
        programTypes: {},
        totalPages:'',
        userEmailId: '',
        corporateId:'',
        businessId: res.locals.selectedCompany.company.businessID,
        allowDownload: allowDownload,
        allowOtherDocuments: allowOtherDocuments
    }
    res.locals.pageName = 'document';
    let corpID = res.locals.corp.id;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let businessId = res.locals.selectedCompany.company.businessID
    let companyID = res.locals.selectedCompany.company.companyID;
    pageData.userEmailId = res.locals.user.account.accountDetails.email;
    pageData.corporateId = corpID;
    pageData.requestedSource = req.session.requestedSource
    let othersCompany = [];
    let currentCompany = [];
    currentCompany.push({businessId:businessId+'-'+companyID,companyName:res.locals.selectedCompany.company.companyName}); // storing current company businessid and name
    for( let i=0; i<res.locals.companies.length; i++){
        if(res.locals.companies[i].company.businessID != businessId ){
            othersCompany.push({businessId:res.locals.companies[i].company.businessID+'-'+res.locals.companies[i].company.companyID,companyName:res.locals.companies[i].company.companyName});
        }
    }
    pageData.currentCompany = currentCompany;
    pageData.othersCompany = othersCompany;
    pageData.businessId = businessId;
    pageData.allStates = Constant.DOC_DRIVE_STATUS_UPDATED;
    if(allowOtherDocuments == true){
        pageData.allDocumentsSource = Constant.DOC_DRIVE_DOC_SOURCE;
    }
    
    BenefitSvc.getActiveCardsDocumentDrivePost(authToken, corpID, companyID).then(function(respData) {
        let activeCards = respData;
        programs = activeCards.programs;
        programsCount = programs.length;
        for (let i = 0; i < programsCount; i++) {
            if (programs[i].cardProgramObject.spendConfiguration.channels.allowReimbursement === true) {
                pageData.programTypes[programs[i].cardProgramObject.cardProgramID] = programs[i].cardProgramObject.cardName;
            }
        }
        let totalBenefitPrograms = activeCards.totalRequestCount;
        let totalPages = Math.ceil(totalBenefitPrograms/12); 
        pageData.totalPages = totalPages;
        DocStoreSvc.getAllDocGroupsWithPetro(authToken, corpID, businessId, companyID).then(function(respData) {
            if (respData.docGroupsInfos.length == 0) {
                pageData["noData"] = true;
            } else {
                pageData["noData"] = false;
            }
            for (var i = 0; i < respData.docGroupsInfos.length; i++) {
                respData.docGroupsInfos[i].attrs['ProductType'] = Constant.CARD_PROGRAM_TYPES[respData.docGroupsInfos[i].attrs.producttype];
                if (respData.docGroupsInfos[i].attrs.claimAmount) {
                    respData.docGroupsInfos[i].attrs.claimAmount = Util.formatINR(Number(respData.docGroupsInfos[i].attrs.claimAmount) / 100);
                }
                
                if(typeof (respData.docGroupsInfos[i].docGroupID.split('biller/claims/')[1]) == 'undefined'){ // for petro card
                    let docGroupIdTemp = respData.docGroupsInfos[i].docGroupID.split('optima/proofs/')[1];
                    let doccorpId = docGroupIdTemp.split('/', 1)[0];
                    let doccompanyid = docGroupIdTemp.split('/', 2)[1];
                    let docprogramid =docGroupIdTemp.split('/', 3)[2];
                    let docemployeeid = docGroupIdTemp.split('/', 4)[3];
                    let doccardid  = docGroupIdTemp.split('/', 5)[4];
                    let docrand = docGroupIdTemp.split("/").pop();
                    respData.docGroupsInfos[i].docGroupID = doccorpId+'+-'+doccompanyid+'+-'+docprogramid+'+-'+docemployeeid+'+-'+doccardid+'+-'+docrand+'+-pc';
                } else{
                    respData.docGroupsInfos[i].docGroupID = respData.docGroupsInfos[i].docGroupID.split('biller/claims/')[1];
                }
                if (respData.docGroupsInfos[i].attrs && respData.docGroupsInfos[i].attrs.state) {
                    respData.docGroupsInfos[i].attrs.state = respData.docGroupsInfos[i].attrs.state.toLowerCase();
                    if (respData.docGroupsInfos[i].attrs.state === 'approved' || respData.docGroupsInfos[i].attrs.state === 'paid' || respData.docGroupsInfos[i].attrs.state === 'unpaid' || respData.docGroupsInfos[i].attrs.state === 'partially_paid' || respData.docGroupsInfos[i].attrs.state === 'partially_approved') {
                        respData.docGroupsInfos[i].attrs.state = 'approved';
                    }
                } else {
                    respData.docGroupsInfos[i].attrs.state = '-';
                }
            }
            pageData.docGroup = respData;
            res.render('document', pageData);
            res.end();
        }, function(error) {
            if (error.body.type == 'AccessDeniedException' || error.body.type == 'DocStoreNotFoundException') {
                pageData["noData"] = true;
                let docGroup={
                     docGroupsInfos:[]
                }
                pageData.docGroup=docGroup;
                res.render('document', pageData);
                res.end();
            } else {
                res.locals.showErrorWitHdr = true;
                Util.errorFoundHandler(error, req, res, next);
            }
        });
    });
}

let details = function(req, res, next) {
    let pageData = {};
    res.locals.pageName = 'document';
    let corpID = res.locals.corp.id;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let businessId = res.locals.selectedCompany.company.businessID
    let docGroupID = req.params.docGroupID;
    let businessID = req.query.businessID ? req.query.businessID.split('-', 1)[0] : businessId;
  //  let businessID = req.query.businessID.split('-', 1)[0]; // taking from url
    let petroDocGroupCheck = docGroupID.split("+-").pop();
    if((petroDocGroupCheck == 'pc') || (petroDocGroupCheck == 'tl') || (petroDocGroupCheck == 'ep')){
        let doccorpId = docGroupID.split('+-', 1)[0];
        let doccompanyid = docGroupID.split('+-', 2)[1];
        let docprogramid =docGroupID.split('+-', 3)[2];
        let docemployeeid = docGroupID.split('+-', 4)[3];
        let doccardid  = docGroupID.split('+-', 5)[4];
        let docrand = docGroupID.split('+-', 6)[5];
        if(petroDocGroupCheck == 'pc'){
            docGroupID = "optima/proofs/"+doccorpId+'/'+doccompanyid+'/'+docprogramid+'/'+docemployeeid+'/'+doccardid+'/'+docrand;
        } 
        else if(petroDocGroupCheck == 'ep'){
            docGroupID = "optima/employee_proof/"+doccorpId+'/'+doccompanyid+'/'+docprogramid+'/'+docemployeeid+'/'+doccardid+'/'+docrand;
        }else {
            docGroupID = "optima/travel_log/"+doccorpId+'/'+doccompanyid+'/'+docprogramid+'/'+docemployeeid+'/'+doccardid+'/'+docrand;
        }
    }
    else{
        docGroupID = "biller/claims/"+docGroupID;
    }
    DocStoreSvc.getDocDetailsNew(authToken, corpID, businessID, docGroupID).then(function(respData) {
  //      console.log('responseData', respData);
        let claimStatus = [],
            checkList = [],
            dependants = [],
            category,
            subCategory,
            operatorName;
            let timeArray = [];
        for (var i = 0; i < respData.docs.length; i++) {
            timeArray.push(Date.parse(respData.docs[i].createdAt));
            if (respData.docs[i].docType == 'CONTENT') {
                respData.docs[i].data = JSON.parse(Util.htmlDecoder(respData.docs[i].data));
                var diffCreationTime = new Date(respData.docs[i].createdAt),
                    state = '',
                    declineReason = '',
                    claimTime;
                if (respData.docs[i].data.newBillState.state) {
                    state = Constant.DOC_DRIVE_STATUS[respData.docs[i].data.newBillState.state];
                }
                if (respData.docs[i].data.newBillState.attrs && respData.docs[i].data.newBillState.attrs.declineReason) {
                    declineReason = respData.docs[i].data.newBillState.attrs.declineReason;
                }
                if (respData.docs[i].data.newBillState.attrs && respData.docs[i].data.newBillState.attrs.category) {
                    category = respData.docs[i].data.newBillState.attrs.category;
                }
                if (respData.docs[i].data.newBillState.attrs && respData.docs[i].data.newBillState.attrs.subCategory) {
                    subCategory = respData.docs[i].data.newBillState.attrs.subCategory;
                }
                if (respData.docs[i].data.newBillState.attrs && respData.docs[i].data.newBillState.attrs.dependants) {
                    dependants = JSON.parse(respData.docs[i].data.newBillState.attrs.dependants);
                }
                if (respData.docs[i].data.newBillState.state != 'UPLOADED') {
                    if (respData.docs[i].data.newBillState.attrs && respData.docs[i].data.newBillState.attrs.checkList) {
                        checkList = JSON.parse(respData.docs[i].data.newBillState.attrs.checkList);
                   //     console.log('old checklist', checkList);
                    }
                    if (respData.docs[i].data.newBillState.attrs && respData.docs[i].data.newBillState.attrs.operatorName) {
                        operatorName = respData.docs[i].data.newBillState.attrs.operatorName;
                    }
                    var claimStatusItem = {
                        "state": state,
                        "declineReason": declineReason,
                        "stateClass": Constant.DOC_DRIVE_STATUS_CLASS[state],
                        "timeStamp": respData.docs[i].data.newBillState.updatedAt,
                        "operatorName": operatorName
                    }
                } else {
                    if (respData.docs[i].data.newBillState.uploadedAt) {
                        claimTime = respData.docs[i].data.newBillState.uploadedAt;
                    } else {
                        claimTime = respData.docs[i].data.newBillState.updatedAt;
                    }
                    var claimStatusItem = {
                        "state": state,
                        "declineReason": declineReason,
                        "stateClass": Constant.DOC_DRIVE_STATUS_CLASS[state],
                        "timeStamp": claimTime,
                        "operatorName": respData.attrs.name
                    }
                }
                claimStatus.push(claimStatusItem);
                if ((respData.attrs.state === "UNPAID" || respData.attrs.state === "UPLOADED") && respData.docs[i].data.newBillState.attrs && respData.docs[i].data.newBillState.attrs['corpben.escalationState'] && ((respData.attrs.stateTime && diffCreationTime > respData.attrs.stateTime) || !respData.attrs.stateTime)) {
                    // Picking up the escalation state from the latest diff of the doc
                    respData.attrs.state = respData.docs[i].data.newBillState.attrs['corpben.escalationState'];
                    respData.attrs.stateTime = diffCreationTime;
                }
                if (respData.docs[i].data.newBillState.attrs && respData.docs[i].data.newBillState.attrs && respData.docs[i].data.newBillState.attrs.partialAmount && ((respData.attrs.approvalTime && diffCreationTime > respData.attrs.approvalTime) || !respData.attrs.approvalTime)) {
                    // Picking up the approvedAmount in case of escalated or partially paid from the latest diff of the doc
                    respData.approvedAmount = respData.docs[i].data.newBillState.attrs.partialAmount;
                    respData.attrs.approvalTime = diffCreationTime;
                }
            }
        }
      //  console.log('timeArray', timeArray);
        let sortedDocsResponseForChecklist = respData.docs.sort(function(a, b) {
            return Date.parse(b.createdAt) - Date.parse(a.createdAt);
        });
      //  console.log('sorted response',sortedDocsResponseForChecklist);
        for(var j=0;j<sortedDocsResponseForChecklist.length; j++){
            if (sortedDocsResponseForChecklist[j].data.newBillState) {
                if(sortedDocsResponseForChecklist[j].data.newBillState.state != 'UPLOADED'){
                    if(sortedDocsResponseForChecklist[j].data.newBillState.attrs && sortedDocsResponseForChecklist[j].data.newBillState.attrs.checkList){
                    checkList = JSON.parse(sortedDocsResponseForChecklist[j].data.newBillState.attrs.checkList);
                    break;
                    }
                }

            }

        }



        if (!respData.attrs.emailID) {
            respData.attrs.emailID = respData.attrs.email;
        }

        claimStatus.sort(function(a, b) {
            return b.timeStamp - a.timeStamp;
        });

        var uploadStatusCount = 0;
        claimStatus.forEach(function(val, i) {
            if (val.state == 'Uploaded') {
                uploadStatusCount++;
            }
        });
        var newStatusCount = 1;
        if (uploadStatusCount > 1) {
            for (var j = 0; j < claimStatus.length; j++) {
                if (claimStatus[j].state == 'Uploaded' && newStatusCount < uploadStatusCount) {
                    claimStatus[j].state = "Requeued";
                    claimStatus[j].operatorName = operatorName;
            //       console.log('requeue',operatorName);
                    newStatusCount++;
                }
            }
        }
        let claimStatusNew = [];
        
        for(var i=0;i< claimStatus.length; i++){ // for LTA declined reason backfill.
            if((claimStatus[i].state == 'Declined') && (claimStatus[i].declineReason == '')){
                if((i-1) >=0){
                if(claimStatus[i-1].declineReason != 'undefined') {
                    claimStatus[i].declineReason = claimStatus[i-1].declineReason;
                }
            }
            }
            if(claimStatus[i].state){
                claimStatusNew.push(claimStatus[i]);
            }
        }
        var amount = ((respData.attrs.claimAmount || respData.attrs.claimamount) / 100 || 0);
        respData.attrs.formattedClaimAmount = Util.formatINR(amount);
        respData.approvedAmount = respData.approvedAmount ? Util.formatINR(respData.approvedAmount / 100) : 0;
        respData.attrs.state = respData.attrs.state ? Constant.DOC_DRIVE_STATUS[respData.attrs.state] : '-';
        respData.claimStatus = claimStatusNew;
        respData.attrs.paidAmount = -1;
        if(respData.attrs.reimbursedAmount){
            respData.attrs.paidAmount = Util.formatINR(respData.attrs.reimbursedAmount.split(" ").pop() / 100);
        }
        // for LTA program dropdown and check list
        let productType = '';
        let ltaTypes = [];
        let billUrls = [];
        let hasProofOfLeave = false;
        if (respData.attrs.productType) {
            productType = respData.attrs.productType;
        }
        pageData.requestedSource = req.session.requestedSource
        if (productType === 'LTA') { // get bill details for lta
            DocStoreSvc.getBillDetails(authToken, docGroupID).then(function(data) {
                let billDetails = JSON.parse(data);
                if (billDetails.proofOfLeave) {
                    hasProofOfLeave = true;
                }
                if (billDetails.trips) {
                    for (var k = 0; k < billDetails.trips.length; k++) {
                        let select = billDetails.trips[k].origin.name + ' - ' + billDetails.trips[k].destination.name;
                        ltaTypes.push(select);
                    }
                }
                if (billDetails.trips && billDetails.trips[0] && billDetails.trips[0].tripAttrs && billDetails.trips[0].tripAttrs.checklist) {
                    checkList = JSON.parse(Util.htmlDecoder(billDetails.trips[0].tripAttrs.checklist));
                }
                if (billDetails.trips && billDetails.trips[0].supportingDocuments) {
                    for (var i = 0; i < billDetails.trips[0].supportingDocuments.length; i++) {
                        for (var j = 0; j < billDetails.trips[0].supportingDocuments[i].imageUrls.length; j++) {
                            billUrls.push(billDetails.trips[0].supportingDocuments[i].imageUrls[j]);
                        }
                    }
                    respData.attrs.billUrls = billUrls;
                }
                if(billDetails.attrs && billDetails.attrs.partialAmount){
                    respData.approvedAmount = billDetails.attrs.partialAmount ? Util.formatINR(billDetails.attrs.partialAmount / 100) : 0;
                }
                respData.checkList = checkList;
                pageData.ltaTypes = ltaTypes;
                pageData.docDetails = respData;
                pageData.dependants = dependants;
                pageData.category = category;
                pageData.subCategory = subCategory;
                pageData.docGroupID = docGroupID;
                pageData.proofOfLeave = hasProofOfLeave;
                pageData.Util = Util;
                pageData.petroDocGroupCheck = petroDocGroupCheck;
         //       console.log('page Data1',pageData);
                res.render('document-details', pageData);
                res.end();
            });
        } else {
            respData.checkList = checkList;
            pageData.ltaTypes = ltaTypes;
            pageData.docDetails = respData;
            pageData.dependants = dependants;
            pageData.category = category;
            pageData.subCategory = subCategory;
            pageData.docGroupID = docGroupID;
            pageData.Util = Util;
            pageData.petroDocGroupCheck = petroDocGroupCheck;
       //     console.log('page Data2',pageData);
            res.render('document-details', pageData);
            res.end();
        }

    }, function(error) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(error, req, res, next);
    });
}

module.exports = {
    init: init,
    details: details
};
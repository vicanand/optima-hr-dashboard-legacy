var QuickshopInfoSvc = require('../service/express/quickShopInfoSvc'),
    Constant = require('../common/constant'),
    ZetaMerchantSvc = require('../service/express/zetaMerchantSvc'),
    moment = require('moment'),
    Util = require('../common/util'),
    EmployeeSvc = require('../service/express/employeeSvc');

var init = function (req, res, next) {
    res.locals.pageName = 'express';
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    var corpID = res.locals.corp.id;

    var pageData = {
        bannerTitle: 'Employee Insights',
        companyID: req.params.companyId
    };

    var params = {
        corporateID: corpID
    };

    ZetaMerchantSvc.getCitiesOfficeStore(authToken, params).then(function (respData) {
        pageData.citiesOfficesStores = JSON.stringify(respData.expressInsightsStoreList);
        // console.log('pageData.citiesOfficesStores: ', pageData.citiesOfficesStores);
        res.render('section/express/employeeInsights', pageData);
        res.end();
    }, function (error) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(error, req, res, next);
    });
};


var getEmployeeStats = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    EmployeeSvc.getEmployeeStatistics(authToken, req.query).then(function (respData) {
        console.log('respData: ', respData);        
        res.status(200);
        res.send(respData);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
};

var getPaymentPreference = function (req, res, next) {
    console.log('req: ', req);
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    req.body.corporateID = res.locals.corp.id;
    EmployeeSvc.getPaymentPreference(authToken, req.body).then(function (respData) {
        
       
        res.status(200);
        res.send(respData);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
};

var getEmployeeInsightsTransactionDetails = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    req.query.corporateID = res.locals.corp.id;
    EmployeeSvc.getEmployeeInsightsTransactionDetails(authToken, req.query).then(function (respData) {
        res.status(200);
        res.send(respData);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
};


module.exports = {
    init,
    getEmployeeStats,
    getPaymentPreference,
    getEmployeeInsightsTransactionDetails
}
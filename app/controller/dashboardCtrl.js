var Constant = require('../common/constant');
var AuthSvc = require('../service/authSvc');
var Config = require('../../config');
var Endpoint = require('../common/url');
var ServiceConnector = require('../common/serviceConnector');
const Util = require('../common/util');
const _ = require('lodash');

let init = function (req, res, next) {
    let hostName = req.hostname;
    let isSodexoSignup = hostName.includes('signup-vt') || hostName.includes('signup.sodexo.vn');
    if (isSodexoSignup) {
        res.redirect('/vn-signup');
        res.end();
        return;
    }
    let pageData = {};
    res.locals.pageName = 'dashboard';
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let companyID = req.params.companyId || (req.session[Constant.SESSION_SELECTED_COMP_INFO] && req.session[Constant.SESSION_SELECTED_COMP_INFO].company.companyID);
    let activeProduct = req.session.selectedProgram || 'optima';
    
    if (companyID) {
        if (req.session[Constant.SESSION_CONFIG_DATA]) {
            const config_data = Util.getDataFromObject(req.session[Constant.SESSION_CONFIG_DATA], 'dashboardCtrl.init.activeProduct');
            if (config_data) {
                activeProduct = config_data;
            }
        }
        res.redirect('/companies/' + companyID + '/' + activeProduct);
        /*res.writeHead(301, {
            Location: '/companies/' + companyID + '/' + activeProduct
        });*/
        res.end(); 
    } else {
        let error = new Error(Constant.ERROR_MEESAGE);
        error.mailto = res.locals.country_details.supportEmail;
        dashboardErrorHandler(error, req, res, next);
    }
}

let goToHome = function(req, res, next) {
    let pageData = {};
    let goToCorpDetails;
    res.locals.pageName = 'dashboard';
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let companyID = req.params.companyId || (req.session[Constant.SESSION_SELECTED_COMP_INFO] && req.session[Constant.SESSION_SELECTED_COMP_INFO].company.companyID);
    let landingProductList = req.session[Constant.SESSION_CORP_INFO].allowedPrograms;
    let activeProduct='';
    if(landingProductList.indexOf("OPTIMA") > -1){   
        activeProduct = req.session.selectedProgram || 'optima';
    } else if(landingProductList.indexOf("SPOTLIGHT") > -1){
        activeProduct = "spotlight";
    } else if(landingProductList.indexOf("EXPRESS") > -1){
        activeProduct = "EXPRESS";
    } else if(landingProductList.indexOf("EXPENSE") > -1) {
        activeProduct = "EXPENSE";
    } else{
        activeProduct = req.session.selectedProgram || 'optima';
    }
    let companyResp = req.session[Constant.SESSION_COMP_INFO];
    res.setHeader('Content-Type', 'application/json');
    if (companyResp[0].company.attributes && companyResp[0].company.attributes.reviewBillingAddress) {
        goToCorpDetails = companyResp[0].company.attributes.reviewBillingAddress;
    }
    if (companyID) {
        res.status(200).json({
            "id": companyID,
            "product": activeProduct,
            "goToCorpDetails": goToCorpDetails
        });
    } else {
        let error = new Error(Constant.ERROR_MEESAGE);
        error.mailto = res.locals.country_details.supportEmail;
        dashboardErrorHandler(error, req, res, next);
    }
    res.end();
};


let dashboardErrorHandler = function(err, req, res, next) {
    let errorTemplate = '500-error-page';
    res.render(errorTemplate, {
        message: err.message,
        mail: err.mailto,
        error: {},
        title: 'Error'
    });
};

let genieInit= function(req, res, next){
    req.session["genie"] = true;
    next();
    // res.redirect(Config.ssoUrl + '?redirectUrl=' + 'https://' + req.headers.host + '/dashboard');
}

module.exports = {
    init: init,
    goToHome: goToHome,
    genieInit: genieInit
};
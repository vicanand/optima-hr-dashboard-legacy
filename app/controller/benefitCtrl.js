var AdminSvc = require('../service/adminSvc');
var AuthSvc = require('../service/authSvc');
var BenefitSvc = require('../service/benefitSvc');
var Constant = require('../common/constant');
var RecommonedProgramsModel = require('../model/recommonedProgramsModel');
var Util = require('../common/util');
var Config = require('../../config');
var Urls = require('../common/url');
var JSONbig = require('json-bigint');
var UtilsUser = require('../common/utils/user.js');
var programStaticData = require('../common/programStaticData.js');

let init = function (req, res, next) {
    res.locals.pageName = 'optima';
    let corpID = res.locals.corp && res.locals.corp.id;
    let companyID = req.params.companyId;
    setSelectedCompany(req, res);
    let checkGenie = 0;
    if (req.session.genie) {
        checkGenie = req.session.genie;
    }

    let rbacData = req.session.rbac_resources;
    let rbacInfo = req.session.rbac_data
    if(rbacData && rbacInfo.countryVar == 'isSodexoPh' && (rbacData['getFundingAccounts_'+companyID] || rbacData['moveFunds_'+companyID])){
        if(!rbacData['createOrder_'+companyID] & !rbacData['listOrders_'+companyID]){
            res.redirect('/admin/funding-accounts');
        }
    }


    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let isUpdateCorp = req.query.update;
    if (isUpdateCorp == "true") {
        AuthSvc.updateCorpAndCompDetails(req, res);
    }

    let promiseCalls = [
        BenefitSvc.getActiveCardsPost(authToken, corpID, companyID, req),
        BenefitSvc.getRecommendedPrograms(authToken, corpID, companyID, req)
    ];

    Promise.all(promiseCalls).then(function (respArr) {
        console.log('r' , respArr);
        let activeCardData = respArr[0];
        let activeCards = activeCardData,
            allRecommondedCardPrograms = RecommonedProgramsModel.init(),
            recommondedCardPrograms = JSON.parse(respArr[1]).entries,
            programs = activeCards.programs,
            programsCount = programs.length,
            newProgramSet = [],
            giftPrograms = [],
            allCardsSchedule = [],
            productTypeFilter = [],
            displayBanner = false,
            lastIssuanceDate,
            displayBannerDate = UtilsUser.checkGKFlag(req, res.locals.selectedCompany.company.companyID, Constant.YEAR_END_CLOSURE_GK_FLAG),
            programSetupEnabled = UtilsUser.checkGKFlag(req, res.locals.selectedCompany.company.companyID, Constant.PROGRAM_SETUP_ENABLED),
            displayTag = false;
        totalProgramPages = '';
        // for pagination
        let totalBenefitPrograms = activeCards.totalRequestCount;
        totalProgramPages = Math.ceil(totalBenefitPrograms / 12);
        //
        for (let i = 0; i < programsCount; i++) {
            if (programs[i].corpProductType === 'OPTIMA' && programs[i].cardProgramObject.productType.toLowerCase() !== 'incentive' && programs[i].cardProgramObject.productType.toLowerCase() !== 'cashless_cafeteria') {
                let productType = programs[i].cardProgramObject.productType.toLowerCase();
                productTypeFilter[i] = programs[i].cardProgramObject.productType;
                let cardName = programs[i].cardProgramObject.cardName;
                programs[i].cardProgramObject.taxStatus = Constant.CARDS_TAX_STATUS[productType];
                let compareProductTypeToPrograms = Util.compareProductTypeToPrograms(programs[i].cardProgramObject.productType);

                if (!compareProductTypeToPrograms) {
                    allCardsSchedule.push(programs[i]);
                }

                if (productType === 'gift' && !cardName) {
                    programs[i].cardProgramObject.cardName = 'Gift Vouchers';
                }

                if (programs[i].cardProgramObject.issuanceConfiguration && programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate) {
                    lastIssuanceDate = new Date(programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate);
                } else {
                    lastIssuanceDate = '';
                }
                if (Util.compareLastIssuanceToCurrentDate(lastIssuanceDate) || programs[i].status === 'CLOSED') {
                    // closedCards.push(programs[i]);
                } else {
                    if (productType === 'gift') {
                        giftPrograms.push(programs[i]);
                    } else {
                        newProgramSet.push(programs[i]);
                    }
                    if (programs[i].cardProgramObject.issuanceConfiguration && programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate && programs[i].cardProgramObject.issuanceConfiguration.extraIssuanceConfig) {
                        let compareTenDaysBefore = Util.compareTenDaysBefore(programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate);
                        if ( /*Util.displayBanner(Constant.DISPLAY_BANNER_DATE)*/ displayBannerDate && !compareProductTypeToPrograms && !programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate && programs[i].cardProgramObject.issuanceConfiguration.extraIssuanceConfig["corpben.financialYear"] === '2016-17') {
                            displayBanner = true;
                        }

                        if (compareTenDaysBefore && !compareProductTypeToPrograms) {
                            displayTag = true;
                        }
                    }
                }

            }
        }
        activeCards.programs = newProgramSet.concat(giftPrograms);
        activeCardData.programs = Util.sortArrayOfObject(activeCardData.programs, "cardProgramObject.cardName");
        let benefitHomePageData = {
            "activeCards": activeCards,
            "allCardsSchedule": allCardsSchedule,
            "displayBanner": displayBanner,
            "displayTag": displayTag,
            "productTypeFilter": productTypeFilter,
            "recommondedCardPrograms": recommondedCardPrograms,
            "companyID": companyID,
            "totalProgramPages": totalProgramPages,
            "checkGenie": checkGenie,
            "corpID": corpID,
            "programSetupEnabled": programSetupEnabled
        };


        AdminSvc.getFundingAcc(authToken, corpID, 1, 10).then(function (fundRespData) {
            let totalResp = []
            benefitHomePageData["fundingAccounts"] = JSON.parse(fundRespData);
            let totalFundCount = benefitHomePageData["fundingAccounts"].count;
            let pageNum = 2;
            let totalPages = Math.ceil(totalFundCount / 10);
            let fundingAccPromise = [];
            for (pageNum; pageNum < totalPages + 1; pageNum++) {
                fundingAccPromise.push(AdminSvc.getFundingAcc(authToken, corpID, pageNum, 10, req));
            }
            Promise.all(fundingAccPromise).then(function (respArr) {
                respArr.forEach(function (fundResData) {
                    let fundAccDet = JSON.parse(fundResData);
                    fundAccDet.fundingAccounts.forEach(function (accDetails) {
                        benefitHomePageData["fundingAccounts"].fundingAccounts.push(accDetails);
                    })
                });
                req.session["fundingAccounts"] = benefitHomePageData["fundingAccounts"].fundingAccounts;
            }).catch(function (err) {
                console.log('error', err)
            }).then(function () {
                if (benefitHomePageData.recommondedCardPrograms) {
                    for (var currIndex = 0; currIndex < benefitHomePageData.recommondedCardPrograms.length; currIndex++) {
                        let currentProgram = benefitHomePageData.recommondedCardPrograms[currIndex];
                        if (currentProgram.taxSaving && currentProgram.taxSaving.amount) {
                            benefitHomePageData.recommondedCardPrograms[currIndex].taxSaving.amount = Util
                            .formatINR(benefitHomePageData.recommondedCardPrograms[currIndex].taxSaving.amount / 100);
                        } else {
                            benefitHomePageData.recommondedCardPrograms[currIndex].taxSaving = {
                                PERIOD: '',
                                amount: 0
                            }
                        }
                    }
                } else {
                    benefitHomePageData.recommondedCardPrograms = [];
                }
                let isZetaSettled = false,
                    ifiIDValue;
                for (let k = 0; k < req.session.fundingAccounts.length; k++) {
                    if (req.session.fundingAccounts[k].type == "ZETA_SETTLED") {
                        isZetaSettled = true;
                        ifiIDValue = req.session.fundingAccounts[k].ifiID;

                    } else if (isZetaSettled == false) {
                        ifiIDValue = req.session.fundingAccounts[k].ifiID;
                    }
                }

                AdminSvc.getIfisFundingAcc(req.session[Constant.SESSION_AUTH].authToken, corpID, ifiIDValue, req).then(function (getIfisData) {
                    let getIfisBankDetails = JSON.parse(getIfisData);
                    req.session[Constant.SESSION_IFIS_BANK_NAME] = getIfisBankDetails.name;
                    benefitHomePageData.IFIsBankName = getIfisBankDetails.name;
                    benefitHomePageData.ifiBankName = getIfisBankDetails.name;
                    req.session["programs"] = activeCards.programs;
                    benefitHomePageData['selected_comp_info'] = req.session.selected_comp_info
                    // req.session["programs"] = activeCards.programs.concat(closedCards);

                    benefitHomePageData.templateData = {
                        enableSpotlightPromotionModal: benefitHomePageData.IFIsBankName != "HDFC",
                        enableMopinion: true,
                        showTaxStatus: true,
                        documentQuoteText: "Your employees can now claim everything from medical and fuel benefits to meal vouchers – instantly and with zero paperwork. Save tax the easy way with Zeta."
                    }

                    if (req.session[Constant.SESSION_CONFIG_DATA]) {
                        const config_data = Util.getDataFromObject(req.session[Constant.SESSION_CONFIG_DATA], 'benefitCtrl.init.benefitHomePageData.templateData');
                        if (config_data) {
                            benefitHomePageData.templateData = config_data;
                        }
                    }

                    res.render('benefit', benefitHomePageData);
                    res.end();
                }).catch(function (error) {
                    res.locals.showErrorWitHdr = true;
                    Util.errorFoundHandler(error, req, res, next);
                });
            });
        }, function(err){
            Util.errorFoundHandler(err, req, res, next);
        });
    }, function (respErr) {
        console.log(respErr);
        if (respErr && respErr.statusCode === 500 && respErr.body.type === 'UnauthorizedException') {
            console.log('Unauthorised to access the corporate');
            res.status(401).render('unauthorised', {
                title: 'Zeta corpotate'
            });
        } else {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(respErr, req, res, next);
        }

    });
};

let _getBenefitDetails = function (req, respData) {
    let displayTag = false;
    respData = JSONbig.parse(respData);
    respData.compareDates = Util.compareLastIssuanceToCurrentDate(respData.cardProgramObject.issuanceConfiguration.lastIssuanceDate);
    req.session["compareDates"] = respData.compareDates;
    let compareTenDaysBefore = Util.compareTenDaysBefore(respData.cardProgramObject.issuanceConfiguration.lastIssuanceDate);
    if (respData.cardProgramObject.issuanceConfiguration.lastIssuanceDate && !Util.compareProductTypeToPrograms(respData.cardProgramObject.productType)) {
        var issuanceConfiguration = respData.cardProgramObject.issuanceConfiguration;
        var reimbursementProgram = respData.cardProgramObject.reimbursementProgram;
        respData.cardProgramObject.issuanceConfiguration.lastIssuanceDate = Util.timestampToDate(issuanceConfiguration.lastIssuanceDate);
        respData.cardProgramObject.reimbursementProgram.lastClaimUploadDate = Util.timestampToDate(reimbursementProgram.lastClaimUploadDate);
        respData.cardProgramObject.reimbursementProgram.lastClaimProcessDate = Util.timestampToDate(reimbursementProgram.lastClaimProcessDate);
        if (respData.cardProgramObject.closeCardConfiguration && respData.cardProgramObject.closeCardConfiguration.closeCardDate) {
            respData.cardProgramObject.closeCardConfiguration.closeCardDate = Util.timestampToDate(respData.cardProgramObject.closeCardConfiguration.closeCardDate);
        }
        if (issuanceConfiguration && issuanceConfiguration.extraIssuanceConfig) {
            respData.cardProgramObject.issuanceConfiguration.extraIssuanceConfig["corpben.reportGenerationDate"] = Util.timestampToDate(issuanceConfiguration.extraIssuanceConfig["corpben.reportGenerationDate"]);
        }
    }
    respData.cardProgramObject.voucherProgram.voucherValidity = Constant.VOUCHER_VALIDITY[respData.cardProgramObject.voucherProgram.voucherValidity];
    if (respData.cardProgramObject.productType == 'Meal') {
        respData.cardProgramObject['leftColor'] = Constant.MEAL_LEFT_COLOR;
        respData.cardProgramObject['rightColor'] = Constant.MEAL_RIGHT_COLOR;
    } else if (respData.cardProgramObject.productType == 'Gift') {
        respData.cardProgramObject['leftColor'] = Constant.GIFT_LEFT_COLOR;
        respData.cardProgramObject['rightColor'] = Constant.GIFT_RIGHT_COLOR;
    } else if (respData.cardProgramObject.productType == 'Fuel') {
        respData.cardProgramObject['leftColor'] = Constant.FUEL_LEFT_COLOR;
        respData.cardProgramObject['rightColor'] = Constant.FUEL_RIGHT_COLOR;
    } else if (respData.cardProgramObject.productType == 'Medical') {
        respData.cardProgramObject['leftColor'] = Constant.MEDICAL_LEFT_COLOR;
        respData.cardProgramObject['rightColor'] = Constant.MEDICAL_RIGHT_COLOR;
    } else if (respData.cardProgramObject.productType == 'Communication') {
        respData.cardProgramObject['leftColor'] = Constant.COMMUNICATION_LEFT_COLOR;
        respData.cardProgramObject['rightColor'] = Constant.COMMUNICATION_RIGHT_COLOR;
    } else if (respData.cardProgramObject.productType == 'Asset') {
        respData.cardProgramObject['leftColor'] = Constant.ASSET_LEFT_COLOR;
        respData.cardProgramObject['rightColor'] = Constant.ASSET_RIGHT_COLOR;
    } else {
        respData.cardProgramObject['leftColor'] = Constant.DEFAULT_LEFT_COLOR;
        respData.cardProgramObject['rightColor'] = Constant.DEFAULT_RIGHT_COLOR;
    }
    respData.cardProgramObject.taxStatus = Constant.CARDS_TAX_STATUS[respData.cardProgramObject.productType.toLowerCase()];

    if (respData.totalPayoutsAmount) {
        if (respData.totalPayoutsAmount.amount) {
            respData.totalPayoutsAmount.amount = Util.formatINR(Math.round(respData.totalPayoutsAmount.amount / 100));
        }
    }
    if (respData.totalSpentAmount) {
        if (respData.totalSpentAmount.amount) {
            respData.totalSpentAmount.amount = Util.formatINR(Math.round(respData.totalSpentAmount.amount / 100));
        }
    }
    if (respData.reimbursementProgramDetails && respData.reimbursementProgramDetails.totalApprovedAmount) {
        if (respData.reimbursementProgramDetails.totalApprovedAmount.amount) {
            respData.reimbursementProgramDetails.totalApprovedAmount.amount = Util.formatINR(Math.round(respData.reimbursementProgramDetails.totalApprovedAmount.amount / 100));
        }
    }
    if (respData.fundingAccountDetails && (respData.fundingAccountDetails.balance || respData.fundingAccountDetails.balance === 0)) {
        respData.fundingAccountDetails.formattedBalance = Util.formatCurrency(respData.fundingAccountDetails.balance / 100, respData.fundingAccountDetails.currency);
        respData.fundingAccountDetails.balance = Util.formatINRWithPaise(respData.fundingAccountDetails.balance / 100);
    }
    if (respData.numberOfTransactions) {
        respData.numberOfTransactions = Util.formatNumber(respData.numberOfTransactions);
    }

    if (respData.fundingAccountDetails.transferInDetails) {
        if (respData.fundingAccountDetails && respData.fundingAccountDetails.transferInDetails.length > 0) {
            respData.fundingAccountDetails.accountNumber = Util.maskAccountNumber(respData.fundingAccountDetails.transferInDetails[0].accountNumber);
        }
    } else {
        respData.fundingAccountDetails.accountNumber = '';
    }

    if (compareTenDaysBefore) {
        displayTag = true;
    }
    respData.cardProgramObject['fundingAccountId'] = respData.fundingAccountDetails.accountID

    respData.displayTag = displayTag;

    return respData;
};

let getBenefitDetails = function (req, res, next) {
    res.locals.pageName = 'optima';
    res.locals.currPage = 'benefitDetails';
    let programID = req.params.cardProgramId;
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let programSetupEnabled = UtilsUser.checkGKFlag(req, res.locals.selectedCompany.company.companyID, Constant.PROGRAM_SETUP_ENABLED)
    let checkGenie = 0;
    let createTransferOrder = true;
    let changeFundingAcct = true;
    let rbac = res.locals.rbac
    if(rbac){
        createTransferOrder = Util.checkPriviledge(rbac, 'updateCardProgram', companyID);
        if(Util.isProgramManagerRead(rbac, companyID) || Util.isProgramManagerWrite(rbac, companyID)){
            changeFundingAcct = false;
        }
    } 

    if (req.session.genie) {
        checkGenie = req.session.genie;
    }
    let IFIName = req.session.IFIS_NAME;
    if(IFIName == 'Sodexo_Vietnam' || IFIName == "Sodexo_Philippines"){
        programSetupEnabled = false;
    }
    setSelectedCompany(req, res);
    BenefitSvc.getDetails(authToken, corpID, companyID, programID).then(function (respData) {
        respData = respData.replace(/&amp;/g, "&");
        var parsedData = JSON.parse(respData);
        if(parsedData.cardProgramObject.productType == 'OTmeal' || parsedData.cardProgramObject.productType == 'sodexo_cafeteria'){
            programSetupEnabled = true;
        }
        if (programSetupEnabled) {
            var timeSlice = parsedData.cardProgramObject.spendConfiguration.allowedTimeSlices;
            var productType = parsedData.cardProgramObject.productType
            var allowedTimeSlice = [];
            var selectedSpendDays = [];
            if (timeSlice && timeSlice.length > 0) {
                timeSlice.forEach(function (timeString) {
                    allowedTimeSlice.push(timeString.split(" ")[2])
                })
                if (timeSlice[0].split(" ")[5] == "1-7" || timeSlice[0].split(" ")[5] == "*") {
                    selectedSpendDays = Constant.DAYS
                } else if (timeSlice[0].split(" ")[5]) {
                    selectedSpendDays = timeSlice[0].split(" ")[5].split(",");
                }
            }
            var allowRules = parsedData.cardProgramObject.reimbursementProgram.extraReimbursementProgramConfig ? parsedData.cardProgramObject.reimbursementProgram.extraReimbursementProgramConfig.allowRules || [] : [],
                requireRules = parsedData.cardProgramObject.reimbursementProgram.extraReimbursementProgramConfig ? parsedData.cardProgramObject.reimbursementProgram.extraReimbursementProgramConfig.requireRules || [] : [];
        }
        respData = _getBenefitDetails(req, respData);
        respData['programSetupEnabled'] = programSetupEnabled
        respData['fundingAccounts'] = req.session["fundingAccounts"]
        respData['checkGenie'] = checkGenie
        respData['createTransferOrder'] = createTransferOrder
        respData['changeFundingAcct'] = changeFundingAcct
        if (programSetupEnabled) {
            respData['allowedTimeSlice'] = allowedTimeSlice
            respData['selectedSpendDays'] = selectedSpendDays
            respData['FUND_MOVEMENT_BEHAVIOUR'] = Constant.FUND_MOVEMENT_BEHAVIOUR;
        }

        if (parsedData.cardProgramObject){

        }

        respData.corpID = corpID;
        let getFundingAccountName = respData['fundingAccounts'] || [];
        let allFundingAccountName = [];
        for (var m = 0; m < getFundingAccountName.length; m++) {
            respData['fundingAccounts'][m].formattedBalance = Util.formatCurrency(respData['fundingAccounts'][m].balance / 100, respData['fundingAccounts'][m].currency)
            allFundingAccountName.push(getFundingAccountName[m].name);
        }
        respData["allFundingAccountName"] = allFundingAccountName;
        if (programSetupEnabled) {
            respData['allRules'] = parsedData.cardProgramObject.reimbursementProgram.extraReimbursementProgramConfig ? parsedData.cardProgramObject.reimbursementProgram.extraReimbursementProgramConfig.allowRules || [] : [];
            respData['allRules'].push(...parsedData.cardProgramObject.reimbursementProgram.extraReimbursementProgramConfig ? parsedData.cardProgramObject.reimbursementProgram.extraReimbursementProgramConfig.requireRules || [] : []);
            respData['uniformCategories'] = programStaticData.uniformCategories;
            respData['booksCategories'] = programStaticData.booksCategories;
            respData['expiryDropdownData'] = programStaticData.expiryDropdownData;
        }
        req.session['benefitDetails'] = respData;

        var closeCardConfiguration = parsedData.fundingAccountDetails.programsFunded.filter(program => program.id == programID);

        respData['closeCardConfiguration'] = closeCardConfiguration;

        respData.templateData = {
            overview: {
                showCardProgramLogo: false,
                enableCardLogoInput: false,
                showTaxAndExpiry: true
            }
        }
        if (req.session[Constant.SESSION_CONFIG_DATA]) {
            const config_data = Util.getDataFromObject(req.session[Constant.SESSION_CONFIG_DATA], 'benefitCtrl.getBenefitDetails.templateData');
            if (config_data) {
                respData.templateData = config_data;
            }
        }

        if (programSetupEnabled) {
            var finalRuleList = [];
            allowRules.forEach(function (item) {
                finalRuleList.push(item.rule)
            })
            requireRules.forEach(function (item) {
                finalRuleList.push(item.rule)
            })
            if (finalRuleList.length > 0) {
                BenefitSvc.getChecklistByRules(authToken, finalRuleList).then(function (checkLists) {
                    respData['checkListMap'] = checkLists.billChecklistMap
                    res.render('benefitDetails', respData);
                    res.end();
                }, function (err) {
                    respData['checkListMap'] = {}
                    res.render('benefitDetails', respData);
                    res.end();
                })
            } else {
                respData['checkListMap'] = {}
                res.render('benefitDetails', respData);
                res.end();
            }
        } else {
            respData['checkListMap'] = {}
            res.render('benefitDetails', respData);
            res.end();
        }
    }, function (respErr) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);
    });
};

let getTransfers = function (req, res, next) {
    res.locals.pageName = 'benefits';
    res.locals.currPage = 'newTransfer';
    let programID = req.params.cardProgramId;
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let revokeFunds = true
    let rbac = res.locals.rbac
    if(rbac){
        revokeFunds = Util.checkPriviledge(rbac, 'updateCardProgram', companyID);
    } 
    let isProgramManagerRW = Util.isProgramManagerRead(rbac, companyID) || Util.isProgramManagerWrite(rbac, companyID) ? true : false
    BenefitSvc.getDetails(authToken, corpID, companyID, programID).then(function (respData) {
        respData = _getBenefitDetails(req, respData);
        req.session['benefitDetails'] = respData;
        let currMonth = Util.timestampToMonthYear(parseInt(req.query.startDate));
        let benefitDetails = respData;
        let companyID = req.params.companyId;
        let authToken = req.session[Constant.SESSION_AUTH].authToken;
        setSelectedCompany(req, res);
        let benefitHomePageData = {
            "benefitDetails": benefitDetails,
            "currMonth": currMonth,
            "companyID": companyID,
            "programID": programID,
            "corpID": corpID,
            "revokeFunds":revokeFunds,
            "isProgramManagerRW": isProgramManagerRW
        };
        res.render('transferDetail', benefitHomePageData);
        res.end();
    }, function (respErr) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);
    });
};

let getOrderTransfers = function (req, res, next) {
    let orderID = req.params["orderId"];
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let programID = req.params.cardProgramId;
    let apiKey = req.session[Constant.SESSION_AUTH].authToken;
    let revokeFunds = true
    let rbac = res.locals.rbac
    if(rbac){
        revokeFunds = Util.checkPriviledge(rbac, 'updateCardProgram', companyID);
    } 
    let isProgramManagerRW = Util.isProgramManagerRead(rbac, companyID) || Util.isProgramManagerWrite(rbac, companyID) ? true : false
    setSelectedCompany(req, res);
    BenefitSvc.getDetails(apiKey, corpID, companyID, programID).then(function (respData) {
        respData = _getBenefitDetails(req, respData);
        res.locals.corpID = corpID;
        req.session['benefitDetails'] = respData;
        res.locals.pageName = 'benefits';
        res.locals.currPage = 'newTransfer';
        res.locals.benefitDetails = req.session.benefitDetails;
        res.locals.companyID = companyID;
        res.render('transferDetail', { orderID: orderID,revokeFunds:revokeFunds, isProgramManagerRW: isProgramManagerRW});
    }, function (respErr) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);
    });
};

let getOrderErrors = function (req, res, next) {
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let orderID = req.params.orderId;
    let programID = req.params.cardProgramId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let downloadURL = (Config.corpBaseUrl + Urls['DOWNLOAD_PAYOUT_REPORT']).replace(':authToken:', authToken).replace(':corpID:', corpID).replace(':orderID:', orderID);
    let pageData = {
        companyID: companyID,
        orderID: orderID,
        programID: programID,
        downloadURL: downloadURL
    };
    res.locals.pageName = 'benefits';
    res.locals.currPage = 'newTransfer';
    res.locals.benefitDetails = req.session.benefitDetails;
    setSelectedCompany(req, res);
    res.render('newTransferErrors', pageData);
    res.end();
};


let getNewTransfer = function (req, res, next) {
    let IFIName = req.session.IFIS_NAME;
    res.locals.pageName = 'benefits';
    res.locals.currPage = 'newTransfer';
    res.locals.benefitDetails = req.session.benefitDetails;
    let companyID = req.params.companyId;
    let rbac = res.locals.rbac
    let newTransfer = true
    if(rbac){
        newTransfer = Util.checkPriviledge(rbac, 'updateCardProgram', res.locals.selectedCompany.company.companyID);
    } 

    let isSodexoPhp = false;

    if(IFIName == 'Sodexo_Philippines'){
        isSodexoPhp = true;
    }

    

    setSelectedCompany(req, res);
    let productType = req.session.benefitDetails.cardProgramObject.productType.toLowerCase();
    productType = Constant.PROGRAM_TRANSFER_ENUM[productType] || productType;
    let corpID = req.session[Constant.SESSION_CORP_INFO].id;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let programID = req.params.cardProgramId;
    let programSetupEnabled = UtilsUser.checkGKFlag(req, res.locals.selectedCompany.company.companyID, Constant.PROGRAM_SETUP_ENABLED)

    const promisesList = [
        BenefitSvc.getCorpCardProgram(authToken, corpID, companyID, programID),
        BenefitSvc.getFileUploadDetails(authToken, corpID)
    ];
    Promise.all(promisesList).then(respData => {
        let blockDates = JSON.stringify(respData[0].disallowedPayoutPeriodList || []);

        let fileUploadUrl = JSON.parse(respData[1]).url;
        let pageData = {
            productType: productType,
            previewImage: Constant.TRANSFER_PREVIEW_IMAGES[productType],
            excelFileDownload: Constant.TRANSFER_EXCEL_DOWNLOAD_FILE[productType],
            companyID: companyID,
            fileUploadDetails: respData[1],
            fileUploadUrl: fileUploadUrl,
            blockDates: blockDates,
            corpID: corpID,
            programSetupEnabled: programSetupEnabled,
            newTransfer: newTransfer,
            isSodexoPhp: isSodexoPhp,
            IFIName: IFIName
        };

        if (req.session[Constant.SESSION_CONFIG_DATA]) {
            const config_data = Util.getDataFromObject(req.session[Constant.SESSION_CONFIG_DATA], 'benefitCtrl.newTransfer');
            if (config_data) {
                if (config_data.TRANSFER_PREVIEW_IMAGES) {
                    pageData.previewImage = config_data.TRANSFER_PREVIEW_IMAGES[productType];
                }
                if (config_data.TRANSFER_EXCEL_DOWNLOAD_FILE) {
                    pageData.excelFileDownload = config_data.TRANSFER_EXCEL_DOWNLOAD_FILE[productType];
                }
            }
        }

        if(IFIName == "HDFC"){
            pageData.previewImage = pageData.previewImage.replace('previews', 'previews/HDFC')
            pageData.excelFileDownload = pageData.excelFileDownload.replace('download-samples', 'download-samples/HDFC')
        }
        res.render('newTransfer', pageData);
        res.end();
    }, function (respErr) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);
    });

};

let addPayoutTransfer = function(req, res, next) {
    res.locals.pageName = 'benefits';
    res.locals.currPage = 'newTransfer';
    res.locals.benefitDetails = req.session.benefitDetails;
    let companyID = req.params.companyId;
    let orderId = req.params.orderId;
    let payoutStartDate = req.query.payoutStartDate;
    let payoutEndDate = req.query.payoutEndDate;
    let poNumber = req.query.poNumber;
    let status = req.query.status;
    let payoutDate = req.query.payoutDate;
    let totalAmount= req.query.totalAmount;
    let recurringOrderID = req.query.recurringOrderID;

    console.log('req.params',req.params, req);

    setSelectedCompany(req, res);
    let productType = req.session.benefitDetails.cardProgramObject.productType.toLowerCase();
    productType = Constant.PROGRAM_TRANSFER_ENUM[productType] || productType;
    let corpID = req.session[Constant.SESSION_CORP_INFO].id;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let programID = req.params.cardProgramId;

    const promisesList = [
        BenefitSvc.getCorpCardProgram(authToken, corpID, companyID, programID),
        BenefitSvc.getFileUploadDetails(authToken, corpID)
    ];
    Promise.all(promisesList).then(respData => {
        let blockDates = JSON.stringify(respData[0].disallowedPayoutPeriodList || []);

        let fileUploadUrl = JSON.parse(respData[1]).url;
        let pageData = {
            productType: productType,
            previewImage: Constant.TRANSFER_PREVIEW_IMAGES[productType],
            excelFileDownload: Constant.TRANSFER_EXCEL_DOWNLOAD_FILE[productType],
            companyID: companyID,
            fileUploadDetails: respData[1],
            fileUploadUrl: fileUploadUrl,
            blockDates: blockDates,
            corpID:corpID,
            orderId: orderId,
            payoutStartDate: payoutStartDate,
            payoutEndDate: payoutEndDate,
            poNumber: poNumber,
            status: status,
            payoutDate: payoutDate,
            totalAmount: totalAmount,
            recurringOrderID : recurringOrderID

        };
        res.render('addPayoutTransfer', pageData);
        res.end();
    }, function(respErr) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);
    });

};

let setSelectedCompany = function (req, res) {
    let companyID = req.params.companyId;
    let companiesInfo = req.session[Constant.SESSION_COMP_INFO];
    req.session['selectedProgram'] = "optima";
    for (var i = 0; i < companiesInfo.length; i++) {
        if (companyID == companiesInfo[i].company.companyID) {
            req.session[Constant.SESSION_SELECTED_COMP_INFO] = companiesInfo[i];
            res.locals.selectedCompany = companiesInfo[i];
        }
    }
};

let createCustomProgram = function (req, res, next) {
    let companyID = req.params.companyId;
    let corpID = req.session[Constant.SESSION_CORP_INFO].id;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let pageData = {
        companyID: companyID,
        iframeUrl: 'https://card-program-stage.zetaapps.in/#/home?corpId=' + corpID + '&companyID=' + companyID + '&from=https://corporates-stage.zetaapps.in'
    };
    res.locals.pageName = 'benefits';
    res.locals.currPage = 'createCustomProgram';
    res.render('createCustomProgram', pageData);
    res.end();
};

let getBenefitPrograms = function (req, res) {
    let companyID = req.params.companyId;
    let corpID = req.session[Constant.SESSION_CORP_INFO].id;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    BenefitSvc.getClosedCards(authToken, corpID, companyID).then(function (closedCards) {
        closedCards = closedCards;
        let totalClosedProgramPages = closedCards.totalRequestCount;
        let closedPrograms = Util.sortArrayOfObject(closedCards.programs, "cardProgramObject.cardName");
        res.render('section/benefits/closed-cards', {
            closedCards: closedPrograms,
            companyID: companyID,
            totalClosedProgramPages: Math.ceil(totalClosedProgramPages / 12)
        });
        res.end();
    }, function (error) {
        res.render("error", {
            error: error,
            title: "",
            message: ""
        });
        res.end();
    });
};

let getBenefitMoreClosedPrograms = function (req, res) {
    let companyID = req.params.companyId;
    let pageNumber = req.params.pageNumber;
    let corpID = req.session[Constant.SESSION_CORP_INFO].id;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    BenefitSvc.getMoreClosedCardsPost(authToken, corpID, companyID, pageNumber).then(function (closedCards) {
        closedCards = JSON.parse(closedCards);
        let closedPrograms = Util.sortArrayOfObject(closedCards.programs, "cardProgramObject.cardName");
        res.render('section/benefits/closed-cards-more', {
            closedCards: closedPrograms,
            companyID: companyID
        });
        res.end();
    }, function (error) {
        res.render("error", {
            error: error,
            title: "",
            message: ""
        });
        res.end();
    });
};

let getBenefitMoreActivePrograms = function (req, res) {
    let companyID = req.params.companyId;
    let pageNumber = req.params.pageNumber;
    let corpID = req.session[Constant.SESSION_CORP_INFO].id;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    BenefitSvc.getMoreActiveCardsPost(authToken, corpID, companyID, pageNumber).then(function (data) {
        let data1 = data;
        let activeCardData = data1;
        let allRecommondedCardPrograms = RecommonedProgramsModel.init();
        let recommondedCardPrograms,
            programs = activeCardData.programs,
            programsCount = programs.length,
            newProgramSet = [],
            giftPrograms = [],
            allCardsSchedule = [],
            productTypeFilter = [],
            displayBanner = false,
            lastIssuanceDate,
            displayBannerDate = UtilsUser.checkGKFlag(req, res.locals.selectedCompany.company.companyID, Constant.YEAR_END_CLOSURE_GK_FLAG),
            displayTag = false;
        for (let i = 0; i < programsCount; i++) {
            if (programs[i].corpProductType === 'OPTIMA' && programs[i].cardProgramObject.productType.toLowerCase() !== 'incentive' && programs[i].cardProgramObject.productType.toLowerCase() !== 'cashless_cafeteria') {
                let productType = programs[i].cardProgramObject.productType.toLowerCase();
                productTypeFilter[i] = programs[i].cardProgramObject.productType;
                let cardName = programs[i].cardProgramObject.cardName;
                programs[i].cardProgramObject.taxStatus = Constant.CARDS_TAX_STATUS[productType];
                let compareProductTypeToPrograms = Util.compareProductTypeToPrograms(programs[i].cardProgramObject.productType);

                if (!compareProductTypeToPrograms) {
                    allCardsSchedule.push(programs[i]);
                }

                if (productType === 'gift' && !cardName) {
                    programs[i].cardProgramObject.cardName = 'Gift Vouchers';
                }


                if (programs[i].cardProgramObject.issuanceConfiguration && programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate) {
                    lastIssuanceDate = new Date(programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate);
                } else {
                    lastIssuanceDate = '';
                }
                if (Util.compareLastIssuanceToCurrentDate(lastIssuanceDate) || programs[i].status === 'CLOSED') {
                    // closedCards.push(programs[i]);
                } else {
                    if (productType === 'gift') {
                        giftPrograms.push(programs[i]);
                    } else {
                        newProgramSet.push(programs[i]);
                    }
                    if (programs[i].cardProgramObject.issuanceConfiguration && programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate && programs[i].cardProgramObject.issuanceConfiguration.extraIssuanceConfig) {
                        let compareTenDaysBefore = Util.compareTenDaysBefore(programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate);
                        if ( /*Util.displayBanner(Constant.DISPLAY_BANNER_DATE)*/ displayBannerDate && !compareProductTypeToPrograms && !programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate && programs[i].cardProgramObject.issuanceConfiguration.extraIssuanceConfig["corpben.financialYear"] === '2016-17') {
                            displayBanner = true;
                        }

                        if (compareTenDaysBefore && !compareProductTypeToPrograms) {
                            displayTag = true;
                        }
                    }
                }
            }

        }
        activeCardData.programs = newProgramSet.concat(giftPrograms);
        activeCardData.programs = Util.sortArrayOfObject(activeCardData.programs, "cardProgramObject.cardName");
        res.render('section/benefits/active-cards-more', {
            activeCards: activeCardData,
            companyID: companyID,
            "displayTag": displayTag
        });
        res.end();
    }, function (error) {
        res.render("error", {
            error: error,
            title: "",
            message: ""
        });
        res.end();
    });
};


let getSuperCardOrder = function (req, res, next) {
    res.locals.pageName = 'optima';
    res.locals.currPage = 'superCard';
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let benefitHomePageData = {};
    // setSelectedCompany(req, res);
    const promisesList = [
        BenefitSvc.getCardorderTemplate(authToken, corpID, companyID),
        AdminSvc.getFundingAcc(authToken, corpID, 1, 10),
        BenefitSvc.getCorpAddress(authToken, corpID),
        BenefitSvc.getFileUploadDetails(authToken, corpID)
    ];
    Promise.all(promisesList).then(respData => {
        // card template
        let cardorderTemplate = JSON.parse(respData[0]).cards;
        //fileupload url
        let fileUploadUrl = JSON.parse(respData[3]).postAction;
        // company address
        let addressData = JSON.parse(respData[2]);
        let companyAddress = addressData.companySummaries.filter((data) => data.company.companyID == companyID);
        //"address": companyAddress[0].company,
        //fundingAccount
        //let fundResData = respData[1];
        benefitHomePageData["fundingAccounts"] = JSON.parse(respData[1]);
        let totalFundCount = benefitHomePageData["fundingAccounts"].count;
        let pageNum = 2;
        let totalPages = Math.ceil(totalFundCount / 10);
        let fundingAccPromise = [];
        for (pageNum; pageNum < totalPages + 1; pageNum++) {
            fundingAccPromise.push(AdminSvc.getFundingAcc(authToken, corpID, pageNum, 10));
        }
        Promise.all(fundingAccPromise).then(function (respArr) {
            //console.log('this is response', respArr);
            respArr.forEach(function (fundResData) {
                let fundAccDet = JSON.parse(fundResData);
                fundAccDet.fundingAccounts.forEach(function (accDetails) {
                    benefitHomePageData["fundingAccounts"].fundingAccounts.push(accDetails);
                });
            });
            req.session["fundingAccounts"] = benefitHomePageData["fundingAccounts"].fundingAccounts;

            let fundingPageData = {
                "fundingAccounts": req.session["fundingAccounts"]
            };
            benefitHomePageData["fundingAccounts"] = fundingPageData;
            fundingAccountDetails = fundingPageData;

            let cardorderData = {
                "cardorderTemplate": cardorderTemplate,
                "fileUploadDetails": respData[3],
                "fileUploadUrl": fileUploadUrl,
                "fundingAccountDetails": fundingAccountDetails,
                "address": companyAddress[0].company,
                "corpID": corpID,
                "companyID": companyID,
                "authToken": authToken,
            };
            console.log(cardorderData);
            res.render('section/supercard/superCard', cardorderData);
            res.end();
        }, function (respErr) {
            console.log("respErr", respErr);
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(respErr, req, res, next);
        });

    }, function (respErr) {
        console.log("respErr", respErr);
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);

    });
};

let getSuperCardOrderDetails = function (req, res, next) {
    res.locals.pageName = 'optima';
    res.locals.currPage = 'superCard';
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let superCardID = req.params.superCardID;
    BenefitSvc.getSuperCardDetails(authToken, corpID, companyID, superCardID).then(function (response) {
        let responseObject = {
            'companyID': companyID,
            'cardMaster': response.cardMaster,
            'superCard': response.superCard,
            'superCardOrder': response.superCardOrder
        };
        console.log(responseObject);
        res.render('section/supercard/index', responseObject);
        res.end();
    }, function (error) {
        console.log("error", error);
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(error, req, res, next);
    });


};
let setupProgram = function (req, res, next) {
    let companyID = req.params.companyId;
    var productType = req.params.productType
    res.locals.pageName = 'optima';
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let checkGenie = 0;
    if (req.session.genie) {
        res.redirect('/');
        checkGenie = req.session.genie;
    }
    if (req._parsedUrl.query == null || !req._parsedUrl.query.split('=')[1]) {
        res.render("notFound", {});
        res.end();
    } else {
        var allowedMerchants = programStaticData[productType].cardProgram.spendConfiguration.allowedMerchants || []
        var disallowedMerchants = programStaticData[productType].cardProgram.spendConfiguration.disallowedMerchants || []
        var programData = {
            companyID: companyID,
            cardName: decodeURIComponent(req._parsedUrl.query.split('=')[1]),
            fundingAccounts: req.session.fundingAccounts,
            programConfig: programStaticData[productType],
            booksCategories: programStaticData.booksCategories,
            uniformCategories: programStaticData.uniformCategories,
            allMerchants: allowedMerchants.concat(disallowedMerchants),
            productType: productType,
            expiryDropdownData: programStaticData.expiryDropdownData,
            bussinessId: Util.getCompBussinessId(req.session.comp_info, req.session.selected_comp_info.company.companyID),
            checkGenie: checkGenie
        }
        var allowRules = programStaticData[productType].cardProgram.reimbursementProgram.extraReimbursementProgramConfig.allowRules || [],
            requireRules = programStaticData[productType].cardProgram.reimbursementProgram.extraReimbursementProgramConfig.requireRules || [];

        var finalRuleList = [];
        allowRules.forEach(function (item) {
            finalRuleList.push(item.rule)
        })
        requireRules.forEach(function (item) {
            finalRuleList.push(item.rule)
        })
        if (finalRuleList.length > 0) {
            BenefitSvc.getChecklistByRules(authToken, finalRuleList).then(function (checkLists) {
                programData['checkLists'] = checkLists.billChecklistMap
                res.render("programSetup", programData);
                res.end();
            }, function (err) {
                console.log('Errrror', err)
                res.render("errorFound", {});
                res.end();
            })
        } else {
            programData['checkLists'] = {}
            res.render("programSetup", programData);
            res.end();
        }
    }
};

module.exports = {
    init: init,
    getBenefitMoreActivePrograms: getBenefitMoreActivePrograms,
    getBenefitMoreClosedPrograms: getBenefitMoreClosedPrograms,
    getBenefitDetails: getBenefitDetails,
    getTransfers: getTransfers,
    getNewTransfer: getNewTransfer,
    getOrderTransfers: getOrderTransfers,
    setSelectedCompany: setSelectedCompany,
    createCustomProgram: createCustomProgram,
    getBenefitPrograms: getBenefitPrograms,
    getOrderErrors: getOrderErrors,
    getSuperCardOrder: getSuperCardOrder,
    getSuperCardOrderDetails: getSuperCardOrderDetails,
    setupProgram: setupProgram,
    addPayoutTransfer: addPayoutTransfer
};
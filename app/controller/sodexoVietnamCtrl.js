Constant = require('../common/constant');
Env = process.env.NODE_ENV || 'development_stage';
const {COUNTRY_CONFIGS} = require('../common/countryConfig/configMappings');

const BASE_TOOL_URL = {
    'development_stage': 'https://report-center-stage.zetaapps.in',
    'development_production': 'https://report-center.zetaapps.in',
    'development_preprod': 'https://report-center-pp.zetaapps.in',
    'stage': 'https://report-center-stage.zetaapps.in',
    'production': 'https://report-center.zetaapps.in',
    'preprod': 'https://report-center-pp.zetaapps.in'
}

var init = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    res.locals.pageName = 'vn-report-center';

    var ifi = COUNTRY_CONFIGS.SODEXO_VIETNAM_CONFIGS.PLATFORM_IFI[Env];
    var catalogId = COUNTRY_CONFIGS.SODEXO_VIETNAM_CONFIGS.CATALOG_ROOT_ID[Env];
    var corpID = res.locals.corp.id;
    var url = BASE_TOOL_URL[Env]+"/?header=false&mode=standalone&catalogRootId="+catalogId+"&ifi="+ifi+"&corpId="+corpID+"&authToken="+authToken+"#/home"
    res.render('section/sodexo-vietnam/report-center', { url });
    res.end();
}

module.exports = {
    init: init
}
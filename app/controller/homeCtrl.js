var Constant = require('../common/constant');
var AuthSvc = require('../service/authSvc');
var Config = require('../../config');
var Endpoint = require('../common/url');
var Util = require('../common/util');
var ServiceConnector = require('../common/serviceConnector');
var reCAPTCHA = require('recaptcha2')
const env = process.env.NODE_ENV || 'development_stage';
const Countries = require('country-list');
const {COUNTRY_CONFIGS} = require('../common/countryConfig/configMappings');


let logout = function (req, res, next) {
  let ssoUrl = Config.ssoUrl; let queryParams;
  if (!req.query.redirectUrl) {
    console.log('no query', req.protocol);
    req.session.destroy();
    queryParams = '?redirectUrl=' + 'https://' + req.headers.host + '/dashboard';
  } else {
    console.log("query", req.query.redirectUrl);
    queryParams = '?redirectUrl=' + 'https://' + req.headers.host + (req.query.redirectUrl || '/dashboard');
  }
  if (req.headers.host.indexOf('smdigitalportal') != -1) {
    ssoUrl = Config.ssoPhUrl || COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.SSO_URL[env];
    let ifi = COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.PLATFORM_IFI[env];
    queryParams += '&appID=' + COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.APP_ID + '&ifi=' + ifi;
  } else if (req.headers.host.indexOf('sodexo.vn') != -1 || req.headers.host.indexOf('sodexovn') != -1 || Util.getUrlParameter('ifi', req.originalUrl) == COUNTRY_CONFIGS.SODEXO_VIETNAM_CONFIGS.PLATFORM_IFI[env]) {
    queryParams += '&countryCode=' + COUNTRY_CONFIGS.SODEXO_VIETNAM_CONFIGS.COUNTRY_CODE + '&ifi=' + COUNTRY_CONFIGS.SODEXO_VIETNAM_CONFIGS.PLATFORM_IFI[env];
  }
  res.redirect(ssoUrl + 'logout' + queryParams);
};

let signUpCorp = function (req, res, next) {
  let data = req.body;

  recaptcha = new reCAPTCHA({
    siteKey: '6Lecc0sUAAAAAMzYRmclMp0gXWZ66pOaRx-6yWSx',
    secretKey: ' 6Lecc0sUAAAAAJJ5on7q3nHAwQM8DrKMRbgomTGi'
  })

  recaptcha.validate(data.gRecaptchaResponse)
    .then(function (response) {
      delete data.response;
      console.log(data);
      AuthSvc.signUpCorp(data).then(respData => {
        // console.log(response);
        res.status(200);
        res.send(respData);
      }, function (error) {
        res.status(error.statusCode || 500);
        res.send(error.body);
      })
    })
    .catch(function (errorCodes) {
      let error = {
          body:{
              message: 'Invalid Captcha'
          }
      };
      console.log(recaptcha.translateErrors(errorCodes)); // translate error codes to human readable text
      res.status(error.statusCode || 500);
      res.send(error.body);
    });
}

// let renew = function (req, res, next) {
//     console.log('homectrl',req.session.redirect);
//     if (req.session.redirect) {
//         res.redirect(Config.ssoUrl + '?redirectUrl=' + 'https://' + req.headers.host + req.session.redirect);
//     } 
//     else {
//         res.redirect(Config.ssoUrl + 'logout?redirectUrl=' + 'https://' + req.headers.host + '/'+ (req.query.redirectUrl || 'dashboard'));
//     }
// };


let verifyEmail = function (req, res, next) {
  res.redirect(Config.ssoUrl + '?redirectUrl=' + 'https://' + req.headers.host + '/dashboard'); 
  /*var errors = req.flash('errors');
  res.render('loginLanding', {
      errors: errors
  });
  res.end();*/
};

let redirectToSSO = function (req, res, next) {
  var queryParams = '?redirectUrl=' + 'https://' + req.headers.host + '/dashboard'
  if (req.originalUrl.indexOf('/login/vn') > -1) {
    queryParams = '?redirectUrl=' +  COUNTRY_CONFIGS.SODEXO_VIETNAM_CONFIGS.URL[env];
    queryParams += '&countryCode=' + COUNTRY_CONFIGS.SODEXO_VIETNAM_CONFIGS.COUNTRY_CODE + '&ifi=' + COUNTRY_CONFIGS.SODEXO_VIETNAM_CONFIGS.PLATFORM_IFI[env];
  }
  res.redirect(Config.ssoUrl + queryParams);
};

let signUp = function (req, res, next) {
  res.render('signup');
  res.end();
};

let hdfcSignup = function (req, res, next) {
  res.render('hdfcSignup');
  res.end();
};

let vietnamSignup = function (req, res, next) {
  res.render('sodexoVnSignup');
  res.end();
};

let signIn = function (req, res, next) {
  let pageData = {
    "emailID": decodeURIComponent(req.query.emailId)
  }
  res.render('oldDashboardLogin', pageData);
  res.end();
};

let emailVerification = function (req, res, next) {
  let pageData = {
    "emailID": decodeURIComponent(req.query.emailId)
  }
  res.render('verifyEmail', pageData);
  res.end();
};

let displaySession = function (req, res, next) {
  res.end(JSON.stringify(req.session));
};

let verifyJwt = function (req, res, next) {
  let emailID = req.query.email;
  let jwtToken = req.query.jwtToken;
  let authToken = req.query.authToken;
  req.session[Constant.SESSION_AUTH] = req.query;
  if (jwtToken && authToken) {
    req.session[Constant.SESSION_JWT_TOKEN] = jwtToken;
    AuthSvc.associateZetaUserWithCorp(req.session[Constant.SESSION_JWT_TOKEN], authToken).then(function (respData) {
      console.log(JSON.stringify(respData));
      let companyResp = req.session[Constant.SESSION_SELECTED_COMP_INFO];
      let corporateListOfAccounts = Config.omsBaseUrl + Endpoint.GET_CORPORATE_LIST.replace(':authToken:', req.session[Constant.SESSION_AUTH].authToken);
      let getUserProfileURL = Config.omsBaseUrl + Endpoint.GET_USER_PROFILE.replace(':userJID:', req.session[Constant.SESSION_AUTH].userJID);
      let headers = {
        'X-Zeta-AuthToken': req.session[Constant.SESSION_AUTH].authToken
      }
      ServiceConnector.get(getUserProfileURL, '', headers).then(function (userProfile) {
        userProfile = JSON.parse(userProfile);
        let accountName = userProfile.name ? userProfile.name.firstName : '';
        req.session[Constant.SESSION_USER_PROFILE] = userProfile;
        if (userProfile.name && userProfile.name.lastName) {
          accountName = accountName + " " + userProfile.name.lastName;
        }
        let accResp = {
          "account": {
            "accountDetails": {
              "id": userProfile.userID,
              "name": accountName,
              "email": userProfile.emailList[0].email
            }
          }
        };
        req.session[Constant.SESSION_ACC_INFO] = accResp;
        ServiceConnector.get(corporateListOfAccounts).then(function (corpLists) {
          let corporateList = JSON.parse(corpLists);
          // Has to be verified
          req.session[Constant.SESSION_CORP_LIST] = corporateList;
          let selectedCorpID = corporateList.corporateCompanies[0].corporate.id;
          if (corporateList.corporateCompanies.length > 1) {
            res.redirect('https://' + req.headers.host + '/account/corporateSwitch');
          } else {
            let companyDetailsUrl = Config.omsBaseUrl + Endpoint.GET_COMPANY_DETAILS.replace(':corpID:', selectedCorpID)
              .replace(':authToken:', req.session[Constant.SESSION_AUTH].authToken);
            ServiceConnector.get(companyDetailsUrl, {}).then(function (companyResp) {
              let corpDetails = {
                "id": selectedCorpID,
                "ifi": corporateList.corporateCompanies[0].corporate.defaultIFI,
                "name": corporateList.corporateCompanies[0].corporate.name,
                "accountHolderName": req.session[Constant.SESSION_ACC_INFO].account.accountDetails.name,
                "allowedPrograms": corporateList.corporateCompanies[0].corporate.attributes.allowedProducts                
              }
              req.session[Constant.SESSION_CORP_INFO] = corpDetails;
              companyResp = JSON.parse(companyResp);
              req.session[Constant.SESSION_COMP_INFO] = companyResp.companySummaries;
              req.session[Constant.SESSION_SELECTED_COMP_INFO] = companyResp.companySummaries[0];
              if (companyResp.companySummaries[0].company.attributes && companyResp.companySummaries[0].company.attributes.reviewBillingAddress) {
                res.redirect('https://' + req.headers.host + '/account/corpDetails');
              } else {
                if(corpDetails.allowedPrograms.indexOf("OPTIMA") > -1){   
                  res.redirect('https://' + req.headers.host + '/companies/' + req.session[Constant.SESSION_SELECTED_COMP_INFO].company.companyID + '/optima');
                } else if(corpDetails.allowedPrograms.indexOf("EXPRESS") > -1){
                  res.redirect('https://' + req.headers.host + '/express');
                } else if(corpDetails.allowedPrograms.indexOf("EXPENSE") > -1){
                  res.redirect('https://' + req.headers.host + '/companies/' + req.session[Constant.SESSION_SELECTED_COMP_INFO].company.companyID + '/optima');
                  // to do  res.redirect('https://' + req.headers.host + '/companies/' + req.session[Constant.SESSION_SELECTED_COMP_INFO].company.companyID + '/expense');
                } else if(corpDetails.allowedPrograms.indexOf("SPOTLIGHT") > -1) {
                  res.redirect('https://' + req.headers.host + '/companies/' + req.session[Constant.SESSION_SELECTED_COMP_INFO].company.companyID + '/optima');
                // to do res.redirect('https://' + req.headers.host + '/companies/' + req.session[Constant.SESSION_SELECTED_COMP_INFO].company.companyID + '/spotlight');
                } else{
                  res.redirect('https://' + req.headers.host + '/companies/' + req.session[Constant.SESSION_SELECTED_COMP_INFO].company.companyID + '/optima');
                }
              }
            }, function (error) {
              res.sendStatus(500);
            });
          }
        }, function (error) {
          reject(error);
        });
      }, function (error) {
        reject(error);
      });
    }, function (respErr) {
      console.log(JSON.stringify(respErr));
      res.redirect(Config.ssoUrl + '?redirectUrl=' + 'https://' + req.headers.host + '/account/link&corporateEmail=' + emailID + '&accountExist=true');
    });
  } else {
    res.redirect(Config.ssoUrl + '?redirectUrl=' + 'https://' + req.headers.host + '/account/link&corporateEmail=' + emailID);
  }
};

let corpDetails = function (req, res, next) {
  let corpID = res.locals.corp.id;
  let corpDetailsPageData = {
    "corpID": corpID,
    "companyDtls": req.session[Constant.SESSION_SELECTED_COMP_INFO],
    "corpDtls": req.session[Constant.SESSION_CORP_INFO]
  };
  let countryList = Object.keys(Countries.getNameList()).sort((a, b) => (a > b) ? 1 : -1)
  corpDetailsPageData.countries = countryList
  corpDetailsPageData.templateData = {
    logoClassName: "zeta-purple-logo",
    showSodexoHelpSupport: null,
    showCinPanGst: true
  };

  if (req.session[Constant.SESSION_CONFIG_DATA]) {
    const config_data = Util.getDataFromObject(req.session[Constant.SESSION_CONFIG_DATA], 'homeCtrl.corpDetails.templateData');
    if (config_data) {
      corpDetailsPageData.templateData = config_data;
    }
  }


  res.render('corpDetails', corpDetailsPageData);
  res.end();
};

let corporateSwitch = function (req, res, next) {
  let corpListData = req.session[Constant.SESSION_CORP_LIST];
  for(var i = 0; i < corpListData.corporateCompanies.length; i++) {
    var obj = corpListData.corporateCompanies[i];
    var retailCorp = "RETAIL_CORPORATE"; 

    if(obj.corporate.type.indexOf(retailCorp) !== -1) {
      corpListData.corporateCompanies.splice(i, 1);
    }
  }
  corpListData.requestedSource = req.session.requestedSource
  corpListData.authToken = req.session.auth.authToken
  corpListData.sessionID = req.session.id
  corpListData.corporateCompanies = Util.sortArrayOfObject(corpListData.corporateCompanies, "corporate.name", 'ASC');
  res.render('corporateSwitch', JSON.parse(JSON.stringify(corpListData)));
  res.end();
};

let noCorpAccount = function (req, res, next) {
  AuthSvc.getUserProfile(req, res, next).then(function (userProfile) {
    userProfile = JSON.parse(userProfile);
    let hostName = req.headers.host;
    req.session[Constant.SESSION_USER_PROFILE] = userProfile;
    if (userProfile.name && userProfile.name.lastName) {
      userProfile['fullName'] = userProfile.name.firstName + ' ' + userProfile.name.lastName;
    } else {
      userProfile['fullName'] = userProfile.name ? userProfile.name.firstName : '';
    }
    // SODEXO_PHILIPPINES_SM Changes - START
    if (hostName.indexOf('smdigitalportal') != -1) {
      userProfile['employeeID'] = null;
      if (userProfile.emailList.length) {
          userProfile['employeeID'] = Util.extractEmailFirstPart(userProfile.emailList[0].email);
      }
    }

    userProfile['isPh'] = false;
    if (hostName.indexOf('sodexo.ph') != -1 || hostName.indexOf('sodexoph') != -1) {
      userProfile['isPh'] = true;
    }
    // SODEXO_PHILIPPINES_SM Changes - END
    res.render('noCorpAccount', userProfile);
    res.end();
  }).catch(function (error) {
    console.log(error);
    res.sendStatus(500);
    res.end();
  });
};

let inactiveCorpAccount = function (req, res, next) {
  AuthSvc.getUserProfile(req, res, next).then(function (userProfile) {
    userProfile = JSON.parse(userProfile);
    let hostName = req.headers.host
    req.session[Constant.SESSION_USER_PROFILE] = userProfile;
    if (userProfile.name && userProfile.name.lastName) {
      userProfile['fullName'] = userProfile.name.firstName + ' ' + userProfile.name.lastName;
    } else {
      userProfile['fullName'] = userProfile.name ? userProfile.name.firstName : '';
    }
    userProfile['emails'] = req.session[Constant.SESSION_INACTIVE_EMAILS];
    // SODEXO_PHILIPPINES_SM Changes - START
    if (hostName.indexOf('smdigitalportal') != -1) {
      userProfile['employeeID'] = null;
      if (userProfile.emailList.length) {
          userProfile['employeeID'] = Util.extractEmailFirstPart(userProfile.emailList[0].email);
      }
    }
    // SODEXO_PHILIPPINES_SM Changes - END
    userProfile['isPh'] = false;
    if (hostName.indexOf('sodexo.ph') != -1 || hostName.indexOf('sodexoph') != -1) {
      userProfile['isPh'] = true;
    }
    res.render('inactiveCorpAccount', userProfile);
    res.end();
  }).catch(function (error) {
    console.log(error);
    res.sendStatus(500);
    res.end();
  });
};

module.exports = {
  logout: logout,
  // renew: renew,
  verifyEmail: verifyEmail,
  signUp: signUp,
  hdfcSignup:hdfcSignup,
  vietnamSignup:vietnamSignup,
  signIn: signIn,
  emailVerification: emailVerification,
  displaySession: displaySession,
  verifyJwt: verifyJwt,
  corpDetails: corpDetails,
  corporateSwitch: corporateSwitch,
  noCorpAccount: noCorpAccount,
  inactiveCorpAccount: inactiveCorpAccount,
  signUpCorp: signUpCorp,
  redirectToSSO: redirectToSSO
};

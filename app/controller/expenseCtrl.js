var AdminSvc = require('../service/adminSvc');
var BenefitSvc = require('../service/benefitSvc');
var Constant = require('../common/constant');
var RecommonedProgramsModel = require('../model/recommonedProgramsModel');
var Util = require('../common/util');
var LosslessJSON = require('lossless-json');
var Urls = require('../common/url');
var JSONbig = require('json-bigint');
var UtilsUser = require('../common/utils/user.js');

let init = function(req, res, next) {
    res.locals.pageName = 'expense';
    let corpID = res.locals.corp && res.locals.corp.id;
    let companyID = req.params.companyId;
    res.locals.companyID = companyID;
    setSelectedCompany(req, res);
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    res.locals.expenseActive = UtilsUser.checkGKFlag(req, res.locals.selectedCompany.company.companyID, Constant.EXPENSE_GK_FLAG);
    if (!res.locals.expenseActive) {
        res.redirect('/companies/' + companyID + '/optima');
    }
    BenefitSvc.getActiveCards(authToken, corpID, companyID).then(function(respData) {
        let activeCards = JSON.parse(respData),
            expenseCards = [];
        activeCards.programs.forEach(function(item, index) {
            if (item.corpProductType === "EXPENSE") {
                expenseCards.push(item);
            }
        });
        let benefitHomePageData = {
            "activeCards": {
                "programs": expenseCards
            }
        };
        AdminSvc.getFundingAcc(authToken, corpID, 1, 10).then(function(fundRespData) {
            benefitHomePageData["fundingAccounts"] = LosslessJSON.parse(fundRespData);
            res.render('section/expense/home/index', benefitHomePageData);
            res.end();
        }).catch(function(error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });
    }, function(error) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(error, req, res, next);
    });
};

let getExpenseDetails = function(req, res, next) {
    res.locals.pageName = 'expense';
    res.locals.currPage = 'expenseDetails';
    let corpID = res.locals.corp.id;
    let programID = req.params.cardprogramId;
    let companyID = req.params.companyId;
    setSelectedCompany(req, res);
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    BenefitSvc.getDetails(authToken, corpID, companyID, programID).then(function(respData) {
        respData = JSONbig.parse(respData);
        respData.cardProgramObject.voucherProgram.voucherValidity = Constant.VOUCHER_VALIDITY[respData.cardProgramObject.voucherProgram.voucherValidity];
        respData.cardProgramObject.taxStatus = Constant.CARDS_TAX_STATUS[respData.cardProgramObject.productType.toLowerCase()];
        if (respData.totalPayoutsAmount) {
            if (respData.totalPayoutsAmount.amount) {
                respData.totalPayoutsAmount.amount = Util.formatINR(Math.round(respData.totalPayoutsAmount.amount / 100));
            }
        }
        if (respData.totalSpentAmount) {
            if (respData.totalSpentAmount.amount) {
                respData.totalSpentAmount.amount = Util.formatINR(Math.round(respData.totalSpentAmount.amount / 100));
            }
        }
        if (respData.fundingAccountDetails && respData.fundingAccountDetails.balance) {
            respData.fundingAccountDetails.balance = Util.formatINR(Math.round(respData.fundingAccountDetails.balance / 100));
        }
        if (respData.numberOfTransactions) {
            respData.numberOfTransactions = Util.formatNumber(respData.numberOfTransactions);
        }
        respData.cardProgramObject.cardType = Constant.FUNDING_TYPE[respData.cardProgramObject.cardType];

        req.session['expense'] = respData;
        res.render('section/expense/details/index', respData);
        res.end();
    }, function(respErr) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);
    });
}

let setSelectedCompany = function(req, res) {
    let companyID = req.params.companyId;
    let companiesInfo = req.session[Constant.SESSION_COMP_INFO].companies;
    req.session['selectedProgram'] = "expense";
    for (var i = 0; i < companiesInfo.length; i++) {
        if (companyID == companiesInfo[i].id) {
            req.session[Constant.SESSION_SELECTED_COMP_INFO] = companiesInfo[i];
            res.locals.selectedCompany = companiesInfo[i];
        }
    }
}

module.exports = {
    init: init,
    getExpenseDetails: getExpenseDetails,
    setSelectedCompany: setSelectedCompany,
};

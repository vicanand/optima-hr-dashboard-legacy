
var ZetaMerchantSvc = require('../service/express/zetaMerchantSvc'),
FeedbackSvc = require('../service/feedback/feedbackSvc'),
moment = require('moment'),
Constant = require('../common/constant');

var init = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    res.locals.pageName = 'feedback';
    var pageData = {
        companyID: req.params.companyId
    };
    var corpID = res.locals.corp.id;
    
    
        let params = {
            corporateID: corpID,
            businessID: req.params.businessID
        };
        
       
        ZetaMerchantSvc.getCitiesOfficeStore(authToken,params).then(function(allstores){
           var parsed = allstores.expressInsightsStoreList;
           var cities = filterCities(parsed);
           var offices = filterOffices(parsed);
           var stores = filterStores(parsed);
           var onlyStoreId = storeId(stores);
           let getCustomerRatings= {
            corporateID:corpID,
            fromDate: moment().subtract(30, 'days').format('YYYY-MM-DD'),
            toDate: moment().format('YYYY-MM-DD'),
            stores:onlyStoreId
            }
           FeedbackSvc.getCustomerRatings(authToken,getCustomerRatings).then(function(ratings){
               var ratings = filterratings(ratings);
               FeedbackSvc.getCustomerFeedback(authToken,getCustomerRatings).then(function(feedback){
                    var feedback = feedback;
                    getCustomerRatings.rating = 5;
                    getCustomerRatings.pageSize =10;
                    getCustomerRatings.toDate = moment().format('YYYY-MM-DD HH:mm:ss'),
                    FeedbackSvc.getCustomerComments(authToken,getCustomerRatings).then(function(comments){
                        var comments = comments;
                        pageData.response = parsed;
                        pageData.cities = cities;
                        pageData.offices = offices;
                        pageData.stores = stores;
                        pageData.ratings = ratings;
                        pageData.feedback = feedback;
                        var new_object = JSON.stringify(comments.comments);
                        var new_comments = JSON.parse(new_object);
                        console.log('new_comments: ', new_comments);
                        pageData.comments = comments.comments;
                        pageData.new_comments = new_comments;
                        formatTime(comments.comments);
                        console.log(pageData);
                        
                        res.render('section/feedback/home', pageData);
                        res.end();
        
                    });

               });
                
           });
          
        },function(error){
            console.log("getAllStores",error);
    });
  
};

var filterCities = function(allstores){
    var cities = new Set();
    var filteredCities = []
    allstores.forEach(function(stores){
        cities.add(stores.city)
    });
    cities.forEach(function(cities){
        filteredCities.push({cityName:cities,check:"1"});
    })
    return filteredCities;
    
}

var filterOffices = function(allstores){
    var offices = new Set();
    var offices_names = new Set();
    var filteredOffices = [];
    var offices_values = [];
    var final_offices = [];
    
    
    allstores.forEach(function(stores){
        offices.add(stores.businessID);
    });
    offices.forEach(function(offices){
        console.log('offices: ', offices);
       filteredOffices.push(offices);
    });
    filteredOffices.forEach(function(stores){
        allstores.forEach(function(all){
            if(all.businessID == stores){
                offices_names.add(all.office);
            }
        });
    });
    offices_names.forEach(function(offices){
        offices_values.push(offices);
    });
    for(var i = 0;i < filteredOffices.length;){
        console.log("aset_arr",filteredOffices[i]);
          for(var j = 0;j < allstores.length;j++){
          console.log("a",allstores[j].businessID);
            if(allstores[j].businessID == filteredOffices[i]){
              final_offices.push({officeName:allstores[j].office,businessID:allstores[j].businessID,check:"1"});
            i++;
          }
        }
      }
      
    console.log('offices_values: ', final_offices);
   // filteredOffices.forEach();
   return final_offices;
  
   
}

var filterStores = function(allstores){
    var filteredStores = [];
    allstores.forEach(function(stores){
        filteredStores.push({storeName:stores.storeName,storeId:stores.businessID+':'+stores.storeID,check:"1"})
    })
    return filteredStores;
}

var storeId = function(params){
    var arr = [];
    
    params.forEach(function(res){
        arr.push(res.storeId.toString());
    });
    arr = arr.join(",");
    console.log('arr: ', arr);
    return arr;
}

var filterratings = function(params){
    var ratings = params;
    console.log('ratings: ', ratings);
    var filteredratings={};
    filteredratings.ratingPercentages=[];
    filteredratings.totalNoOfUsers = params.totalNoOfUsers;
    filteredratings.averageRating = parseFloat(params.averageRating).toFixed(1); 
    var starToShow = parseFloat(params.averageRating).toFixed(2);
    var normalStar = 5 - Math.round(starToShow);
    var goldenStar = Math.round(starToShow);
    console.log('goldenStar: ', goldenStar);
    var stars=[];
    filteredratings.normalStar = normalStar;
    filteredratings.goldenStar = goldenStar;
    for(var i = 0;i<= ratings.ratingPercentages.length-1;i++){
      stars[ratings.ratingPercentages[i].field]=1;
      stars[ratings.ratingPercentages[i].field]={};
      stars[ratings.ratingPercentages[i].field].percentage = ratings.ratingPercentages[i].percentage;
     
    }
    
    
    
    for(var j=0;j<=stars.length-1;j++){
      if(stars[j] == undefined){
        filteredratings.ratingPercentages[j]={};
        filteredratings.ratingPercentages[j].field=j;
        filteredratings.ratingPercentages[j].percentage=0;
      }else{
        filteredratings.ratingPercentages[j]={};
        filteredratings.ratingPercentages[j].field=j;
        filteredratings.ratingPercentages[j].percentage=stars[j].percentage;
      }
    }
    console.log('filteredratings: ', filteredratings);
    return filteredratings;
    
}

var formatTime = function(time){
    
    time.forEach(function(item){
        item.timeStamp = moment(item.timeStamp).format('YYYY-MM-DD,hh:mm:ss a');
       
    });
    
}
module.exports = {
    init,
    
}

var i2b = require("imageurl-base64");
var Constant = require('../common/constant');
var EmployeeDetailsSvc = require('../service/employeeStatementSvc');
var URLs = require('../common/url');
var Config = require('../../config');
var pdfMake = require('pdfmake/build/pdfmake.js');
var pdfFonts = require('pdfmake/build/vfs_fonts.js');
var Utils = require('../common/util');
pdfMake.vfs = pdfFonts.pdfMake.vfs;
var fonts = {
    Roboto: {
        normal: './public/fonts/Roboto/Roboto-Regular.ttf',
        bold: './public/fonts/Roboto/Roboto-Medium.ttf'
    }
};
var PdfPrinter = require('pdfmake/src/printer');
var printer = new PdfPrinter(fonts);
let init = function (req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    let params = req.body.params;
    let dateRange = params.dateRange;
    let fromDate = Utils.dateToepochTime(dateRange.slice(0, 10), '-', 'after')
    let toDate = Utils.dateToepochTime(dateRange.slice(13, 23), '-', 'before')
    let fromToDate = Utils.timestampToDate1(fromDate) + ' to ' + Utils.timestampToDate1(toDate);
    let billFromDate = '' + dateRange.slice(0, 4) + dateRange.slice(5, 7) + dateRange.slice(8, 10);
    let billToDate = '' + dateRange.slice(13, 17) + dateRange.slice(18, 20) + dateRange.slice(21, 23);

    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let cardProgramID = params.cardProgramID;
    let cardID = params.cardID;
    let zetaUserID = params.zetaUserID;
    let userName = params.userName;
    let userEmail = params.userEmail;
    let companyName = params.companyName;
    let thisCardStatus = params.thisCardStatus;
    let productType = params.productType;
    let corpID = params.corpID;
    let imageUrl = params.imageUrl;
    let cardName = params.cardName;
    let currencyName = res.locals.country_details && res.locals.country_details.currencyName || 'INR';

    let billStatementObj = {
        "cardProgramID": cardProgramID,
        "cardID": cardID,
        "limit": 25,
        "offset": 0,
        "userID": zetaUserID,
        "dateRange": {
            "fromDateYYYYmmDD": billFromDate,
            "toDateYYYYmmDD": billToDate
        },
        "includePartiallyRejectedAmount": true
    };

    let url1 = Config.omsBaseUrl + URLs.GET_TRANSACTIONS
        .replace(":token:", authToken)
        .replace(":zetaUserID:", zetaUserID)
        .replace(":corpID:", corpID)
        .replace(":cardID:", cardID)
        .replace(":after:", fromDate)
        .replace(":before:", toDate)
        .replace(":pageSize:", 20);

    let url2 = Config.omsBaseUrl + URLs.GET_BILL_FOR_CARDPROGRAM
        .replace(":token:", authToken);


    let allData = []
    EmployeeDetailsSvc.getClaimBills(billStatementObj, url2).then(function (billerResponse) {
        var allBillerData = [];
        allBillerData.push(billerResponse)
        let resp = billerResponse;
        let count = billStatementObj.limit;
        if (count < billerResponse.count) {
            function billerProgramTranasactionService() {
                let billerObj = billStatementObj;
                billStatementObj.offset = count;
                EmployeeDetailsSvc.getClaimBills(billStatementObj, url2).then(function (billerResponse1) {

                    count += billerResponse1.bills.length;
                    for (var i = 0; i < billerResponse1.bills.length; i++) {
                        allBillerData[0].bills.push(billerResponse1.bills[i])
                    }
                    if (count < billerResponse1.count - 1) {
                        billerProgramTranasactionService()
                    } else {
                        allData.push(allBillerData);
                        getStatements();
                    }
                }, function (err) {
                    res.send(JSON.stringify({error: "Failed to get claims data, " + JSON.stringify(err)}));
                })
            }

            billerProgramTranasactionService()
        } else {
            allData.push(allBillerData);
            getStatements();
        }

        function getStatements() {
            EmployeeDetailsSvc.getTransactions(url1).then(function (txnResponse1) {
                var allResponseData = [];
                txnResponse1 = JSON.parse(txnResponse1);
                allResponseData.push(txnResponse1)
                let nextOff = txnResponse1.employeeStatement.nextOffset;
                if (nextOff != undefined) {
                    function cardProgramTranasactionService() {
                        let url = url1.replace(new RegExp("([?&]" + 'offset' + "(?=[=&#]|$)[^#&]*|(?=#|$))"), "&" + 'offset' + "=" + encodeURIComponent(nextOff))
                        EmployeeDetailsSvc.getTransactions(url).then(function (respData) {
                            respData = JSON.parse(respData);
                            allResponseData[0].employeeStatement.balanceAfterPeriod.amount = respData.employeeStatement.balanceAfterPeriod.amount;
                            allResponseData[0].employeeStatement.credits.amount += respData.employeeStatement.credits.amount;
                            allResponseData[0].employeeStatement.debits.amount += respData.employeeStatement.debits.amount;

                            for (var i = 0; i < respData.employeeStatement.cardTransactions.length; i++) {
                                allResponseData[0].employeeStatement.cardTransactions.push(respData.employeeStatement.cardTransactions[i])
                            }
                            if (respData.employeeStatement.nextOffset == undefined) {
                                nextOff = undefined
                                allData.push(allResponseData);
                                getEligibility()
                            } else {
                                nextOff = respData.employeeStatement.nextOffset;
                                cardProgramTranasactionService()
                            }
                        }, function (err) {
                            res.send(JSON.stringify({error: "Failed to get transactions, " + JSON.stringify(err)}));
                        })
                    }

                    cardProgramTranasactionService()
                } else {
                    allData.push(allResponseData);
                    getEligibility()
                }

                function getEligibility() {
                    let eligibilityUrl = Config.omsBaseUrl + URLs.GET_CARD_ELIGIBILITY
                        .replace(":authToken:", authToken)
                        .replace(":corpID:", corpID)
                        .replace(":cardID:", cardID);
                    EmployeeDetailsSvc.getCardEligibility(eligibilityUrl).then(function (resp) {
                        resp = JSON.parse(resp);
                        let cardEligibility = "";
                        if (resp.config.companySpecificData && resp.config.companySpecificData.yearlyEligibility) {
                            cardEligibility = resp.config.companySpecificData.yearlyEligibility;
                        } else if (resp.config.yearlyEligibility) {
                            cardEligibility = resp.config.yearlyEligibility;
                        } else if (resp.config.companySpecificData && resp.config.companySpecificData.eligibility) {
                            cardEligibility = resp.config.companySpecificData.eligibility;
                        } else if (resp.config.eligibility) {
                            cardEligibility = resp.config.eligibility;
                        }
                        let userImage = '';
                        if (!imageUrl.indexOf('profileDp.png') >= 0) {
                            i2b(imageUrl, function (err, data) {
                                userImage = data;
                            });
                        }
                        let billerResponse = allData[0][0] || allData["0"]["0"];
                        let txnResponse = allData[1][0] || allData["1"]["0"];
                        let billTxnResp = [billerResponse, txnResponse];
                        let employeeTransaction = txnResponse.employeeStatement;
                        let pdfData = downloadPDF(userName, userEmail, companyName, fromToDate, billTxnResp, thisCardStatus, productType, zetaUserID, cardEligibility, userImage, cardID, cardName, currencyName, res.locals.country_details.supportEmail);
                        
                        if (req.session[Constant.SESSION_CONFIG_DATA]) {
                            const config_data = Utils.getDataFromObject(req.session[Constant.SESSION_CONFIG_DATA], 'employeeStetmentCtrl.init');
                            if (config_data && config_data.downloadSodexoPDF) {
                                pdfData = downloadSodexoPDF(pdfData, userName);
                            }
                        }
                                        
                        pdfData.styles.symbol = { font: 'Roboto' };
                        var fileName = userName + "_" + companyName + "_" + fromToDate + '.pdf';
                        var pdfDoc;
                        if (billerResponse.bills.length != 0 || employeeTransaction.cardTransactions.length != 0 || employeeTransaction.escrowTransactions.length != 0) {
                            try {
                                pdfDoc = pdfMake.createPdf(pdfData).getBuffer(buffer => {
                                    res.send(new Buffer(buffer, 'binary'))
                                }, function (error) {
                                    console.log(error);
                                });
                            } catch (err) {
                                console.log("Failed to generate pdf data", err);
                                res.send(JSON.stringify({error: "Failed to generate pdf data"}));
                            }
                        } else {
                            console.log("No data found for this date range");
                            res.send(JSON.stringify({error: "No data found for this date range"}));
                        }
                    }, function (err) {
                        console.log("Failed to get eligibility", err);
                        res.send(JSON.stringify({error: "Failed to get eligibility"}));
                    })
                }
            }, function (err) {
                let message;
                if (err.statusCode === 504) {
                    message = "Something went wrong!"
                }
                else if (JSON.parse(err.body).message.indexOf("ledger") >= 0) {
                    message = "Failed to get employee transactions, Ledger not found";
                } else {
                    message = "Failed to get employee transactions";
                }
                res.send(JSON.stringify({error: message}));
            })
        }
    }, function (err) {
        console.log("Failed to get claims data", err);
        res.send(JSON.stringify({error: "Failed to get claims data"}));
    });
}

let downloadPDF = function (userName, userEmail, companyName, fromToDate, billTxnResp, thisCardStatus, productType, zetaUserID, cardEligibility, userImage, cardID, cardName, currencyName, supportEmail) {
    if (cardEligibility) {
        cardEligibility = 'Eligibility: ' + Utils.formatCurrency(cardEligibility, currencyName);
    } else {
        cardEligibility = '';
    }
    if (!supportEmail) {
        supportEmail = 'support@zeta.in'
    }
    userName = userName.toString();
    let billerAPI = billTxnResp[0] || billTxnResp["0"];
    let transactionAPI = billTxnResp[1] || billTxnResp["1"];
    transactionAPI = transactionAPI.employeeStatement;

    let cardStatusText = thisCardStatus;
    let summaryAmountTextSize = 18;
    let summarySecTitleSize = 9;
    let summaryCreditDebitTitle = 9;
    let inReviewClaimAmount =  "₹" + (billerAPI.amountPerBillState.UPLOADED !== undefined ? Utils.formatINR(billerAPI.amountPerBillState.UPLOADED / 100) : 0);
    let declinedClaimAmount = "₹" + (billerAPI.amountPerBillState.DECLINED !== undefined ? Utils.formatINR(billerAPI.amountPerBillState.DECLINED / 100) : 0);
    let totalClaimedAmount = billerAPI.totalAmount != undefined ? Utils.formatINR(billerAPI.totalAmount / 100) : 0;
    let ApprovedTotalAmount = (billerAPI.amountPerBillState.UNPAID !== undefined ? billerAPI.amountPerBillState.UNPAID / 100 : 0) + (billerAPI.amountPerBillState.PAID !== undefined ? billerAPI.amountPerBillState.PAID / 100 : 0) + (billerAPI.amountPerBillState.PARTIALLY_PAID !== undefined ? billerAPI.amountPerBillState.PARTIALLY_PAID / 100 : 0)
    let approvedClaimedAmount = "₹" + Utils.formatINR(ApprovedTotalAmount);
    let declinedClaimCount = "DECLINED (" + (billerAPI.countPerBillState.DECLINED !== undefined ? billerAPI.countPerBillState.DECLINED : 0) + ")";
    let inReviewClaimCount = "IN REVIEW (" + (billerAPI.countPerBillState.UPLOADED !== undefined ? billerAPI.countPerBillState.UPLOADED : 0) + ")";
    let approvedTotalCounts = (billerAPI.countPerBillState.PAID !== undefined ? billerAPI.countPerBillState.PAID : 0) + (billerAPI.countPerBillState.UNPAID !== undefined ? billerAPI.countPerBillState.UNPAID : 0) + (billerAPI.countPerBillState.PARTIALLY_PAID !== undefined ? billerAPI.countPerBillState.PARTIALLY_PAID : 0)
    let approvedClaimedCount = "APPROVED (" + approvedTotalCounts + ")";
    let totalClaimedCount = "TOTAL CLAIMED (" + (billerAPI.count != undefined ? billerAPI.count : 0) + ")";
    let claimObj = []
    let declinedAmount = []
    let claimsForLinkedTxn = {};
    if (billerAPI.bills.length != 0) {
        let uploadedAt = []
        let approvedAmount = []
        let billStatus = []
        let declineReason = []
        let reimbursedAmount = []
        let merchantName = []
        let claimAmount = []
        let taxExemptAmountLTA = []
        let reimbursedAmountLTA = []
        let communicationConnectionNumber = []
        let communicationConnectionType = []
        let communicationServiceProvider = []
        let CarMaintenanceAndDriverSalaryCategory = []
        let claimID = []


        for (var i = 0; i < billerAPI.bills.length; i++) {
            declineReason.push((billerAPI.bills[i].billStatus == "DECLINED" || billerAPI.bills[i].billStatus == "DECLINED_AFTER_APPROVAL" || billerAPI.bills[i].billStatus == "PARTIALLY_APPROVED") ? (billerAPI.bills[i].attrs.declineReason !== undefined ? billerAPI.bills[i].attrs.declineReason : (billerAPI.bills[i].attrs.partialReason !== undefined ? billerAPI.bills[i].attrs.partialReason : '')) : '')
            reimbursedAmount.push(billerAPI.bills[i].reimbursedAmount !== undefined && billerAPI.bills[i].billStatus !== "DECLINED_AFTER_APPROVAL" ? billerAPI.bills[i].reimbursedAmount.amount / 100 : 0)
            billStatus.push(billerAPI.bills[i].billStatus)
            uploadedAt.push(billerAPI.bills[i].uploadedAt)
            approvedAmount.push(((billerAPI.bills[i].billStatus !== "UPLOADED") && (billerAPI.bills[i].billStatus !== "DECLINED_AFTER_APPROVAL") && (billerAPI.bills[i].billStatus !== "DECLINED")) ? billerAPI.bills[i].approvedAmount / 100 : 0);
            claimAmount.push(billerAPI.bills[i].value.amount / 100);
            merchantName.push(billerAPI.bills[i].attrs.merchantName !== undefined ? billerAPI.bills[i].attrs.merchantName
                :
                '-'
            )
            taxExemptAmountLTA.push(billerAPI.bills[i].attrs["ltaCalculation.taxExemptAmount"] !== undefined && billerAPI.bills[i].billStatus !== "DECLINED_AFTER_APPROVAL" ? billerAPI.bills[i].attrs["ltaCalculation.taxExemptAmount"] / 100 : 0)
            reimbursedAmountLTA.push(billerAPI.bills[i].attrs.PAID_OUT_AMOUNT !== undefined && billerAPI.bills[i].billStatus !== "DECLINED_AFTER_APPROVAL" ? billerAPI.bills[i].attrs.PAID_OUT_AMOUNT / 100 : 0)
            communicationConnectionNumber.push(billerAPI.bills[i].attrs["billPayment.rechargeNumber"] !== undefined ? billerAPI.bills[i].attrs["billPayment.rechargeNumber"] : billerAPI.bills[i].attrs["phoneNumber"])
            communicationConnectionType.push(billerAPI.bills[i].attrs["billPayment.txnType"] !== undefined ? (billerAPI.bills[i].attrs["billPayment.txnType"]).toUpperCase() + ' ' + (billerAPI.bills[i].attrs["billPayment.connectionType"]).toUpperCase() : '-')
            communicationServiceProvider.push(billerAPI.bills[i].attrs.businessId !== undefined && billerAPI.bills[i].attrs.businessId !== "NA" ? billerAPI.bills[i].attrs.businessId : '-')
            CarMaintenanceAndDriverSalaryCategory.push(billerAPI.bills[i].attrs.productType !== undefined ? billerAPI.bills[i].attrs.productType == "carMaintenance" ? 'Car Maintenance' : billerAPI.bills[i].attrs.productType == "DriverSalary" ? 'Driver Salary' : billerAPI.bills[i].attrs.productType : '-')
            claimID.push(billerAPI.bills[i].claimId !== undefined ? billerAPI.bills[i].claimId.slice(37, 55): '-')
            if (billerAPI.bills[i].billStatus == "DECLINED" || billerAPI.bills[i].billStatus == "DECLINED_AFTER_APPROVAL") {
                declinedAmount.push(billerAPI.bills[i].value.amount / 100);
            } else if (billerAPI.bills[i].billStatus == "PARTIALLY_APPROVED") {
                declinedAmount.push((billerAPI.bills[i].value.amount - billerAPI.bills[i].approvedAmount) / 100);
            } else {
                declinedAmount.push(0);
            }
            if (billerAPI.bills[i].attrs.transactionID !== undefined) {
                claimsForLinkedTxn[billerAPI.bills[i].attrs.transactionID] = {
                    'claimId': billerAPI.bills[i].claimId !== undefined ? zetaUserID + '-' + billerAPI.bills[i].claimId.slice(51, 55) + '-' + cardID.slice(0, 4) : '-',
                    'amount': billerAPI.bills[i].value.amount / 100
                };
            }
        }
        //NOTE : Keep the order
        claimObj.push(uploadedAt, approvedAmount, merchantName, billStatus, declineReason,
            claimAmount, approvedAmount, reimbursedAmount, taxExemptAmountLTA, reimbursedAmountLTA,
            communicationConnectionNumber, communicationConnectionType, communicationServiceProvider, CarMaintenanceAndDriverSalaryCategory, claimID, declinedAmount)
    }
    // transaction summary
    let transactionOpeningBalance = {
        amount: transactionAPI.balanceBeforePeriod.amount / 100, // openingBalances
        currency: transactionAPI.balanceBeforePeriod.currency
    };
    let transactionClosingBalance = {
        amount: transactionAPI.balanceAfterPeriod.amount / 100, // closingBalance
        currency: transactionAPI.balanceAfterPeriod.currency
    }
    let transactionCredits = {
        amount: transactionAPI.credits.amount / 100,
        currency: transactionAPI.credits.currency
    }
    let transactionDebits = {
        amount: transactionAPI.debits.amount / 100,
        currency: transactionAPI.debits.currency
    }

    let calcOpenCredit = transactionOpeningBalance.amount + transactionCredits.amount;
    calcOpenCredit = "#" + calcOpenCredit;
    if (calcOpenCredit.length > 8) {
        summaryAmountTextSize = 10;
    }

    // transaction statement
    let reimbursedText = "Reimbursed to cash card for approved claim";
    let transactionStatementObj = []
    if (transactionAPI.cardTransactions.length != 0) {
        let transactionDate = []
        let transactionTxtRef = []
        let transactionParticulars = []
        let transactionDebit = []
        let transactionCredit = []
        let transactionBalance = []
        let transactionType = []
        let claimID = []
        let isClaimed = []
        let claimApprovedAmount = []
        let isTransactionReversals = []
        let isTransactionReverseds = []
        let debitType = []
        let payoutId = []
        let currency = []

        for (var i = 0; i < transactionAPI.cardTransactions.length; i++) {
            let reimbursedText = "";
            let txnDebitType = transactionAPI.cardTransactions[i].debitType;
            if (txnDebitType && txnDebitType == "REIMBURSEMENT") {
                reimbursedText = "Reimbursed to cash card for approved claim";
            } else if (txnDebitType && txnDebitType == "EXPIRY") {
                reimbursedText = companyName;
            }
            let isTransactionReversal = transactionAPI.cardTransactions[i].isTransactionReversal;
            let isTransactionReversed = transactionAPI.cardTransactions[i].isTransactionReversed;
            let transactionTypes = transactionAPI.cardTransactions[i].transactionType;
            let transforAndSpendText = '';
            // if((transactionTypes != 'ESCROW') && (transactionTypes != 'ZERO_PAYOUT')){
            if (isTransactionReversal == false && isTransactionReversed == false && transactionAPI.cardTransactions[i].debitType != "REVOKED" && transactionAPI.cardTransactions[i].debitType != "EXPIRY") {
                if (transactionAPI.cardTransactions[i].claimID == undefined) {
                    transactionTypes == "CREDIT" ? transforAndSpendText = "Transfer from " + companyName : transactionTypes == "DEBIT" ? transforAndSpendText = "Spend at " + transactionAPI.cardTransactions[i].displayName : ""
                }
            }
            if (isTransactionReversal == false && isTransactionReversed == false && transactionAPI.cardTransactions[i].debitType == "REVOKED") {
                if (transactionAPI.cardTransactions[i].claimID == undefined) {
                    transactionTypes == "CREDIT" ? transforAndSpendText = "Transfer from " + companyName : transactionTypes == "DEBIT" ? transforAndSpendText = "Revoked by " + companyName : ""
                }
            }

            if (isTransactionReversal == false && isTransactionReversed == false && transactionAPI.cardTransactions[i].debitType == "EXPIRY") {
                if (transactionAPI.cardTransactions[i].claimID == undefined) {
                    if (transactionTypes == "DEBIT") {
                        if (transactionAPI.cardTransactions[i].displayName == "Expired - Moved to Cash Card") {
                            transforAndSpendText = transactionAPI.cardTransactions[i].displayName;
                            reimbursedText = "";
                        } else {
                            transforAndSpendText = "Expired - Moved to ";
                        }
                    } else {
                        transforAndSpendText = "";
                    }
                }
            }

            if (isTransactionReversal == false && isTransactionReversed == true) {
                if (transactionAPI.cardTransactions[i].claimID == undefined) {
                    transactionTypes == "DEBIT" ? transforAndSpendText = "Spend at " + transactionAPI.cardTransactions[i].displayName : ""
                }
            }
            if (isTransactionReversal == true && isTransactionReversed == false) {
                if (transactionAPI.cardTransactions[i].claimID == undefined) {
                    transactionTypes == "CREDIT" ? transforAndSpendText = "Transaction reversed by " + transactionAPI.cardTransactions[i].displayName : ""
                }
            }

            transactionDate.push(transactionAPI.cardTransactions[i].time)
            transactionTxtRef.push(transactionAPI.cardTransactions[i].receiptID !== undefined ? transactionAPI.cardTransactions[i].receiptID : '-')
            transactionParticulars.push(transforAndSpendText + reimbursedText)
            transactionDebit.push(transactionAPI.cardTransactions[i].transactionType == "DEBIT" ? transactionAPI.cardTransactions[i].amount / 100 : 0)
            transactionCredit.push(transactionAPI.cardTransactions[i].transactionType == "CREDIT" ? transactionAPI.cardTransactions[i].amount / 100 : 0)
            transactionBalance.push(transactionAPI.cardTransactions[i].newBalance != undefined ? transactionAPI.cardTransactions[i].newBalance / 100 : 0)
            transactionType.push(transactionAPI.cardTransactions[i].transactionType)
            claimApprovedAmount.push(transactionAPI.cardTransactions[i].claimApprovedAmount !== undefined ? 'TOTAL APPROVED AMOUNT: ₹' + transactionAPI.cardTransactions[i].claimApprovedAmount / 100 : '')
            isTransactionReversals.push(transactionAPI.cardTransactions[i].isTransactionReversal);
            isTransactionReverseds.push(transactionAPI.cardTransactions[i].isTransactionReversed)
            debitType.push(transactionAPI.cardTransactions[i].debitType !== undefined ? transactionAPI.cardTransactions[i].debitType : '')
            payoutId.push(transactionAPI.cardTransactions[i].payoutID != undefined && transactionAPI.cardTransactions[i].debitType !== 'SPENDS' ? transactionAPI.cardTransactions[i].payoutID : 'NA');
            currency.push(transactionAPI.cardTransactions[i].currency);
            if (transactionAPI.cardTransactions[i].claimID !== undefined) {
                claimID.push('CLAIM ID: ' + transactionAPI.cardTransactions[i].claimID.slice(37, 55));
                isClaimed.push(true);
            } else if (claimsForLinkedTxn[transactionAPI.cardTransactions[i].receiptID]) {
                claimID.push('CLAIM ID: ' + claimsForLinkedTxn[transactionAPI.cardTransactions[i].receiptID].claimId);
                claimApprovedAmount[i] = 'TOTAL APPROVED AMOUNT: ₹' + claimsForLinkedTxn[transactionAPI.cardTransactions[i].receiptID].amount;
                isClaimed.push(true);
            } else {
                claimID.push('');
                isClaimed.push(false);
            }

        }

        //NOTE : keep the Order
        transactionStatementObj.push(transactionDate, transactionTxtRef, transactionParticulars, transactionDebit,
            transactionCredit, transactionBalance, transactionType, claimID, claimApprovedAmount,
            isClaimed, isTransactionReversals, isTransactionReverseds, debitType, payoutId, currency)
    }

    let StatementTableData = [
        [{
            text: 'Date',
            margin: [5, 5, 0, 5],
            fillColor: 'F7F6F8',
            bold: true,
            font: 'Roboto',
            border: [true, true, true, true]
        },
            {
                text: 'Txn Ref. ID',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Particulars',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Debit',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            },
            {
                text: 'Credit',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            },
            {
                text: 'Balance',
                margin: [0, 5, 5, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            }
        ]
    ]
    if (transactionStatementObj[0] !== undefined) {
        var totalAmountOfDebitReversal = 0;
        var totalAmountOfCreditReversal = 0;

        for (let j = 0; j < transactionStatementObj[0].length; j++) {
            if (productType.toLowerCase() == 'meal' || productType.toLowerCase() == 'gift') {
                let claimData = (transactionStatementObj[9][j] == true) ?
                    {
                        listType: 'none',
                        columns: [
                            {
                                text: '' + transactionStatementObj[7][j],
                                margin: [-10, 3, 0, -5],
                                fontSize: 8,
                                color: '#756F7D',
                                lineHeight: 1.5,
                                width: 'auto'
                            },
                            {
                                text: ' • ',
                                margin: [4, 1, 0, -5],
                                fontSize: 12,
                                color: '#D1CFD4',
                                alignment: 'left',
                                lineHeight: 1.5,
                                width: 'auto'
                            },
                            {
                                text: transactionStatementObj[8][j],
                                margin: [4, 3, 0, -5],
                                fontSize: 8,
                                color: '#756F7D',
                                lineHeight: 1.5,
                                alignment: 'left',
                                width: 'auto'
                            }
                        ]
                    } : {
                        listType: 'none',
                        columns: [
                            {
                                text: 'PAYOUT ID: ' + transactionStatementObj[13][j],
                                margin: [-10, 3, 0, 0],
                                fontSize: 8,
                                color: '#756F7D',
                                lineHeight: 1.5,
                                alignment: 'left',
                                width: '58%'
                            }
                        ]
                    }
                StatementTableData.push([
                    {text: Utils.timestampToDate(transactionStatementObj[0][j]), margin: [0, 5, 0, 5]},
                    {text: transactionStatementObj[1][j], margin: [0, 5, 0, 5]},
                    {
                        ul: [{
                            text: transactionStatementObj[2][j],
                            margin: [-10, 5, 0, 0],
                            listType: 'none',
                            color: '#190f27'
                        },
                            claimData
                        ]
                    },
                    {
                        text: Utils.formatCurrency(transactionStatementObj[3][j], transactionStatementObj[14][j]),
                        margin: [0, 5, 0, 5],
                        alignment: 'right',
                        color: '#190f27'
                    },
                    {
                        text: Utils.formatCurrency(transactionStatementObj[4][j], transactionStatementObj[14][j]),
                        margin: [0, 5, 0, 5],
                        alignment: 'right',
                        color: '#190f27'
                    },
                    {
                        text: Utils.formatCurrency(transactionStatementObj[5][j], transactionStatementObj[14][j]),
                        margin: [0, 5, 5, 5],
                        alignment: 'right',
                        color: '#190f27'
                    }
                ])
            } else {
                let claimData = (transactionStatementObj[9][j] == true) ? {
                    listType: 'none',
                    columns: [
                        {
                            text: '' + transactionStatementObj[7][j],
                            margin: [-10, 3, 0, -5],
                            fontSize: 8,
                            color: '#756F7D',
                            lineHeight: 1.5,
                            width: 'auto'
                        },
                        {
                            text: ' • ',
                            margin: [4, 1, 0, -5],
                            fontSize: 12,
                            color: '#D1CFD4',
                            alignment: 'left',
                            lineHeight: 1.5,
                            width: 'auto'
                        },
                        {
                            text: transactionStatementObj[8][j],
                            margin: [4, 3, 0, -5],
                            fontSize: 8,
                            color: '#756F7D',
                            lineHeight: 1.5,
                            alignment: 'left',
                            width: 'auto'
                        }
                    ]
                } : (transactionStatementObj[13][j] == "NA") ? {} : {
                    listType: 'none',
                    columns: [
                        {
                            text: 'PAYOUT ID: ' + transactionStatementObj[13][j],
                            margin: [-10, 3, 0, 0],
                            fontSize: 8,
                            color: '#756F7D',
                            lineHeight: 1.5,
                            alignment: 'left',
                            width: '58%'
                        }
                    ]
                };

                StatementTableData.push([
                    {
                        text: Utils.timestampToDate(transactionStatementObj[0][j]),
                        margin: [5, 5, 0, 5],
                        color: '#190f27'
                    },
                    {text: transactionStatementObj[1][j], margin: [0, 5, 0, 5], color: '#190f27'},
                    {
                        ul: [{
                            text: transactionStatementObj[2][j],
                            margin: [-10, 5, 0, 0],
                            listType: 'none',
                            color: '#190f27'
                        },
                            claimData
                        ]
                    },
                    {
                        text: Utils.formatCurrency(transactionStatementObj[3][j], transactionStatementObj[14][j]),
                        margin: [0, 5, 0, 5],
                        alignment: 'right',
                        color: '#190f27'
                    },
                    {
                        text: Utils.formatCurrency(transactionStatementObj[4][j], transactionStatementObj[14][j]),
                        margin: [0, 5, 0, 5],
                        alignment: 'right',
                        color: '#190f27'
                    },
                    {
                        text: Utils.formatCurrency(transactionStatementObj[5][j], transactionStatementObj[14][j]),
                        margin: [0, 5, 5, 5],
                        alignment: 'right',
                        color: '#190f27'
                    }
                ])
            }
        }
        StatementTableData.push([
            {text: '', border: [false, true, false, false]},
            {text: '', border: [false, true, false, false]},
            {
                text: 'TOTAL',
                border: [false, true, false, false],
                margin: [0, 5, 0, 5],
                font: 'Roboto',
                bold: true,
                alignment: 'right',
                style: 'header'
            },
            {
                text: Utils.formatCurrency((transactionDebits.amount - totalAmountOfDebitReversal), transactionDebits.currency),
                border: [false, true, false, false],
                font: 'Roboto',
                margin: [0, 5, 0, 5],
                bold: true,
                alignment: 'right'
            },
            {
                text: Utils.formatCurrency((transactionCredits.amount - totalAmountOfCreditReversal), transactionCredits.currency),
                border: [false, true, false, false],
                font: 'Roboto',
                margin: [0, 5, 0, 5],
                bold: true,
                alignment: 'right'
            },
            {
                text: Utils.formatCurrency(transactionClosingBalance.amount, transactionClosingBalance.currency),
                margin: [0, 5, 5, 5],
                border: [false, true, false, false],
                font: 'Roboto',
                bold: true,
                alignment: 'right'
            },
        ])
    }

    let StatementTableWidth;
    let transactionStatementTable = [{
        style: 'tableExample',
        marginLeft: -20,
        fontSize: 8,
        table: {
            headerRows: 1,
            widths: [50, 55, 275, 40, 40, 50],
            body: StatementTableData,
            dontBreakRows: true,
        },
        // layout: 'lightHorizontalLines'
        layout: {
            hLineWidth: function (i, node) {
                return (i === 0 || i === node.table.body.length) ? 1 : 1;
            },
            vLineWidth: function (i, node) {
                return (i === 0 || i === node.table.widths.length) ? 0 : 0;
            },
            hLineColor: function (i, node) {
                return (i === 0 || i === node.table.body.length) ? '#e5e3e9' : '#e5e3e9';
            },
            vLineColor: function (i, node) {
                return (i === 0 || i === node.table.widths.length) ? '#e5e3e9' : '#e5e3e9';
            },
            // paddingLeft: function(i, node) { return 9; },
            // paddingRight: function(i, node) { return 4; },
            // paddingTop: function(i, node) { return 2; },
            // paddingBottom: function(i, node) { return 2; },
            // fillColor: function (i, node) { return null; }
        }
    },]

    // escrow data mapping
    var newEscrowBalance = 0;
    let escrowStatementObj = []

    let escrowTransactionsData = transactionAPI.escrowTransactions;

    if (escrowTransactionsData.length > 1) {
        escrowTransactionsData = Utils.sortArrayOfObject(escrowTransactionsData, "time", 'ASC');
    }

    if (escrowTransactionsData.length != 0) {
        let transactionDate = []
        let transactionTxtRef = []
        let transactionParticulars = []
        let transactionDebit = []
        let transactionCredit = []
        let transactionBalance = []
        let transactionType = []
        let claimID = []
        let isClaimed = []
        let claimApprovedAmount = []
        let isTransactionReversals = []
        let isTransactionReverseds = []
        let debitType = []

        for (var i = 0; i < escrowTransactionsData.length; i++) {
            let isTransactionReversal = escrowTransactionsData[i].isTransactionReversal;
            let isTransactionReversed = escrowTransactionsData[i].isTransactionReversed;
            let transactionTypes = escrowTransactionsData[i].transactionType;
            let transforAndSpendText = '';
            if (escrowTransactionsData[i].transactionType == "DEBIT") {
                newEscrowBalance -= escrowTransactionsData[i].amount / 100;
                transforAndSpendText = "Moved to ";
            } else if (escrowTransactionsData[i].transactionType == "CREDIT") {
                newEscrowBalance += escrowTransactionsData[i].amount / 100;
                transforAndSpendText = "Transfer from ";
            }

            transactionDate.push(escrowTransactionsData[i].time)
            transactionTxtRef.push(escrowTransactionsData[i].payoutID !== undefined ? escrowTransactionsData[i].payoutID : '-')
            transactionParticulars.push(transforAndSpendText + companyName)
            transactionDebit.push(escrowTransactionsData[i].transactionType == "DEBIT" ? escrowTransactionsData[i].amount / 100 : 0)
            transactionCredit.push(escrowTransactionsData[i].transactionType == "CREDIT" ? escrowTransactionsData[i].amount / 100 : 0)
            transactionBalance.push(newEscrowBalance)
            transactionType.push(escrowTransactionsData[i].transactionType)
            claimID.push(escrowTransactionsData[i].claimID !== undefined ? 'CLAIM ID: ' + escrowTransactionsData[i].claimID.slice(37, 55) : '')
            claimApprovedAmount.push(escrowTransactionsData[i].claimApprovedAmount !== undefined ? 'TOTAL APPROVED AMOUNT: ₹' + escrowTransactionsData[i].claimApprovedAmount / 100 : '')
            isClaimed.push(escrowTransactionsData[i].claimID !== undefined ? true : false)
            isTransactionReversals.push(escrowTransactionsData[i].isTransactionReversal);
            isTransactionReverseds.push(escrowTransactionsData[i].isTransactionReversed)
            debitType.push(escrowTransactionsData[i].debitType !== undefined ? escrowTransactionsData[i].debitType : '')
        }
        // }
        //NOTE : keep the Order
        escrowStatementObj.push(transactionDate, transactionTxtRef, transactionParticulars, transactionDebit,
            transactionCredit, transactionBalance, transactionType, claimID, claimApprovedAmount,
            isClaimed, isTransactionReversals, isTransactionReverseds, debitType)
    }

    // Escrow table data
    let EscrowTableData = [
        [{
            text: 'Date',
            margin: [5, 5, 0, 5],
            fillColor: 'F7F6F8',
            bold: true,
            font: 'Roboto',
            border: [true, true, true, true]
        },
            {
                text: 'Payout ID',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Particulars',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Debit (₹)',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            },
            {
                text: 'Credit (₹)',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            },
            {
                text: 'Balance (₹)',
                margin: [0, 5, 5, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            }
        ]
    ]

    // Prepare escrow table data
    var totalAmountOfEscrowDebitReversal = 0;
    var totalAmountOfEscrowCreditReversal = 0;
    if (escrowStatementObj[0] !== undefined) {


        for (let j = 0; j < escrowStatementObj[0].length; j++) {
            let claimData = (escrowStatementObj[9][j] == true) ?
                {
                    listType: 'none',
                    columns: [
                        {
                            text: '' + escrowStatementObj[7][j],
                            margin: [-10, 3, 0, 0],
                            fontSize: 8,
                            color: '#756F7D',
                            lineHeight: 1.5,
                            width: '37%'
                        },
                        {
                            text: ' • ',
                            margin: [-1, 1, 0, 0],
                            fontSize: 12,
                            color: '#D1CFD4',
                            alignment: 'left',
                            lineHeight: 1.5,
                            width: '4%'
                        },
                        {
                            text: escrowStatementObj[8][j],
                            margin: [-6, 3, 0, 0],
                            fontSize: 8,
                            color: '#756F7D',
                            lineHeight: 1.5,
                            alignment: 'left',
                            width: '58%'
                        }
                    ]
                } : {}
            EscrowTableData.push([
                {text: Utils.timestampToDate(escrowStatementObj[0][j]), margin: [5, 5, 0, 5]},
                {text: escrowStatementObj[1][j], margin: [0, 5, 0, 5]},
                {
                    ul: [{
                        text: escrowStatementObj[2][j],
                        margin: [-10, 5, 0, 0],
                        listType: 'none',
                        color: '#190f27'
                    },
                        claimData
                    ]
                },
                {
                    text: Utils.formatINR(escrowStatementObj[3][j]),
                    margin: [0, 5, 0, 5],
                    alignment: 'right',
                    color: '#190f27'
                },
                {
                    text: Utils.formatINR(escrowStatementObj[4][j]),
                    margin: [0, 5, 0, 5],
                    alignment: 'right',
                    color: '#190f27'
                },
                {
                    text: Utils.formatINR(escrowStatementObj[5][j]),
                    margin: [0, 5, 5, 5],
                    alignment: 'right',
                    color: '#190f27'
                }
            ])
            totalAmountOfEscrowDebitReversal += escrowStatementObj[6][j] == "DEBIT" ? escrowStatementObj[3][j] : 0
            totalAmountOfEscrowCreditReversal += escrowStatementObj[6][j] == "CREDIT" ? escrowStatementObj[4][j] : 0
        }
        EscrowTableData.push([
            {text: '', border: [false, true, false, false]},
            {text: '', border: [false, true, false, false]},
            {
                text: 'TOTAL',
                border: [false, true, false, false],
                margin: [0, 5, 0, 5],
                font: 'Roboto',
                bold: true,
                alignment: 'right'
            },
            {
                text: Utils.formatINR(totalAmountOfEscrowDebitReversal),
                border: [false, true, false, false],
                font: 'Roboto',
                margin: [0, 5, 0, 5],
                bold: true,
                alignment: 'right'
            },
            {
                text: Utils.formatINR(totalAmountOfEscrowCreditReversal),
                border: [false, true, false, false],
                font: 'Roboto',
                margin: [0, 5, 0, 5],
                bold: true,
                alignment: 'right'
            },
            {
                text: Utils.formatINR(newEscrowBalance),
                border: [false, true, false, false],
                margin: [0, 5, 5, 5],
                font: 'Roboto',
                bold: true,
                alignment: 'right'
            },
        ])
    }

    // Escrow statement table
    let EscrowTableWidth;
    let escrowStatementTable = [{
        style: 'tableExample',
        marginLeft: -20,
        fontSize: 8,
        table: {
            headerRows: 1,
            widths: [60, 55, 265, 40, 40, 50],
            body: EscrowTableData,
            dontBreakRows: true
        },
        // layout: 'lightHorizontalLines'
        layout: {
            hLineWidth: function (i, node) {
                return (i === 0 || i === node.table.body.length) ? 1 : 1;
            },
            vLineWidth: function (i, node) {
                return (i === 0 || i === node.table.widths.length) ? 0 : 0;
            },
            hLineColor: function (i, node) {
                return (i === 0 || i === node.table.body.length) ? '#e5e3e9' : '#e5e3e9';
            },
            vLineColor: function (i, node) {
                return (i === 0 || i === node.table.widths.length) ? '#e5e3e9' : '#e5e3e9';
            }
        }
    },]
    var closedImage = ''
    var header = [{
        type: 'rect',
        x: -40,
        y: -54,
        w: 600,
        h: 100,
        linearGradient: ['#a5149d', '#623bff']
    }]
    var plus = {
        text: '+',
        fontSize: 20,
        color: '#8B8792',
        Opacity: 0.5,
        absolutePosition: {x: 154.5, y: 158}
    }
    var minus = {
        text: '_',
        fontSize: 20,
        color: '#8B8792',
        Opacity: 0.5,
        absolutePosition: {x: 295, y: 150}
    }
    var equalTo = {
        text: '=',
        fontSize: 20,
        color: '#8B8792',
        absolutePosition: {x: 435, y: 158}
    }

    let reimbursementstatus = false;
    let reimbursementAmount = 0;
    let rokeStatus = false;
    let rokeAmount = 0;
    let expiredStatus = false;
    let expiredAmount = 0
    let spendStatus = false;
    let spendAmount = 0
    let refundStatus = false;
    let refundAmount = 0;
    let escrowStatus = false;
    let escrowAmount = 0;

    for (let r = 0; r < transactionAPI.cardTransactions.length; r++) {

        if ((transactionAPI.cardTransactions[r].isTransactionReversal == false) && (transactionAPI.cardTransactions[r].isTransactionReversed == false)) {
            if (transactionAPI.cardTransactions[r].debitType == "REIMBURSEMENT") {
                reimbursementstatus = true;
                // reimbursementAmount += transactionAPI.cardTransactions[r].claimApprovedAmount / 100;
                reimbursementAmount += transactionAPI.cardTransactions[r].amount / 100;
            }
            if (transactionAPI.cardTransactions[r].debitType == "REVOKED") {
                rokeStatus = true;
                rokeAmount += transactionAPI.cardTransactions[r].amount / 100;
            }
            if (transactionAPI.cardTransactions[r].transactionType == "ESCROW") {
                escrowStatus = true;
                escrowAmount += transactionAPI.cardTransactions[r].amount / 100;
            }
            if (transactionAPI.cardTransactions[r].debitType == "EXPIRY") {
                expiredStatus = true;
                expiredAmount += transactionAPI.cardTransactions[r].amount / 100;
            }
            if (transactionAPI.cardTransactions[r].debitType == "SPENDS" && transactionAPI.cardTransactions[r].transactionType == "DEBIT") {
                spendStatus = true;
                spendAmount += transactionAPI.cardTransactions[r].amount / 100;
            }
            if (transactionAPI.cardTransactions[r].debitType == "REIMBURSEMENT" && transactionAPI.cardTransactions[r].transactionType == "CREDIT") {
                refundStatus = true;
                refundAmount += transactionAPI.cardTransactions[r].amount / 100;
            }
        }
    }

    var transactionSummary = [{
        type: 'rect',
        x: -20,
        y: 20,
        w: 140,
        h: 70,
        lineColor: '#e5e3e9'
    },
        {
            type: 'rect',
            x: 120,
            y: 20,
            w: 140,
            h: 70,
            lineColor: '#e5e3e9'
        },
        {
            type: 'rect',
            x: 260,
            y: 20,
            w: 140,
            h: 70,
            lineColor: '#e5e3e9'
        },
        {
            type: 'rect',
            x: 400,
            y: 20,
            w: 140,
            h: 70,
            lineColor: '#e5e3e9'
        },
        {
            type: 'ellipse',
            x: 120,
            y: 55,
            color: 'white',
            lineColor: '#e5e3e9',
            fillOpacity: 1,
            fillColor: 'white',
            r1: 15,
            r2: 15
        },
        {
            type: 'ellipse',
            x: 260,
            y: 55,
            color: 'white',
            lineColor: '#e5e3e9',
            fillOpacity: 1,
            fillColor: 'white',
            r1: 15,
            r2: 15
        },
        {
            type: 'ellipse',
            x: 400,
            y: 55,
            color: 'white',
            lineColor: '#e5e3e9',
            fillOpacity: 1,
            fillColor: 'white',
            r1: 15,
            r2: 15
        }
    ]

    var headerContent = {
        style: 'tableExample',
        margin: [-20, 10, 10, 10],
        absolutePosition: {x: 75, y: 10},
        table: {
            widths: [241, 245],
            body: [
                [{
                    border: [false],
                    ul: [{
                        text: userName.charAt(0).toUpperCase() + userName.slice(1),
                        border: false,
                        margin: [-10, 5, 0, 0],
                        style: 'headerTextOne',
                        font: 'Roboto',
                        listType: 'none'
                    },
                        {
                            text: companyName,
                            margin: [-10, 5, 0, 0],
                            style: 'headerTextTwo',
                            font: 'Roboto',
                            listType: 'none'
                        },
                        {
                            text: userEmail,
                            margin: [-10, 5, 0, 0],
                            style: 'headerTextThree',
                            color: '#EAD1EF',
                            font: 'Roboto',
                            listType: 'none'
                        },
                    ]
                },
                    {
                        border: [false],
                        ul: [{
                            text: cardName.charAt(0).toUpperCase() + cardName.slice(1),
                            style: 'headerTextOne',
                            margin: [-10, 5, 0, 0],
                            alignment: 'right',
                            font: 'Roboto',
                            listType: 'none'
                        },
                            {
                                text: fromToDate,
                                margin: [-10, 5, 0, 0],
                                style: 'headerTextTwo',
                                alignment: 'right',
                                font: 'Roboto',
                                listType: 'none'
                            },
                            {
                                text: cardEligibility,
                                margin: [-10, 5, 0, 0],
                                style: 'headerTextThree',
                                alignment: 'right',
                                font: 'Roboto',
                                color: '#E1D6FB',
                                listType: 'none'

                            },

                        ]
                    }
                ],
            ],
            dontBreakRows: true
        }
    }


    let transactionSummaryTitle = {
        text: 'Transaction summary',
        style: 'subHeader',
        margin: [-20, 0, 0, -10],
        font: 'Roboto',
        fontSize: 16
    }

    let hrLine = {
        canvas: [

            {
                type: 'line',
                x1: -20,
                y1: 10,
                x2: 540,
                y2: 10,
                lineWidth: 2,
                lineColor: '#e5e3e9',

            }
        ]
    }

    let lineHide = {
        canvas: [

            {
                type: 'line',
                x1: -20,
                y1: -2,
                x2: 600,
                y2: -2,
                lineWidth: 8,
                lineColor: '#ffffff',

            }
        ]
    }

    let transactionStatementTitle = {
        text: 'Card transaction statement',
        margin: [-20, 20, 10, 10],
        fontSize: 16,
        font: 'Roboto'
    }

    let escrowStatementTitle = {
        text: 'Holding A/c Transaction statement',
        margin: [-20, 20, 10, 10],
        fontSize: 16,
        font: 'Roboto'
    }


    let claimsStatementTitle = {
        text: 'Claim statement',
        margin: [-20, 20, 10, 10],
        font: 'Roboto',
        fontSize: 18
    }

    let claimsStatementTableData = []


    if (claimObj[0] !== undefined) {
        let allClaimTableHeader = [
            [{
                text: 'Claim Date & ID',
                margin: [5, 5, 0, 5],
                fillColor: '#F7F6F8',
                bold: true,
                font: 'Roboto',
                border: [true, true, true, true]
            },
                {
                    text: 'Merchant',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8'
                },
                {
                    text: 'Status',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8'
                },
                {
                    text: 'Claimed (₹)',
                    margin: [0, 5, 0, 5],
                    fillColor: '#F7F6F8',
                    bold: true,
                    font: 'Roboto',
                    alignment: 'right'
                },
                {
                    text: 'Approved (₹)',
                    margin: [0, 5, 0, 5],
                    fillColor: '#F7F6F8',
                    bold: true,
                    font: 'Roboto',
                    alignment: 'right'
                },
                {
                    text: 'Declined (₹)',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8',
                    alignment: 'right'
                },
                {
                    text: 'Reimbursed (₹)',
                    margin: [0, 5, 5, 5],
                    fillColor: '#F7F6F8',
                    bold: true,
                    font: 'Roboto',
                    alignment: 'right'
                }
            ],
            [{
                text: 'Claim Date & ID',
                margin: [5, 5, 0, 5],
                fillColor: '#F7F6F8',
                bold: true,
                font: 'Roboto',
                border: [true, true, true, true]
            },
                {
                    text: 'Status',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8'
                },
                {
                    text: 'Claimed (₹)',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8',
                    alignment: 'right'
                },
                {
                    text: 'Approved (₹)',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8',
                    alignment: 'right'
                },
                {
                    text: 'Declined (₹)',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8',
                    alignment: 'right'
                },
                {
                    text: 'Reimbursed (₹)',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8',
                    alignment: 'right'
                },
                {
                    text: 'Tax Exemption (₹)',
                    margin: [0, 5, 5, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8',
                    alignment: 'right'
                }
            ],
            [{
                text: 'Claim Date & ID',
                margin: [5, 5, 0, 5],
                fillColor: '#F7F6F8',
                bold: true,
                font: 'Roboto',
                border: [true, true, true, true]
            },
                {
                    text: 'Towards',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8'
                },
                {
                    text: 'Service Provider',
                    bold: true,
                    font: 'Roboto',
                    margin: [0, 5, 0, 5],
                    fillColor: '#F7F6F8'
                },
                {
                    text: 'Status',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8'
                },
                {
                    text: 'Claimed (₹)',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8',
                    alignment: 'right'
                },
                {
                    text: 'Approved (₹)',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8',
                    alignment: 'right'
                },
                {
                    text: 'Declined (₹)',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8',
                    alignment: 'right'
                },
                {
                    text: 'Reimbursed (₹)',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8',
                    alignment: 'right'
                }
            ],
            [{
                text: 'Claim Date & ID',
                margin: [5, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                border: [true, true, true, true]
            },
                {
                    text: 'Category',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8'
                },
                {
                    text: 'Merchant',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8'
                },
                {
                    text: 'Status',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8'
                },
                {
                    text: 'Claimed (₹)',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8',
                    alignment: 'right'
                },
                {
                    text: 'Approved (₹)',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8',
                    alignment: 'right'
                },
                {
                    text: 'Declined (₹)',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8',
                    alignment: 'right'
                },
                {
                    text: 'Reimbursed (₹)',
                    margin: [0, 5, 5, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8',
                    alignment: 'right'
                }
            ],
            [{
                text: 'Claim Date & ID',
                margin: [5, 5, 0, 5],
                fillColor: '#F7F6F8',
                bold: true,
                font: 'Roboto',
                border: [true, true, true, true]
            },
                {
                    text: 'Status',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8'
                },
                {
                    text: 'Claimed (₹)',
                    margin: [0, 5, 0, 5],
                    fillColor: '#F7F6F8',
                    bold: true,
                    font: 'Roboto',
                    alignment: 'right'
                },
                {
                    text: 'Approved (₹)',
                    margin: [0, 5, 0, 5],
                    fillColor: '#F7F6F8',
                    bold: true,
                    font: 'Roboto',
                    alignment: 'right'
                },
                {
                    text: 'Declined (₹)',
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    fillColor: '#F7F6F8',
                    alignment: 'right'
                },
                {
                    text: 'Reimbursed (₹)',
                    margin: [0, 5, 5, 5],
                    fillColor: '#F7F6F8',
                    bold: true,
                    font: 'Roboto',
                    alignment: 'right'
                }
            ]
        ]

        var claimsTableWidth;
        var totalApprovalPartialApprovalAmount = 0;
        var totalReimbursedAmount = 0;
        var totalTaxExemption = 0
        if ((billerAPI.bills[0].productType.toLowerCase() == "medical") || (billerAPI.bills[0].productType.toLowerCase() == "books")) {

            claimsTableWidth = [80, 100, 70, 55, 60, 60, 75]
            claimsStatementTableData.push(allClaimTableHeader[0])
            for (let j = 0; j < claimObj[0].length; j++) {
                let arrData = [
                    {
                        ul: [
                            {text: Utils.timestampToDate(claimObj[0][j]), margin: [-3, 5, 0, -2], listType: 'none'},
                            {
                                text: claimObj[14][j],
                                margin: [-3, 5, 0, 0],
                                lineHeight: 1.5,
                                listType: 'none',
                                fontSize: 7,
                                color: '#5e5767'
                            }
                        ]
                    },
                    {text: claimObj[2][j], margin: [0, 5, 0, 0], lineHeight: 1.5},
                    {
                        ul: [
                            {
                                text: claimObj[3][j] == 'APPROVED' ? 'Approved' : claimObj[3][j] == 'DECLINED' ? 'Declined' : claimObj[3][j] == 'UPLOADED' ? 'In Review' : claimObj[3][j] == 'PARTIALLY_APPROVED' ? 'Partially Approved' : claimObj[3][j] == 'DECLINED_AFTER_APPROVAL' ? 'Declined after approval' : claimObj[3][j],
                                style: claimObj[3][j],
                                listType: 'none',
                                margin: [-10, 5, 0, 0]
                            }
                        ]
                    },
                    {text: claimObj[5][j], margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: claimObj[6][j], margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: claimObj[15][j], margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: claimObj[7][j], margin: [0, 5, 5, 5], alignment: 'right'}
                ];
                if (claimObj[4][j]) {
                    arrData[2].ul.push({
                        text: ' ' + claimObj[4][j] + ' ',
                        fontSize: 8,
                        color: '#5e5767',
                        listType: 'none',
                        margin: [-10, 5, 0, 0],
                        lineHeight: 1.5
                    });
                }
                claimsStatementTableData.push(arrData);
                totalApprovalPartialApprovalAmount += claimObj[6][j];
                totalReimbursedAmount += claimObj[7][j];
            }
            claimsStatementTableData.push([
                {text: '', border: [false, true, false, false]},
                {text: '', border: [false, true, false, false]},
                {
                    text: 'TOTAL',
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    alignment: 'right'
                },
                {
                    text: totalClaimedAmount,
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
                {
                    text: Utils.formatINR(totalApprovalPartialApprovalAmount),
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
                {
                    text: Utils.formatINR(billerAPI.amountPerBillState.DECLINED / 100),
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
                {
                    text: Utils.formatINR(totalReimbursedAmount),
                    border: [false, true, false, false],
                    margin: [0, 5, 5, 5],
                    bold: true,
                    font: 'Roboto',
                    alignment: 'right'
                },
            ])

        } else if (billerAPI.bills[0].productType.toLowerCase() == "lta") {

            claimsTableWidth = [80, 70, 120, 45, 50, 60, 75]
            claimsStatementTableData.push(allClaimTableHeader[1])
            for (let j = 0; j < claimObj[0].length; j++) {
                let arrData = [
                    {
                        ul: [
                            {text: Utils.timestampToDate(claimObj[0][j]), margin: [-3, 5, 0, -2], listType: 'none'},
                            {
                                text: claimObj[14][j],
                                margin: [-3, 5, 0, 0],
                                lineHeight: 1.5,
                                listType: 'none',
                                fontSize: 7,
                                color: '#5e5767'
                            }
                        ]
                    },
                    {
                        ul: [
                            {
                                text: claimObj[3][j] == 'APPROVED' ? 'Approved' : claimObj[3][j] == 'DECLINED' ? 'Declined' : claimObj[3][j] == 'UPLOADED' ? 'In Review' : claimObj[3][j] == 'PARTIALLY_APPROVED' ? 'Partially Approved' : claimObj[3][j] == 'DECLINED_AFTER_APPROVAL' ? 'Declined after approval' : claimObj[3][j],
                                style: claimObj[3][j],
                                listType: 'none',
                                margin: [-10, 5, 0, 0]
                            },
                        ]
                    },
                    {text: Utils.formatINR(claimObj[5][j]), margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: Utils.formatINR(claimObj[1][j]), margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: Utils.formatINR(claimObj[15][j]), margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: Utils.formatINR(claimObj[9][j]), margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: Utils.formatINR(claimObj[8][j]), margin: [0, 5, 5, 5], alignment: 'right'}
                ];
                if (claimObj[4][j]) {
                    arrData[1].ul.push({
                        text: ' ' + claimObj[4][j] + ' ',
                        fontSize: 8,
                        color: '#5e5767',
                        listType: 'none',
                        margin: [-10, 5, 0, 0],
                        lineHeight: 1.5
                    });
                }
                claimsStatementTableData.push(arrData);

                totalApprovalPartialApprovalAmount += claimObj[1][j];
                totalReimbursedAmount += claimObj[9][j];
                totalTaxExemption += claimObj[8][j]
            }
            claimsStatementTableData.push([
                {text: '', border: [false, true, false, false]},
                {
                    text: 'TOTAL',
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    alignment: 'right'
                },
                {
                    text: totalClaimedAmount,
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
                {
                    text: Utils.formatINR(totalApprovalPartialApprovalAmount),
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
                {
                    text: Utils.formatINR(billerAPI.amountPerBillState.DECLINED / 100),
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
                {
                    text: Utils.formatINR(totalReimbursedAmount),
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
                {
                    text: Utils.formatINR(totalTaxExemption),
                    border: [false, true, false, false],
                    margin: [0, 5, 5, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                }
            ])


        } else if (billerAPI.bills[0].productType.toLowerCase() == "communication") {


            claimsTableWidth = [80, 70, 70, 70, 50, 50, 50, 55]
            claimsStatementTableData.push(allClaimTableHeader[2])
            for (let j = 0; j < claimObj[0].length; j++) {
                let arrData = [
                    {
                        ul: [
                            {text: Utils.timestampToDate(claimObj[0][j]), margin: [-3, 5, 0, -2], listType: 'none'},
                            {
                                text: claimObj[14][j],
                                margin: [-3, 5, 0, 0],
                                lineHeight: 1.5,
                                listType: 'none',
                                fontSize: 7,
                                color: '#5e5767'
                            }
                        ]
                    },
                    {
                        ul: [
                            {
                                text: claimObj[10][j],
                                bold: true,
                                color: '#000',
                                font: 'Roboto',
                                listType: 'none',
                                margin: [-10, 5, 0, 2]
                            },
                            {
                                text: claimObj[11][j],
                                fontSize: 8,
                                alignment: 'justify',
                                font: 'Roboto',
                                background: '#e9e6ed',
                                color: '#4a4a4a',
                                bold: true,
                                lineHeight: 1,
                                listType: 'none',
                                margin: [-10, 0, 0, 5]
                            }
                        ]
                    },
                    {text: claimObj[12][j], margin: [0, 5, 0, 5]},
                    {
                        ul: [
                            {
                                text: claimObj[3][j] == 'APPROVED' ? 'Approved' : claimObj[3][j] == 'DECLINED' ? 'Declined' : claimObj[3][j] == 'UPLOADED' ? 'In Review' : claimObj[3][j] == 'PARTIALLY_APPROVED' ? 'Partially Approved' : claimObj[3][j] == 'DECLINED_AFTER_APPROVAL' ? 'Declined after approval' : claimObj[3][j],
                                style: claimObj[3][j],
                                listType: 'none',
                                margin: [-10, 5, 0, 0]
                            }
                        ]
                    },
                    {text: claimObj[5][j], margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: claimObj[6][j], margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: Utils.formatINR(claimObj[15][j]), margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: claimObj[7][j], margin: [0, 5, 5, 5], alignment: 'right'}

                ];
                if (claimObj[4][j]) {
                    arrData[3].ul.push({
                        text: ' ' + claimObj[4][j] + ' ',
                        fontSize: 8,
                        color: '#5e5767',
                        listType: 'none',
                        margin: [-10, 5, 0, 0],
                        lineHeight: 1.5
                    });
                }
                claimsStatementTableData.push(arrData);
                totalApprovalPartialApprovalAmount += claimObj[6][j];
                totalReimbursedAmount += claimObj[7][j];
            }
            claimsStatementTableData.push([
                {text: '', border: [false, true, false, false]},
                {text: '', border: [false, true, false, false]},
                {text: '', border: [false, true, false, false]},
                {
                    text: 'TOTAL',
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    alignment: 'right'
                },
                {
                    text: totalClaimedAmount,
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
                {
                    text: Utils.formatINR(totalApprovalPartialApprovalAmount),
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
                {
                    text: Utils.formatINR(billerAPI.amountPerBillState.DECLINED / 100),
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
                {
                    text: Utils.formatINR(totalReimbursedAmount),
                    border: [false, true, false, false],
                    margin: [0, 5, 5, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
            ])


        } else if ((billerAPI.bills[0].productType.toLowerCase() == "car_maintenance_and_driver_salary") || (billerAPI.bills[0].productType.toLowerCase() == "carmaintenanceanddriversalary") ||
            (billerAPI.bills[0].productType.toLowerCase() == "driver_salary") || (billerAPI.bills[0].productType.toLowerCase() == "driversalary")
        ) {

            claimsTableWidth = [80, 50, 65, 60, 65, 50, 60, 65]
            claimsStatementTableData.push(allClaimTableHeader[3])
            for (let j = 0; j < claimObj[0].length; j++) {
                let arrData = [
                    {
                        ul: [
                            {text: Utils.timestampToDate(claimObj[0][j]), margin: [-3, 5, 0, -2], listType: 'none'},
                            {
                                text: claimObj[14][j],
                                margin: [-3, 5, 0, 0],
                                lineHeight: 1.5,
                                listType: 'none',
                                fontSize: 7,
                                color: '#5e5767'
                            }
                        ]
                    },
                    {text: claimObj[13][j], margin: [0, 5, 0, 5]},
                    {text: claimObj[2][j], margin: [0, 5, 0, 5]},
                    {
                        ul: [
                            {
                                text: claimObj[3][j] == 'APPROVED' ? 'Approved' : claimObj[3][j] == 'DECLINED' ? 'Declined' : claimObj[3][j] == 'UPLOADED' ? 'In Review' : claimObj[3][j] == 'PARTIALLY_APPROVED' ? 'Partially Approved' : claimObj[3][j] == 'DECLINED_AFTER_APPROVAL' ? 'Declined after approval' : claimObj[3][j],
                                style: claimObj[3][j],
                                listType: 'none',
                                margin: [-10, 5, 0, 0]
                            }
                        ]
                    },
                    {text: claimObj[5][j], margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: claimObj[6][j], margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: Utils.formatINR(claimObj[15][j]), margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: claimObj[7][j], margin: [0, 5, 5, 5], alignment: 'right'}
                ];
                if (claimObj[4][j]) {
                    arrData[3].ul.push({
                        text: ' ' + claimObj[4][j] + ' ',
                        fontSize: 8,
                        color: '#5e5767',
                        listType: 'none',
                        margin: [-10, 5, 0, 0],
                        lineHeight: 1.5
                    });
                }
                claimsStatementTableData.push(arrData);
                totalApprovalPartialApprovalAmount += claimObj[6][j];
                totalReimbursedAmount += claimObj[7][j];
            }
            claimsStatementTableData.push([
                {text: '', border: [false, true, false, false]},
                {text: '', border: [false, true, false, false]},
                {text: '', border: [false, true, false, false]},
                {
                    text: 'TOTAL',
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    alignment: 'right'
                },
                {
                    text: totalClaimedAmount,
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    alignment: 'right'
                },
                {
                    text: Utils.formatINR(totalApprovalPartialApprovalAmount),
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    alignment: 'right'
                },
                {
                    text: Utils.formatINR(billerAPI.amountPerBillState.DECLINED / 100),
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
                {
                    text: Utils.formatINR(totalReimbursedAmount),
                    border: [false, true, false, false],
                    margin: [0, 5, 5, 5],
                    bold: true,
                    font: 'Roboto',
                    alignment: 'right'
                },
            ])

        } else {
            claimsTableWidth = [80, 170, 65, 60, 60, 75]
            claimsStatementTableData.push(allClaimTableHeader[4])
            for (let j = 0; j < claimObj[0].length; j++) {
                let arrData = [
                    {
                        ul: [
                            {text: Utils.timestampToDate(claimObj[0][j]), margin: [-3, 5, 0, -2], listType: 'none'},
                            {
                                text: claimObj[14][j],
                                margin: [-3, 5, 0, 0],
                                lineHeight: 1.5,
                                listType: 'none',
                                fontSize: 7,
                                color: '#5e5767'
                            }
                        ]
                    },
                    {
                        ul: [
                            {
                                text: claimObj[3][j] == 'APPROVED' ? 'Approved' : claimObj[3][j] == 'DECLINED' ? 'Declined' : claimObj[3][j] == 'UPLOADED' ? 'In Review' : claimObj[3][j] == 'PARTIALLY_APPROVED' ? 'Partially Approved' : claimObj[3][j] == 'DECLINED_AFTER_APPROVAL' ? 'Declined after approval' : claimObj[3][j],
                                style: claimObj[3][j],
                                listType: 'none',
                                margin: [-10, 5, 0, 0]
                            },
                        ]
                    },
                    {text: claimObj[5][j], margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: claimObj[6][j], margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: Utils.formatINR(claimObj[15][j]), margin: [0, 5, 0, 5], alignment: 'right'},
                    {text: claimObj[7][j], margin: [0, 5, 5, 5], alignment: 'right'}
                ];
                if (claimObj[4][j]) {
                    arrData[1].ul.push({
                        text: ' ' + claimObj[4][j] + ' ',
                        fontSize: 8,
                        color: '#5e5767',
                        listType: 'none',
                        margin: [-10, 5, 0, 0],
                        lineHeight: 1.5
                    });
                }
                claimsStatementTableData.push(arrData);
                totalApprovalPartialApprovalAmount += claimObj[6][j];
                totalReimbursedAmount += claimObj[7][j];
            }
            claimsStatementTableData.push([
                {text: '', border: [false, true, false, false]},
                {
                    text: 'TOTAL',
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    bold: true,
                    font: 'Roboto',
                    alignment: 'right'
                },
                {
                    text: totalClaimedAmount,
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
                {
                    text: Utils.formatINR(totalApprovalPartialApprovalAmount),
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
                {
                    text: billerAPI.amountPerBillState.DECLINED !== undefined ? Utils.formatINR(billerAPI.amountPerBillState.DECLINED / 100) : 0,
                    border: [false, true, false, false],
                    margin: [0, 5, 0, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
                {
                    text: Utils.formatINR(totalReimbursedAmount),
                    border: [false, true, false, false],
                    margin: [0, 5, 5, 5],
                    font: 'Roboto',
                    bold: true,
                    alignment: 'right'
                },
            ])
        }
    }

    let claimsStatementTable = [{
        style: 'tableExample',
        marginLeft: -20,
        fontSize: 8,
        table: {
            headerRows: 1,
            widths: claimsTableWidth,
            body: claimsStatementTableData,
            dontBreakRows: true
        },
        // layout: 'lightHorizontalLines'
        layout: {
            hLineWidth: function (i, node) {
                return (i === 0 || i === node.table.body.length) ? 1 : 1;
            },
            vLineWidth: function (i, node) {
                return (i === 0 || i === node.table.widths.length) ? 0 : 0;
            },
            hLineColor: function (i, node) {
                return (i === 0 || i === node.table.body.length) ? '#e5e3e9' : '#e5e3e9';
            },
            vLineColor: function (i, node) {
                return (i === 0 || i === node.table.widths.length) ? '#e5e3e9' : '#e5e3e9';
            }
        }
    }]

    let closedCardImage = {}
    let emptyValue = {}
    if (cardStatusText == "CLOSED") {
        closedCardImage = {
            image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOAAAACmCAYAAADDLIOQAAAAAXNSR0IArs4c6QAAQABJREFUeAHsnQmcllXVwJ93VpZhlx1kEHABF0I0tVRQU7O+LA1TS7PMyr5WS/tavsCy1fbSzK2yssJSy630U8bcysQtFwxwRnZREJB9tu//P8/zvAwwQwyNCMX5zZ27nXvuueeec+597rO8SbIL/qkEmpOkhFAwXpAkXYjLbUTcdRHBckKPJ5OkwmCdMWVlWZzjlFNWmuFXTE3TQZeyypkE61YkSR9pPEQ/WZk4hYxWSdan9AsZP/Yf+RZ4pdLYBbsk8G8hARRbJdd4coMpywwoDGduknTO6suMHTRx+Ywk6Za1sV358iTpndVVTsNAKStbliS9MpxSjKyKdKdaQmZw9qnhGkpyIzVNKMtoldp/LW3yMsunUS+PpnfBLgnstBJAqTUeVyeNTMXP8646JS9gZC8nSb/MIDWqMCAHTDry4mlQLcryFTCMaxp4hlxI2cpmnxpgWW54S5KkO3nb5HTlpSIrr8rwrAtnkdPbFe+SwE4rAZQ5Vj8HkCl+jxYGojG4ylW6CrU2SOrE0SBiNdJYxHO1a4lPfRjowiSpbllumrpYQY3dmhKXZwZdqMXgMwPUQWh8EbIVtLAprV35HUcCuyanHXOh0tdhRNVJ0nAdNjGJgACb3EL2SJLlz2OAlUkynrJD6rk2xNr6E57+U5L8YAzteiXJ/ljohyjr1pgka0uSZDe6n9mEIZIuo6wv+R7UV5J+EDoDzNPvIsr+uipJfjkwSZZS1p2+6vsT5kF3aJKsoSy5n/5HQQeia2jbIL8UN8qj9btgx5MAc7MLtkYCmTIHKhYwgKXqIIykTwNGhpbPQsNd/T5DGIBQVfwSY9qtID6VLeoTXZPkGgxtAnUJ7dZhVBXUrZc2satsM3SWsQ9dQoxdJZ3A13o0onoC5zPJDd0wYq4lNcy3kB9OvJj4XvipxfhWgkdRkswGZw/6Ib/W/C7Y8SRQvObY8Vj71zhCAwu5IpJG1yPvqrBpeaxiLXsDR7mgz2FEjeRLWPGaTqHsJbaHVP6KytdiHAUtDWgmnRuSebpOmmjXTOhG5oddsDniUZZbD0PLSLhKWt9E2jbL+DcF46yF3lXkvcYUpFNJX/3WYZTis+p+A5z3UkcUFrecxG04hzvgcTY0FoK7mipsfwNAp5T2MSZKdRKgRnvpKIvokn8ki7KKMvF2QcdK4N/WABGTq49Kr8KrXF6DaVSeNJaMztLkVUaVLXBIrOcarIKt3rp5qQGgx0nBLaR4rDzLIbgY5AKx9L02k0AZwb6IUiUmoUJLeARb02R9WiFOE+X9KbepeRvJRy/iMwnDCH3IWy+E06B9MxM2AAM7gEYvQXM91iNp23v9dypGeirxy+DNJfyDbetk8HtKi+PXe55iHLXpCt3EOEGJrWrlPNgcwpjBs6/giTrH7BhlxMh/JHdBR0nACfi3g0xZNDZXjsILKCkXV27DwuN7iGFdDYq2N7cFOrG968n2juuqclaWrqTR2dA025ShtAUNFuVrVFFZzZ4PbQzbCmPSeFXMPI5+cxwYmcs14a3kve77EHgarfyF8eYNyWuY48lbFMZLHJpvGW1LMLiRxJWLk+R/GdMRpPeEUCXtnMv1WI6OoBsDHE35PvQ7iryHPY7tZsru4Cbjb6ULzYYXk2QwycUYH2gp/3PT7bRbV5oXQfryBQu7oKMkoND/7QAtUVk0MpXIraCrhMrUjGV15zBkBVvKkkNZ4dAmlUqEEla9pukYxkiuv1DkQ1DWm7DABrT3IFDORNkHiQzNo7I+TLsSNlLnyaNGyl9cz0UaRkrp+H9YvS6h4kiYUvnlR1JhZLSPVce2FNpeQzQOpCyt4ruqX4WDOAcDrOI6tC/0q6jwYOeD8mGTjJ60C/BVamEGDdTNouw7LOu/p3wUF67zoDeflb2qR3q92jiNsUxI+5dHx6FDoVkUQmIXdJQEYpI7itiOREcvPgSGGOAalKcUwyoZxvUThxSrKGvS81O+JwYxifhggk+r3ISWseAl5xLvi6Z9HdwDUO7jSWs0JANADePx4EQw0kA0fGUaSkphAQUvcCH2KZT8Uox5AOE+EKTTCeTuImsgmVaH0eRZ24NTsJ6wiPAj+PoJPHfGgKpoMx+mXg+xn1MXhke6knJXS405X0XJhhERUcFKSZ3Xho75EfA/xbXjIyPTMeiEysbjsKiL8YAfKyF56dg+Y9fcLvhXJOD8/9tBS0UhXXDVW4ghch233se4WDmOQpM+SziEeq3H65y4xtMy0K44pKAsV2JXUUHFK6VdGB6ZOeS7ke+JAXiwso74dtqNZVs71DbUeY/gk3ckyfcm8Rgb28CZlN9F8d3UvYMY24zbD+wog74OoxuaXw6tlZTNJswg3IUVLKJ8BOmh9NGZ8Dy0H6GvG0lnLIY9l8Cbq6jGYiT/rrLBN+nc0C1zrHPBuRLat7Bdf2Y4W2XK3Mp6aOUQxHcVVDymlcMu6AAJIMt/P8gVJRuZp356cw9QjmGZOhFFezua1F8lpTxXRpKhrDZzNYlrrsiQJi6hXVyzYRhr0Mq/QetiaPcDt5p8N/LzqbuGVek48gfSqIK65eBcxoq7aCQKzErzTvLzOBC5TWfAatYbvHHg70W55zTYaDKctM+czoHPP3XnMEU+oPFh8JbRzxx4GQ3yI9TPhcZ9lFdTlrAyajEanzyHAyE2E9vcLKY6xq6hOs5SjNiL5Oegdw04f6P83tvZkp6SGihdBE2Gtwt2SSCTAIriqqXe+eCyCquHjweaTePNfUKlhOul49HqG1gJl69C4Ug3c0NtHQb5MsFbCw3GWWigzrRlDeCats0K4luIz13Cysm2csi0rO+tmZAnMcatwWsLh3GUarB5n1PSsXpN2BV+D4WvkzHQd2Hgj8GnPNcTvF1Rn4Um8vl4rMvHnKfFa8a4m6lbz/50CjLV8PQIPquqbDXo2ClYnuU15m2GnGZOgLz9xIPltWzTLZ+bOqkq09mcRp/gyUvwZd3OCP+S8F7tATsB8ODJZv1U0kcwYf1ZndhuduJApZ5rmfI9U49+BK67r8iA2ypXxRfx8s8QH0yZ2y2KI4jjpLocSjtujpP/Lsp5BdeQ6HmU50f2or4qQMel81BWHYbba5nAAPdlRbua5Ajq64j3Ia8iM9wIZGOll2+DeWOGutH43Yb+Fpl9olu6KndGrk3Ida0yeY6t8+5pue0EjXW9dZHbyn8g277AOCqHpPw5Dsua6a8zjq5qRHoqXYIj6NyTe5tUxlhpK+8J8xxT6/U2Xq4wKt0IWLXDgwPdacEJ4NZBZ4zOia9fwKNdKMgqPPgAjOVIZuccZuYQNM/TQK9hYrsJbgkG+Th5D10+Rr4rQiCblDiTtBWWUfAC4U4m9VIQnqasKesPvYjJpyr63i5bM3hxtY9TSWIZVgHLUMDmA9M6ipJ18FiNNQwgvTvjuZK0BzZan5E4RSCjDsT1IrGOJz+BVRSNyOIEBlfbL712FbfHUxgBu4omFL0HRrNkCdfYtTg+D26obxfQXxcaOH8hdvmZhVNhYJ0Y10oujAsTQMjrJT4TZ8q2vBuOZz6W2DwIGhim18s6UnbSOw/E9m3nYXcDp1NS5StHy1azNYvtJ7XL2U6ex+R9HIMZoDYwoXGz3KSTywQ1ZgbZn/zrCa6GarJG2ciEzkTz/sK11B/J3wV97DnAw5xuGLunh6VMfneU4GXpZfXbK3LblRshyaQe40tQ2pKRKOB8tmsYRS2rx0o0uxr+vsSY9gHxIMbFQhkGGIdKNmbc8q8eaJwquuIQPJzRcV1LuJ/V5+cY2v3sAObPpQ88zyrKPTAqe4G2yMXbMMpaslsN4K+eAhs0wk8EL82MQ2NcuQf/5lGOZVUh+33A7QSzPRnHMaQPIrwXA30MQ13F6uf9WTt3jtvFA812QXslgIR9vcfXb3w9p3IKeTz/HljLLFyg12w3kH6RpcprGq9vvLaL6zryXgt5TWRdI2nD7WzfTmOih7XkBdpe95RPS5XUmVXRSqcSiDV8lXe7gP0SVNYIeafy4DUmcWUtSuqKRBo93QCUDWWMX2W8zyufVQTk4fhbXgsqJ2UR14rEITdxlRWymQad45GTz6HmLxn7BE5pNg/YZPsgaxt8k472xPE2h5S87qXvX8Nrk3ysIeABg3fiRfB0Bg5BHnwXMq4d28fBq4uNs9g5AWEX6lC46sxrohTHYAkfYjTHM4vluNCz8ZZzGODpaOK7KacJjdI4VxSNy23WdNz/Gdk2yzqVVy9cBd1CHw5gwLMsaGT15l019PqxepB+xQEG8jkzzm8PeODUpS98ULganMo68HpT1p1VmovWThhdAwNq7pE+VHA4zO8N3hjwx0EngEH4QEFxkNTFKaqDpryEJarArmIWcj2rF6shfXalT3xckaf89DXobc0/aMdc0Jerslt6V/fGBSnvk+jrHZQdRZkHTrLigw+O26ffSzHIixnLNzmhwTcELXmg2S7oMAkgdeW9GdSiWIuT5DV46tvxhIv10qQNevUfUHYkBrQfYQ1lrnieDjZRvo78OuKZ5N+JJ+1HH5VzU0+af/LBCXfFqbAf4uDBmGA+X41a5W0zZjuwIOOhuArKS23GE2N1dbJOHsseQnGz1drxuLIUzDP+A8E9HRlMJz2V9LxsRcxPhZVhPWURg7OedKOrD+mnWJn2g1bnTGbSDUOSfnuGCn4uR2N5rKTfM+HrPvpZkfGUn+DmJ7ZxSu1KDt5bbEfs864l0xhje/rfhftPJIBQCxiZn2lo+UZ5Aa/+WoR+o8ZFHLcKmLjYPpF3C1VPPIf4r5RrcLnyzMBzn0f7k9yqsXXytoVG1S7F+Sdsv2rVWzsO8TCmMSj4XhoRsvgU8nrJbZ7yQ2Z5aKn0sUVF5hrpbML3CSe6WklPY1empHUAFeZNKwzrCbmxhRPI65xfnSBzdBT9eqvHW0Rue3OnGQbXgqfIewAD7gcg7ymqD6/bh31HP9NSh+P8Goq8vGqTszN2jOA8QncCK5lsvVwZE+G12ny9o4ZFPiaE9EbXLuTj2oWL+LhmoM3NKM/h0qtNV4hY7VwRLPtPBORZjgH6MEABeZ1NeEojRKb5TqIo30zO+TVig9fX4K+n/FsY3lDlCB1XXq/FimnlmuXDCDODZQcZt012Z17OhMbvCWuyFS/mk3zuBPI458W4yf51pE+m/RWy1VhDs++KzHEHLxlv/xZOVrltN0CQ5bXppIbwsslaxgSEp0YB9NCxbSIdE2VMsCwMFKO7lfQZHhTA+KYT5WS5hfL64z8KGLNb0V4qpzsBB89KsidlFxHmKWOCOwllHHJW1lm6KGsPRsB5hHBiJsuuGkO2u9AglHtuhEU5My//RT81Gp3GtCntLN/S+JxXg7ealtBfLYY/WtpCtuI6n7FjIi4aXGaksRqn2DvG/yKDOwY7m3OBEEvqMA60o4r7PS8yWTdyGHAitwm8WA8Jc8Vtsgnpuv3IwYv1Ryn7MvHferMdtUJlm4Ry0MD7RgG0iRWQsv+Yi3eVE0XuQVhblW7Pmvui2MjA2zVlyPlo5HYZsq7m8IbkZrcplHkOvqfoWx914E4B+Xc1rGZHssVlmVvQjYOgp6A7htXSU80uPABPw7eCezDzM5D+PFRR9t5CkC5FEaiOmCgttwIEX2Z+jngE4QGKniZeRP+XiUgfa7FK6VVMx2APhMZ1hFN2wPmF7x0b9GrPInNmrGwxk8V+9GaY3psJ8LTLkzMnLd4oZ7YcTyl1LzO5X6TuF/1iPtJHmLiHRVWyGu/Zswf391CKMibKOQU9Gpv+jwFXqAGsThjg0lnIAtn6Um49nqrbMK4HMcL3ILApCHX3TDBxSqoQAYs0EOdB0PmZX0p+yn1JcjmKP4Gye/uDwy2ibtA/k7zPwo4kptto4P1GQTIav4l4YMLCFmC94JNJ+N94xnZgVqgerKJ8Hvnf9eQtFupXEaKavn1Yw1fTYp4p32Eg43+H4WczRhBsyYvcVGeJOhFmD2OGT0DQvjnuCug1Re41m8ApRcI+IvZZJuF7eL+SvTA2vPxKFKx+Qqo0cVQuXTpz5TNuYOtVMRSvTfo/BpBBaR2GUZ3eSmhERmUjUyelonqt7VZO4zsfWZ/AKtcL2YdxsLxojBqMDzK42miA1vn+oXL8DvlrEfAA4tPIvwV8rzV9vE98ZU82gvMYhkwsPema528jyPMaHAteXGvGq1ekX6aMxTZZCvOn9cHwSasj8RkS0mHYxjsS5APqMJ4QQps0qaB6c7CNdcTseMJLOTkKr3kGQsVLn0r+x0yg20q3muLm/Uiz1AsLBL+Yk7FfgjOZ1U4l8hEnPWzJLGiPSo3Tx5WcFPQp+qNJTLZ0XhGwfwirnNEH/3LeI+afCpWnA+cVYWQTonQUspgCf5PTa8D8BWb5FeQp3g8czjU0k/I58kMJL1HBjpXKdJ5EFNegcSlb5f4SoTft3JXEJQPlWwLb0yR2KvJgPgfLc8jxNFzL1R/ByLn9PI2v4sJzCWm/aOAnSJovhIcpKW1xdwjI+O44XhhwPnmbEaUz5mFzoI1H4n1wX6up1XuW1vBvbJK8mTZuWU4CR0nn3k46udCd3FrqfoHF/R7De5w6vzbmFuRVA8dE56GIGRMtDVBHo6L6dH8XY7R5/TzGPSRzGpa9mlDLwRfX3X4lIPiEFw8/Dmdyu1DgWL5Gek/G6RG/g823ojnbjt9gtWF7gS8xl7A8Xo9OvQt5NtaRr4aHUxgD14L5eLYXP1vsRwF1KCDpdhugDGB5Q7nmWMpqt4prPbecPsHyaZSgH0sUthV0w/Ayhu3HlfAGJPotLryn4+UcjwLWON2ibAbbyt9mhLKCLdHbgBK8l2NsZZz8FRhTfTV8ogxNk8jXUU4+/9ZobJGztv9yBH/KpNV5prBVh2inD7E74BpOWXLpFm8fFLiWqnB+rMcYjyH6JDSOJxS3eda9yuBY/YqAb20c2otnRTMZKASSOxbooV91wHi6LuGEs2u6p69icj8FYx9GWpUYX1FJFKTSBXz7vIBVzqHsEvb7DxB3pnwdoYxrRnS8dQOkfHuCTsKtl9uxJlc5gkPw/lXXMSm/zYy9kTH0nIDSUOcOQMV/1UA507lyR8whU3kp6U8ZzrGKuSntmST/hzMpR+jHM0e506Xpqw/KGoa8Fj0Jbh4jqDdFPXr1OdzAQS64DSWvQkqvisX45Wjv7+yJsD6EtLyB6srnpMYnFmTNjAHhNsD8dSA8RF5HYrEX8Co5u4/WwYloK7TeYsulW6DV7LWHrWEMtIDmWfBax9jkEQNcz72sLqw0DWy/l1DmbYAONT7ouV1sdcwZT5tFtskK85iipJkVsAQeVy/ESaziRJrC47Jthsqd425GbzsXyIsGV4H3OAWPtpd5mNshFptNZbFDGKBMMbHraogRmp92d9ujIjrxui49mhmVKfb4VN6C8V30RLpi+Fm+taycPqHvjd9oZ9tXC+RVA6N/VxHPiOLlXg+ChqcrXbCG02msYcUW37Aj8N6CB1iKlaMriXouVgfjMD6Dw/wIsr8cjf6Yc0PYYfRIOWc8N8LfKC5t/hveO9SpQb/DQAF3KDB5bU4Gnemd2gS2NO8H5yJCHwKkUoUkLcQtB2PC1Xje/8W7vYRSlLOFXdsPJeYAhqLYivqenu03gxbKtVldW202Q2xR0Ba91mix1RyKNe5Nnd+jqabtMlaSaxnD3pCcyzZ0bgvSHZJsiz+Jt8aj5bapQ54ob9NoVg7wVrOlGIPRfQ9ej6Y+tiZotZ85dMJB2TFA3gQZQtalXJM82DtJJpD33UVXxhwl8F7tf9tNcIxaw3SLWF/LClfN6sBWptMgJhcD2pct6CnUn43Q/Pam8xqC8p9MEjztRAeSz7Ks/ByDW0Rd0KRMD0fTpKGGeGKKR3ZzoI3kWgUq8hUotrTkNz1cyD/SW3wFSXq2k6D8kG5iPF1hbCBEBsCzWyEumZJ3Evzwks+z+nl5dDmuU39FeiJl3r/0tafnyP+JlfEa6knGG+AVI9PV1AJ5dEWtz9It+1cGseUEx5NWjScUj+1jF67hsKmQmXS8Li1MB4ctcFF+8+BtCLQpL1DuPNjobBzeRxjPviAW54YqSGwkT8hGYazmJL036PZX5xlgmoQ05DUn4M31uG4Dz2vmuKdIdXFs4gLSkZ7znpOMivyfhVkfFkkXH5dcheJczC5rIfWFGvpmjKXIlGEFvvy8KqAwths4+BdQTq7z6hm8CuKK5YeDPkadnwn0ifYQBml5Ixv/NL6nKfsWhTdwfweHHBPVYYKjo9L5GAcT470jlcOJVtnLUN5KVq96VgMnzAksGcthBOWNOBBvefibDfugoIfA46FM9mBiDcw6ByGffYJomndwKj1ocS3rtrmlRj1N5c0UPUbDmdA9D/y9SPvJDMt+St0jCG89RqWSrqR9SR394cj8FV/fkrdrQTmvqaF8f3hiNcgPeewyxyEZGfmUD3+ltyeIh0Hvk/T5Wso6UyXPtrFta5DTzHE2iqmUV/tQFEtJsKONTy4+B+FmGPUbrT5k4Y/V2DZVhrRfjVowyvuJgk3+WSf4TKjGXEpnTczP/zzOd30mZHM7j3llrj20y52FbbY7ZGN65ftFGKEgwzE6lLk72811GKETWsGJYA+U7Gdo49Hk9XL5TVsnzNOsh5ms/0Z5HqyBzoS0nauIytRhQL/KIz49wQrkakFRrNquThqLhygVKH0VljiAAo/gz6RuBOmu1AsqWPGlUeqlKR1BgxakZYXjD7o5QoZvGwnNp9zPGo6JBhSR9kDKD+s+QXwPaBrk3ztxIlzFCkq9H1NiNx4vqJYuhN+BGDnltokVZTby3iOVHc3S2wrUN+JQ+hO7crtN9trpUEKuyLGKkae7GBPRZkB1cayxWpG3T8ttGPdx4eMewveZvD/eix5MonweTg4LPxa08RjMXsTy4FftFrPcH2x7HBvZ9MEK6ky3BvYVdf4Tj/H402+3kX/XYsbLoH3kDnIpvS3Qao1+h5bJ7HYBBh8GWM08sHx16xa3krj3gFdGQD9FwU6CEZ8IUW7FGSajAr6RVe9vFPdAodarUKSL20Dx/1Wgn8IslFU6o9LtIA45vKN9FQHHsTv8vBt+z2UiB4YLTZVSvg3K1JAbW16WFlIhMGZXdRtohIFjmobaWhgK9Sq/7fJyswHWySB03Eb4k2ReF3/jh3xyYjL81DGWnyEr0uhz2gfbys7jcSyskHEznfHWT0xl6X29PaD5Aei8AyEMy5hyPuw/i9IxkW8VcqS0SaDY3u/K5OMI66VsIg6uZirlR7DlxaH5NkU5DnY5dbGNR877wfgg5Dubcbo72os6HYKG7dy3xYYdi5OvbDrwJuTUQJjItsRbVorNIeayUd4Mf/vDlgbR4dwwcPvT665zwItYNchciIA/iZZTnc9PTLTXi14LnMOW9Xe7MznUuw1ai6d20vw8nW06DFQIvLEG4OR4Mumb9OzmktPoSCXqzIwdRH4cQXDSvM7In9AJry9f4EongHxRgy0gb90vGfO+aML+4IovWigCiVAg6LrieR2XOybrBdvbJlfw/HMRf6Xswz3jPnrg2a/bOT9M7FfGGhmMDsX2KrpfkhuGUM+gwAcf9okBZeOije3ji3Ik7askOibTGogPzVwGsur8CX7o97uMdQjpz1B2O/lfscWYMZhPQwYG/yj304eFgal+OAeWlV5I9CkOynEwF9H/GRSHc7S+DbBb+27puHzl7FL6PN9zB9uRl0eD/xz6dofofHv1yoDLrmPcKjkrX1+M721I2d9f0LAUeCgGsYrnCvEg3tg315cycySTlXjGrhju2jF4d/IdCvBn/yosjjdW57ehNJ8mrxH6SfowABgFNWkkjgcCyOhtnUDbk4x6ogAV0sIwKmMGW2D79WXKDyb/hqze8edgkeMPLw5RjVyDEeLwJE1Gf9IWz4ze/hka3wm+K3UN+VvZcdRWp+39XozK2/Qc163sKt5Lm3cQ3O75Kfx86x8OQSao4y9AA4zBZfnWoqxJOlYQXiDcU4nSM+Y6Krvh0H6PoR8Jfy9BeAV9/hRhX4ZDXf4sTmKP9PrePuWlWSdNnXPdUMcOqAe/j0F7VKhNsK1OhubBe7DNP/Nz6est0HucvA7eQzb1qkN3U9DbapCp7QJTEMaRhAkMPMEQmYiTkNQPEQB6EAZHFIorT80Ypx/cOR+BfwcF6smWdQXblC5em4EQJ6nDY/dls20D+tag8tVHIvIWE4/RH0/mu/Do1gd9SYF0vrXRoGLFosZJD89PmTPNX+Kn7NMlEgWi3GszhhR9LMIKPkH+dSjguZTh3OOHOrtAqJzOJKNS6Azyzi2TD4tMhNFleevcVWiknaiDbHqtSH41+YsouLwvPChL0odR7gPLrr6VBB2ecpCOihn9WE4+CsGleHOwAeAq6QGRmr8W+ktAnkX2C6zGfyftQ9keSK3H8Z4F3qXUuZtQGDqyc5nfK8lqsX7oycfIGmoZS3XqDDUSdwKrFnB9izFPRbYTs3HarCXIEuwE36aVU5yqEjch6PfWJclvWAXLXQlBwD/EiXKMlfR2BXjafsBgPV3bA4G/Hwl5kc+WPJQlhMa/mHjqXG1W4K0m9WG70pEc0oeTqcIreFiJp/1LDiTtBJFXCSZiIJeS3xt8cYraJ6NCLjjzpondnvll5nDXNPJAZA5hIeVP48LvA6dOVKxwNoqp/hW4fjuIvp4C38OH1xKPRbnmwQg+IDmA9gYW/vjIrp97kEmNxZVQTbNPX4adQdtbKTsHet3AibGIQgLflfyB+Flov4H0oQT7p0nbIH36TiclVWSzQl4s7XgZF0IeGBn+j/FdxsSWMMZ57li4tnu5BnlPxLAwwKPh4RsQOEA6EqKNXyn/DDQeuIvfwWB3Uzo6naNwcNSLpnOJr6XhnMeR/iUDGEIdf/kUkNqQj8wm/9xV1ODEJz6JHtCHK6U/h1VGn0zR9gcHtt2AizhfxPwNEzAOhfG+T1xXZAyER4ch3+lbRt1XUc5LBmSndB3FJP3jbOOi30ffVpL2Wm91HQcqVHjhX02Zq5IKEisDcSirMw2E0kSCcpQ9AGfhilVHZjqKNId4DuP4I2HBQCaXuphsPHgXPW8eL+ZEFY/vqq4s8ldnxG2GWCXbg7egNM/Tj1/43gPZHU7az80HQS2Iti9RdysadC71X4VZj/PxXenqTr2rMVHahrp8XBa1CdBMG6VxtCcZOwYyUQ1djf9PjP87XFj9tRqZsrupQrZ+Wdx3Crv1ID0LhYehg2nvr/t6CvJtaEhTeq66WtJUjPa0XDZ4oNHgLaL9UuoUdeE6jHASqPTxWVbCL9Gv5ARFoVjaBGhY/zzxCfD3SEZTWbs6Q2r7g+PeboBAT+mKAepqGLzgtc1sFGY90vPLx/Lj5wY+8xhbkgkIlYIMVfR/HViBfe1pFZRcBWEnWYsRDGF2r2R2DmU16WQFnWpQHrzIYyM8OnnF6zgyev+nCX+i/DEU5Tms+XniuRjUy5RJQ8VypdKAQcX9kp6QKcs8Jn4I/T+PUfZPPbCGIb59eULsduzlqeCdknloeB0Fr/uB0AXcfcB9L/EMlP/N0EkGwiOdnU/9/xK8xrHv2DbDgKuIQqY4gtktQV6vDKIdBbHVJluCrPzF3j8xd6dhOKsWYiSsTuUo9xrGVny1CmPp9V2u8T4BT9A5GU0/ne3Hz5DV2BBKStsvk7tVPZMV82bmyd3HcOqfgt5zpEugWYm8nJ5GdOkg2v+awVWDIxnHlPNLcnMAwXoN/j76On83zhiexFmw+umIXxVgDNsPEFYPtQFpzUASX0IQPyDuT6yyI5NQ+N8h8Kv3TQ9mtNUO9UzLWW1YVbwek5VVKPlYFMmT2IkoUzMdxmTCj/fyYpVGkcWlSYDbmGXUT0Xpv0fhYpTePM0DydM2msRWV1qOK7/GKh+J86+hYAI4EKVZUsD42LWlMBMlI2UbH61bhSX3rWI7C9/uHpag6KBE8F8lsvoD/PeDRjOa2QmCwwknw4D8Sl+F0wDzlTvy1lm5BcidjfjiKgPBseTyUEZr4cnfmy9HDspg3YX0N5kykWkLe8nKKSTPS7fkPTHS/gywhjA248HnScVlsUsuZk78QdSryfyZfD7/BYwvTs8pS6Zxb3gshyko8HDmLMhIg3GbhtTmkFU0I+DDIXoqSH+D7+Bzc+ztUwK/2wcWpavNgXaI4P9KuIWR38FkdEMwvZhRnYHbgk8ilDKUT6UMpe5IDpnEtShu93koBko9Ae34A/2/iY6Kqy35mJRcOORdOcLwqLgZXt/MKvoxFO6py9PrsyZWqVK8cneUp/OsdLukUyne/GZcZJMG+787pd9Ae8dYUQP9Z5FDHWkMtHk6uNDwBVi/17IEmr1xGvPpu8k+5tIH11Y9kF1X5PRX6m4CpxSF/QDKdQsdjaY/DSeGQD6/HUBR0TDlZ0uQG20Z43UnwOKb1BDPIWb6Er+S7Wp9LKvV013YBcDvFXWM/f3w6xjA8XpaA3QlVqge+Mwlfh80LwLhUirlUQPX2TWzrdybeA1zU0uZO5VmOq6CDlXBR6njnwhNaNyP8clLAPgOzq7aAsekLOzvRGQ2ahAOuS3kf6tyBuuT9HVM0mLCyQjBL2/tTdnDeHn39IsR8gSF8xCTaCyOcUcKAnrS1ljG0Pfd8BCfLmTC80/e+Rk8v33p5/eauQ71c3kvgnsj/L1JXoi7aQDyp3K4EpFWkZx9+Y50lld5nXT7jb7vR4FajNG2jlO8ggZGf347xQMXaRnEsR6diz4iNg0vA+HtCPi9BGuWVx/t85OMfk6wOCbT1MXYiB1f/sHdvLwY257gAxPKZgXhNtLOmT8w6u9LBK540spCjvv2jMec99LadCz+lFof6OwL/kO0PZD4jfZBHLxIL8t/GH57Ts1kIr0c4EG6sctgvK+n7YPM4UafSqSsOJaWaehbHvzaD318zDERvL58VcBVp0OBwah8RAHhlPjXjMbsiXcehsf6PzwgOptUdOPEC4Gc3MCJKA0expvfS3nJeCaEvEoWW7eg1M5/KviBCJu+PdyQltdjHobUI/x9KPgWgz+CvvWg8hvXRyTifpf4BH8C+jb4/SMr+A1j0hVNPEjEqwv+Dv2aftCgnbIENcavkkgzQB5IGMJAD4UHkvmK62NutrW/kj7QJk+TOJ0rH53yp0wLT1E/JaXrS8f+qu4Iyt8K8jFoUB9ky3DStpTp5aVjv3lanvItpXVFyJmlsHhgA70nyV8Fgat6YogiM1/zwaWq+MpYPt/240eczpyGsyIdqySxg7Ov5sHpivbMSm6JUFlNg2fB/we87wnj8u6Jagnbw9chjF9PSseq8+KvqA/K2g9IVYxCXzCir8DA1ZT5DGlx/OBvBtTLh/Mrz2ZOh5drq9L5dE7sKG7DEMeqbNlOBTDuJOhVjFWsZAH3bvCcv0Zr/dS4q8mjzObh1Bfmcm1Tm05mh40TuvZfAe3Obn1Jl5mmA5/z9HcHnkDwel5XCD+3Hr8CRL4ePhvg07r5hFjxpNdhzGWEoFmoZdzyJX1C0QubF41Y5dPbVyLDLvD2ZuQ2FX5vYwxzScevBZGPsTieVoIe369XG8cK4Hg3wXNe4rPz1C0nTGUlPg7nU52xKy+xG5nPK1XU30L/xQ/1So8QvyxM+UrCHwivoY2OyDHEr0nlY+Lgo4L6WOGIz2AcSxyLPEHH1XchK/v42nTldN7s3xe0w3CkR5CfLs9zMgyNWbS1/VathFk//kZIA+P8JnQ6uyvJ+SMvzzEfxBrtzgMwrGDcalWyzFWyRTsBxfkNQn2Z4ESHIlD2OEJ+E/mbSO/bkSNUaIT8J8ScLLd2QxH4lYRlmYGpjPmPj8iXebdvbqX8wZJjbCdf0upI/lRAZQNdJzoPFcjBba0/FCPPBpxz9F+OHF8PT7NUVC5a4ie6yIdBwXduXOY3DbmxOUYNMR9ztJUGwfZhpPBwETLqSbf52CvkF14KxvID/jsItbSJfrP2Go7BgyyNYQptymsxIubZh6qLimw6zysH+rvPOcnaa8he6x4nTuZAlUUYsv0LGU9u0zvT1/ScF+JNx79pPpeVctEIlxKOmJLSV3dz5xdjzvPR6SvwLzxKR9JFkD3mIfALmeg+HKhwUPBrlsEBSL+C2GX9Gtb61zkb5C8nerBX7LY6kotYecsmIWC7gSdfBfoGLu5s+u1OoeWy4KSaJooP+XjKNhWeT0OJ/nId9V6HZPVEHQNjUPaRBKgVLkxJyksDcmBhjhvwHsS41fWemj9Y8gu27zeRHy6/yM8XYT1QEOTdMbQFKrs4bglzRfa2gmN3PhBPcgfx/xAvZrDjiL1GqNBwVnE9OgRHwPbX7XA8U0r5bfDwdtr7xIt8hgyJ7aOROgvGsjr1qubkclp20AF+bkT5SlbCNtIt+J/B9/IgtsrkfbLmAgyxV4v7wBSn22cS3qhvwFl47Xst5Yg0QBzItAnKQl6jHxJe4vh+5jmTofkMxgzPTP8GyMa0oaCDUx1ugCjOcgS3EuUvg/jIbkwis1LDYBtc0xnsMgxzOgN7hvwgyn3SQ6F0GEDPAwgfPvbxpXVoujfbb2HleAKeSMZWRmWBnXRbQ4yuJRfx7z1o5EKuMbzhW7p/er+tQ/mjU3lwxeuqnMiEcVCGTwpZlO3O9TL1p5L3J6ZPIXYVEYxsL09ZUdtKlyGHsZH2utP7d/bpzf/Z0PgwnZ6FrL5P+gZwjiK+ZjeegaXfr3NR9XvKruzHEzRcd46izVqcx0rqn8BYv0de3kOPwNPAvcD1RPPErlyfks1laFWAbQjmo4x+vs1gnsgQopz6w6nncjnAMvugGOVhPo3pv5qCNxEqaZ/zkZERY3OASNC3JogRU/ZGnNxJezGu/qlD0AjjevKhbCsq/isBOQ8dRhvPWYngSwcxwXinD0L4o4y4H4PsSWfebEW3kyfJH0G5Pwl9FpN7a4cxACHo+rvphfHoAs7gPUzOufTnzf7LqRtC+iPwQrcBvsng0xwX9OYe1GpOa1kpFy9E8APhT2ZROCeLs4GOAXhQ7q4cHiigf7GaeVRfQBGOJn8SdcehYHuEFmTKQH2uNLYXPFDJy6KglX+iMMT0TQZk8A/k4a2XJ6D9JOE5VhlsK1b6rsjrbxjkKPoOC2d+fFHWFUMBzILY7RDzUOZhVsJx1D1AlQYtL67KGmEjFlJG249353SW+hzkQ5AnDUb8eOAamX8dvt5PXhz702ncybycgbHTVQwzb2d/Xne6IziLuo/QyO17viugqFWgWYA7AOfdvnxVqYyx3Y7zfetQDtWQgQ9rLK1hLMx9uWVps47/T98dCzDciPIzvhjdfKTs1oJFL06f9IwjmNxRIX22Pn3ZziA4Ba5wOwqaDkR4S7juo5/PoQwjlDxM9SX+CcmnCGGAdLoaJbwTnq4i7e+dL0cZmjC+teAUGE9Sk04+qY6BbKwan3quMjnp/irRZ8kfxaQw58GvOILGqnJ549silUcIuVlAXVtgW9GfwCCuIn0n9BfO5VRzDHNDW2/ee6+tE3IoR1a3gvsx0k6RUCDtyWEBYxtJ/yMxioPhdTbtNABPDWXKbaMy9mt1PiFjet95bPmHpM+iikZRgKi2c0tbgQU3jeUzIxjB66g4gDINxO2hz8c6BewMY3dgY+norBxXLXL7PrjvIu1Wkr+2wXrwbB8OJcM0b+FuGN1AcOrI4geTwl7whh7A1isHCuJfBph2b53T8v5RKSM4iBn8HoVjGJyCD+9kgskBPa53vpx1jk60H/J+7TtLxz3ErD8nqJowEIVpQPn8LYM9yX+OsB+9+RCzPF/CdvXjPdIb6o1YZXwaEBwVwK1S48RguW3+6NuL95wHD6A82SxMScvyclcGD17igKo2vdYooECDUeZLkNUdMPNuBDFU5SMfxieD8ECzAB0Y2SiIMnCNFatgnWnnw6CSL4TWWSiyN8wv683bCSjakux6TkX0qSCN61gcz09oexshVhyJETS+WN0gvA5aq8AdD5++xnQwdcEfeBpNNJGe/JM5veuGbSSYofhNK7m2I62NBhyaOrt5ZJQjTWNi7LM7/Xw0zSYNs9L5KrkQOnUYh9tDcI6jvkcMOu2fbOsgcwBNUl3M0sEIY/Fy6LA66FLeWENfXZgv2gQ/lL0i4Py2GxiBk6uwkE+MWuMKL0m2dO/0euUdbKSHk7cPlSkmkUy8NQD+b5kIZBpQnIwsv7WRx9k+SdOEcjmpvrjXxEz24frpdPr6FnUK1JVGySvMKjIsvEk9ru1hyv4IkwscC7hEEYi2DmgQ4wO7kmBfjmX9dOi9LTvarkvrlJVPx+APkkYM3hXvYvL3Ej4IkSGWw6C6FM6KeFMANf0ddxIadqw2pGN81Ml/pKGnzF+A3rsQzM9QpkXV9D0bZQWniiCEoRL79oRvMVyATGYwgB9jZI5Lehqpb6fUU/4d4lMom60gaWP/0ggAT75btvP1qq8xRz55FMoMboHOl9SRJy2uwfcjvf/5K9I6Kv6iQtrHLOfJGeg2jWRMFDdNph5hx/1i8n4u33uAtol2xO0CGuls/cTIAcNTZ5BMgD5eyNeV5O8Vg3+FeAyYfwoM/gNU4nq00JOtYWoiIJ5K4cPLPmj9M6T4HfbbFwzlXhvlKqZhW8CHcsODM8EscuHxG7nu2BMevgfRg+hcw3fijFUkLeBJCqYQJoLzVC0nfTWUbyPEO3uM2+sEjau0Ds88jBUOpW8gXZFNqr+x3gPH4O2E/4GPO1Dyj8OTW2HEEoIi27YSWQmolA5DYy4qa9SkY9QRKlAPwV5EKOtQpIE6qKdwCHuwghF72ita7pzWsDKWDsSwGMAKJmt+NneS1QDt80Xo3Yn13orcfkmZY5WP4F1EgOKi81DePi42EiRfkSrBkOgmKaX/stUEkFV8t/qVVPhZjado43wGcxD2256eHRxOmX15Omz78gX0z5h2A2d/+tAZuQILoLcPaOADAMLu0PHNiOYaeHsE/mfRV9S8Qv+yfttHXQZpoVKbkEFXQw2gAcMaROKT4Phum9IQN78+UFD1SPJSFHH53HSFqL8uxQFt6wE6kE7yY+yuozEAhOU1XGf6PZ3tVDe8udtOn+H0IMGDlgWEnxKu7o+y1WIk1dSxJdP7smBv2BZtLScwsc6tEGNWlvEiKbQc+vrn6HM842UFOIDxvhVeXkP5UXjvbuD78HTIkHLHosK6OyBqE/LVxoeXfUZTyK+XzKrQ0pG28d70dS3ly+hoBtb2AXGQVdMMeHiRzG7gIbNy5m1NNf2DEzoB/l3I7Cji2AqD2hPexhG7Ul7dlc8sQn8Y/dmv43AMAQ4AXAfifCvXT0HXF6xvxTm+ROwBmV9ys63oblebkBFTmDxM4Fwo2UNZSJSxjuSypvfzzBM7HIaWlNSk5RfA43ichTTUMWNDu8EB0HA8Dn0EyacnpuN2GOr1KwZ20G6AUW9SKrwApYagFJY3Yb02+DBFQ8ynVbE9dVJ6oexuJ96KEtQPSYXpJyq2Cei3MC1VGCeleSTCYrYPhP47NT6JqkCUaXy3wvAJuNsva3wYRc9qFA8a6+emjiDwbdMeYIB+V9PHp/LvdCpTQzPl/o66nyj8BooyBT5OhIeqBnQKHg0btJYkeeW1JQgZg+f1bSOKqUEyhOQdxN8luIq4suSCt49qlH8s/b6dVeZs8k0YXJe9GPfidCVc15d0deh5UoLmv9STa0Vofw2BLKUdf2EgXSmDBMspT+Ewhhuh5Qq8GYBn/zqGZuWPRfeHyDjys6lSXg0z0225rDqW9RhfD+KV5KlKbiW9GHwNWKjCaLszv3Qbv27csH96PX8mOAwvlaWI2wL2YaDPShyWc2e+ZCF5yjIWLO14iM62gSx8FaFpFBMJl6UqNQQnUamBIpv8+naDsSJIxpXcT/AAojsxznSjbQzZfw4Khgmpn5Ciqvx+5Gk3Zug8JrtXJjW9osanFJdz7fHYoPT2SC+U7GWK1/XACIekfDq52wIazroJdIFCV8lHDWNjuzWcDr6CQ7oXoscqD9L5gUZu7CoffwHh7bN0qxGIHqqgz3Ht5erubsLXt65nW+uXwz6P0EmGUit/wW9i+j6jK+Z7UfR9MTgWvGQ9TrCZxEBoxjOylLmLKF2AgWJpd8Pk7+BbUWoIj8H/zcSQCaavoYN4kdh81llc62cDcjUPbZZh2vntVNGimLnz9FWZ6xC6MYZVzMkiEH5F+DH4D+O0bKATmUFSnpX1KsawO/G3YMRX2aTpam8fRu0FaXp5JNyHfj4HEYriKZm4NRQ1r9A/hbEt4IQpbBnVC5fNw+iQpkfIIwgKzTonS8H54mojrspXWH7BVmL6swgd61NZWJTaD1nfRCF0dpyx13kTHR6H4uT9Wy+Cir+YoMcuMNH4ipi4sjkYD/zoRVUyeS6C+bwsS28mLxpo3zZs7AstDG/UWF48JX8nbS4gHka1Bkc27YCy3LPn5RS1CtEmq3Hb+SzhjzS6DGP4NPGPkbljKh2eyvFacF+EmPSiD/Dji23guc3bl/LPeA3FfHXCE5bj/RbS3nGWX0cbnSkrnFvp9Qz2Efpwq7iG9Pf64UwuhB6OpRf0nqXsRozLOQ5jM6adMtIgHJt1TSi1heOZ7wnUY4exW1oODs3jROzlWeA+Aw/sEp7A+GeC57VmPaGO8IX+3IrAUHfDyb0eot/HOI+nHDZSAF/IojTjf/rYCMiLkxcrOx0fpJJHSX+FOQxDtxEOynLH84pBCGAbqDu5vqFcNgQhcS3hO3BrxnEjFQH2k2u00i2Shx6eMP4XRXo2j60fdJKngELQMEKBqW8XIDQnWeE46avZu/RnNv6HPmILRjnV8c8LbAU9pg6jH54avbNgPTvEgOCBAq9lLVcuDkOwnybG57t6gqt93q8espzrhjejFP7IaDX9f4r0QGL7kIb0coi0/6gTpGO2iENC5xGKQXkZBhJMUl4HzRP7cIBEP31Z7f2wsePuNQ/epiFHNGctnmgRuLKqZxeUseCcSXsSBnbLYzxyx9zZT/7QgvcEPdaX9ZJphGzuVjOI7siWvySZDN1nSf+cvj/OgRqCey3jHQmOBu6Ow3b+QKbg+C3z58P9mPHb4fseytbhedl8JCtxBsq8YT718LPENOUlGO1P2A6eSNtRMFSJ8XXD6I6g/rv0o3ylTXWA8UY7CPByGefCVR5RzD+/GuAX5HQC18NXXRnbYxj6O6trNxyATqLeLeigDToSHXX0P/ptPzAQVz+/LlyYxUCYbZ2cA74Djf4HdeNQnLcoIUYyEOlMRmA98NovIUS3oAHSIJELUcG3F2wLyTgi/wL9q3hqkPfL3FbkT7kso+6mutSb5xMhahHgJcbyS2i9mzTMlI6C/bk4ly70AeG4P4gH9pP0zXjk1UyWp5oXMpnvw7t3dQCMX4P1hDIfV7GPTRKgpNdJGWLOr+XK0lXpLuj5MaUPU+THdFUMP2qDPqaAIvuSr6FQx7gxQJ/uH0noB0bIwdhAma+FlcHnV/bH42PMT6nYKB5TE+AQgi+Wyi70/QBbgzr4O5jwIca9iMr/A4eFHq+TJDUjeD+Q5OUEh9EAgzejVDeR/iC44+yTtM+vFig/jXAd5X96jkL6bWSVKWUM3QejGwvoU9x54A5F3ji2/4VoDRN8PeW1DMJXsHyWV2MXpF0Ey6jTEOXFMZsuI6HjUVFKGahq6VNR65HFt3EIX3eeKZNYGXxGPVn5lc4rCvTRfoBRlRhek/qZeAnipvHce6PcAVfA9ZeZvD8zkucZ5DmUnYlBaqWPI4A625MlKq405rcFFNYaVoQjSZxIOgTPP+kpdFfH6Ij69RMRrp22BSPBn0I7gkfhsB4nHPXVkMAruloUaqiDTsNLbLdB8OteZJNyeJCX/PGm4IM81W1CsAKCuAFZwkjj8ymiz2MkD2AkvZmo8xH0ZSjro7TpTPD5x+eR5z3g3zEPuVfDF+mbwfkc7S+hTQU0nBPni7+QhW+dD0PjXj8NZwnu+tzRXEd6UqAkJbvhSzCAvWmkHG4nPhYFvoJx15G/mvzNj3AdTXwDnX6AuoOJvc10cU+2qljoIPCw83SLCp4OoAv1332R61Xo+9yp8mwewhzSxuvReIoKJ1dC30Pp+K20d0ezO47Ad0mFcCTEFG8M0BOcp7h2A0GnFsZH+VrGfCV1t0MXkcYq/bRjh9fe8M5fsppDqFXgOAclNdnCQnrHAhl0JSB2+xCCYCI/Q1i5Ai9J3SjKO+FduuI1RzBpN2N8voHsg72BT1xSG5dtMeObCXNrRgwNhe39pZ+tRNDEvnriazXx2hO8rKbPZ5nMKcTV4G6xH+unpWNS6TyO82Zx1ydRZPN6aGj5Jv01hAXQz99B8xUXX8Wxf59R9NZD/trLpq/DbJqPdrahrXW+EuUD7RfQN3ode+YLKPcr3faXv9sXrxfRz1LKHoSv99QiT/kl+KD3V1BoaclPS158Idm5uCkbn3NY6lwZ218OtOsF7ePg5XXEi5GxrwwZXqbsLrR4Twx/CHX305fls6ARlyeUjSI/g7iljBoyGh6yiOeWN/qcz/dXpyF7aPqG/+dpN4M+1hIrl5CvMTQ1mJbj2VSeOkfH3Gxf0LDNHIJPAu3mHDo++6UvH3DvZL/E8qPji0+LZGnz+LFXDraJOFqsF9Lj42DCCBykNzArKPjpHLwkGltVhefCrZwG7pF4L7c/C8HJr93KqlEmh2aZcXuBdn564TW0mwBtshE0Sq85vP/3I9I/6c3+nso1KGl38p7c6e02AsoLdRjcxHT1czKUjaer62zHZB3PmE6l/I2U+9yhq6m7AHFbrjL5FlfZ0Lx1sAIE2+VIJqXlY2hf6sEBC7uI3VA2DUMZdqI+tv4gKkP+YoXvSeyjYVfQ5mDwr8R9v0x4DOY8JVXJ45YA7WIlpF9XhfGD020qup/Eb7+D5yN0jsm59f7G6pGsGNORxShkSR9foEIevCXh5xHvJ7hiDEKg8uTnB92uVzCxi8B/kP51xvxF8LrLa+hfIs8ufdMVRsP11PrgsekJ+lngDievPARXM+VEFPxDNtLWbQSBgEyYJy8hVsHD4yD8nbY/6sXOQWTpEqTRtBsLBnE5e3XYDLDPBvTF8sAhDllE7SvwL++4XaQZgO0cr3HTdcQk/kj684zgG8NQFvL9mMnfIww/7Y4tJl7MP0UshNcjbp6ZbmGjcFv+0c9ebM00dBVenkKp6Xs54VaE+QRG6m2RcvIKU5xWAYWXhq+7eNwPyaSEtm+jD0/dfkN8GuV6zThwII7ja8oCJEwwUnvy4qjb9J9I4mT44kI+HNs9WNovSevlfcrmQwgL3SwaeXRgW8oEnWHwDY8fJH0nmUuo9LBC4ytu2eyAfMogxo08zjMLnltuv1aOKENGKp8Wb5P4tAe4v+K4em7Wqf2Z7M2/oYSQK3EpRuUK4+tKK0G6jIqXiWN1EY9+fsJ4nkLpV6I3HjmOIfomdT+m/Euk9yAt8RgX+PIg5HGLIQQPojumOFgho/HNps+vY3RHE84l/Sz1kA8oYVfmOP0GKyKLn50DJQyuWV2Ghn2UzKIsS5N9ZSAf1LZQd/WScT/UX4b3/WtPjI94KZPwMoIfDfNDQ4sZECOaSwPvI6lUzGcoeEy0NLYFMI6eMHC0g4CmSkAU0i2F8BWUP0R/vg2/dgWG4/6e6uBZPOrKnsQ4TdOweTQ05rIKDMWgUcaz2appdNdyzfRu6uOdMzvIhBbXFtmsRhF1RR6kKdBHhMik6dzoLIprVBPSpbG/BTiZaxV8QbJmEFsn2vcOBqkmLf10kJCljStaDr4p4b3BKmgcQyHnRCFjUeJWBO1tI0uCrJ/KGA+hoFCHLJi/NcQqp9Pmw6mlNcgLRS2rS3cRV0PbdtFvRkt55pci3WGwK3mV1w/x3g/OnzJ5xSET9T9n8u5vk5AAAEAASURBVNczJ4e+gUcS6eg2aL4XHLfP0srnR54DHACQl5tWhoKGrVzkyWdVr4f+yeC/GcP7EvEaQhN8MP3F9k3qAeXrmfuQJ0YXTogySKRAet0oQp5/pWLGvk2gbJRBLpT1M9mm4fXeTtlhVHqDdC+CXkxjcwVaNBcjZDJJFqEwssWgi6VbmYDQUPoaF1JMJ0N+CnoG6h5lElYgSI2/voY0swCL8diZAg8HwrWpkxlzzUSouScxm5+hfneCq52zIF2TReap46+Yd5wW5N7TKusNuYwsCwDXo3lp5e3cnvuu5FIMom5gaoDYfbKa8jvo/1jigdTbjSGH6MN/FuYxePzFmJxfHYVjtC8Nx649pbWsH8x9iPQDCyi/EEc1OTXAgkbHUtYwIesPHK+rboaX90F0MB1IQ5ohO2j61MvNL7Dz4PqzzNWTOh+W/wV4vjTrjfdyxvFD+lQmfcgPg5ZvTpgHvQje8wzC4ORQTIKo/OzPsWn8PnR+Hum7Mba5eYOWMXVBP4+tOyUzQJI0T6FFWV70isZO0LZAkWEaV1zI5HycC3JG+FEIHmBlpmAmPchQkX4+PjXG6A9BOHnK2bBNAE1P1VRMpcsfzOANsXhv5D6ksCmUlfKxrGzdUk9o3vfQfNq9CzytrEU58IoDjmbVo/K/qe8FTY/wpSl5AXKtQpS3qAzd4Z/ji5WHOqsNsQKZt05qpFXUSNPmbxR5jVrPCqEBupxMRaEHMtazye4OYlzjguPDEP7wit4c9Pgditm48JnguoUW92T2WPtkbt3rYuUhX8rALZurkg+HH9WTQ5WsXMeU/26C18DNGKdzWLiQp1M+kX4s91TK+SuuwI7nYWh9bSgr8GJuKQxJ69Yj44fg4UGQJ4LvCn0Awf4dtKtW0DFLIt8RhINKUcTcGMBzHDoA7+N5jXgOhvdHsHT0jj2fs40b7oA5eG0/MEjbIddYsj18WVuLErPi+NNTxzIRejAFKqLp25ngt5jOe4OGk2Dweops+4HjbO9PTaNPDyjsT6Vyafs5/b3XMgJ2F1s6v97cidXFbUn0R96vTo+GqUnM2ImUD6a9kytExD9Xjawordjkv/igBI6TL27g88/xhdERi+M/t0yWtwQVzpXgFPj+LfWhRMYX0n4SXh4HMRLeDqVRf/hVwZ5i0I+wUj2vYwMX3xO8epPbR+1eYjV9B87o49AeS53j9rCDv+I82I+K7KXBZeA9/BDtJoAAiJfzq1gNfjFOHqZiOIMo0Jhl5hrCl2uSpBZeXZWUidfPr4Pmx0A5kDCMtPhUp7SzmOLAj/lrUaauiLsZ0MAXfu9ay2rMftnT4MXMYznbm1Ug5/LerN2OWLBNK2Am4Fw4DXPT66a1CPyvCOZIBOS9wJgEpFiHEvgbgLnndc5sq+BjsrZFMBDxhz/8nkxn+ojJI+0TGC9A2PtW9ucq4qQI8ZmMqfC2khM88Ljki/uHb0FzdSbiaxzyRpQaD7HKLq+tQoZvnW1EDFytwYb0Iz2TBk9mPbAqaiBJZeBPkvn4FzviFCAWdCbTnnKfUnmaGHvbAJSVotkl05BFhuNKRDL9hicO6vesoPMxlA8TZsHAayB6AgjSjmtK8al7q/NG+rAJbIOpk31XQvmUHuwFy+XdeV6Srbz30s6kThq3si09H8e2dAx8gNeJeXFVHUz6IgJV6filRRCkGf/Ie0JrwygTR/4oM7ZqU/Ba18fkHsApzWeFrazmKuEB8DFAaWRdbNpsx8xvkwEyQuQfk6jiOwlBB23/GZNxFnVDCGEUCKoRb+x7XuIoHCdfUCHzCVZy1m01IPjdIHgewYmKU8lM+i+gTDMhZtb7cVW92arguXuT9npH4xuH52TBDgPx1R3+Aj8UBBxjabol4i8UIefbZkXI6t1GWr+AvhdT9hf6uwMhufqejVyOYLAewTtIX7pNO0ujIl3w10/J5ApKSR0yg09pr5lOmhjyG+SIpXRibDoY+XVOBO/92e8yVoXd8SynMzifpqmBj0YIfBuZnY515ds8dyi2fZQy2A6mio7APLS7zqLvkamcFJY/RqNcFtDmYhiwnYdYrkZXkT6WQTHtwVe+IpINgNxGMvX6NyY/q1A+t0Ece06qCbC3GYjqpwv74Uwb+kLvUGS0kDkdlH7fZrMGO2oBctwmUCjYWiiH6aaZeE22RLOZ/OcR3hAkFB6MysEoygAUZSnl+XbNGVCI/mspf4u2ClAsr0uGQt9rNRUwjIf0yzD2HERU7JLHUFAMbwJ4/rzxwQy4XANAcYzkg7+iU4hrNssBy/PgGDcF6Tsex3AXiItRzJsYzL0o+UsoRSgzY390Nc/CUq/CvxF0V+0uBNvaXHAM2FoyeArpyWm/TdXpKgRq3AqAfBFs5/0qDaWAYbiC19fwb18cDiuB7zf6NImPXsnTE+7Ru8LjivTzFxOQg59gUAbKzdX/YPCOIv8Lgox5XN+5PyszWW+IO155UXaP0OFPwDmdpfKQeex8GK+vG30ZBJ+YcTvdUr7qgm01RmP7lF6+XdTJyIPXt+qVXzI4H36GBxEKWoBzVEn5QHBXMKYu1Nmmc+YIWqDu+Enmof2AcFSefEKMfR7U9+rQiaQvBQoY+cVEziFdvM6zreU5kG9FxnltILsd0vODGm29bvFwwDe+VTwnzrocYTFKg94nZUxIr/3ZclH/WfIjQGqCUK4YNhGK/EiEjHkVQqUx7VjydIwZRVMJFlHn5Ps1rm/i7u8kHSeDxFb608pr6njcq5rDCYoKKOqlPdiegX8l+d2gH7cO7IdBlqHpI6kLuVHmzqIz20h/qAadTx3aQyQOZBwYR5c/YxgsE34jUyifkCqihzi2LfSJe+nJDJyij3H9BKH8jqXxxxiTX0U7J1qBZ8x43Mp/m7onp6U3sBvh336rQPCwowSP0gsa9b14/I00C048LP3h13AAR3oiNEbZNwJ2XEE4iJMGIBE6Y5WGmAo3QyMfN9CxJGXr4dKCDIVoI3B7qoOZgVxYAGMHoB6sHEVb+YSWfewUwFjbDw6SVhpvY11qBMqzCSXyxu9DTMBdxH9D2Zch0M+hCM+SD8UCr10wK1MQlMhtlX2GcJ0waOqd9ZqCx9Ja10pmpTeKdB7a8yCdXkEYAY5vZ2isWwLHFd6YOJ9EvbTgCma9T3L8Cn7eAMIXSavwn2aV3Vsk8sqg89OpMXj6I8+FF9guPoAiM4i74fEown2007CVi0//0yw5A77HPovSk6moQdlVMoxtEHWVtdAYgWFfBz7XP6sm0QYDrBeP+ua69NrN/lVI+2UK4vUr7Dj5IoX70J9P2vwAnDlZ31RF3w4O20p+PJYXmqlrxsJ6EDzgKaVP3ys8jPqvz0+ffPFWE2JIhhHOAR/Wgg7FxdNfklFGde4fI4aFAMu9Z6vBNsMsYkt+QyEiSHaDH+tbA2X+eIsK6cWqSjmkdlxAOMEnsXoW5wHt5jYbpMIpVDNgEgqkuR9bAmb6nXh5t1ofoWx+ZXpDuAunBypauwHj7WSjntCGHjYVBwQamoZA0QaQBybNh4J/SKlbmKGWkfaoWmQVvk0ARzRBPI2tiE7CA5QXCBejpWcPY6VAIX8BzgwQD0GJfoA2fpxtH2JIb2JPpw2r0zqUuIItWhMGU/EMMe1nYDEXwOc3Qc0f/vXN8d1h4KP0oWNp3hdDJtbYaul/TXd2Fxhqj0OQqSssdT7D2WUCfLK17DI8bRfKCL5GSZQ0Y8C+kuU7mbOgXwrfdcQsYgEkA892fuxoPPGZ1gzEwAiWdzqFGHkuIYypSt9SuBTie9AYsql8s7TzrJK5GgnxGJwxefkRdBCueMp4CZN6B/FFqzkFRja+WfIiiN1yZOpagsW+3Mwwgl6c5JKWln3vDKBoIjAv7QcFWUez6tQY/DDSGMqYl+Tx5QiBcDp5v7i8F0o2mPKVKKLKpAG1C9jT+mDybmikj0utnc3ETEH4zEIvJC7NOOyRKGUqywj6HWECyKLiyaPKskUjpL5ID2TTHhL4SsscwidQkOslrAzYjvnJAt80Nz4ChToCJdqTFfgLGJy/3cfCmKwYlK4iyrpkb8bwEG0OTO9TPoDs5lNxMXW+rOxHYs/E6ZQvpq/+XE+/yJM+9kMffpNzXJf07YcTGLsPC/xjLrSgMQI5z83mQMVsfCw1UIqThk7ME5Pz4gu8xQC9LtCCTCgsUXhi5aQiq9VzqL/G8ZHVueq8fOD5DPLvJD0Oeq93IknHVpekuLncNDT+Iq9R5B0pd4aQlCInH5ZYSl9fpOP7mcR50F+OnIo/3Q0BXwRuDaTRDTntD717WuDEGMg7DtM7JMBbC5aZvG3lsjr1QOVomJN5HC5td4TYgGL9nbI9cM8HOPt02Od5FGhe6p3b3R3c6i3dujVNZy6ZrGWTETKrzVnMhNeBbiudFL1uGCHSp9t0u0nCtFUqiAphui1wKxTXJzbKglvcNYzlu9C9nbTF4cFRVBajZJAEqWtw1lGuszEkH8Hywe2biS9dlBqDhuENdj/VL7++7uO7bVdBewwT8R4Jk/aGud/V2RM5XoOi3kJnKmofcGuoszsPPLqzhH0AfJ8yuYdJuJZy7CtWvE77p/Juxng9BfbthV7w1twDWVJ2GG2WQsNraQ3DMTowP1r1dhzAI6R9i+F02g0lPpb8ONr3YBCNOEOahpy83sqahlzMKD/lTJTOC7F5wdsl/uTbz0nf3YtbGOSdv5IFqUxdddeQVqdc4VsF8K0bk1U6784HUbG/rGrHi2BSuRgCtskAaR3bCWI/7OM9PxV0MPHrmbBayhczSc14Sr36SyhPbwzzBXok225ouBA670SRoLEGDfOnzs6jL59ayb1tTpSiACcjVjoHLL/mCcZtgngZrvtzFSmuTzCCO2q5NrJ/GtuH49BYV5FxKxyKR+TLrqa/TOT+vmIVn21Am7DB6LurxkA6gPRKxuPbDlMo6Mr27zSVm7x80F0yBs16L3nsJvlRZvDVpO3mBvpnkUzWEfvbgG/DOJzPSgxlHjL6HfFNfZA77TuRX43Ce6jTFTqfsK3GTJ18usp7YvpzCD/HnPbBORwAzlepG6gc7JP6cE6kHYyKr2xBi9jVEPQiRLk5yj0BV05LCR9jS34vBP157aAjDrTLEZL8r4WItD38aQ3iCwfgj9BQwVkNrn0hvvbvsFrrYHuWbZMBMmAnJL8vtZYJux9Ce1PYD4FY/joksgDDG0y+O+XMf+zZY2zUK1sDyUiEIpinMIz7KSbkOiadvMJtnoyyY9zj6OeH0PPTFrZVIWyjwVjQctItoqq4Eqoc1guWtwVRx79YUVFIje1RLoycaFdePyC0zDQGdCsDuw9+JoFPVHRMg8j75ImPWh1B+e8YT2E0YyCtt5amcQPbVHasMbbvI6//Iq1SxUoMw+WE/ShTM79On45J+TgA/uL2hQb8WoKGZJmPe+1P7CGR3+j5FOn11ZCYxRjo73PUv0E8gts16cVzlZSfjRbvSTyYuoNoT7K4nXN8OofcsdmZNMII7Z8gg/IQ88kAfTDCe8AXUDYHef2d2Ic1feDdk0xp2l46yiHaY/z1rIQvUe98BT3rcqBcgEQcAM0AIfoj79sM4u+wIK8wlw0hFfC2MOvE4ITiyLcBN/wYFH9BwY1VCPb2dPt1jRdogNdPdugEaZw45phYBeUEWGZsiMmZnl7bVUy2AHALx+y8Dzo/ZVIPJnZiDIKGwl8YYgjffxQYObn5zXRxoox4S+DYxM3BAyacbTT2oQKNSEJeG/kUye8xMstsF4MgoUL49kQF14SP15DH+EBLSjBYH0qWvkYoFNiid2Zp/TsF96m0MimNqExjZaeSWpav0o7fvLEntHm9BTomT0NPZtsxJmvjieoAyg/CmD3Kl1/bC3Yp3b7079sEbq1FCNokjcUVT/Cwy0zkpUVlGCblzoc7hxIYepH0LzG8W3LjszGW407Cxt6eku8AcEM3kJVynEGFMrIPqiKO/mxA/XB2WCOzOvfe4UxOaUGPuh0elGO7ASn4wZrOxivSa5VvQehktjvzIVY/iQlDYj5ZoXT9WBDzkfiEvf3Fb9/VpSsA2Q2TOw/lHEIT9l7r0ZqVKI+/CnsuXtmfib4UIqOhp3KpzG2Cs5UNzKTgxOWTGAWt/QNBPHkPgIYr0TK04BnrCCpIAGnHVNqd5yJpNIO0zWLlMkFbr3caQJo7YUPfTSiihws5HfQ9tLFxAEZM+Y8Y2Fz6lI+cYRVLRQ6llTZgfe54QvEp16j4C/49JdSg+uAE3en5bJqfXVChPaxJO7BiY/AWhVtJdbxVgKb9CvbVEizXEHUg86n8KDz4G/A/okxZtQrUef2ZTVcM2fZ+s8WDLeWjMxNia57herJ6J3oxnXwXgvdKwzGmqDvP/3zg7eKYARfYInh/yCceDkMQn0CjzufQ4Cg8kYbpNYXfB1EJCl1Tg/FI3ov+egysoTpdQZ1oP8IDiaR5KErG6lcGna4Y3puQ/k3gf434cOrj6Qpp0i/ktwig0IBAgiYBKkc+mVnRZpETG6umBEj7hEp3wghoWaQiSTa5Ls1L27KrCNHESoJ9WXcwXvos8o4zB/HiwQQcUvOFZCAQxv0oKwW4D1Nk3h9M8fG175A+Dh7eCMEnpE1Z7iSUQyimMXWC/MdJI3jPYAQz3VF0Y2cCMj4yDnDE26a5p130A08aDn/Rn31quE/C47uIX4djuqIvJ73D0pPgluO375Yg2zFeYuUR15IwtzdykUf7sy8dkHQakYv99Sajj+eyOX2JGhzx/pluiL7DwDZNwjxWKkYQT2MgcN8mCO9H2fGMfvhyFA8BjcbjXkHZ9RirgnLVUkBdjAnNz3ChjcFWDUW4KHQT6cEjUTSYup72U0GaQNwNt+2kxMRTp/DzCYNMqwBKGEo4ANL5vSjbbgliawWCfZnWkJ5jG/lnYtuSTLeOe1AHz5KrR1FuQBnYSRYN1H51Qr4w/DE0ZE/yjTiVbnWZs4FeiQ5pMrKYRYwxJhPBAf9ekvYTxGlXRWYGCm35u2m3Drqwlm6tybtCypsrR2wtidXU50G6hBVXvpLF6Up4MUn8XzgWULYJJG9/dJ3OAzz/hXAR4QSW299QjgqkByJePpDeEuTb0BKur6umpLIP+dsRYKTjdEchrEeZniO/eB1GSF5ZuGtSv+RppwJ0p/0whCY1CGYcyoQwVjqTWhSScGKuxzpHoDSrKH8jErpnCavICgRXndavfhJBs2LW741XNs313VikdywkzqL9PsRKXc/uIQx/QZf5DQmbN23cFljnZHik7yltesEKvWyGWm1LYaxcxNaHhyexqgoeyfvImYrlO2cx2Ux6KMrLqYJcDe7nqdM5BH+M3wMRZfGGhbCALOYTbOuK7/t8KlbTRPCIS5BFOQ3/CM/nUd9Zx0P8HuT5OPU/xoh8ZM1Xh56n7QjqNKa+1Lv1jU6J/dk1l4UnuF69YhEGMJ4xoNyH0m4f+InV0/7sH9T2guPTwSgkDV7n9p0+vEaFcBhefJxpLX07NtFcuVQPx70ZzMOZg+fOyLlxGx7GTbyYBkSRV47iLSH8g/AHd0cYu2lPiFcRnObcmEnuHLBNBsjQ1k7gH0JbilL4JEhMPlEXQjcm3ycqumKchyGgB8BbQVzBClcJflU/jNbJQik+Cb5vChyI0uymtjGZ/KWKQRsnQyAZ//LJsW+roty6lkBhCRPkUuUW7kE85T3weDQNPBW0TVvtVCyHY718qGyjCefA6zdQqhWUyaZ4Go314r9Mvb8eZCVFG5SbvNvYychDGidCY47ts0AUBuFYfD9Kh/MEnXjKOkAk8t7r/BpO6knkdj9FZxB8uqYzXmEsOEMw8CqY9XjeWxL+UIwPQezHVv6H1C9A1jRN3kXoTT5/VzP6pGxrQXxZCsEb2yfGX8J8ezlSUoN8JlBeTahjrok1rCYci6JpFYakvrEzjkKZaoAaq7J9APpvocxnVEshoOPwutDfDPwScj1wdvo8K/4vlTmx8xE60mpnO2AhY2k/IITmh5hohcXo55OPySCaSJnXgG6l3DK8GaX7DSsgu6/4UI83og8icxB4GsPpTGBXDERjQaYbjIo6yRJtFDsJTob9ufqYLOJYR1CJn8Z7/h0c34d7DOVdDqLefyxl4m8JWta7Ii4k3Kkx19DfxNRT20/uDIzXMKZ/0Odq+vDLYiRDkcJQ0ai+BgrfDe4XqW+ehUOaxpjJ6/kdhz6jkTLT2Fd8k5OqGKcf/Z2CIb0Vd89wkpKB9ElbDXIzQOZ7QuyzjPW/EapbVk9khXAatJMvBSHvytFsPodmYnXM2sQ4STu2SnkNZPCNZRCwzLnJskl9dZo27/Oq+ITWgXbyVN8feWQY0S19DSePeqR0wdGp+qtSfwJhX8r9aTJsvwjKXJZ2KtgmA0QYCsYB1yMEvZKSV5A9FKgSoJxsPLx7OPuSX7P6+WhWCR2eBM651HmjtYDxoduhrJIpAhnbC25llGw8DC1dgkqht7NKjyk4GW5Jfkr9zaw4S1FWHCvLCffpQJqYGXmbsyRN0KUpmBb0yk+yKs2bQNlcDIwyx67CqTSNM5EBijEdobg1OpW0PMFG8OkzpMpEwzqFHcDlxEtHMm6CTydwPpKsmsW/R+h7YrqqwX5AjBFGNBJvv0yi9GpC8IYT7HIgyl3L4cpwDBIeve7ej/rRjNX+HYsf583T+aCiPf+s919svUkqV3HC0aVV8WbCC5Q9DZ7X9n0sJ3ZM/rY6C3DQt13QIx86QLxVQDtv0q8nlNdAwxUTAupHXIcTK+gCE+y7jN9gjMMo6of8dFr5Vp5kyFg5t6t/G75awHi2CTQaJ1UhDSY4Yfm9KOXlTLj1UvnOxgD6oVGjUZBDqKomuK2w3glXYKDHPyc22pPNY5XPFSeEaj/kNUpP+ry7v4R8HQxdQTyLAR3Yc8MLwH4sZQi459CfK7M8k20TrFcZ885dyedyEfMcHlpDWTYkuoxqjUSemjA8H5hezJbvcvg5kX50UHF95OAA6cr3YHj2UOJ5guw3LKAOxxQ/rWYZGvUl4tcSYry0ya/VfB70i8hyMW1upbG7D3/W6xgs4h2U+3mO3gTnpOW8OpxWx0xhGBxxrIIx+Gz8tFlE+V+R62XQe5a+3wNvnsRKS1Tv3/p87ve/z/UYscbg7sP6rQaQY/4vhJfJyG0i23nK/NrBPyCiUfrJkxKcmruFM4iraNCPcCN5dwN26HjlqakmpUdy54CWE7XVHDNwVztP315GUH/BIy1mgnw9RSEoEOO4J0b5kQjrSMvVKNpYpzD5CzrxKJSFtFMh9PpCvk0ytlojNFZzS/GGfnj1buhf3z29DeAJiavEYumA1jQVvN4oO0rqB2TfAJ/F7ZZ0WoEwvqwfeXG7O44l71j6+D3lnevga3i62oYMoOFvZOjB7fNhjMJXoI50BbKAcisgE+lyeBhNfjblJJPmA7LrnqfoC8fhk0Q+IlaGrGKs4BSv2WijjE9gS3ej9DC6Y8n/AnxXJZ1gMGFMyMcacqQogPJcrnneNvyl180wNYuMt0Om9kJu4Hv/tutg5hq5uwpJXnzBLwDcMjnNt9v4JDCdcR+IHkBDuspRh9PA2LxuVcdi3omdj97Mxfvh4Ul2EleB626EbNIMj2VudSmTzk4D22SA2ehKpyEUFMWP8L6EcOLQwDrSoXAkQymYpFwoTr5KrpEpUMuLW0zyQpQR5zh6WpUxVygvKO6E5rcJNQOZACajqm+6FXocGg9KhO1Z2R4SYauHm7wR3FNQ1H0gFCuLOK1A9EEbWbF/++3DYE7FiGeTfro6HZNyc+u9ijgAXK95l7MK3gn+YRSaj/GTjrGS70zDc+B3GmlvyHebidJQ72/1+TkHx1pOnUJwh2Ccr4De1hAOYvncvwvXQIzp6+D2IeavaOTybH9CPgZpCJALyOsjA10/cvQcgrmS9A0s9U/aL6EcxS7A2zrGdQ87gdU0cCeRG3VtP26PgIdtBmxJthnKxtH49BJExmIFZWydMT5Pml9HgW++e40svw7BH3lxoH6zcwXCH/Q0u3hpgMNfIEVsemcA57jdwAgrahDG3QSksoIZX5JJ3lUvF4TKp87kAlEJop4CJ9DyvMyMeY3ROASe4cSDwrYVn7CQ8GmU9U6NTw+K8bHzS9sv4LpoLpOIV60kqIyeg+tF++W8WdYG2Ec0At+2TSiBCrc7A1l7IX1wzeYWUh79sKuK4vG47VwRfc3mcsJ1FCgSxyFurGIwL//+ws/BxELjSP6JA93+xGeS9cAldgjSI3jNmw4uLd8bnn4NztXQYQGNfqXhXBiHsRFH31lMTZAIRs0A4QylDa07YfY0Vvmv5cZHsdvoClcVZD0Ux/FOZOB2MHjK4p7wTZP0mpi43cD4nBvIBT9N/ZEz+WrG5tsfQm58FKcAcrfc+JjjhiWcjOZ1O1scq9HWMK2QMkGJ3jCBCZ9CYOLcBjRoaUBuWPlBSeiO0iVkNpoepogLPfHCm4oD5IYXWerXoRwX0NCtlr8zIB2VoG8fbivgKX+KAvtemI7RV6P87KBfwbYv7C5pmkYZCusK/Sg05Idkm5DzkvMhsiuZn+DrOoX2GLurgBCfUCCGxZBBHJzcze0Itm4fpK8ayuP+nDE0YjUi7oMy47QD3D7Vs1qXs4TsR5tPErxmzWVNMuQVTMs8UM549oGfYVTm8g0lNg992wgkQ7bGAUEsdYo6ixcR0g+pOJq0P1/9EIix2lBWxqFZ6TzoqdzUswGIh8R9rncBeM/QVpkPIORjcXr+FWjUeULbuRtOCKOib+nqzKmKVVm9GYCTO9mVj8Jm5MFf6JM7iEwVKdkJAF0ILx4rO4NxAkpqKJuIIss/eXEcfKEuFYCDblzEisKsvBbpfI68T84rOBHFTf+ZyJLQUQk91bJeRc/xqEqFTJ0P8bon8wa629vfkr3idrZ6b+AAgz4OopHHz7+j3onxWHzNVL52No9t6JB0S+iWKfqg3m/VFLDEPszQHdAeQUU1beQ1+icWIJkqvQkJZ+DvDDRhWdUssYdOY2tGud8XrYReKduvcrZM69DCVTPwyihx2cSUrl78fYQaOhlGG/sLspR5+2Y36ddQNoE2KJKfdFwPfxqsyiQbQsgsTQZhmgaOBuQAHEMYdpqNfFG2WRlRGKW0/CraLGR3B8xcgobD9sYAjgpe/wQ8TCCQ15H5DtVXmZc1ZL7NFkA9uRsaPWHWX0j6C7KHdKoD4m8tyJM7CeasEecpDQWFSNOvklPvy9cavjLJ5eEXy/3twdLpoLMEI76Qr2MPPSS/U4DbFbdvuXKQTconZINggP5cVKdn8UyUNw9HURwgK8zeKOXlCP8P4LzWOuItgoQBo3xLpeIIKp1pT0xr8QR/Jl7HZLu1vZqVbsWktP4ucHyPTBosGBFs91/HYBwogJPgqSD6kSp8DQnK19NmmcYMvUdjoOlkkk1XEGg4scFiMAiP1AmRhScdxxfGpd6+sAq5kC+gwPLo2Cv24pquL3yTbq4h9GSrTN13oeHhhN2G0Zsg+ApRYULav8bkNdhM+nlCIwSlLbB58EQbwShkT2HEDiqDQJE2NEuZK9vWEs7pwedCUNqZOWIrcek+OBnK/ZbNIGT6euj0xsF8gEIfTJhMXRXBB6GHDE2NNIynFVr/tIgdAyzGMuu2V6Cr6N+t+0v0ETsTYofnzkMhDF0Ijqug143kla8ycJw7DbgFdfBe+McpFrEex4vaTnXUzWJge6DElqHhR3A98G2kcCuNTiT2ESEFT3UYEVGbAMlUsTWiaICwiA2eXH6K+AyU8F2krwFnIX30z6g1M0lzKb8KKUvH5v6zf18YPR8lGULczMytJA6eJoAzD+NgonzT21/J9ZOBfuHLMecQ10yMJb+GAi06ietVk+A7uQOJfzCHrdEoDI8dgMpQGMSWlNUBfQ6e/J34VRNTWfpE/y2Uz4NH/sLBqEg6mrjHRZnljTXEMHQQyr0f/Dm+tsA6Q/7uX8gRIjoinYTyiLQ4yNBT3Fl42G8QTgLnI4vTrXi5K0dbnVwHLQS/Dlzf1P8Z4VZ4vpa5v5BGP+Sa4wzGgR0nq6hbCF1puRsJ2bVFt63ygchrHvPk3D1JjNz2hXd8RswJG40Yb5llyMgdzRr4uUHZS5O8TqysBjzGBdrOA8gvlCv22AjB14E0wPiZ4GrSw0kv4RoM4/sAUh7P6HZX8whGQi5083k6Kjb5Z11sITQiMiq8RtWXsI6yWzGyGQoSqV6IwQymr0W18EQ7rw187O0O6h9mZg6kM5oUYQIpr1X0hGswjq5MxHq8Y8M0KphBHY381RL+QjiK4BF3blzypRL9f3tnAqZXUSXsr/dOZ98XEuiQBJQwsoNBlARQVGBcgygqzuiDCiiOMowgDomOOgMuPz+CiCDiCgnwIyKCLN0iiwsRCDuJSZNuErJg9r2Xed9zb33pJumQzg88pNP1dHVtp05VnTpLVd176zPNX3EgoVEpk6ll7kP7cvIIIz5C354l/646+vcsAklb0rBXQ1bfJag/HPoijd5ARz8J7CC8nVAR1EPPvZ6H4aD3i+y3DqPoe8Al603yZV30lXaC6OCM/Tyh44ymEOZ7sBYXMH+zx27ZUlRCGz+k8+a1lTl8h8ZOzmmLUvPF+KPwHjQ5D243fHnCsaq4nqDPt5NvN+KBeAdEO5CgogLTjDl1jr3xwG2Cb0rpnBdfJ5SetxC/EWFrIu3Pn/ligPSCVQob5yK4U+hDXs+6u4STiPZZXw4zsPIrlM3P1vZe7Oq9JDdAoevwatA9GbzaJqxlXo8g29OBi79OnW24pJCZfdfxPIg5ifBcqOjvyJ/Focqhqwrx44xtWJV5aNqlYyE+xPVtlKq11KP+pXh4Ozptex7X+9X92Q1kAdfnCiZybyZ0KZZpMnn7MUn9s2Xfxyg/kDqgCliC6E/giYQZRgJ9FqfOBtOEnsieRvxi6HA9tKk7mKXQIdAEWo0hf0Mt/bFd4oXnoCdK5T+BPQ1h+AVZ6IaChzqV5D8H57TAcP4a0/VowrGMy73Vy7lQYvYFwKQ4nEe9c4N8xM/DXQldPlzD4VVtll9SR9sIVTX9bYUeq4Cnyc4d5XeA7wX6/gKAjwKp0tJZ70nyP8Uau4F4mcvAafSB+M44l+yraEv0o/hXC81sQ6tu5lwm4Ev9sMLQ7Q9D4/Fh8AVdCDq411f4IOVO94Gqr71TGJj77DfgJjIIhAAFGK88SYxD+TcqHxhBCFoisgQSRq9GlBLxUJ1oZy6WTnDI2YN5ux8gP+hV+98EMgmPTAYTiu9p4jcyEX9B2/2ReHkTBIbL18P49XT6aOo4AY5Bv45JOmZEPAIslDTA5NcynhOpA8PJnP5oyVTgTsM/yEx9nboKleNwEPxls2mIoyg2/9778jMKx5FxnEBU0Bq6D9QMn06nb8Y8vJPw3oUsd2lPRvBRgkt56aug+PNe0vYa8h5A+KZT5wRwnUEZvLVlKU/ctjtzsTS2H3j3dnFoRdBApb/h68D9KP165kpWDRcC9iRwKKHmeugwOcNt112BALa1A4flFQjrR+j8RcSRj3DmU1zwVbGHEYbDUDb+0q1Cn5RyAHblX96ec9RMm/6gy++oj1GMF8xLGM8sJvprEPWN9hml9f3xlBFX6KSvc9jWhHIZTV/Ijzkl73Xv6GtQ1MG71JxIeAXLlqPkbIsCgDKpTtykUV0UkYi9E+mY1Hb5AghbzCdRhn8C/xnW+/fBrDUw4UHAeCqXNtKh4ckrk4sxyfOBv5joY/RrFbPi1YfuE/0FWNBkVjVvq470Z9DuTdNh+Atpm0mpYFI2sQ9k+1Co4d+thF+Fcb/NLL3J+njQdVAuZoT2ZfJ96P9Z+nkEeZfRjswYE0zcsf0VmB8R8afS3HNdA2OueAGLizJYC00nkTeUvFuIa6nvo94/OTYbpY649CqRjKiExkUOo9sPqmR5ROLQinH4zJGmC/cC8zh9vGY4gkc9H9PImKVzGTuMqmBoGd0Ll49CURGXFtbNmyhaT60zqOJAZH8Kvda+1j5Sx3nmL+saefNQOJPA59f65isIOyWE1JccnkP4CMlT7tuZ60MYn32RLsELdpaM1QQHMgnz2rdLnr9cU6ERMb6rOJ+bwFvxpsMBEMCfCT6CQUrIxCMxFtL8ZdQ3JEHVCCOfqGnjeidLehk334OHJiK/xFdCoWcIFbR1WLMhcEP69i3gLaNOM/3wzpWxpC8jvQhGm0H4bwjYz6l3EEBfJC2D2pYNTSJ4P1r5B5+DyRC+zWOwmOSXjcwmcyPa87dYIn/L4JPAXgUOb/+K91IzFFmfyfcxyAa46tdY63nUm0rcfYrMSJAxJQ37exO+AvcY8afXoMAo8xx9rSFlvk96HP1V2A/F701lrzAUR3IR9x+4wxF6UZIrhkw6syKZUWW5YgNLWsK7YMR603k1kQYDEtJsWKYoyvNTmRbLtkRPFwttjcwBoWNrmUk+4UQKFT6i0VlhjYMq6v0ZJL7NI/+IQ2HRd9k1wROj6fcc5owxn0RDQwlV/iob6WibNSB37L78cCzpecTtj4okuV1K+Oy0hPN+lz2h/mUw+xEMODQdg2sm7sAVFB3JiJt04J05l3VKr7/DELDGifwGov6Eon7E1xGmMp9NqbHVgFGXsugDeaKSgQtoxFFQdypWxH3JcvIOZKIslzFAHaGafQrt/RHTOAsfD5LRjK0swTawT3EP+oPxCCVtPcRy54eM+/J81mJ/mnNZTL79AbHNi9xnT/fjjyDfl8kJotClpuU+SB/eF4uJFveUbhn5fgbkC9L7UOZlsjUyFvHAaf2XOuqILlYewGm5nCPh44pEaPU4OD5NupHxeYilUIbVI6+rrmUudScgqNC1hspeQOWV9INo9KR8eWt/aTb6Yee08o2El9XSr1nAs+S2fztr/Wrog7cF9EbRnQje79Je/5zuMTbm0zBWRoQkC/viu4VzX9WfZd37IfZbZA4I4KRrsdQ2DlxHNLRjLMvyvCjY1j/rOSNWwvkQlRVZ4Wp8f4j8F/Kd1Eom/RDaXEtaBtI5icxv+ITCryx840ZG940Uraj9hNejT8EUxL3avRxhCoYH98oh2UnZehg1uswSbTOTLTM5Rm+2fgT16XXrg/OxC5cmW7z+0EijmpklpFb4Iuo8BPw4yop7MToKWNBrGOGwvH9JOwAaHfC3DxyTfY92zO/ECVeh5NlZ4anwIvGr6O+lWO+FpCtYDpRRZts7q/krfgFNGlEo0GgdgoeeKzT3hh7g3xMLJ81lfIYczi3EX2nw/N8zj1OzfOV0fQM0osz9F0GXXPn4rP+trBL8heUBjDPaBYsrk0qIIU733VpmH4lc0qUWXsfA5WNY7q9m78CgnHRHqgAoCDqXqOY7CWEJpcTLOWBkMlGkOn9g2TibtDzlQ2iXFS43RtPYCcRdymgBLbeezUR9QpdF4ciwL/0TAPHEzAEDPtNjAXordarns2wVhon1arB1c8ljsm3bU7tKuAo5LdQB/15CG3Ts8dIzuEysqeKxxYRsKbcoF8RrwP01YFUMOscYb2rYT/P1RItWnETs4wjtp0wUAATbcjbt1RQr8XNoYykZ84nffQ/LYZheRvcFicHHZysHl5/u3XZGCOOtodHM9yy6hCVzJTJgZaHwTSbirbRrfx2HQ/VlgWeInD4AKzwls5jrUWrV1PWiLXTDTjnYL9vugDsuNpZO5BFkNKYPxoMXiV+EsnBeu4WTsDLMUoiLIsyYg9G77GObEXsQLSHjDpcsYp7cOgiqZRNmHQkp9f5E0LaKSWsCt5tl0l5mtA/hR/G/ws8DTk1rPTW+/TJunkxrPDnjMqouWVu1sz8g4rdi76KBcxC6Dz1DHwhpulCBZmeYhVYkcJB7NPZrC+Ca74LIazXozpa3dBwHnW8Axj1kWEXMg3vAn1G0lDzb00UQhMz67Jhj3Bbg2yKRw5mXj4vYNp3C7BL2Uto+HsX1bhjuzAaW8EchfIStcOwAxrCiX7aUr0bBsJLuupsJPY5FIdIfBahFIUT4TgbTiUiz9I15MHR8+N9By0dYBdSwuvBUuc8YFPhe2T5Qa+zwuuSsw/7ae3MYTqGGNsQRc05o+6YT3s0MFBYKS02w6zsZ2+P05fiH5UAIkvxlMMF/knRvRPEWSgjTmQM2ubSE9SHqbDLbmNTNo/HsxxQENbbG6WaQnwfMU+3wJuGjOBghrIkJXGqiaEmsR2ZYXfD4BcMAQvecDawJ7XtNA/ksqWTeQQiiVsNz7v0JvHbQ54s629WF1ocLbhyZCR3okDosIrgdj8qpmXpJIckoOoPoV542T0spXFIqkee/bThppvL7Kn3/H6zvpodQHC41D4VuEKu5Fo/1Xk1YmE45wlCeK5htoNt+FtbUe0rjUQm4fJC5J+1/mWiFEuAYSOs8kLL/kBACIvhIQRnKyw+KfR3NX2DyHdccXKgdc9TxoGkRuM4kvi9jp6lM6IgHPvGLjX+boIFKEHntHs6jat/YCMJKdJyabD3COAMpuYpBP4Hmh0bBlFIkMV0Av/SfFJNaeJcvwrrfOqEBrQWRW5ioKjRtNAXs/bRxLfnDgfFtGAlrWwpCLCuN554gnOmic7bIMC8eYudtqtX9SuIpmIXhFTbW0ia+WYtGOpZrTHAjHZlLwoflj5Fvu+Ky3z58/kMDQncbfSavD9q3pC/4AHAJ2uEkM9UDjqoZkjxuF0WaBDNZRC13eIq1ps7BEujxBbTDVUOx0PiNWCZ/1ozqHEtm/W6dQD4ZG6ZRxvgY0vYduD05RHdE5+yHNNaZpzJpQSmWML5zie8FbrqRzXM0TDX4QdrUkK9rGU0fhtE2Cmqjfb0CulIO6s5d3o+czbK+MOEjyD8UWnyc+jH3YIhm2+HzMYTtX7GWw63OW9j1StwAtzDh3u3i3Y5HMQR4rFAGlzrogyDOPs4GTuKEZYhUJ/+AD0tEsUSUkUFbWFPLP5yTrfQpIKtfpF044A1kPU96MD59Ce7m231NYl6iHV2e4aQ467YpvE4L4nuQk/GT5sPUWK5ymMR+tMIw4ixDk/YZQBnxf6DNF8JZlxD/IV4GrcOfA12eroXB8P6oySgsjUu0zwNzHG0plOIsOvKolg08D03Tjcg3HquC1E/CjeR5BcNzxP31pd+hDJr3RtByXFonwfXhiLy0zQ7pBJdC6qM/Y08vnHj8erxifjaf8QoX9Ck9lrdmAPgQAMI417FvJc/Q18GWwQdPiAMA0BbHGnOU0oRbuTSGudRFkBX6OAQTkPGuI+N0cPq1Szp7sH15zb5IQBXVLSjqH8Ovq8zrLs6XrWtgqrVM+hUQ6i78DxjcYQz6Z1DBT49QjGFdJIoEkdlf1gnj5BFsZoa84mADs1cJN1gUSysm9EUA/k7eOmCWUOByLe2tYpIE3pYzP8fvRMX+j3RicK2nJ7mfRWh+z1jKYbqVaGx/GahUq0j5qpnEpzI29jOjkIQjQDmb9n0A7Sc3jzDTgxvIBP5LMMnXYB6tbLzNQ5hOCDvrogNVAbl0K1NaQ/vQLvm/Jm8R7fhTXb8eDg1Ii4fuBX2DqdvlSUjpTtBl5xzah/KGTEnZjVgBkOc1GIOgkffJfIXG+0MzxxiKDTgVodLo45g/QqcrZ5A8OVNilOyYy9svQXhs18OUElZBvvpYOYLlP4puILRV8cXqgPboRtDDBsyXHq4KfH5sl7qN0wKiWGJdLVP6236bc6L72o/OwZtFccZQKUG6UxfAlBJ62riY0GrNCEL5frTBXmwwptZnZTcxAVMJfwrV9wb2SADjYCWrXpyIDm05QziZkiqZZjUPH0V0WsF8K0L+z2TMQ8h9pU0Nu74RpTId63MhVuwZ6u+LBV7HCgBEDwH3Qzh0vHsv6o2FOeG3+Ck0xxLMkI8FVDlRssBkexfC56Dpy2IE3K8ivBbxlv6Zxg95hKFVHqHkAKW7QWOr6UoboBeZXtrkuADtmqOTCl/pIuahIacN6ap6ujWXeR/MkpdxnwFW7xZNc21bjk/6Sgf7dSc+3lE03AlXjtD5zDDGQChO35sdScZAG9SlgZOnEkjfhS4g7aOX6NN8Dn3GFvVZVm9X/Y/iKbTUYwEXZut7v2xfHaPMCOS4QjORF8tJCGTYqcvhgsrgkmK9oDT8ExfSevmsbfoR5aoGJhdcNWvYa2EJRmFlziB9PJPxP9STGaIt4lu5PC+YnMINtFFP2Ad/ODj8WS9BXD5No8E7R3KkT4Y3fSFjhZILM8HdgGWK0zcY8ccon73WYpHoxwV0+J3AH8gYRtoR6sUekzz7r6aOk8GsiP9bnO3qqBIM5fLtWyT8FaVx4F5AtkKn8PvpzcYGYJFAn4v6WECFSLej/yW1WTrxJ9ldc7TjNFQw/vV4O1XVAO7JMDdjrcEkvY0B9aFv/EWb/os5Jx2PQ+jMd6DTb0kT7PTjBqrG62Jeye94pFNfJuM7RNpfmOwqRqXhKibeBKJjiyEKOiQqeSetU9ItnL9v4L36A5gcN/abGOwMqOM6WyaREaRUYoDEGNsbfCxfBKSS9fzW7fAMTeDb1JAlbMvTM384ZMlQ9ltYm0exDv+Xdi+HUUN+aXt7zh8wsakHYaSzwfcR6v4yzyMa5+O1dGIy5cNyRFpBT/H8EZi+Y7MT0fV1pMnXu0wdCtJ3EVf4nOwYh/XzMbk0k0kzKbMgd9SRecLqgKd0I1+TsMy7Er8IC3w30j4fUC3epn1hepixdCwK5CnoT9y+aXVsU2/ape7OLj+duxY4N5gafL5gXTWW9rD4I2igL+PwZ8RVKIDGcOy7bTr/XuXx6SU8e+N9s4Vzt/QN0K458GzS6oFLWoaDyfaljXeTiLtLietCuREmQbSCP63m0t2+xYlwQHaDf+6BKhCAlQ64EcZgv/NTJmU6A13KgHXSxckIwhmakecBmgEY6nK4AKG+wuhy8kgOMdhWBY6WWgiqxR0PXvY/y5/IrFJbXSYAvn95HRrPyc+7IOZtOvcmtjkMBvEnqp4neR+dsp598EoJG0UOIk0Qn0tUjMEiYHG8DGkU4dtgjouocBcVFVb3nz5+if0Q8WiGsrT/S+MsdtDGcLGHBc7l73zqX8VYzie+HiEIhTYr6xuyH7TB8GcWZXLWBsnMUacodODW0quQXtblsFTPHGlvlLbtknr6NZ++4KshyBfIexBAVwCx1yN0Pu17ucsE+v8NFOKNjMEvKnyJIR0KUdo1N4M+IMCVkxkXSl++aGGO+xK69A2FBsbYejh/5CtsQV/i8/C+rGG3go6E3cJB22BOfzTSB86F6RDjzOyS26oa3oiASNAnY2ZgdDKCoAtJs3KLZaTMojOUWSSg9WJCCY+CCbRAMpyWpwqLa9w7PfyFEau3TM5wN/fltA2N8CPqnEv7lonLMMWLl97Sltp6IBsqhUYN79v+OmFjEunE+tWcdiroTH5hCu2z75xM9FBgJxEeg+8NXBoryWwshniKMnzt8omGi3Ygiu9IKk2+VfR7FMKl0A+dlrlR9CuP2kbRUVnhFrnt5F0vFkckLwu4jiVbUlS0H8mLKHA1MVaUjSetvSdntNrA2Pekr1oeXzmTbuGsjIt3TqHnQhpE7goqTIui39OzeKpC9ss7gK0f5LE/R5Emz5u896+iPRhBSy9MCF6O0R+Z8UJeldU95qHEq+qgI0ZDXB3oaPmu6BRAXRCU5WDl0QjkUCYMAXiYCdhIga+ISR0fS8gt3kJVCtHugEBNWJizyPf0rL0QilMXeKGWe7P/AMfpWJua2ew5R2cT7aZc9J7USVAtjs8h/wHVb4f4/0reENLCWK4P4psncgbgmyPfow+LEa7Wd7CPE4gyJ9P++jHvb5jspRNZBtG+r795/cVHKe5PPL1GxlCiHYKOLuFqn2vbOvD4yMN2FoDg/4Hv+6wi5lJu9mvpHK73eMYHyMSd2xLovAmmV1FK47XQdS/Cy+nffuRJqnBWxsVLA4zjaSpfipW8l3xXCdXyhGM6OqNRsV5WbYf+2x8brB6DUC3hgAzhO5s2/MWnOGiyLWH451cw/ry1n65dD01vJYTtCmsRvrYHwUHc+dolneMrdtwE3j2JA/eBLfSIjdFArMkvOCBpQxibmbgmwjOxJFPRoMcwqfu8gFYF5jbyvVS2RTg95cY352k/uvVF278Du7/tUT6wDqIbx1ewqfYwxrZdXhh3f1UG/EXg2IhvpU4RX8ItXvJXkD7MPtNGLXVmk2d/9G3EvY7Q33Yfh/8RMA9TzzHpAyd59ln46P92QsdV9OKhXgPhF6HLfgtRLrehpWEuFc4WItu5V9kxRmlZJS1TU47bfPtVB72X8aiF/j62Nht7mi8/FPZ9WWkhTW4nfbA4qAvvRxjzRDqEyLyuOPuAd15LoNUQlOA7aWOOvMWywDn0AOwxQvvgc2n7cw3hWOqUPgB/6q1P/oCutP16g3UMjin5RNDY68Ax8VnQEwgDo1zOYGeS58GG2vzKfjwrnMskjIeYVPTkrJL85yh0r/UssGzDCgMJqRJ11FJxogZX1KLtPkfaD2Y31oKnAU/o3S2xuabMetaxvVasrMu4d8AFfqtomUvP4AoTqEHvJ7kTyz17BnXQmKNpZyKwgjlQ96u1TOod5LEKjPs0LbRcR3a2XBafiU7cS4toqrAEJN+n/RtgqmUTOCQAR8V94B7KGIjHsr4TfK9GtsLSAi2jbWin1VuKYhjHJE08KHvp3R9Q3YPOY7CzFQL9dOitzGcZhP85Yzqfio1kOrfu+VQm0ms91lUh8qDKOdphB3zbfPAjbJ7yrkHwjqTyeJD6tYnz9HZgfMk+LDArHw8CzyfflzVGwi8rqRdv2pCH/uhmjkFJBC0QNMiccSbvjWihOYRq+qkI5NcJ1+D9wcclWJs/kTeLuNrUG5TVYFq7xeQly6VlCe0KoyrUb7Mt6n4UwTlgPoxKOrSjLWtVbRsfFnIJz+Go7zUURY0tvrxPT4PnXdOQyUY0JG3OoCxpc62ZddSw9t9+mE54krX2peIW8Nv/7VnAsJrAzcaSvI9+ealR0ULAKP0WZgoojQGefm2c/cCje9i4Ye1Sq+T1YWwX4VfTb8eWaJDmKs2LZdcyH8OtSz1pH/TP07EiIa/IH6mNroTUr6Qvn6KtpXgtXXjj9o+0/VvGnJ5gW/iyRZxIE/rjpbE6m0GeZV1p9/UEm4+raAGN+BsHbfUMno4mhjKsQPXNofzPrNWdlGvIvABYl6vWGUL6MPxBAuuAMXo/Icv0IJI49Tp/29uXpE+ByKNh2JnD+KwF3KUQv2Z6RnD3U+IomQuhCSvqOYVDS9Zh7exrakNL6A/CfGUQr2+dCqP1zibq7YCkEz0nUMvqIY1eJwqdoeV2Uo0er1tZsA1nVV/Ve5jwJyS+T/xOrBwKPVMc1p9NGhMb1zPAxRjGjJaEr7qbBa0QHi2WZn6dygD/dhTeN8k6DYKGUDJeLZc0kcYe80tj3W/J+wLwK+qgx/Qtc1ZiWvzAyAZFRWmlHXXgltbeMeq3fR+k3hDSOoOISkO8+/YBNGa8jXZ9XlzaRNv70XfyNk1hLOS/ZsotevkK/nNceE+awxcHwsDaLysoj+PxXkTKfWWDGYPHAyYEgXgQidBlnvAenigYf4WAEjLeoCCMMmBawGOdz+KnsMy5azqb8GkZI4fVqIdxp+Sb/fFZvU1TQYzF+QIcXY0QTmF5YidYYRU+1T/bnNc00C5tfhG8LpeKz7RI8xc+TbRpnWOIo2/COCmlIMHInDqGkgmRsMTFq2a59o3gRHk47gTTMpnxiwDnlY7eQyPZOjjKK+dSl7E5DAXAE0iZ27jLPdsOuhLXojlJLhfDkVdc/uWwFIeT3q5KBgL8CSqeCI2OZHKrCe2klxPbP7ICp5Uct0u+X+D//VlWNodQNiHrE6TODsZsB4aXiJ4up/asv5UTlszEFxLUtOMEhIBbAAAjDklEQVRwvH5h8TUSbyVfuumIFusopI7XuVHJezpuOr1FFW0PYZwov1RfsF3aOfFq6w6OvDTprtF9MH4FAGkCZNwgck4FCaNX+P5G5HbiA6U8cYnJX3zN7GT7VbOW8A2EZ52NBWPJeQQCtgdMuwlt5yT0XZRp241quunkQfRG9i3nYHKupD8LmEi1PeDxLVkZwvzvtHU6deNEM2s6+mTbel2KRxr4sIB5ocxo1G4bd2iARNyx+g7rwZiAT0xin4SUT1zCMX5DpnQSrVJ9b+OW730G0auOPlHgslrczXBiG/X8IqTIaOQriLanlRGuMDPrg0IunMLDsDNl53JMGFwIKBW9ze4YwtsA+i+k7BjKqvK9njDWd7zxyIHB2Zb47qc/l6NZF8HssVIARoHxxDTGRdh8aKaMrVQULmC2csxh9B8cVXWZsBONj30VfOd/HyQ7Pl8ibZn9CMWWE1weUlg9cbXMbCMxzhQ37C7OQXbqIIKT5F2Sw4g8APAepHNaBQUlopMinBM8DyL7I47fJzyOMGApdzKdBNM6ioOZXLJ5q9cvEbKbG2FYmNc7aiqZyXKWlxuIl4/MlIR7sP5wtq9OuQxcQ3gNdb089kjwid92xB34Cdu7NFb7HI6IfZcTtN7FcvstgrzcfL19V2EpEAuJey/Mo6S/hSW+k7QutVvSBBPOZSyTs3rW922HfsCuIL9yfJYfdFlKGq0u4zU3EK8lpAJDjT6IU/ra12Ie8VbmpT8Fvk0ymfJzoNlQkNjP6LuBDeND2WTR6L/v/f6E9DWPcNQ/OatDVoyzEvitrDdlL+umMf4Lo+mgQ8JnF3yU0QutcSt9PBrkvm/smFQw0T8D4grajSyFzwBmLTRKj6co7p5Opt2mgxhOvNqwFQvTuDz7YHYP8mNSyXfZE8s3Zlzt6TJwLFr3HSD9PJx1Mel3S2CcGjiEw/ogltA6+K7wPhqZCP7lf0fIx8BkcLfXlK9uYgl1NcwwFeEaCPOOYjXKRP6V+ivw+4PwG+BxInUko8+i9iRQYQlHPKL8i2WXIQX239fkPMF1j7eQzCbyxtCfPclzTPwFLukU46URmWcUsP4y7mTWwr+aizXGijRTz8uEVrkHHA083q/M/ak0VxnlCB8yGL/+ap8hVfS9nOWsb3lI65JaYGnUftJMdECBSkIF/xb6Q6thL0IzAN5PWssHeeJEONE1tgWUi0Nc/IWCckBPE/8CfbmbuHTy06y+IPBK/7CQ5MVYgeuSY59WMg9c4Fun1QSPcyDOPgz4LSAb5ECIu5dMcxQ8JCx+Jv04lb5ZRxoRdG8HfbbtGLlMV/UkhERKKtnFnwRzuV8IbQohZfjgUGAVyjjMgKv+i6Xit1jEe7PWDfg3w8z8ZczQrkHxWM9lh9n3gvse4H8LUy9C6BeTaT1vmB5I+8Pg0H8i+S/Um0JZHB5ZTlqG0dkn8TrxzrWTGgwYnJiVkVXw561D8Ej8jfQ9AN+G0N8H83ycdn5IP6o1SY4TXDn6wBlxkQPji9a3E/8NmVQvHEx4J209Cl6vVXyasC9jWbSGxwIsXZc3wXxj2MM2Ipho+dbxWD5oXIEQVrEK8FayoCu4/JHRinEIO+3UkjmCdsaD+2iKjoQ7kfnon11xjI5by23/9MHYpONTKKWd/vwOXF+mH7OBhaSxlyaIFUDQmnxAYl6K/RBgRxx1FRoVSdwxoxAyzkH9skdZ3wBxP5D6qKsBP5ZOOk/Okd1bsIqxjWHecxx+E+nYurVzorbrII5at2wZp1NQ9kkq+PzL15X2JqQ4iKSFa4WpfXvB6xQuhRtXsi/5IAzwfyjTcsbe0QrJEU+a1n74zqIzsYj8x/GrmM2wDHCGS+BxFO9Fu6Xgd9+QbskSZRJk43JfEshgwpQnHPV93rWW+neB50YGd3tfjsWFYT8aTM043gaOY/HvwY8HqUKY1HFE+RdtkR3vTZoAZ2gZzN1aog8wdpqIZfONlP0KRYZx36YLnAi/d50ewlhH0ccxQI4Ax96EY8jv60TIqSC1KZtU6Iw4fvOoGvleLOV+2DeZ3Jf/ifB+LMu9xJfC6EOeZX6wzNaRXmFxDE3vrJsDr4zPlKYPzHtjzXyjyud7F9D+O2jM6z9Gkab5wj6EoTQItYinIqjXIX29mhBS+uYynOzu7Rhj547RO6E6CeE7nIcjJF6P50PmM8l7D17NvxDi/jeh3901wcUo9UIpXD0J+B/D9OOY2ZhcJkCh0Oq5FLV90+JXG8bjAAXRAr1OppNT8AbChsvrCQaqbFlLnmkZMqrzL9ozjzLrz6Qjv4aZ67EEnrDZXI6eWNYPGbJ6CfsrCs8F+UfAk/Yrwhh33xhLPeL8xSGDEdvWwoYTMfRaTTiH/Hkk/0roS8h+cTGX1cJfoM9J1DsIYo8mnxVsoQ9l/hpQEE0cuOg/5fxl7UXulnwLvFFAYqiAbqJd79hcjsDNG8tSnrjdaoHBK0dnOCQtzQeJqRZ4bc6xUb1rTou9J0KIolmLINUwEPdwm+CDkYQD8MfT2AXQdDANi5+ssIDLyDsArbWauptYtrdpPbvW+q4JLQE6dUxYsZwIycwRqZI5YeK1LNP6MmMrYZZlEG91OxhfO/owsN8jbxj1QwBT+U6G9if6wT8Z3bh8GnmExfI8LlP5M2GcVxRmMfn+BPPDADHXmaNiMJ648njFXIomZPs210PD4doP04iWXSPUCx9LXOq45worlGGL9kMozScv+gV94isKGyK+IU+7anC/hwzGsy9PS6NCTigDl6S98YEnL1e4rCNzK8hauj8A/A36+BnS3lXzMzo6C0W4CLhw1KUoBBPwQBirD/MpSPi3yovK7f4BKL2Fi/0jDVRxSMZqOk6k+7CM9p5XX8YYQN4B0G4Oa90ldHgY/XoH9c8jfx86oQC6B3fV9Dsm5KNXs7e/kDT5rjiiT8S7tVP7depeSgQoEowHsT4AYccTXjMqOwmUeT1q9xdjB6L5llqXSRgMNXtD7GSRdpaooAtnOxknZRMVSxiyIjvvr8whwyvxK5jlq2n/OzCGX99vpVXJC4YUu3EQNY/PmjDLZ5DL2ZNeuRcvBaOlXZq+B5ha8E8idP9CM9El+xbjBEakJDMmytvwUxvzKu2bjoQC7S1fPu7xYmGv0RDZiwS+tOBz2PZO/K4enkcK7qT8IxwEDYL7kYGC10t8gDzRlzxJui6zboUpjIl6FGX9EWGeLoad5Znf3lHP1UElnmihjYbjpJJ0RR2K4Xj6zdbjQNIXUX4QCnrRxuzswCX9geR5OONzSa21P0F+I/lXo/RWnUifiW9+Ygt+94GAd1+3XQHc1rChhhcTPQ1hXDYsl0DmNcFzELFtCtquEcYhbyMCyCovvk6XijKPzoDkjjkB8zaiIvFgZLhMXg2GNI/JTHEvZVrIxHrNoj8ucgOHICuw2L0X0hcYZgPw1u3MWaZghzWYCLPMgOnYkzDswg3UvX4NVhGgH+DfhyJyT+mAFN6wiNa3f6RDYZnGx6CJJCFqJC59PKwShzdnP0gdf9loAHkngyP21lYmXyRlmN8SrMnDMPa10P9NKME1jPHnMP2D+R6WqoVV+/EPV8a4tzfWDKrr/xVyx6Yw2jX3HCUnk2YJNIL83zIGdEO0PYr+nUtaQBWg86ey8SPch1AavpjhXr8KawpoXFvpGJLSINp9nQPtqiuBaJ4cSlAZFT4oNI/Go4nLpkF0qAhNQ8N5yZEva08gLZN5cW5YMdI75EAkLv/5gWyWyGvStkwQ/cCUxE91wZB3A3cOJuQZGvUQogSN2nsogjc3m+C8dsdAOHJkgJxXgvnNcznUMh+GqSVsJGRpvRpB/hiCeDxj+hB1p0AEmih4e3Q48pLwyawki32XZjamkkJ+C89iIepIn0T8TA4uHkZoptKRA0i7LPUQKH6WGdybEbxH8JcPg3nRCOcC78GYD657sfqwLQx2oYR4NUK4DgVC0Svu4oQSulbOzuZ/LW01r2UM9O00aOH7vdJN14rU+XmRzhWDjzs2MxavP7m5nnl5CByzgEPJxQqFvpc8RTpqdPN/OY12bJQQLzGTjGM88ZthwiXjxf6APG9tPph63nh8OASfrvYTNgETfzlH9WxZRcS4PuoTsS0F+jGQ/gz8CwgXYPH+VAfzwnx+x7Yur6/lse1OHXhcAgUMcbvoGA1bGzjBQ1ttHAODU+Y3kptnAP8uLBjpCRBgb/L2ocKhpEdTRyZ0eYmMZO9iEleJ2B/3foMIHYcvI3jyO4b4bYQ/WoY1Q8D2JO1y8yT8B8B7K/B/Im8BArp4fIbHvpY+j8XcI7PQpTBvBVzcSgeQhei/ggCKV8bRP2WJIHzFTOh1Mn4Bp5uY87sRvjfQKa2XcPKFVlzapzoqoEvo470w0SkAfANCPGUfydcgFOGIu6Iw3W0d49txByVkyJc6CQTdg3GdaOP+ao5fU7RNyYUUi3QO1L0Y4KQZAdtxRz0nUWeggPuh7QvM8n2E3+YI++9NLGNI+/2bV5iXoJVlUPtU1cC/2iy93aUNuKIZ/hGNf445jVt8VQ38w1XXIgTAyejhGrFCMJO/rbAZZmzDFPmI4g3AHI73l5Keh/E8BPKRy9ephK6IxwTW92Twfhq4HuGLPTR5Kpc+q7A0jM/DDYwl637aAPcmrHAFwtqb/S0yGQ6QopKJPpOhELxi7iHaTPNK38LKM97D6PfnafCDNOSbNDrpp1Cp1JwD+2ORK6RDe/MGDnjG0blFaCiGGGWxbAVog8pNwSa/xyUKSPDcq9XiEyAJ7KQQqtmq8H5yMjBPIwdB3SEwzS3k+zpZ+iSo+FkQ+X6KEp5yP42JsvZ5xsn3dM3PoWZyrPkW2rBdBd7ZTkJiPPXJ/lRPy8uI57xhja2d5XiXfeIt4kuQeVmM3TyXYOS5lNwKti7DE+0JZ7odnkoZzPQcaGYIjmI5imQwaftiWwEnjHVI1xiHBtLYvhbnwTi+ZD5jFialje+Ms36qZ7wuHxPxoJNlKLsa5uRa5sZPnPzsq8OnXUhW+sjWJaoHc/UI3tCcZxxPorc49cFLiadS+9013C5DdjZoiCRTODlqKD8z6YXWZksUk47SD0Y39CDmCBr5FMAotNjzUL2jyzMM0tI0DjDaQ4HDcifM51k3Er9gYLwkE69ODKd95je898yUHILAWgfYXqSbSeu0oq+oRcjQds//0E7lQBDOd09rsPDuMZsXQVcI3oL1PR3NcTEmzvn2UEbeiFUOccidHTox/+6PqVY4dQCPTQiDb4Zmlj21Qfbu5STQTrmc0C5BPFb2m76+j0JMBMF7H0vRhN68NZAZvI7lxuHMmsuQbRI6z1T4Iso/1/6xdKGS+dZ1Yg1DKyP5T5DxBBl3sHS7gcn12vm07xGPllGm8J7Tljo07ZQe4YMcO+YgIGQL+rmd8NOgloYsrXXdMBaPdbsI+n8eQK2WFZwf6+mNx4SS0Lo/xzz922AOXog7h5abv90tAeXd2kmoLrtGtN9omJ3KajyJGD85xoa8+cIMW0wASxIvP/oq3q/HY0K21RhlWiaXT06G+IzH+p90ONI64cyKl6jNQPBopvBnyi/uXyjcQzwmlbD0STzKgHmPSlrDvkS92oBqPa4zCkCnRHa3ENJP8qf9mMJTuZofLCXjQyRcgitR8kI8InKO9NazjIn3/Vjn5z6S0l687mfF5ZzGHBHf7ZxE6rKT0FTSJ6vk18tlaElP4coxPV4v/x8UvgXia4UEVgA7a8985yrKAVxDfeExnpHnROmc0GQRTZsfkw3wYsp+jgm8GCu8hPzyeuAnZ4xTTr+8eTp+Gi3vCyA9blsUyOfXFUgoqudYybDc9+v0lax03kj+d6l3bD4paV6dP537OIVK572t9zAnHxse29ZCyzIOXtirLKilzHLgXLXsti4xdpcJAEOrxYKILj/RiMY3I23nIYC3gXgKGW6wJbLaU2HtzGnRvPbQY/cHATob/2H8POpq0axvA85r2h9GPE9bPow2v8SS9xr2pOMp3DyBF4HJt34bDBDX9ZEvw/S47VPAuXLuylheDNqLg1dOYV9kW3EqBfdAw+OgNcWZgBIJ62eGjnKjzqk4rkfxUTUUsXv4VhhH/B7WaD2N77ZO5twZ54NVhVfGL0E7rtqfNyDYpPvO5FkQ1bIQFCIuJ52UbTI+8D6gX49/CjB/xPMMhMXPZSqY/EsIv0m+b9SrjX3DJN6kAJn7z7TUcc8oE/hNor+Ou8cLMMuI7KsK+0JxaFqKM6Yh7HGdU8AtRSkriRpo6WOVfSC+n0B5v8wQJt15dzWilZS40pVktuQEJviKubyXAh8HOU9r5oMLBbkQX14P8BuZi7kZrPh63I5SAEq76VabxTG/9WD4sWi02flRs3dNxuMEhHIhcW9IM+2RdOQLx1LEn61+nPIPER+xLvsEx5kUt4LjM6/3A7NRC5vXLeLJ2wh87fC3iJt2n8LvD54S+uZ7qi5n061hLpNsIzQwYRzfW94dXT6+zh6tBJ3nZHMZceETHeYjNMzBJdIUGqfHSInmceNdmgfmKWCA/wu0P+U5lq6slNCnW5yPF0zRhvTfWQOwBeEuHlOBddlBuOp6BGlyZtXEoYbz8t4LmbnziCuYakjf8XM/NwiYRHiZvQl/HWrPNz+ahvJZDnWIbu3ILGXyjwX2X4m/Fwg/xNXiaVIVpHj1jdAsgiyPNv1KwA98T0Xy3BP6ZUMvuEEloEC2Dci+3vCiYH+s0x//oEr3cgy8D/T1awXHJn0ULr30MjQvLN5EVgnCAzwcerwX6+WlTidQvi/eVYjArkSsp3M14tw7p241/sw8/Yj476H7uv4ZfX0kYRs97pWiANR0bZ8ESiGIgxYmbwTMfA8M/hx+EQK5jtALg5L1WkDeTWzEj3sir6NGFF9nfVvIMsgycE+grrclr0G7+tDXh/XxkJf89BBfjawiMB0P/IE9H+sJDwYXyCgV82EswjJDrWNdNh5WRt3PMc5k5Uu0co45jZIyv9n0CokB5hEfBf3ezJ7tu9BMGntnarJ80tbflbgM+MeJJ2sYVpC8hdQbI05xGTrHCnT7Ni3bnR10UV7kw/A7TQsQKDixlBMJ8bIZ2bLCk9DhTMh7WZKsRyP69sMq/KUKkROS17O+b5K4FFG5duooD+EQHjw3o6Fj+QoTxOQTuiQKoYRBQviA25Dnv4AQ/jfM8eb2DeTtVirgetJJq7cH6xZxxzY/UzquTJx4Fah018e4Ebhh0Osy6UcYCtM4dDQdS07mcyH+EfLXEaY3mbzB3DdgfgWuvo0IfE7P4A3b6BZEfIUGAT2kS9F3anm2154IKI+lBRbMCS1pYFLDzFDA5v0fTJq/EnQtezuvKPzDHdyzMhlLBIzPf2y3tQEhZj1TMp4JJo2sbu3AXcHkqsV97uiPq9zKXnEl8f2Z2Te5rrIzeL+lAyySfllfSVy8w3rzSIQ6RyKIXkDrM6fbgX+SePVaYCbQJ+LdllGehN77MW7G3KKSPArajGTc9czBFPIQpjdAO1+U96Vv6eDzWJmEv5hfQ0ntp0ZUjZfKSzClpSjDFgTwQfy5/TPBDDoCBGjUV8jdMohjt3cvpYNE6rKDkiFAVpzFhA3EgozLHohLZXFWNDA5Y7FCLvFG8IYM+f6QS9mg7FUmJ8OJUuhC8F7aMfLDAVjWBMMwub2RnE2Dsabk+aaNy53zYIJPb8yYK/okHjjF09LUhowT+xQlUgezPEzhd5DOvyCAS2uzvvuFQ7d8JsVYGVo2L4QqIK8J7M0eQkvonv0DLDHGUeDJtYoV8mWONH9ZfZHgTKvsWMwUfknibmj4QC+UJHkrcwUrnC5oT9izDwxybP0vp+nWBdvLgejWU4D61LMnw+JVT8wOXORxf/fbz4DWNrB0rIX4ixBINCLKMjbqHs44weJIE6SAbNMCAuNM9wHnJgS9vAEck7BYwLci3GMRwMtp9J0IoZc0JRe4QS5eG0oP8G3QPLP/wb81NPoC4bkDYCJCLWZywshsu7Rzj30rtNmPMR8E/VhtbGYPPh7hez+DmwrNDmAivd1N+jtexy2N4sdPCYOG5Cmc/mZfGbS+BDpeicBq3V5AwaLTop4KTBo6r/JHkYZE2tOWot3TSUNGrg9XjKSMroQiAwFBR5fyXxp2hNrxVMJjjfZx0yyf9oFzvkM/JjPj/tqqLg+ibyHw+UCNe1pnUgYJB/DzMOIMuOduMh9D0J+nwH2njKUT1mVU4AVHqltcWpHnb/P5zMvnk74wbhv+s83iioEs9USnFiGHVRiSEz6NJ/pBOuUBXnS2h1zFL9C2TKVOPf2cAuPbF/Zy/Qj3oCOngPxfQDSctFZfwUo0IUrCfzgbMY4Xt8i9kW4FgncKQrgAq+ce+imIVH5ItnrodFzW73HdiAIyxRMwAKFauApB/ARM9hv888T93bn4NIalajot9ZAmPYeM01PS6YDBz5ziYAd4v0g/UFJNg+HArS8eOBF3b5ROUm1fL0zfRF7iAZP3L5g3h/EQJA5CEuy2wrx+4E3l5Nmm98QoFwpFeX6C7Pjtnye7ldMof4DlYA5TwrJ/f8Z5AuO7mrGt8tkr6TgtTuOnLJ0ix2EL+XGCnIfmWcfT7LvBd8IMaA6NBxPvT5txkmqebfa4rlEgmKNrVV4f0Ey8fWcFWmiph6Gm5EsclqonoamPpPwYyg4DSCulgLgEirdnCLUocRJFGIiSiWGJVYp2vxIuv6h/odBA3bBGcxAclm/i2izjc2rYNpky2vO6Cz+W1dLJ+G2LYcbhHEjkecn6uVqIV7bYN5cfmu3FbL6DA4eMDGh4+2y9WP5hYStoy2vsfaE83vzJ4fxBmOrR1CXfN06qe/MFCib8aMpdah6oOcfCe1+O1k46pCGHQFNs/6PRPDCp6S6jnvHLB3CYRYhuiwMuDGHxoEsaeTGzeT2uCxSAZruugyvsv8uuSvaZraMy5ijchrBM4sQOztoPmOsB6kvoHlGhk/GKTJePXjwUhYB5wOAes5HwGfK8Te06mO/3Oawf0fZDGBUO4Vpy4dw0l37IrCxDdSE8hOIOZidiWFIHU0/JFQbpDo5O2LdiHYUOfOYFPgStfDRt5jCeVoaC0NpORMY4Jd4PCf4c5d5cN9QGAQiYvI7j1NtGComGS3nm26aK6yoavg6F47WOXrlvP0Qby2/KVRhBTyr3CCDE6IqT4LusY/KdePii0DKTf4chaIPIW4JFQkAUiL5Yi5soEs7fc6glInxy5pOdCYaZJiBKvOuauBGOe5psBXIJFbyG/kTibyZ+Vj8esWDx+nEQsYS61U3UhUll0E0NKIKx2eETyew1OEykvz7c6Vs34ChnudcX62tXfNxiGI6IFi4s33RyLmRcHKj4W+vvRMIOJmsk5b6xMgzvElbLaV+0eCIyrkU1GXHTeF3kA8NfXB8vwDQUyqWMzVPssnpghoELhbAZMzgCOi9vQCAn02eQsHDY0lfiPW4HKJCIvwOgrz8QmEIB0gLKNF6LWIbg+dvyMoQvia8l3+8CK4ewVIRRP07++Xj3UshVuBBC0kEL/gVjihDHlqkwi7xawr3xMrTXGlbiPcJfDLBC4iHOBSBsIq7VEIfr4xYYuJIODkZA5iOsGKgddzB4HwR2T5D5mwrraMNX+/qBdzmCV9mHRzC09zH8YGDi8l/KdR6G6AzyrGx8JNJS00c1vrwenaX+CspeIM+PnB9Cmu5F2LzF2w+uSxpQJghdeQ10Y5xt0DlZOy9masb6limYwKf2wNzjXo4C+Ty9HNjrr1ymoFdahBAk0mEd2veUPFZhhVaWp5WjYOD5yARCcBZMdwEF/eEg6wYNgOWvg3WQN5fiT4MhjwPoi4TpykVhrWBdGb+ZsudIICdx+ZKM6HJ3LWVextSLsI7wXtpci1A93hdGF0d7h7SPoEN0NW5aGwMOv6c8jvQQ2wAfxrEwkPxFeG9YG0eoAurw4Jw8XfSR0H647wtnJvEy8Cl4y/AN5D1FeCenmfeMyU6AEyzkisdH0kkcgfPv0G7vOIAulM5FKCdg/SgoB6bnFDQot+P/0rzseI1dHFJGgYvfx8C/jIQdjMTIVApO0IJ/vmDMXwijIQd+cXuZ8WBAQq1cWBnCYHAYWqsbLirncfNsA4H3GVqYDazic8DcQdJfClphm/g9SO8Pnj2JD4vTJQHwuGiXMi8gdu9FNDKLfSAvz8oi/M+zihYvyu0n0sSWrvBtAO4n8vCY7Op+G7EfWsj2hzRFhD2RV54CaZJeecyvU4wcubv89CGzD6M/TDc/CRHYloWT0ePEUUYkLqNLI62Nr7eFsyAvV/iCafOiooSSTs/WUlGUUa+49LMgF7A4PqVPYZLB6ds88WtSZNm+XsFob81Su5bZZ515RWce6cgirtIwMRt/PqbtDkItW3yiNRIcRD1U6hFAifIaOei9ezmYsGwFhxzsD93zVGMBatnvTGb9dArEGA0Xeomwhxgytfscl3ju9+JXc4krhzKuS14FU2eelok/OJh0Xt900WIpVGZQrhDqTCYY80PIyFDQLMiDyI/HB3mGspqjyNoknWADD2kLXEZrgV0KP0n7P8Wy/pp9XCPAbIlD6ARpnU79C4FnSVnK/q54umphj3v1KMAc7V4OxusHk22EyXQaoGr2Xn0WsL/iEKGcU5vxZJ4BYfxq43r8McQ/wL5tjFyfC5xMroCaVBCNKFztrZ7lCgBBJhyUCxrOTOHzZMoPWHAl6yaMZfooM536kNe13QobEkipNNyAB86lpocqVwzmvc16cExBuNh8VjJWhyPOJMiiVdn40nvqF8ke92pSwLna7Rzc5a8OxRXzDF5m35SYcjECCRd6kqqQBjNy4vh5rMbHMYc+Txwmd1JnQL6n00qa5dsoIYCUhQUjrSXRGmrZhCkuWSOR5asE/Mg4pNkQF0tJ4iYj3l7oiK9Camy+yr0lwuaLAI34JsriGkbwLyI+G5g7ED4tnttK21lJvAYl1ML4vCTJA6ISHt9Yr60OJUR+/Iov8D3uVaZA+auM/3WHfhoCgZAlrQ8vxt7I5VhzA4xXi/FQCJsyC1GJIFYjYf7az6+AGcDacy+sjYcntUjOP8O0txCfRN5nCLVO/v7fMhrwd/3cW6b9WbQJnBdP6bRAqR+eovp4w9/7U+BiiUq7GwGYB6CPIPyKX6s8lPJ49Yz6jyB8PyFvHuVPs69roEyF0N4p9BpIgsI6FE0fhG8zQhaPbSYgeBSUclLsJ18aTp2wPe41oADztXs5OEsG78BgJGTQ1nrCKQgi6RAM4FpnkHdyJixbEYr13VAeJyxlT7k3hYchnB5iLMZ7DcdwQn+bcAxx29MSrif9R/xmJP4EJSUKaIc8LbKWuQn4BcTnIHgPAPIgndtAnqeXPn8cj7+U8nrC7yHhs4ApOvKL4yNOtWyswMb+EmBfnNYym3b5HK/HAScNeh4jFCn52kSgf4/7/6WAjNyQWU+XrMhS5hDQiQjRf1I+GO52OXgTQjebvL1ZD34MgXL5+Rh+P+p9GrivkHEzy90Wyrz+fT0PuIUJN4clJBZrIw8np5Rj9ThIem4+FnrsFsuVQHvCHgrsHhRAaF761ULs89LoKff4X+9JasTdbxp/IdtPDkagRmJFv8kjkoMSDGERD3G/eIi687O9nGbNO1dckrp01NL1uB4K7H4UgPndjxUFgLiCpleAkuD5LR5GK9aDVQibv2yk4MQK5EUef7DvPI50jUL1aLbfU0gVPH9uzTtr4heVgBVX+kUk8duOBzI9rocCuy8FckEIgWpPBYVlPlbL0HwFaElmufwuUAGLMsKq3DKGMJFWiBVuBSyEN+ElXVNHXm4BO5QlmJ6whwK7DQUQCAVJYTGMpWIdAtLYbnloeW69FLgQLNMKnYTKYbWKWr5kPUNo8zwFsiyHiyUsafOM+/yux/VQYPejgMyPj6Wgoyeu8GgJkxCF4JAXwumpKns+nycWhTV/PS7VCyJSbj2tpPnRRsJvyOMRv0ZPwtpjBYNqu96//wUw6hdhr8VrBAAAAABJRU5ErkJggg==',
            width: 134,
            absolutePosition: {
                x: 243,
                y: 45
            }
        }
    }
    var revokeFundingAcc = {}
    if ((rokeStatus == true) || (expiredStatus == true)) {
        revokeFundingAcc = {
            text: '* Funds revoked or moved to a company is transferred to the program’s corresponding funding account.',
            margin: [-20, 10, 0, 0],
            fontSize: 9
        }
    }

    var escrowText = "";
    let closeBalanceText = 'CLOSING BALANCE'
    if (escrowTransactionsData.length > 0 && newEscrowBalance > 0) {
        closeBalanceText = 'CLOSING BALANCE *'
        escrowText = [{
            text: "* Closing Balance",
            margin: [-20, 10, 0, 10],
            fontSize: 9,
            color: '#190f27'
        },
            {
                text: "Card Balance                        ₹" + (transactionAPI.balanceAfterPeriod.amount / 100),
                margin: [-14, -6, 0, 10],
                fontSize: 9
            },
            {
                text: "Holding A/c Balance           ₹" + newEscrowBalance,
                margin: [-14, -6, 0, 10],
                fontSize: 9
            }]
    }

    var transactionSummaryInfo = [{
        text: 'OPENING BALANCE',
        color: '#756F7D',
        fontSize: summarySecTitleSize,
        absolutePosition: {x: 35, y: 150}
    },
    {
        text: Utils.formatCurrency(transactionOpeningBalance.amount, transactionOpeningBalance.currency),
        bold: true,
        font: 'Roboto',
        fontSize: summaryAmountTextSize,
        absolutePosition: {x: 35, y: 165}
    },
    {
        text: 'CREDITS',
        color: '#756F7D',
        fontSize: summarySecTitleSize,
        absolutePosition: {x: 185, y: 150}
    }, {
        text: Utils.formatCurrency((transactionCredits.amount + totalAmountOfEscrowCreditReversal), transactionCredits.currency),
        bold: true,
        font: 'Roboto',
        fontSize: summaryAmountTextSize,
        absolutePosition: {x: 185, y: 165}
    }, {
        text: 'DEBITS',
        color: '#756F7D',
        fontSize: summarySecTitleSize,
        absolutePosition: {x: 325, y: 150}
    },
    {
        text: Utils.formatCurrency((transactionDebits.amount + totalAmountOfEscrowDebitReversal), transactionDebits.currency),
        width: 10,
        bold: true,
        font: 'Roboto',
        fontSize: summaryAmountTextSize,
        absolutePosition: {x: 325, y: 165}
    }, {
        text: closeBalanceText,
        color: '#756F7D',
        fontSize: summarySecTitleSize,
        absolutePosition: {x: 465, y: 150}
    },
    {
        text: Utils.formatCurrency(transactionClosingBalance.amount, transactionClosingBalance.currency),
        bold: true,
        fontSize: summaryAmountTextSize,
        font: 'Roboto',
        absolutePosition: {x: 465, y: 165}
    },
    {
        text: '\n',
        bold: true,
    },
    ]

    // Claim summary box changes

    var claimsSummaryTitle = {
        text: 'Claims summary',
        style: 'subHeader',
        font: 'Roboto',
        margin: [-20, 0, 10, -5],
        fontSize: 16
    }

    //totalClaimedCount, approvedClaimedCount, declinedClaimCount, inReviewClaimCount, "₹" + totalClaimedAmount, approvedClaimedAmount, declinedClaimAmount, inReviewClaimAmount

    var claimSummaryInfo = [{
        text: totalClaimedCount,
        color: '#756F7D',
        fontSize: summarySecTitleSize,
        absolutePosition: {x: 35, y: 265}
    },
        {
            text: "₹" + totalClaimedAmount,
            bold: true,
            font: 'Roboto',
            fontSize: summaryAmountTextSize,
            absolutePosition: {x: 35, y: 280}
        },
        {
            text: approvedClaimedCount,
            color: '#756F7D',
            fontSize: summarySecTitleSize,
            absolutePosition: {x: 185, y: 265}
        }, {
            text: approvedClaimedAmount,
            bold: true,
            font: 'Roboto',
            fontSize: summaryAmountTextSize,
            absolutePosition: {x: 185, y: 280},
            color: '#00b898'
        }, {
            text: declinedClaimCount,
            color: '#756F7D',
            fontSize: summarySecTitleSize,
            absolutePosition: {x: 325, y: 265}
        },
        {
            text: declinedClaimAmount,
            width: 10,
            bold: true,
            font: 'Roboto',
            fontSize: summaryAmountTextSize,
            absolutePosition: {x: 325, y: 280},
            color: '#e74c3c'
        }, {
            text: inReviewClaimCount,
            color: '#756F7D',
            fontSize: summarySecTitleSize,
            absolutePosition: {x: 465, y: 265}
        },
        {
            text: inReviewClaimAmount,
            bold: true,
            fontSize: summaryAmountTextSize,
            font: 'Roboto',
            absolutePosition: {x: 465, y: 280},
            color: '#ffa500'
        },
        {
            text: '\n',
            bold: true,
        },
    ]

    var claimSummary = [{
        type: 'rect',
        x: -20,
        y: 20,
        w: 140,
        h: 70,
        lineColor: '#e5e3e9'
    },
        {
            type: 'rect',
            x: 120,
            y: 20,
            w: 140,
            h: 70,
            lineColor: '#e5e3e9'
        },
        {
            type: 'rect',
            x: 260,
            y: 20,
            w: 140,
            h: 70,
            lineColor: '#e5e3e9'
        },
        {
            type: 'rect',
            x: 400,
            y: 20,
            w: 140,
            h: 70,
            lineColor: '#e5e3e9'
        },
        {
            type: 'ellipse',
            x: 120,
            y: 55,
            color: 'white',
            lineColor: '#e5e3e9',
            fillOpacity: 1,
            fillColor: 'white',
            r1: 15,
            r2: 15
        },
        {
            type: 'ellipse',
            x: 260,
            y: 55,
            color: 'white',
            lineColor: '#e5e3e9',
            fillOpacity: 1,
            fillColor: 'white',
            r1: 15,
            r2: 15
        },
        {
            type: 'ellipse',
            x: 400,
            y: 55,
            color: 'white',
            lineColor: '#e5e3e9',
            fillOpacity: 1,
            fillColor: 'white',
            r1: 15,
            r2: 15
        }
    ]

    var plusForClaim1 = {
        text: '+',
        fontSize: 20,
        color: '#8B8792',
        Opacity: 0.5,
        absolutePosition: {x: 435, y: 275}
    }
    var plusForClaim2 = {
        text: '+',
        fontSize: 20,
        color: '#8B8792',
        Opacity: 0.5,
        absolutePosition: {x: 295, y: 275}
    }
    var equalToForClaim = {
        text: '=',
        fontSize: 20,
        color: '#8B8792',
        absolutePosition: {x: 154.5, y: 275}
    }

    // Claim summary box changes ends here

    if (transactionAPI.totalRecords == 0) {
        transactionStatementTable = emptyValue;
        transactionStatementTitle = emptyValue;
    }

    if (!escrowTransactionsData.length || !escrowTransactionsData) {
        escrowStatementTitle = "";
        escrowStatementTable = [];
    } else {
        transactionSummaryInfo[7].text = transactionClosingBalance.currency + (Utils.formatINR(transactionClosingBalance.amount + newEscrowBalance));
    }

    if (!transactionAPI.cardTransactions.length || !transactionAPI.cardTransactions) {
        transactionSummaryTitle = "";
        transactionSummary = [];
        transactionStatementTitle = "";
        transactionStatementTable = [];
        transactionSummaryInfo = [];
        // plus = emptyValue
        // minus = emptyValue;
        // equalTo = emptyValue;

        let tempCount = 0;
        claimSummaryInfo.forEach(function (item) {
            if (tempCount % 2 == 0 && item.absolutePosition !== undefined) {
                item.absolutePosition.y = 155;
            } else if (item.absolutePosition !== undefined) {
                item.absolutePosition.y = 170;
            }
            tempCount++;
        })
        plusForClaim1.absolutePosition.y = 162;
        plusForClaim2.absolutePosition.y = 162;
        equalToForClaim.absolutePosition.y = 162;
        escrowText = '';
        newEscrowBalance = 0;
    }

    if (newEscrowBalance > 0) {
        let tempCount = 0;
        claimSummaryInfo.forEach(function (item) {
            if (tempCount % 2 == 0 && item.absolutePosition !== undefined) {
                item.absolutePosition.y = 325;
            } else if (item.absolutePosition !== undefined) {
                item.absolutePosition.y = 340;
            }
            tempCount++;
        })
        plusForClaim1.absolutePosition.y = 335;
        plusForClaim2.absolutePosition.y = 335;
        equalToForClaim.absolutePosition.y = 335;
    }

    if (billerAPI.bills.length == 0) {
        claimsStatementTable = emptyValue;
        claimsStatementTitle = emptyValue;
        claimsSummaryTitle = emptyValue;
        plusForClaim1 = emptyValue;
        plusForClaim2 = emptyValue;
        equalToForClaim = emptyValue;
        claimSummary = [];
        claimSummaryInfo = [];
    }

    if (userImage == "") {
        userImage = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAACz9JREFUeAHlXVtMHNcZPrPAXox3uSywgMFQHIpNQhzc9KFItRorVaRKcaQ0z5asqFLzEB6cWK7UG71Jrez6wX5oXqJIfrajxpZSpbVIlUZuq1a1a7sYbIIxlwWMue2y5s72+4adzbLM7s7szBlscqTVOXMu/+Wb/1znnLOK2EYXiUQqFhYW2tfX11sgRks8Hm9RFKUaYT/CfvoJ8aKIjyIcRfw4wn0I97lcrj6fz3c9EAg8SuRz3FOc5AjlfRMTEy8DsCMA4Qh4tyHOkgygEwedW6DTDUC7Q6HQVcQtOKWXJeGNCEmAxsfHDyPvMfzewHPASLl88wC8CMpexO9CdXX1ZwmA8yWXs5w0AAGUZ2xs7DgUOIlwU05JJGQA7wHwPl1TU/MBwksSWAjbAYTAPgD3Qwj8LsK1MoQ2SxOyhCHLGQD5HsK2Vm9bAURVfRWCnsOv0aySTuQHeIP4daJqX7GLny0AhsPhBghG4I7aJZhMOpD1ssfjebu8vHzIKh/LAAK81yHE+/iVWhXG4fKz4PdmbW3th1b4uvItDGtzo607h/KX8HvawKPalPkSdaAujMjH5WWBc3Nz5bFYjO1IRz5Mn8Ay14qLi18tKSmZNiubaQCnpqbqlpeXP8FbazXL7EnOj3axx+12vxIMBkfMyGkKwMnJyZaVlZW/gEG9GSZPUd7hoqKi71ZWVnKqaMgZBpCWt7S0dA1Udyp4GmDD6KE7jFqioU6EbR6r7VcAPIJYT12ps4ZoNj8ngOyh2GHstDYvGyjUNaFzzt45J4CYXZwBs53S22bDLT2tI6F7evym56xtYGKQzHHeV9l9P9tgOyOAnJ4BtRv4OTpIjsUWxMLjRbG2tqa+tKKiQuH1eoRvl08oGaWV+n5nvV7vwUzTvsJMrBNzW0fAi0ZiYnxsUkxPz4nV1VVdkQoKCkRZWUBUVpWL8qAjYmlylGL0cR4Pr2kRqb7uO+WqClaNL6dmlBFeWloWX9wbEjMzc6bI0yLr6qtFqLrCVDkrmbHafVRvFWcLgOiBfACwB36jFYa5ykbm5sWd//WL1URVzZVfL93vLxbNX29E9fbqJdsahxo5CABb4W9aT9zSC2Ny/ZZs8ObnH4sei+ARnWg0Jv57o1c8mpyxFSw9YsSEC8XpaZsARCYPEH4nPZOdz+wc7vbeT3YSVmmTXl/vgNqGWqWVqzyw4Sq7JzXfpk4ECB9HopRl+Bl0ECPD46rVQIhUGWwJf9E/JAoLC0VFZZkt9PSIQO7aBEbvaelJC0QiAFZOagl2+qMj42qVjUTmhQzwNFnv3R0Ujx9vaqK0JNt8YkSsNIJJANFxHEZCk5Zglx9De/dgMGwXuax0MHIQ9/oG8ZKyZrOUSIyIlUYkCSAijmmRdvrh0YdSrS5dVnZQkw+n0qPtfk5ipQIIVH3g8IbdXEhvbjYqg2xWmqMjE1nTbUjkBgFiJlQAud0CEbbvGIivx8XS8rIN8pojwXaQU0JZjlgRM9JXAeReFRnMVjJMy2TwSqc5NxtJj7L1WcNMBRA9ixQAXds0+ydSbAtlOg0zF7eYgVGbDGaFWEnhIsB2uNXVjdUcibzbiJ2L+/NQp5PjGrsZlpTstpukMXoShzIUgJgROxcC+41JlF+uUixBbYdze4qks0U72EIAuTtUmisrL5FGOxthLnk54PZLB5CK7MJqstNut3+XdJY0PnSUSkg2J64kO+12F8sHkNhxGOOXqRznp64CdbQkk80m2uz5F7Ha7YDzswpLBZBLWMNDYw7o8iULrhHevN4rVlf0v698mdNaiNhJt8DFRSlbk3NqHheYRsq3QhXAnMJYyYDNOlaKWypbUCh/EE8LlLpc4tBwYgvQ+IomPJ6cOzO2lDMZEWUvLBXA4mLnhzAEgXyhm0k8zGUndtIt0B8o3pb5cKBEat+oIR1lLyx19ZFWsB3jwGBQ/gwI2I2zChvejanBbtavCgXNFrGU3+fzCn9A/iIGsWMV7rUkrYHCnA+XljozG6HFN35tjwGpbMnCE6Mu6RZIUZ9taxbPPtdsi9TZiLQfanVs8xGxc/G8Ld6a5NWzDZW5tMXqJcsFUG2d2CdD+YkZsXMlDivfkqVUOt092FUly9XUVskirUf3FrFjG8jV1W69HDLiqrC/T8bg2uvxiGCFc/sGNcxUAFGXHQOQjXz93hrb3019Q430gXOq0BpmKoCJY/JyvwOmcK+sCmKmYN963e7du7Bz1bmhEowgQsyokgogIvgVmsfkHXEwQrGvea8tFqPgzPi+5gbQckR0jcnFBGYbACZiL2ipTvjcXWpHo1+3t1rQAh12SaxUCyTzxAUNA04K0tC4RxDIfF1ZWQnaUynbGTOKBMsbIFZahiSASEDHEj+tJTjhu1yKaMXgmkCYdSzTcqDJ6arLEctpYqXJu6nlQKIHe99404Wjr5X7+UaGx9Slf/DWZNP1uV2kDr14XT17Xd0s0iIBXBjW1wQ/ucyetEByZQIU4NEuRx2B4NDmYPuBrHNmzqkPHjqg5nUaPAJCbFLBY9yWd4hMjhxzoKUtL6/orhrPY/f9zHRE8HsKq3kxOgmCp7fCvLiwJDxeN18+9ZHmQF/3mIMuV1RjKQdtInNRMfRgRAw/GBXh0XHBT57tLz4vvvHNg3kB0HfnnvjbX/8BkF1qld7bWI/qXYteOf+OKdMbAA9jB200AtiN/hGs5Kj2nI+/trYOoMbEyFBYDA2OiNlZ/RNJNXuqxeHvfEuUGuxM5udj4u+f/0sM9A/qilUeLBN7G/YIAhqqrlQB1s1oMBLWdxmX9ryml13XApkx38OG89F5WNmoChitDFcE6PHdEscq2NzSJJ57vjXjrGJmelb03O4TvT33Mp6pSyfsdhfBKgEmAK1vqMtnm0nWw4YZAaQgRo+7xmKPRd+dftF/dwBt12y6DqafuZocqq5Sp3us5tyy+3BiUkRxTMKqq6yqEPueaRQtrc9gUcPQ0lp+x101QVGVea/K29pzqs8q+u9//kfcvoldANu4nTdVJqNhfq9uf7FNvHCoLWP7i1pxHlW3MxvNrBbIggCPF+x8imBHKiFa3Z+uXBVTj6ZTo5+6cE1tSLzyvSPoybdsh7sG8F4CiFk32WwaB+ppTwK8lAZ+j5bOKvXRpY+fevCoz1h4Qlz54ydiOWUbCHVN6JwVPJbPCSAz8UYfXkqD4DDHb3/+uNuW9oi0nwTHWvTp1c81UYapq9FbjAwBSMq8R4WX0ty+eWd4YnxSY7Zj/MH7Q6K//7568Y7RO2OovGEAmZk3+ty42dOBhjNZnRm/Exx1unGjp8PMrUXU2xSALHDqVOeIcHu/jXaCtxjtCKfqAp06O38wYlYh0wCSQVfXiWlRtO8lvLXzZhk+aflVHaCLqlMewqG8Ndf1k9+9LuJr76Nvce6TmDWR1dKY+MwKpeDNrl+f+tAKubwsMJUhBSgU7hfwbUL6LR+pfK2EVVmLXAetgkcZLFtgqiK/+PFvNy6hFfHG1PgnJQzgBtHedf78Nz+6YpdMtgJIoc6ePeuLTC2+JeLKO9in7OjKdiZQAFxYKPHfB4LeP5w4ccLWc7C2A6gpce7cOc/0ROw49nqfBJBNWryTPoAbQB07XR4q/qCzszO5DG+nDNIA1ITEzEX51c9OH15fXzuGmTVOeouAlibDR+eADQLKRZer4MJPf3nyM1TZ7B9ZLAohHcBU+Vi9o1PLL0OjIyLOQ95KG6zTkgywMpCL3xKKqxuEuv1B91W7q2mqDulhS8KnEzP73NV1pkJZX2sX6/H9cf4lhqK0gEYI1Z4bnP3AZmOjc3zjrzBQHaOIn8ASUZ/CfY0upTfuKrje1fXuI7O87cr/f8v9Dcw684X1AAAAAElFTkSuQmCC';
    } else {
        userImage = 'data:image/png;base64,' + userImage.base64;
    }

    if (productType.toLowerCase() == 'meal' || productType.toLowerCase() == 'gift') {
        claimsStatementTable = emptyValue;
        claimsStatementTitle = emptyValue;
        claimsSummaryTitle = emptyValue;
        plusForClaim1 = emptyValue;
        plusForClaim2 = emptyValue;
        equalToForClaim = emptyValue;
        claimSummary = [];
        claimSummaryInfo = [];

        plus.absolutePosition.y = 159;
        minus.absolutePosition.y = 151;
        equalTo.absolutePosition.y = 159;
        transactionSummaryTitle = {
            text: 'Transaction summary',
            style: 'subHeader',
            margin: [-20, 0, 0, -10],
            font: 'Roboto',
            fontSize: 16
        }
        transactionSummaryInfo.forEach(function (item) {
            if (item.absolutePosition) {
                item.absolutePosition.y += 1;
            }
        })
    }

    var dd = {
        info: {
            title: 'Employee Statement Zeta ' + userName,
            author: 'Zeta - BWTPL',
            subject: 'Year End Report',
            keywords: 'Zeta',
        },
        footer: function (currentPage, pageCount) {
            return [
                {
                    text: '( ' + Utils.epochToIstDate(new Date().getTime()) + ', ' + Utils.epochToIstTime(new Date().getTime()) + ' hrs ) This is a system generated statement. For any help contact us on '+supportEmail,
                    color: 'gray',
                    fontSize: 8,
                    absolutePosition: {
                        x: 20,
                        y: 20
                    },
                },
                {
                    text: 'Page ' + currentPage + ' of ' + pageCount,
                    absolutePosition: {
                        x: 535,
                        y: 20
                    },
                    color: 'gray',
                    fontSize: 8
                }
            ]
        },
        // footer end
        content: [
            {
                canvas: header
            },
            headerContent,
            {
                image: userImage,
                width: 40,
                height: 40,
                absolutePosition: {
                    x: 30,
                    y: 18
                }
            },
            {
                canvas: [{
                    type: 'ellipse',
                    x: 10,
                    y: -68,
                    color: '',
                    lineColor: '#a5149f',
                    fillOpacity: 0.1,
                    fillColor: 'white',
                    r1: 25,
                    r2: 25,
                    lineWidth: 10
                }]
            },
            transactionSummaryTitle,
            {
                canvas: transactionSummary
            },
            plus,
            minus,
            equalTo,
            escrowText,
            transactionSummaryInfo,
            closedCardImage,
            claimsSummaryTitle,
            {
                canvas: claimSummary
            },
            equalToForClaim,
            plusForClaim1,
            plusForClaim2,
            claimSummaryInfo,
            hrLine,
            transactionStatementTitle,
            transactionStatementTable,
            escrowStatementTitle,
            escrowStatementTable,
            claimsStatementTitle,
            claimsStatementTable,
            {text: '', margin: [0, 100, 0, 0]},
        ],

        styles: {
            headerTextOne: {
                fontSize: 15,
                color: '#fff'
            },
            headerTextTwo: {
                fontSize: 12,
                color: '#FDFAFD'
            },
            headerTextThree: {
                fontSize: 9,
            },
            subHeader: {
                fontSize: 16,
                color: '#190f27',
                marginLeft: -20
            },
            small: {
                fontSize: 8
            },
            APPROVED: {
                color: '#00b898'
            },
            PARTIALLY_APPROVED: {
                color: '#00b898'
            },
            UPLOADED: {
                color: '#ffa500'
            },
            DECLINED: {
                color: '#e74c3c'
            },
            DECLINED_AFTER_APPROVAL: {
                color: '#e74c3c'
            }
        },
        // defaultStyle: {
        //     font: 'Roboto'
        // }
        defaultStyle: {
            font: 'Roboto'
        }
    }

    return dd;
}

let downloadSodexoPDF = function (pdfData, userName) {
    pdfData.info = {
        title: 'Employee Statement ' + userName,
        author: 'Sodexo - Vietnam',
        subject: 'Year End Report',
        keywords: 'Sodexo',
    }
    return pdfData;
};

module.exports = {
    init: init
}
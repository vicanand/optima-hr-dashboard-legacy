Constant = require('../common/constant');
Env = process.env.NODE_ENV || 'development_stage';
var init = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    res.locals.pageName = 'power';
    var pageData = {
        companyID: req.params.companyId
    };
    var corpID = res.locals.corp.id;
    var userID = req.session.user_profile.userID;
    var catalogId = '131412432';
    var url;
    if (Env == "development_stage") {
        url = 'https://power-center-stage.zetaapps.in/#/?catalogId='+catalogId+'&authToken='+authToken;
    } else if(Env == "development_preprod"){
        url = 'https://power-center-pp.zetaapps.in/#/?catalogId='+catalogId+'&authToken='+authToken;
    } else {
        url = 'https://power-center.zetaapps.in/#/?catalogId='+catalogId+'&authToken='+authToken;
    }
   // console.log("!------------------------IN THE SURVEY FLOW---------------------------!");
    res.render('section/power-center/home',{corpID,authToken,userID,url});
    res.end();
}

module.exports = {
    init,
}
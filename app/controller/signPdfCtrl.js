var Config = require('../../config');
var EmployeeSvc = require('../service/employeeStatementSvc');
var Urls = require('../common/url');
var Constant = require('../common/constant');
var Util = require('../common/util');
let init = function(req, res, next){
    let documentUrl = req._parsedUrl.query.split('url=')[1];
    let authToken = req.session[Constant.SESSION_AUTH].authToken;

    let url = Config.omsBaseUrl + Urls.GET_SIGNED_DOCUMENT
        .replace(":authToken:", authToken)
        .replace(":s3FileUrl:", documentUrl)
        .replace(":apiKey:", Config.corpBenAPIToken)
        .replace(":x:", 450)
        .replace(":y:", 30)
        .replace(":width:", 130)
        .replace(":height:", 30);
    console.log(url);
    EmployeeSvc.getSignedPdf(url).then(function(resp){
        let test = Util.htmlDecoder(resp);
        res.end(test);
    }, function(err){
        res.end(JSON.stringify(err));
    })
}

module.exports = {
    init: init
}
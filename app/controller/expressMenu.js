var ZetaMerchantSvc = require('../service/express/zetaMerchantSvc'),
    Constant = require('../common/constant'),
    moment = require('moment'),
    Util = require('../common/util'),
    BusinessSvc = require('../service/express/businessSvc'),
    ExpressVendorsCtrl = require('./expressVendorsCtrl');



var vendorMenu = function (req, res, next) {
    res.locals.pageName = 'express';
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    var corpID = res.locals.corp.id;

    var pageData = {
        bannerTitle: 'Vendor Insights',
        companyID: req.params.companyId,
        currentTab: 'menu',
        businessID: req.params.businessID
    };



    var params = {
        corporateID: corpID
    };

    ZetaMerchantSvc.getVendors(authToken, {corporateID: corpID}).then(function (vendorsResponse) {
        ZetaMerchantSvc.getCitiesOfficeStore(authToken, params).then(function (cityData) {
            var businessData = ExpressVendorsCtrl.filterVendors(vendorsResponse, pageData.businessID);
            var cities = ExpressVendorsCtrl.filterCitiesByBusinessId(cityData.expressInsightsStoreList, pageData.businessID);
            pageData.businessData = businessData;
            pageData.citiesOfficesStores = JSON.stringify(cities);
            res.render('section/express/vendorMenu', pageData);
            res.end();
        });
    });


};



module.exports = {
    vendorMenu
};
var Constant = require('../common/constant');
var Util = require('../common/util');
var AdminSvc = require('../service/adminSvc');
var LosslessJSON = require('lossless-json');
var RecommonedProgramsModel = require('../model/recommonedProgramsModel');
//var JSONbig = require('json-bigint');
const env = process.env.NODE_ENV || 'development_stage';

let init = function(req, res, next) {
    res.locals.pageName = 'admin';
    res.render('admin', {});
    res.end();
};

let companies = function(req, res, next) {
    res.locals.pageName = 'admin';
    const adminCompanies = {};
    adminCompanies.templateData = {
        pageHeading: 'Create companies in Zeta if your business has multiple legal entities and you wish to seperate their benefits, rewards and expenses programs accordingly.'
    }
    if (req.session[Constant.SESSION_CONFIG_DATA]) {
        const config_data = Util.getDataFromObject(req.session[Constant.SESSION_CONFIG_DATA], 'adminCtrl.admin-companies.templateData');
        if (config_data) {
            adminCompanies.templateData = config_data;
        }
    }
    res.render('admin-companies', adminCompanies);
    res.end();
};

let companyDetails = function(req, res, next) {
    res.locals.pageName = 'admin';
    let companyID = req.params[0];
    let companyDetailsData = res.locals.companies.find(function(item) {
        return item.company.companyID == companyID;
    });
    let corpID = res.locals.corp.id;
    let displayTag = false;
    let closedCards = [];
    let activeCards = { "programs": [] };
    AdminSvc.getActiveCardsAll(req.session[Constant.SESSION_AUTH].authToken, corpID, companyID, 0, 12).then(function(respData) {
        let benefitProgramData = {};
        let totalResp = []
        benefitProgramData["programsObj"] = JSON.parse(respData);
        let totalBenefitPrograms = benefitProgramData["programsObj"].totalRequestCount;
        let pageNum = 2;
        let totalPages = Math.ceil(totalBenefitPrograms / 12);
        let benefitProgramsPromise = [];
        //   console.log('first call programs ', benefitProgramData["programsObj"].programs);
        for (pageNum; pageNum < totalPages + 1; pageNum++) {
            benefitProgramsPromise.push(AdminSvc.getActiveCardsAll(req.session[Constant.SESSION_AUTH].authToken, corpID, companyID, pageNum - 1, 12));
        }
        Promise.all(benefitProgramsPromise).then(function(respArr) {
                //  console.log('this is response', JSON.parse(respArr));
                respArr.forEach(function(respData) {
                    let benefirProgDet = JSON.parse(respData);
                    benefirProgDet.programs.forEach(function(progDetails) {
                        benefitProgramData["programsObj"].programs.push(progDetails);
                    })
                })
            }).catch(function(err) {
                console.log('error', err)
            }).then(function() {
                //   console.log('all programs ', benefitProgramData["programsObj"].programs);
                programs = benefitProgramData["programsObj"].programs;
                //   console.log('All Programs', programs);
                programsCount = programs.length;
                for (let i = 0; i < programs.length; i++) {
                    if (programs[i].corpProductType === 'OPTIMA') {
                        if (programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate) {
                            var lastIssuanceDate = new Date(programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate);
                        } else {
                            var lastIssuanceDate = '';
                        }
                        if (Util.compareLastIssuanceToCurrentDate(lastIssuanceDate)) {
                            closedCards.push(programs[i]);
                        } else {
                            activeCards.programs.push(programs[i]);
                            if (programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate) {
                                let compareTenDaysBefore = Util.compareTenDaysBefore(programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate);
                                if (compareTenDaysBefore) {
                                    displayTag = true;
                                }
                            }
                            programs[i].cardProgramObject.taxStatus = Constant.CARDS_TAX_STATUS[programs[i].cardProgramObject.productType.toLowerCase()];
                        }
                    }
                }
                // respObj.programs = activeCards;
                console.log('closed cards', closedCards);
                let companyDetailsPageData = {
                    "activeCards": activeCards,
                    "closedCards": closedCards,
                    "displayTag": displayTag,
                    "companyDetails": companyDetailsData,
                    "companyID": companyID
                };
                companyDetailsPageData.templateData = {
                    showTaxStatus: true
                }

                if (req.session[Constant.SESSION_CONFIG_DATA]) {
                    const config_data = Util.getDataFromObject(req.session[Constant.SESSION_CONFIG_DATA], 'adminCtrl.companyDetails.templateData');
                    if (config_data) {
                        companyDetailsPageData.templateData = config_data;
                    }
                }

                res.render('admin-companyDetails', companyDetailsPageData);
                res.end();
            })
            .catch(function(error) {
                res.locals.showErrorWitHdr = true;
                Util.errorFoundHandler(error, req, res, next);
            });
    });
};

let fundingAccHome = function(req, res, next) {

    res.locals.pageName = 'fundingAccounts';
    let rbac = res.locals.rbac

    let disableOpenFundingAcc = false
    if(rbac){
        disableOpenFundingAcc = !Util.checkPriviledge(rbac, 'addFundingAccount', res.locals.selectedCompany.company.companyID);
    } 



    let benefitHomePageData = {};
    AdminSvc.getFundingAcc(req.session[Constant.SESSION_AUTH].authToken, res.locals.corp.id, 1, 10, null, res.locals.selectedCompany.company.companyID, res.locals.rbac).then(function(fundRespData) {

            let pageData = {
                //"fundingAccounts" : JSON.parse(fundRespData).fundingAccounts
            };

            benefitHomePageData["fundingAccounts"] = JSON.parse(fundRespData);
            let totalFundCount = benefitHomePageData["fundingAccounts"].count;
            let pageNum = 2;
            let totalPages = Math.ceil(totalFundCount / 10);
            let fundingAccPromise = [];
            for (pageNum; pageNum < totalPages + 1; pageNum++) {
                fundingAccPromise.push(AdminSvc.getFundingAcc(req.session[Constant.SESSION_AUTH].authToken, res.locals.corp.id, pageNum, 10, null, res.locals.selectedCompany.company.companyID, res.locals.rbac));
            }
            Promise.all(fundingAccPromise).then(function(respArr) {
                respArr.forEach(function(fundResData) {
                    let fundAccDet = JSON.parse(fundResData);
                    fundAccDet.fundingAccounts.forEach(function(accDetails) {
                        benefitHomePageData["fundingAccounts"].fundingAccounts.push(accDetails);
                    })
                })
                req.session["fundingAccounts"] = benefitHomePageData["fundingAccounts"].fundingAccounts;
                pageData = {
                    "fundingAccounts": req.session["fundingAccounts"]
                };
            }).catch(function(err) {
                console.log('error', err)
            }).then(function() {
                const isSodexoPhpSm = res.locals.countryVar === "isSodexoPhpSm";
                if (pageData.fundingAccounts) {
                    for (var currAcc = pageData.fundingAccounts.length - 1; currAcc >= 0; currAcc--) {
                        if (pageData.fundingAccounts[currAcc].type == 'NOT_ZETA_SETTLED') {
                            pageData.fundingAccounts.splice(currAcc, 1);
                        }
                    }
                    for (var currIndex = 0; currIndex < pageData.fundingAccounts.length; currIndex++) {
                        let companiesFunded = [];
                        // if (pageData.fundingAccounts[currIndex].balance) {
                        //     pageData.fundingAccounts[currIndex].balanceInPaise = pageData.fundingAccounts[currIndex].balance;
                        //     pageData.fundingAccounts[currIndex].balance = Util.formatINRWithPaise(pageData.fundingAccounts[currIndex].balance);
                        // }
                        pageData.fundingAccounts[currIndex].balance = Util.formatCurrency(pageData.fundingAccounts[currIndex].balance / 100, pageData.fundingAccounts[currIndex].currency);
                        for (var currProgram = 0; currProgram < pageData.fundingAccounts[currIndex].programsFunded.length; currProgram++) {
                            if (companiesFunded.length > 0) {
                                var newCompany = true;
                                for (var currComp = 0; currComp < companiesFunded.length; currComp++) {
                                    if (pageData.fundingAccounts[currIndex].programsFunded[currProgram].companyID.value == companiesFunded[currComp].companyId) {
                                        newCompany = false;
                                        var program = {
                                            "programId": pageData.fundingAccounts[currIndex].programsFunded[currProgram].id,
                                            "programType": Constant.CARD_PROGRAM_TYPES[pageData.fundingAccounts[currIndex].programsFunded[currProgram].productType],
                                        }
                                        companiesFunded[currComp].programs.push(program);
                                    }
                                }
                                if (newCompany) {
                                    var company = {
                                        "companyId": pageData.fundingAccounts[currIndex].programsFunded[currProgram].companyID,
                                        "companyName": pageData.fundingAccounts[currIndex].programsFunded[currProgram].companyName,
                                        "programs": []
                                    };
                                    var program = {
                                        "programId": pageData.fundingAccounts[currIndex].programsFunded[currProgram].id,
                                        "programType": Constant.CARD_PROGRAM_TYPES[pageData.fundingAccounts[currIndex].programsFunded[currProgram].productType],
                                    }
                                    company.programs.push(program);
                                    companiesFunded.push(company);
                                }
                            } else {
                                var company = {
                                    "companyId": pageData.fundingAccounts[currIndex].programsFunded[currProgram].companyID,
                                    "companyName": pageData.fundingAccounts[currIndex].programsFunded[currProgram].companyName,
                                    "programs": []
                                };
                                var program = {
                                    "programId": pageData.fundingAccounts[currIndex].programsFunded[currProgram].id,
                                    "programType": Constant.CARD_PROGRAM_TYPES[pageData.fundingAccounts[currIndex].programsFunded[currProgram].productType],
                                }
                                company.programs.push(program);
                                companiesFunded.push(company);
                            }
                            pageData.fundingAccounts[currIndex].companiesFunded = companiesFunded;
                        }
                    }
                }
                pageData.templateData = {
                    bannerBgClass: 'gradient-indigo-blue',
                    breadcrumbs: [{href: '/admin', text: 'Admin Console'}],
                    headingText: 'Funding Accounts',
                    subHeadingText: 'Create a Zeta business account and get a funding account (similar to your Bank’s current account) to successfully add funds to transfer to your Beneficiaries.',
                    subHeadingLearnMoreLink: 'https://docs.zetaapps.in/display/OND/Managing+Funding+Account',
                    disableOpenFundingAcc: disableOpenFundingAcc
                };
                pageData.fundingAccountsForTransfer = pageData.fundingAccounts;
                pageData.ifisDetails = ''
                if (pageData.fundingAccounts && pageData.fundingAccounts.length > 0 && (pageData.fundingAccounts.ifisDetails == undefined || pageData.fundingAccounts.ifisDetails == "")) {
                    AdminSvc.getIfisFundingAcc(req.session[Constant.SESSION_AUTH].authToken, res.locals.corp.id, pageData.fundingAccounts[0].ifiID, req).then(function(getIfisData) {
                        let getIfisBankDetails = JSON.parse(getIfisData);
                        req.session[Constant.SESSION_IFIS_DETAILS] = getIfisBankDetails;
                        pageData.ifisDetails = getIfisBankDetails.bankDetails;

                        req.session[Constant.SESSION_ALL_FUNDING_ACCOUNTS] = pageData;
                        // SODEXO Changes
                        pageData.templateData = {
                            bannerBgClass: 'gradient-indigo-blue',
                            breadcrumbs: [{href: '/admin', text: 'Admin Console'}],
                            headingText: 'Funding Accounts',
                            subHeadingText: 'Create a Zeta business account and get a funding account (similar to your Bank’s current account) to successfully add funds to transfer to your Beneficiaries.',
                            subHeadingLearnMoreLink: 'https://docs.zetaapps.in/display/OND/Managing+Funding+Account',
                            disableOpenFundingAcc: disableOpenFundingAcc,
                            addAccountHeading: 'With funding accounts your funds are instantly updated when you transfer money via IMPS/NEFT.'
                        };
                        pageData.fundingAccountsForTransfer = pageData.fundingAccounts;
                        if (isSodexoPhpSm) {
                            if (pageData.fundingAccounts.length) {
                                pageData.fundingAccountsForTransfer = pageData.fundingAccounts.filter((item) => item.name.indexOf('Issuance') != -1);
                            }
                        }

                        if (req.session[Constant.SESSION_CONFIG_DATA]) {
                            const config_data = Util.getDataFromObject(req.session[Constant.SESSION_CONFIG_DATA], 'adminCtrl.fundingAccHome.templateData');
                            if (config_data && config_data) {
                                pageData.templateData = config_data;
                            }
                            if(!pageData.templateData.hasOwnProperty('disableOpenFundingAcc')){
                                pageData.templateData.disableOpenFundingAcc = disableOpenFundingAcc
                            }
                        }
                        
                        res.render('fundingAccHome', pageData);
                        res.end();
                    }).catch(function(error) {
                        //console.log('error' + error);
                        res.locals.showErrorWitHdr = true;
                        Util.errorFoundHandler(error, req, res, next);
                    });
                }else{
                    res.render('fundingAccHome', pageData);
                    res.end();
                }
            })
        })
        .catch(function(error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });
};


let getFundingAccount = function(req, res, next) {
    let accId = req.params[0];
    AdminSvc.getFundingAcc(req.session[Constant.SESSION_AUTH].authToken, res.locals.corp.id, 1, 100).then(function(fundRespData) {
            AdminSvc.getAccDetails(req.session[Constant.SESSION_AUTH].authToken, res.locals.corp.id, accId).then(function(fundDetails) {
                    let pageData = {
                        "fundingAccounts": JSON.parse(fundRespData).fundingAccounts,
                        // "fundingAccounts": req.session["fundingAccounts"],
                        "accountId": accId,
                        "fundDetails": JSON.parse(fundDetails)
                    };
                    res.locals.pageName = 'admin';
                    // SODEXO Changes
                    pageData.templateData = {
                        bannerBgClass: 'gradient-indigo-blue',
                        breadcrumbs: [{href: '/admin', text: 'Admin Console'}, {href: '/admin/funding-accounts', text: 'Funding Accounts'}],
                        accountStmt: {
                            enableDateRangeDownload: false
                        },
                        fundAdditions: {
                            enableDateRangeDownload: false
                        }
                    };

                    if (req.session[Constant.SESSION_CONFIG_DATA]) {
                        const config_data = Util.getDataFromObject(req.session[Constant.SESSION_CONFIG_DATA], 'adminCtrl.funding-history');
                        if (config_data) {
                            if (config_data.templateData) {
                                pageData.templateData = config_data.templateData;
                            }
                            if (config_data.pageName) {
                                res.locals.pageName = config_data.pageName;
                            }
                        }
                    }

                    res.render('funding-history', pageData);
                    res.end();
                })
                .catch(function(error) {
                    console.log("why?" + error);
                })
        })
        .catch(function(error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });

};

let getLegacyFundingAcc = function(req, res, next) {
    res.locals.pageName = 'admin';
    res.render('funding-history-legacy');
    res.end();
};

let addFundsToAccount = function(req, res, next) {
    res.locals.pageName = 'admin';
    // SODEXO Changes - START
    const pageData = {};
    let currencySymbol = res.locals.currencySymbol ? res.locals.currencySymbol : '₹';
    pageData.templateData = {
        bannerBgClass: 'gradient-indigo-blue',
        breadcrumbs: [{href: '/admin', text: 'Administration'}, {href: '/admin/funding-accounts', text: 'Funding Accounts'}],
        headingText: 'Details of your cheque/transfer to Zeta’s Bank Account',
        transferTypes: [{value: 'NEFT', text: 'NEFT', tranferNumberLabel: 'Transaction Number', trasferDateLabel: 'Transfer Date'}, {value: 'CHEQUE', text: 'Cheque', tranferNumberLabel: 'Cheque Number', trasferDateLabel: 'Cheque Date'}],
        addFundFields: [
            {
                wrapperClass: 'pd-top-twenty',
                fieldLabel: 'Transaction Number',
                labelFor: 'chequeNumber',
                inputClass: 'width-xlg',
                inputId: 'transactionRefNumber',
                name: '',
                isCalendarInput: false,
            },
            {
                wrapperClass: 'date-picker-xlg',
                fieldLabel: 'Transfer Date',
                labelFor: 'transferDate',
                inputClass: 'bdr-radius datepicker-img',
                inputId: 'transactionDate',
                name: '',
                isCalendarInput: true,
            },
            {
                wrapperClass: '',
                fieldLabel: `Amount ( ${currencySymbol} )`,
                labelFor: 'chequeAmount',
                inputClass: 'width-xlg',
                inputId: 'transactionAmount',
                name: '',
                isCalendarInput: false,
            }
        ],
        disableFooterNote: false,
    };

    if (req.session[Constant.SESSION_CONFIG_DATA]) {
        const config_data = Util.getDataFromObject(req.session[Constant.SESSION_CONFIG_DATA], 'adminCtrl.addFund');
        if (config_data) {
            if (config_data.templateData) {
                pageData.templateData = config_data.templateData;
            }
            if (config_data.pageName) {
                res.locals.pageName = config_data.pageName;
            }
        }
    }

    res.render('addFund', pageData);
    res.end();
};

let accessCtrlHome = function(req, res, next) {
    let pageData = {
        "accId": req.session[Constant.SESSION_ACC_INFO].account.accountDetails.id,
        "addUserRoles": false
    };
    const config_data = Util.getDataFromObject(req.session[Constant.SESSION_CONFIG_DATA], 'adminCtrl.accessCtrl');
    if(config_data && config_data.addUserWithRoles) {
        pageData.accessRoles = [
            // {
            //     role: 'Company Admin',
            //     canRead: true,
            //     canWrite: false,
            //     roleReadId: 'COMPANY_ADMIN_READ',
            //     roleWriteId: 'COMPANY_ADMIN_WRITE',
            //     rowClass: ''
            // },
            {
                role: 'Program Manager',
                canRead: true,
                canWrite: true,
                roleReadId: 'PROGRAM_MANAGER_READ',
                roleWriteId: 'PROGRAM_MANAGER_WRITE',
                rowClass: 'other-roles'
            },
            // {
            //     role: 'Beneficiary Manager',
            //     canRead: true,
            //     canWrite: true,
            //     roleReadId: 'BENEFICIARY_MANAGER_READ',
            //     roleWriteId: 'BENEFICIARY_MANAGER_WRITE',
            //     rowClass: 'other-roles row-disabled'
            // },
            // {
            //     role: 'Document Manager',
            //     canRead: true,
            //     canWrite: true,
            //     roleReadId: 'DOCUMENT_MANAGER_READ',
            //     roleWriteId: 'DOCUMENT_MANAGER_WRITE',
            //     rowClass: 'other-roles row-disabled'
            // },
            {
                role: 'Fund Manager',
                canRead: true,
                canWrite: true,
                roleReadId: 'FUND_MANAGER_READ',
                roleWriteId: 'FUND_MANAGER_WRITE',
                rowClass: 'other-roles'
            },
            // {
            //     role: 'Report Manager',
            //     canRead: true,
            //     canWrite: false,
            //     roleReadId: 'REPORT_MANAGER_WRITE',
            //     rowClass: 'other-roles row-disabled'
            // },
        ];
        pageData.addUserRoles = true;
    }
    res.locals.pageName = 'admin';
    res.render('access-control', pageData);
    res.end();
};

let accessCtrlV2Home = function(req, res, next) {
    let pageData = {
        "accId": req.session[Constant.SESSION_ACC_INFO].account.accountDetails.id
    };
    let companyID = res.locals.selectedCompany.company.companyID;
    res.locals.pageName = 'admin';
    // SODEXO_PHILIPPINES_SM Changes - START
    // accessTypes : Sodexo Super Admin : {text: 'Sodexo', value: 'ADMIN'}
    pageData.templateData = {
        bannerBgClass: 'royal-blue-sodexo',
        breadcrumbs: [{href: '/admin/access-controlV2', text: 'Admin Console'}],
        headingText: 'Access Control',
        subHeadingText: 'Grant access & manage users for your corporate account.',
        accessTypes: [{text: 'BCO Manager/Supervisor', value: 'PHP_COMPANY_ADMIN'}, {text: 'BCO Customer Service Agent', value: 'OPERATOR'}, {text: 'Branch Accounting', value: 'ACCOUNTANT'}, {text: 'Branch Treasury', value: 'TREASURY'}],
        filterByStatus: ['active', 'suspended', 'pending']
    };
    if (res.locals.rbac && res.locals.rbac['addAccountForEmployee_'+companyID] && res.locals.rbac['addAccountForEmployee_'+companyID].indexOf('own') !== -1) {
        pageData.templateData.accessTypes = pageData.templateData.accessTypes.filter((item) => item.value !== 'PHP_COMPANY_ADMIN' && item.value !== 'ACCOUNTANT' && item.value !== 'TREASURY');
    }
    // SODEXO_PHILIPPINES_SM Changes - END
    res.render('access-controlV2', pageData);
    res.end();
};

module.exports = {
    init: init,
    companies: companies,
    companyDetails: companyDetails,
    fundingAccHome: fundingAccHome,
    getFundingAccount: getFundingAccount,
    addFundsToAccount: addFundsToAccount,
    getLegacyFundingAcc: getLegacyFundingAcc,
    accessCtrlHome: accessCtrlHome,
    accessCtrlV2Home: accessCtrlV2Home
};

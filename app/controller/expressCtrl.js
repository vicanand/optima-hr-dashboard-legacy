var QuickshopInfoSvc = require('../service/express/quickShopInfoSvc'),
    Constant = require('../common/constant'),
    ZetaMerchantSvc = require('../service/express/zetaMerchantSvc'),
    moment = require('moment'),
    Util = require('../common/util'),
    ExpressVendorsCtrl = require('../controller/expressVendorsCtrl');

var toHHMMSS = function (seconds) {
    if (seconds == 0) {
        return 0;
    }


    var days = Math.floor(seconds / 86400);
    var hours = Math.floor((seconds % 86400) / 3600);
    var minutes = Math.floor(((seconds % 86400) % 3600) / 60);
    var secs = ((seconds % 86400) % 3600) % 60;


    // var sec_num = parseInt(sec, 10); // don't forget the second param
    // var days = Math.floor(sec_num / 86400);
    // var hours = Math.floor(sec_num / 3600);
    // var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    // var seconds = sec_num - (hours * 3600) - (minutes * 60);

    // if (hours   < 10) {hours   = "0"+hours;}
    // if (minutes < 10) {minutes = "0"+minutes;}
    // if (seconds < 10) {seconds = "0"+seconds;}
    // return hours+':'+minutes+':'+seconds;

    let formatedTime = '';

    if (days && days != 0) {
        formatedTime += days + '<sub>d</sub> ';
    }
    if (hours && hours != 0) {
        formatedTime += hours + '<sub>hr</sub> ';
    }
    if (minutes && minutes != 0) {
        formatedTime += minutes + '<sub>m</sub> ';
    }
    if (secs && secs != 0) {
        formatedTime += secs + '<sub>s</sub> ';
    }

    return formatedTime;
};


var init = function (req, res, next) {
    res.locals.pageName = 'express';
    var pageData = {
        bannerTitle: 'Vendor Insights',
        companyID: req.params.companyId
    };

    res.render('section/express/home', pageData);
    res.end();
};

var cafeteriaInsightOverview = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    res.locals.pageName = 'express';
    var corpID = res.locals.corp.id;


    var pageData = {
        bannerTitle: 'Cafeteria Insights',
        companyID: req.params.companyId
    };

    let params = {
        corporateID: corpID,
        businessID: req.params.businessID
    };


    ZetaMerchantSvc.getCitiesOfficeStore(authToken, params).then(function (cityData) {
   
        var cities = ExpressVendorsCtrl.filterCitiesByBusinessId(cityData.expressInsightsStoreList, pageData.businessID);

        pageData.citiesOfficesStores = JSON.stringify(cityData.expressInsightsStoreList);


        console.log("************************",pageData);
        res.render('section/express/cafeteriaInsightsOverview', pageData);
        res.end();
    }, function (error) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(error, req, res, next);
    });
};

var cafeteriaInsightMenu = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    res.locals.pageName = 'express';
    var corpID = res.locals.corp.id;

    var pageData = {
        bannerTitle: 'Cafeteria Insights',
        companyID: req.params.companyId
    };

    var params = {
        corporateID: corpID
    };

    ZetaMerchantSvc.getCitiesOfficeStore(authToken, params).then(function (respData) {
        pageData.citiesOfficesStores = JSON.stringify(respData.expressInsightsStoreList);     
        res.render('section/express/cafeteriaInsightsMenu', pageData);
        res.end();
    }, function (error) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(error, req, res, next);
    });
};

var getVendorsTrends = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    var corpID = res.locals.corp.id;
    req.body.corporateID = corpID;
    ZetaMerchantSvc.getVendorsTrends(authToken, req.body).then(function (data) {
        res.status(200);
        res.send(data);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);   
    });
};

var getStoreTrends = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    var corpID = res.locals.corp.id;
    req.body.corporateID = corpID;
    ZetaMerchantSvc.getStoreTrends(authToken, req.body).then(function (data) {
        res.status(200);
        res.send(data);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
};

var getListOfUploadedMenus = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;

    QuickshopInfoSvc.getListOfUploadedMenus(authToken, req.query).then(function (respData) {
        res.status(200);
        res.send(respData);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
};

var getQuickshopInfo = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;

    QuickshopInfoSvc.getQuickshopInfo(authToken, req.query).then(function (respData) {
        res.status(200);
        res.send(respData);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
};

var getQuickshopInfoDate = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;

    QuickshopInfoSvc.getQuickshopInfoDate(authToken, req.query).then(function (respData) {
        res.status(200);
        res.send(respData);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
};

var getCafeStatistics = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;

    req.body.corporateID =  res.locals.corp.id;
    // let params = {
    //     fromDate: moment().subtract(30, 'days').format('YYYY-MM-DD'),
    //     toDate: moment().format('YYYY-MM-DD'),
    //     corporateID: corpID,
    // };


    ZetaMerchantSvc.getCafeStatistics(authToken, req.body).then(function (respData) {
        res.status(200);
        res.send(respData);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
};



module.exports = {
    init,
    cafeteriaInsightOverview,
    getVendorsTrends,
    cafeteriaInsightMenu,
    getQuickshopInfo,
    getQuickshopInfoDate,
    getStoreTrends,
    getListOfUploadedMenus,
    getCafeStatistics
};
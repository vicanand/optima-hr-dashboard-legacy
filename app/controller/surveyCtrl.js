var ZetaMerchantSvc = require('../service/express/zetaMerchantSvc'),
moment = require('moment'),
Constant = require('../common/constant');
Env = process.env.NODE_ENV || 'development_stage';
var init = function (req, res, next) {
    // console.log('res: ', res);
    // console.log('req: ', req);
    // console.log("Env ",Env);
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    res.locals.pageName = 'Survey';
    var pageData = {
        companyID: req.params.companyId
    };
    var corpID = res.locals.corp.id;
    var userID = req.session.user_profile.userID;
    var url;
    if (Env == "development_stage") {
        url = 'https://survey-stage.zetaapps.in';
    } else {
        url = 'https://survey.zetaapps.in';
    }
   // console.log("!------------------------IN THE SURVEY FLOW---------------------------!");
    res.render('section/survey/home',{corpID,authToken,userID,url});
    res.end();
}

module.exports = {
    init,
}
const moment = require("moment");

const _ = require('underscore');
var ReportCentreSvc = require('../service/reportCentreSvc');
var Constant = require('../common/constant');
var Util = require('../common/util');

let setProduct = function(req, res, next){
    if(req.query.product == 'express'){
        res.locals.pageName = 'reportCentreExpress'; 
        req.session['product'] = req.query.product.toUpperCase(); 
    } else if(req.query.product == 'expense'){
        res.locals.pageName = 'reportCentreExpense';
        req.session['product'] = req.query.product.toUpperCase();
    }
    else{
        res.locals.pageName = 'reportCentre';
        req.session['product'] = 'OPTIMA';
    }
}

let init = function (req, res, next) {

    let xprogramtype = []; //new RB

    let cardProgramsMap = {};
    req.session["programs"].map(cardProgram => {
        cardProgramsMap[cardProgram.programID] = cardProgram.cardProgramObject.cardName
    });

    cardProgramsMap = JSON.stringify(cardProgramsMap);

    // req.session["cardProgramsMap"] = JSON.parse(Util.htmlDecoder(cardProgramsMap));


    setProduct(req, res, next);
    
    if (res.locals.reportCentreActive) {
        let corpID = res.locals.corp && res.locals.corp.id,
            companyID = Constant.SESSION_SELECTED_COMP_INFO,
            authToken = req.session[Constant.SESSION_AUTH].authToken;
        ReportCentreSvc.getDashboardReports(authToken, corpID, req.session['product']).then(function (respData) {
            respData = JSON.parse(respData);
            dashboardReports = _.keys(respData.dashboardReports);
            dashboardReports = dashboardReports.sort();
            var productTypes = new Object();
            //new RB
            ReportCentreSvc.getCorpdetails(authToken, corpID).then(function (resp) {
                resp = JSON.parse(resp);
                var programsList;
                var companyID;
                var companyName;
                var cardProgramSummaryPerCompany = resp.cardProgramSummaryPerCompany;
                let comapnyWithAccess = req.session['comp_info'];
                let respData1 = {};
                comapnyWithAccess.map(i => {
                    respData1[i.company.companyID] = cardProgramSummaryPerCompany[i.company.companyID]
                });

                req.session["ALL_COMPANIES"] = resp.cardProgramSummaryPerCompany;

                req.session["COMPANY_MAP"] = respData1;

                for (var key in respData1) {
                    if (respData1.hasOwnProperty(key)) {
                        for (var pType in respData1[key]) {
                            programsList = respData1[key].programs;
                            companyID = respData1[key].companyID;
                            companyName = respData1[key].companyName;
                            if (programsList) {
                                for (var i = 0; i < programsList.length; i++) {
                                    if (productTypes[programsList[i].productType]) {
                                        productTypes[programsList[i].productType][companyID] = companyName;
                                    } else {
                                        productTypes[programsList[i].productType] = {};
                                    }
                                }
                            }

                        }
                    }
                }
                let reportCategorymap = {};
                Object.keys(respData.dashboardReports).forEach((i, j) => {
                    respData.dashboardReports[i].reports.map(report => {
                        reportCategorymap[report.reportID] = respData.dashboardReports[i].sectionDisplayName;
                    })
                });

                reportCategorymap = JSON.stringify(reportCategorymap);

                req.session["reportCategorymap"] = JSON.parse(Util.htmlDecoder(reportCategorymap));

                xprogramtype = Object.keys(productTypes);

                dashboardReports = dashboardReports.filter(section => {
                    if(respData.dashboardReports[section].hidden){
                        delete(respData.dashboardReports[section]);
                    } else {
                        return section;                                                    
                    }
                });
                
                //filter out reports not needed for Tata motors
                //TODO - Gatekeeper to be added
                if(corpID === '61263') {
                    respData.dashboardReports = filterReportsForTataMotors(respData.dashboardReports);
                }

                let reportCentrePageData = {
                    "status": "success",
                    "message": "",
                    "mail": "",
                    "dashboardReports": dashboardReports,
                    "respData": respData.dashboardReports,
                    "programs": req.session["programs"],
                    "companyID": companyID,
                    "fundingAccounts": req.session["fundingAccounts"],
                    "onlyCompany": req.session['comp_info'],
                    "xprogramtype": xprogramtype,
                    "productTypes": productTypes,
                    "resp": resp
                };

                res.render('report-centre', reportCentrePageData);
                res.end();

            }, function (resperr) {
                res.locals.showErrorWitHdr = true;
                Util.errorFoundHandler(resperr, req, res, next);
            }); // upto here new RB
        }, function (error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });
    } else {
        res.end();
    }
};

let reportDetails = function (req, res, next) {
    var productSource = req.query.product || '';
    if(productSource == 'express'){
        res.locals.pageName = 'reportCentreExpress';
    }else if(productSource == 'expense'){
        res.locals.pageName = 'reportCentreExpense';
    }else{
        res.locals.pageName = 'reportCentre';
    }

    let programsData = req.session["programs"];
    let reportId = req.params.reportId
    let corpID = res.locals.corp && res.locals.corp.id,
        authToken = req.session[Constant.SESSION_AUTH].authToken,
        companies = req.session["ALL_COMPANIES"];
    ReportCentreSvc.getReportById(authToken, corpID, reportId).then(resp => {
        resp = JSON.parse(resp);
        let data = {};
        data.startDate = resp.userParameters.START_DATE ? moment(parseInt(resp.userParameters.START_DATE[0])).utcOffset(330).format("DD MMM YYYY") : '';
        data.endDate = resp.userParameters.END_DATE ? moment(parseInt(resp.userParameters.END_DATE[0])).utcOffset(330).format("DD MMM YYYY") : '';
        data.generationTime = resp.createdAt ? moment(parseInt(resp.createdAt)).utcOffset(330).format("DD MMM YYYY hh:mm:ss a") : '';
        data.productTypes = resp.userParameters.PRODUCT_TYPE || [];
        data.generatedReportUrl = '/report-centre'+(productSource ? '?product='+productSource : '')+'#Generated-Reports'
        data.fundingAccs = resp.userParameters.FUNDING_ACCOUNT_NAMES || [];
        data.programIds = resp.userParameters.PROGRAM_IDS || [];
        data.reportId = resp.reportID;
        if(resp.reportID != 'ESCROW_BALANCES_PROGRAM_LEVEL' && resp.reportID != 'BENEFICIARY_SPENDS_REPORT'){
            data.programIds = []
        }

        if(resp.userParameters.COMPANY_IDS){
            data.rep_companies = resp.userParameters.COMPANY_IDS.map(i => {
                return companies[parseInt(i)].companyName;
            });
        } else if (resp.userParameters.COMPANY_ID){
            data.rep_companies = resp.userParameters.COMPANY_ID.map(i => {
                return companies[parseInt(i)].companyName;
            });
        }else{
            data.rep_companies = []
        }

        data.url = resp.url;

        data.reportName = resp.reportDisplayName;
        if(resp.userParameters.PROGRAM_IDS && resp.userParameters.PROGRAM_IDS.length > 0){
            ReportCentreSvc.getProgramDetails(authToken,resp.userParameters.PROGRAM_IDS,corpID).then(function(programDetails){
                data.programsData = programDetails.programs
                res.render('report-centre/report-details', data);
                res.end();
            })
        }else{
            res.render('report-centre/report-details', data);
            res.end();
        }
            

    }, (error) => {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(error, req, res, next);
    });
};

let getReportList = function (req, res, next) {
    let pageNumber = req.query.pageNo,
        pageSize = req.query.pageSize,
        corpID = res.locals.corp && res.locals.corp.id,
        authToken = req.session[Constant.SESSION_AUTH].authToken,
        companies = req.session["ALL_COMPANIES"];
    cardPrograms = req.session["cardProgramsMap"];
    // setProduct(req, res, next);

    ReportCentreSvc.getReportList(authToken, pageNumber, pageSize, corpID, req.body, req.session['product']).then(resp => {
        resp = JSON.parse(Util.htmlDecoder(JSON.stringify(resp)));
        var tempProgramIds = []
        resp.reports.forEach(function(report){
            if(report.userParameters.PROGRAM_IDS && report.userParameters.PROGRAM_IDS.length > 0){
                report.userParameters.PROGRAM_IDS.forEach(function(id){
                    if(tempProgramIds.indexOf(id) < 0){
                        tempProgramIds.push(id);
                    }
                })
            }
        })
        if(tempProgramIds.length > 0){
            ReportCentreSvc.getProgramDetails(authToken,tempProgramIds,corpID).then(function(programDetails){
                resp = prepareReportList(resp, companies, programDetails, req)
                res.setHeader('Content-Type', 'application/json');
                res.status(200).json(resp);
            })
        }else{
            resp = prepareReportList(resp, companies, {}, req)
            res.setHeader('Content-Type', 'application/json');
            res.status(200).json(resp);
        }
        
            
    }, (error) => {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(error, req, res, next);
    })
};

let prepareReportList = function(resp, companies, programDetails, req){
    resp.reports = resp.reports.map(report => {
        report["category"] = req.session.reportCategorymap[report.reportID];

        if (report.userParameters.COMPANY_IDS) {
            report.userParameters.COMPANIES = report.userParameters.COMPANY_IDS.map(i => {
                if (companies[parseInt(i)] && companies[parseInt(i)].companyName) {
                    return Util.htmlDecoder(companies[parseInt(i)].companyName);
                }
            });
        }
        else if (report.userParameters.COMPANY_ID) {
            report.userParameters.COMPANIES = report.userParameters.COMPANY_ID.map(i => {
                if (companies[parseInt(i)] && companies[parseInt(i)].companyName) {
                    return Util.htmlDecoder(companies[parseInt(i)].companyName);
                }
            });
        }
        else {
            report.userParameters.COMPANIES = [];
        }

        report.userParameters.CARD_PROGRAMS = !report.userParameters.PROGRAM_IDS ? [] : report.userParameters.PROGRAM_IDS.map(i => {
            return programDetails.programs[parseInt(i)].cardDesign.cardName || '--';
        })

        return report;
    });

    return resp;
}

let filterReportsForTataMotors = function(reports){
    let list = [];
    list = reports["REIMBURSEMENT_REPORTS"].reports.filter(item => {
        return item.reportID != 'EMPLOYEE_REIMBURSEMENT_MASTER_REPORT_CLAIM_LEVEL_SODEXO_INDIA' && item.reportID != 'SDX_IND_BALANCE_REPORT'
    });
    reports["REIMBURSEMENT_REPORTS"].reports = list;
    return reports;
}

module.exports = {
    init: init,
    reportDetails: reportDetails,
    getReportList: getReportList
};

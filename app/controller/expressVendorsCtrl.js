var ZetaMerchantSvc = require('../service/express/zetaMerchantSvc'),
    moment = require('moment'),
    Constant = require('../common/constant'),
    Util = require('../common/util'),
    QuickshopSvc = require('../service/express/quickShopInfoSvc'),
    BusinessSvc = require('../service/express/businessSvc');

var init = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    res.locals.pageName = 'express';
    var corpID = res.locals.corp.id;

    var pageData = {
        bannerTitle: 'Vendor Insights',
        companyID: req.params.companyId,
    };

    var params = {
        corporateID: corpID
    };

    ZetaMerchantSvc.getCitiesOfficeStore(authToken, params).then(function (citiesData) {
        pageData.citiesOfficesStores = JSON.stringify(citiesData.expressInsightsStoreList);
        res.cookie('companyID', pageData.companyID);
       
        res.render('section/express/vendorInsights', pageData);
        res.end();
    }, function (error) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(error, req, res, next);
    });

};

var vendorOverview = function (req, res, next) {
    res.locals.pageName = 'express';
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    var corpID = res.locals.corp.id;

    var pageData = {
        bannerTitle: 'Vendor Insights',
        companyID: req.params.companyId,
        currentTab: 'overview',
        businessID: req.params.businessID
    };


    let params = {
        fromDate: moment().subtract(30, 'days').format('YYYY-MM-DD'),
        toDate: moment().format('YYYY-MM-DD'),
        businessID: req.params.businessID
    };


    res.cookie('businessID', params.businessID);


    ZetaMerchantSvc.getVendors(authToken, {
        corporateID: corpID
    }).then(function (vendorsResponse) {
        var businessData = filterVendors(vendorsResponse, params.businessID);
        params.corporateID = corpID;
        QuickshopSvc.getVendorInsightsSnapshot(authToken, params).then(function (respData) {
            pageData.businessData = businessData;
            pageData.respData = respData;

            pageData.respData.totalSales = Util.formatINR(pageData.respData.totalSales);
            console.log("expressVendorsCtrl------------>",pageData);   
            res.render('section/express/vendorOverview', pageData);
            res.end();
        }, function (error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });
    }, function (error) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(error, req, res, next);
    });


};


var getVendorInsightsSnapshot = function(req, res, next){
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    var corpID = res.locals.corp.id;
    
    req.query.corporateID = corpID;
    // req.query.businessID = req.cookies.businessID;

    QuickshopSvc.getVendorInsightsSnapshot(authToken, req.query).then(function (respData) {
        res.status(200);
        res.send(respData);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
}

var getVendors = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    var corpID = res.locals.corp.id;


    req.body.corporateID = corpID;
    ZetaMerchantSvc.getVendors(authToken, req.body).then(function (respData) {

        respData.vendorDetails.map(i => {
            i.vendorName = Util.htmlDecoder(i.vendorName);
        })


        res.status(200);
        res.send(respData);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
};

var getStoresByBusiness = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    var corpID = res.locals.corp.id;


    req.query.corporateID = corpID;


    ZetaMerchantSvc.getStoresByBusiness(authToken, req.query).then(function (respData) {
        res.status(200);

        res.send(respData);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
};


var filterVendors = function (vendors, businessID) {
    var vendorList = vendors.vendorDetails;

    var selectedVendor = vendorList.find(function (item) {
        return item.businessID == businessID;
    });

    var otherVendors = vendorList.filter(function (item) {
        return item.businessID != businessID;
    });

    return {
        selectedVendor,
        otherVendors
    };
};

var filterCitiesByBusinessId = function (cities, businessID) {
    return cities.filter(function (item) {
        return item.businessID == businessID;
    });
};


module.exports = {
    init,
    vendorOverview,
    getVendors,
    getStoresByBusiness,
    filterVendors,
    getStoresByBusiness,
    filterCitiesByBusinessId,
    getVendorInsightsSnapshot
};
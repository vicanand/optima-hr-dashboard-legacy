var AdminSvc = require('../service/adminSvc');
var BenefitSvc = require('../service/benefitSvc');
var SpotlightSvc = require('../service/spotlightSvc');
var Constant = require('../common/constant');
var Util = require('../common/util');
var Utils = require('../common/utils/user');
var LosslessJSON = require('lossless-json');
var JSONbig = require('json-bigint');
var Config = require('../../config');
var Urls = require('../common/url');

let init = function (req, res, next) {
    res.locals.pageName = 'spotlight';
    let corpID = req.session.corp_info && req.session.corp_info.id;
    let companyID = req.params.companyId;
    let companyIDs = req.session.comp_list.corporateCompanies[0].companies;
    let companyName = req.session.selected_comp_info.company.companyName;
    let accountID = req.session.acc_info.account.accountDetails.id;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    var benefitHomePageData = {};
    let benefitProgramData = {};
    let isRnREnabled = false;
    setSelectedCompany(req, res);

    const promisesListInit = [
        BenefitSvc.getMoreActiveCards(authToken, corpID, companyID, 0),
        AdminSvc.getFundingAcc(authToken, corpID, 1, 10),        
        SpotlightSvc.listProgramDetails(authToken, companyID, corpID)        
    ];

    Promise.all(promisesListInit).then(respData => {
        // ifi name

        //fundingAccount
        let fundResData = respData[1];
        benefitHomePageData["fundingAccounts"] = JSON.parse(respData[1]);
        let totalFundCount = benefitHomePageData["fundingAccounts"].count;
        let pageNum = 2;
        let totalPages = Math.ceil(totalFundCount / 10);
        let fundingAccPromise = [];
        for (pageNum; pageNum < totalPages + 1; pageNum++) {
            fundingAccPromise.push(AdminSvc.getFundingAcc(authToken, corpID, pageNum, 10));
        }

        //rnr check 
        let rnrPrograms = JSON.parse(respData[2]).programDetail;
        if(rnrPrograms){
            isRnREnabled = rnrPrograms.length === 0 ? false : true;
        }

        //card programs
        benefitProgramData["programsObj"] = JSON.parse(respData[0]);
        let totalBenefitPrograms = benefitProgramData["programsObj"].totalRequestCount;
        let pageX = 1;
        let totalPagesX = Math.ceil(totalBenefitPrograms / 12);
        let benefitProgramsPromise = [];
        for (pageX; pageX < totalPagesX + 1; pageX++) {
            benefitProgramsPromise.push(BenefitSvc.getMoreActiveCards(authToken, corpID, companyID, pageX));
        }

        Promise.all(fundingAccPromise).then(function (respArr) {
            //console.log('this is response', respArr);
            respArr.forEach(function (fundResData) {
                let fundAccDet = JSON.parse(fundResData);
                fundAccDet.fundingAccounts.forEach(function (accDetails) {
                    benefitHomePageData["fundingAccounts"].fundingAccounts.push(accDetails);
                });
            });
            req.session["fundingAccounts"] = benefitHomePageData["fundingAccounts"].fundingAccounts;

            let fundingPageData = {
                "fundingAccounts": req.session["fundingAccounts"]
            };
            benefitHomePageData["fundingAccounts"] = fundingPageData;
            fundingAccountDetails = fundingPageData;
            // ifi bankName
            let isZetaSettled = false,
                ifiIDValue;
            for (let k = 0; k < req.session.fundingAccounts.length; k++) {
                if (req.session.fundingAccounts[k].type == "ZETA_SETTLED") {
                    isZetaSettled = true;
                    ifiIDValue = req.session.fundingAccounts[k].ifiID;

                } else if (isZetaSettled == false) {
                    ifiIDValue = req.session.fundingAccounts[k].ifiID;
                }
            };

            Promise.all(benefitProgramsPromise).then(function (respArr) {
                console.log('this is response', respArr);
                respArr.forEach(function (respData) {
                    let benefirProgDet = JSON.parse(respData);
                    benefirProgDet.programs.forEach(function (progDetails) {
                        benefitProgramData["programsObj"].programs.push(progDetails);
                    });
                });
                console.log('all programs ', benefitProgramData["programsObj"].programs);
                let programs = benefitProgramData["programsObj"].programs;

                let InstaGiftStatus = 'INSTAGIFT_ENABLED';
                let compProductsInfoCheckIG = Utils.checkGKFlag(req, companyID, InstaGiftStatus);
                // let activeCards = JSON.parse(respData);
                // let programs = activeCards.programs;
                let instaProgramID, cashProgramID;
                let checkInstagift = false,
                    checkSGOrderList = false,                  
                    checkCashIncentive = false;

                for (var i = 0; i < programs.length; i++) {
                    if (programs[i].cardProgramObject.productType.toLowerCase() === "instagift") {
                        checkInstagift = true;
                        instaProgramID = programs[i].programID;
                        break;
                    }
                }
                for (var i = 0; i < programs.length; i++) {
                    if (programs[i].cardProgramObject.productType.toLowerCase() === "incentive") {
                        checkCashIncentive = true;
                        cashProgramID = programs[i].programID;
                        break;
                    }
                }
                AdminSvc.getIfisFundingAcc(authToken, corpID, ifiIDValue).then(function (getIfisData) {
                    let getIfisBankDetails = JSON.parse(getIfisData);
                    req.session[Constant.SESSION_IFIS_BANK_NAME] = getIfisBankDetails.name;


                    benefitHomePageData = {
                        "companyID": companyID,
                        "accountID": accountID,
                        "compProductsInfoCheckIG": compProductsInfoCheckIG,
                        "checkInstagift": checkInstagift,
                        "checkCashIncentive": checkCashIncentive,
                        "companyName": companyName,
                        "instaProgramID": instaProgramID,
                        "cashProgramID": cashProgramID,
                        "isRnREnabled":isRnREnabled,
                        "corpID": corpID
                    };
                    benefitHomePageData["IFIsBankName"] = getIfisBankDetails.name;
                    benefitHomePageData["ifiBankName"] = getIfisBankDetails.name;
                    benefitHomePageData["fundingAccounts"] = fundingPageData;
                    res.render('section/spotlight/home/index', benefitHomePageData);
                    res.end();
                }).catch(function (error) {
                    res.locals.showErrorWitHdr = true;
                    Util.errorFoundHandler(error, req, res, next);
                });
                //let programsCount = programs.length;


            }, function (error) {
                res.locals.showErrorWitHdr = true;
            });
        }, function (respErr) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(respErr, req, res, next);
        });

    }, function (respErr) {
        console.log("respErr", respErr);
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);

    });


};

let cashIncentive = function (req, res, next, authToken, corpID, companyID, programID, callback) {
    let displayTag = false;
    BenefitSvc.getDetails(authToken, corpID, companyID, programID).then(function (respData) {
        respData = JSONbig.parse(respData);
        respData.cardProgramObject.cardName = 'Cash Transfer';
        respData.compareDates = Util.compareLastIssuanceToCurrentDate(respData.cardProgramObject.issuanceConfiguration.lastIssuanceDate);
        req.session["compareDates"] = respData.compareDates;
        let compareTenDaysBefore = Util.compareTenDaysBefore(respData.cardProgramObject.issuanceConfiguration.lastIssuanceDate);
        if (respData.cardProgramObject.issuanceConfiguration.lastIssuanceDate && !Util.compareProductTypeToPrograms(respData.cardProgramObject.productType)) {
            var issuanceConfiguration = respData.cardProgramObject.issuanceConfiguration;
            var reimbursementProgram = respData.cardProgramObject.reimbursementProgram;
            respData.cardProgramObject.issuanceConfiguration.lastIssuanceDate = Util.timestampToDate(issuanceConfiguration.lastIssuanceDate);
            respData.cardProgramObject.reimbursementProgram.lastClaimUploadDate = Util.timestampToDate(reimbursementProgram.lastClaimUploadDate);
            respData.cardProgramObject.reimbursementProgram.lastClaimProcessDate = Util.timestampToDate(reimbursementProgram.lastClaimProcessDate);
            if (respData.cardProgramObject.closeCardConfiguration && respData.cardProgramObject.closeCardConfiguration.closeCardDate) {
                respData.cardProgramObject.closeCardConfiguration.closeCardDate = Util.timestampToDate(respData.cardProgramObject.closeCardConfiguration.closeCardDate);
            }
            if (issuanceConfiguration && issuanceConfiguration.extraIssuanceConfig) {
                respData.cardProgramObject.issuanceConfiguration.extraIssuanceConfig["corpben.reportGenerationDate"] = Util.timestampToDate(issuanceConfiguration.extraIssuanceConfig["corpben.reportGenerationDate"]);
            }
        }
        respData.cardProgramObject.voucherProgram.voucherValidity = Constant.VOUCHER_VALIDITY[respData.cardProgramObject.voucherProgram.voucherValidity];
        respData.cardProgramObject['leftColor'] = Constant.DEFAULT_LEFT_COLOR;
        respData.cardProgramObject['rightColor'] = Constant.DEFAULT_RIGHT_COLOR;
        respData.cardProgramObject.taxStatus = Constant.CARDS_TAX_STATUS[respData.cardProgramObject.productType.toLowerCase()];
        if (respData.totalPayoutsAmount) {
            if (respData.totalPayoutsAmount.amount) {
                respData.totalPayoutsAmount.amount = Util.formatINR(Math.round(respData.totalPayoutsAmount.amount / 100));
            }
        }
        if (respData.totalSpentAmount) {
            if (respData.totalSpentAmount.amount) {
                respData.totalSpentAmount.amount = Util.formatINR(Math.round(respData.totalSpentAmount.amount / 100));
            }
        }
        if (respData.reimbursementProgramDetails && respData.reimbursementProgramDetails.totalApprovedAmount) {
            if (respData.reimbursementProgramDetails.totalApprovedAmount.amount) {
                respData.reimbursementProgramDetails.totalApprovedAmount.amount = Util.formatINR(Math.round(respData.reimbursementProgramDetails.totalApprovedAmount.amount / 100));
            }
        }
        if (respData.fundingAccountDetails && respData.fundingAccountDetails.balance) {
            respData.fundingAccountDetails.balance = Util.formatINRWithPaise(respData.fundingAccountDetails.balance / 100);
        }
        if (respData.numberOfTransactions) {
            respData.numberOfTransactions = Util.formatNumber(respData.numberOfTransactions);
        }

        if (compareTenDaysBefore) {
            displayTag = true;
        }
        respData.displayTag = displayTag;
        respData['companyName'] = res.locals.selectedCompany.company.companyName;
        req.session['benefitDetails'] = respData;
        callback(respData);
    }, function (respErr) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);
    });

};

let getCashIncentiveDetails = function (req, res, next) {
    res.locals.pageName = 'spotlight';
    res.locals.currPage = 'spotlightDetails';
    let programID = req.params.cardProgramId;
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;

    setSelectedCompany(req, res);
    cashIncentive(req, res, next, authToken, corpID, companyID, programID, function (respData) {
        res.render('section/spotlight/cashincentive/programDetails', respData);
        res.end();
    });
};

let getnewSelectGifting = function (req, res, next) {
    res.locals.pageName = 'spotlight';
    res.locals.currPage = 'spotlightDetails';
    res.locals.companyID = req.params.companyId;
    let corporateId = res.locals.corp.id;
    let companyId = res.locals.companyID;
    setSelectedCompany(req, res);
    let token = req.session[Constant.SESSION_AUTH].authToken;
    SpotlightSvc.getSelectGiftingOrderList(token, corporateId, companyId).then(function (respData) {
            let sgOrderList = respData;
            if (sgOrderList.orders.length > 0) {
                res.render('section/spotlight/selectgifting/listSelectGiftOrder', sgOrderList);
                res.end();
            } else {
                res.render('section/spotlight/selectgifting/newselectgifting', {});
                res.end();

            }
        })
        .catch(function (error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });
};

let getSelectGiftingDetails = function (req, res, next) {
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken;
    res.locals.pageName = 'spotlight';
    res.locals.companyID = req.params.companyId;
    res.locals.currPage = 'spotlightDetails';
    res.locals.accountID = req.session.acc_info.account.accountDetails.id;
    setSelectedCompany(req, res);
    authToken = req.session[Constant.SESSION_AUTH].authToken;
    SpotlightSvc.getSelectGiftingDetails(authToken, corpID, companyID).then(function (responseData) {
        let respData = JSON.parse(responseData);
        respData.corpID = corpID;
        console.log(respData);
        let fundingPageData = {
            "fundingAccounts": req.session["fundingAccounts"]
        };
        respData.fundingAccountDetails = fundingPageData;
        let brandCategory = [];
        let unique_array = [];
        for (var i = 0; i < respData.products.length; i++) {
            respData.products[i].brandName = Util.htmlDecoder(respData.products[i].brandName);
            respData.products[i].productCategory = Util.htmlDecoder(respData.products[i].productCategory);
            if (respData.products[i].isActive == true) {
                brandCategory.push(respData.products[i].productCategory);
            }
        }

        for (let j = 0; j < brandCategory.length; j++) {
            if (unique_array.indexOf(brandCategory[j]) == -1) {
                unique_array.push(brandCategory[j]);
            }
        }
        respData.uniqueArray = unique_array;

        SpotlightSvc.getFileUploadDetails(authToken, corpID).then(function (fileResData) {
            let fileUploadUrl = "https://optima-order-files-temp-stage.s3.amazonaws.com/";
            respData.fileUploadDetails = fileResData;
            respData.fileUploadUrl = fileUploadUrl;
            res.render('section/spotlight/selectgifting/newgiftorder', respData);
            res.end();
        }, function (respErr) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(respErr, req, res, next);
        });

    }, function (respErr) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);
    });
};

let getRewardDetails = function (req, res, next) {
    res.locals.pageName = 'spotlight';
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken;
    let rewardData = [];
    let cardProgramType = "reward";
    let templateType = [];
    let benefitHomePageData = {};
    let totalResp = [];
    res.locals.companyID = req.params.companyId;
    // res.locals.currPage = 'rewardDetails';
    res.locals.accountID = req.session.acc_info.account.accountDetails.id;
    setSelectedCompany(req, res);
    authToken = req.session[Constant.SESSION_AUTH].authToken;
    BenefitSvc.getMoreActiveCards(authToken, corpID, companyID, 0).then(function (respData) {
        let benefitProgramData = {};
        let totalResp = []
        benefitProgramData["programsObj"] = JSON.parse(respData);
        let totalBenefitPrograms = benefitProgramData["programsObj"].totalRequestCount;
        let pageNum = 2;
        let totalPages = Math.ceil(totalBenefitPrograms / 10);
        let benefitProgramsPromise = [];
        for (pageNum; pageNum < totalPages + 1; pageNum++) {
            benefitProgramsPromise.push(BenefitSvc.getMoreActiveCards(authToken, corpID, companyID, pageNum - 1));
        }
        Promise.all(benefitProgramsPromise).then(function (respArr) {
            console.log('this is response', respArr);
            respArr.forEach(function (respData) {
                let benefirProgDet = JSON.parse(respData);
                benefirProgDet.programs.forEach(function (progDetails) {
                    benefitProgramData["programsObj"].programs.push(progDetails);
                });
            });
            console.log('all programs ', benefitProgramData["programsObj"].programs);
            let programs = benefitProgramData["programsObj"].programs;

            //let activeCards = JSON.parse(respData);
            //programs = activeCards.programs;

            const promisesList2 = [
                //BenefitSvc.getActiveCards(authToken, corpID, companyID),
                SpotlightSvc.getTemplateDesign(authToken, corpID, companyID, cardProgramType),
                AdminSvc.getFundingAcc(authToken, corpID, 1, 10),
                BenefitSvc.getFileUploadDetails(authToken, corpID)
            ];

            Promise.all(promisesList2).then(respData => {
                //activeCards
                //let activeCards = JSON.parse(respData[0]);
                //programs = activeCards.programs;
                programs.map(card => {
                    if (card.cardProgramObject.productType.toLowerCase() === "reward") {
                        card.cardProgramObject.programID = card.programID;
                        card.cardProgramObject.fundingAccountDetails = card.fundingAccountDetails;
                    }
                })
                for (var i = 0; i < programs.length; i++) {
                    if (programs[i].corpProductType === 'SPOTLIGHT' && programs[i].cardProgramObject.productType.toLowerCase() === "reward") {
                        rewardData.push(programs[i].cardProgramObject);
                    }
                }
                //template
                let newtemplateType = JSON.parse(respData[0]);
                templateType = newtemplateType.cardProgramTemplateList;
                //fileupload
                let fileUploadUrl = JSON.parse(respData[2]).url;

                //fundingAccount
                let fundResData = respData[1];
                benefitHomePageData["fundingAccounts"] = JSON.parse(respData[1]);
                let totalFundCount = benefitHomePageData["fundingAccounts"].count;
                let pageNum = 2;
                let totalPages = Math.ceil(totalFundCount / 10);
                let fundingAccPromise = [];
                for (pageNum; pageNum < totalPages + 1; pageNum++) {
                    fundingAccPromise.push(AdminSvc.getFundingAcc(authToken, corpID, pageNum, 10));
                }
                Promise.all(fundingAccPromise).then(function (respArr) {
                    //console.log('this is response', respArr);
                    respArr.forEach(function (fundResData) {
                        let fundAccDet = JSON.parse(fundResData);
                        fundAccDet.fundingAccounts.forEach(function (accDetails) {
                            benefitHomePageData["fundingAccounts"].fundingAccounts.push(accDetails);
                        });
                    });
                    req.session["fundingAccounts"] = benefitHomePageData["fundingAccounts"].fundingAccounts;

                    let fundingPageData = {
                        "fundingAccounts": req.session["fundingAccounts"]
                    };
                    benefitHomePageData["fundingAccounts"] = fundingPageData;
                    fundingAccountDetails = fundingPageData;

                    let benefitRewardData = {
                        "rewardData": rewardData,
                        "templateType": templateType,
                        "fileUploadDetails": respData[2],
                        "fileUploadUrl": fileUploadUrl,
                        "fundingAccountDetails": fundingAccountDetails,
                    };
                    console.log(benefitRewardData);
                    res.render('section/spotlight/reward/index', benefitRewardData);
                    res.end();
                }, function (respErr) {
                    res.locals.showErrorWitHdr = true;
                    Util.errorFoundHandler(respErr, req, res, next);
                });

            }, function (respErr) {
                console.log("respErr", respErr);
                res.locals.showErrorWitHdr = true;
                Util.errorFoundHandler(respErr, req, res, next);

            });
        })
    })
}

let getVirtualDetails = function (req, res, next) {
    res.locals.pageName = 'spotlight';
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken;
    let rewardData = [];
    let cardProgramType = "virtual";
    let benefitHomePageData = {};
    let totalResp = [];
    res.locals.companyID = req.params.companyId;
    res.locals.currPage = 'virtual';
    //var ifiBankName = req.session[Constant.SESSION_IFIS_BANK_NAME];
    //res.locals.accountID = req.session.acc_info.account.accountDetails.id;
    setSelectedCompany(req, res);
    authToken = req.session[Constant.SESSION_AUTH].authToken;

    const promisesListVirtual = [
        SpotlightSvc.getVirtualTemplate(authToken, corpID, companyID),
        AdminSvc.getFundingAcc(authToken, corpID, 1, 10),
        //BenefitSvc.getFileUploadDetails(authToken, corpID),
        SpotlightSvc.getFileUploadDetails(authToken, corpID)
    ];

    Promise.all(promisesListVirtual).then(respData => {
        //template
        console.log(respData);
        let newtemplateVirtual = JSON.parse(respData[0]).templates;
        //fileupload
        let fileUploadUrl = JSON.parse(respData[2]).url;
        //fundingAccount
        let fundResData = respData[1];
        benefitHomePageData["fundingAccounts"] = JSON.parse(respData[1]);
        let totalFundCount = benefitHomePageData["fundingAccounts"].count;
        let pageNum = 2;
        let totalPages = Math.ceil(totalFundCount / 10);
        let fundingAccPromise = [];
        for (pageNum; pageNum < totalPages + 1; pageNum++) {
            fundingAccPromise.push(AdminSvc.getFundingAcc(authToken, corpID, pageNum, 10));
        }
        Promise.all(fundingAccPromise).then(function (respArr) {
            //console.log('this is response', respArr);
            respArr.forEach(function (fundResData) {
                let fundAccDet = JSON.parse(fundResData);
                fundAccDet.fundingAccounts.forEach(function (accDetails) {
                    benefitHomePageData["fundingAccounts"].fundingAccounts.push(accDetails);
                });
            });
            req.session["fundingAccounts"] = benefitHomePageData["fundingAccounts"].fundingAccounts;

            let fundingPageData = {
                "fundingAccounts": req.session["fundingAccounts"]
            };
            benefitHomePageData["fundingAccounts"] = fundingPageData;
            fundingAccountDetails = fundingPageData;

            let benefitVirtualData = {
                "newtemplateVirtual": newtemplateVirtual,
                "fileUploadDetails": respData[2],
                "fileUploadUrl": fileUploadUrl,
                "fundingAccountDetails": fundingAccountDetails,
                corpID: corpID
            };
            console.log(benefitVirtualData);
            res.render('section/spotlight/virtual/index', benefitVirtualData);
            res.end();
        }, function (respErr) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(respErr, req, res, next);
        });

    }, function (respErr) {
        console.log("respErr", respErr);
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);

    });

};

let getPhysicalDetails = function (req, res, next) {

    res.locals.pageName = 'spotlight';
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken;
    let cardProgramType = "physical";
    res.locals.companyID = req.params.companyId;
    res.locals.currPage = 'physical';
    //res.locals.accountID = req.session.acc_info.account.accountDetails.id;
    setSelectedCompany(req, res);
    authToken = req.session[Constant.SESSION_AUTH].authToken;
    let respData;
    res.render('section/spotlight/physical/index', respData);
    res.end();
}

let getPointBased = function (req, res, next) {

    res.locals.pageName = 'spotlight';
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken;
    let cardProgramType = "pointbased";
    res.locals.companyID = req.params.companyId;
    res.locals.currPage = 'pointbased';
    //res.locals.accountID = req.session.acc_info.account.accountDetails.id;
    setSelectedCompany(req, res);
    authToken = req.session[Constant.SESSION_AUTH].authToken;
    const promisesListPhysical = [
        SpotlightSvc.listProgramDetails(authToken, companyID, corpID)
    ];
    Promise.all(promisesListPhysical).then(resp => {        
        let respData = JSON.parse(resp);        
        let respData1 = respData.programDetail.map(d => {
            d.name = Util.htmlDecoder(d.name);
            return d;
        });
        respData.programDetail = respData1;
        res.render('section/spotlight/pointbased/index',respData);
        res.end();
    }).catch(function (error) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(error, req, res, next);
    });

}

let getPointBasedProgram = function (req, res, next) {

    res.locals.pageName = 'spotlight';
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken;
    let cardProgramType = "pointbased";
    res.locals.companyID = req.params.companyId;
    res.locals.currPage = 'pointbased';
    //res.locals.accountID = req.session.acc_info.account.accountDetails.id;
    setSelectedCompany(req, res);
    authToken = req.session[Constant.SESSION_AUTH].authToken;
    let respData;
    res.render('section/spotlight/pointbased/employee/index', respData);
    res.end();
}


let getPointBasedProgramDetails = function (req, res, next) {

    res.locals.pageName = 'spotlight';
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken;
    let cardProgramType = "pointbased";
    res.locals.companyID = req.params.companyId;
    res.locals.currPage = 'pointbased';
    //res.locals.accountID = req.session.acc_info.account.accountDetails.id;
    setSelectedCompany(req, res);
    authToken = req.session[Constant.SESSION_AUTH].authToken;
    let respData = { 
        programName:req.params.programName,
        programID:req.params.programID,
        corpID: res.locals.corp.id
    };
    res.render('section/spotlight/pointbased/program-details', respData);
    res.end();
}

let uploadPointBasedBeneficary = function (req, res, next) {

    res.locals.pageName = 'spotlight';
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let cardProgramType = "pointbased";
    let benefitPointbasedData = {};
    res.locals.companyID = req.params.companyId;
    res.locals.currPage = 'pointbased';
    setSelectedCompany(req, res);
    let authToken = req.session[Constant.SESSION_AUTH].authToken;

    const promisesListPhysical = [
        //AdminSvc.getFundingAcc(authToken, corpID, 1, 10),
        SpotlightSvc.getFileUploadDetails(authToken, corpID)
    ];
    Promise.all(promisesListPhysical).then(respData => {
        //fileupload
        let fileUploadUrl = JSON.parse(respData[0]).postAction;
        let benefitPointbasedData = {
            "fileUploadDetails": respData[0],
            "fileUploadUrl": fileUploadUrl,
            "programName":req.params.programName,
            "programID":req.params.programID,
        };
        console.log(benefitPointbasedData);
        res.render('section/spotlight/pointbased/beneficiary/index', benefitPointbasedData);
        res.end();

    }, function (respErr) {
        console.log("respErr", respErr);
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);

    });

}

let uploadPointBasedissue = function (req, res, next) {
   
    res.locals.pageName = 'spotlight';
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let cardProgramType = "pointbased";
    let benefitPointbasedData = {};
    res.locals.companyID = req.params.companyId;
    res.locals.currPage = 'pointbased';
    setSelectedCompany(req, res);
    let authToken = req.session[Constant.SESSION_AUTH].authToken;           
    const promise = [        
        SpotlightSvc.getFileUploadDetails(authToken, corpID),
        AdminSvc.getFundingAcc(authToken, corpID, 1, 10),
    ];
    Promise.all(promise).then(respData => {                
        let fundingAccounts = [];
        let fundResData = JSON.parse(respData[1]);   
        fundingAccounts = [...fundResData.fundingAccounts]; 
        let totalFundCount = fundResData.count;
        let pageNum = 2;
        let totalPages = Math.ceil(totalFundCount / 10);
        let fundingAccPromise = [];
        for (pageNum; pageNum < totalPages + 1; pageNum++) {
            fundingAccPromise.push(AdminSvc.getFundingAcc(authToken, corpID, pageNum, 10));
        }
                Promise.all(fundingAccPromise).then(function (respArr) {            
                    respArr.forEach(function (fundResData) {
                        let fundAccDet = JSON.parse(fundResData);
                        fundAccDet.fundingAccounts.forEach(function (accDetails) {
                            fundingAccounts.push(accDetails);
                        });
                    });            
                    // req.session["fundingAccounts"] = fundingAccounts;   
                    let fileUploadUrl = JSON.parse(respData[0]).postAction;
                    let benefitPointbasedData = {
                        "fileUploadDetails": respData[0],
                        "fileUploadUrl": fileUploadUrl,        
                        "programName":req.params.programName,
                        "programID":req.params.programID,
                        "fundingAccountDetails":fundingAccounts,
                        "corpID": corpID
                    };                
                    console.log(benefitPointbasedData);
                    res.render('section/spotlight/pointbased/issuepoints/index', benefitPointbasedData);
                    res.end();
            }, function (respErr) {
                    console.log("respErr", respErr);
                    res.locals.showErrorWitHdr = true;
                    Util.errorFoundHandler(respErr, req, res, next);
            });
    });
}

let pointBasedSinglePayoutDetails = function (req, res, next) {
    // :programID/:fileUploadOrderID/:updatedAt/:totalBeneficiaries/:orderAmount/orders
    res.locals.pageName = 'spotlight';
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let cardProgramType = "pointbased";
    let benefitPointbasedData = {};
    res.locals.companyID = req.params.companyId;
    res.locals.currPage = 'pointbased';
    setSelectedCompany(req, res);
        let requireDetails = {  
            "programName":req.params.programName,
            "programID":req.params.programID,
            "orderID": req.params.fileUploadOrderID,
            "orderStatus": req.params.status,
            "totalPayouts": req.params.totalBeneficiaries,
            "amount": req.params.orderAmount,
            "scheduledDate":req.params.updatedAt
        };
        console.log(benefitPointbasedData);
        res.render('section/spotlight/pointbased/individual-orders', requireDetails);
        res.end();

}



let orderPhysicalCard = function (req, res, next) {

    res.locals.pageName = 'spotlight';
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken;
    let cardProgramType = "physical";
    res.locals.companyID = req.params.companyId;
    res.locals.currPage = 'physical';
    //res.locals.accountID = req.session.acc_info.account.accountDetails.id;
    setSelectedCompany(req, res);
    authToken = req.session[Constant.SESSION_AUTH].authToken;
    let respData;
    res.render('section/spotlight/physical/order-card/index', respData);
    res.end();
}

let payOutPhysicalCard = function (req, res, next) {
    res.locals.pageName = 'spotlight';
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let cardProgramType = "physical";
    let benefitHomePageData = {};
    res.locals.companyID = req.params.companyId;
    res.locals.currPage = 'physical';
    let data = {
        fundingAccountDetails: req.session.fundingAccounts
    };
    setSelectedCompany(req, res);
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    res.render('section/spotlight/physical/payout/index', data);
    res.end();

}

let issuePhysicalCard = function (req, res, next) {

    res.locals.pageName = 'spotlight';
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let cardProgramType = "physical";
    let benefitHomePageData = {};
    res.locals.companyID = req.params.companyId;
    res.locals.currPage = 'physical';
    setSelectedCompany(req, res);
    let authToken = req.session[Constant.SESSION_AUTH].authToken;

    const promisesListPhysical = [
        AdminSvc.getFundingAcc(authToken, corpID, 1, 10),
        SpotlightSvc.getFileUploadDetails(authToken, corpID)
    ];

    Promise.all(promisesListPhysical).then(respData => {

        //fileupload
        let fileUploadUrl = JSON.parse(respData[1]).postAction;
        //fundingAccount
        let fundResData = JSON.parse(respData[0]);
        benefitHomePageData["fundingAccounts"] = JSON.parse(respData[0]);
        let totalFundCount = benefitHomePageData["fundingAccounts"].count;
        let pageNum = 2;
        let totalPages = Math.ceil(totalFundCount / 10);
        let fundingAccPromise = [];
        for (pageNum; pageNum < totalPages + 1; pageNum++) {
            fundingAccPromise.push(AdminSvc.getFundingAcc(authToken, corpID, pageNum, 10));
        }
        Promise.all(fundingAccPromise).then(function (respArr) {
            respArr.forEach(function (fundResData) {
                let fundAccDet = JSON.parse(fundResData);
                fundAccDet.fundingAccounts.forEach(function (accDetails) {
                    benefitHomePageData["fundingAccounts"].fundingAccounts.push(accDetails);
                });
            });
            req.session["fundingAccounts"] = benefitHomePageData["fundingAccounts"].fundingAccounts;

            let fundingPageData = {
                "fundingAccounts": req.session["fundingAccounts"]
            };
            benefitHomePageData["fundingAccounts"] = fundingPageData;
            fundingAccountDetails = fundingPageData;

            let benefitPhysicalData = {
                "fileUploadDetails": respData[1],
                "fileUploadUrl": fileUploadUrl,
                "fundingAccountDetails": fundingAccountDetails,
                corpID: corpID
            };
            console.log(benefitPhysicalData);
            res.render('section/spotlight/physical/issue-gift/index', benefitPhysicalData);
            res.end();
        }, function (respErr) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(respErr, req, res, next);
        });

    }, function (respErr) {
        console.log("respErr", respErr);
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);

    });

}

let fetchingGiftingDetails = function (req, res, next) {
    let respData;
    res.render('section/spotlight/selectgifting/confirmation-page', respData);
    res.end();
};

let getSpotlightDetails = function (req, res, next) {
    let corpID = res.locals.corp.id;
    let programID = req.params.cardProgramId;
    let companyID = req.params.companyId;
    let authToken;
    res.locals.pageName = 'spotlight';
    res.locals.companyID = req.params.companyId;
    res.locals.programID = req.params.cardProgramId;
    res.locals.currPage = 'spotlightDetails';
    res.locals.accountID = req.session.acc_info.account.accountDetails.id;
    setSelectedCompany(req, res);
    authToken = req.session[Constant.SESSION_AUTH].authToken;
    BenefitSvc.getDetails(authToken, corpID, companyID, programID).then(function (respData) {
        respData = JSON.parse(respData);
        if (respData.totalPayoutsAmount && respData.totalPayoutsAmount.amount) {
            respData.totalPayoutsAmount.amount = Util.formatINR(Math.round(respData.totalPayoutsAmount.amount / 100));
        }
        if (respData.totalSpentAmount && respData.totalSpentAmount.amount) {
            respData.totalSpentAmount.amount = Util.formatINR(Math.round(respData.totalSpentAmount.amount / 100));
        }
        if (respData.fundingAccountDetails && respData.fundingAccountDetails.balance) {
            respData.fundingAccountDetails.rsBalance = Math.round(respData.fundingAccountDetails.balance / 100);
            respData.fundingAccountDetails.balance = Util.formatINR(Math.round(respData.fundingAccountDetails.balance / 100));
        }
        if (respData.numberOfTransactions) {
            respData.numberOfTransactions = Util.formatNumber(respData.numberOfTransactions);
        }
        req.session['instagift'] = respData;
        res.render('section/spotlight/instagift/index', respData);
        res.end();
    }, function (respErr) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);
    });
};

let getNewTransfer = function (req, res, next) {
    res.locals.pageName = 'spotlight';
    let programID = req.params.cardProgramId;
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    res.locals.currPage = 'newTransfer';
    cashIncentive(req, res, next, authToken, corpID, companyID, programID, function (respData) {
        res.locals.benefitDetails = req.session.benefitDetails;
        let companyID = req.params.companyId;
        setSelectedCompany(req, res);
        let productType = req.session.benefitDetails.cardProgramObject.productType.toLowerCase();
        productType = Constant.PROGRAM_TRANSFER_ENUM[productType] || productType;
        //let corpID = req.session[Constant.SESSION_CORP_INFO].id;
        BenefitSvc.getFileUploadDetails(authToken, corpID).then(function (respData) {
            let fileUploadUrl = JSON.parse(respData).postAction;
            let pageData = {
                productType: productType,
                previewImage: Constant.TRANSFER_PREVIEW_IMAGES[productType],
                excelFileDownload: Constant.TRANSFER_EXCEL_DOWNLOAD_FILE[productType],
                companyID: companyID,
                fileUploadDetails: respData,
                fileUploadUrl: fileUploadUrl
            };
            res.render('section/spotlight/cashincentive/newTransfer', pageData);
            res.end();
        }, function (respErr) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(respErr, req, res, next);
        });
    });
};

let getTransfers = function (req, res, next) {
    res.locals.pageName = 'spotlight';
    res.locals.currPage = 'newTransfer';
    res.locals.benefitDetails = req.session.benefitDetails;
    let currMonth = Util.timestampToMonthYear(parseInt(req.query.startDate));
    let benefitDetails = res.locals.benefitDetails;
    let companyID = req.params.companyId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    setSelectedCompany(req, res);
    let benefitHomePageData = {
        "benefitDetails": benefitDetails,
        "currMonth": currMonth,
        "companyID": companyID
    };
    res.render('cashIncentiveTransferDetail', benefitHomePageData);
    res.end();
};

let getOrderTransfers = function (req, res, next) {
    let orderID = req.params["orderId"];
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let apiKey = req.session[Constant.SESSION_AUTH].authToken;
    setSelectedCompany(req, res);
    BenefitSvc.getTransferDetails(apiKey, companyID, orderID, corpID).then(function (respData) {
            respData = JSON.parse(respData);
            let createdDate = respData.order.createdOn.split('T');
            let createdDateArr = createdDate[0].split('-');
            res.locals.pageName = 'spotlight';
            res.locals.currPage = 'newTransfer';
            res.locals.benefitDetails = req.session.benefitDetails;
            respData.order.createdOn = Util.dateFormat(parseInt(createdDateArr[2]), parseInt(createdDateArr[1]), parseInt(createdDateArr[0]));
            respData.order["benefitType"] = Constant.CARD_PROGRAM_TYPES[respData.order.type];
            respData.order.statusLabel = Constant.ORDER_STATUS_LABELS[respData.order.status];
            respData.order.statusTooltip = Constant.ORDER_STATUS_TOOLTIPS[respData.order.status];
            respData.order.fileUploadedLink = BenefitSvc.getUploadedFile(apiKey, orderID);
            respData.order.proformaLink = BenefitSvc.getProformaFile(apiKey, orderID);
            respData.payoutStatusLabels = Constant.PAYOUT_STATUS_LABELS
            respData.companyID = companyID;
            respData.compareDates = req.session.compareDates;
            res.render('section/spotlight/cashincentive/transferDetail', respData);
            res.end();
        })
        .catch(function(error) {
            console.log(error);
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });
};

let getOrderErrors = function (req, res, next) {
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let orderID = req.params.orderId;
    let programID = req.params.cardProgramId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let downloadURL = (Config.corpBaseUrl + Urls['DOWNLOAD_PAYOUT_REPORT']).replace(':authToken:', authToken).replace(':corpID:', corpID).replace(':orderID:', orderID);
    let pageData = {
        companyID: companyID,
        orderID: orderID,
        programID: programID,
        downloadURL: downloadURL
    };
    res.locals.pageName = 'spotlight';
    res.locals.currPage = 'newTransfer';
    res.locals.benefitDetails = req.session.benefitDetails;
    setSelectedCompany(req, res);
    res.render('section/spotlight/cashincentive/newTransferErrors', pageData);
    res.end();
};

let getInstagiftFaqs = function (req, res, next) {
    res.locals.pageName = 'spotlight';
    res.locals.currPage = 'InstagiftFaq';
    res.locals.benefitDetails = req.session.instagift;
    res.locals.programID = req.params.cardProgramId;
    res.locals.companyID = req.params.companyId;
    setSelectedCompany(req, res);
    res.render('section/spotlight/instagift/faq', {});
    res.end();
};

let getInstagiftControlCenter = function (req, res, next) {
    res.locals.pageName = 'spotlight';
    res.locals.currPage = 'InstagiftControlCenter';
    res.locals.benefitDetails = req.session.instagift;
    res.locals.programID = req.params.cardProgramId;
    res.locals.companyID = req.params.companyId;
    setSelectedCompany(req, res);
    res.render('section/spotlight/instagift/control-center', {});
    res.end();
};

let getPayoutOrders = function (req, res, next) {
    res.locals.pageName = 'spotlight';
    res.locals.currPage = 'transferDetails';
    res.locals.companyID = req.params.companyId;
    res.locals.programID = req.params.cardProgramId;
    let orderID = req.params.orderId;
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let pageData = {};
    res.locals.benefitDetails = req.session.instagift;
    setSelectedCompany(req, res);
    SpotlightSvc.getPayoutsOrders(authToken, corpID, companyID, orderID).then(function (respData) {
            respData = JSON.parse(respData);
            let refName = respData.payouts[0].refName;
            let date = Util.epochToIstDate(respData.payouts[0].createdOn);
            pageData = {
                orderID: orderID,
                companyID: companyID,
                refName: refName,
                date: date
            };
            res.render('section/spotlight/instagift/transferDetail', pageData);
            res.end();
        })
        .catch(function (error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });
};

let loadCards = function (req, res, next) {
    res.locals.pageName = 'spotlight';
    res.locals.currPage = 'newTransfer';
    let companyID = req.params.companyId;
    let corpID = res.locals.corp.id;
    let programID = req.params.cardProgramId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    BenefitSvc.getDetails(authToken, corpID, companyID, programID).then(function (respData) {
        respData = JSON.parse(respData);
        if (respData.totalPayoutsAmount && respData.totalPayoutsAmount.amount) {
            respData.totalPayoutsAmount.amount = Util.formatINR(Math.round(respData.totalPayoutsAmount.amount / 100));
        }
        if (respData.totalSpentAmount && respData.totalSpentAmount.amount) {
            respData.totalSpentAmount.amount = Util.formatINR(Math.round(respData.totalSpentAmount.amount / 100));
        }
        if (respData.fundingAccountDetails && respData.fundingAccountDetails.balance) {
            respData.fundingAccountDetails.rsBalance = Math.round(respData.fundingAccountDetails.balance / 100);
            respData.fundingAccountDetails.balance = Util.formatINR(Math.round(respData.fundingAccountDetails.balance / 100));
        }
        if (respData.numberOfTransactions) {
            respData.numberOfTransactions = Util.formatNumber(respData.numberOfTransactions);
        }
        req.session['instagift'] = respData;
        let instagiftDetail = req.session.instagift;
        let pageData = {};
        let productType = 'instagift';
        res.locals.programID = req.params.cardProgramId;
        res.locals.benefitDetails = req.session.instagift;
        let fundingAccountID = instagiftDetail.fundingAccountDetails.accountID.toString();
        res.locals.funAccountLastDigit = fundingAccountID.substr(fundingAccountID.length - 4);
        setSelectedCompany(req, res);
        pageData = {
            productType: productType,
            companyID: companyID
        };
        res.render('section/spotlight/instagift/load-cards/index', pageData);
        res.end();
    }, function (respErr) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);
    });

};

let setSelectedCompany = function (req, res) {
    let companyID = req.params.companyId;
    let companiesInfo = req.session[Constant.SESSION_COMP_INFO];
    req.session['selectedProgram'] = "optima";
    for (var i = 0; i < companiesInfo.length; i++) {
        if (companyID == companiesInfo[i].company.companyID) {
            req.session[Constant.SESSION_SELECTED_COMP_INFO] = companiesInfo[i];
            res.locals.selectedCompany = companiesInfo[i];
        }
    }
};
let getnewInvoice = function (req, res, next) {
    let companyId = req.params.companyId;
    let orderId = req.params.orderId;
    let token = req.session[Constant.SESSION_AUTH].authToken;
    let corporateId = res.locals.corp.id;
    SpotlightSvc.getSelectGiftingInvoice(token, companyId, corporateId, orderId).then(function (respData) {
            respData = JSON.parse(respData);
            console.log(respData);
            let addbr = respData.zetaAddress;
            addbr = addbr.replace(/&lt;/gi, '<').replace(/&gt;/gi, '>');
            respData.zetaAddress = addbr;
            respData.invoiceCreationDate = Util.timestampToDate(respData.invoiceCreationDate);
            respData.creationDate = Util.timestampToDate(respData.creationDate);
            if (respData.ifiName.toLowerCase() == "kotak") {
                res.render('section/spotlight/selectgifting/csinvoice', respData);
            } else if (respData.ifiName.toLowerCase() == "hdfc") {
                res.render('section/spotlight/virtual/invoices/hdfcinvoice', respData);
            } else {
                res.render('section/spotlight/selectgifting/invoice', respData);
            }
            res.end();
        })
        .catch(function (error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });

};
let getnewPInvoice = function (req, res, next) {
    let companyId = req.params.companyId;
    let orderId = req.params.orderId;
    let token = req.session[Constant.SESSION_AUTH].authToken;
    let corporateId = res.locals.corp.id;
    SpotlightSvc.getSelectGiftingPInvoice(token, companyId, corporateId, orderId).then(function (respData) {
            respData = JSON.parse(respData);
            respData.zetaAddress = Util.htmlDecoder(respData.zetaAddress);
            respData.creationDate = Util.timestampToDate(respData.creationDate);
            respData.invoiceCreationDate = Util.timestampToDate(respData.invoiceCreationDate);
            respData.orderTypeText = Util.htmlDecoder(respData.orderTypeText)
            if (respData.ifiName.toLowerCase() == "kotak") {
                res.render('section/spotlight/selectgifting/csproforma-invoice', respData);
            } else if (respData.ifiName.toLowerCase() == "hdfc") {
                res.render('section/spotlight/selectgifting/hdfcproforma-invoice', respData);
            } else {
                res.render('section/spotlight/selectgifting/proforma-invoice', respData);
            }
            res.end();
        })
        .catch(function (error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });

};

let getSingleTransferDetails = function (req, res, next) {
    res.locals.pageName = 'spotlight';
    let respData = {}
    res.render('section/spotlight/reward/single-order-details', respData)
}


let getOrderErrorFound = function (req, res, next) {
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let orderID = req.params.orderId;
    let programID = req.params.cardProgramId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let downloadURL = (Config.corpBaseUrl + Urls['DOWNLOAD_PAYOUT_REPORT']).replace(':authToken:', authToken).replace(':corpID:', corpID).replace(':orderID:', orderID);
    let pageData = {
        companyID: companyID,
        orderID: orderID,
        programID: programID,
        downloadURL: downloadURL
    };
    res.locals.pageName = 'benefits';
    res.locals.currPage = 'newTransfer';
    res.locals.benefitDetails = req.session.benefitDetails;
    setSelectedCompany(req, res);
    res.render('section/spotlight/reward/get-order-error-found', pageData);
    res.end();
}

let getVirtualIndividualOrderDetails = function (req, res, next) {
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let orderID = req.params.orderID;
    let programID = req.params.cardProgramId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let amount = req.params.amount
    let orderStatus = req.params.orderStatus
    let totalPayouts = req.params.totalPayouts;
    orderStatus = orderStatus.charAt(0).toUpperCase() + orderStatus.slice(1).toLowerCase();
    res.locals.pageName = 'spotlight';
    let pageData = {
        orderID: orderID,
        companyID: companyID,
        amount: amount,
        orderStatus: orderStatus,
        totalPayouts: totalPayouts,
        corpID: corpID
    }
    res.render('section/spotlight/virtual/individual-orders', pageData);
    res.end();
}

let getPhysicalIndividualOrderDetails = function (req, res, next) {
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let orderID = req.params.orderID;
    let programID = req.params.cardProgramId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let amount = req.params.amount
    let orderStatus = req.params.orderStatus
    let totalPayouts = req.params.totalPayouts;
    orderStatus = orderStatus.charAt(0).toUpperCase() + orderStatus.slice(1).toLowerCase();
    res.locals.pageName = 'spotlight';
    let pageData = {
        orderID: orderID,
        companyID: companyID,
        amount: amount,
        orderStatus: orderStatus,
        totalPayouts: totalPayouts,
        corpID: corpID
    }
    res.render('section/spotlight/physical/individual-orders', pageData);
    res.end();
}


let getVirtualGiftInvoice = function (req, res, next) {
    let companyId = req.params.companyId;
    let orderId = req.params.orderID;
    let token = req.session[Constant.SESSION_AUTH].authToken;
    let corporateId = res.locals.corp.id;

    SpotlightSvc.getVirtualGiftingInvoice(token, companyId, corporateId, orderId).then(function (respData) {
            respData = JSON.parse(respData);
            console.log(respData);
            let addbr = respData.zetaAddress;
            addbr = addbr.replace(/&lt;/gi, '<').replace(/&gt;/gi, '>');
            respData.zetaAddress = addbr;
            respData.invoiceCreationDate = Util.timestampToDate(respData.invoiceCreationDate);
            respData.creationDate = Util.timestampToDate(respData.creationDate);
            if (respData.ifiName.toLowerCase() == "kotak") {
                res.render('section/spotlight/virtual/invoices/csinvoice', respData);
            } else if (respData.ifiName.toLowerCase() == "hdfc") {
                res.render('section/spotlight/virtual/invoices/hdfcinvoice', respData);
            } else {
                res.render('section/spotlight/virtual/invoices/invoice', respData);
            }
            res.end();
        })
        .catch(function (error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });

};

let getVirtualGiftProformaInvoice = function (req, res, next) {
    let companyId = req.params.companyId;
    let orderId = req.params.orderId;
    let token = req.session[Constant.SESSION_AUTH].authToken;
    let corporateId = res.locals.corp.id;



    SpotlightSvc.getVirtualGiftingProformaInvoice(token, companyId, corporateId, orderId).then(function (respData) {
            respData = JSON.parse(respData);
            console.log(respData);
            let addbr = respData.zetaAddress;
            addbr = addbr.replace(/&lt;/gi, '<').replace(/&gt;/gi, '>');
            respData.zetaAddress = addbr;
            respData.invoiceCreationDate = Util.timestampToDate(respData.invoiceCreationDate);
            respData.creationDate = Util.timestampToDate(respData.creationDate);
            if (respData.ifiName.toLowerCase() == "kotak") {
                res.render('section/spotlight/virtual/invoices/csproforma-invoice', respData);
            } else if (respData.ifiName.toLowerCase() == "hdfc") {
                res.render('section/spotlight/virtual/invoices/hdfcproforma-invoice', respData);
            } else {
                res.render('section/spotlight/virtual/invoices/proforma-invoice', respData);
            }
            res.end();
        })
        .catch(function (error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });
}

let getPhysicalCardGiftProformaInvoice = function(req, res, next) {
    let companyId = req.params.companyId;
    let orderId = req.params.orderId;
    let token = req.session[Constant.SESSION_AUTH].authToken;
    let corporateId = res.locals.corp.id;



SpotlightSvc.getPhysicalCardProformaInvoice(token, companyId, corporateId, orderId).then(function(respData) {
    respData = JSON.parse(respData);
    console.log(respData);
    let addbr = respData.zetaAddress;
    addbr= addbr.replace(/&lt;/gi,'<').replace(/&gt;/gi, '>');
    respData.zetaAddress = addbr;
    respData.invoiceCreationDate = Util.timestampToDate(respData.invoiceCreationDate);
    respData.creationDate = Util.timestampToDate(respData.creationDate);
    if(respData.ifiName.toLowerCase() == "kotak"){
        res.render('section/spotlight/virtual/invoices/csproforma-invoice', respData);
    }
    else if(respData.ifiName.toLowerCase() == "hdfc"){
        res.render('section/spotlight/virtual/invoices/hdfcproforma-invoice', respData);
    }
    else{
        res.render('section/spotlight/virtual/invoices/proforma-invoice', respData);
    }
    res.end();
})
.catch(function(error) {
    res.locals.showErrorWitHdr = true;
    Util.errorFoundHandler(error, req, res, next);
});
}

let PointBasedPInvoice = function (req, res, next) {
    let companyId = req.params.companyId;
    let orderId = req.params.orderId;
    let token = req.session[Constant.SESSION_AUTH].authToken;
    let corporateId = res.locals.corp.id;
    let programID = req.params.programID;
    SpotlightSvc.getPointBasedPInvoice(token, companyId, corporateId, orderId, programID).then(function (respData) {
            respData = JSON.parse(respData);
            respData.zetaAddress = Util.htmlDecoder(respData.zetaAddress);
            respData.creationDate = Util.timestampToDate(respData.creationDate);
            respData.invoiceCreationDate = Util.timestampToDate(respData.invoiceCreationDate);
            respData.orderTypeText = Util.htmlDecoder(respData.orderTypeText)
            console.log(respData)
            if (respData.ifiName.toLowerCase() == "kotak") {
                res.render('section/spotlight/pointbased/invoices/csproforma-invoice', respData);
            } else if (respData.ifiName.toLowerCase() == "hdfc") {
                res.render('section/spotlight/pointbased/invoices/hdfcproforma-invoice', respData);
            } else {
                res.render('section/spotlight/pointbased/invoices/proforma-invoice', respData);
            }
            res.end();
        })
        .catch(function (error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });

};



module.exports = {
    init: init,
    getSpotlightDetails: getSpotlightDetails,
    getCashIncentiveDetails: getCashIncentiveDetails,
    getNewTransfer: getNewTransfer,
    getTransfers: getTransfers,
    getOrderTransfers: getOrderTransfers,
    getOrderErrors: getOrderErrors,
    loadCards: loadCards,
    getPayoutOrders: getPayoutOrders,
    setSelectedCompany: setSelectedCompany,
    getInstagiftFaqs: getInstagiftFaqs,
    getInstagiftControlCenter: getInstagiftControlCenter,
    getnewSelectGifting: getnewSelectGifting,
    getSelectGiftingDetails: getSelectGiftingDetails,
    fetchingGiftingDetails: fetchingGiftingDetails,                                        
    getPhysicalCardGiftProformaInvoice:getPhysicalCardGiftProformaInvoice,                                    
    getnewInvoice: getnewInvoice,
    getnewPInvoice: getnewPInvoice,
    getRewardDetails: getRewardDetails,
    getSingleTransferDetails: getSingleTransferDetails,
    getOrderErrorFound: getOrderErrorFound,
    getVirtualDetails: getVirtualDetails,
    getVirtualIndividualOrderDetails: getVirtualIndividualOrderDetails,
    getPhysicalIndividualOrderDetails: getPhysicalIndividualOrderDetails,
    getVirtualGiftInvoice: getVirtualGiftInvoice,
    getVirtualGiftProformaInvoice: getVirtualGiftProformaInvoice,
    getPhysicalDetails: getPhysicalDetails,
    getPointBased: getPointBased,
    getPointBasedProgram: getPointBasedProgram,
    getPointBasedProgramDetails: getPointBasedProgramDetails,
    orderPhysicalCard: orderPhysicalCard,
    issuePhysicalCard: issuePhysicalCard,
    uploadPointBasedBeneficary: uploadPointBasedBeneficary,
    uploadPointBasedissue: uploadPointBasedissue,
    payOutPhysicalCard: payOutPhysicalCard,
    pointBasedSinglePayoutDetails: pointBasedSinglePayoutDetails,
    PointBasedPInvoice:PointBasedPInvoice
};

var AdminSvc = require('../service/adminSvc');
var BenefitSvc = require('../service/benefitSvc');
var Constant = require('../common/constant');
var RecommonedProgramsModel = require('../model/recommonedProgramsModel');
var CashlessModel = require('../model/cashlessModel');
var Util = require('../common/util');
var LosslessJSON = require('lossless-json');
var Urls = require('../common/url');
var JSONbig = require('json-bigint');
var UtilsUser = require('../common/utils/user.js');

let init = function(req, res, next) {
    res.locals.pageName = 'cashless';
    let corpID = res.locals.corp && res.locals.corp.id;
    let companyID = req.params.companyId;
    setSelectedCompany(req, res);
    //let apiKey = req.session[Constant.SESSION_AUTH].apiKey;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    BenefitSvc.getActiveCards(authToken, corpID, companyID).then(function(respData) {
        let activeCards = JSON.parse(respData),
            allRecommondedCardPrograms = CashlessModel.init(),
            recommondedCardPrograms,
            programs = activeCards.programs,
            programsCount = programs.length,
            newProgramSet = [],
            giftPrograms = [],
            closedCards = [],
            allCardsSchedule = [],
            productTypeFilter = [],
            displayBanner = false,
            lastIssuanceDate,
            displayBannerDate = UtilsUser.checkGKFlag(req, res.locals.selectedCompany.company.companyID, Constant.YEAR_END_CLOSURE_GK_FLAG),
            displayTag = false;
        for (let i = 0; i < programsCount; i++) {
            if (programs[i].corpProductType === 'OPTIMA') {
                let productType = programs[i].cardProgramObject.productType.toLowerCase();
                productTypeFilter[i] = programs[i].cardProgramObject.productType;
                let cardName = programs[i].cardProgramObject.cardName;
                programs[i].cardProgramObject.taxStatus = Constant.CARDS_TAX_STATUS[productType];
                let compareProductTypeToPrograms = Util.compareProductTypeToPrograms(programs[i].cardProgramObject.productType);

                if (!compareProductTypeToPrograms) {
                    allCardsSchedule.push(programs[i]);
                }

                if (productType === 'gift' && !cardName) {
                    programs[i].cardProgramObject.cardName = 'Gift Vouchers';
                }


                if (programs[i].cardProgramObject.issuanceConfiguration && programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate) {
                    lastIssuanceDate = new Date(programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate);
                } else {
                    lastIssuanceDate = '';
                }
                if (Util.compareLastIssuanceToCurrentDate(lastIssuanceDate) || programs[i].status === 'CLOSED') {
                    closedCards.push(programs[i]);
                } else {
                    if (productType === 'gift') {
                        giftPrograms.push(programs[i]);
                    } else {
                        newProgramSet.push(programs[i]);
                    }
                    if (programs[i].cardProgramObject.issuanceConfiguration && programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate && programs[i].cardProgramObject.issuanceConfiguration.extraIssuanceConfig) {
                        let compareTenDaysBefore = Util.compareTenDaysBefore(programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate);
                        if ( /*Util.displayBanner(Constant.DISPLAY_BANNER_DATE)*/ displayBannerDate && !compareProductTypeToPrograms && !programs[i].cardProgramObject.issuanceConfiguration.lastIssuanceDate && programs[i].cardProgramObject.issuanceConfiguration.extraIssuanceConfig["corpben.financialYear"] === '2016-17') {
                            displayBanner = true;
                        }

                        if (compareTenDaysBefore && !compareProductTypeToPrograms) {
                            displayTag = true;
                        }
                    }
                }


            }

        }

        activeCards.programs = newProgramSet.concat(giftPrograms);
        recommondedCardPrograms = Util.filterRecommondedPrograms(allRecommondedCardPrograms, activeCards.programs);
        let benefitHomePageData = {
            "activeCards": activeCards,
            "closedCards": closedCards,
            "allCardsSchedule": allCardsSchedule,
            "displayBanner": displayBanner,
            "displayTag": displayTag,
            "productTypeFilter": productTypeFilter,
            "recommondedCardPrograms": recommondedCardPrograms,
            "companyID": companyID

        };
        AdminSvc.getFundingAcc(authToken, corpID, 1, 10).then(function(fundRespData) {
            benefitHomePageData["fundingAccounts"] = LosslessJSON.parse(fundRespData);
            if (benefitHomePageData.recommondedCardPrograms) {
                for (var currIndex = 0; currIndex < benefitHomePageData.recommondedCardPrograms.length; currIndex++) {
                    benefitHomePageData.recommondedCardPrograms[currIndex].taxSaving.amount = Util
                        .formatINR(benefitHomePageData.recommondedCardPrograms[currIndex].taxSaving.amount / 100);
                }
            } else {
                benefitHomePageData.recommondedCardPrograms = [];
            }
            req.session["programs"] = activeCards.programs.concat(closedCards);
            req.session["fundingAccounts"] = benefitHomePageData["fundingAccounts"].fundingAccounts;
            res.render('section/cashless/home', benefitHomePageData);
            res.end();
        }).catch(function(error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });
    }, function(error) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(error, req, res, next);
    });
};

let getCashlessDetails = function(req, res, next) {
    res.locals.pageName = 'cashless';
    // res.locals.currPage = 'benefitDetails';
    let programID = req.params.cardProgramId;
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let displayTag = false;
    setSelectedCompany(req, res);
    BenefitSvc.getDetails(authToken, corpID, companyID, programID).then(function(respData) {
        respData = JSONbig.parse(respData);
        respData.compareDates = Util.compareLastIssuanceToCurrentDate(respData.cardProgramObject.issuanceConfiguration.lastIssuanceDate);
        req.session["compareDates"] = respData.compareDates;
        let compareTenDaysBefore = Util.compareTenDaysBefore(respData.cardProgramObject.issuanceConfiguration.lastIssuanceDate);
        if (respData.cardProgramObject.issuanceConfiguration.lastIssuanceDate && !Util.compareProductTypeToPrograms(respData.cardProgramObject.productType)) {
            var issuanceConfiguration = respData.cardProgramObject.issuanceConfiguration;
            var reimbursementProgram = respData.cardProgramObject.reimbursementProgram;
            respData.cardProgramObject.issuanceConfiguration.lastIssuanceDate = Util.timestampToDate(issuanceConfiguration.lastIssuanceDate);
            respData.cardProgramObject.reimbursementProgram.lastClaimUploadDate = Util.timestampToDate(reimbursementProgram.lastClaimUploadDate);
            respData.cardProgramObject.reimbursementProgram.lastClaimProcessDate = Util.timestampToDate(reimbursementProgram.lastClaimProcessDate);
            if (respData.cardProgramObject.closeCardConfiguration && respData.cardProgramObject.closeCardConfiguration.closeCardDate) {
                respData.cardProgramObject.closeCardConfiguration.closeCardDate = Util.timestampToDate(respData.cardProgramObject.closeCardConfiguration.closeCardDate);
            }
            if (issuanceConfiguration && issuanceConfiguration.extraIssuanceConfig) {
                respData.cardProgramObject.issuanceConfiguration.extraIssuanceConfig["corpben.reportGenerationDate"] = Util.timestampToDate(issuanceConfiguration.extraIssuanceConfig["corpben.reportGenerationDate"]);
            }
        }
        respData.cardProgramObject.voucherProgram.voucherValidity = Constant.VOUCHER_VALIDITY[respData.cardProgramObject.voucherProgram.voucherValidity];
        if (respData.cardProgramObject.productType == 'Meal') {
            respData.cardProgramObject['leftColor'] = Constant.MEAL_LEFT_COLOR;
            respData.cardProgramObject['rightColor'] = Constant.MEAL_RIGHT_COLOR;
        } else if (respData.cardProgramObject.productType == 'Gift') {
            respData.cardProgramObject['leftColor'] = Constant.GIFT_LEFT_COLOR;
            respData.cardProgramObject['rightColor'] = Constant.GIFT_RIGHT_COLOR;
        } else if (respData.cardProgramObject.productType == 'Fuel') {
            respData.cardProgramObject['leftColor'] = Constant.FUEL_LEFT_COLOR;
            respData.cardProgramObject['rightColor'] = Constant.FUEL_RIGHT_COLOR;
        } else if (respData.cardProgramObject.productType == 'Medical') {
            respData.cardProgramObject['leftColor'] = Constant.MEDICAL_LEFT_COLOR;
            respData.cardProgramObject['rightColor'] = Constant.MEDICAL_RIGHT_COLOR;
        } else if (respData.cardProgramObject.productType == 'Communication') {
            respData.cardProgramObject['leftColor'] = Constant.COMMUNICATION_LEFT_COLOR;
            respData.cardProgramObject['rightColor'] = Constant.COMMUNICATION_RIGHT_COLOR;
        } else if (respData.cardProgramObject.productType == 'Asset') {
            respData.cardProgramObject['leftColor'] = Constant.ASSET_LEFT_COLOR;
            respData.cardProgramObject['rightColor'] = Constant.ASSET_RIGHT_COLOR;
        } else {
            respData.cardProgramObject['leftColor'] = Constant.DEFAULT_LEFT_COLOR;
            respData.cardProgramObject['rightColor'] = Constant.DEFAULT_RIGHT_COLOR;
        }
        respData.cardProgramObject.taxStatus = Constant.CARDS_TAX_STATUS[respData.cardProgramObject.productType.toLowerCase()];
        if (respData.totalPayoutsAmount) {
            if (respData.totalPayoutsAmount.amount) {
                respData.totalPayoutsAmount.amount = Util.formatINR(Math.round(respData.totalPayoutsAmount.amount / 100));
            }
        }
        if (respData.totalSpentAmount) {
            if (respData.totalSpentAmount.amount) {
                respData.totalSpentAmount.amount = Util.formatINR(Math.round(respData.totalSpentAmount.amount / 100));
            }
        }
        if (respData.reimbursementProgramDetails && respData.reimbursementProgramDetails.totalApprovedAmount) {
            if (respData.reimbursementProgramDetails.totalApprovedAmount.amount) {
                respData.reimbursementProgramDetails.totalApprovedAmount.amount = Util.formatINR(Math.round(respData.reimbursementProgramDetails.totalApprovedAmount.amount / 100));
            }
        }
        if (respData.fundingAccountDetails && respData.fundingAccountDetails.balance) {
            respData.fundingAccountDetails.balance = Util.formatINR(Math.round(respData.fundingAccountDetails.balance / 100));
        }
        if (respData.numberOfTransactions) {
            respData.numberOfTransactions = Util.formatNumber(respData.numberOfTransactions);
        }

        if (compareTenDaysBefore) {
            displayTag = true;
        }
        respData.displayTag = displayTag;
        console.log("-------------------> Display <----------------------",respData);
        req.session['benefitDetails'] = respData;
        res.render('section/cashless/cashless-details', respData);
        res.end();
    }, function(respErr) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);
    });
}

let getTransfers = function(req, res, next) {
    res.locals.pageName = 'cashless';
    res.locals.currPage = 'newTransfer';
    res.locals.benefitDetails = req.session.benefitDetails;
    let currMonth = Util.timestampToMonthYear(parseInt(req.query.startDate));
    let benefitDetails = res.locals.benefitDetails;
    let companyID = req.params.companyId;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    setSelectedCompany(req, res);
    let benefitHomePageData = {
        "benefitDetails": benefitDetails,
        "currMonth": currMonth,
        "companyID": companyID
    };
    res.render('section/cashless/transfer-details', benefitHomePageData);
    res.end();
};

let getOrderTransfers = function(req, res, next) {
    let orderID = req.params["orderId"];
    let corpID = res.locals.corp.id;
    let companyID = req.params.companyId;
    let apiKey = req.session[Constant.SESSION_AUTH].authToken;
    setSelectedCompany(req, res);
    BenefitSvc.getTransferDetails(apiKey, companyID, orderID, corpID).then(function(respData) {
            respData = JSON.parse(respData);
            let createdDate = respData.order.createdOn.split('T');
            let createdDateArr = createdDate[0].split('-');
            res.locals.pageName = 'benefits';
            res.locals.currPage = 'newTransfer';
            res.locals.benefitDetails = req.session.benefitDetails;
            respData.order.createdOn = Util.dateFormat(parseInt(createdDateArr[2]), parseInt(createdDateArr[1]), parseInt(createdDateArr[0]));
            respData.order["benefitType"] = Constant.CARD_PROGRAM_TYPES[respData.order.type];
            respData.order.statusLabel = Constant.ORDER_STATUS_LABELS[respData.order.status];
            respData.order.statusTooltip = Constant.ORDER_STATUS_TOOLTIPS[respData.order.status];
            respData.order.fileUploadedLink = BenefitSvc.getUploadedFile(apiKey, orderID);
            respData.order.proformaLink = BenefitSvc.getProformaFile(apiKey, orderID);
            respData.payoutStatusLabels = Constant.PAYOUT_STATUS_LABELS
            respData.companyID = companyID;
            respData.compareDates = req.session.compareDates;
            respData.revokeFunds = true;
            res.render('section/cashless/transfer-details', respData);
            res.end();
        })
        .catch(function(error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });
}

let getOrderErrors = function(req, res, next) {
    res.locals.pageName = 'benefits';
    res.locals.currPage = 'newTransfer';
    res.locals.benefitDetails = req.session.benefitDetails;
    let companyID = req.params.companyId;
    let orderID = req.params.orderId;
    let programID = req.params.cardProgramId;
    setSelectedCompany(req, res);
    let pageData = {
        companyID: companyID,
        orderID : orderID,
        programID : programID
    };
    res.render('newTransferErrors', pageData);
    res.end();
}

let getNewTransfer = function(req, res, next) { 
    res.locals.pageName = 'cashless';
    res.locals.currPage = 'newTransfer';
    res.locals.benefitDetails = req.session.benefitDetails;
    let companyID = req.params.companyId;
    setSelectedCompany(req, res);
    let productType = req.session.benefitDetails.cardProgramObject.productType.toLowerCase();
    productType = Constant.PROGRAM_TRANSFER_ENUM[productType] || productType;
    let corpID = req.session[Constant.SESSION_CORP_INFO].id;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    BenefitSvc.getFileUploadDetails(authToken, corpID).then(function(respData) {
        let fileUploadUrl = JSON.parse(respData).url;
        let pageData = {
            productType: productType,
            previewImage: Constant.TRANSFER_PREVIEW_IMAGES[productType],
            excelFileDownload: Constant.TRANSFER_EXCEL_DOWNLOAD_FILE[productType],
            companyID: companyID,
            fileUploadDetails: respData,
            fileUploadUrl: fileUploadUrl
        };
        res.render('section/cashless/new-transfer', pageData);
        res.end();
    }, function(respErr) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(respErr, req, res, next);
    });

    // if( req.session.benefitDetails.cardProgramObject &&  req.session.benefitDetails.cardProgramObject.cardName) {
    //  for(var prop in Constant.CARD_PROGRAM_TYPES) {
    //
    //      let productType = req.session.benefitDetails.cardProgramObject.productType.toLowerCase();
    //
    //      if(prop === productType) {
    //            pageData.productTypPe = prop;
    //          break;
    //      }
    //  }
    // }

};

let setSelectedCompany = function(req, res) {
    let companyID = req.params.companyId;
    let companiesInfo = req.session[Constant.SESSION_COMP_INFO];
    req.session['selectedProgram'] = "optima";
    for (var i = 0; i < companiesInfo.length; i++) {
        if (companyID == companiesInfo[i].company.companyID) {
            req.session[Constant.SESSION_SELECTED_COMP_INFO] = companiesInfo[i];
            res.locals.selectedCompany = companiesInfo[i];
        }
    }
}

let createCustomProgram = function(req, res, next) {
    let companyID = req.params.companyId;
    let corpID = req.session[Constant.SESSION_CORP_INFO].id;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let pageData = {
        companyID: companyID,
        iframeUrl : 'https://card-program-stage.zetaapps.in/#/home?corpId='+corpID+'&companyID='+companyID+'&from=https://corporates-stage.zetaapps.in'
    };
    res.locals.pageName = 'benefits';
    res.locals.currPage = 'createCustomProgram';
    res.render('createCustomProgram', pageData);
    res.end();
};

module.exports = {
    init: init,
    getCashlessDetails: getCashlessDetails,
    getTransfers: getTransfers,
    getNewTransfer: getNewTransfer,
    getOrderTransfers: getOrderTransfers,
    setSelectedCompany: setSelectedCompany,
    createCustomProgram : createCustomProgram,
    getOrderErrors : getOrderErrors
};

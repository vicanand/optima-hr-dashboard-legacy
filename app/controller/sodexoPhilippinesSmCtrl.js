const Constant = require('../common/constant');
const Util = require('../common/util');
const env = process.env.NODE_ENV || 'development_stage';
const queryString = require('query-string');
const AuthSvc = require('../service/authSvc');

const BASE_TOOL_URL = {
    'development_stage': 'https://php-merchant-pos-stage.zetaapps.in',
    'development_production': 'https://php-merchant-pos.zetaapps.in',
    'development_preprod': 'https://php-merchant-pos-pp.zetaapps.in',
    'stage': 'https://php-merchant-pos-stage.zetaapps.in',
    'production': 'https://php-merchant-pos.zetaapps.in',
    'preprod': 'https://php-merchant-pos-pp.zetaapps.in',
}

let init = function(req, res, next) {
    res.locals.pageName = 'sm-corp-' + req.params.page;
    let corpID = res.locals.corp && res.locals.corp.id;
    let companyID = req.params.companyId;
    if (res.locals.selectedCompany && res.locals.selectedCompany.company && res.locals.selectedCompany.company.companyID) {
        companyID = res.locals.selectedCompany.company.companyID;
    }
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let userProfile = req.session[Constant.SESSION_USER_PROFILE];
    const rbac = res.locals.rbac;
    setSelectedCompany(req, res);

    // req.protocol is not giving expected (https) result
    // var sourceUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    var sourceUrl = 'https://' + req.get('host') + req.originalUrl;
    // showOwnOnly : addAccountForEmployee checks for BCO Manager or Sodexo Admin
    let pageData = {
        corpID,
        companyID,
        authToken,
        userID: userProfile.userID,
        sourceUrl,
        historyOnly: !!(rbac && rbac["ICshowHistoryOnly"]),
        showOwnOnly: !!(rbac && rbac["ICshowOwnOnly"]),
    };
    if (userProfile.emailList.length) {
      pageData['employeeID'] = Util.extractEmailFirstPart(userProfile.emailList[0].email);
    }
    // For local purpose only
    // const local_env = env.indexOf('preprod') !== -1 ? 'pp' : 'stage'; 
    // const localUrl = 'http://local-'+local_env+'.zetaapps.in:8000';
    // BASE_TOOL_URL[env] = localUrl;

    let baseUrl=BASE_TOOL_URL[env] + '/#/sm-corp/';
    // const companies = req.session[Constant.SESSION_COMP_INFO];
    let routeTo = req.query.routeTo;
    if (routeTo && (routeTo === '/admin/funding-accounts'
    || routeTo === '/admin/access-controlV2'
    || routeTo === '/admin/funding-accounts'
    || routeTo === '/admin/funding-accounts/add-funds-details'
    || routeTo.includes('/admin/transaction-history')
    || routeTo.includes('/sm-corp/convert-gift')
    || routeTo.includes('/sm-corp/gift-pass'))) {
        if (routeTo.includes('/sm-corp/convert-gift') || routeTo.includes('/sm-corp/gift-pass')) {
            let pathTo = routeTo.includes('/sm-corp/convert-gift') ? 'sm-corp/convert-gift' : 'sm-corp/gift-pass';
            routeTo = '/companies/' + companyID + '/' + pathTo;
        } else if (routeTo === '/admin/funding-accounts/add-funds-details' || routeTo.includes('/admin/transaction-history')) {
            routeTo = '/admin/funding-accounts';
        }
        res.redirect(routeTo);
    } else if (rbac) {
        if (rbac['createOrder_'+companyID] || rbac['listOrders_'+companyID]) {
            res.render('section/sodexo-philippines-sm/home/index', {
                query: queryString.stringify(pageData),
                page: req.params.page,
                baseURL: baseUrl
            });
        } else if (rbac['addFunds_'+companyID] || rbac['moveFunds_'+companyID]) {
            res.redirect('/admin/funding-accounts');
        } else if (rbac['addAccountForEmployee_'+companyID]) {
            res.redirect('/admin/access-controlV2');
        } else {
            res.redirect('/account/noAccount');
        }
    } else {
        res.render('section/sodexo-philippines-sm/home/index', {
            query: queryString.stringify(pageData),
            page: req.params.page,
            baseURL: baseUrl
        });
    }
};

let setSelectedCompany = function (req, res) {
    let companyID = req.params.companyId;
    let companiesInfo = req.session[Constant.SESSION_COMP_INFO];
    // req.session['selectedProgram'] = "optima";
    for (var i = 0; i < companiesInfo.length; i++) {
        if (companyID == companiesInfo[i].company.companyID) {
            req.session[Constant.SESSION_SELECTED_COMP_INFO] = companiesInfo[i];
            res.locals.selectedCompany = companiesInfo[i];
        }
    }
};
  
module.exports = {
    init: init
};
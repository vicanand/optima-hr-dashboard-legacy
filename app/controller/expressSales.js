var ZetaMerchantSvc = require('../service/express/zetaMerchantSvc'),
    Constant = require('../common/constant'),
    moment = require('moment'),
    Util = require('../common/util'),
    BusinessSvc = require('../service/express/businessSvc'),
    ExpressVendorsCtrl = require('./expressVendorsCtrl');

var employeeSales = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    res.locals.pageName = 'express';
    var corpID = res.locals.corp.id;

    var pageData = {
        bannerTitle: 'Vendor Insights',
        companyID: req.params.companyId,
        currentTab: 'sales',
        businessID: req.params.businessID
    };

    var params = {
        corporateID: corpID
    };


    ZetaMerchantSvc.getVendors(authToken, {
        corporateID: corpID
    }).then(function (vendorsResponse) {
        ZetaMerchantSvc.getCitiesOfficeStore(authToken, params).then(function (cityData) {
            var businessData = ExpressVendorsCtrl.filterVendors(vendorsResponse, pageData.businessID);
            var cities = ExpressVendorsCtrl.filterCitiesByBusinessId(cityData.expressInsightsStoreList, pageData.businessID);
            pageData.businessData = businessData;
            pageData.citiesOfficesStores = JSON.stringify(cities);

            res.render('section/express/vendorSales', pageData);
            res.end();
        }, function (error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });
    });

};


var getSalesTransactionsMetrics = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    var corpID = res.locals.corp.id;
    req.query.corporateID = corpID;
    ZetaMerchantSvc.getSalesTransactionsMetrics(authToken, req.query).then(function (respData) {
        res.status(200);
        res.send(respData);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
};

var getSalesFootfall = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;

    ZetaMerchantSvc.getSalesFootfall(authToken, req.query).then(function (respData) {
        res.status(200);
        res.send(respData);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
};


module.exports = {
    getSalesTransactionsMetrics,
    getSalesFootfall,
    employeeSales
};
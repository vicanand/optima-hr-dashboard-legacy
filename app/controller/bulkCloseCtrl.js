var Constant = require('../common/constant');
var Util = require('../common/util');
var BulkActionSvc = require('../service/bulkActionSvc');
var BenefitSvc = require('../service/benefitSvc');


let getBulkOrders = function(req, res, next) {
    res.locals.pageName = 'bulk';
    res.render('section/bulk-close/orders', {});
    res.end();
};

let getIndividualOrdersInBulkOrder = function(req, res, next) {
    res.locals.pageName = 'bulk';
    res.render('section/bulk-close/individual-orders', {});
    res.end();
};

let newBulkOrder = function(req, res, next) {
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let corpID = req.session.corp_info && req.session.corp_info.id;
    res.locals.pageName = 'bulk';
    BulkActionSvc.getProgramShortCodes(authToken, corpID).then(function(shortcodeData) {
        shortcodeData = JSON.parse(shortcodeData);
        for(let i=0;i<shortcodeData.programShortCodeSummaryList.length;i++){
            shortcodeData.programShortCodeSummaryList[i]["productIcon"] = Constant.BULK_TRANSFER_ICON_IMAGES[shortcodeData.programShortCodeSummaryList[i].productType];
            shortcodeData.programShortCodeSummaryList[i]["shortCodeProductType"] = Constant.CARD_PROGRAM_TYPES[shortcodeData.programShortCodeSummaryList[i].productType];
        }
        BulkActionSvc.getFileUploadDetails(authToken, corpID).then(function(respData) {
            let fileUploadUrl = JSON.parse(respData).url;
            let productType = 'meal';
            let pageData = {
                shortcodeList : shortcodeData.programShortCodeSummaryList,
                productType: productType,
                previewImage: Constant.TRANSFER_PREVIEW_IMAGES[productType],
                excelFileDownload: Constant.TRANSFER_EXCEL_DOWNLOAD_FILE[productType],
                fileUploadDetails: respData,
                fileUploadUrl: fileUploadUrl
            };
            res.render('section/bulk-close/newTransfer', pageData);
            res.end();
        }, function(respErr) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(respErr, req, res, next);
        });
    }, function(error) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(error, req, res, next);
    });
};


module.exports = {
    getBulkOrders : getBulkOrders,
    getIndividualOrdersInBulkOrder : getIndividualOrdersInBulkOrder,
    newBulkOrder: newBulkOrder
};
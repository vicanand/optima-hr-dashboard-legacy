var Constant = require('../common/constant');
var Util = require('../common/util');
var BulkActionSvc = require('../service/bulkActionSvc');
var BenefitSvc = require('../service/benefitSvc');


let init = function(req, res, next) {
    let companyCodeSet = true;
    let companiesCount = res.locals.companies.length;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let corpID = req.session.corp_info && req.session.corp_info.id;
    let companyInfo = req.session[Constant.SESSION_COMPANY_SHORT_CODE];
    //companyInfo = companyInfo.companies;
    for(let i=0; i<res.locals.companies.length; i++){
        if(res.locals.companies[i].company.companyShortCode == undefined || res.locals.companies[i].company.companyShortCode == ''){
            companyCodeSet = false;
            { break; }
        }
    }
    res.locals.pageName = 'bulk';
    res.render('section/bulk-action/home', {companyCodeSet, companiesCount});
    res.end();

};

let getBulkOrders = function(req, res, next) {
    res.locals.pageName = 'bulk';
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let corpID = req.session.corp_info && req.session.corp_info.id;
    BulkActionSvc.getProgramShortCodes(authToken, corpID).then(function(shortcodeData) {
        res.render('section/bulk-action/orders', {shortcodeData: JSON.parse(shortcodeData)});
        res.end();
    })
};

let getIndividualOrdersInBulkOrder = function(req, res, next) {
    res.locals.pageName = 'bulk';
    res.render('section/bulk-action/individual-orders', {});
    res.end();
};



let newBulkOrder = function(req, res, next) {
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let corpID = req.session.corp_info && req.session.corp_info.id;
    res.locals.pageName = 'bulk';
    //console.log("Showing newBulkOrder");
    BulkActionSvc.getCompanies(authToken, corpID).then(function(companies){   
            
        //console.log(companies, 'make service call');
        let company =JSON.parse(companies);
        req.session[Constant.SESSION_COMPANY_SHORT_CODE] = company;
        //console.log(company);
    
        
        //console.log(req.session[Constant.SESSION_COMPANY_SHORT_CODE],'yyyyy');
        let companiessss=req.session[Constant.SESSION_COMPANY_SHORT_CODE].companies;
        let x = {};
        companiessss.forEach(function(company){
            x[company.companyID] = company;
        });
        //console.log(req.session[Constant.SESSION_COMP_INFO],'before update');
        req.session[Constant.SESSION_COMP_INFO].forEach(function(company) {
            // companyIDs.push(company.company.companyID);
            //console.log('----',company);
            company.company= x[company.company.companyID];
            
        });
        //console.log(req.session[Constant.SESSION_COMP_INFO],'after update');
        res.locals.companyInfo = req.session[Constant.SESSION_COMPANY_SHORT_CODE];




        BulkActionSvc.getProgramShortCodes(authToken, corpID).then(function(shortcodeData) {
            shortcodeData = JSON.parse(shortcodeData);
            for(let i=0;i<shortcodeData.programShortCodeSummaryList.length;i++){
                shortcodeData.programShortCodeSummaryList[i]["productIcon"] = Constant.BULK_TRANSFER_ICON_IMAGES[shortcodeData.programShortCodeSummaryList[i].productType];
                shortcodeData.programShortCodeSummaryList[i]["shortCodeProductType"] = Constant.CARD_PROGRAM_TYPES[shortcodeData.programShortCodeSummaryList[i].productType];
            }
            BulkActionSvc.getFileUploadDetails(authToken, corpID).then(function(respData) {
                let fileUploadUrl = JSON.parse(respData).url;
                let productType = 'meal';
                let pageData = {
                    shortcodeList : shortcodeData.programShortCodeSummaryList,
                    productType: productType,
                    previewImage: Constant.TRANSFER_PREVIEW_IMAGES[productType],
                    excelFileDownload: Constant.TRANSFER_EXCEL_DOWNLOAD_FILE[productType],
                    fileUploadDetails: respData,
                    fileUploadUrl: fileUploadUrl
                };
                res.render('section/bulk-action/newTransfer', pageData);
                res.end();
            }, function(respErr) {
                res.locals.showErrorWitHdr = true;
                Util.errorFoundHandler(respErr, req, res, next);
            });
        }, function(error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });
    }, function(error) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(error, req, res, next);
    });
};
  

let companyCodeEdit = function(req, res, next){
    let companyCodeSet = false;  
    let companiesCount = res.locals.companies.length;
    let companyInfo = req.session[Constant.SESSION_COMPANY_SHORT_CODE];
    companyInfo = companyInfo.companies;
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let corpID = req.session.corp_info && req.session.corp_info.id;
    for(let i=0; i<res.locals.companies.length; i++){
        if(res.locals.companies[i].company.companyShortCode){
            companyCodeSet = true;
            { break; }
        }
    }
    res.locals.pageName = 'bulk';
    res.render('section/bulk-action/edit-company-code', {companyCodeSet, companiesCount, companyInfo});
    res.end();
}


module.exports = {
    init: init,
    getBulkOrders : getBulkOrders,
    getIndividualOrdersInBulkOrder : getIndividualOrdersInBulkOrder,
    newBulkOrder: newBulkOrder,
    companyCodeEdit: companyCodeEdit
};

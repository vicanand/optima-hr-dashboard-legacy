var ZetaMerchantSvc = require('../service/express/zetaMerchantSvc'),
moment = require('moment'),
Constant = require('../common/constant');
Env = process.env.NODE_ENV || 'development_stage';
var init = function (req, res, next) {
    console.log('res: ', res);
    console.log('req: ', req);
    console.log("Env ",Env);
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    res.locals.pageName = 'EmployeeOnboard';
    var pageData = {
        companyID: req.params.companyId
    };
    var corpID = res.locals.corp.id;
    var companyID = res.locals.selectedCompany.company.companyID;
    var userID = req.session.user_profile.userID;
    var url;
    if (Env == "development_stage") {
        url = 'https://employee-onboard-stage.zetaapps.in';
    } else {
        url = 'https://employee-onboard.zetaapps.in';
    }
   // console.log("!------------------------IN THE SURVEY FLOW---------------------------!");
    res.render('section/employee-onboard/home',{corpID,authToken,userID,url,companyID});
    res.end();
}

module.exports = {
    init,
}
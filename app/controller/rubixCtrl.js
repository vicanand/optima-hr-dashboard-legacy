var Constant = require("../common/constant"),
  queryString = require("query-string");

const EXPENSE_BASE_URLS = {
  HCR: {
    production: "https://user-views.zetaapps.in/expenseCorporateHCR/",
    preprod: "https://user-views-pp.zetaapps.in/expenseCorporateHCR/",
    stage: "https://user-views-stage.zetaapps.in/expenseCorporateHCR/" //this won't work (dummy URL) replace this with the localhost url for developement
  },
  CPS: {
    production: "https://user-views.zetaapps.in/expenseCorporateProgramSetup/",
    preprod: "https://user-views-pp.zetaapps.in/expenseCorporateProgramSetup/",
    stage: "https://user-views-stage.zetaapps.in/expenseCorporateProgramSetup/" //this won't work (dummy URL) replace this with the localhost url for developement
  }
};

let init = function(req, res, next) {
  res.locals.pageName = "rubix-" + req.params.page;
  let corpID = res.locals.corp && res.locals.corp.id;
  let companyID = req.params.companyId;
  let authToken = req.session[Constant.SESSION_AUTH].authToken;
  let userProfile = req.session[Constant.SESSION_USER_PROFILE];

  var sourceUrl = req.protocol + "://" + req.get("host") + req.originalUrl;
  var env = process.env.NODE_ENV || "stage";
  let pageData = {
    corpID,
    companyID,
    authToken,
    userID: userProfile.userID,
    sourceUrl
  };
  let baseUrl = "https://corporates-expense-stage.zetaapps.in";
  if (env == "production") {
    baseUrl = "https://corporates-expense.zetaapps.in";
  } else if (env == "preprod") {
    baseUrl = "https://corporates-expense-pp.zetaapps.in";
  }
  res.render("rubix-page", {
    query: queryString.stringify(pageData),
    page: req.params.page,
    baseURL: baseUrl
  });
};

let hcrInit = function(req, res, next) {
  res.locals.pageName = "rubix-hcr";
  let corpID = res.locals.corp && res.locals.corp.id;
  let companyID = req.params.companyId;
  let authToken = req.session[Constant.SESSION_AUTH].authToken;
  let userProfile = req.session[Constant.SESSION_USER_PROFILE];
  let index = req.params.index || 0;

  var sourceUrl = req.protocol + "://" + req.get("host") + req.originalUrl;
  var env = process.env.NODE_ENV || "stage";
  let pageData = {
    corpID,
    companyID,
    authToken,
    userID: userProfile.userID,
    sourceUrl,
    index: index
  };
  let tempEnv = env.split("_");
  if (tempEnv.length == 2) {
    env = tempEnv[1];
  }
  let baseUrl = EXPENSE_BASE_URLS.HCR[env.toLocaleLowerCase()];
  res.render("rubix-page", {
    query: queryString.stringify(pageData),
    page: req.params.page,
    baseURL: baseUrl
  });
};

let cpsInit = function(req, res, next) {
  res.locals.pageName = "rubix-cps";
  let corpID = res.locals.corp && res.locals.corp.id;
  let companyID = req.params.companyId;
  let authToken = req.session[Constant.SESSION_AUTH].authToken;
  let userProfile = req.session[Constant.SESSION_USER_PROFILE];
  let index = req.params.index || 0;

  var sourceUrl = req.protocol + "://" + req.get("host") + req.originalUrl;
  var env = process.env.NODE_ENV || "stage";
  let pageData = {
    corpID,
    companyID,
    authToken,
    userID: userProfile.userID,
    sourceUrl,
    index: index
  };
  let tempEnv = env.split("_");
  if (tempEnv.length == 2) {
    env = tempEnv[1];
  }
  let baseUrl = EXPENSE_BASE_URLS.CPS[env.toLocaleLowerCase()];
  res.render("rubix-page", {
    query: queryString.stringify(pageData),
    page: req.params.page,
    baseURL: baseUrl
  });
};

module.exports = {
  init: init,
  hcrInit: hcrInit,
  cpsInit: cpsInit
};

var i2b = require("imageurl-base64");

let init = function(req, res, next) {
    let imageUrl = req._parsedUrl.query.split('url=')[1];
    i2b(imageUrl, function(err, data){
        res.end(JSON.stringify(data));
    });
    
};

module.exports = {
    init: init
}
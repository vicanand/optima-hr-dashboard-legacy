var ZetaMerchantSvc = require('../service/express/zetaMerchantSvc'),
    Constant = require('../common/constant'),
    moment = require('moment'),
    SettlementSvc = require('../service/express/settlementSvc'),
    Util = require('../common/util'),
    ExpressVendorsCtrl = require('./expressVendorsCtrl');


var employeeSettlements = function (req, res, next) {
    res.locals.pageName = 'express';
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    var corpID = res.locals.corp.id;

    var pageData = {
        bannerTitle: 'Vendor Insights',
        companyID: req.params.companyId,
        currentTab: 'settlement',
        businessID: req.params.businessID
    };


    var params = {
        corporateID: corpID
    };

    ZetaMerchantSvc.getVendors(authToken, {corporateID: corpID}).then(function (vendorsResponse) {
        ZetaMerchantSvc.getCitiesOfficeStore(authToken, params).then(function (cityData) {
            var businessData = ExpressVendorsCtrl.filterVendors(vendorsResponse, pageData.businessID);
            var cities = ExpressVendorsCtrl.filterCitiesByBusinessId(cityData.expressInsightsStoreList, pageData.businessID);
            pageData.businessData = businessData;
            pageData.citiesOfficesStores = JSON.stringify(cities);
            res.render('section/express/vendorSettlement', pageData);
            res.end();
        });
    });

};


var getSettlementsForCounter = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;

    SettlementSvc.getSettlementsForCounter(authToken, req.query).then(function (respData) {
        res.status(200);
        res.send(respData);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
};

var getSettlementMetrics = function(req, res, next){
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    
        SettlementSvc.getSettlementsMetrics(authToken, req.query).then(function (respData) {
            res.status(200);
            res.send(respData);
        }, function (error) {
            res.status(error.statusCode);
            res.send(error.body);
        });
}
module.exports = {
    employeeSettlements,
    getSettlementsForCounter,
    getSettlementMetrics
};
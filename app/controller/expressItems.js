var ZetaMerchantSvc = require('../service/express/zetaMerchantSvc'),
    Constant = require('../common/constant'),
    moment = require('moment'),
    Util = require('../common/util'),
    BusinessSvc = require('../service/express/businessSvc'),
    ExpressVendorsCtrl = require('./expressVendorsCtrl'),
    QuickshopInfoSvc = require('../service/express/quickShopInfoSvc');



var getItems = function (req, res, next) {
    res.locals.pageName = 'express';
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    var corpID = res.locals.corp.id;

    var pageData = {
        bannerTitle: 'Vendor Insights',
        companyID: req.params.companyId,
        currentTab: 'item-trends',
        businessID: req.params.businessID
    };


    var params = {
        corporateID: corpID
    };

    ZetaMerchantSvc.getVendors(authToken, {corporateID: corpID}).then(function (vendorsResponse) {
        ZetaMerchantSvc.getCitiesOfficeStore(authToken, params).then(function (cityData) {
            var businessData = ExpressVendorsCtrl.filterVendors(vendorsResponse, pageData.businessID);
            var cities = ExpressVendorsCtrl.filterCitiesByBusinessId(cityData.expressInsightsStoreList, pageData.businessID);
            pageData.businessData = businessData;
            pageData.citiesOfficesStores = JSON.stringify(cities);
            res.render('section/express/vendorItemTrends', pageData);
            res.end();
        }, function (error) {
            Util.errorFoundHandler(error, req, res, next);
        });
    }, function (error) {
        Util.errorFoundHandler(error, req, res, next);
    });
};


var getItemsConsumption = function (req, res, next) {
    res.locals.pageName = 'express';
    var authToken = req.session[Constant.SESSION_AUTH].authToken;
    var corpID = res.locals.corp.id;

    var pageData = {
        bannerTitle: 'Vendor Insights',
        companyID: req.params.companyId,
        currentTab: 'item-consumption',
        businessID: req.params.businessID
    };


    var params = {
        corporateID: corpID
    };

    ZetaMerchantSvc.getVendors(authToken, {corporateID: corpID}).then(function (vendorsResponse) {
        ZetaMerchantSvc.getCitiesOfficeStore(authToken, params).then(function (cityData) {
            var businessData = ExpressVendorsCtrl.filterVendors(vendorsResponse, pageData.businessID);
            var cities = ExpressVendorsCtrl.filterCitiesByBusinessId(cityData.expressInsightsStoreList, pageData.businessID);
            pageData.businessData = businessData;
            pageData.citiesOfficesStores = JSON.stringify(cities);
            res.render('section/express/vendorItemConsumption', pageData);
            res.end();
        }, function (error) {
            Util.errorFoundHandler(error, req, res, next);
        });
    }, function (error) {
        Util.errorFoundHandler(error, req, res, next);
    });
};

var getItemTrends = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;

    QuickshopInfoSvc.getItemTrends(authToken, req.query).then(function (respData) {
        res.status(200);
        res.send(respData);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
};


var getItemConsumptionsData = function (req, res, next) {
    var authToken = req.session[Constant.SESSION_AUTH].authToken;

    QuickshopInfoSvc.getItemConsumptions(authToken, req.query).then(function (respData) {
        res.status(200);
        res.send(respData);
    }, function (error) {
        res.status(error.statusCode);
        res.send(error.body);
    });
};



module.exports = {
    getItems,
    getItemTrends,
    getItemsConsumption,
    getItemConsumptionsData
};

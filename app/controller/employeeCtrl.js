var Config = require('../../config');
var EmployeeSvc = require('../service/employeeSvc');
var Constant = require('../common/constant');
var Util = require('../common/util');
var BenefitSvc = require('../service/benefitSvc');
var JSONbig = require('json-bigint');
var Promise = require('bluebird');
var UtilsUser = require('../common/utils/user.js');

let init = function(req, res, next) {
    let corpID = res.locals.corp.id;
    let companyID = res.locals.selectedCompany.company.companyID;
    let employeeSetupEnabled = UtilsUser.checkGKFlag(req, res.locals.selectedCompany.company.companyID, Constant.EMPLOYEE_SETUP_ENABLED);
    BenefitSvc.getActiveCards(req.session[Constant.SESSION_AUTH].authToken, corpID, companyID).then(function(respData) {
        let pageData = {
            programTypes: Constant.CARD_PROGRAM_TYPES,
            activeCards: JSON.parse(respData)
        };
        pageData['employeeSetupEnabled'] = employeeSetupEnabled;
        res.locals.pageName = 'employees';
        res.render('employees', pageData);
        res.end();
    }).catch(function(error) {
        res.locals.showErrorWitHdr = true;
        Util.errorFoundHandler(error, req, res, next);
    });
};

let getCompleteEmployeeDetails = function(authToken, corpID, companyID, employeeID) {
    var promises = [
        EmployeeSvc.getEmployeeDetails(authToken, corpID, employeeID),
        BenefitSvc.getActiveCards(authToken, corpID, companyID)
    ];
    return Promise.all(promises).then(function(response) {
        let employee = JSONbig.parse(response[0]);
        let benefits = JSONbig.parse(response[1]);
        benefits = benefits.programs;
        let displayTag = false;
        for (var i = 0; i < employee.benefits.length; i++) {
            for (var j = 0; j < benefits.length; j++) {
                if (employee.benefits[i].programID === benefits[j].programID) {
                    employee.benefits[i].cardProgram = benefits[j].cardProgramObject;
                    if (employee.benefits[i].cardProgram.issuanceConfiguration) {
                        let benefitsCardProgram = employee.benefits[i].cardProgram;
                        employee.benefits[i].cardProgram.compareDates = Util.compareLastIssuanceToCurrentDate(benefitsCardProgram.issuanceConfiguration.lastIssuanceDate);
                        let compareTenDaysBefore = Util.compareTenDaysBefore(benefitsCardProgram.issuanceConfiguration.lastIssuanceDate);
                        if (compareTenDaysBefore) {
                            displayTag = true;
                        }
                        if (employee.benefits[i].cardProgram.issuanceConfiguration.lastIssuanceDate) {
                            employee.benefits[i].cardProgram.issuanceConfiguration.lastIssuanceDate = Util.timestampToDate(benefitsCardProgram.issuanceConfiguration.lastIssuanceDate);
                        }
                    }
                    break;
                }
            }
        }
        employee.displayTag = displayTag;
        if (employee.profile && employee.profile.emailList && employee.email) {
            for (var i = 0; i < employee.profile.emailList.length; i++) {
                if (employee.profile.emailList[i].email === employee.email) {
                    employee.isEmailVerified = employee.profile.emailList[i].isVerified;
                }
            }
        }
        employee.profile = employee.profile || {
            mobileNumber: '',
            physSuperCards: [],
            phoneNumbers: []
        }
        employee.isEmailVerified = employee.isEmailVerified || false;
        return employee;
    })
}

let employeeDetails = function(req, res, next) {
    res.locals.pageName = 'employees';
    let companyID = req.query.companyID;
    let corpID = res.locals.corp.id;
    let employeeID = req.params[0];
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    getCompleteEmployeeDetails(authToken, corpID, companyID, employeeID).then(function(respData) {
            let noOfActiveSuperTags = 0;
            let noOfActiveSuperCards = 0;
            for (var currIndex = 0; currIndex < respData.benefits.length; currIndex++) {
                respData.benefits[currIndex].cardProgram['statementURL'] = Config.corpBaseUrl +
                    'account/companies/' + respData.benefits[currIndex].companyID +
                    '/employees/' + respData.employeeID +
                    '/statement?productType=' + Constant.PROGRAM_TRANSFER_ENUM[respData.benefits[currIndex].cardProgram.productType.toLowerCase()] +
                    '&token=' + req.session[Constant.SESSION_AUTH].authToken +
                    '&corpID=' + corpID;
                if (respData.benefits[currIndex].totalPayoutAmount) {
                    respData.benefits[currIndex].totalPayoutAmount.amount = Util.formatINR(respData.benefits[currIndex].totalPayoutAmount.amount / 100);
                }
                if (respData.benefits[currIndex].totalSpentAmount) {
                    respData.benefits[currIndex].totalSpentAmount.amount = Util.formatINR(respData.benefits[currIndex].totalSpentAmount.amount / 100);
                }
                if (respData.benefits[currIndex].reimbursement && respData.benefits[currIndex].reimbursement.totalApprovedAmount) {
                    respData.benefits[currIndex].reimbursement.totalApprovedAmount.amount = Util.formatINR(respData.benefits[currIndex].reimbursement.totalApprovedAmount.amount / 100);
                }
                var productType = (respData.benefits[currIndex].cardProgram.cardName || respData.benefits[currIndex].cardProgram.productType).toLowerCase();
                respData.benefits[currIndex].cardProgram.color = respData.benefits[currIndex].cardProgram.cardDesign.bgColor || Constant.CARD_PROGRAM_COLORS['default'];
                respData.benefits[currIndex].productType = productType;
            }
            if (respData.superTags) {
                for (var currIndex = 0; currIndex < respData.superTags.length; currIndex++) {
                    if (respData.superTags[currIndex].state == 'ENABLED') {
                        noOfActiveSuperTags++;
                    }
                }
            }

            for (var key in respData.superCards) {
                if (respData.superCards[key].isPhysical && respData.superCards[key].state == 'ENABLED') {
                    noOfActiveSuperCards++;
                }
            }
            respData['noOfActiveSuperTags'] = noOfActiveSuperTags;
            respData['noOfActiveSuperCards'] = noOfActiveSuperCards;
            res.render('employee-employeeDetails', respData);
            res.end();
        })
        .catch(function(error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });

}
let employeeDetailsV2 = function(req, res, next) {
    res.locals.pageName = 'employees';
    let companyID = req.query.companyID;
    let corpID = res.locals.corp.id;
    let employeeID = req.params[0];
    let authToken = req.session[Constant.SESSION_AUTH].authToken;
    let cardPrograms = [];
    let set1 = new Set();
    let benefits = [];
    let cardsTooltip = [];
    let item = {};
    let emp = [];
    let finalItem = {};
    let noOfActiveCards = 0;
    EmployeeSvc.getEmployeeV2(authToken, corpID, companyID, employeeID).then(function(respData) {

            item = JSON.parse(respData);
            console.log('employeev2', item.employees);
            emp.push(item.employees[0]);
            if (emp[0].cards) {
                for (let k = 0; k < emp[0].cards.length; k++) {
                    if (emp[0].cards[k].status == 'ACTIVE') {
                        noOfActiveCards++;
                    }
                }
            }
            emp[0].noOfActiveCards = noOfActiveCards;
            //  if(emp[0].cards){
            //      for(let j = 0; j< emp[0].cards.length; j++){
            //       set1.add(emp[0].cards[j].programID);
            //      }
            //  }
            //  if(set1.size > 0){
            //      for( let a of set1){
            //          cardPrograms.push(a);
            //      }
            //  }

            let templateData = {
                showKYCStatus: true,
                showOverviewTab: true
            };

            if (req.session[Constant.SESSION_CONFIG_DATA]) {
                const config_data = Util.getDataFromObject(req.session[Constant.SESSION_CONFIG_DATA], 'employeeCtrl.employeeDetailsV2.templateData');
                if (config_data) {
                  templateData = config_data;
                }
            }

            if (emp[0].programIDs.length) { // fetching active card program details.
                let input = {
                    'corpID': corpID,
                    'programIDs': emp[0].programIDs,
                    'detailsRequired': ['cardDesign']
                };
                
                EmployeeSvc.getEmployeeProgramV2(input, authToken, {}).then(function(cardData) { // post call
                    let cardProgramData = cardData;
                    console.log('programdetails', cardProgramData)
                    if (emp.length > 0) {
                        //  item.employees[i].benefits = [];
                        emp[0].benefits = [];
                        emp[0].cardsTooltip = [];
                        if (emp[0].programIDs) {
                            for (let j = 0; j < emp[0].programIDs.length; j++) {
                                let temp = emp[0].programIDs[j];
                                emp[0].benefits.push(cardProgramData.programs[temp]);
                                emp[0].cardsTooltip.push(cardProgramData.programs[temp].cardDesign.cardName);
                            }
                        }
                    }
                    let noOfActiveSuperTags = 0;
                    let noOfActiveSuperCards = 0;
                    for (var key in respData.superCards) {
                        if (respData.superCards[key].isPhysical && respData.superCards[key].state == 'ENABLED') {
                            noOfActiveSuperCards++;
                        }
                    }
                    emp[0]['noOfActiveSuperTags'] = noOfActiveSuperTags;
                    emp[0]['noOfActiveSuperCards'] = noOfActiveSuperCards;
                    emp[0].corpID = corpID;
                    if (templateData && Object.keys(templateData).length) { emp[0].templateData = templateData; }
                    console.log('employeeDetails', emp[0]);
                    res.render('employee-employeeDetails', emp[0]);
                    res.end();
                });
            } else {
                let noOfActiveSuperTags = 0;
                let noOfActiveSuperCards = 0;
                for (var key in respData.superCards) {
                    if (respData.superCards[key].isPhysical && respData.superCards[key].state == 'ENABLED') {
                        noOfActiveSuperCards++;
                    }
                }
                item.employees[0]['noOfActiveSuperTags'] = noOfActiveSuperTags;
                item.employees[0]['noOfActiveSuperCards'] = noOfActiveSuperCards;
                item.employees[0].benefits = benefits;
                if (templateData && Object.keys(templateData).length) { item.employees[0].templateData = templateData; }
                console.log('employeeDetails', item.employees);
                res.render('employee-employeeDetails', item.employees[0]);
                res.end();
            }
        })
        .catch(function(error) {
            res.locals.showErrorWitHdr = true;
            Util.errorFoundHandler(error, req, res, next);
        });

}

module.exports = {
    init: init,
    employeeDetails: employeeDetails,
    employeeDetailsV2: employeeDetailsV2
};
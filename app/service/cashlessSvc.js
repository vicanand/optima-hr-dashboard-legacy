var Config = require('../../config');
var Constant = require('../common/constant');
var Util = require('../common/util');
var Endpoint = require('../common/url');
var ServiceConnector = require('../common/serviceConnector');

let _activeCards = function(authToken, corpID, companyID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_BENEFIT_PROGRAM'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID)
        .replace(':companyID:', companyID);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};


let getActiveCards = function(authToken, corpID, companyID) {
    return _activeCards(authToken, corpID, companyID);
}

let _getProgramDetails = function(authToken, corpID, companyID, programID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_BENEFIT_DETAILS_PROGRAM'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID)
        .replace(':companyID:', companyID)
        .replace(':programID:', programID);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};


let getDetails = function(authToken, corpID, companyID, programID) {
    return _getProgramDetails(authToken, corpID, companyID, programID);
};

let _getTransferDetails = function(authToken, companyID, orderID, corpID) {
    let url = (Config.corpBaseUrl + Endpoint['GET_TRANSFER_DETAILS'])
        .replace(':companyID:', companyID)
        .replace(':orderID:', orderID)
        .replace(':corpID:', corpID)
        .replace(':authToken:', authToken);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};

let getTransferDetails = function(authToken, companyID, orderID, corpID) {
    return _getTransferDetails(authToken, companyID, orderID, corpID);
};

let getUploadedFile = function(authToken, orderId) {
    return Config.corpBaseUrl + Endpoint['DOWNLOAD_UPLOADED_FILE'].replace(":authToken:", authToken).replace(":orderId:", orderId);
};

let getProformaFile = function(authToken, orderId) {
    return Config.corpBaseUrl + Endpoint['GET_PROFORMA_INVOICE'].replace(":authToken:", authToken).replace(":orderId:", orderId);
}

let getFileUploadDetails = function(authToken,corpID) {
    let url = Config.corpBaseUrl + Endpoint['GET_FILE_UPLOAD_DETAILS'];
    let header = {
        'token': authToken,
        "corpID": corpID
    }
    return ServiceConnector.get(url, header);
}

module.exports = {
    getActiveCards: getActiveCards,
    getDetails: getDetails,
    getTransferDetails: getTransferDetails,
    getUploadedFile: getUploadedFile,
    getProformaFile: getProformaFile,
    getFileUploadDetails: getFileUploadDetails
};

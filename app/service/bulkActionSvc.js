var Config = require('../../config');
var Constant = require('../common/constant');
var Util = require('../common/util');
var Endpoint = require('../common/url');
var ServiceConnector = require('../common/serviceConnector');

let _programShortCodes = function(authToken, corpID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_PROGRAM_SHORT_CODES'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};

let _companies = function(authToken, corpID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_COMPANIES'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};


let getProgramShortCodes = function(authToken, corpID) {
    return _programShortCodes(authToken, corpID);
};

let getCompanies = function(authToken, corpID){
    return _companies(authToken, corpID);
};

let getFileUploadDetails = function(authToken,corpID) {
    /* Node Call */
    // let url = Config.corpBaseUrl + Endpoint['GET_FILE_UPLOAD_DETAILS']; 
    // let header = {
    //     'token': authToken,
    //     "corpID": corpID
    // }
    // return ServiceConnector.get(url, header);
    /* OMS Call */
    let url = Config.omsBaseUrl + Endpoint['GET_ASSET_UPLOAD_ENDPOINT']; 
    let header = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, header);
}



module.exports = {
    getProgramShortCodes : getProgramShortCodes,
    getCompanies : getCompanies,
    getFileUploadDetails : getFileUploadDetails
};

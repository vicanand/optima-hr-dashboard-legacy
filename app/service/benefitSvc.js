var Config = require('../../config');
var Constant = require('../common/constant');
var Endpoint = require('../common/url');
var ServiceConnector = require('../common/serviceConnector');
var Util = require('../common/util');

let _activeCards = function (authToken, corpID, companyID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_BENEFIT_PROGRAM_PAGINATION'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID)
        .replace(':companyID:', companyID)
        .replace(':programStatus:', 'ACTIVE')
        .replace(':pageNumber:', 0)
        .replace(':pageSize:', 12);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};
let _activeCardsDocumentDrivePost = function (authToken, corpID, companyID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_BENEFIT_PROGRAM_PAGINATION_POST']);
    let params = {
        "token": authToken,
        "corpID": corpID,
        "companyID": companyID,
        "programStatus": "ACTIVE",
        "pageNumber": 0,
        "pageSize": 12,
        "productTypes": Constant.DOC_CORPBEN_PROGRAM_TYPES
    };
    let headers = {};
    return ServiceConnector.post(url, params, headers);
};
let _activeCardsPost = function (authToken, corpID, companyID, req) {
    let url = (Config.omsBaseUrl + Endpoint['GET_BENEFIT_PROGRAM_PAGINATION_POST']);
    let params = {
        "token": authToken,
        "corpID": corpID,
        "companyID": companyID,
        "programStatus": "ACTIVE",
        "pageNumber": 0,
        "pageSize": 12,
        "productTypes": Constant.PROGRAM_TYPES_ARRAY
    };
    if (req.session[Constant.SESSION_CONFIG_DATA]) {
        const config_data = Util.getDataFromObject(req.session[Constant.SESSION_CONFIG_DATA], 'benefitSvc.programTypeArray');
        if (config_data) {
            params.productTypes = Constant[config_data]
        }
    }
    let headers = {};
    console.log('param', params)
    return ServiceConnector.post(url, params, headers, req);
};

let _closedCards = function (authToken, corpID, companyID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_BENEFIT_PROGRAM_PAGINATION'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID)
        .replace(':companyID:', companyID)
        .replace(':programStatus:', 'CLOSED')
        .replace(':pageNumber:', 0)
        .replace(':pageSize:', 12);
    let params = {
        "productTypes": Constant.PROGRAM_TYPES_ARRAY
    };
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.post(url, params, headers);
};
let _moreActiveCards = function (authToken, corpID, companyID, pageNumber) {
    let url = (Config.omsBaseUrl + Endpoint['GET_BENEFIT_PROGRAM_PAGINATION'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID)
        .replace(':companyID:', companyID)
        .replace(':programStatus:', 'ACTIVE')
        .replace(':pageNumber:', pageNumber)
        .replace(':pageSize:', 12);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    console.log('url', url);
    return ServiceConnector.get(url, inputObj, headers);
};
let _moreActiveCardsPost = function (authToken, corpID, companyID, pageNumber) {
    let url = (Config.omsBaseUrl + Endpoint['GET_BENEFIT_PROGRAM_PAGINATION'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID)
        .replace(':companyID:', companyID)
        .replace(':programStatus:', 'ACTIVE')
        .replace(':pageNumber:', pageNumber)
        .replace(':pageSize:', 12);
    let params = {
        "productTypes": Constant.PROGRAM_TYPES_ARRAY
    };
    let headers = {
        "Content-Type": "application/json"
    };
    console.log('url', url);
    return ServiceConnector.post(url, params, headers);
};
let _moreClosedCards = function (authToken, corpID, companyID, pageNumber) {
    let url = (Config.omsBaseUrl + Endpoint['GET_BENEFIT_PROGRAM_PAGINATION'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID)
        .replace(':companyID:', companyID)
        .replace(':programStatus:', 'CLOSED')
        .replace(':pageNumber:', pageNumber)
        .replace(':pageSize:', 12);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};
let _moreClosedCardsPost = function (authToken, corpID, companyID, pageNumber) {
    let url = (Config.omsBaseUrl + Endpoint['GET_BENEFIT_PROGRAM_PAGINATION'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID)
        .replace(':companyID:', companyID)
        .replace(':programStatus:', 'CLOSED')
        .replace(':pageNumber:', pageNumber)
        .replace(':pageSize:', 12);
    let params = {
        "productTypes": Constant.PROGRAM_TYPES_ARRAY
    };
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, params, headers);
};

let getActiveCards = function (authToken, corpID, companyID) {
    return _activeCards(authToken, corpID, companyID);
};

let getActiveCardsDocumentDrivePost = function (authToken, corpID, companyID) {
    return _activeCardsDocumentDrivePost(authToken, corpID, companyID);
};

let getActiveCardsPost = function (authToken, corpID, companyID, req) {
    return _activeCardsPost(authToken, corpID, companyID, req);
};

let getClosedCards = function (authToken, corpID, companyID) {
    return _closedCards(authToken, corpID, companyID);
};

let getMoreActiveCards = function (authToken, corpID, companyID, pageNumber) {
    return _moreActiveCards(authToken, corpID, companyID, pageNumber);
};

let getMoreActiveCardsPost = function (authToken, corpID, companyID, pageNumber) {
    return _moreActiveCardsPost(authToken, corpID, companyID, pageNumber);
};

let getMoreClosedCards = function (authToken, corpID, companyID, pageNumber) {
    return _moreClosedCards(authToken, corpID, companyID, pageNumber);
};

let getMoreClosedCardsPost = function (authToken, corpID, companyID, pageNumber) {
    return _moreClosedCardsPost(authToken, corpID, companyID, pageNumber);
};

let getSelectGiftingProgramDetails = function (token, corporateId, companyID, spotlightType) {
    let url = (Config.omsBaseUrl + Endpoint['GET_SELECT_GITFTING_PROGRAM'])
        .replace(':token:', token)
        .replace(':corporateId:', corporateId)
        .replace(':companyId:', companyID)
        .replace(':spotlightType:', 'Select_Gifting');
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};

let _getProgramDetails = function (authToken, corpID, companyID, programID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_BENEFIT_DETAILS_PROGRAM'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID)
        .replace(':companyID:', companyID)
        .replace(':programID:', programID);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};


let getDetails = function (authToken, corpID, companyID, programID) {
    return _getProgramDetails(authToken, corpID, companyID, programID);
};

let _getCorpAddress = function (authToken, corpID) {

};

let getCorpAddress = function (authToken, corpID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_CORP_COMPANY_DETAILS'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID)
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
}

let _getTransferDetails = function (authToken, companyID, orderID, corpID) {
    let url = (Config.corpBaseUrl + Endpoint['GET_TRANSFER_DETAILS'])
        .replace(':companyID:', companyID)
        .replace(':orderID:', orderID)
        .replace(':corpID:', corpID)
        .replace(':authToken:', authToken);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};

let getTransferDetails = function (authToken, companyID, orderID, corpID) {
    return _getTransferDetails(authToken, companyID, orderID, corpID);
};

let getUploadedFile = function (authToken, orderId) {
    return Config.corpBaseUrl + Endpoint['DOWNLOAD_UPLOADED_FILE'].replace(":authToken:", authToken).replace(":orderId:", orderId);
};

let getProformaFile = function (authToken, orderId) {
    return Config.corpBaseUrl + Endpoint['GET_PROFORMA_INVOICE'].replace(":authToken:", authToken).replace(":orderId:", orderId);
};

let getFileUploadDetails = function (authToken, corpID) {
    const SIZE_10MB = 10485760;
    let url = Config.omsBaseUrl + Endpoint['GET_ASSET_UPLOAD_ENDPOINT'] + '?maxUploadSize=' + SIZE_10MB;
    let header = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, header);
};

let getAssestUploadEndPoint = function (authToken, corpID) {
    let url = Config.omsBaseUrl + Endpoint['GET_ASSET_UPLOAD_ENDPOINT'];
    let header = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, header);
};

let getCorpCardProgram = function (authToken, corpID, companyID, programID) {
    let url = Config.omsBaseUrl + Endpoint['GET_CORP_CARD_PROGRAM'].replace(":authToken:", authToken);
    let header = {
        'token': authToken,
        "corpID": corpID
    };
    let params = {
        "corpID": corpID,
        "companyID": companyID,
        "programID": programID
    };
    return ServiceConnector.post(url, params, header);
};

let syncProgramDetails = function (authToken, corpID, programID) {
    let url = Config.omsBaseUrl + Endpoint['CARD_DETAILS_SYNC'].replace(":authToken:", authToken);
    let header = {
        'token': authToken,
        "corpID": corpID
    };
    let params = {
        "programIDs": [programID],
        "dashboardDataSyncReportID": "CARD_PROGRAM_AGGREGATE_FINANCIAL",
        "corporateID": corpID
    };
    return ServiceConnector.post(url, params, header);
};

let getSignedDocument = function (authToken, documentUrl) {
    let url = Config.omsBaseUrl + Endpoint['GET_SIGNED_DOCUMENT']
        .replace(":authToken:", authToken)
        .replace(":s3FileUrl:", documentUrl)
        .replace(":apiKey:", Config.corpBenAPIToken)
        .replace(":x:", 450)
        .replace(":y:", 30)
        .replace(":width:", 130)
        .replace(":height:", 30);
    console.log(url);
    return ServiceConnector.get(url, {});
};

let getRecommendedPrograms = function (authToken, corpID, programID) {
    let url = Config.omsBaseUrl + Endpoint['GET_RECOMMENDED_PROGRAMS']
        .replace(":companyID:", programID)
        .replace(":corpID:", corpID)
        .replace(":authToken:", authToken)
    return ServiceConnector.get(url, {});
};
let getCardorderTemplate = function (authToken, corpID, companyID) {
    // let type = "VIRTUAL_NON_PERSONALISED";
    let url = (Config.omsBaseUrl + Endpoint['CARD_ORDER_LIST'])
        .replace(':corpID:', corpID)
        .replace(':companyID:', companyID);
    // .replace(':pageSize:', 10)
    // .replace(':pageNumber:', 0)

    let inputObj = {};
    let headers = {
        "Content-Type": "application/json",
        "X-Zeta-Authtoken": authToken
    };
    return ServiceConnector.get(url, inputObj, headers);

};
let getSuperCardDetails = function (authToken, corpID, companyID, superCardID) {
    let url = Config.omsBaseUrl + Endpoint['CARD_ORDER_DETAILS'].replace(":token:", authToken);
    let header = {
        'token': authToken,
    };
    let params = {
        "superCardID": superCardID,
        "corpID": corpID,
        "companyID": companyID
    };
    return ServiceConnector.post(url, params, header);
};

let getChecklistByRules = function (authToken, rulesList) {
    let url = Config.omsBaseUrl + Endpoint['GET_CHECKLIST_BY_RULE']
        .replace(":authToken:", authToken);
    let requestBody = {
        "checklistKey": rulesList
    }
    let header = {
        "Content-Type": "application/json"
    }
    return ServiceConnector.post(url, requestBody, header);
};

let getAllFundingAccounts = function (corpId, authToken) {
    let url = Config.omsBaseUrl + Endpoint['GET_FUNDING_ACCOUNTS']
        .replace(":authToken:", authToken)
        .replace(":corpID:", corpId);

    return ServiceConnector.get(url, {});
};

module.exports = {
    getActiveCardsPost: getActiveCardsPost,
    getMoreActiveCardsPost: getMoreActiveCardsPost,
    getMoreClosedCardsPost: getMoreClosedCardsPost,
    getMoreActiveCards: getMoreActiveCards,
    getMoreClosedCards: getMoreClosedCards,
    getActiveCards: getActiveCards,
    getActiveCardsDocumentDrivePost: getActiveCardsDocumentDrivePost,
    getClosedCards: getClosedCards,
    getDetails: getDetails,
    getTransferDetails: getTransferDetails,
    getUploadedFile: getUploadedFile,
    getProformaFile: getProformaFile,
    getFileUploadDetails: getFileUploadDetails,
    getCorpCardProgram: getCorpCardProgram,
    getSelectGiftingProgramDetails: getSelectGiftingProgramDetails,
    syncProgramDetails: syncProgramDetails,
    getAssestUploadEndPoint: getAssestUploadEndPoint,
    getSignedDocument: getSignedDocument,
    getRecommendedPrograms: getRecommendedPrograms,
    getCorpAddress: getCorpAddress,
    getCardorderTemplate: getCardorderTemplate,
    getSuperCardDetails: getSuperCardDetails,
    getChecklistByRules: getChecklistByRules,
    getAllFundingAccounts: getAllFundingAccounts,
};
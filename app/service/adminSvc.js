var Config = require('../../config');
var Constant = require('../common/constant');
var Util = require('../common/util');
var Endpoint = require('../common/url');
var ServiceConnector = require('../common/serviceConnector');
const Promise = require('bluebird');

let loadData = function() {
    let displayObj = {
        title: 'Admin'
    };
    return displayObj;
};

let getCorpInfo = function() {
    return new Promise(function(resolve, reject) {
        let respData = {
            name: "Apple Inc."
        };
        resolve(respData);
    });
};

// Get Companies
let getCompanies = function() {
    return new Promise(function(resolve, reject) {

        let companies = [{
            name: "Appl .inc"
        }, {
            name: "Twitter"
        }, {
            name: "Google"
        }];

        resolve(companies);
    });
};

// Get programs
let getPrograms = function() {
    return new Promise(function(resolve, reject) {

        let programs = [{
            cardUrl: " ",
            cardName: "Meal Vouchers",
            status: "Active",
            taxable: "Non - Taxable"

        }, {
            cardUrl: " ",
            cardName: "Medical Reimbursements",
            status: "In Active",
            taxable: "Taxable"
        }, {
            cardUrl: " ",
            cardName: "Gift Card",
            status: "In Active",
            taxable: "Taxable"
        }, {
            cardUrl: " ",
            cardName: "Meal Vouchers",
            status: "Active",
            taxable: "Non - Taxable"
        }];

        resolve(programs);
    });
};


// let getFundingAcc = function(){
// 	return new Promise(function (resolve, reject) {
// 		let programs = {
//   "fundingAccounts": [
//     {
//       "corpID": 342,
//       "accountID": 676,
//       "name": "Funding Account for- Test_Company5",
//       "transferInDetails": [
//         {
//           "transferModes": [
//             "IMPS"
//           ],
//           "ifscCode": "RATN0123456",
//           "accountNumber": "550000034262"
//         }
//       ],
//       "balance": 186000,
//       "type": "ZETA_SETTLED",
//       "productType": "CORPBEN_FUNDING_ACCOUNT",
//       "headers": {}
//     },
//     {
//       "corpID": 342,
//       "accountID": 1153,
//       "name": "Funding Account for - Test_CloseCard51061",
//       "transferInDetails": [
//         {
//           "transferModes": [
//             "IMPS"
//           ],
//           "ifscCode": "RATN0123456",
//           "accountNumber": "550000034268"
//         }
//       ],
//       "balance": 0,
//       "type": "ZETA_SETTLED",
//       "productType": "CORPBEN_FUNDING_ACCOUNT",
//       "headers": {}
//     },
//     {
//       "corpID": 342,
//       "accountID": 683,
//       "name": "Funding Account for- Zeta",
//       "transferInDetails": [
//         {
//           "transferModes": [
//             "IMPS"
//           ],
//           "ifscCode": "RATN0123456",
//           "accountNumber": "550000034261"
//         }
//       ],
//       "balance": 3792300,
//       "type": "ZETA_SETTLED",
//       "productType": "CORPBEN_FUNDING_ACCOUNT",
//       "headers": {}
//     },
//     {
//       "corpID": 342,
//       "accountID": 930,
//       "name": "Funding Account- FooBarTe",
//       "transferInDetails": [
//         {
//           "transferModes": [
//             "IMPS"
//           ],
//           "ifscCode": "RATN0123456",
//           "accountNumber": "550000034243"
//         }
//       ],
//       "balance": 0,
//       "type": "ZETA_SETTLED",
//       "productType": "CORPBEN_FUNDING_ACCOUNT",
//       "headers": {}
//     }]
// }
// 			resolve(programs);
// 		});
// }

let _activeCards = function(authToken, corpID, companyID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_BENEFIT_PROGRAM'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID)
        .replace(':companyID:', companyID);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};

let _activeCardsAll = function(authToken, corpID, companyID, pageNumber) {
    let url = (Config.omsBaseUrl + Endpoint['GET_BENEFIT_PROGRAM_PAGINATION_ADMIN'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID)
        .replace(':companyID:', companyID)
        .replace(':pageNumber:', pageNumber)
        .replace(':pageSize:', 12);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};

let getActiveCards = function(authToken, corpID, companyID) {
    return _activeCards(authToken, corpID, companyID);
};

let getActiveCardsAll = function(authToken, corpID, companyID, pageNumber) {
    return _activeCardsAll(authToken, corpID, companyID, pageNumber);
};

let getFundingAcc = function(authToken, corpID, pageNo, pageSize, req, companyID, rbac) {
    let url = (Config.omsBaseUrl + Endpoint['GET_FUNDING_ACCOUNTS'])
        .replace(':pageNo:', pageNo)
        .replace(':pageSize:', pageSize)
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    if (rbac && rbac['getFundingAccounts_'+companyID] && rbac['getFundingAccounts_'+companyID].indexOf('own') !== -1) {
        url += "&companyID=" + companyID;
    }
    return ServiceConnector.get(url, inputObj, headers, req);
};

let getIfisFundingAcc = function(authToken, corpID, ifiID, req) {
    let url = Config.omsBaseUrl + Endpoint['GET_IFIS_ACCOUNTS'].replace(':corpID:', corpID)
        .replace(':ifiID:', ifiID)
        .replace(':type:', "ZETA_SETTLED")
        .replace(':authToken:', authToken);
    return ServiceConnector.get(url, undefined, undefined, req);
};

let getAccDetails = function(authToken, corpID, accountId) {
    let url = (Config.omsBaseUrl + Endpoint['GET_FUNDING_ACCOUNT'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID)
        .replace(':accountID:', accountId);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};

let getUserProfile = function(getUserProfileURL,params,headers){
    return ServiceConnector.get(getUserProfileURL,params,headers);
};

// let triggerReportSync = function(authToken) {
//     let url = (Config.omsBaseUrl + Endpoint['TRIGGER_REPORT_SYNC'])
//     .replace(':authToken:', authToken)

//     let headers = {
//         "Content-Type": "application/json"
//     };

//     return ServiceConnector.post(url, inputObj, headers);
// }


module.exports = {
    getFundingAcc: getFundingAcc,
    loadData: loadData,
    getCorpInfo: getCorpInfo,
    getCompanies: getCompanies,
    getPrograms: getPrograms,
    getActiveCards: getActiveCards,
    getActiveCardsAll: getActiveCardsAll,
    getAccDetails: getAccDetails,
    getIfisFundingAcc: getIfisFundingAcc,
    getUserProfile : getUserProfile,
    // triggerReportSync : triggerReportSync
};
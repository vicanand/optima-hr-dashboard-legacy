var Util = require('../common/util');
var ServiceConnector = require('../common/serviceConnector');

let getClaimBills = function (params, url) {
    let headers = {};
    return ServiceConnector.post(url, params, headers);
}

let getTransactions = function (url) {
    let headers = {};
    return ServiceConnector.get(url, "", headers);
}

let getCardEligibility = function (url) {
    return ServiceConnector.get(url, "", {});
}
let getBillsForCardProgram = function (request, url) {
    let header = {}
    ServiceConnector.post(request, url, header).then(function (responseJson) {
        return responseJson;
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        return respError;
    });
}
let getTransactionsForCardId = function (request, url) {
    let header = {}
    ServiceConnector.post(request, url, header).then(function (responseJson) {
        return responseJson;
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        return respError;
    });
}
let getEmployeeImage = function (imageUrl) {
    let url = '/baseimage?url=' + imageUrl;
    ServiceConnector.get(url).then(function (responseJson) {
        return responseJson;
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        return respError;
    });
}

let getSignedPdf = function (url) {
    return ServiceConnector.get(url, {});
}

module.exports = {
    getClaimBills: getClaimBills,
    getTransactions: getTransactions,
    getCardEligibility: getCardEligibility,
    getBillsForCardProgram: getBillsForCardProgram,
    getTransactionsForCardId: getTransactionsForCardId,
    getEmployeeImage: getEmployeeImage,
    getSignedPdf: getSignedPdf
};
var Config = require('../../config');
var Constant = require('../common/constant');
var Util = require('../common/util');
var Endpoint = require('../common/url');
var ServiceConnector = require('../common/serviceConnector');

let loadData = function() {
    let displayObj = {
        title: 'Document'
    }
    return displayObj;
}

let _allDocGroups = function(authToken, corpID, businessId) {
    let url = (Config.omsBaseUrl + Endpoint['GET_DOCS']);
    let inputObj = {
        "corpID": corpID,
        "token": authToken,
        "docGroupsRequest": {
            "owner": businessId + "@business.zeta.in",
            "directoryPath": "biller/claims",
            "range": {
                "startInclusive": 0,
                "endInclusive": 11
            },
            "attrs": {}
        }
    };
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.post(url, inputObj, headers);
}
let _allDocGroupsPetro = function(authToken, corpID, businessId, companyID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_DOCS']);
    let inputObj = {
        "corpID": corpID,
        "token": authToken,
        "docGroupsRequest": {
            "owner": businessId + "@business.zeta.in",
            "directoryPath": "optima/proofs/"+corpID+"/"+companyID,
            "range": {
                "startInclusive": 0,
                "endInclusive": 11
            },
            "attrs": {}
        }
    };
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.post(url, inputObj, headers);
}

let _allDocGroupsWithPetro = function(authToken, corpID, businessId, companyId) {
    let url = (Config.omsBaseUrl + Endpoint['GET_DOCS']);
    let dateRange = '2020-04-01 - 2021-03-31';
    let startDate = Util.dateToepochTime(dateRange.slice(0, 10), '-', 'after');
    let endDate = Util.dateToepochTime(dateRange.slice(13, 23), '-', 'before');

    let inputObj = {
        "corpID": corpID,
        "token": authToken,
        "docGroupsRequest": {
            "owner": businessId + "@business.zeta.in",
            "directoryPaths": ["biller/claims","optima/proofs/"+corpID+"/"+companyId],
            "range": {
                "startInclusive": 0,
                "endInclusive": 11
            },
            "startDate":startDate,
            "endDate":endDate,
            "attrs": {
                "producttype": Constant.ALL_DOC_AND_CORPBEN_CARD_PROGRAM_TYPES
            }
        }
    };
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.post(url, inputObj, headers);
}

let _docDetails = function(authToken, corpID, businessId, docGroupID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_DOC_DETAILS']);
    let inputObj = {
        "corpID": corpID,
        "token": authToken,
        "docGroupRequest": {
            "docGroupID": "biller/claims/" + docGroupID,
            "owner": businessId + "@business.zeta.in",
        }
    };

    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.post(url, inputObj, headers);
}
let _docDetailsNew = function(authToken, corpID, businessId, docGroupID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_DOC_DETAILS']);
    let inputObj = {
        "corpID": corpID,
        "token": authToken,
        "docGroupRequest": {
            "docGroupID":docGroupID,
            "owner": businessId + "@business.zeta.in",
        }
    };

    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.post(url, inputObj, headers);
}

let _billDetails = function(authToken, docGroupID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_BILL_DETAILS'])+ "?token=" + authToken + "&claimID=" +docGroupID.replace('biller/claims/','');
    let inputObj = {
    };

    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
}

let getAllDocGroups = function(authToken, corpID, businessId) {
    return _allDocGroups(authToken, corpID, businessId);
}
let getAllDocGroupsWithPetro = function(authToken, corpID, businessId, CompanyId) {
    return _allDocGroupsWithPetro(authToken, corpID, businessId, CompanyId);
}
let getDocDetails = function(authToken, corpID, businessId, docGroupID) {
    return _docDetails(authToken, corpID, businessId, docGroupID);
}
let getDocDetailsNew = function(authToken, corpID, businessId, docGroupID) {
    return _docDetailsNew(authToken, corpID, businessId, docGroupID);
}
let getBillDetails = function(authToken, docGroupID) {
    return _billDetails(authToken, docGroupID);
}

module.exports = {
    loadData: loadData,
    getAllDocGroups: getAllDocGroups,
    //getAllDocGroupsPetro:getAllDocGroupsPetro,
    getAllDocGroupsWithPetro: getAllDocGroupsWithPetro,
    getDocDetails: getDocDetails,
    getBillDetails: getBillDetails,
    getDocDetailsNew:getDocDetailsNew
};

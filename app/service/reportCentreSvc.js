var Config = require('../../config');
var Endpoint = require('../common/url');
var ServiceConnector = require('../common/serviceConnector');

let getDashboardReports = function(authToken,corpID, product) {
    // let url = (Config.omsBaseUrl + Endpoint['GET_DASHBOARD_REPORTS'])
    //     .replace(':authToken:', authToken)
    //     .replace(":corpID:", corpID)
    //     .replace(':status:', true);
    let url = (Config.omsBaseUrl + Endpoint['GET_DASHBOARD_REPORTS_ON_PRODUCTS'])
        .replace(':authToken:', authToken)
        .replace(":corpID:", corpID)
        .replace(':product:', product)
        .replace(':status:', true);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
}

let getCorpdetails = function(authToken,corpID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_CORPORATE_DETAIL'])
        .replace(':token:', authToken)
        .replace(":corpID:", corpID);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
}

let getReportById = function(authToken, corpId, reportId){
    let url = (Config.omsBaseUrl + Endpoint['GET_REPORT_BY_ID'])
        .replace(":authToken:", authToken)
        .replace(":corporateId:", corpId)
        .replace(":reportId:", reportId);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
}

let getReportList = function(authToken, pageNumber, pageSize, corpId, body, product = 'OPTIMA'){
    let url = (Config.omsBaseUrl + Endpoint['GET_ARCHIVED_REPORTS'])
          .replace(":authToken:", authToken)
          .replace(":corpID:", corpId)
          .replace(":pageNo:", pageNumber)
          .replace(":product:", product)
          .replace(":pageSize:", pageSize);

    let inputObj = body;
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.post(url, inputObj, headers);
}

let getProgramDetails = function(authToken, programIds, corpId){
    let url = (Config.omsBaseUrl + Endpoint['GET_PROGRAMDETAILSV2'])
          .replace(":authToken:", authToken);

    let inputObj = {
        corpID: corpId,
        programIDs: programIds
    };
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.post(url, inputObj, headers);
}

module.exports = {
    getDashboardReports: getDashboardReports,
    getCorpdetails: getCorpdetails,
    getReportById: getReportById,
    getReportList: getReportList,
    getProgramDetails: getProgramDetails
};

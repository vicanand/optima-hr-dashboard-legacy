var Constant = require('../common/constant');
var ServiceConnector = require('../common/serviceConnector');
var Config = require('../../config');
var Endpoint = require('../common/url');
var Promise = require('bluebird');
var GateKeeperService = require('../service/gatekeeper.js');
var DashboardController = require('../controller/dashboardCtrl');
var RestApi = require('../common/restApi');
var Util = require('../common/util');
const env = process.env.NODE_ENV || 'development_stage';
var { craeteResourceConfigs } = require('../common/utils/roles');
const { COUNTRY_CONFIGS } = require('../common/countryConfig/configMappings');

let getUserProfile = function (req, res, next) {
    let getUserProfileURL = Config.omsBaseUrl + Endpoint.GET_USER_PROFILE.replace(':userJID:', req.session[Constant.SESSION_AUTH].userJID);
    let headers = {
        'X-Zeta-AuthToken': req.session[Constant.SESSION_AUTH].authToken,
        "Content-Type": "application/json"
    }
    return ServiceConnector.get(getUserProfileURL, '', headers);
};

let goToCorporate = function (req, res, next) {
    let selectedCorpID='';
    let corpDetails = {
        "id": '',
        "name": "",
        "accountHolderName": "",
        "allowedPrograms": []
    };
    let corpList;
    if(req.session.genie){
        selectedCorpID = req.session.genie;
        corpDetails = {
           "id": req.session.genie,
           "name": "",
           "accountHolderName": "",
           "allowedPrograms": ["OPTIMA","SPOTLIGHT","EXPENSE","EXPRESS"]
       }
   } else {
    corpList = req.session[Constant.SESSION_CORP_LIST];
    selectedCorpID = corpList.corporateCompanies[0].corporate.id;
    corpDetails = {
        "id": corpList.corporateCompanies[0].corporate.id,
        "ifi": corpList.corporateCompanies[0].corporate.defaultIFI,
        "name": corpList.corporateCompanies[0].corporate.name,
        "accountHolderName": "",
        "allowedPrograms": corpList.corporateCompanies[0].corporate.attributes.allowedProducts
    }
    }
    let getLastLogin = Config.omsBaseUrl + Endpoint.GET_LAST_LOGIN.replace(':token:', req.session[Constant.SESSION_AUTH].authToken).replace(':corpID:', selectedCorpID);
    RestApi.lastLoginRecord(req, res, next, getLastLogin)
    let getUserProfileURL = Config.omsBaseUrl + Endpoint.GET_USER_PROFILE.replace(':userJID:', req.session[Constant.SESSION_AUTH].userJID);
    let headers = {
        'X-Zeta-AuthToken': req.session[Constant.SESSION_AUTH].authToken
    }
    ServiceConnector.get(getUserProfileURL, '', headers).then(function (userProfile) {
        userProfile = JSON.parse(userProfile);
        req.session[Constant.SESSION_USER_PROFILE] = userProfile;
        req.session[Constant.SESSION_CORP_INFO] = corpDetails;
        let accountName = userProfile.name ? userProfile.name.firstName : '';
        corpDetails.accountHolderName = accountName;
        if (userProfile.name && userProfile.name.lastName) {
            accountName = accountName + " " + userProfile.name.lastName;
        }
        let accResp = {
            "account": {
                "accountDetails": {
                    "id": userProfile.userID,
                    "name": accountName,
                    "email": userProfile.emailList.length > 0 ? userProfile.emailList[0].email : ''
                }
            }
        };
        req.session[Constant.SESSION_ACC_INFO] = accResp;
        let companyDetailsUrl = Config.omsBaseUrl + Endpoint.GET_COMPANY_DETAILS.replace(':corpID:', selectedCorpID)
            .replace(':authToken:', req.session[Constant.SESSION_AUTH].authToken);
        ServiceConnector.get(companyDetailsUrl, {}).then(function (companyResp) {
            companyResp = JSON.parse(companyResp);
            req.session[Constant.SESSION_COMP_INFO] = companyResp.companySummaries;
            req.session[Constant.SESSION_SELECTED_COMP_INFO] = companyResp.companySummaries[0];
            let companyIDs = companyResp.companySummaries.map(function (company) {
                return company.company.companyID;
            });
            let authToken = req.session[Constant.SESSION_AUTH].authToken;
            let userId = req.session[Constant.SESSION_AUTH].userId;
            if (companyResp.companySummaries[0].company.attributes && companyResp.companySummaries[0].company.attributes.reviewBillingAddress) {
                console.log('Redirecting');
                console.log('https://' + req.headers.host + '/account/corpDetails');
                res.redirect('https://' + req.headers.host + '/account/corpDetails');
            }
            getGKFlagsForCorporate(req, authToken, userId, selectedCorpID, companyIDs).then(function (respData) {
                let activeProduct = '';
                const isSodexoPhpSm = COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.SM_CORP_IDS[env].indexOf(parseInt(selectedCorpID)) > -1;
                if (isSodexoPhpSm) {
                    activeProduct = 'sm-corp/gift-pass';
                    res.redirect('/companies/' + companyResp.companySummaries[0].company.companyID + '/' + activeProduct);
                } else if(corpDetails.allowedPrograms.indexOf("OPTIMA") > -1){   
                    activeProduct = req.session.selectedProgram || 'optima';
                    res.redirect('/companies/' + companyResp.companySummaries[0].company.companyID + '/' + activeProduct);
                } else if(corpDetails.allowedPrograms.indexOf("EXPRESS") > -1){
                    res.redirect('/express');
                } else if(corpDetails.allowedPrograms.indexOf("EXPENSE") > -1){
                    activeProduct = req.session.selectedProgram || 'optima';
                    res.redirect('/companies/' + companyResp.companySummaries[0].company.companyID + '/' + activeProduct);
                  // to do  res.redirect('/companies/' + companyResp.companySummaries[0].company.companyID + '/expense');
                } else if(corpDetails.allowedPrograms.indexOf("SPOTLIGHT") > -1) {
                    activeProduct = req.session.selectedProgram || 'spotlight';
                    res.redirect('/companies/' + companyResp.companySummaries[0].company.companyID + '/' + activeProduct);
                    // to do res.redirect('/companies/' + companyResp.companySummaries[0].company.companyID + '/spotlight');
                } else{
                    activeProduct = req.session.selectedProgram || 'optima';
                    res.redirect('/companies/' + companyResp.companySummaries[0].company.companyID + '/' + activeProduct);
                }
            }).catch(function (error) {
                console.log(error);
                res.sendStatus(500);
            });
        }).catch(function (error) {
            if(error.statusCode===500 && JSON.parse(error.body).type === 'UnauthorizedException'){
                console.log('Unauthorised to access the corporate');
                //res.sendStatus(401);
                res.status(401)
                .render('unauthorised', {title:'Zeta corpotate'});
            }else{
                res.sendStatus(500);
            }
            console.log(error);
        });
    }).catch(function (error) {
        console.log(error);
        res.sendStatus(500);
    });
};

let redirectToLogin = function (req, res) {
    return new Promise(function (resolve, reject) {
        if (req.session[Constant.SESSION_AUTH] &&
            req.session[Constant.SESSION_SELECTED_COMP_INFO] &&
            req.session[Constant.SESSION_ACC_INFO] &&
            req.session[Constant.SESSION_CORP_INFO] &&
            req.session[Constant.SESSION_COMP_INFO] && !req.query.authToken) {
            resolve();
        } else {
            let authToken;
            console.log('new auth');
            if (req.session[Constant.SESSION_AUTH] && req.session[Constant.SESSION_AUTH].authToken) {
                authToken = req.session[Constant.SESSION_AUTH].authToken;
            }
            if (req.query && req.query.authToken) {
                req.session[Constant.SESSION_AUTH] = req.query;
                authToken = req.query.authToken;
            }
            if (authToken) {
                let corpId = req.session.genie || req.query.corpID;
                if(req.session.genie){
                let response = {
                    corporateCompanies:[req.session.genie]
                }
                let corpList = {
                    corporateCompanies:[]
                };
                let selectedCorpID = req.session.genie; //req.originalUrl.split("=").pop();
                let getGenieForUserURL = Config.omsBaseUrl + Endpoint.GET_GENIE_FOR_USER.replace(':userId:', req.session[Constant.SESSION_AUTH].userId).replace(':corpId:', selectedCorpID);
                let headers = {
                    'X-Zeta-AuthToken': req.session[Constant.SESSION_AUTH].authToken,
                    "Content-Type": "application/json"
                }
                ServiceConnector.get(getGenieForUserURL, '', headers).then(function (newRespData) {
                    newRespData =JSON.parse(newRespData);
                    console.log("genie response",newRespData);
                    console.log("new genie session",newRespData.genieAuthToken);
                    req.session.auth.authToken = newRespData.genieAuthToken;
                    req.session.auth.userId = newRespData.genieUserId;
                    req.session.auth.userJID = newRespData.genieUserId+'@'+req.session.auth.userJID.split("@").pop();
                    req.session[Constant.SESSION_CORP_LIST] = corpList;
                    console.log(req.session.auth.userJID);
                    resolve(response);
                }, function (error) {
                    reject(error);
                });
                } else {
                let corporateListOfAccounts = Config.omsBaseUrl + Endpoint.GET_CORPORATE_LIST.replace(':authToken:', req.session[Constant.SESSION_AUTH].authToken).replace(':corporateID:', corpId);
                ServiceConnector.get(corporateListOfAccounts).then(function (corpLists) {
                    let corporateList = JSON.parse(corpLists);
                    req.session[Constant.SESSION_CORP_LIST] = corporateList;
                    resolve(corporateList);
                }, function (error) {
                    reject(error);
                });
              }
            } else {
                reject();
            }
            if (!authToken) {
                console.log("no token");
                let ssoUrl = Config.ssoUrl;
                let queryParams = '?redirectUrl=' + 'https://' + req.headers.host + '/dashboard';
                if (req.headers.host.indexOf('smdigitalportal') != -1) {
                    ssoUrl = Config.ssoPhUrl || COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.SSO_URL[env];
                    let ifi = COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.PLATFORM_IFI[env];
                    queryParams += '&appID=' + COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.APP_ID + '&ifi=' + ifi;
                } else if (req.headers.host.indexOf('sodexo.ph') != -1 || req.headers.host.indexOf('sodexoph-') != -1) {
                    queryParams += '&countryCode=' + COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.COUNTRY_CODE + '&ifi=' + COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.PLATFORM_IFI[env];
                } else if (req.headers.host.indexOf('sodexo.vn') != -1 || req.headers.host.indexOf('sodexovn') != -1 || Util.getUrlParameter('ifi', req.originalUrl) == COUNTRY_CONFIGS.SODEXO_VIETNAM_CONFIGS.PLATFORM_IFI[env]) {
                    queryParams += '&countryCode=' + COUNTRY_CONFIGS.SODEXO_VIETNAM_CONFIGS.COUNTRY_CODE + '&ifi=' + COUNTRY_CONFIGS.SODEXO_VIETNAM_CONFIGS.PLATFORM_IFI[env];
                }
                res.redirect(ssoUrl + queryParams);
            }
        }
    });
};

let updateCorpAndCompDetails = function (req, res) {
    return new Promise(function (resolve, reject) {
        let corpList = req.session[Constant.SESSION_CORP_LIST];
        let selectedCorpID = corpList.corporateCompanies[0].corporate.id;
        let companyDetailsUrl = Config.omsBaseUrl + Endpoint.GET_COMPANY_DETAILS.replace(':corpID:', selectedCorpID)
            .replace(':authToken:', req.session[Constant.SESSION_AUTH].authToken)
        ServiceConnector.get(companyDetailsUrl).then(function (companyResp) {
            companyResp = JSON.parse(companyResp);
            req.session[Constant.SESSION_COMP_INFO] = companyResp.companySummaries;
            req.session[Constant.SESSION_SELECTED_COMP_INFO] = companyResp.companySummaries[0];
            let companyIDs = companyResp.companySummaries.map(function (company) {
                return company.companyID;
            });
            if (companyResp.companySummaries[0].company.attributes && companyResp.companySummaries[0].company.attributes.reviewBillingAddress) {
                console.log('Redirecting');
                console.log('https://' + req.headers.host + '/account/corpDetails');
                res.redirect('https://' + req.headers.host + '/account/corpDetails');
            }
            getGKFlagsForCorporate(req, req.session[Constant.SESSION_AUTH].authToken, req.session[Constant.SESSION_AUTH].userId, selectedCorpID, companyIDs).then(function (respData) {
                res.redirect('https://' + req.headers.host + '/companies/' + req.session[Constant.SESSION_COMP_INFO].company.companyID + '/optima');
            }).catch(function (error) {
                reject(error);
            });
        }, function (error) {
            console.log(error);
            reject(error);
        });
    });
}

let getGKFlagsForCorporate = function (req, authToken, userId, corpID, companyIDs) {
    let promises = [];
    for (var i = 0; i < companyIDs.length; i++) {
        promises.push(GateKeeperService.getFlagsForCompany(authToken, userId, corpID, companyIDs[i]));
    }
    return Promise.all(promises).then(function (response) {
        req.session[Constant.SESSION_GK] = {
            companies: {}
        };
        for (var i = 0; i < response.length; i++) {
            req.session[Constant.SESSION_GK].companies[companyIDs[i]] = response[i];
        }
    }, function (error) {

    });
};

let associateZetaUserWithCorp = function (jwtToken, authToken) {
    let url = Config.omsBaseUrl + Endpoint.ASSOCIATE_ZETA_USER_WITH_CORP.replace(':authToken:', authToken);
    let inputObj = {
        "jwtToken": jwtToken,
    }
    return ServiceConnector.post(url, inputObj, {});
};

let signUpCorp = function(data){
    let url = Config.omsBaseUrl + Endpoint.NEW_SIGNUP_CORPORATE;
    return ServiceConnector.post(url, data, {});
}

let getPrivilegies = function (req, res) {
    const corpID = req.session[Constant.SESSION_CORP_INFO] && req.session[Constant.SESSION_CORP_INFO].id;
    if (corpID) {
        const companyId = req.session[Constant.SESSION_SELECTED_COMP_INFO].company.companyID;
        let getPrevilegiesUrl = Config.omsBaseUrl + Endpoint.GET_PRIVILEGIES.replace(':subjectJID:', req.session[Constant.SESSION_AUTH].userJID).replace(':authToken:', req.session[Constant.SESSION_AUTH].authToken)
        // .replace(':objectJID:', corpID + '@corpben.zeta.in/comp~' + companyId);
        return ServiceConnector.get(getPrevilegiesUrl);
    } else {
        return Promise.resolve(null);
    }
};
let getCorpAccountsV2 = function (req, res) {
    const corpID = req.session[Constant.SESSION_CORP_INFO] && req.session[Constant.SESSION_CORP_INFO].id;
    const userProfile = req.session[Constant.SESSION_USER_PROFILE];
    if (corpID && userProfile && userProfile.emailList && userProfile.emailList.length) {
        let getCorpAccountsV2Url = Config.omsBaseUrl + Endpoint.GET_CORP_ACCOUNTSV2.replace(':corpID:', corpID).replace(':authToken:', req.session[Constant.SESSION_AUTH].authToken);
        getCorpAccountsV2Url += "&searchKey=" + "EMAIL";
        getCorpAccountsV2Url += "&searchValue=" + userProfile.emailList[0].email;
        return ServiceConnector.get(getCorpAccountsV2Url);
    } else {
        return Promise.resolve(null);
    }
};
let privilegeCheck = function (req, res, next) {
    let corpID = req.session[Constant.SESSION_CORP_INFO] && req.session[Constant.SESSION_CORP_INFO].id;
    if (req.session[Constant.SESSION_RBAC_DATA] && req.session[Constant.SESSION_RBAC_DATA].corpID && req.session[Constant.SESSION_RBAC_DATA].corpID !== corpID) {
        req.session[Constant.SESSION_RBAC_DATA] = null;
    }
    return new Promise((resolve, reject) => {
        if (req.session[Constant.SESSION_RBAC_DATA] && (req.session[Constant.SESSION_RBAC_RESOURCES] == null || req.session[Constant.SESSION_RBAC_RESOURCES])) {
            resolve();
        } else if (req.session && req.session[Constant.SESSION_COMP_INFO] && corpID && req.session[Constant.SESSION_AUTH] && req.session[Constant.SESSION_USER_PROFILE]) {
            getPrivilegies(req, res).then((respData) => {
                if (respData) {
                    let data = {privilegeData: JSON.parse(respData)};
                    data.companyID = req.session[Constant.SESSION_SELECTED_COMP_INFO].company.companyID;
                    data.corpID = corpID;
                    data.countryVar = res.locals.countryVar;
                    getCorpAccountsV2(req, res).then((corpData) => {
                        if (corpData) {
                            data.corpAccountData = JSON.parse(corpData);
                        }
                        req.session[Constant.SESSION_RBAC_DATA] = data;
                        req.session[Constant.SESSION_RBAC_RESOURCES] = craeteResourceConfigs(data);
                        resolve();
                    }).catch((error) => {
                        console.log('Failed to get corp acc info', error);
                        Util.errorFoundHandler(error, req, res, next);
                    });
                } else {
                    reject('No Privilegies Data');
                }
            }).catch((error) => {
                console.log('Failed to get previlegies', error);
                Util.errorFoundHandler(error, req, res, next)
            });
        } else {
            reject('Session Not Found');
        }
    });
};

module.exports = {
    redirectToLogin: redirectToLogin,
    associateZetaUserWithCorp: associateZetaUserWithCorp,
    updateCorpAndCompDetails: updateCorpAndCompDetails,
    getGKFlagsForCorporate: getGKFlagsForCorporate,
    goToCorporate: goToCorporate,
    getUserProfile: getUserProfile,
    signUpCorp: signUpCorp,
    getPrivilegies: getPrivilegies,
    getCorpAccountsV2: getCorpAccountsV2,
    privilegeCheck: privilegeCheck
}
var Config = require('../../../config'),
Endpoint = require('../../common/url'),
ServiceConnector = require('../../common/serviceConnector'),
Promise = require('bluebird');


var getCustomerRatings = function(authToken,params){
    var url = (Config.omsBaseUrl + Endpoint['GET_CUSTOMER_RATINGS'])
    // .replace(':authToken:', authToken);
    var inputObj = params;
    var headers = {
        "Content-Type": "application/json",
        "X-Zeta-AuthToken":authToken
    };
    return ServiceConnector.post(url, inputObj, headers);
}

var getCustomerFeedback = function(authToken,params){
    var url = (Config.omsBaseUrl + Endpoint['GET_CUSTOMER_FEEDBACK'])
    // .replace(':authToken:', authToken);
    var inputObj = params;
    var headers = {
        "Content-Type": "application/json",
        "X-Zeta-AuthToken":authToken
    };
    return ServiceConnector.post(url, inputObj, headers);
}

var getCustomerComments = function(authToken,params){
    var url = (Config.omsBaseUrl + Endpoint['GET_CUSTOMER_COMMENTS'])
    // .replace(':authToken:', authToken);
    var inputObj = params;
    var headers = {
        "Content-Type": "application/json",
        "X-Zeta-AuthToken":authToken
    };
    return ServiceConnector.post(url, inputObj, headers);
}






module.exports={
    getCustomerRatings:getCustomerRatings,
    getCustomerFeedback:getCustomerFeedback,
    getCustomerComments:getCustomerComments
}
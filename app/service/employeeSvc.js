var Config = require('../../config');
var Constant = require('../common/constant');
var Util = require('../common/util');
var Endpoint = require('../common/url');
var ServiceConnector = require('../common/serviceConnector');

let loadData = function() {
    let displayObj = {
        title: 'Employee'
    }
    return displayObj;
}

// Get Employee programs
let getEmpPrograms = function() {
    return new Promise(function(resolve, reject) {

        let programs = [{
            cardName: "Meal Vouchers",
            status: "Active",
            issuedBy: "Apple Inc"

        }, {
            cardName: "Medical Reimbursement",
            status: "Active",
            issuedBy: "Google"
        }, {
            cardName: "Gift Card",
            status: "Active",
            issuedBy: "Twitter"
        }];

        resolve(programs);
    });

}

let _employeeDetails = function(authToken, corpID, employeeID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_EMPLOYEE_DETAILS'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID)
        .replace(':employeeID:', employeeID);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};


let getEmployeeDetails = function(authToken, corpID, employeeID) {
    return _employeeDetails(authToken, corpID, employeeID);
}


let _employeeV2 = function(authToken, corpID,companyID, employeeID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_EMPLOYEESV2'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID) + "&employeeID=" + employeeID;
      //  .replace(':companyID:', companyID)
      //  .replace(':employeeID:', employeeID);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};
let getEmployeeV2 = function(authToken, corpID,companyID, employeeID) {
    return _employeeV2(authToken, corpID,companyID, employeeID);
}

let _employeeProgramV2 = function(input, authToken, {}) {
    let url = (Config.omsBaseUrl + Endpoint['GET_PROGRAMDETAILSV2'])
        .replace(':authToken:', authToken);
   // let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.post(url, input, headers);
};
let getEmployeeProgramV2 = function(input, authToken, {}) {
    return _employeeProgramV2(input, authToken, {});
}
module.exports = {
    loadData: loadData,
    getEmpPrograms: getEmpPrograms,
    getEmployeeDetails: getEmployeeDetails,
    getEmployeeV2: getEmployeeV2,
    getEmployeeProgramV2 : getEmployeeProgramV2 
};

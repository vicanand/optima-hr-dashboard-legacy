var Config = require('../../config');
var Constant = require('../common/constant');
var Util = require('../common/util');
var Endpoint = require('../common/url');
var ServiceConnector = require('../common/serviceConnector');

let _checkAgreement = function(authToken, corpID, companyID) {
    let url = (Config.omsBaseUrl + Endpoint['CHECK_AGREEMENT_SIGNUP'])
        .replace(':authToken:', authToken)
        .replace(':corpID:', corpID)
        .replace(':companyID:', companyID);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};


let checkAgreement = function(authToken, corpID, programID, companyID, orderID) {
    return _checkAgreement(authToken, corpID, programID, companyID, orderID);
}

let _getPayoutsOrders = function(authToken, corpID, companyID, orderID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_INSTAGIFT_PAYOUTS'])
        .replace(':companyID:', companyID)
        .replace(':corpID:', corpID)
        .replace(':authToken:', authToken)
        .replace(':orderID:', orderID);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, {}, {});
};

let getPayoutsOrders = function(authToken, corpID, companyID, orderID) {
    return _getPayoutsOrders(authToken, corpID, companyID, orderID);
};

let getSelectGiftingDetails = function(token, corporateId, companyID, spotlightType) {
    let url = (Config.omsBaseUrl + Endpoint['GET_SELECT_GITFTING_DETAILS'])
        .replace(':token:', token)
        .replace(':corporateId:', corporateId)  
        .replace(':companyId:', companyID)
        .replace(':spotlightType:', 'Select_Gifting');
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};
let getSelectGiftingOrderList = function(token, corporateId, companyId) {
    let url = (Config.omsBaseUrl + Endpoint['GET_SELECT_GITFTING_ORDERLIST']);
    let inputObj = {
        "token":token,
        "corporateId":corporateId,
        "companyId":companyId,
        "spotlightType":"SELECT_GIFTING",
        "pageSize": 10
    };
    let headers = {
        "Content-Type": "application/json",
        'token': token
    };
    return ServiceConnector.post(url, inputObj, headers);
};

let getFileUploadDetails = function() {
    let url = Config.omsBaseUrl + Endpoint['GET_ASSET_UPLOAD_ENDPOINT'];
    let header = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, header);
};

let getProgramDetails = function(corpID, companyID, authToken) {
    let url = Config.omsBaseUrl + Endpoint['GET_SELECT_GITFTING_PROGRAM']
        .replace(':token:', authToken)
        .replace(':corporateId:', corpID)
        .replace(':companyId:', companyID)
    let header = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, header);
};
let getSelectGiftingInvoice = function(token, companyId, corporateId ,orderId) {
    let url = (Config.omsBaseUrl + Endpoint['GET_SELECT_GITFTING_INVOICEURL']);
    let inputObj = {
        "token":token,
        "corporateId" :corporateId,
        "companyId" :companyId,
        "orderId" :orderId,
        "spotlightType" :"SELECT_GIFTING",
    };
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};
let getSelectGiftingPInvoice = function(token, companyId, corporateId ,orderId) {
    let url = (Config.omsBaseUrl + Endpoint['GET_SELECT_GITFTING_PINVOICEURL']);
        let inputObj = {
            "token":token,
            "corporateId" :corporateId,
            "companyId" :companyId,
            "orderId" :orderId,
            "spotlightType" :"SELECT_GIFTING",
        };
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};

let getTemplateDesign = function(authToken, corpID, companyID, cardProgramType) {
    
    let url = (Config.omsBaseUrl + Endpoint['CARD_TEMPLATE_TYPE'])
        .replace(':token:', authToken)
        .replace(':corporateID:', corpID)
        .replace(':companyID:', companyID)
        .replace(':cardProgramType:', cardProgramType);
    let inputObj = {};
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};

let getVirtualTemplate = function(authToken, corpID, companyID){
    let type = "VIRTUAL_NON_PERSONALISED";
    let url = (Config.omsBaseUrl + Endpoint['VIRTUAL_CARD_TEMP'])
        .replace(':corpID:', corpID)
        .replace(':pageSize:', 10)
        .replace(':pageNumber:', 0)
        .replace(':type:', type)
        .replace(':companyID:', companyID);
    let inputObj = {
    };
    let headers = {
        "Content-Type": "application/json",
        "X-Zeta-Authtoken":authToken
    };
    return ServiceConnector.get(url, inputObj, headers);

};

let getVirtualGiftingInvoice = function(token, companyId, corporateId ,orderId) {
    let url = (Config.omsBaseUrl + Endpoint['GET_VIRTUAL_INVOICE']);
    let inputObj = {
        "token":token,
        "corporateID" :corporateId,
        "companyID" :companyId,
        "orderID" :orderId,        
    };
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};


let getVirtualGiftingProformaInvoice = function(token, companyId, corporateId ,orderId){
    let url = (Config.omsBaseUrl + Endpoint['GET_VIRTUAL_PROFORMA_INVOICE']);
    let inputObj = {
        "token":token,
        "corporateID" :corporateId,
        "companyID" :companyId,
        "orderID" :orderId,        
    };
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
}
let getPhysicalCardProformaInvoice = function(token, companyId, corporateId ,orderId){
    let url = (Config.omsBaseUrl + Endpoint['GET_PHYSICAL_CARD_PROFORMA_INVOICE']);
    let inputObj = {
        "token":token,
        "corporateID" :corporateId,
        "companyID" :companyId,
        "cardOrderID" :orderId,        
    };
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
}


let listProgramDetails = function(token, companyId, corporateId){

    let url = (Config.omsBaseUrl + Endpoint['LIST_PROGRAM_DETAILS']).replace(':companyID:', companyId).replace(':corpID:', corporateId).replace(':token:',token);   
         
    let inputObj = {             
    };
    
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
}

let getPointBasedPInvoice = function(token, companyId, corporateId ,orderId,programID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_POINT_BASED_PERFORMA_INVOICE']).replace(':token:', token);
        let inputObj = {           
            "corpID" :corporateId,
            "companyID" :companyId,
            "fileUploadID" :orderId,     
            "programID": programID,      
        };
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
};

module.exports = {
    checkAgreement: checkAgreement,
    getPayoutsOrders: getPayoutsOrders,
    getSelectGiftingDetails: getSelectGiftingDetails,
    getFileUploadDetails: getFileUploadDetails,
    getSelectGiftingOrderList: getSelectGiftingOrderList,
    getProgramDetails: getProgramDetails,
    getSelectGiftingInvoice:getSelectGiftingInvoice,
    getSelectGiftingPInvoice:getSelectGiftingPInvoice,
    getTemplateDesign:getTemplateDesign,
    //virtual
    getVirtualTemplate:getVirtualTemplate,
    getVirtualGiftingInvoice,
    getVirtualGiftingProformaInvoice,
    getPhysicalCardProformaInvoice,
    listProgramDetails,
    getPointBasedPInvoice
};
var Config = require('../../config');
var Constant = require('../common/constant');
var Util = require('../common/util');
var Endpoint = require('../common/url');
var ServiceConnector = require('../common/serviceConnector');

let getFlagsForCompany = function(authToken, userId, corpID, companyID) {
    let url = (Config.omsBaseUrl + Endpoint['GET_GATEKEEPERS'])
                .replace(':authToken:', authToken);
    let request = {
        "listID": "CORP_GKS",
        "userJID": userId + "@zetauser.zeta.in",
        "userAgent": {
            "corpID": corpID,
            "companyID": companyID
        }
    };
    let headers = {
        "Content-Type": "application/json"
    };
    
    return ServiceConnector.post(url, request, headers);
}

module.exports = {
    getFlagsForCompany: getFlagsForCompany
};

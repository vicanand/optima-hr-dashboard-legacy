var Config = require('../../../config'),
    Endpoint = require('../../common/url'),
    ServiceConnector = require('../../common/serviceConnector'),
    Promise = require('bluebird');


var getCafeStatistics = function (authToken, params) {

    return new Promise(function (resolve, reject) {

        var url = (Config.omsBaseUrl + Endpoint['GET_CAFETERIA_INSIGHTS_SNAPSHOT'])
            .replace(':authToken:', authToken);

        var inputObj = params;
        var headers = {
            "Content-Type": "application/json"
        };
        ServiceConnector.post(url, inputObj, headers).then(function (res) {
            var respData = res
            resolve(respData);
        }, function (error) {
            reject(error); 
        });

    });
};


var getVendorsTrends = function (authToken, queryParams) {
    return new Promise(function (resolve, reject) {

        var url = (Config.omsBaseUrl + Endpoint['GET_VENDOR_TRENDS']);


        var headers = {
            "Content-Type": "application/json",          
            "X-Zeta-AuthToken": authToken
        };
        ServiceConnector.post(url, queryParams, headers).then(function (res) {
            var respData = res;
            resolve(respData);
        }, function (error) {
            reject(error);
        });

    });
};

var getStoreTrends = function (authToken, queryParams) {
    return new Promise(function (resolve, reject) {

        var url = (Config.omsBaseUrl + Endpoint['GET_STORE_TRENDS']);

        var headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };
        ServiceConnector.post(url, queryParams, headers).then(function (res) {
            var respData = res;
            resolve(respData);
        }, function (error) {
            reject(error);
        });
    });
};


var getCitiesOfficeStore = function (authToken, queryParam) {
    return new Promise(function (resolve, reject) {

        var url = (Config.omsBaseUrl + Endpoint['GET_ALL_STORES']);

        var headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };
        ServiceConnector.get(url, queryParam, headers).then(function (res) {
            var respData = JSON.parse(res);
            resolve(respData);
        }, function (error) {
            reject(error);
        });

    });
};

var getSalesTransactionsMetrics = function (authToken, queryParam) {
    return new Promise(function (resolve, reject) {

        var url = (Config.omsBaseUrl + Endpoint['GET_SALES_TRANSACTION']);

        var headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };
        ServiceConnector.get(url, queryParam, headers).then(function (res) {
            var respData = JSON.parse(res);
            resolve(respData);
        }, function (error) {
            reject(error);
        });

    });
};


var getSalesFootfall = function (authToken, queryParam) {
    return new Promise(function (resolve, reject) {


        var url = (Config.omsBaseUrl + Endpoint['GET_FOOT_FALL']);

        var headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };
        ServiceConnector.get(url, queryParam, headers).then(function (res) {
            var respData = JSON.parse(res);
            resolve(respData);
        }, function (error) {
            reject(error);
        });

    });
};


var getVendors = function (authToken, queryParam) {
    return new Promise(function (resolve, reject) {

        var url = (Config.omsBaseUrl + Endpoint['GET_VENDORS']);

        var headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };
        ServiceConnector.post(url, queryParam, headers).then(function (res) {
            var respData = res;
            resolve(respData);
        }, function (error) {
            reject(error);
        });

    });
};

var getStoresByBusiness = function (authToken, queryParam) {
    return new Promise(function (resolve, reject) {

        var url = (Config.omsBaseUrl + Endpoint['GET_STORES_IN_BUSINESS_FOR_CORPORATE']);

        var headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };
        ServiceConnector.get(url, queryParam, headers).then(function (res) {
            var respData = res;
            resolve(respData);
        }, function (error) {
            reject(error);
        });

    });
};


module.exports = {
    getCafeStatistics: getCafeStatistics,
    getVendorsTrends: getVendorsTrends,
    getStoreTrends: getStoreTrends,
    getCitiesOfficeStore: getCitiesOfficeStore,
    getSalesTransactionsMetrics: getSalesTransactionsMetrics,
    getSalesFootfall: getSalesFootfall,
    getVendors: getVendors,
    getStoresByBusiness: getStoresByBusiness
};
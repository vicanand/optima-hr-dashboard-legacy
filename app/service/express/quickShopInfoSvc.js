var Config = require('../../../config'),
    Endpoint = require('../../common/url'),
    ServiceConnector = require('../../common/serviceConnector');


var getQuickshopInfo = function (authToken, params) {
    return new Promise(function (resolve, reject) {
        var url = (Config.omsBaseUrl + Endpoint['EXPRESS_GET_QUICKSHOP_INFO']);

        var inputObj = params;
        var headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };


        ServiceConnector.get(url, inputObj, headers).then(function (respData) {
           resolve(respData);
        }, function (error) {
            reject(error);
        });

    });
};

var getQuickshopInfoDate = function (authToken, params) {
    return new Promise(function (resolve, reject) {
        var url = (Config.omsBaseUrl + Endpoint['EXPRESS_GET_QUICKSHOP_INFO_DATE']);

        var inputObj = params;
        var headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };


        ServiceConnector.get(url, inputObj, headers).then(function (respData) {
            resolve(respData);
        }, function (error) {
            reject(error);
        });

    });
};





var getListOfUploadedMenus = function (authToken, params) {
    return new Promise(function (resolve, reject) {
        var url = (Config.omsBaseUrl + Endpoint['GET_LIST_OF_UPLOAD_MENUS']);

        var inputObj = params;
        var headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };


        ServiceConnector.get(url, inputObj, headers).then(function (respData) {
            resolve(JSON.parse(respData));
        }, function (error) {
            reject(error);
        });
    });
};

var getVendorInsightsSnapshot = function (authToken, params) {
    return new Promise(function (resolve, reject) {
        var url = (Config.omsBaseUrl + Endpoint['GET_VENDOR_INSIGHTS_SNAPSHOT']);

        var inputObj = params;
        var headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };


        ServiceConnector.get(url, inputObj, headers).then(function (respData) {
            resolve(JSON.parse(respData));
        }, function (error) {
            reject(error);
        });

    });
};

var getItemTrends = function (authToken, params) {
    return new Promise(function (resolve, reject) {
        var url = (Config.omsBaseUrl + Endpoint['GET_ITEM_TRENDS']);

        var inputObj = params;
        var headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };


        ServiceConnector.get(url, inputObj, headers).then(function (respData) {
            resolve(JSON.parse(respData));
        }, function (error) {
            reject(error);
        });

    });
};


var getItemConsumptions = function (authToken, params) {
    return new Promise(function (resolve, reject) {
        var url = (Config.omsBaseUrl + Endpoint['GET_ALL_ITEMS']);

        var inputObj = params;
        var headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };


        ServiceConnector.get(url, inputObj, headers).then(function (respData) {
            resolve(JSON.parse(respData));
        }, function (error) {
            reject(error);
        });

    });
};




module.exports = {
    getQuickshopInfo,
    getQuickshopInfoDate,
    getVendorInsightsSnapshot,
    getItemTrends,
    getItemConsumptions,
    getListOfUploadedMenus
};
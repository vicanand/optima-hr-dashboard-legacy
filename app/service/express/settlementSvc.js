var Config = require('../../../config'),
    Endpoint = require('../../common/url'),
    ServiceConnector = require('../../common/serviceConnector'),
    Promise = require('bluebird');


var getSettlementsForCounter = function (authToken, queryParam) {
    return new Promise(function (resolve, reject) {

        var url = (Config.omsBaseUrl + Endpoint['GET_SETTLEMENT_FOR_COUNTER']);

        var headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };
        ServiceConnector.get(url, queryParam, headers).then(function (res) {
            var respData = JSON.parse(res);
            resolve(respData);
        }, function (error) {
            reject(error);
        });

    });
};

var getSettlementsMetrics = function(authToken, queryParam){
    return new Promise(function (resolve, reject) {
        
                var url = (Config.omsBaseUrl + Endpoint['GET_SETTLEMENT_METRICS']);
        
                var headers = {
                    "Content-Type": "application/json",
                    "X-Zeta-AuthToken": authToken
                };
                ServiceConnector.get(url, queryParam, headers).then(function (res) {
                    var respData = JSON.parse(res);
                    resolve(respData);
                }, function (error) {
                    reject(error);
                });
        
            });
}
module.exports = {
    getSettlementsForCounter,
    getSettlementsMetrics
};
var Config = require('../../../config'),
    Endpoint = require('../../common/url'),
    ServiceConnector = require('../../common/serviceConnector'),
    Promise = require('bluebird');


var getEmployeeStatistics = function (authToken, params) {
    console.log('params: ', params);

    return new Promise(function (resolve, reject) {
        var url = (Config.omsBaseUrl + Endpoint['GET_EMPLOYEE_INSIGHTS_SNAPSHOT'])
            .replace(':authToken:', authToken);

        var inputObj = params;
        var headers = {
            "Content-Type": "application/json"
        };
        ServiceConnector.get(url, inputObj, headers).then(function (res) {
            var respData = JSON.parse(res);
            resolve(respData);
        }, function (error) {
            reject(error);
        });
    });
};


var getPaymentPreference = function (authToken, params) {

    return new Promise(function (resolve, reject) {
        var url = (Config.omsBaseUrl + Endpoint['GET_PAYMENT_PREFERENCE']);

        var inputObj = params; 
        var headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };
        ServiceConnector.post(url, inputObj, headers).then(function (res) {
            var respData = res;
            resolve(respData);
        }, function (error) {
            reject(error);
        });
    });
};


var getEmployeeInsightsTransactionDetails = function (authToken, params) {

    return new Promise(function (resolve, reject) {
        var url = (Config.omsBaseUrl + Endpoint['GET_EMPLOYEE_TRANSACTION_DETAILS']);

        var inputObj = params;
        var headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };
        ServiceConnector.get(url, inputObj, headers).then(function (res) {
            var respData = JSON.parse(res);
            resolve(respData);
        }, function (error) {
            reject(error);
        });
    });
};


module.exports = {
    getEmployeeStatistics: getEmployeeStatistics,
    getPaymentPreference: getPaymentPreference,
    getEmployeeInsightsTransactionDetails: getEmployeeInsightsTransactionDetails
};
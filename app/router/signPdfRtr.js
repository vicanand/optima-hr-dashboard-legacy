var express = require('express'),
    router = express.Router(),
    SignPdfControlller = require('../controller/signPdfCtrl');
RestApi = require('../common/restApi');

router.get('/signPdf', SignPdfControlller.init);

module.exports = function(app) {
    app.use('/', router);
};
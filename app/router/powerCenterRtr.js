var express = require('express'),
router = express.Router(),
PowerCenterController = require('../controller/powerCenterCtrl');

router.get('/power-center', PowerCenterController.init);

module.exports = function (app) {
    app.use('/', router);
};

var express = require('express'),
    router = express.Router(),
    DocumentController = require('../controller/documentCtrl');

router.get('/documents-drive', DocumentController.init);
router.get('/document-drive/:docGroupID', DocumentController.details);

module.exports = function(app) {
    app.use('/', router);
};

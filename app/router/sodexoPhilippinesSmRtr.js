var express = require('express'),
    router = express.Router(),
    SodexoPhSmController = require('../controller/sodexoPhilippinesSmCtrl');

router.get('/companies/:companyId/sm-corp/:page', SodexoPhSmController.init);

module.exports = function(app) {
    app.use('/', router);
};

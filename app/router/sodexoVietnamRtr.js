var express = require('express'),
    router = express.Router(),
    SodexoVietnamController = require('../controller/sodexoVietnamCtrl');

router.get('/vn/report-center', SodexoVietnamController.init);

module.exports = function(app) {
    app.use('/', router);
};

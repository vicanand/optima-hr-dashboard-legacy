var express = require('express'),
    router = express.Router(),
    EmployeeController = require('../controller/employeeCtrl');

router.get('/beneficiaries', EmployeeController.init);
router.get('/beneficiaries/*', EmployeeController.employeeDetailsV2);

module.exports = function(app) {
    app.use('/', router);
};
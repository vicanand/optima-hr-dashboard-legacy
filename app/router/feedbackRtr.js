var express = require('express'),
router = express.Router(),
FeedbackController = require('../controller/feedbackCtrl');


router.get('/feedback', FeedbackController.init);



module.exports = function (app) {
    app.use('/', router);
};
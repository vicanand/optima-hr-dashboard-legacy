var express = require('express'),
    router = express.Router(),
    BenefitController = require('../controller/benefitCtrl');
var SpotlightController = require('../controller/spotlightCtrl');

router.get('/companies/:companyId/optima', BenefitController.init);
router.get('/companies/:companyId/spotlight', SpotlightController.init);
router.get('/companies/:companyId/optimaPrograms', BenefitController.getBenefitPrograms);
router.get('/companies/:companyId/optima/cardorder', BenefitController.getSuperCardOrder);
router.get('/companies/:companyId/optima/cardorder/order/:superCardID/view/cardDetails/', BenefitController.getSuperCardOrderDetails);
router.get('/companies/:companyId/loadActivePrograms/:pageNumber', BenefitController.getBenefitMoreActivePrograms);
router.get('/companies/:companyId/loadClosedPrograms/:pageNumber', BenefitController.getBenefitMoreClosedPrograms);
router.get('/companies/:companyId/optima/create-custom-program', BenefitController.createCustomProgram);
router.get('/companies/:companyId/optima/:cardProgramId', BenefitController.getBenefitDetails);
router.get('/companies/:companyId/optima/:cardProgramId/new-transfer', BenefitController.getNewTransfer);
router.get('/companies/:companyId/optima/:cardProgramId/transfers', BenefitController.getTransfers);
router.get('/companies/:companyId/optima/:cardProgramId/transfers/:orderId', BenefitController.getOrderTransfers);
router.get('/companies/:companyId/optima/:cardProgramId/transfers/:orderId/errors', BenefitController.getOrderErrors);
router.get('/optima/:companyId/setup-program/:productType', BenefitController.setupProgram);
router.get('/companies/:companyId/optima/:cardProgramId/transfers/:orderId/addPayoutTransfer', BenefitController.addPayoutTransfer);

module.exports = function (app) {
    app.use('/', router);
};
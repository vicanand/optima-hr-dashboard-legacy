var express = require('express'),
    router = express.Router(),
    BaseImageController = require('../controller/baseImageCtrl');

router.get('/baseimage', BaseImageController.init);

module.exports = function(app) {
    app.use('/', router);
};

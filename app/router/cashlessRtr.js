var express = require('express'),
    router = express.Router(),
    CashlessController = require('../controller/cashlessCtrl');

router.get('/companies/:companyId/cashless', CashlessController.init);
router.get('/companies/:companyId/cashless/:cardProgramId', CashlessController.getCashlessDetails);
router.get('/companies/:companyId/cashless/:cardProgramId/new-transfer', CashlessController.getNewTransfer);
router.get('/companies/:companyId/cashless/:cardProgramId/transfers/:orderId', CashlessController.getOrderTransfers);
router.get('/companies/:companyId/cashless/:cardProgramId/transfers', CashlessController.getTransfers);



module.exports = function(app) {
    app.use('/', router);
};

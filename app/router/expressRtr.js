var express = require('express'),
    router = express.Router(),
    ExpressController = require('../controller/expressCtrl'),
    ExpressVendorsCtrl = require('../controller/expressVendorsCtrl'),
    ExpressEmployeeCtrl = require('../controller/expressEmployeeCtrl'),
    ExpressSalesCtrl = require('../controller/expressSales'),
    ExpressSettlement = require('../controller/expressSettlements'),
    ExpressMenu = require('../controller/expressMenu'),
    ExpressItems = require('../controller/expressItems');

router.get('/express', ExpressController.init);
router.get('/express/insight/cafeteria/overview', ExpressController.cafeteriaInsightOverview);
router.get('/express/insight/cafeteria/menu', ExpressController.cafeteriaInsightMenu);


router.get('/express/insight/vendors', ExpressVendorsCtrl.init);
router.get('/express/insight/employee', ExpressEmployeeCtrl.init);

router.get('/express/insight/vendor/:businessID/overview', ExpressVendorsCtrl.vendorOverview);
router.get('/express/insight/vendor/:businessID/sales', ExpressSalesCtrl.employeeSales);
router.get('/express/insight/vendor/:businessID/settlement', ExpressSettlement.employeeSettlements);
router.get('/express/insight/vendor/:businessID/menu', ExpressMenu.vendorMenu);
router.get('/express/insight/vendor/:businessID/item-trends', ExpressItems.getItems);
router.get('/express/insight/vendor/:businessID/item-consumption', ExpressItems.getItemsConsumption);


router.post('/express/getVendorsTrends', ExpressController.getVendorsTrends);
router.post('/express/getStoreTrends', ExpressController.getStoreTrends);
router.get('/express/getQuickshopForDate', ExpressController.getQuickshopInfoDate);
router.get('/express/getQuickshopInfo', ExpressController.getQuickshopInfo);


router.get('/express/getEmployeeSnapshotStats', ExpressEmployeeCtrl.getEmployeeStats);
router.post('/express/getPaymentPreference', ExpressEmployeeCtrl.getPaymentPreference);
router.get('/express/getEmployeeInsightsTransactionDetails', ExpressEmployeeCtrl.getEmployeeInsightsTransactionDetails);
router.get('/express/getSalesTransactionsMetrics', ExpressSalesCtrl.getSalesTransactionsMetrics);
router.get('/express/getSalesFootfall', ExpressSalesCtrl.getSalesFootfall);
router.get('/express/getSettlementsForCounter', ExpressSettlement.getSettlementsForCounter);
router.get('/express/getSettlementMetrics',ExpressSettlement.getSettlementMetrics);
router.post('/express/getVendors', ExpressVendorsCtrl.getVendors);
router.get('/express/getStoresByBusiness', ExpressVendorsCtrl.getStoresByBusiness);
router.get('/express/getItemTrends', ExpressItems.getItemTrends);
router.get('/express/getItemConsumptions', ExpressItems.getItemConsumptionsData);
router.get('/express/getListOfUploadedMenus', ExpressController.getListOfUploadedMenus);
router.post('/express/getCafeStatistics', ExpressController.getCafeStatistics);
router.post('/express/insight/cafeteria/express/getCafeStatistics', ExpressController.getCafeStatistics);                            
router.get('/express/getVendorInsightsSnapshot',ExpressVendorsCtrl.getVendorInsightsSnapshot);


module.exports = function (app) {
    app.use('/', router);
};

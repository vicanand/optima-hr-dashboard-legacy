var express = require('express'),
router = express.Router(),
SpotlightController = require('../controller/spotlightCtrl');
var Constant = require('../common/constant');
var RestApi = require('../common/restApi');


router.get('/companies/:companyId/spotlight', SpotlightController.init);
router.get('/companies/:companyId/spotlight/reward', SpotlightController.getRewardDetails);
router.get('/companies/:companyId/spotlight/virtual', SpotlightController.getVirtualDetails);
router.get('/companies/:companyId/spotlight/physical', SpotlightController.getPhysicalDetails);
router.get('/companies/:companyId/spotlight/physical/issuegift', SpotlightController.issuePhysicalCard);
router.get('/companies/:companyId/spotlight/physical/payout', SpotlightController.payOutPhysicalCard);
router.get('/companies/:companyId/spotlight/physical/ordercard', SpotlightController.orderPhysicalCard);


// RnR Routers
router.get('/companies/:companyId/spotlight/pointbased', SpotlightController.getPointBased); //new
router.get('/companies/:companyId/spotlight/pointbased/employee/setup', SpotlightController.getPointBasedProgram); // new
router.get('/companies/:companyId/spotlight/pointbased/employee/:programID/:programName/pointsprogram', SpotlightController.getPointBasedProgramDetails);//new
router.get('/companies/:companyId/spotlight/pointbased/employee/:programID/:programName/upload/beneficairy', SpotlightController.uploadPointBasedBeneficary); //new
router.get('/companies/:companyId/spotlight/pointbased/employee/:programID/:programName/upload/issuepoints', SpotlightController.uploadPointBasedissue); //new
router.get('/companies/:companyId/spotlight/pointbased/employee/:programID/:orderId/proforma-invoice', SpotlightController.PointBasedPInvoice); //new
router.get('/companies/:companyId/spotlight/pointbased/employee/:programID/:programName/:fileUploadOrderID/:updatedAt/:totalBeneficiaries/:orderAmount/:status/orders', SpotlightController.pointBasedSinglePayoutDetails); //new

router.get('/companies/:companyId/spotlight/physical/order/:orderID/view/:amount/:orderStatus/:totalPayouts', SpotlightController.getPhysicalIndividualOrderDetails);
router.get('/companies/:companyId/spotlight/physical/invoice/:orderID/view', SpotlightController.getVirtualGiftInvoice);
router.get('/companies/:companyId/spotlight/physical/proforma-invoice/:orderId/view', SpotlightController.getVirtualGiftProformaInvoice);
router.get('/companies/:companyId/spotlight/physical/cardcost-proforma-invoice/:orderId/view', SpotlightController.getPhysicalCardGiftProformaInvoice);
router.get('/companies/:companyId/spotlight/virtual/order/:orderID/view/:amount/:orderStatus/:totalPayouts', SpotlightController.getVirtualIndividualOrderDetails);
router.get('/companies/:companyId/spotlight/virtual/invoice/:orderID/view', SpotlightController.getVirtualGiftInvoice);
router.get('/companies/:companyId/spotlight/virtual/proforma-invoice/:orderId/view', SpotlightController.getVirtualGiftProformaInvoice);
router.get('/companies/:companyId/spotlight/reward/:orderId', SpotlightController.getSingleTransferDetails);
router.get('/companies/:companyId/spotlight/reward/:cardProgramId/transfers/:orderId/errors', SpotlightController.getOrderErrorFound);
router.get('/companies/:companyId/spotlight/instagift/:cardProgramId', SpotlightController.getSpotlightDetails);
router.get('/companies/:companyId/spotlight/:cardProgramId', SpotlightController.getCashIncentiveDetails);
router.get('/companies/:companyId/spotlight/:cardProgramId/new-transfer', SpotlightController.getNewTransfer);
router.get('/companies/:companyId/spotlight/:cardProgramId/transfers/:orderId', SpotlightController.getOrderTransfers);
router.get('/companies/:companyId/spotlight/:cardProgramId/transfers/:orderId/errors', SpotlightController.getOrderErrors);
router.get('/companies/:companyId/spotlight/:cardProgramId/transfers', SpotlightController.getTransfers);
router.get('/companies/:companyId/spotlight/instagift/:cardProgramId/load-cards', SpotlightController.loadCards);
//router.get('/companies/:companyId/spotlight/instagift/:cardProgramId/order-cards', SpotlightController.orderCards);
router.get('/companies/:companyId/spotlight/instagift/:cardProgramId/transfers/:orderId', SpotlightController.getPayoutOrders);
router.get('/companies/:companyId/spotlight/instagift/:cardProgramId/faq', SpotlightController.getInstagiftFaqs);
router.get('/companies/:companyId/spotlight/instagift/:cardProgramId/control-center', SpotlightController.getInstagiftControlCenter);
router.get('/companies/:companyId/spotlight/selectgifting/newselectgifting', SpotlightController.getnewSelectGifting);
router.get('/companies/:companyId/spotlight/selectgifting/newgiftorder', SpotlightController.getSelectGiftingDetails);
router.get('/fetchinggift', SpotlightController.fetchingGiftingDetails);
router.get('/companies/:companyId/spotlight/selectgifting/invoice/:orderId', SpotlightController.getnewInvoice);
router.get('/companies/:companyId/spotlight/selectgifting/proforma-invoice/:orderId', SpotlightController.getnewPInvoice);
router.get('/companies/:companyId/spotlight/selectgifting/newgiftorder/setSelectedFundingAcc', RestApi.setSelectedFundingAcc);


module.exports = function(app) {
app.use('/', router);
};
var express = require('express'),
router = express.Router(),
employeeOnBoardCtrl= require('../controller/employeeOnboardCtrl');

router.get('/employee-onboard', employeeOnBoardCtrl.init);

module.exports = function (app) {
    app.use('/', router);
};
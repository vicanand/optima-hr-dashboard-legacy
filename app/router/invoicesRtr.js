var express = require('express'),
    router = express.Router(),
    InvoiceController = require('../controller/invoiceCtrl');
RestApi = require('../common/restApi');

router.get('/invoices-commercials', InvoiceController.invoicesAndCommercials);

module.exports = function(app) {
    app.use('/', router);
};
var express = require('express'),
  router = express.Router(),
  BaseImageController = require('../controller/employeeStementCtrl');

router.post('/employeestatement', BaseImageController.init);

module.exports = function (app) {
  app.use('/', router);
};

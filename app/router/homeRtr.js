var express = require('express'),
    router = express.Router(),
    HomeController = require('../controller/homeCtrl'),
    BenefitController = require('../controller/benefitCtrl'),
    DashboardController = require('../controller/dashboardCtrl'),
    RestApi = require('../common/restApi');

router.get('/', DashboardController.init);
router.get('/genie', DashboardController.genieInit);
router.get('/logout', HomeController.logout);
// router.get('/renew', HomeController.renew);
router.get('/session', HomeController.displaySession);
router.get('/setSelectedCompanyID', RestApi.setSelectedCompanyID);
router.get('/createProgram', RestApi.createProgram);
router.get('/createNewFundAndProgram', RestApi.updateCardFundingAccount);
router.get('/changeFundAcc', RestApi.updateFundAcc);
router.get('/createCashIncentiveProgram', RestApi.createCashIncentiveProgram);
router.get('/createSelectGiftingProgram', RestApi.createSelectGiftingProgram);
router.post('/verifyNewCorporateDetails', RestApi.verifyNewCorporateDetails);
router.get('/createCashlessProgram', RestApi.createCashlessProgram);
router.get('/login/:country', HomeController.redirectToSSO);
router.get('/email', HomeController.verifyEmail);
router.get('/signup', HomeController.signUp);
router.get('/hdfcSignup', HomeController.hdfcSignup);
router.get('/vn-signup', HomeController.vietnamSignup);
router.get('/signin', HomeController.signIn);
router.get('/verifyEmail', HomeController.emailVerification);
router.get('/checkAuthTokenExist', RestApi.checkAuthTokenExist);
router.get('/dashboard', DashboardController.init);
router.get('/companies', DashboardController.init);
router.get('/companies/:companyId', DashboardController.init);
router.get('/account/link', HomeController.verifyJwt);
router.get('/account/corpDetails', HomeController.corpDetails);
router.get('/setSelectedCorporatesID', RestApi.setSelectedCorporateID);
router.get('/account/noAccount', HomeController.noCorpAccount);
router.get('/account/inactiveAccount', HomeController.inactiveCorpAccount);
router.get('/account/corporateSwitch', HomeController.corporateSwitch);
router.post('/signin-corp', HomeController.signUpCorp);

module.exports = function(app) {
    app.use('/', router);
};
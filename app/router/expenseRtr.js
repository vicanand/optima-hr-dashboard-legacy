var express = require("express"),
  router = express.Router(),
  ExpenseController = require("../controller/expenseCtrl");
RubixController = require("../controller/rubixCtrl");
var Constant = require("../common/constant");

router.get("/companies/:companyId/expense", ExpenseController.init);
router.get(
  "/companies/:companyId/expense/:cardprogramId",
  ExpenseController.getExpenseDetails
);

router.get('/companies/:companyId/rubix/hcr', RubixController.hcrInit);
router.get('/companies/:companyId/rubix/cps', RubixController.cpsInit);
router.get('/companies/:companyId/rubix/:page', RubixController.init);

module.exports = function(app) {
  app.use("/", router);
};

var express = require('express'),
    router = express.Router(),
    AdminController = require('../controller/adminCtrl');
RestApi = require('../common/restApi');

router.get('/admin', AdminController.init);
router.get('/admin/companies', AdminController.companies);
router.get('/admin/companies/*', AdminController.companyDetails);
router.get('/admin/funding-accounts', AdminController.fundingAccHome);
router.get('/admin/transaction-history/*', AdminController.getFundingAccount);
router.get('/admin/funding-accounts/add-funds-details', AdminController.addFundsToAccount);
router.get('/admin/transaction-history-legacy', AdminController.getLegacyFundingAcc);
router.get('/admin/access-control', AdminController.accessCtrlHome);
router.get('/admin/access-controlV2', AdminController.accessCtrlV2Home);
router.get('/programsFunded', RestApi.programsFunded);
router.get('/setSelectedFundingAcc', RestApi.setSelectedFundingAcc);


module.exports = function(app) {
    app.use('/', router);
};
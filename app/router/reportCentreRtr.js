var express = require('express'),
    router = express.Router(),
    ReportCentreController = require('../controller/reportCentreCtrl');

router.get('/report-centre', ReportCentreController.init);

router.post('/report-centre/report-list', ReportCentreController.getReportList);

router.get('/report-centre/report-details/:reportId', ReportCentreController.reportDetails);

module.exports = function(app) {
    app.use('/', router);
};

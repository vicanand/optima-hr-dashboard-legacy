var express = require('express'),
    router = express.Router(),
    BulkActionController = require('../controller/bulkActionCtrl'),
    BulkCloseController = require('../controller/bulkCloseCtrl'),
    RestApi = require('../common/restApi');

router.get('/companyCodes', RestApi.companyCodes);
router.get('/bulk-action', BulkActionController.init);
router.get('/bulk-action/orders', BulkActionController.getBulkOrders);
router.get('/bulk-action/orders/:bulkOrderID', BulkActionController.getIndividualOrdersInBulkOrder);
router.get('/bulk-action/new-transfer', BulkActionController.newBulkOrder);
router.get('/bulk-action/edit-company-code', BulkActionController.companyCodeEdit);

router.get('/bulk-close/new-transfer', BulkCloseController.newBulkOrder);
router.get('/bulk-close/orders', BulkCloseController.getBulkOrders);

module.exports = function(app) {
    app.use('/', router);
};

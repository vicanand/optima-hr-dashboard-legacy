var express = require('express'),
router = express.Router(),
SurveyController = require('../controller/surveyCtrl');

router.get('/survey', SurveyController.init);

module.exports = function (app) {
    app.use('/', router);
};

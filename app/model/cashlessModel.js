module.exports = {
    init: function() {
        let cashlessPrograms = [{
            "type": "CashlessCafeteria",
            "programName": "",
            "programDescription": "Issue food allowance digitally to your employees. Wow them with fast and secure digital payments at your cafeteria. Enable vendors to track their sales and get feedback. Settle with vendors transparently based on rich Zeta reports. Set it up now!",
            "cardDesign": {
                "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                "bg": "https://card-program-files.s3.amazonaws.com/cashless_cafeteria_new.jpeg",
                "bgColor": "",
                "offerColor": "#FFFFFF",
                "textColor": "",
                "cardName": ""
            },
            "helpPage": "https://zeta.in/express",
            "taxSaving": {
                "amount": "", // in paise
                "PERIOD": ""
            }
        }];
        return cashlessPrograms;
    }
}

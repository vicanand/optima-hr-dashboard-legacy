module.exports = {
    init: function() {
        let recommondedPrograms = [{
                "type": "meal",
                "programName": "Meal Vouchers",
                "programDescription": "Help your employees save tax on the purchase of food and non-alcoholic beverages with Zeta Meal Vouchers. These 100% compliant digital meal vouchers offer the widest acceptance through our 3 lakh merchant network.",
                "cardDesign": {
                    "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                    "bg": "https://card-program-files.s3.amazonaws.com/meal_new.jpeg",
                    "bgColor": "",
                    "offerColor": "#FFFFFF",
                    "textColor": "",
                    "cardName": ""
                },
                "helpVideoURL": "https://www.youtube.com/watch?v=YfahiewmzPk",
                "helpPage": "http://zeta.in/#meal-vouchers",
                "taxSaving": {
                    "amount": "792000", // in paise
                    "PERIOD": "YEARLY"
                }
            }, {
                "type": "medical",
                "programName": "Medical Reimbursements",
                "programDescription": "Digitise your company’s medical reimbursements programme with Zeta Medical Reimbursements and go paper-free. Outsource bill verifications and get access to signed audit-ready digital reports and statements.",
                "cardDesign": {
                    "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                    "bg": "https://card-program-files.s3.amazonaws.com/medical_new.jpeg",
                    "bgColor": "",
                    "offerColor": "#FFFFFF",
                    "textColor": "",
                    "cardName": ""
                },
                "helpPage": "http://zeta.in/#medical-reimbursements",
                "taxSaving": {
                    "amount": "450000", // in paise
                    "PERIOD": "YEARLY"
                }
            }, {
                "type": "gift",
                "programName": "Gift Card",
                "programDescription": "Send gifts in a click! Zeta Gift Cards can help your employees save taxes on up to Rs 5000 a year. Your employees also get the added benefit of being able to spend anywhere through the Zeta app and the Zeta Super Card<sup>&reg;</sup>.",
                "cardDesign": {
                    "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                    "bg": "https://card-program-files.s3.amazonaws.com/gift_Card_new.jpeg",
                    "bgColor": "",
                    "offerColor": "#FFFFFF",
                    "textColor": "",
                    "cardName": ""
                },
                "helpPage": "http://zeta.in/#gift-cards",
                "taxSaving": {
                    "amount": "150000", // in paise
                    "PERIOD": "YEARLY"
                }
            }, {
                "type": "asset",
                "programName": "Gadget Card",
                "programDescription": "Keep your employees at the cutting-edge of tech and help them save on taxes too. Purchase of smartphones, tablets and computers with the Zeta Gadget Card is 100% tax free.",
                "cardDesign": {
                    "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                    "bg": "https://card-program-files.s3.amazonaws.com/gadget_new.jpeg",
                    "bgColor": "",
                    "offerColor": "#FFFFFF",
                    "textColor": "",
                    "cardName": ""
                },
                "helpPage": "http://zeta.in/#gadget-cards",
                "taxSaving": {
                    "amount": "1800000", // in paise
                    "PERIOD": "YEARLY"
                }
            }, {
                "type": "communication",
                "programName": "Communications Card",
                "programDescription": "Talking can help your employees save tax. This digital solution can be customised to include multiple prepaid and postpaid mobile connections, data card and broadband bills. It saves HR managers from the hassles of bill verifications and  statement preparations by outsourcing them to Zeta.",
                "cardDesign": {
                    "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                    "bg": "https://card-program-files.s3.amazonaws.com/communication_new.jpeg",
                    "bgColor": "",
                    "offerColor": "#FFFFFF",
                    "textColor": "",
                    "cardName": ""
                },
                "helpPage": "https://zeta.in/#communication-cards",
                "taxSaving": {
                    "amount": "900000", // in paise
                    "PERIOD": "YEARLY"
                }
            }, {
                "type": "fuel",
                "programName": "Fuel & Travel Card",
                "programDescription": "Manage your employees fuel, car maintenance, insurance and travel allowances with one digital programme. No longer restricted to one brand of fuel, your employees can rejoice too.",
                "cardDesign": {
                    "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                    "bg": "https://card-program-files.s3.amazonaws.com/fuel_new.jpeg",
                    "bgColor": "",
                    "offerColor": "#FFFFFF",
                    "textColor": "",
                    "cardName": ""
                },
                "helpPage": "http://zeta.in/#fuel-reimbursements",
                "taxSaving": {
                    "amount": "864000", // in paise
                    "PERIOD": "YEARLY"
                }
            }, {
                "type": "driver_salary",
                "programName": "Driver Salary Card",
                "programDescription": "Manage your employees driver salary reimbursements. Employees can commute in chauffer driven cars avoiding the stress of traffic and show up to work fresh and energetic. What's more, they also save tax on this by uploading the receipt of driver salary.",
                "cardDesign": {
                    "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                    "bg": "https://card-program-files.s3.amazonaws.com/driver_salary_card.jpg",
                    "bgColor": "",
                    "offerColor": "#FFFFFF",
                    "textColor": "",
                    "cardName": ""
                },
                "helpPage": "http://zeta.in/#fuel-reimbursements",
                "taxSaving": {
                    "amount": "324000", // in paise
                    "PERIOD": "YEARLY"
                }
            },
            {
                "type": "LTA",
                "programName": "LTA Card",
                "programDescription": "Help your employees take vacations with their family so that they come back rejuvenated and also get tax benefits on their travel. Zeta takes care of all the complex calculations and verification so that your employees' vacation is relaxing for them (and for you).",
                "cardDesign": {
                    "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                    "bg": "https://card-program-files.s3.amazonaws.com/LTA_card_new.jpeg",
                    "bgColor": "",
                    "offerColor": "#FFFFFF",
                    "textColor": "",
                    "cardName": ""
                },
                "helpPage": "https://zeta.in/#lta-card",
                "taxSaving": {
                    "amount": "3000000", // in paise
                    "PERIOD": "YEARLY"
                }
            },
            {
               "type": "uniform",
               "programName": "Uniform Card",
               "programDescription": "Digitise your company’s Uniform Reimbursements programme with Zeta Uniform Card and go paper-free. Outsource bill verifications and get access to signed audit-ready digital reports and statements.",
               "cardDesign": {
                   "logo": "https://card-program-files.s3.amazonaws.com/transparent.png",
                   "bg": "https://corp-ben-hr-files.s3.amazonaws.com/app_uniformCard.png",
                   "bgColor": "",
                   "offerColor": "#FFFFFF",
                   "textColor": "",
                   "cardName": ""
               },
               "helpPage": "https://zeta.in",
               "taxSaving": {
                   "amount": "237600", // in paise
                   "PERIOD": "YEARLY" 
               }
           },
        ];
        return recommondedPrograms;
    }
}

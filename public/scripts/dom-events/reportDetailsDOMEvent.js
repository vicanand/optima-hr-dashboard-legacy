import ReportCentreSvc from '../service/reportCentreSvc.js';
import Utils from '../common/util';


let createTable, table, metaData = "";
createTable = function (url) {
    $(document).ready(function () {
        $.ajax({
            type: "GET",
            url: url,
            dataType: "text",
            success: function (data) {
                processData(data, url);
            },
            error: function (textStatus, errorThrown) {
                
            }
        });
    });

    function processData(allText, url) {

        let allTextLines = allText.split(/\r\n|\n/);
        let lines = [];
        let column = [];
        let headerIndex = 0;
        let reg = /^(.)\1{1,}$/

        allTextLines.every((i, j) => {
            if(
                reg.test(i))
            { headerIndex = j+1;
                return false;
            }
            return true;
        });

        // let  = "";

        for(let i=0;i<headerIndex-1;i++){
            metaData += allTextLines[i] + "\r\n";
        }


        let headers = allTextLines[headerIndex].split(',');
        let dates = [];
        const expr = /date/gi;

        for (let k = 0; k < headers.length; k++) {
            let temp = {};
            let str = headers[k].replace(/_/g, ' ');
            temp.title = str.charAt(0).toUpperCase() + str.slice(1);
            if(temp.title.search(expr) > -1){
                dates.push(k);
            }
            column.push(temp);
        }

        for (let i = headerIndex + 1; i < allTextLines.length; i++) {

            let str = allTextLines[i];


            let flag = true;

            while(flag){flag = false; str = str.replace(/"[a-zA-Z0-9 ]+,[a-zA-Z0-9 ]+"/, function(i){ flag = true; return i.replace(",","");})}


            let data = str.split(',');
            let tarr = [];
            if (data.length === headers.length) {
                for (let j = 0; j < headers.length; j++) {
                    data[j] = data[j].replace(/"/g, '');
                    tarr.push(data[j]);
                }
                lines.push(tarr);
            }
        }

        table = $('#example').DataTable({
            "processing": true,
            // "searching": false,
            // "order": [],
            "order": [],
            caption: "Itemised Report",
            data: lines,
            columns: column,
            "pageLength": 50,
            columnDefs: [ { orderable: false, targets: dates } ],
            dom: '<"download-btn"B><"table-container"t><"table-info"<"length-container"l><"pagination-container"p>>',
            "language": {
                "info": "_TOTAL_ entries",
                "infoEmpty": "0 entries",
                "emptyTable": "No data available",
                "lengthMenu": 'Showing <select>' +
                '<option value="25">25</option>' +
                '<option value="50">50</option>' +
                '<option value="100">100</option>' +
                '</select> rows per page'
            }
        });


        $('#example thead th').each(function () {
            let title = $('#example thead th').eq($(this).index()).text();
            let th = "<div class='th-title'>" + title + "</div>";

            $(this).html(th + '<div class="search-box"><input type="text" placeholder="Filter ' + 'value' +
                '" /><div>');
        });

        table.columns().every(function () {
            let column = this;
            $('input', this.header()).on('keyup change', function () {
                column
                    .search(this.value)
                    .draw();
            });
        });
        table.columns().every(function () {
            $('div.search-box').on("click", function (event) {
                event.stopImmediatePropagation();
            });
        });
        table.column('2').order('asc');

        $(".loader-table").remove();
    }
};


let url = $("#example").attr('data-reporturl');

createTable(url);

let downloadReport = function () {
    let hedear = table.columns().header();
    let x = hedear.map(i => {
        return i.firstChild.innerHTML
    });
    let arr = [], fileContent;
    for(let i=0;i<x.length;i++){
        arr.push(x[i]);
    }
    let tableData = table.rows({
        filter: 'applied'
    }).data();

    let type = $(this).data("reportType");

    fileContent = metaData + "\r\n";
    fileContent += arr + "\r\n";


    for(let i=0;i<tableData.length;i++){
        fileContent += tableData[i] + "\r\n";
    }

    let link = document.createElement("a"),
        fileObject = new Blob([fileContent], { type: 'text/'+ type +'' }),
        fileUrl = URL.createObjectURL(fileObject),
        reportName = $('#reportName')[0].innerText || 'report';



    link.setAttribute("download", reportName+"."+type);
    link.setAttribute("href", fileUrl);
    link.innerHTML = "Click Here to download";
    document.body.appendChild(link);
    link.click();
}

export default {
    downloadReport: downloadReport
}
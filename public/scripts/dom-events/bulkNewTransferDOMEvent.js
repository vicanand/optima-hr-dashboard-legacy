import Storage from '../common/webStorage';
import Constant from '../common/constants';
import DomEventHandler from '../common/domEventHandler.js';
import ServiceConnector from '../common/serviceConnector';
import Util from '../common/util';
import companyCodesModal from "../../template/company-codes-modal.ejs";
import BulkNewTransferCtrl from '../controller/bulkNewTransferCtrl.js';
import BulkNewTransferSvc from '../service/bulkTransferSvc';
import TableWidget from '../widgets/table-widget';
import Table from '../widgets/table';
import FilterWidget from '../widgets/filter-widget';
import Meta from '../common/metaStorage';

Table.init();
TableWidget.init();
FilterWidget.init();

let newTransferData = {};

let bulkOrderAvailCorp = [4707, 96, 100, 326, 4753, 495, 1];

let selectProgramCode = function () {
  $('.program-code-img-cntr').removeClass('active');
  $(this).addClass('active');
  $('#chooseShortcodeNext').removeAttr('disabled').removeClass('btn-disabled');
};

let shortcodeNext = function () {
  let selectedShortCode = $('.program-code-img-cntr.active').data('shortcode');
  let productType = $('.program-code-img-cntr.active').data('producttype');
  let totcompany = $('.program-code-img-cntr.active').data('totcompany');
  let prodType = $('.program-code-img-cntr.active').data('prodtype');
  let bulkCorpId = parseInt($("meta[name='corpID']").attr("content"));
  console.log(bulkCorpId, typeof bulkCorpId);
  if (bulkOrderAvailCorp.indexOf(bulkCorpId) > -1 && selectedShortCode != 'meal') {
    $('#bulkOrderDateInfo').removeClass('hidden');
  }
  initGAEvent('event', 'Bulk Actions', 'click', 'Product Type Selected, corpId=' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', productType=' + prodType + ', timestamp=' + new Date().toLocaleString());
  newTransferData['selectedShortCode'] = selectedShortCode;
  newTransferData['programType'] = prodType;
  $('#chooseTxt').html(selectedShortCode);
  let productIcon = Constant.BULK_TRANSFER_ICON_IMAGES[productType];
  let productPreviewImage = Constant.BULK_TRANSFER_PREVIEW_IMAGES[productType];
  $('#uploadHdgIcon').attr('src', productIcon);
  $('#previewImage,#previewImageModal').attr('src', productPreviewImage);
  $('#previewImage').parent().attr('data-featherlight', productPreviewImage);

  $('#xlsxFile').attr('href', Constant.BULK_TRANSFER_EXCEL_DOWNLOAD_FILE[productType] + '.xlsx');
  $('#xlsFile').attr('href', Constant.BULK_TRANSFER_EXCEL_DOWNLOAD_FILE[productType] + '.xls');
  $('#csvFile').attr('href', Constant.BULK_TRANSFER_EXCEL_DOWNLOAD_FILE[productType] + '.csv');
  if (newTransferData['programType'] == Constant.BULK_CARD_PROGRAM_TYPES['gift'] || newTransferData['programType'] == Constant.BULK_CARD_PROGRAM_TYPES['partner_gifting']) {
    $('#scheduleTransferDate').closest('.row').hide();
    $('.scheduleDate').hide();

  } else {
    $('#scheduleTransferDate').closest('.row').show();
  }
  $('#chooseShortcodeCntr').hide();
  $('#uploadFileCntr').show();
  $('#chooseStep').addClass('done');
  $('#uploadStep').addClass('active');
};

let showCompanyCodesModal = function () {
  $('#companyCodesModalLoader').modal('show');

  let url = '/companyCodes';
  let headers = {};
  ServiceConnector.get(url, headers).then(function (respData) {
    $('#companyCodesModalLoader').modal('hide');
    DomEventHandler.renderMyTemplate('companyCodesModalWrpr', companyCodesModal, respData);
    $('#companyCodesModalWrpr').modal();
  }, function (respErr) {
    $('#companyCodesModalLoader').modal('hide');
    $('#companyCodesModalOopsFund').modal('show');
  })
};

let loadCompanyCodes = function () {
  let url = '/companyCodes';
  let headers = {};
  ServiceConnector.get(url, headers).then(function (respData) {
    console.log('this is resp data', respData);
  }, function (respErr) {
    console.log('error');
  })
};

let validationForTransfer = function () {
  if (newTransferData['programType'] == Constant.BULK_CARD_PROGRAM_TYPES['gift'] || newTransferData['programType'] == Constant.BULK_CARD_PROGRAM_TYPES['partner_gifting']) {
    if (BulkNewTransferCtrl.setFlag() != 0) {
      $('#create-order').removeAttr('disabled');
      $('#create-order').removeClass('btn-disabled');
    }
  } else {
    if ($('#scheduleTransferDate').val() != '' && BulkNewTransferCtrl.setFlag() != 0) {
      $('#create-order').removeAttr('disabled');
      $('#create-order').removeClass('btn-disabled');
    }
  }
};

let initiateTransfer = function () {
  var uploadDetails = $('#uploadDetails').val();
  let productType = $('.program-code-img-cntr.active').data('producttype');
  let inputObj = {
    "corpID": $("meta[name='corpID']").attr("content"),
    "key": JSON.parse(uploadDetails).key,
    "fileName": Storage.get(Constant.STORAGE_FILE_NAME),
    "fileURL": Storage.get(Constant.STORAGE_FILE_URL),
    "programCode": $('.program-code-img-cntr.active').data('shortcode'),
    "poNumber": $('#purchaseOrderNo').val()
  };
  var fileName = Storage.get(Constant.STORAGE_FILE_NAME).split('.');
  var fileExt = fileName[fileName.length - 1];
  var orderNumber = $('#purchaseOrderNo').val();
  var transferDate = $("#scheduleTransferDate").val();
  var prodType = $('.program-code-img-cntr.active').data('prodtype');
  if (newTransferData['programType'] != Constant.BULK_CARD_PROGRAM_TYPES['gift'] && newTransferData['programType'] != Constant.BULK_CARD_PROGRAM_TYPES['partner_gifting']) {
    inputObj.payoutDate = $('#scheduleTransferDate').val() + " 12:00:00";
  }
  $("#createBulkOrderModalOops").modal("hide");
  $("#createBulkOrderModalLoader").modal("show");
  BulkNewTransferSvc.createOrder(inputObj).then(function (respData) {
    $("#createBulkOrderModalLoader").modal("hide");
    Storage.set(Constant.STORAGE_BULK_TRANSFER_ID, respData.bulkOrderID);
    newTransferData['scheduledTransferDate'] = inputObj.payoutDate;
    newTransferData['poNumber'] = inputObj.poNumber;
    newTransferData['noOfBeneficiaries'] = respData.fileProcessingSummary.validRows;
    newTransferData['validAmount'] = Util.formatINR(respData.fileProcessingSummary.amountOfValidRows);
    if (respData.fileProcessingSummary.validRows == 0) {
      $('.hideWhenNoValid').hide();
    } else {
      $('.hideWhenNoValid').show();
    }
    // if(respData.fileProcessingSummary.validRows == 0){
    //     $("#createBulkOrderModalLoader").modal("hide");
    //     $("#createBulkOrderModalOops").modal("show");
    // }
    /*else*/
    if (respData.fileProcessingSummary.invalidRows > 0) {
      Storage.set(Constant.STORAGE_INVALID_BULK_ENTRIES, respData.fileProcessingSummary.invalidRows);
      $('#noOfInvalidEntries').html(respData.fileProcessingSummary.invalidRows);
      $('#fileName').html(Storage.get(Constant.STORAGE_FILE_NAME));
      switch (Storage.get(Constant.STORAGE_FILE_TYPE)) {
        case "text/csv":
          $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-12.png");

          break;
        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
          $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xlsx.png");
          break;
        case "application/vnd.ms-excel":
          $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
          break;
      }
      $("#errorBulkTransfers").table({
        source: BulkNewTransferSvc.getErrorRows,
        pageSize: 10,
        pageNumber: 1,
        tableTemplate: '/template/section/bulk-action/tables/error-entries.ejs',
        rowTemplate: '/template/section/bulk-action/tables/error-entries-row.ejs',
        extraParam: {
          bulkOrderID: respData.bulkOrderID,
          dynamicHeader: true
        }
      });
      $('#fileErrorWrpr').show();
      $('.processStage').css('width', '100%');
      $('#uploadFileCntr').hide();
    } else if (respData.fileProcessingSummary.invalidRows == 0 && respData.fileProcessingSummary.validRows > 0) {
      $('#uploadFileCntr').hide();
      _goToVerifyPage();
    } else {
      $("#createBulkOrderModalLoader").modal("hide");
      $("#createBulkOrderModalOops").modal("show");
    }
    initGAEvent('event', 'Bulk Actions', 'click', 'Bulk Transfer-Beneficiary Details Uploaded, corpId=' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', productType=' + prodType + ', fileType=' + fileExt + ', transferDate=' + transferDate + ', orderNumber=' + orderNumber + ', timestamp=' + new Date().toLocaleString() + ', Success');
  }, function (respErr) {

    let errorMessage = respErr.responseJSON.message.slice(52).replace(/([A-Z])/g, ' $1').trim().split(',');
    let mainOrderList = '<ol id="onlyHeaderList">';
    for (let j = 0; j < errorMessage.length; j++) {
      mainOrderList += '<li>' + errorMessage[j] + '</li>';
    }
    $('#appenErrors').html(mainOrderList);
    if (respErr.responseJSON.type == 'FileHeadersIncorrectException') {
      $('#fileHeaderError').text('Invalid or missing headers');
      $('#fileHeaderErrorDesc').text('Please check the file for missing headers or spelling mistakes. These are expected headers:')
    } else {
      if(Util.checkGenie()){
        $('#fileHeaderError').text('Oops!');
        $('#fileHeaderErrorDesc').text('You do not have access to initiate transfer order.');
        $('#onlyHeaderList').hide();
    } else {
        $('#fileHeaderError').text('Erro while processing the bulk order file');
        $('#fileHeaderErrorDesc').text('Please check the file for errors or make sure you are uploading the correct order file');
        $('#onlyHeaderList').hide();
      }
    }
    initGAEvent('event', 'Bulk Actions', 'click', 'Bulk Transfer-Beneficiary Details Uploaded, corpId=' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', productType=' + prodType + ', fileType=' + fileExt + ', transferDate=' + transferDate + ', orderNumber=' + orderNumber + ', timestamp=' + new Date().toLocaleString() + ', Failed, Reason=' + JSON.stringify(respErr));
    $("#createBulkOrderModalLoader").modal("hide");
    $("#createBulkOrderModalOops").modal("show");
    if (respErr.responseJSON.type == 'InvalidAuthTokenException') {
      window.location.href = "/logout";
      $("#createBulkOrderModalOops").modal("hide");
    }
  });
};

let uploadAgain = function () {
  $("#uploadAgainModalOops").modal("hide");
  $("#uploadAgainModalLoader").modal("show");
  let inputObj = {
    "corpID": $("meta[name='corpID']").attr("content"),
    "bulkOrderID": Storage.get(Constant.STORAGE_BULK_TRANSFER_ID),
    "action": "CANCEL"
  }
  BulkNewTransferSvc.actionOnBulkOrder(inputObj).then(function (respData) {
    $("#uploadAgainModalLoader").modal("hide");
    $('.dz-remove')[0].click();
    $('#fileErrorWrpr').hide();
    $('.processStage').css('width', '708px');
    $('#uploadFileCntr').show();
    $("#errorBulkTransfers").table('destroy');
    $("#errorBulkTransfers table > tbody, #errorBulkTransfers table > thead").remove();
  }, function (respErr) {
    $("#uploadAgainModalLoader").modal("hide");
    $("#uploadAgainModalOops").modal("show");
  });
};

let uploadAgainFile = function () {
  $('#createBulkOrderModalOops').modal('hide');
  $('.dz-remove')[0].click();
  $('#uploadFileCntr').show();
}

let continueValid = function () {
  $('#uploadStep').addClass('done');
  $('#fileErrorWrpr').hide();
  $('.processStage').css('width', '708px');
  _goToVerifyPage();
};

let _goToVerifyPage = function () {
  $('#uploadStep').addClass('done');
  $('#verifyStep').addClass('active');
  $('#verifyOrderCntr').show();
  $('#programCode').html(newTransferData.selectedShortCode);
  $('#programType').html(newTransferData.programType);
  if (newTransferData.scheduledTransferDate) {
    let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let newUpdatedDate = new Date(newTransferData.scheduledTransferDate);
    let transferWeekday = days[newUpdatedDate.getDay()];
    let transferMonth = months[newUpdatedDate.getMonth()];
    let transferYear = newUpdatedDate.getFullYear();
    let transferMonthDate = newUpdatedDate.getDate();
    $('#scheduledTransferDate').text(transferWeekday + ', ' + transferMonthDate + ' ' + transferMonth + ' ' + transferYear);
  } else {
    $('#scheduledTransferDate').html('-');
  }

  if (newTransferData.poNumber) {
    $('#purchaseOrderNumber').html(newTransferData.poNumber);
  } else {
    $('#purchaseOrderNumber').html('-');
  }
  $('#noOfBeneficiaries').html(newTransferData.noOfBeneficiaries);
  $('#totalAmount').html(newTransferData.validAmount);
}

let createBulkTransferOrder = function () {
  $("#verifiedBulkOrderModalLoader").modal("show");
  let inputObj = {
    "corpID": $("meta[name='corpID']").attr("content"),
    "bulkOrderID": Storage.get(Constant.STORAGE_BULK_TRANSFER_ID),
    "action": "PROCEED"
  }
  BulkNewTransferSvc.actionOnBulkOrder(inputObj).then(function (respData) {
    initGAEvent('event', 'Bulk Actions', 'click', 'Bulk Transfer-Order Created, corpId=' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', programCode=' + $('#programCode').text() + ', transferDate=' + $("#scheduledTransferDate").text() + ', amount=' + $("#totalAmount").text() + ',noOfBeneficiaries=' + $("#noOfBeneficiaries").text() + ',orderNumber=' + $("#purchaseOrderNumber").text() + ',timestamp=' + new Date().toLocaleString() + ', Success');
    $("#verifiedBulkOrderModalLoader").modal("hide");
    $("#bulkOrderSuccess").modal({
      backdrop: 'static',
      keyboard: false
    });
    $('#bulkOrderID').html(respData.orderID);
    if (newTransferData.scheduledTransferDate) {
      let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
      let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      let newUpdatedDate = new Date(newTransferData.scheduledTransferDate);
      let transferWeekday = days[newUpdatedDate.getDay()];
      let transferMonth = months[newUpdatedDate.getMonth()];
      let transferYear = newUpdatedDate.getFullYear();
      let transferMonthDate = newUpdatedDate.getDate();
      $('#newScheduleDate').html(transferWeekday + ', ' + transferMonthDate + ' ' + transferMonth + ' ' + transferYear);
    } else {
      $('#newScheduleDate').html('-');
    }

    $('#newStatus').html(respData.status);
  }, function (respErr) {
    initGAEvent('event', 'Bulk Actions', 'click', 'Bulk Transfer-Order Created, corpId=' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', programCode=' + $('#programCode').text() + ', transferDate=' + $("#scheduledTransferDate").text() + ', amount=' + $("#totalAmount").text() + ',noOfBeneficiaries=' + $("#noOfBeneficiaries").text() + ',orderNumber=' + $("#purchaseOrderNumber").text() + ',timestamp=' + new Date().toLocaleString() + ', Failed, Reason=' + JSON.stringify(respErr));
    $("#verifiedBulkOrderModalLoader").modal("hide");
    $("#verifiedBulkOrderModalOops").modal("show");
  });
};

let gotoChooseStep = function () {
  $('#verifyOrderCntr,#fileErrorWrpr,#uploadFileCntr').hide();
  $('.processStage').css('width', '708px');
  $('#chooseShortcodeCntr').show();
  $('#chooseStep').removeClass('done');
  $('#uploadStep,#verifyStep').removeClass('active').removeClass('done');
  $('.dz-remove')[0].click();
  $("#errorBulkTransfers").table('destroy');
};

let gotoUploadStep = function () {
  if ($(this).hasClass('done')) {
    $('#verifyOrderCntr,#fileErrorWrpr,#chooseShortcodeCntr').hide();
    $('.processStage').css('width', '708px');
    $('#uploadFileCntr').show();
    $('#uploadStep').removeClass('done');
    $('#verifyStep').removeClass('active').removeClass('done');
    $('.dz-remove')[0].click();
  }
};

let downloadBulkReport = function (e) {
  $("#downloadReportModalLoader").modal("show");
  e.preventDefault();
  BulkNewTransferSvc.downloadReport(Storage.get(Constant.STORAGE_BULK_TRANSFER_ID)).then(function (respData) {
    $("#downloadReportModalLoader").modal("hide");
    var win = window.open(Util.htmlDecoder(respData.location), '_blank');
    win.focus();
  }, function (respErr) {
    $("#downloadReportModalLoader").modal("hide");
  });
}


export default {
  selectProgramCode: selectProgramCode,
  shortcodeNext: shortcodeNext,
  showCompanyCodesModal: showCompanyCodesModal,
  validationForTransfer: validationForTransfer,
  initiateTransfer: initiateTransfer,
  uploadAgain: uploadAgain,
  uploadAgainFile: uploadAgainFile,
  continueValid: continueValid,
  createBulkTransferOrder: createBulkTransferOrder,
  gotoChooseStep: gotoChooseStep,
  gotoUploadStep: gotoUploadStep,
  downloadBulkReport: downloadBulkReport,
  loadCompanyCodes: loadCompanyCodes
};

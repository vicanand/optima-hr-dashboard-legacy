import ServiceConnector from '../common/serviceConnector';
import Util from '../common/util';
import Urls from '../common/urls';


export
default {
    complteSetup: function() {
        let $corporateName = $('#corporateName');
        let $companyName = $('#companyName');
        let $companyAddrs = $('#companyAddrs');
        let $companyCity = $('#companyCity');
        let $companyState = $('#companyState');
        let $companyCountry = $('#companyCountry');
        let $companyPincode = $('#companyPincode');
        let corporateName = $corporateName.val();
        let companyName = $companyName.val();
        let companyAddrs = $companyAddrs.val();
        let companyCity = $companyCity.val();
        let companyState = $companyState.val();
        let companyCountry = $companyCountry.val();
        let companyPincode = $companyPincode.val();
        let validForm = true;

        if(corporateName == ''){
            $corporateName.addClass('error-input').parent().find('.error-msg').show();
            validForm = false;
        }
        if(companyName == ''){
            $companyName.addClass('error-input').parent().find('.error-msg').show();
            validForm = false;
        }
        if(companyAddrs == ''){
            $companyAddrs.addClass('error-input').parent().find('.error-msg').show();
            validForm = false;
        }
        if(companyCity == ''){
            $companyCity.addClass('error-input').parent().find('.error-msg').show();
            validForm = false;
        }
        if(companyState == ''){
            $companyState.addClass('error-input').parent().find('.error-msg').show();
            validForm = false;
        }
        if(companyCountry == ''){
            $companyCountry.addClass('error-input').parent().find('.error-msg').show();
            validForm = false;
        }
        if(companyPincode == '' || companyPincode.length < 6){
            $companyPincode.addClass('error-input').parent().find('.error-msg').show();
            validForm = false;
        }

        if(validForm){
            $('#completeSetup').addClass('loading');
            let corpID = $("meta[name='corpID']").attr("content");
            let companyID = $("meta[name='companyID']").attr("content");
            let inputJson = {
                "corpID": corpID,
                "companyID": companyID,
                "corporateName": corporateName,
                "companyName": companyName,
                "billingAddress": companyAddrs,
                "pincode": companyPincode,
                'city' : companyCity,
                'state' : companyState,   
                'country' : companyCountry
            }
            let headers = {
                "Content-Type": "application/json"
            };
            let url = '/verifyNewCorporateDetails';
            ServiceConnector.post(inputJson, url, headers).then(function(respData) {
                $('#completeSetup').removeClass('loading');
                if(!respData.goToCorpDetails){
                    window.location.href = '/companies/' + companyID + '/' + 'optima';
                }
            }, function(respErr) {
                if(respErr.responseText=="Cannot modify corporate details. Action allowed only for new corporates"){
                    window.location.href = '/companies/' + companyID + '/' + 'optima';
                }
                $('#completeSetup').removeClass('loading');
            });
            
        }
    },
    validateItem : function(){
        if($(this).attr('name') === 'pincode'){
            if($(this).val() === '' || $(this).val().length < 6){
                $(this).addClass('error-input').parent().find('.error-msg').show();
            }
            else{
                $(this).removeClass('error-input').parent().find('.error-msg').hide();
            }
        }
        else if($(this).val() === ''){
            $(this).addClass('error-input').parent().find('.error-msg').show();
        }
        else{
            $(this).removeClass('error-input').parent().find('.error-msg').hide();
        }
    },
    validateCountry: function(){
        if($(this).val()){
            $(this).parent().find('.error-msg').hide();
        }else{
            $(this).parent().find('.error-msg').show();
        }
    }
}

import Storage from '../common/webStorage';
import Constant from '../common/constants';
import DomEventHandler from '../common/domEventHandler.js';
import ServiceConnector from '../common/serviceConnector';
import Util from '../common/util';
import companyCodesModal from "../../template/company-codes-modal.ejs";
import BulkCloseCtrl from '../controller/bulkCloseCtrl.js';
import BulkCloseSvc from '../service/bulkCloseSvc.js';
import TableWidget from '../widgets/table-widget';
import Table from '../widgets/table';
import FilterWidget from '../widgets/filter-widget';
import Meta from '../common/metaStorage';

Table.init();
TableWidget.init();
FilterWidget.init();

let newTransferData = {};

let shortcodeNext = function() {
    let selectedShortCode = $('.program-code-img-cntr.active').data('shortcode');
    let productType = $('.program-code-img-cntr.active').data('producttype');
    newTransferData['selectedShortCode'] = selectedShortCode;
    newTransferData['programType'] = Constant.BULK_CARD_PROGRAM_TYPES[productType];
    $('#chooseTxt').html(selectedShortCode);
    let productIcon = Constant.BULK_TRANSFER_ICON_IMAGES[productType];
    let productPreviewImage = Constant.BULK_TRANSFER_PREVIEW_IMAGES[productType];
    $('#uploadHdgIcon').attr('src', productIcon);
    $('#previewImage,#previewImageModal').attr('src', productPreviewImage);
    $('#previewImage').parent().data('featherlight', productPreviewImage);
    $('#xlsxFile').attr('href', Constant.BULK_TRANSFER_EXCEL_DOWNLOAD_FILE[productType] + '.xlsx');
    $('#xlsFile').attr('href', Constant.BULK_TRANSFER_EXCEL_DOWNLOAD_FILE[productType] + '.xls');
    $('#csvFile').attr('href', Constant.BULK_TRANSFER_EXCEL_DOWNLOAD_FILE[productType] + '.csv');
    $('#chooseShortcodeCntr').hide();
    $('#uploadFileCntr').show();
    $('#chooseStep').addClass('done');
    $('#uploadStep').addClass('active');
};

let showCompanyCodesModal = function() {
    $('#companyCodesModalLoader').modal('show');
    let url = '/companyCodes';
    let headers = {};
    ServiceConnector.get(url, headers).then(function(respData) {
        $('#companyCodesModalLoader').modal('hide');
        DomEventHandler.renderMyTemplate('companyCodesModalWrpr', companyCodesModal, respData);
        $('#companyCodesModalWrpr').modal();
    }, function(respErr) {
        $('#companyCodesModalLoader').modal('hide');
        $('#companyCodesModalOopsFund').modal('show');
    })
};

let validationForTransfer = function() {
    if ($('#companies').val() != '0' && BulkCloseCtrl.setFlag() != 0 && $('#termsConditions').is(':checked')) {
        $('#create-order').removeAttr('disabled');
        $('#create-order').removeClass('btn-disabled');
    } else {
        $('#create-order').prop('disabled', true).addClass('btn-disabled');
    }
};

let initiateTransfer = function() {
    var uploadDetails = $('#uploadDetails').val();
    let inputObj = {
        "corpID": $("meta[name='corpID']").attr("content"),
        "companyID": $('#companies').val(),
        "key": JSON.parse(uploadDetails).key,
        "fileName": Storage.get(Constant.STORAGE_FILE_NAME),
        "fileURL": Storage.get(Constant.STORAGE_FILE_URL)
    };
    $("#createBulkOrderModalOops").modal("hide");
    $("#createBulkOrderModalLoader").modal("show");
    var fileName = $(".dz-filename span").text().split('.');
    var fileExt = fileName[fileName.length - 1];
    BulkCloseSvc.createOrder(inputObj).then(function(respData) {
        initGAEvent('event', 'Bulk Actions', 'click', 'Bulk Close Card-Beneficiary Details Uploaded, corpId=' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', fileType=' + fileExt + ', companyId=' + $('#companies').val() + ', timestamp=' + new Date().toLocaleString() + ', Success');
        $("#createBulkOrderModalLoader").modal("hide");
        Storage.set(Constant.STORAGE_BULK_TRANSFER_ID, respData.orderID);
        Storage.set(Constant.STORAGE_BULK_TRANSFER_BENEFICIARIES, respData.fileProcessingSummary.validRows);
        $('#bulkRequestID').html(respData.closeCardOrderID);
        $('#noOfBeneficiariesConfirm').html(respData.numberOfVectors);
        if (respData.fileProcessingSummary.validRows == 0) {
            $('.hideWhenNoValid').hide();
        } else {
            $('.hideWhenNoValid').show();
        }
        /*if(respData.fileProcessingSummary.validRows == 0){
            $("#createBulkOrderModalLoader").modal("hide");
            $("#createBulkOrderModalOops").modal("show");
        }
        else*/
        if (respData.fileProcessingSummary.invalidRows > 0) {
            Storage.set(Constant.STORAGE_INVALID_BULK_ENTRIES, respData.fileProcessingSummary.invalidRows);
            $('#noOfInvalidEntries').html(respData.fileProcessingSummary.invalidRows);
            $('#fileName').html(Storage.get(Constant.STORAGE_FILE_NAME));
            switch (Storage.get(Constant.STORAGE_FILE_TYPE)) {
                case "text/csv":
                    $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-12.png");

                    break;
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xlsx.png");
                    break;
                case "application/vnd.ms-excel":
                    $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                    break;
            }
            $("#errorBulkTransfers").table({
                source: BulkCloseSvc.getErrorRows,
                pageSize: 10,
                pageNumber: 1,
                tableTemplate: '/template/section/bulk-action/tables/error-entries.ejs',
                rowTemplate: '/template/section/bulk-action/tables/error-entries-row.ejs',
                extraParam: {
                    bulkOrderID: respData.orderID,
                    dynamicHeader: true
                }
            });
            $('#fileErrorWrpr').show();
            $('#uploadFileCntr').hide();
        } else if (respData.fileProcessingSummary.invalidRows == 0 && respData.fileProcessingSummary.validRows > 0) {
            $('#uploadFileCntr').show();
            createBulkTransferOrder();
        } else {
            $("#createBulkOrderModalLoader").modal("hide");
            $("#createBulkOrderModalOops").modal("show");
        }

    }, function(respErr) {
        let checkGenie = 0;
        if(localStorage.getItem('checkGenie')){
            checkGenie= parseInt(localStorage.getItem('checkGenie'));
        }
        if(checkGenie !=0){
            $("#createBulkOrderModalLoader").modal("hide");
            alert("You may not have access to this feature.");
        } else {
        if (respErr.responseJSON.type === "FileHeadersIncorrectException") {
            $('#fileHeaderError').text('Invalid or missing headers');
            $('#fileHeaderErrorDesc').text('Please check the file for missing headers or spelling mistakes. These are expected headers:')
        } else {
            $('#fileHeaderError').text('Erro while processing the bulk order file');
            $('#fileHeaderErrorDesc').text('Please check the file for errors or make sure you are uploading the correct order file');
            $('#onlyHeaderList').hide();
        }
        initGAEvent('event', 'Bulk Actions', 'click', 'Bulk Close Card-Beneficiary Details Uploaded, corpId=' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', fileType=' + fileExt + ', companyId=' + $('#companies').val() + ', timestamp=' + new Date().toLocaleString() + ', Failed, Reason=' + JSON.stringify(respErr));
        $("#createBulkOrderModalLoader").modal("hide");
        $("#createBulkOrderModalOops").modal("show");
      }
    });
};

let uploadAgain = function() {
    $("#uploadAgainModalOops").modal("hide");
    $("#uploadAgainModalLoader").modal("show");
    let inputObj = {
        "corpID": $("meta[name='corpID']").attr("content"),
        "orderID": Storage.get(Constant.STORAGE_BULK_TRANSFER_ID),
        "action": "CANCEL"
    }
    BulkCloseSvc.actionOnBulkOrder(inputObj).then(function(respData) {
        $("#uploadAgainModalLoader").modal("hide");
        $('.dz-remove')[0].click();
        $('#fileErrorWrpr').hide();
        $('#uploadFileCntr').show();
        $("#errorBulkTransfers").table('destroy');
    }, function(respErr) {
        $("#uploadAgainModalLoader").modal("hide");
        $("#uploadAgainModalOops").modal("show");
    });
};

let uploadAgainFile = function() {
    $('#createBulkOrderModalOops').modal('hide');
    $('.dz-remove')[0].click();
    $('#uploadFileCntr').show();
}

let continueValid = function() {
    $('#uploadStep').addClass('done');
    //$('#fileErrorWrpr').hide();
    createBulkTransferOrder();
};

let _goToVerifyPage = function() {
    $('#verifyStep').addClass('active');
    $('#verifyOrderCntr').show();
    $('#programCode').html(newTransferData.selectedShortCode);
    $('#programType').html(newTransferData.programType);
    $('#noOfBeneficiaries').html(newTransferData.noOfBeneficiaries);
    $('#totalAmount').html(newTransferData.validAmount);
}

let createBulkTransferOrder = function() {
    $("#verifiedBulkOrderModalLoader").modal("show");
    let inputObj = {
        "corpID": $("meta[name='corpID']").attr("content"),
        "orderID": Storage.get(Constant.STORAGE_BULK_TRANSFER_ID),
        "action": "PROCEED"
    }
    BulkCloseSvc.actionOnBulkOrder(inputObj).then(function(respData) {
        $("#verifiedBulkOrderModalLoader").modal("hide");
        $("#bulkOrderSuccess").modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#noOfBeneficiariesConfirm').html(Storage.get(Constant.STORAGE_BULK_TRANSFER_BENEFICIARIES));
        $('#newStatus').html(respData.status);
        $('#bulkRequestID').html(respData.orderID);
    }, function(respErr) {
        $("#verifiedBulkOrderModalLoader").modal("hide");
        $("#verifiedBulkOrderModalOops").modal("show");
    });
};

let gotoChooseStep = function() {
    $('#verifyOrderCntr,#fileErrorWrpr,#uploadFileCntr').hide();
    $('#chooseShortcodeCntr').show();
    $('#chooseStep').removeClass('done');
    $('#uploadStep,#verifyStep').removeClass('active').removeClass('done');
    $('.dz-remove')[0].click();
};

let gotoUploadStep = function() {
    if ($(this).hasClass('done')) {
        $('#verifyOrderCntr,#fileErrorWrpr,#chooseShortcodeCntr').hide();
        $('#uploadFileCntr').show();
        $('#uploadStep').removeClass('done');
        $('#verifyStep').removeClass('active').removeClass('done');
        $('.dz-remove')[0].click();
    }
};

export default {
    validationForTransfer: validationForTransfer,
    initiateTransfer: initiateTransfer,
    uploadAgain: uploadAgain,
    uploadAgainFile: uploadAgainFile,
    continueValid: continueValid,
    createBulkTransferOrder: createBulkTransferOrder,
    gotoChooseStep: gotoChooseStep,
    gotoUploadStep: gotoUploadStep
};
import ServiceConnector from '../common/serviceConnector';
import Storage from '../common/webStorage.js';
import TransferDetailCtrl from '../controller/transferDetailCtrl';
import benefitDetailsCtrl from "../controller/benefitDetailsCtrl";
import Meta from '../common/metaStorage';
import Util from "../common/util";
import DomEventHandler from "../common/domEventHandler";
import TimeFrames from '../../template/section/benefits/time-frame.ejs';
import benefitSvc from '../service/benefitSvc';
import TimeFromFramesVnd from '../../template/section/benefits/VND/time-frame-from.ejs';
import TimeToFramesVnd from '../../template/section/benefits/VND/time-frame-to.ejs';
import SpendDaysVnd from '../../template/section/benefits/VND/spend-days.ejs';

var addMoreRow = function(newRow, timeFrom, timeTo) {
    DomEventHandler.appendToMyTemplate('timeSliceContainer', TimeFrames, { newRow: newRow, timeFrom: timeFrom, timeTo: timeTo });
    DomEventHandler.bindClassEvent('removeRow', 'click', function() {
        $(this).closest('.selectRows').remove();
    })
    if (newRow == 'false') {
        $('#addMoreRow').click(function() {
            if($('.removeRow').length <= 4){
                addMoreRow('true', '', '');
            }
        })
    }
}

let getVNDTime = function(timeSelected){
    let vndTime = '';
    if(timeSelected != 'select'){
        if(parseInt(timeSelected) < 11){
            vndTime = (parseInt(timeSelected) + 1) + ':30 AM ICT'
        }else if(parseInt(timeSelected) == 11){
            vndTime = (parseInt(timeSelected) + 1) + ':30 PM ICT'
        }else if(parseInt(timeSelected) == 23){
            vndTime = '00:30 AM ICT'
        }else{
            vndTime = (parseInt(timeSelected) - 12 + 1) + ':30 PM ICT'
        }
    }

    return vndTime ? '(' + vndTime + ')' : ''
}

let cardProgramObjectData = $('#program-settings').data('cardprogramobject');

var getCardProgramObject = function() {
    return cardProgramObjectData;
}

var setCardProgramObject = function(requestBody, fundingAccName) {
    if(requestBody.cardDesign){
        cardProgramObjectData.cardDesign = requestBody.cardDesign
    }
    if(requestBody.spendConfiguration){
        cardProgramObjectData.spendConfiguration = requestBody.spendConfiguration
    }
    if(requestBody.reimbursementProgram){
        cardProgramObjectData.reimbursementProgram = requestBody.reimbursementProgram
    }
    if(requestBody.issuanceConfiguration){
        cardProgramObjectData.issuanceConfiguration = requestBody.issuanceConfiguration
    }
    if(requestBody.voucherProgram){
        cardProgramObjectData.voucherProgram = requestBody.voucherProgram
    }
    if(requestBody.businessDetails){
        cardProgramObjectData.businessDetails = requestBody.businessDetails
    }

    if(cardProgramObjectData.fundingAccountId != requestBody.fundingAccountID){
        cardProgramObjectData.fundingAccountId = requestBody.fundingAccountID
        
        if(fundingAccName){
            $('#fundingAccountID').append(`<option value="${requestBody.fundingAccountID}" selected>${fundingAccName}</option>`)
            $('#fundingAccountId').append(`<option value="${requestBody.fundingAccountID}" selected>${fundingAccName}</option>`)
            $('#fundingAccName').text(fundingAccName);
            $('#fundingAccBalance').text("0");
        }else{
            let fundingAccNameById = $('#fundingAccountId option[value='+requestBody.fundingAccountID+']').text();
            $('#fundingAccName').text(fundingAccNameById);
            $('#fundingAccBalance').text($('#fundingAccData').data('accounts').filter(acc => acc.name == fundingAccNameById)[0].balance);
        }
        
    }
}

var updateRequestBody = function() {
    return {
        "corpID": $('#corpID').val(),
        "companyID": $('#companyID').val(),
        "programID": $('#programID').val(),
        "fundingAccountID": getCardProgramObject().fundingAccountId
    }
}

var prepareGeneralSetting = function() {
    var cardProgramObject = getCardProgramObject();
    var cardDesign = cardProgramObject.cardDesign;
    var spendConfig = cardProgramObject.spendConfiguration;
    cardDesign.cardName = $('#programName').val()
    var requestBody = updateRequestBody();

    let fundingAccountId = $('#fundingAccountId').val()

    let programType = $('input[name="programType"]:checked').val();
    let claimUploadRequired = $('input[name="claimUpload"]:checked').val();
    if (programType == "allowance") {
        requestBody['programModel'] = 'ALLOWANCE'
    } else {
        requestBody['programModel'] = 'REIMBURSEMENT'
    }
    
    if (programType == "allowance") {
        if(claimUploadRequired == 'Yes'){
            spendConfig.channels.allowReimbursement = true;
        }else{
            spendConfig.channels.allowReimbursement = false;
        }
    }else{
        spendConfig.channels.allowReimbursement = true;
    }

    if(cardProgramObject.fundingAccountId != fundingAccountId && (fundingAccountId != 'createNew' || fundingAccountId != "0")){
        requestBody.fundingAccountID = fundingAccountId
    }

    requestBody["cardDesign"] = cardDesign;
    requestBody["spendConfiguration"] = spendConfig;

    return requestBody;
}

var prepareAddressSetting = function(){
    var cardProgramObject = getCardProgramObject();
    var bussinessConfig = cardProgramObject.businessDetails;
    var requestBody = updateRequestBody();

    let firstName = $('#firstName').val(),
    lastName = $('#lastName').val(),
    email = $('#email').val(),
    countryCode = $('#countryCode').val(),
    phone = $('#phone').val(),
    address1 = $('#address1').val(),
    address2 = $('#address2').val(),
    city = $('#city').val(),
    state = $('#state').val(),
    pincode = $('#pincode').val()

    let billingObj = {
        "firstName": firstName,
        "lastName": lastName,
        "email": email,
        "telephone": phone,
        "line1": address1,
        "line2": address2,
        "city": city,
        "region": state,
        "countryID": countryCode,
        "postCode": pincode
    }

    bussinessConfig.billingAddress = billingObj;
    requestBody["businessDetails"] = bussinessConfig

    return requestBody;
}

var prepareSpendSetting = function(countryName) {
    var cardProgramObject = getCardProgramObject();
    var spendConfig = cardProgramObject.spendConfiguration;
    var requestBody = updateRequestBody();
    /* Spend Settings */
    let spendDeclaration = $('input[name="declaration"]:checked').val();
    let spendDeclarationText = $('#declarationText').val()
    //Spend channels
    let zetaApp = $('#zeta-app').prop('checked');
    let superCard = $('#super-card').prop('checked');
    let superTag = $('#super-tag').prop('checked');
    let maxTxnLimit = $('#maxTxnLimit').val();
    let minTxnLimit = $('#minTxnLimit').val();
    let maxTxnPerDay = $('#maxTxnPerDay').val();
    let maxTxnAmtPerDay = $('#maxTxnAmtPerDay').val();
    let maxTxnPerMonth = $('#maxTxnPerMonth').val();
    let maxTxnAmtPerMonth = $('#maxTxnAmtPerMonth').val();
    let spendDays = $('#spendDays').val();
    let allowedMerchants = $('#allowedMerchants').val();
    let disallowedMerchants = $('#disallowedMerchants').val();
    let customAllowedMerchants = $.trim($('#customAllowedMerchants').val());
    let customDisallowedMerchants = $.trim($('#customDisallowedMerchants').val());

    if (spendDeclaration && spendDeclaration == 'Yes') {
        spendConfig.spendDeclaration = spendDeclarationText
    }
    if(superCard){
        spendConfig.channels['allowSuperCard'] = superCard
    }
    if(zetaApp){
        spendConfig.channels['allowZetaApp'] = zetaApp
    }
    if(superTag){
        spendConfig.channels['allowSuperTag'] = superTag
    }
    spendConfig.allowedTimeSlices = [];
    $('#timeSliceContainer .selectRows').each(function(i, element) {
        var timeFrom = $(element).find('.timeFrom').val()
        var timeTo = $(element).find('.timeTo').val()
        if(!countryName){
            if (timeFrom != 'select' && timeTo != 'select') {
                let cronExp = "* * " + timeFrom + "-" + timeTo + " ? * ";
                if(spendDays && spendDays.length > 0){
                    cronExp += spendDays.join(',') + " *";
                }else{
                    cronExp += "* *";
                }
                spendConfig.allowedTimeSlices.push(cronExp);
            }
        }else{
            var timeFrom = $(element).find('.timeFrom').val()
            let cronExp = "* * " + (timeFrom >= 1.5 ? (parseFloat(timeFrom) - 1.5) : 0) + "-" + '22' + " ? * ";
            if(spendDays && spendDays.length > 0){
                cronExp += spendDays.join(',') + " *";
            }else{
                cronExp += "* *";
            }
            spendConfig.allowedTimeSlices.push(cronExp);
        }
    })
    if(customAllowedMerchants){
        customAllowedMerchants = customAllowedMerchants != "" ? customAllowedMerchants.split(',') : [];
    }
    if(customDisallowedMerchants){
        customDisallowedMerchants = customDisallowedMerchants != "" ? customDisallowedMerchants.split(',') : [];
    }
    let merchantValidation
    if(customAllowedMerchants || customDisallowedMerchants){
        merchantValidation = validateMerchant(customAllowedMerchants, customDisallowedMerchants)
        if(!merchantValidation.validAllowedMerchants){
            $('#customAllowedMerchants').addClass('errorField')
        }else{
            $('#customAllowedMerchants').removeClass('errorField')
        }
        if(!merchantValidation.validDisallowedMerchants){
            $('#customDisallowedMerchants').addClass('errorField')
        }else{
            $('#customDisallowedMerchants').removeClass('errorField')
        }
    }

    if(customAllowedMerchants.length > 0){
        // customAllowedMerchants = customAllowedMerchants.split(',');
        spendConfig['allowedMerchants'] = allowedMerchants.concat(customAllowedMerchants)
    }else if(allowedMerchants){
        spendConfig['allowedMerchants'] = allowedMerchants
    }
    if(customDisallowedMerchants.length > 0){
        // customDisallowedMerchants = customDisallowedMerchants.split(',');
        spendConfig['disallowedMerchants'] = disallowedMerchants.concat(customDisallowedMerchants)
    }else if(disallowedMerchants){
        spendConfig['disallowedMerchants'] = disallowedMerchants
    }


    // spendConfig.limits.transactionLimit = maxTxnLimit * 100
    if(minTxnLimit)
        spendConfig.transactionConstraints.minTransactionValue = minTxnLimit * 100
    if(maxTxnLimit)
        spendConfig.transactionConstraints.maxTransactionValue = maxTxnLimit * 100
    if(maxTxnPerDay)
        spendConfig.limits.dailyTxnCount = maxTxnPerDay
    if(maxTxnAmtPerDay)
        spendConfig.limits.dailyLimit = maxTxnAmtPerDay * 100
    if(maxTxnPerMonth)
        spendConfig.limits.monthlyTxnCount = maxTxnPerMonth
    if(maxTxnAmtPerMonth)
        spendConfig.limits.monthlyLimit = maxTxnAmtPerMonth * 100

    requestBody["spendConfiguration"] = spendConfig;

    return requestBody;
}

var validateMerchant = function(allowedMerchants, disallowedMerchants){
    let validMerchants = ["mid", "mcc", "amc"]
    let validAllowedMerchants = true
    let validDisallowedMerchants = true
    allowedMerchants.forEach(function(merchant){
        if(validMerchants.indexOf(merchant.split(':')[0]) < 0){
            validAllowedMerchants = false
        }
    })
    disallowedMerchants.forEach(function(merchant){
        if(validMerchants.indexOf(merchant.split(':')[0]) < 0){
            validDisallowedMerchants = false
        }
    })

    return {
        validAllowedMerchants: validAllowedMerchants,
        validDisallowedMerchants: validDisallowedMerchants
    }
}

var prepareClaimSetting = function() {
    var cardProgramObject = getCardProgramObject();
    var requestBody = updateRequestBody();
    var productType = cardProgramObject.productType;

    var reimbursementProgram = cardProgramObject.reimbursementProgram;

    if (productType != 'asset' && productType != 'gift') {
        $('#rulesTable tbody tr').each(function(index, element) {
            let rule = $(this).find('input:eq(0)').attr('name');
            let checkedValue = $(this).find('input:eq(0)').prop('checked');
            reimbursementProgram.extraReimbursementProgramConfig.allowRules.forEach(element => {
                if (element.rule == rule) {
                    element.value = checkedValue;
                }
            });

            reimbursementProgram.extraReimbursementProgramConfig.requireRules.forEach(element => {
                if (element.rule == rule) {
                    element.value = checkedValue;
                }
            });
        });
    }

    let optedBooksCategories = [];
    if (productType == "books") {
        $('.booksSection select').each(function(index, element) {
            let selectedValue = $(this).val();
            optedBooksCategories = optedBooksCategories.concat(selectedValue);
        })
    }

    if (productType == "uniform") {
        let optedUniformCategories = [];
        $('#uniformOpts input:checked').each(function(index, element) {
            optedUniformCategories.push($(this).val());
        })
        reimbursementProgram.extraReimbursementProgramConfig.uniformCategories = optedUniformCategories;
    }
    if (productType == "books") {
        reimbursementProgram.extraReimbursementProgramConfig.bookCategories = optedBooksCategories;
    }

    requestBody['reimbursementProgram'] = reimbursementProgram;


    return requestBody;
}

var prepareExpirySetting = function() {
    let cardProgramObject = getCardProgramObject();
    let issuanceConfiguration = cardProgramObject.issuanceConfiguration;
    let voucherProgram = cardProgramObject.voucherProgram;
    let reimbursementProgram = cardProgramObject.reimbursementProgram
    let requestBody = updateRequestBody();
    let expireBalance = $('input[name="expire-balance"]:checked').val();
    let expireOn = $('#expireBalanceOn').val();
    let moveBalanceTo = $('input[name="remainingbalance"]:checked').val();
    let voucherDenomination = $('#voucherDenomination').val()

    if(reimbursementProgram.extraReimbursementProgramConfig){
        reimbursementProgram.extraReimbursementProgramConfig.voucherDenomination = voucherDenomination
    }else{
        reimbursementProgram = {
            extraReimbursementProgramConfig: {
                voucherDenomination: voucherDenomination
            },
            printedBillRequired: false,
            mandatorySpendThroughZeta: false,
            maxValuePerBill: 1000000,
            maxValuePerMonth: 1000000,
            approval: "ZETA"
        }
    }

    if (expireBalance == 'Yes') {
        voucherProgram.voucherValidity = expireOn;
        issuanceConfiguration.expiryBehaviour = moveBalanceTo
    } else {
        voucherProgram.voucherValidity = '';
        issuanceConfiguration.expiryBehaviour = ''
    }

    requestBody['issuanceConfiguration'] = issuanceConfiguration;
    requestBody['voucherProgram'] = voucherProgram;
    requestBody['reimbursementProgram'] = reimbursementProgram

    return requestBody;
};

var prepareClosureSetting = function() {
    var requestBody = updateRequestBody();
    let allowProgramClosureBeneficiary = $('input[name="to-be-closed"]:eq(0)').prop('checked');
    let closeOn = $('#closeProgramOn').val();
    let moveBalanceOnClosure = $('input[name="closureremainingbalance"]:checked').val();

    let closeCardConfiguration = {
        closeBehaviour: "",
        cardCloseFrequency: "CURRENT_FINANCIAL_YEAR",
    };

    if (allowProgramClosureBeneficiary) {
        let closeOnDate = closeOn.split('/');
        closeOn = closeOnDate[2] + "-" + closeOnDate[1] + "-" + closeOnDate[0];
        closeCardConfiguration.closeCardDate = closeOn;
        closeCardConfiguration.closeBehaviour = moveBalanceOnClosure;
        requestBody['closeCardConfiguration'] = closeCardConfiguration
    }

    return requestBody;
};

var updateSettingsView = function(requestBody, section, fundingAccName){
    if(section == 'General'){
        $('#programNameView').text(requestBody.cardDesign.cardName)
        $('#funcdingAcctView').text(fundingAccName || $('#fundingAccountId option[value='+requestBody.fundingAccountID+']').text())
        $('#programTypeView').text(requestBody.programModel)
        $('#claimUploadView').text(requestBody.spendConfiguration.channels.allowReimbursement ? 'Yes' : 'No')
    } else if(section == "Spend"){

    } else if(section == "Address"){
        $('#firstNameView').text(requestBody.businessDetails.billingAddress.firstName)
        $('#lastNameView').text(requestBody.businessDetails.billingAddress.lastName)
        $('#emailView').text(requestBody.businessDetails.billingAddress.email)
        $('#phoneView').text(requestBody.businessDetails.billingAddress.countryID + ' ' + requestBody.businessDetails.billingAddress.telephone)
        $('#address1View').text(requestBody.businessDetails.billingAddress.line1)
        $('#address2View').text(requestBody.businessDetails.billingAddress.line2)
        $('#cityView').text(requestBody.businessDetails.billingAddress.city)
        $('#stateView').text(requestBody.businessDetails.billingAddress.region)
        $('#pincodeView').text(requestBody.businessDetails.billingAddress.postCode)
    } else if(section == "Claim"){

    } else if(section == "Expiry"){

    }
    
}

var updateCustomCardProgram = function(section, requestBody, fundingAccName){
    $('#loaderModal').modal('show');
    benefitSvc.updateCustomCardProgram(requestBody).then(function(response) {
        setCardProgramObject(requestBody, fundingAccName);
        updateSettingsView(requestBody, section, fundingAccName)
        $('#loaderModal').modal('hide');
        $('#programUpdateSuccess').modal('show');
        $('#programUpdateSuccessMessage').text(section + ' settings saved successfully');
    }, function(err){
        $('#loaderModal').modal('hide');
        $('#programUpdateSuccess').modal('show');
        $('#programUpdateSuccessMessage').text('Failed to save setting');
        $('.successImage').hide();
        $('.errorImage').hide();
    })
};

var prepareCardLogoSetting = function (location) {
    let cardProgramObject = $('#over-view').data('cardprogramobject');
    let requestBody = {
        "corpID": $('#corpID').val(),
        "companyID": $('#companyID').val(),
        "programID": $('#programID').val(),
        "cardDesign": cardProgramObject.cardDesign,
    }
    requestBody.cardDesign.logo = location;
    return requestBody;
}

var updateCardProgramLogo = function (inputData, fileUrl, headers, fileName) {
    ServiceConnector.postWithCORS(inputData, fileUrl, headers).then(function (UploadData) {
        let key = UploadData.childNodes[0].childNodes[2].textContent;
        let tag = UploadData.childNodes[0].childNodes[3].textContent.replace('"','').replace('"','');
        let cardLogoLocation = UploadData.childNodes[0].childNodes[0].textContent + '?key=' + key + '&etag=' + tag+'&fileName='+fileName;
        let req = prepareCardLogoSetting(cardLogoLocation);    
        benefitSvc.updateCustomCardProgram(req).then(function (res) {
            $('#cardProgramLogo').attr("src", res.cardDesign.logo);
            $('#loaderModal').modal('hide');
            $('#programUpdateSuccess').modal('show');
            $('#programUpdateSuccessMessage').text('Logo updated successfully');
        }, function (respErr) {
            console.log("updateCardProgramLogorespErr", respErr);
            $('#loaderModal').modal('hide');
            $('#programUpdateSuccess').modal('show');
            $('#programUpdateSuccessMessage').text('Failed to upload logo');
            $('.successImage').hide();
        });
    })
}

var updateOTmealSpendSetting = function (timeFrom, timeTo) {
    let spendDays = $('#spendDays').val();
    let cronExp = "* " + timeFrom + "-" + timeTo + " * * ";
    let cardProgramID = $('#cardProgramID').val()
    if(spendDays && spendDays.length > 0){
        cronExp += spendDays.join(',');
    }else{
        cronExp += "*";
    }

    let requestBody = {
        "cardProgramID": cardProgramID,
        "allowedTimeSlices": [cronExp],
        "disallowedTimeSlices":[]
    }
    $('#loaderModal').modal('show');
    benefitSvc.updateSpendTimeVnd(requestBody).then(function(respData){
        $('#loaderModal').modal('hide');
        $('#programUpdateSuccess').modal('show');
        $('#programUpdateSuccessMessage').text('Spend settings saved successfully');
    }, function (respErr) {
        $('#loaderModal').modal('hide');
        $('#programUpdateSuccess').modal('show');
        $('#programUpdateSuccessMessage').text('Failed to save setting');
        $('.successImage').hide();
        $('.errorImage').hide();
    })
}

var disableAllowedToTimeDropdown = function(fromTime, setDefaultVal) {
    fromTime = parseInt(fromTime);
    $('#timeSliceToContainer option').each(function() {
        $(this).removeAttr("disabled");
        let dropDownValue = parseInt(this.value);
        if(dropDownValue < fromTime) {
            $(this).attr("disabled", true);
        }

        if(setDefaultVal && dropDownValue != 0 && dropDownValue == fromTime) {
            $(this).attr("selected",true);
        }
    });
}

var replaceMonthByNumberValue = function(days){
    let numberValuesForDays = {0:"SUN",1:"MON",2:"TUE",3:"WED",4:"THU",5:"FRI",6:"SAT"}
    days = days.toString().split(',');
    let returnData = []
    for(var day of days){
        if(!numberValuesForDays[day]){
            return days
        }else{
            returnData.push(numberValuesForDays[day])
        }
    }

    return returnData
}

export default {
    closeCard: function () {
        let employeeId = $(this).attr("data-employee-id");
        $("#closeCardLoader").modal("show");

        benefitDetailsCtrl.closeCard($("meta[name='companyID']").attr("content"), employeeId).then(function (respData) {
            $("#closeCardLoader").modal("hide");
            $("#closeCardSuccess").modal("show");
        }, function () {
            $("#closeCardLoader").modal("hide");
            $("#closeCardError").modal("show");
        });
    },

    moveSingleToAddPayout: function () {
        let link = $('#payoutAdd').data().linkorderid;
        window.location = link;
    },
    moveToMultipleAddPayout: function () {
        let link = $('#payoutAdd').data().linkrecurringorderid;
        window.location = link;
    },

    callAddPayout: function () {

        $("#addPayoutConfirmModal").modal("show");
        let orderId = $(this).data().payoutorderid;
        $('#addPayOrderId').text(orderId + ')');
        let reccuringID = $(this).data().recurringid;
        $('#addALLPayOrderId').text(reccuringID);
        if(!reccuringID){
            $("#addRecurringOrderIdButton").hide();
        }
        else{
                $("#addRecurringOrderIdButton").show();
            }

    },

    confirmCancel: function () {
        $("#cancelOrderConfirm").modal("show");
        let orderId = $(this).closest('tr').data("orderid");

        $('#cancelOrderConfirm .confirm').attr('data-orderid', orderId);


    },
    resetCancel: function () {
        $("#cancelOrderConfirm").modal("hide");
    },
    cancelOrderSuccess: function () {

        location.reload();
        $("#cancelOrderSuccess").modal("hide");

    },
    cancelOrder: function (e) {
        let orderId = $(this).data("orderid");
        $("#cancelOrderConfirm").modal("hide");
        $("#cancelOrderLoader").modal("show");
        $("#error-msg").hide();
        $("#cannot-revoke-msg").hide();
        benefitDetailsCtrl.cancelOrder(orderId).then(function (respData) {
            $("#cancelOrderLoader").modal("hide");
            $("#cancelOrderSuccess").modal("show");
            /*
            $("#cancelOrderLoader").modal("hide");
            $("#cancelOrderError").modal("show");
            $("#error-msg").show();
            */
        }, function (respErr) {
            $("#cancelOrderLoader").modal("hide");
            if (Util.checkGenie()) {
                $("#cancelOrderError").modal("show");
                $("#cannot-revoke-msg").show();
                $("#cannot-revoke-msg").text("You do not have access to cancel order");
            } else {
                $("#cancelOrderError").modal("show");
                $("#cannot-revoke-msg").show();
            }
        });
    },
    fundingAccountIDChange: function () {
        let $createProgram = $('#createProgram'),
            $programName = $('#programName'),
            $accountID = $('#fundingAccountID'),
            $showHideNewFundingAcc = $('#shownewfundingaccname'),
            $NewFundingAccName = $('#newfundingaccname');

        if ($accountID.find(":selected").text() == 'Create New') {
            $showHideNewFundingAcc.show();
            if ($accountID.val() == 0 || !$NewFundingAccName.val() || $NewFundingAccName.val().length > 40) {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
                $('#newfundingaccname').val(null)
            } else {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
                $('#newfundingaccname').val(null)
            }
        } else {
            $showHideNewFundingAcc.hide();
            if ($accountID.val() == 0) {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
                $('#newfundingaccname').val(null)
            } else {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
                $('#newfundingaccname').val(null)
            }
        }
    },
    newFundingAccountName: function () {
        let $createProgram = $('#createProgram'),
            $programName = $('#programName'),
            $accountID = $('#fundingAccountID'),
            $showHideNewFundingAcc = $('#shownewfundingaccname'),
            $NewFundingAccName = $('#newfundingaccname');

        if ($accountID.val() == 0 || !$NewFundingAccName.val() || $NewFundingAccName.val().length > 40) {
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $createProgram.removeAttr('disabled');
            $createProgram.removeClass('btn-disabled');
        }
        Storage.set('changeFundingAccName', $NewFundingAccName.val())
        let accountName = $NewFundingAccName.val();
        let allfundingAccountName = JSON.parse(localStorage.getItem('allFundingAccountName'));
        if (allfundingAccountName.indexOf(accountName) > -1) {
            $('#showfundingAccDuplicate').show();
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $('#showfundingAccDuplicate').hide();
        }
    },
    changeAndSetupProgram: function () {
        let TypeofRequest;
        let fundValue = $('#fundingAccountID').val();
        let isNewAccount;
        if (fundValue == 'createNew') {
            TypeofRequest = '/createNewFundAndProgram';
            isNewAccount = false;
        } else {
            TypeofRequest = '/changeFundAcc';
            isNewAccount = false;
        }
        $('#createProgram').attr('disabled', 'disabled');
        $('#createProgram').addClass('btn-disabled');
        $('#createProgramLoader').show();
        $('#showFundAccountError').hide();
        $('#showGenieFundAccountError').hide();

        let url = TypeofRequest + '?fundingAccID=' + $('#fundingAccountID').val() + '&productType=' + $(this).attr('data-programType') +
            '&financialYear=' + $('#financialYear').val() + '&programID=' + $(this).attr('data-programid') + '&currency=INR' + '&newFundAccount=' + $('#newfundingaccname').val() + '&isNewAccount=' + isNewAccount;
        let headers = {
            'X-program-description': $(this).attr('data-programDescription'),
            'programname': $(this).attr('data-programname')
        };
        let x = $('#fundingAccountID').find(":selected").text().trim()
        var fundingAccName = fundValue == 'createNew' ? Storage.get('changeFundingAccName') : x.substring(0, x.indexOf('₹'));

        ServiceConnector.get(url, headers).then(function (respData) {
            $('#createProgramLoader').modal('hide');
            $('#setupProgramModal').modal('hide');
            // $('#changeFundAccProgramSuccess').modal('show');
            $('#changeFundAccProgramSuccess').modal({
                backdrop: 'static'
            })
            $('#changedFundAccountName').html(fundingAccName);
        }, function (respErr) {
            console.log('Error');
            $('#createProgramLoader').hide();
            $('#createProgram').removeAttr('disabled');
            $('#createProgram').removeClass('btn-disabled');
            if (Util.checkGenie()) { //&& (respErr.responseJSON.body) && (respErr.responseJSON.body.message == "Unauthorized")
                $('#showGenieFundAccountError').show();
            } else {
                $('#showFundAccountError').show();
            }
        })
    },
    reload: () => {
        window.location.reload()
    },
    benefitTransferOrder: function() {
        var programName = $(this).data('programname');
        var programId = $(this).data('program');
        initGAEvent('event', 'Zeta Optima', 'click', 'New Transfer Order Clicked, corpId=' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', programId=' + programId + ', programName=' + programName + ', timestamp=' + new Date().toLocaleString());
    },
    downloadOrderFile: function(e) {
        e.preventDefault();
        let url = $(this).attr('data-orderFileAPiUrl');
        TransferDetailCtrl.downloadOrderFile(url).then(function (respData) {
            var win = window.open(Util.htmlDecoder(respData.downloadUrl), '_blank');
            win.focus();
        }, function(respErr) {});
    },
    downloadOrderReport: function(e) {
        e.preventDefault();
        let url = $(this).attr('data-orderReportAPiUrl');
        TransferDetailCtrl.downloadOrderReport(url).then(function (respData) {
            var win = window.open(Util.htmlDecoder(respData.location), '_blank');
            win.focus();
        }, function(respErr) {});
    },
    initProgramSetup: function() {
        $('input[type=radio][name=programType]').change(function() {
            if (this.value == 'allowance') {
                $('#allowancePara').show();
                $('#reimbursementPara').hide();
                $('#claimRequiredAction').show()
                if($('input[name=claimUpload]:checked').val() == 'Yes'){
                    $('div[data-target="#multiCollapseExample4"]').show()
                }else{
                    $('div[data-target="#multiCollapseExample4"]').hide()
                    $('#multiCollapseExample4').removeClass('in');
                }
            } else {
                $('#reimbursementPara').show();
                $('#allowancePara').hide();
                $('#claimRequiredAction').hide();
                $('div[data-target="#multiCollapseExample4"]').show();
            }
        });
        $('input[type=radio][name=remainingbalance]').change(function() {
            if (this.value == 'moveToUserCashCard') {
                $('#cashPara').show();
                $('#fundingPara').hide();
            } else {
                $('#fundingPara').show();
                $('#cashPara').hide();
            }
        });
        $('input[type=radio][name=closureremainingbalance]').change(function() {
            if (this.value == 'MOVE_TO_CASH_CARD') {
                $('#closurecashPara').show();
                $('#closurefundingPara').hide();
            } else {
                $('#closurefundingPara').show();
                $('#closurecashPara').hide();
            }
        });

        $('input[name=to-be-closed]').change(function(){
            if(this.value == 'Yes'){
                $('.optionalClosure').removeClass('hideMe');
            }else{
                $('.optionalClosure').addClass('hideMe');
            }
        })

        $('input[name=expire-balance]').change(function(){
            if(this.value == 'Yes'){
                $('.optionalExpiry').removeClass('hideMe');
            }else{
                $('.optionalExpiry').addClass('hideMe');
            }
        })

        $('#fundingAccountId').change(function(){
            if($(this).val() == 'createNew'){
                $('#fundingAccountName').show();
            }else{
                $('#fundingAccountName').removeClass('errorField')
                $('#fundingAccountName').hide();
            }
        })

        $('input[name=claimUpload]').change(function(){
            if(this.value == 'Yes'){
                $('div[data-target="#multiCollapseExample4"]').show()
            }else{
                $('div[data-target="#multiCollapseExample4"]').hide()
                $('#multiCollapseExample4').removeClass('in');
            }
        })

        $('.multiSelectOpts').multiselect({
            columns: 1,
            search: true,
            selectAll: true,
            nonSelectedText: 'Multi select categories',
            texts: {
                placeholder: '- Multi select categories -',
                nonSelectedText: 'Multi select categories',
                search: 'Search categories'
            }
        });
        $('#spendDays').multiselect({
            columns: 1,
            search: true,
            selectAll: true,
            nonSelectedText: 'Select days',
            texts: {
                placeholder: 'Select days',
                nonSelectedText: 'Select days',
                search: 'Search days'
            }
        });

        $('#allowedMerchants, #disallowedMerchants').multiselect({
            columns: 1,
            search: true,
            selectAll: true,
            nonSelectedText: 'Select merchants',
            texts: {
                placeholder: 'Select merchants',
                nonSelectedText: 'Select merchants',
                search: 'Search merchants'
            }
        });
        setTimeout(function() {
            $('.multiSelectOpts').multiselect('reset', [])
        }, 2000)

        var timeSlices = $('#timeSliceContainer').data('timeslices') || [];
        var countryName = cardProgramObjectData && cardProgramObjectData.productType == 'sodexo_cafeteria' ? cardProgramObjectData.currency.currencyCode : '';
        if(countryName == 'VND') {
            var allowspendsData = $('#allowspendsContainer').data('allowedspenddays') || '';
            var timeSliceData = timeSlices[0] ? timeSlices[0].split("-") : [];
            var timeFrom = timeSliceData[0] || '';
            var timeTo = timeSliceData[1] || '';
            DomEventHandler.appendToMyTemplate('timeSliceFromContainer', TimeFromFramesVnd, { time: timeFrom});
            DomEventHandler.appendToMyTemplate('timeSliceToContainer', TimeToFramesVnd, { time: timeTo });
            DomEventHandler.appendToMyTemplate('allowspendsContainer', SpendDaysVnd, { selectedSpendDays: replaceMonthByNumberValue(allowspendsData)});
            disableAllowedToTimeDropdown(timeFrom, false);
            $('#spendDays').multiselect({
                columns: 1,
                search: true,
                selectAll: true,
                nonSelectedText: 'Select days',
                texts: {
                    placeholder: 'Select days',
                    nonSelectedText: 'Select days',
                    search: 'Search days'
                }
            });
        } else {
            if (timeSlices.length == 0) {
                addMoreRow('false', '', '', countryName);
            }
            if (timeSlices.length > 0) {
                addMoreRow('false', timeSlices[0].split('-')[0], timeSlices[0].split('-')[1], countryName);
            }
            if (timeSlices.length > 1) {
                for (var i = 1; i < timeSlices.length; i++) {
                    addMoreRow('true', timeSlices[i].split('-')[0], timeSlices[i].split('-')[1], countryName);
                }
            }
        }
        // if(cardProgramObjectData && cardProgramObjectData.productType == 'sodexo_cafeteria' && cardProgramObjectData.currency.currencyCode == 'VND'){
            // $('.timeFrom, .timeTo').change(function(){
            //     let timeSelected = $(this).val()
            //     $(this).parent().find('p').text(getVNDTime(timeSelected));
            // })
            // if($('.timeFrom').val()){
            //     let timeSelected = $('.timeFrom').val()
            //     $('.timeFrom').parent().find('p').text(getVNDTime(timeSelected));
            // }
            // if($('.timeTo').val()){
            //     let timeSelected = $('.timeTo').val()
            //     $('.timeTo').parent().find('p').text(getVNDTime(timeSelected));
            // }
        // }

        $('#saveGeneralSettings').click(function() {
            var fundingAccountId = $('#fundingAccountId').val()
            var fundingAccName = $.trim($('#fundingAccountName').val());
            var requestBody = prepareGeneralSetting();
            if (!fundingAccountId) {
                $('#fundingAccountID').addClass('errorField');
                // scrollToTab('multiCollapseExample2');
                return false
            } else {
                $('#fundingAccountID').removeClass('errorField');
            }

            if(fundingAccountId == 'createNew' && !fundingAccName){
                $('#fundingAccountName').addClass('errorField');
                // scrollToTab('multiCollapseExample2');
                return false;
            }
            if(fundingAccountId == 'createNew' && fundingAccName != ""){
                let inputObj = {
                    'corpID': Meta.getMetaElement("corpID"),
                    'name': fundingAccName
                }
                benefitSvc.createFundingAccount(inputObj).then(function(resp){
                    requestBody.fundingAccountID = resp.accountID;
                    updateCustomCardProgram('General', requestBody, fundingAccName);
                }, function(err){
                    $('#loaderModal').modal('hide');
                    $('#programUpdateSuccess').modal('show');
                    $('#programUpdateSuccessMessage').text('Failed create funding account');
                    $('.successImage').hide();
                    $('.errorImage').hide();
                })
            }else{
                updateCustomCardProgram('General', requestBody);
            }
        })
        $('#saveSpendSettings').click(function() {
            $('select').removeClass('errorField')
            let validated = true;
            $('#timeSliceContainer .selectRows').each(function(i, element) {
                var timeFrom = parseInt($(element).find('.timeFrom').val())
                var timeTo = parseInt($(element).find('.timeTo').val())
                if ((timeFrom != 'select' && timeTo == 'select') || (timeTo != 'select' && timeFrom == 'select')) {
                    $(this).find('select').addClass('errorField');
                    validated = false;
                }
            })
            if(validated){
                var requestBody = prepareSpendSetting();
                updateCustomCardProgram('Spend', requestBody)
            }
        })

        $('#saveSpendSettings_VND').click(function() {
            $('#timeSliceFromContainer .timeFrame').removeClass('errorField');
            $('#timeSliceToContainer .timeFrame').removeClass('errorField');
            let validated = true;
            var timeFrom = $('#timeSliceFromContainer .timeFrame').val();
            var timeTo = $('#timeSliceToContainer .timeFrame').val()
            if(timeFrom == "select") {
                validated = false;
                $('#timeSliceFromContainer .timeFrame').addClass('errorField');
            }
            if(timeTo == "select") {
                validated = false;
                $('#timeSliceToContainer .timeFrame').addClass('errorField');
            }
            if(validated) {
                updateOTmealSpendSetting(timeFrom, timeTo);
            }
        })

        $('#saveAddressSettings').click(function() {
            $('input').removeClass('errorField')
            var validated = true;
            $('#addressSection input').each(function(){
                if($(this).attr('id') != "address2"){
                    if($.trim($(this).val())){
                        $(this).removeClass('errorField');
                    }else{
                        $(this).addClass('errorField');
                        validated = false;
                    }
                }
            })
    
            if(!Util.validateEmail($('#email').val())){
                $('#email').addClass('errorField');
                validated = false;
            }else{
                $('#email').removeClass('errorField');
            }
            if(validated){
                var requestBody = prepareAddressSetting();
                updateCustomCardProgram('Address', requestBody)
            }
        })

        $('#saveClaimSettings').click(function() {
            var requestBody = prepareClaimSetting();
            updateCustomCardProgram('Claim', requestBody)
        })
        $('#saveExpirySettings').click(function() {
            var requestBody = prepareExpirySetting();
            updateCustomCardProgram('Expiry', requestBody)
        })
        $('#saveCloseCardSettings').click(function() {
            var requestBody = prepareClosureSetting();
            updateCustomCardProgram('Year end closure', requestBody)
        })

        $('#multiCollapseExample3 input').prop('disabled', true);
        $('#multiCollapseExample3 select').prop('disabled', true);
        $('#addMoreRow, .removeRow').hide();

        $('.enableEdit').click(function() {
            $(this).hide();
            $('.' + $(this).data('show')).show();
            $('.' + $(this).data('hide')).hide();
            $('.' + $(this).data('button')).show();
            if($(this).data('show') == 'spendEdit'){
                $('#multiCollapseExample3 input').prop('disabled', false);
                $('#multiCollapseExample3 select').prop('disabled', false);
                $('#addMoreRow, .removeRow').show();
            }
        })

        $('.cancelEdit').click(function() {
            $(this).closest('.inputField').hide();
            $(this).closest('.inputRow').find('.enableEdit').show();
            $('.' + $(this).data('show')).hide();
            $('.' + $(this).data('hide')).show();
            if($(this).data('show') == 'spendEdit'){
                $('#multiCollapseExample3 input').prop('disabled', true);
                $('#multiCollapseExample3 select').prop('disabled', true);
                $('#addMoreRow, .removeRow').hide();
            }
            if($(this).data('show') == 'billingEdit'){
                $('#multiCollapseExample2 input').prop('disabled', false);
            }
        })

        $('#closeProgramOn').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        });
        $('#timeSliceFromContainer .timeFrame').change(function() {
            disableAllowedToTimeDropdown(this.value, true);
        });
    },

    uploadCardProgramLogo: function () {
        $('#loaderModal').modal('show');
        let file = $(this)[0].files[0];
        if (file) {
        $('#cardLogo').html(file.name);
            benefitSvc.getAssetUploadEndPoint().then(function(respData) {
                let cardLogoData = new FormData();
                let cardLogoUploadUrl = respData.postAction;
                cardLogoData.append('x-amz-algorithm', respData.xAmzAlgorithm);
                cardLogoData.append('x-amz-signature', respData.xAmzSignature);
                cardLogoData.append('acl', respData.acl);
                cardLogoData.append('success_action_status', respData.successActionStatus);
                cardLogoData.append('key', respData.key);
                cardLogoData.append('policy', respData.policy);
                cardLogoData.append('x-amz-date', respData.xAmzDate);
                cardLogoData.append('x-amz-credential', respData.xAmzCredential);
                cardLogoData.append('content-type', 'image/png');
                cardLogoData.append('file', file);

                let headers = {
                    'content-Type': 'multipart/form-data'
                };
                
                updateCardProgramLogo(cardLogoData, cardLogoUploadUrl, headers, file.name);
            }, function (respErr) {
                $('#loaderModal').modal('hide');
            });
        } else {
            $('#loaderModal').modal('hide');
        }
    },
   
}
import TransferDetailCtrl from "../controller/transferDetailCtrl";
import InstagiftSvc from '../service/spotlight/instagift-svc';
import OrderSvc from '../service/ordersService';
import Util from "../common/util";
import DomEventHandler from "../common/domEventHandler.js";
import TransferOrderHistory from "../../template/Modal/tranfer-order-history.ejs";

function getDOMElts() {
    return {
        checkedList : '#payoutstable input.select-chkbox',
        subContainer : '.sub-container',
        selectAllChkBox : '#payoutstable input.selectall',
        payoutTableRow : '#payoutstable tbody.main-content tr'
    }
}
function getChkedItems() {
    let chkList = [ ...document.querySelectorAll(getDOMElts().checkedList) ];
    return chkList.filter((item) => {
        return item.checked;
    });
}
function changeChkBoxState(list, state) {
    return list.map((item) => {
        return item.checked = state;
    });
}
function getBulkRevokeCount(response) {
    let payoutsList = response.payoutResponses,
        total = response.payoutResponses.length,
        count = 0;
    for(let payout of payoutsList){
        count = (payout.success == true) ? (count + 1) : count;
    }
    
    return {
        totalCount : total,
        successCount : count
    }
}

function fetchHistoryData(params) {

    const apiList = [
        OrderSvc.getOrderHistory(params.orderId, "order"), 
        OrderSvc.getOrderHistory(params.payoutId, "payout")
    ];

    return Promise.all(apiList)
        .then(apiResponse => {
            const orderHistoryList = apiResponse[0].auditDataList.filter(data => data.action == 'CREATE');
            const allHistoryData = apiResponse[1].auditDataList.concat(orderHistoryList);
            const finalData = OrderSvc.getFormattedOrderDetails(allHistoryData)
            if(finalData.records && finalData.records.length > 0) {
                $('.updated-by-name').text(finalData.records[0].name);
            } else {
                $('.latest-order-update').hide();
            }
            return finalData;
        })
}

export default {
    downloadOrderFile: function (e) {
        e.preventDefault();
        let url = $(this).attr('data-orderFileAPiUrl');
        TransferDetailCtrl.downloadOrderFile(url).then(function (respData) {
            var win = window.open(Util.htmlDecoder(respData.downloadUrl), '_blank');
            win.focus();
        }, function (respErr) {
        });
    },
    downloadOrderReport: function (e) {
        e.preventDefault();
        let url = $(this).attr('data-orderReportAPiUrl');
        TransferDetailCtrl.downloadOrderReport(url).then(function (respData) {
            var win = window.open(Util.htmlDecoder(respData.location), '_blank');
            win.focus();
        }, function (respErr) {
        });
    },
    cancelPayout: function (e) {
        let payoutId = $(this).data("payout-id"),
            cancelAllScheduledPayouts = $(this).attr("data-cancelAll"),
            orderId = $(this).data("order-id"),
            cancelPayoutLoader = $("#cancelPayoutLoader"),
            cancelPayoutSuccess = $("#cancelPayoutSuccess"),
            cancelPayoutError = $("#cancelPayoutError")
        //  cancelAllScheduledPayouts = $(this).data("cancelAll")

        $("#cancelPayoutConfirm").modal("hide");
        cancelPayoutLoader.modal("show");
        cancelPayoutError.hide();
        TransferDetailCtrl.cancelPayout(orderId, payoutId, cancelAllScheduledPayouts).then(function (respData) {
            cancelPayoutLoader.modal("hide");
            // if (respData.status == 200) {
            cancelPayoutSuccess.modal("show");
            // } else {
            //     cancelPayoutError.modal("show");
            // }
        }, function (respErr) {
            cancelPayoutLoader.modal("hide");
            let checkGenie = 0;
            if (localStorage.getItem('checkGenie')) {
                checkGenie = parseInt(localStorage.getItem('checkGenie'));
            }
            if (checkGenie == 0) {
                cancelPayoutError.modal("show");
            } else {
                $("#cancelPayoutErrorGenie").modal("show");
            }
        });
    },
    revokePayout: function (e) {
        let payoutId = $(this).data("payout-id"),
            orderId = $(this).data("order-id"),
            revokePayoutLoader = $("#revokePayoutLoader"),
            revokePayoutSuccess = $("#revokePayoutSuccess"),
            revokePayoutError = $("#revokePayoutError"),
            revokeBulkPayouts = $('#revokeBulkPayout');
        let remarks, payoutIds=[];

        if((typeof payoutId) == 'number') {
            //in case of single payout selected
            payoutIds.push(payoutId);
        } else {
            //in case of multiple payouts selected
            payoutIds = payoutId.split(',').map((item) => {
                return parseInt(item, 10);
            });
        }
    
        if ($('#revokeOptions').val() == "others")
        {
            remarks =  $('#revokeOthersText').val();
        }
        else {
            remarks = $('#revokeOptions').val();
        }

        $("#revokePayoutConfirm").modal("hide");
        revokePayoutLoader.modal("show");
        $("#error-msg").hide();
        $("#cannot-revoke-msg").hide();
        TransferDetailCtrl.revokePayout(orderId, payoutIds, remarks).then(function (respData) {
            let countObj = getBulkRevokeCount(respData);
            revokePayoutLoader.modal("hide");
            if(countObj.totalCount == 1) {
                if (respData.payoutResponses[0].status == 200 || respData.payoutResponses[0].success) {
                    revokePayoutSuccess.modal("show");
                } else {
                    revokePayoutError.modal("show");
                    $("#cannot-revoke-msg").show();
                }
            } else {
                revokeBulkPayouts.modal("show");
                $('#revoke-successCount').text(countObj.successCount);
                $('#revoke-totalCount').text(countObj.totalCount);
            }
        }, function (respErr) {
            let countObj = (respErr.payoutResponses) ? getBulkRevokeCount(respErr) : false;

            revokePayoutLoader.modal("hide");
            if (Util.checkGenie()) {
                revokePayoutError.modal("show");
                $("#error-msg").show();
                $("#error-msg").text("You do not have access to revoke payout.");
            } else if(countObj) {
                if(countObj.totalCount == 1) {
                    if (respErr.payoutResponses[0].status == 200 || respErr.payoutResponses[0].success) {
                        revokePayoutSuccess.modal("show");
                    } else {
                        revokePayoutError.modal("show");
                        $("#error-msg").show();
                    }
                } else if(countObj.totalCount > 1) {
                    revokeBulkPayouts.modal("show");
                    $('#revoke-successCount').text(countObj.successCount);
                    $('#revoke-totalCount').text(countObj.totalCount);
                }
            } else {
                revokePayoutError.modal("show");
                $("#error-msg").show();
            }
        });
    },
    confirmCancelPayout: function () {
        let payoutId = $(this).closest('tr').data("payout-id"),
            orderId = $(this).closest('tr').data("order-id");
        $("#cancelPayoutConfirm").modal("show");
        $('#cancelPayoutConfirm .confirm')
            .attr('data-order-id', orderId)
            .attr('data-payout-id', payoutId);
    },
    resetCancelPayout: function () {
        $("#cancelPayoutConfirm").modal("hide");
    },
    confirmRevokePayout: function () {
        let payoutId = [], orderId = document.querySelector(getDOMElts().payoutTableRow).getAttribute('data-order-id');
        let chkedList = getChkedItems();
        if(chkedList.length == 0) {
            //individual revoke fund
            payoutId = $(this).closest('tr').data("payout-id");
        } else {
            //bulk revoke
            payoutId = chkedList.map((item) => {
                return item.getAttribute('data-payout-id');
            });
        }

        $("#revokePayoutConfirm").modal("show");
        $('#revokePayoutConfirm .confirm')
            .attr('data-order-id', orderId)
            .attr('data-payout-id', payoutId);
    },
    rowSelect: function (e) {
        //when checkbox is selected
        let parentElt = document.querySelector(getDOMElts().subContainer).classList;
        let totalList = [ ...document.querySelectorAll(getDOMElts().checkedList) ];
        let selectAllCheckBox = document.querySelector(getDOMElts().selectAllChkBox);
        totalList = totalList.filter((item) => {
            if(!item.classList.contains('disabled'))
                return item;
        });
        if(!parentElt.contains("bulk-action")) {
            parentElt.add("bulk-action");
        }
        let chkedList = getChkedItems();
        if(chkedList.length === 0) {
            parentElt.remove("bulk-action");
        }
        if(chkedList.length === totalList.length) {
            selectAllCheckBox.checked = true;
            selectAllCheckBox.classList.add('selected');
        } else if (selectAllCheckBox.classList.contains('selected')){
            selectAllCheckBox.checked = false;
            selectAllCheckBox.classList.remove('selected');
        }
    },
    selectAllChkbox: function(e) {
        //select all , unselect all checkboxes for bulk action
        let elt = e.target;
        let parentElt = document.querySelector(getDOMElts().subContainer).classList;
        let chkList = [ ...document.querySelectorAll(getDOMElts().checkedList) ];
        chkList = chkList.filter((item) => {
            if(!item.classList.contains('disabled'))
                return item;
        });
        let chkedList = [];
        if(chkList.length > 0) {
            if(elt.classList.contains('selected')) {
                chkedList = changeChkBoxState(chkList, false);
                elt.classList.remove('selected');
                parentElt.remove("bulk-action");
            } else {
                chkedList = changeChkBoxState(chkList, true);
                elt.classList.add('selected');
                parentElt.add("bulk-action");
            }
        } else {
            this.checked = false;
        }
    },
    cancelBulkRevokePayout: function () {
        //cancel bulk revoke after making one or more selection
        let parentElt = document.querySelector(getDOMElts().subContainer).classList;
        let chkList = [ ...document.querySelectorAll(getDOMElts().checkedList) ];
        for(let i of chkList){
            i.checked = false;
        } 
        let selectAllCheckBox = document.querySelector(getDOMElts().selectAllChkBox);
        selectAllCheckBox.checked = false;
        selectAllCheckBox.classList.remove('selected');
        parentElt.remove("bulk-action");
    },
    resetRevokePayout: function () {
        $("#revokePayoutConfirm").modal("hide");
    },
    revokePaymentModalShow: function () {
        $('#confirmRevoke').removeClass('btn-disabled');
        $('#confirmRevoke').prop('disabled', false);
        $('#confirmRevokeLoader').hide();
        let orderID = $(this).data('orderid');
        let payoutID = $(this).data('payoutid');
        $('#confirmRevoke').data('orderid', orderID);
        $('#confirmRevoke').data('payoutid', payoutID);
        $('#revokePayoutModal').modal('show');
    },
    revokePayment: function () {
        $('#confirmRevoke').addClass('btn-disabled');
        $('#confirmRevoke').prop('disabled', true);
        $('#confirmRevokeLoader').show();

        let requestPayload = {
            "corpID": $("meta[name='corpID']").attr("content"),
            "companyID": $("meta[name='companyID']").attr("content"),
            "orderID": $(this).data('orderid'),
            "payoutID": $(this).data('payoutid'),
            "remarks": "revoked payment, orderID: " + $(this).data('orderid')
        };
        InstagiftSvc.revokePayment(requestPayload).then(function (successResponse) {
            let amtObj = successResponse.amount;
            let amount = amtObj.amount / 100;
            let message = "₹" + amount + " was successfully revoked";
            $('#revokePayoutModal').modal('hide');

            $('#successImg').show();
            $('#errorResponse').html("");
            $('#errorResponseMsg').html(message);
            $('#responseBtn').html('DONE');

            $('#requestResponseModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#requestResponseModal').modal('show');
        }, function (errorResponse) {
            let errorObj = JSON.parse(errorResponse.responseText);

            $('#revokePayoutModal').modal('hide');
            $('#errorImg').show();
            $('#errorResponse').html(errorResponse.statusText);
            $('#errorResponseMsg').html(errorObj.message);
            $('#responseBtn').html('DONE');

            $('#requestResponseModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#requestResponseModal').modal('show');
        });
    },
    reloadPage: function () {
        $('#responseBtn').addClass('btn-disabled');
        $('#responseBtn').prop('disabled', true);
        location.reload();
    },

    viewOrderHistoryList: function () {
        DomEventHandler.renderMyTemplate('showOrderHistoryListModal', TransferOrderHistory, {});
        $('#showOrderHistoryListModal').modal('show');
        let payoutId = $(this).closest('tr').data('payout-id');
        let orderId = $('#orderID').val();
        $("#orderHistory").table({
            source: fetchHistoryData,
            tableTemplate: "/template/tables/transfer-order-history.ejs",
            rowTemplate: "/template/tables/transfer-order-history-row.ejs",
            extraParam: {
                payoutId: payoutId,
                orderId: orderId
            },
        });
    }
};

import DocumentSvc from "../service/docStoreSvc";
import RowTemplate from "../../template/tables/doc-item-row.ejs";
import DocumentItemTemplate from "../../template/documentItem.ejs";
import DomEventHandler from "../common/domEventHandler";
import AppLabels from "../common/appLabels";
import Util from "../common/util";
import Constant from "../common/constants";
import Meta from "../common/metaStorage";
import Storage from '../common/webStorage'
let _searchDocs = function(
  searchTxt,
  programType,
  statusType,
  dateRange,
  docSource
) {
  $("#searchingTxt").show();
  $("#loadMoreDocsBtn").hide();
  let phoneno = /^\d{10}$/;
  let shortClaimId = /^[0-9]+-[a-zA-Z0-9]{4}$/;
  let startInclusive = $("#docStoreList .doc-store-item-wrpr").length;
  let endInclusive = startInclusive + 11;
  let searchTxtArr = [];
  let productTypeArr = [];
  let status = [];
  let businessId = $(".company-dropdown .dropdown-toggle").attr("data-company");
  let corpID = $("meta[name='corpID']").attr("content");
  let companyId = $(".company-dropdown .dropdown-toggle")
    .attr("data-company")
    .split("-")
    .pop();
  dateRange = dateRange.split("-");
  let startFromDate = $.trim(dateRange[0]);
  let endFromDate = $.trim(dateRange[1]);
  startFromDate = startFromDate.split(" ");
  endFromDate = endFromDate.split(" ");
  dateRange =
    startFromDate[2] +
    "-" +
    Util.monthNameToMonth(startFromDate[1]) +
    "-" +
    startFromDate[0] +
    " - " +
    endFromDate[2] +
    "-" +
    Util.monthNameToMonth(endFromDate[1]) +
    "-" +
    endFromDate[0];
  let startDate = Util.dateToepochTime(dateRange.slice(0, 10), "-", "after");
  let endDate = Util.dateToepochTime(dateRange.slice(13, 23), "-", "before");
  let directoryPath = [];
  if (statusType != "none") {
    directoryPath.push(
      "biller/claims",
      "optima/proofs/" + corpID + "/" + companyId
    );
  } else {
    directoryPath.push(
      "optima/travel_log/" + corpID + "/" + companyId,
      "optima/employee_proof/" + corpID + "/" + companyId
    );
  }
  let requestJson = {
    corpID: $("meta[name='corpID']").attr("content"),
    token: Storage.get('authToken'),
    docGroupsRequest: {
      directoryPaths: directoryPath,
      owner:
        $(".company-dropdown .dropdown-toggle")
          .attr("data-company")
          .split("-", 1)[0] + "@business.zeta.in",
      attrs: {},
      range: {
        startInclusive: startInclusive,
        endInclusive: endInclusive
      },
      startDate: startDate,
      endDate: endDate
    }
  };
  if (Util.isEmail(searchTxt)) {
    searchTxtArr[0] = searchTxt;
    requestJson.docGroupsRequest.attrs["email"] = searchTxtArr;
  } else if (searchTxt.match(phoneno)) {
    searchTxtArr[0] = "+91" + searchTxt;
    requestJson.docGroupsRequest.attrs["phone"] = searchTxtArr;
  } else if (searchTxt.match(shortClaimId)) {
    requestJson.docGroupsRequest.isPrefix = false;
    requestJson.docGroupsRequest.docGroupName = searchTxt;
  } else {
    searchTxtArr[0] = searchTxt;
    requestJson.docGroupsRequest.attrs["name"] = searchTxtArr;
  }

  let allProgramsSelected =
    $("#programType option:not(:selected)").length === 0;
  if (allProgramsSelected) {
    //   requestJson.docGroupsRequest.directoryPath.push("optima/proofs/"+corpID+"/"+companyId);
    requestJson.docGroupsRequest.attrs["producttype"] =
      Constant.ALL_DOC_AND_CORPBEN_CARD_PROGRAM_TYPES;
  } else if ($("#programType option:selected").length === 0) {
    requestJson.docGroupsRequest.attrs["producttype"] = [""];
  } else {
    productTypeArr = programType;
    requestJson.docGroupsRequest.attrs["cardprogramid"] = productTypeArr;
  }

  if (statusType && statusType != "none") {
    statusType.forEach(statusTypeEl => {
      if (
        statusTypeEl === "UPLOADED" ||
        statusTypeEl === "DECLINED" ||
        statusTypeEl === "proof_submitted"
      ) {
        status.push(statusTypeEl);
      } else if (statusTypeEl === "APPROVED") {
        status.push(
          "APPROVED",
          "PAID",
          "UNPAID",
          "PARTIALLY_PAID",
          "PARTIALLY_APPROVED"
        );
      }
    });
    if (statusType.length === 0) {
      status.push("");
    }
    requestJson.docGroupsRequest.attrs["state"] = status;
  }

  DocumentSvc.getSearchDocs(requestJson).then(
    function(respData) {
      $("#searchingTxt").hide();
      $("#loadMoreDocsBtn").show();
      if (respData.docGroupsInfos.length < 12) {
        $("#loadMoreDocsBtn").hide();
      }
      for (var i = 0; i < respData.docGroupsInfos.length; i++) {
        respData.docGroupsInfos[i].attrs["ProductType"] =
          AppLabels.PROGRAM_TYPES[respData.docGroupsInfos[i].attrs.producttype];
        if (respData.docGroupsInfos[i].attrs.claimAmount) {
          respData.docGroupsInfos[i].attrs.claimAmount = Util.formatINR(
            Number(respData.docGroupsInfos[i].attrs.claimAmount) / 100
          );
        }
        if (respData.docGroupsInfos[i].attrs.state) {
          respData.docGroupsInfos[i].attrs.state = respData.docGroupsInfos[
            i
          ].attrs.state.toLowerCase();
        }
        if (
          respData.docGroupsInfos[i].attrs.state === "approved" ||
          respData.docGroupsInfos[i].attrs.state === "paid" ||
          respData.docGroupsInfos[i].attrs.state === "unpaid" ||
          respData.docGroupsInfos[i].attrs.state === "partially_paid" ||
          respData.docGroupsInfos[i].attrs.state === "partially_approved"
        ) {
          respData.docGroupsInfos[i].attrs.state = "approved";
        }
        if (
          typeof respData.docGroupsInfos[i].docGroupID.split(
            "biller/claims/"
          )[1] == "undefined"
        ) {
          let docGroupIdTemp = "";
          if (
            typeof respData.docGroupsInfos[i].docGroupID.split(
              "optima/proofs/"
            )[1] == "undefined"
          ) {
            if (
              respData.docGroupsInfos[i].docGroupID.indexOf(
                "optima/employee_proof/"
              ) > -1
            ) {
              docGroupIdTemp = respData.docGroupsInfos[i].docGroupID.split(
                "optima/employee_proof/"
              )[1];
            } else {
              docGroupIdTemp = respData.docGroupsInfos[i].docGroupID.split(
                "optima/travel_log/"
              )[1];
            }
          } else {
            docGroupIdTemp = respData.docGroupsInfos[i].docGroupID.split(
              "optima/proofs/"
            )[1];
          }
          let doccorpId = docGroupIdTemp.split("/", 1)[0];
          let doccompanyid = docGroupIdTemp.split("/", 2)[1];
          let docprogramid = docGroupIdTemp.split("/", 3)[2];
          let docemployeeid = docGroupIdTemp.split("/", 4)[3];
          let doccardid = docGroupIdTemp.split("/", 5)[4];
          let docrand = docGroupIdTemp.split("/").pop();

          if (
            typeof respData.docGroupsInfos[i].docGroupID.split(
              "optima/proofs/"
            )[1] == "undefined"
          ) {
            if (
              respData.docGroupsInfos[i].docGroupID.indexOf(
                "optima/employee_proof/"
              ) > -1
            ) {
              respData.docGroupsInfos[i].docGroupID =
                doccorpId +
                "+-" +
                doccompanyid +
                "+-" +
                docprogramid +
                "+-" +
                docemployeeid +
                "+-" +
                doccardid +
                "+-" +
                docrand +
                "+-ep"; // employee proof
            } else {
              respData.docGroupsInfos[i].docGroupID =
                doccorpId +
                "+-" +
                doccompanyid +
                "+-" +
                docprogramid +
                "+-" +
                docemployeeid +
                "+-" +
                doccardid +
                "+-" +
                docrand +
                "+-tl"; // travel log
            }
          } else {
            respData.docGroupsInfos[i].docGroupID =
              doccorpId +
              "+-" +
              doccompanyid +
              "+-" +
              docprogramid +
              "+-" +
              docemployeeid +
              "+-" +
              doccardid +
              "+-" +
              docrand +
              "+-pc";
          }
        } else {
          respData.docGroupsInfos[i].docGroupID = respData.docGroupsInfos[
            i
          ].docGroupID.split("biller/claims/")[1];
        }
      }
      let programTypes = JSON.parse(localStorage.getItem("programTypes"));
      // respData.programTypes = programTypes;
      if (respData.docGroupsInfos.length < 1) {
        $(".download-link").hide();
      } else {
        $(".download-link").show();
      }
      let documents = {
        docGroup: respData,
        businessId: businessId,
        programTypes: programTypes
      };
      DomEventHandler.appendToMyTemplate(
        "docStoreList",
        DocumentItemTemplate,
        documents
      );
    },
    function(respError) {
      $("#searchingTxt").hide();
      $("#loadMoreDocsBtn").hide();
      //   $('#noDocumentId').show();
      $(".download-link").hide();
      $("#docStoreList").append(
        '<div class="no-documents"><div class="no-documents"><div class="table-empty-state" style="display:block;"> <img src="https://card-program-files.s3.amazonaws.com/hrDashboard/images/table-empty-state.png" alt=""><p>No documents found</p></div></div></div>'
      );
      console.log(respError);
    }
  );
};
let _downloadDocs = function(searchTxt, programType, statusType, dateRange) {
  let phoneno = /^\d{10}$/;
  let searchTxtArr = [];
  let productTypeArr = [];
  let status = [];
  let businessId = $(".company-dropdown .dropdown-toggle").attr("data-company");
  let corpID = $("meta[name='corpID']").attr("content");
  let companyId = $(".company-dropdown .dropdown-toggle")
    .attr("data-company")
    .split("-")
    .pop();
  dateRange = dateRange.split("-");
  let startFromDate = $.trim(dateRange[0]);
  let endFromDate = $.trim(dateRange[1]);
  startFromDate = startFromDate.split(" ");
  endFromDate = endFromDate.split(" ");
  dateRange =
    startFromDate[2] +
    "-" +
    Util.monthNameToMonth(startFromDate[1]) +
    "-" +
    startFromDate[0] +
    " - " +
    endFromDate[2] +
    "-" +
    Util.monthNameToMonth(endFromDate[1]) +
    "-" +
    endFromDate[0];
  let startDate = Util.dateToepochTime(dateRange.slice(0, 10), "-", "after");
  let endDate = Util.dateToepochTime(dateRange.slice(13, 23), "-", "before");
  let requestJson = {
    corpID: $("meta[name='corpID']").attr("content"),
    token: Storage.get('authToken'),
    docGroupsRequest: {
      directoryPaths: [
        "biller/claims",
        "optima/proofs/" + corpID + "/" + companyId
      ],
      owner:
        $(".company-dropdown .dropdown-toggle")
          .attr("data-company")
          .split("-", 1)[0] + "@business.zeta.in",
      attrs: {},
      startDate: startDate,
      endDate: endDate
    }
  };
  if (Util.isEmail(searchTxt)) {
    searchTxtArr[0] = searchTxt;
    requestJson.docGroupsRequest.attrs["email"] = searchTxtArr;
  } else if (searchTxt.match(phoneno)) {
    searchTxtArr[0] = "+91" + searchTxt;
    requestJson.docGroupsRequest.attrs["phone"] = searchTxtArr;
  } else {
    searchTxtArr[0] = searchTxt;
    requestJson.docGroupsRequest.attrs["name"] = searchTxtArr;
  }
  let allProgramsSelected =
    $("#programType option:not(:selected)").length === 0;
  if (allProgramsSelected) {
    requestJson.docGroupsRequest.attrs["producttype"] =
      Constant.ALL_DOC_AND_CORPBEN_CARD_PROGRAM_TYPES;
  } else if ($("#programType option:selected").length === 0) {
    requestJson.docGroupsRequest.attrs["producttype"] = [""];
  } else {
    productTypeArr = programType;
    requestJson.docGroupsRequest.attrs["cardprogramid"] = productTypeArr;
  }

  if (statusType && statusType.length > 0) {
    statusType.forEach(statusTypeEl => {
      if (
        statusTypeEl === "UPLOADED" ||
        statusTypeEl === "DECLINED" ||
        statusTypeEl === "proof_submitted"
      ) {
        status.push(statusTypeEl);
      } else if (statusTypeEl === "APPROVED") {
        status.push(
          "APPROVED",
          "PAID",
          "UNPAID",
          "PARTIALLY_PAID",
          "PARTIALLY_APPROVED"
        );
      }
    });
    requestJson.docGroupsRequest.attrs["state"] = status;
  }

  DocumentSvc.getDocsDownloadStatus(requestJson).then(
    function(respData) {
      // success
    },
    function(respError) {
      console.log(respError);
      $("#downloadCompile").modal("hide");
      $(".modal-backdrop").remove();
      $("#downloadCompileError").modal("show");
    }
  );
};

export default {
  initFilterSelect: function() {
    $("#statusType").attr("multiple", "multiple");
    $("#statusType option").prop("selected", true);
    $("#statusType").multiselect({
      search: true,
      selectAll: true,
      maxPlaceholderOpts: 1,
      texts: {
        selectedOptions: " Statuses"
      }
    });
    $("#programType").attr("multiple", "multiple");
    $("#programType option").prop("selected", true);
    $("#programType").multiselect({
      search: true,
      selectAll: true,
      maxPlaceholderOpts: 1,
      texts: {
        selectedOptions: " Programs"
      }
    });
  },

  searchByDateRange: function(
    searchTxt,
    programType,
    statusType,
    dateRange,
    docSource
  ) {
    _searchDocs(searchTxt, programType, statusType, dateRange, docSource);
  },
  showOtherDocuments: function() {
    $(".all-status").hide();
    $(".all-documents").show();
    $("#searchDocs").attr("placeholder", "Name, Email or Mobile");
    let searchTxt = $("#searchDocs").val();
    let programType = $("#programType").val();
    let programName = $("#programType option:selected").text();
    let dateRange = $("#timePeriod").val();
    let statusType = "none";
    let docSource = $("#documentType").val();
    $("#docStoreList").empty();
    _searchDocs(searchTxt, programType, statusType, dateRange, docSource);
  },
  showClaimDetails: function() {
    $(".all-status").show();
    $(".all-documents").hide();
    $("#searchDocs").attr("placeholder", "Name, Email, Mobile or Claim-Id");
    let searchTxt = $("#searchDocs").val();
    let programType = $("#programType").val();
    let programName = $("#programType option:selected").text();
    let dateRange = $("#timePeriod").val();
    let statusType = $("#statusType").val();
    let docSource = "none";
    $("#docStoreList").empty();
    _searchDocs(searchTxt, programType, statusType, dateRange, docSource);
  },
  downloadDocuments: function() {
    $("#downloadCompileError").modal("hide");
    $("#downloadDocuments-modal").modal("show");
  },
  processDocuments: function() {
    $("#downloadDocuments-modal").modal("hide");
    $("#downloadCompileError").modal("hide");
    let dateRange = $("#timePeriod").val();
    let searchTxt = $("#searchDocs").val();
    let programType = $("#programType").val();
    let statusType = $("#statusType").val();
    $("#downloadCompile").modal("show");
    _downloadDocs(searchTxt, programType, statusType, dateRange);
  },
  closeModal: function() {
    $("#downloadCompile").modal("hide");
  },
  getCompanyPrograms: function() {
    let companyID = $(".company-dropdown .dropdown-toggle")
      .attr("data-company")
      .split("-")
      .pop();
    let programPageNumber = -1;
    let programMaxPage = 0;
    if (localStorage.getItem("programPageNumber")) {
      programPageNumber = parseInt(localStorage.getItem("programPageNumber"));
    }
    if (localStorage.getItem("programMaxPage")) {
      programMaxPage = parseInt(localStorage.getItem("programMaxPage"));
    }
    let pageSize = 12;
    if (programPageNumber == -1 || programPageNumber == programMaxPage) {
      // programPageNumber == -1 means there is no more program. So we don't need
      return "";
    } else {
      localStorage.setItem("scrollFlag", 1); // scroll called once
      let pageData = {
        programTypesNew: {}
      };
      function getRequiredPrograms(companyID, programPageNumber, pageSize) {
        DocumentSvc.getActiveCardProgramsPaginationPost(
          companyID,
          programPageNumber,
          pageSize
        ).then(
          function(respData) {
            // this we need to update card program dropdown
            let activeCards = respData;
            let programs = activeCards.programs;
            let programsCount = programs.length;
            for (let i = 0; i < programsCount; i++) {
              if (
                programs[i].cardProgramObject.spendConfiguration.channels
                  .allowReimbursement === true ||
                (programs[i].cardProgramObject.reimbursementProgram &&
                  programs[i].cardProgramObject.reimbursementProgram
                    .extraReimbursementProgramConfig &&
                  typeof programs[i].cardProgramObject.reimbursementProgram
                    .extraReimbursementProgramConfig.proofApprovalRequired !==
                    "undefined" &&
                  programs[i].cardProgramObject.reimbursementProgram
                    .extraReimbursementProgramConfig.proofApprovalRequired ==
                    false)
              ) {
                pageData.programTypesNew[
                  programs[i].cardProgramObject.cardProgramID
                ] = programs[i].cardProgramObject.cardName;
              }
            }
            var programTypesNew = pageData.programTypesNew;
            programPageNumber = programPageNumber + 1;
            localStorage.setItem("programPageNumber", programPageNumber);
            var len = $.map(programTypesNew, function(n, i) {
              return i;
            }).length; // length of object
            if (len > 5 || programPageNumber >= programMaxPage) {
              localStorage.setItem("scrollFlag", 0);
              // return programTypesNew;
              let option = "";
              if ($(".dropdown .gradient").length > 1) {
                option +=
                  '<li><a class="list" href="javascript:void(0)">All Programs<span class="value">0</span></a></li>';
              }
              for (var key in programTypesNew) {
                option +=
                  '<li><a class="list" href="javascript:void(0)">' +
                  programTypesNew[key] +
                  '<span class="value">' +
                  key +
                  "</span></a></li>";
              }
              if ($(".dropdown .gradient").length > 1) {
                $(".program-ul").html(option);
              } else {
                $(".program-ul").append(option);
              }
            } else {
              getRequiredPrograms(companyID, programPageNumber, pageSize);
            }
          },
          function(respError) {}
        );
      }

      // if(localStorage.getItem('programPageNumber')){
      //     programPageNumber= parseInt(localStorage.getItem('programPageNumber'));
      // }
      // var programTypesNew= pageData.programTypesNew;
      // var len = $.map(programTypesNew, function(n, i) { return i; }).length; // length of object
      // if((len < 6) || (programPageNumber < programMaxPage)) {
      if (localStorage.getItem("flag")) {
        let flag = parseInt(localStorage.getItem("flag"));
      }
      if (parseInt(localStorage.getItem("flag")) == 0) {
        localStorage.setItem("flag", 1);
        getRequiredPrograms(companyID, programPageNumber, pageSize);
      }
      // return pageData.programTypesNew;
      //   return '<li><a class="list" href="javascript:void(0)">Optima Medical Reimbursement<span class="value">US</span></a></li><li><a class="list" href="javascript:void(0)">Optima Medical Reimbursement<span class="value">US</span></a></li><li><a class="list" href="javascript:void(0)">Optima Medical Reimbursement<span class="value">US</span></a></li><li><a class="list" href="javascript:void(0)">Optima Medical Reimbursement<span class="value">US</span></a></li>';
    }
  },
  clickDocRow: function(e) {
    let rowIndex = $(this).attr("data-value");
    let docId = $(this).attr("data-doc-id");
    let rowItem = $(".rowBox_" + rowIndex);
    rowItem.toggle();
    rowItem.find(".main-content").hide();
    rowItem.find(".table-empty-state").hide();
    rowItem.find(".table-error-state").hide();
    rowItem.find(".loading-content").show();
    let requestJson = {
      corpID: $("meta[name='corpID']").attr("content"),
      token: Storage.get('authToken'),
      docGroupRequest: {
        // "owner" : "138686@business.zeta.in",
        docGroupID: docId,
        owner:
          $(".company-dropdown .dropdown-toggle")
            .attr("data-company")
            .split("-", 1)[0] + "@business.zeta.in"
      }
    };
    DocumentSvc.getDoc(requestJson).then(
      function(respData) {
        var template = ejs.compile(RowTemplate);
        var dataArray = respData.map(function(item) {
          return template(item);
        });
        rowItem.find(".main-content").html(dataArray.join(""));
        rowItem.find(".loading-content").hide();
        if (dataArray.length > 0) {
          rowItem.find(".main-content").show();
        } else {
          rowItem.find(".table-empty-state").show();
        }
      },
      function() {
        rowItem.find(".loading-content").hide();
        rowItem.find(".table-error-state").show();
      }
    );
  },
  previewModal: function(e) {
    let imagePath = $(this).attr("data-image");
    $("#thumbnailImg").attr("src", imagePath);
    $("#myModalThumbnail").modal("show");
  },
  loadMoreDocs: function() {
    let startInclusive = $("#docStoreList .doc-store-item-wrpr").length;
    let endInclusive = startInclusive + 11;
    let searchTxt = $("#searchDocs").val();
    let businessId = $(".company-dropdown .dropdown-toggle").attr(
      "data-company"
    );
    let programName = $("#programType option:selected").text();
    let statusType = $("#statusType option:selected").text();
    initGAEvent(
      "event",
      "Document Drive",
      "click",
      "Document Search Load More, corpId=" +
        Meta.getMetaElement("corpID") +
        ", compId=" +
        Meta.getMetaElement("companyID") +
        ", userId=" +
        Meta.getMetaElement("userID") +
        ", timestamp=" +
        new Date().toLocaleString() +
        ",searchString=" +
        searchTxt +
        ",programName=" +
        programName +
        ",statusType=" +
        statusType
    );
    DocumentSvc.loadMoreDocs(startInclusive, endInclusive).then(
      function(respData) {
        if (respData.docGroupsInfos.length < 12) {
          $("#loadMoreDocsBtn").hide();
        }
        for (var i = 0; i < respData.docGroupsInfos.length; i++) {
          respData.docGroupsInfos[i].attrs["ProductType"] =
            AppLabels.PROGRAM_TYPES[
              respData.docGroupsInfos[i].attrs.producttype
            ];
          if (respData.docGroupsInfos[i].attrs.claimAmount) {
            respData.docGroupsInfos[i].attrs.claimAmount = Util.formatINR(
              Number(respData.docGroupsInfos[i].attrs.claimAmount) / 100
            );
          }
          if (
            typeof respData.docGroupsInfos[i].docGroupID.split(
              "biller/claims/"
            )[1] == "undefined"
          ) {
            let docGroupIdTemp = "";
            if (
              typeof respData.docGroupsInfos[i].docGroupID.split(
                "optima/proofs/"
              )[1] == "undefined"
            ) {
              docGroupIdTemp = respData.docGroupsInfos[i].docGroupID.split(
                "optima/travel_log/"
              )[1]; // travel log
            } else {
              docGroupIdTemp = respData.docGroupsInfos[i].docGroupID.split(
                "optima/proofs/"
              )[1];
            }
            let doccorpId = docGroupIdTemp.split("/", 1)[0];
            let doccompanyid = docGroupIdTemp.split("/", 2)[1];
            let docprogramid = docGroupIdTemp.split("/", 3)[2];
            let docemployeeid = docGroupIdTemp.split("/", 4)[3];
            let doccardid = docGroupIdTemp.split("/", 5)[4];
            let docrand = docGroupIdTemp.split("/").pop();
            if (
              typeof respData.docGroupsInfos[i].docGroupID.split(
                "optima/proofs/"
              )[1] == "undefined"
            ) {
              respData.docGroupsInfos[i].docGroupID =
                doccorpId +
                "+-" +
                doccompanyid +
                "+-" +
                docprogramid +
                "+-" +
                docemployeeid +
                "+-" +
                doccardid +
                "+-" +
                docrand +
                "+-tl"; // travel log
            } else {
              respData.docGroupsInfos[i].docGroupID =
                doccorpId +
                "+-" +
                doccompanyid +
                "+-" +
                docprogramid +
                "+-" +
                docemployeeid +
                "+-" +
                doccardid +
                "+-" +
                docrand +
                "+-pc";
            }
          } else {
            respData.docGroupsInfos[i].docGroupID = respData.docGroupsInfos[
              i
            ].docGroupID.split("biller/claims/")[1];
          }
          if (respData.docGroupsInfos[i].attrs.state) {
            respData.docGroupsInfos[i].attrs.state = respData.docGroupsInfos[
              i
            ].attrs.state.toLowerCase();
          }
          if (
            respData.docGroupsInfos[i].attrs.state === "approved" ||
            respData.docGroupsInfos[i].attrs.state === "paid" ||
            respData.docGroupsInfos[i].attrs.state === "unpaid" ||
            respData.docGroupsInfos[i].attrs.state === "partially_paid" ||
            respData.docGroupsInfos[i].attrs.state === "partially_approved"
          ) {
            respData.docGroupsInfos[i].attrs.state = "approved";
          }
        }
        let programTypes = JSON.parse(localStorage.getItem("programTypes"));
        respData.programTypes = programTypes;
        let documents = {
          docGroup: respData,
          businessId: businessId,
          programTypes: programTypes
        };
        DomEventHandler.appendToMyTemplate(
          "docStoreList",
          DocumentItemTemplate,
          documents
        );
      },
      function(respError) {
        console.log(respError);
      }
    );
  },
  searchDocs: function(e) {
    let searchTxt = $("#searchDocs").val();
    let programType = $("#programType").val();
    let programName = $("#programType option:selected").text();
    let dateRange = $("#timePeriod").val();
    let statusType = "none";
    let status = "none";
    let docSource = "none";
    if ($(".all-status").is(":visible")) {
      status = $("#statusType").val();
      statusType = $("#statusType").val();
    } else {
      docSource = $("#documentType").val();
    }
    if (programType === null) {
      programType = 0;
    }
    if ((searchTxt != "" && e.keyCode == 13) || searchTxt == "") {
      $("#docStoreList").empty();
      _searchDocs(searchTxt, programType, status, dateRange, docSource);
      initGAEvent(
        "event",
        "Document Drive",
        "click",
        "Document Search, corpId=" +
          Meta.getMetaElement("corpID") +
          ", compId=" +
          Meta.getMetaElement("companyID") +
          ", userId=" +
          Meta.getMetaElement("userID") +
          ", timestamp=" +
          new Date().toLocaleString() +
          ",searchString=" +
          searchTxt +
          ",programName=" +
          programName +
          ",statusType=" +
          statusType
      );
    }
  },
  searchDocsOnFilter: function() {
    let searchTxt = $("#searchDocs").val();
    let programType = $("#programType").val();
    let programName = $("#programType option:selected").text();
    let statusType = "none";
    let status = "none";
    let docSource = "none";
    let dateRange = $("#timePeriod").val();
    if ($(".all-status").is(":visible")) {
      let allStatusSelected =
        $("#statusType option:not(:selected)").length === 0;
      statusType = allStatusSelected ? "" : $("#statusType").val();
      status = $("#statusType option:selected").text();
    } else {
      docSource = $("#documentType").val();
    }
    $("#docStoreList").empty();
    _searchDocs(searchTxt, programType, statusType, dateRange, docSource);
    initGAEvent(
      "event",
      "Document Drive",
      "click",
      "Document Search, corpId=" +
        Meta.getMetaElement("corpID") +
        ", compId=" +
        Meta.getMetaElement("companyID") +
        ", userId=" +
        Meta.getMetaElement("userID") +
        ", timestamp=" +
        new Date().toLocaleString() +
        ",searchString=" +
        searchTxt +
        ",programName=" +
        programName +
        ",statusType=" +
        status
    );
  },
  searchDocsOnFilterCompanyChange: function() {
    $(".company-dropdown .dropdown-toggle").html($(this).html());
    $(".company-dropdown .dropdown-menu a").removeClass("selected");
    $(this).addClass("selected");
    $(".company-dropdown .dropdown-toggle").attr(
      "data-company",
      $(this).attr("data-company")
    );
    let searchTxt = $("#searchDocs").val();
    let programType = 0; // because first time we will call by all program types;
    let programName = $("#programType option:selected").text();
    let companyID = $(".company-dropdown .dropdown-toggle")
      .attr("data-company")
      .split("-")
      .pop();
    $("#searchingTxt").show();
    $("#docStoreList").empty();

    DocumentSvc.getActiveCardProgramsPaginationPost(companyID, 0, 12).then(
      function(respData) {
        // this we need to update card program dropdown

        let activeCards = respData;
        let pageData = {
          programTypes: {}
        };
        let programs = activeCards.programs;
        let programsCount = programs.length;
        for (let i = 0; i < programsCount; i++) {
          if (
            programs[i].cardProgramObject.spendConfiguration.channels
              .allowReimbursement === true
          ) {
            pageData.programTypes[programs[i].cardProgramObject.cardProgramID] =
              programs[i].cardProgramObject.cardName;
          }
        }
        let totalBenefitPrograms = activeCards.totalRequestCount;
        let totalPages = Math.ceil(totalBenefitPrograms / 10);
        localStorage.setItem("programMaxPage", totalPages);
        localStorage.setItem("programPageNumber", 0);
        localStorage.setItem("flag", 0);
        localStorage.setItem("scrollFlag", 0);
        var programTypes = pageData.programTypes;
        localStorage.setItem("programTypes", JSON.stringify(programTypes));
        let option = "";
        for (var key in programTypes) {
          option +=
            '<option value="' +
            key +
            '" selected="selected">' +
            programTypes[key] +
            "</option>";
        }
        $("#programType").html(option);
        $("#programType").multiselect("reload");
        $("#docStoreList").empty();
        let dateRange = $("#timePeriod").val();
        let statusType = "none";
        let status = "none";
        let docSource = "none";
        if ($(".all-status").is(":visible")) {
          let allStatusSelected =
            $("#statusType option:not(:selected)").length === 0;
          statusType = allStatusSelected ? "" : $("#statusType").val();
          status = $("#statusType option:selected").text();
        } else {
          docSource = $("#documentType").val();
        }
        _searchDocs(searchTxt, programType, statusType, dateRange, docSource);
        initGAEvent(
          "event",
          "Document Drive",
          "click",
          "Document Search, corpId=" +
            Meta.getMetaElement("corpID") +
            ", compId=" +
            Meta.getMetaElement("companyID") +
            ", userId=" +
            Meta.getMetaElement("userID") +
            ", timestamp=" +
            new Date().toLocaleString() +
            ",searchString=" +
            searchTxt +
            ",programName=" +
            programName +
            ",statusType=" +
            status
        );
      },
      function(respError) {
        $("#searchingTxt").hide();
        //     $('#loadMoreDocsBtn').hide();
        //  //   $('#noDocumentId').show();
        $("#docStoreList").append(
          '<div class="no-documents"><div class="no-documents"><div class="table-empty-state" style="display:block;"> <p>Oops! Unable to change company. Try Again</p></div></div></div>'
        );
        //     console.log(respError);
      }
    );
  },
  openDocDetails: function() {
    let redirect_url = $(this).attr("data-href");
    let reuestedSource = Util.getUrlParameter("source");
    if (reuestedSource) {
      redirect_url += "&source=" + reuestedSource;
    }
    window.open(redirect_url);
  }
};

import Util from '../common/util';
import ServiceConnector from '../common/serviceConnector';
import Urls from '../common/urls';
import Meta from '../common/metaStorage';
import Storage from '../common/webStorage'
export
default {
    newProgram: function() {
        var pathArr = location.pathname.split('/');
        let url = '/setSelectedCompanyID?companyID=' + pathArr[pathArr.length - 1];
        let headers = {};
        ServiceConnector.get(url, headers).then(function(respData) {
            window.location.href = '/dashboard';
        }, function(respErr) {
            console.log('Error');
        })
    },
    redirect: function() {
        initGAEvent('event', 'Admin Console', 'click', $(this).data('event')+', corpId=' + Meta.getMetaElement("corpID") + ', compId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', timestamp='+new Date().toLocaleString());
    },
    validateInputBtn: function(e){
        let disabledBtn2 = true;
        let disabledBtn1 = true;
        let companyShortCode = $(this).val();
        let companyShortCodeId = $(this);
        let companyCodeError = '';
        let companycodeValid = true;
        let disabledBtn = false; 
        let disabledBtn3 = true;
        let defaultValue = e.target.defaultValue;
        let currentValue = e.target.value;
        $('.companyCode').each(function(e){
            if($(this).val().length<2){
                disabledBtn1 = false;
                return;
            }else if(!disabledBtn1){
                disabledBtn1 = false;
            }else{
                disabledBtn1 = true;
            };
            if(!companycodeValid){
                disabledBtn = false;
            }else{
                disabledBtn = true;
            };
        });
        
        if(/^[a-zA-Z0-9- ]*$/.test(companyShortCode) == false) {
            companycodeValid = false;
            companyCodeError = 'Special charecters not allowed';
        }else if(companyShortCode.length <2 || companyShortCode.length >7){
            companycodeValid = false;
            companyCodeError = 'Should be between 2 to 7 charecters';
        }else if(companyShortCode.indexOf(' ') >= 0){
            companycodeValid = false;
            companyCodeError = 'Spaces are not allowed';
        }else{
            companyCodeError = '';
            companycodeValid = true;
        };
        $('.companyCode').each(function(){
            if (($(this).val() === companyShortCode) && ($(this).attr('id') !== companyShortCodeId.attr('id')) && ($(this).val().length !== 0)){
                disabledBtn2 = false;
                companyCodeError = "Already chosen this short code";
            }
            if(defaultValue == currentValue){
                companycodeValid = false;
                companyCodeError = 'Should be different than current code!';
            }
        });
        $(this).next('.companyCodeError').text(companyCodeError); 
        let checkInputSame = $('.companyCodeError').text(); 
        if(companycodeValid && disabledBtn && disabledBtn1 && disabledBtn2 && checkInputSame ==''){
            $('#saveCompanyCodes').removeClass('btn-disabled');
        }else{
            $('#saveCompanyCodes').addClass('btn-disabled');
        }
    }, 
    addCompanyCode: function(){
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let totalCompanys = $(this).data('companycount');
        let url = Urls.getOMSApiUrl().SET_PROGRAM_SHORTCODE.replace(':token:', authToken)
        let headers = {};
        let success = true;
        let errorMsg = 'Something went wrong!';
        for(let i=0; i<totalCompanys; i++){
            let companyId = $('.companycode'+[i]).data('companyid');
            let companyCode = $('.companycode'+[i]).val();
            let defaultValue = $('.companycode'+[i])[0].defaultValue;
            let input =  {'corpID':corpID,'companyID':companyId,'companyShortCode':companyCode};
            if(defaultValue != companyCode){
                ServiceConnector.post(input, url, headers).then(function(respData){
                    //console.log(respData);
                    $('.company_code').show();
                }, function(respError){
                    success = false;
                    let checkGenie = 0;
                    if(localStorage.getItem('checkGenie')){
                        checkGenie= parseInt(localStorage.getItem('checkGenie'));
                    }
                    if(checkGenie == 0){
                        errorMsg = respError;
                    } else{
                        errorMsg = "You may not have access to setup."
                    }
                    alert(errorMsg);
                });
            }
        }
        // if(success){
        //     $('.company_code').show();
        // }else{
        //     alert(errorMsg);
        // }
        //console.log(localStorage);
    },
    successAddedCode: function(){
        window.location.href = '/bulk-action/new-transfer';
    },
    openCompanyCode: function(){
        $('.bulkAction_Cards').hide();
        $('.homeAddCompanyCo').show();
    },
    createBulkTransferOrder: function(){
        initGAEvent('event', 'Bulk Actions', 'click', 'Bulk Transfer Order Clicked, corpId=' + Meta.getMetaElement("corpID") + ', compId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', timestamp='+new Date().toLocaleString());
    },
    bulkCloseCardsRequest: function(){
        initGAEvent('event', 'Bulk Actions', 'click', 'Bulk Close Card Clicked, corpId=' + Meta.getMetaElement("corpID") + ', compId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', timestamp='+new Date().toLocaleString());
    }
}
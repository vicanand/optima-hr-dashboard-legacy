import InvoicingSvc from '../../service/invoicing/invoicingSvc.js';
import Storage from '../../common/webStorage.js';
import CommercialsTemplate from '../../../template/section/invoices/commercials.ejs'
import TableWidget from '../../widgets/table';
// import Urls from '../common/urls';
// import ServiceConnector from "../common/serviceConnector";
import DomEventHandler from '../../common/domEventHandler.js'

let benefitPrograms = ["meal", "fuel", "communication", "medical", "gift", "uniform", "books", "asset", "LTA", "shift"]
let keyMaps = {
    BILL_TO_USER: "Bill to user",
    BILL_TO_CORP: "Bill to corporate",
    physical_gift: "Physical Gift",
    physical: "Physical",
    digital_gift: "Digital Gift",
    digital: "Digital"
}
let listOfCardType = ["physical", "physical_gift", "digital", "digital_gift"]

let init = function(){
    // InvoicingSvc.getInvoices('ALL').then(function(response){
    //     console.log(response)
    // })
    // var commercials = Storage.getCollection('selected_comp_info').company.productCommercials;

    let commercialPromise = [InvoicingSvc.getCommercials(), InvoicingSvc.getAllCommercials(), InvoicingSvc.getCardMasterDetails()]
    
    Promise.all(commercialPromise).then(function(respArr){
        console.log('respArr', respArr)
        let allCommercials = respArr[1].commercials
        let cardMasterDetails = respArr[2].cardMasterList

        //Programs data preparation
        let programTypesList = {}
        let programTypes = []
        let programCommercials = respArr[0].invoiceConfigs.filter(program => benefitPrograms.indexOf(program.category) >= 0)
        programCommercials.forEach(element => {
            programTypesList[element.category] = element.category
        });
        Object.keys(programTypesList).forEach(type => {
            let commercials = programCommercials.filter(program => program.category == type)
            commercials.forEach((element, index) => {
                let commercialData = allCommercials.filter(comm => comm.id == element.commercialID)[0]
                element['taxes'] = commercialData.taxes
                element['triggerType'] = commercialData.triggerType
                element['fee'] = commercialData.fee
                element['valueType'] = commercialData.valueType
                commercials[index] = element
            })
            let commercialObj = {}
            commercialObj['productType'] = type
            commercialObj['commercials'] = commercials
            programTypes.push(commercialObj);
        })

        //Cards data preparation
        let cardTypeList = {}
        let cardTypes = []
        let cardCommercials = respArr[0].invoiceConfigs.filter(card => listOfCardType.indexOf(card.category) >= 0)
        cardCommercials.forEach((element, index) => {
            cardTypeList[element.category] = element.category;
        })

        Object.keys(cardTypeList).forEach(type => {
            let commercials = cardCommercials.filter(program => program.category == type)
            commercials.forEach((element, index) => {
                let commercialData = allCommercials.filter(comm => comm.id == element.commercialID)[0]
                element['taxes'] = commercialData.taxes
                element['fee'] = commercialData.fee
                element['valueType'] = commercialData.valueType
                commercials[index] = element
            })

            let commercialObj = {}
            commercialObj['productType'] = type
            commercialObj['commercials'] = commercials
            cardTypes.push(commercialObj)
        })

        console.log({
            cardMasterDetails: cardMasterDetails,
            programCommercials: programTypes,
            cardCommercials: cardTypes,
            keyMaps: keyMaps
        })

        DomEventHandler.renderMyTemplate('commercialsListData', CommercialsTemplate, 
            {
                cardMasterDetails: cardMasterDetails,
                programCommercials: programTypes,
                cardCommercials: cardTypes,
                keyMaps: keyMaps
            }
        )
        bindDomEvents();
    })

    TableWidget.init();    
    $("#invoicesListTable").table({
        source: InvoicingSvc.getInvoices,
        pageSize: 10,
        pageNumber: 1,
        status:'',
        tableTemplate: "/template/section/invoices/invoicesTable.ejs",
        rowTemplate: "/template/section/invoices/invoicesTableRow.ejs",
        extraParam: {
        },
        filterSelect: $('#filterInvoicesOnState')
    });

    $('.status').click(addShowHideAction())
    $("body").on('click', '.listItem img', function() {
        let cardId = $(this).closest('.listItem').attr('data-id')
        let cardType = $(this).closest('.listItem').attr('data-type')
        let contentId = cardType+cardId
        $(this).closest('.table-container').find('.listItem').removeClass('active');
        $(this).closest('.listItem').addClass('active');
        $(this).closest('.table-container').find('.cardTableRow').hide();
        $(this).closest('.table-container').find('.cardTableRow#'+contentId).show();
    })

    $("body").on('click', '.saveCommercialAddress', function() {
        let commercialID = $(this).data('commid');
        let addressData = $(this).closest('.addressContainer')
        let firstName = $(addressData).find('input[name=firstName]').val(),
            lastName = $(addressData).find('input[name=lastName]').val(),
            email = $(addressData).find('input[name=email]').val(),
            countryCode = $(addressData).find('input[name=countryCode]').val(),
            phone = $(addressData).find('input[name=phone]').val(),
            address1 = $(addressData).find('input[name=address1]').val(),
            address2 = $(addressData).find('input[name=address2]').val(),
            city = $(addressData).find('input[name=city]').val(),
            state = $(addressData).find('input[name=state]').val(),
            pincode = $(addressData).find('input[name=pincode]').val();
        if (!firstName || !lastName || !email || !countryCode || !phone || !address1 || !city || !state || !pincode) {
            alert('Address details is not complete');
            return false;
        }

        let addressObj = {
            "firstName": firstName,
            "lastName": lastName,
            "email": email,
            "telephone": phone,
            "line1": address1,
            "line2": address2,
            "city": city,
            "region": state,
            "countryID": countryCode,
            "postCode": pincode
        }
        InvoicingSvc.updateInvoiceConfig(commercialID, addressObj).then(function(respData){
            
        })
    })

    $("body").on('click', '.showAddress', function() {
        $(this).toggleClass('open');
        $(this).closest('.billingAddress').find('.addressContainer').toggleClass('hideMe')
    })
    
}

let bindDomEvents = function() {
    $('.status').click(function(){
        $(this).toggleClass('open');
        $(this).closest('.programRow').find('.content').toggleClass('hideMe');
    })
}

let addShowHideAction = function(){
    console.log('I am here')
    
}

export default {
    init: init,
    addShowHideAction: addShowHideAction
}
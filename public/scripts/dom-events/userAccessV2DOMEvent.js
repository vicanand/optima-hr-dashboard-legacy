import AccessCtrlSvc from '../service/accessCtrlSvc.js';
import Util from '../common/util';
import CacheDataStorage from '../common/cacheDataStorage';
import Constants from '../common/constants';
import Meta from '../common/metaStorage';

let validateUserName = function() {
    if ($('#userName').attr("data-touched")) {
        if ($('#userName').val().length == 0) {
            $('#userName').addClass('input-has-error');
            $('#userName').parents('.form-group').find('.error-message').html('Please enter a User Name');
            return false;
        } else {
            $('#userName').removeClass('input-has-error');
            $('#userName').parents('.form-group').find('.error-message').html('');
            return true;
        }
    }
};

let validateEmployeeID = function() {
    if ($('#employeeID').attr("data-touched")) {
        const prefix = $('#employeeID').val(),
        employeeID = prefix + '@zeta-smretail1.com';
        if (prefix && /\d/.test(prefix) && Util.validateEmail(employeeID)) {
            $('#employeeID').removeClass('input-has-error');
            $('#employeeID').parents('.form-group').find('.error-message').html('');
            return true;
        } else {
            $('#employeeID').addClass('input-has-error');
            $('#employeeID').parents('.form-group').find('.error-message').html('Please enter a valid Employee ID with atleast one numeric value');
            return false;
        }
    }
};

let validatePassword = function(elemID) {
    if ($('#'+elemID).attr("data-touched")) {
        const password = $('#'+elemID).val();
        if (password && /\d{6}/.test(password)) {
            $('#'+elemID).removeClass('input-has-error');
            $('#'+elemID).parents('.form-group').find('.error-message').html('');
            return true;
        } else {
            $('#'+elemID).addClass('input-has-error');
            $('#'+elemID).parents('.form-group').find('.error-message').html('Your Password must be minimum 6 digit');
            return false;
        }
    }
};

let setTouched = function() {
    $(this).attr('data-touched', true);
}

let validateFields = function($event) {
    let userNameValidation = validateUserName();
    let employeeIDValidation = validateEmployeeID();
    let passwordValidation = validatePassword('password');
    let accessTypeValidation = !!$('#accessType').val();
    let selectedCompany = $('#chooseCompany').val();
    if ((userNameValidation == true) && (employeeIDValidation == true) && (passwordValidation == true) && (accessTypeValidation == true) && (selectedCompany && selectedCompany.length)) {
        $('#addNewUserBtn').removeClass('btn-disabled');
        $('#addNewUserBtn').prop('disabled', false);
    } else {
        $('#addNewUserBtn').addClass('btn-disabled');
        $('#addNewUserBtn').prop('disabled', true);
    }
};

let accessTypeChange = function() {
    const selectedAaccessType = $('#accessType').val();
    if (selectedAaccessType && selectedAaccessType === 'ADMIN') {
        $('#chooseCompany').parents('.form-group').find('.ms-selectall').click();
        $('#chooseCompany').parents('.form-group').find('.ms-options-wrap button').prop('disabled', true);
    } else {
        $('#chooseCompany').multiselect('reset');
    }
    validateFields();
}

let addUser = function() {
    $('#addNewUserBtn').html("Adding User");
    $('#addNewUserBtn').addClass('btn-disabled');
    $('#addNewUserBtn').prop('disabled', true);
    $('#addUserLoader').show();
    let curCompany = $("meta[name='companyID']").attr("content");
    let requestPayload = {};
    requestPayload.corpId = $("meta[name='corpID']").attr("content");
    requestPayload.name = $('#userName').val();
    requestPayload.employeeId = $('#employeeID').val().toLowerCase();
    requestPayload.password = $('#password').val();
    requestPayload.role = $('#accessType option:selected').val();
    requestPayload.companyIds = $('#chooseCompany').val();
    let rbac = CacheDataStorage.getItem(Constants.ROLES_CONFIG);
    if (rbac && rbac['addAccountForEmployee_'+curCompany] && rbac['addAccountForEmployee_'+curCompany].indexOf('own') !== -1 && requestPayload.companyIds && requestPayload.companyIds.length) {
        requestPayload.companyId = requestPayload.companyIds[0];
    }
    // requestPayload.role = $('input[name=user-type]:checked').val();
    // requestPayload.role = 'ADMIN';
    // console.log(requestPayload);
    AccessCtrlSvc.addAccountForEmployee(requestPayload).then(function(responseData) {
        let message = "A new user named " + $('#userName').val() + " has been added successfully";
        $('#addUserModal').modal('hide');
        $('#success-img').show();
        $('#errorResponse').html("");
        $('#errorResponseMsg').text(message);
        $('#responseBtn').html('DONE');
        $('#requestResponseModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#requestResponseModal').modal('show');
    }, function(responseError) {
        let errorObj = JSON.parse(responseError.responseText);
        $('#addUserModal').modal('hide');
        $('#error-img').show();
        if(Util.checkGenie()){    
            $('#errorResponse').html("Failed to add user");
            $('#errorResponseMsg').html("You do not have access to add user");
            // $("#showDownloadStatementError #showDownloadStatementErrorMsg").text("You do not have access to download employee statement");
        } else {  
            $('#errorResponse').html(responseError.statusText);
            let message = errorObj.message;
            message = message.replace('email', 'employeeID').replace('@zeta-smretail1.com', '');
            $('#errorResponseMsg').html(message);
        }
        $('#responseBtn').html('DONE');
        $('#requestResponseModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#requestResponseModal').modal('show');
    });
};

let validateChangePassword = function() {
    if ($('#changePassword').val().length >= 6) {
        $(this).attr('data-touched', true);
    }
    const validate = validatePassword('changePassword');
    if (validate == true) {
        $('#changePasswordBtnV2').removeClass('btn-disabled');
        $('#changePasswordBtnV2').prop('disabled', false);
    } else {
        $('#changePasswordBtnV2').addClass('btn-disabled');
        $('#changePasswordBtnV2').prop('disabled', true);
    }
};

let generatePassword = function() {
    // Digit only password pattern generator
    var length = 6,
    numbers = "01234567890123456789",
    retVal = "";
    for (var i = 0; i < length; ++i) {
        retVal += numbers.charAt(Math.floor(Math.random() * numbers.length));
    }
    const input = $(this).parents('.form-group').find('input');
    input.attr('data-touched', true);
    input.val(retVal);
    if ($(this).attr('id') === 'forChangePassword') {
        validateChangePassword();
    } else {
        validateFields();
    }
};

let changePassword = function() {
    $('#changePasswordBtnV2').html("Changing Password");
    $('#changePasswordBtnV2').addClass('btn-disabled');
    $('#changePasswordBtnV2').prop('disabled', true);
    $('#changePasswordLoader').show();
    let requestPayload = {};
    requestPayload.corpId = $("meta[name='corpID']").attr("content");
    requestPayload.accountId = $(this).data('accountid');
    requestPayload.password = $('#changePassword').val();
    requestPayload.companyId = Meta.getMetaElement('companyID');
    AccessCtrlSvc.changeCorpAccountPassword(requestPayload).then(function(responseData) {
        let message = "Password has been changed successfully";
        $('#changePasswordModal').modal('hide');
        $('#success-img').show();
        $('#errorResponse').html("");
        $('#errorResponseMsg').html(message);
        $('#responseBtn').html('DONE');
        $('#requestResponseModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#requestResponseModal').modal('show');
    }, function(responseError) {
        let errorObj = JSON.parse(responseError.responseText);
        $('#changePasswordModal').modal('hide');
        $('#error-img').show();
        if(Util.checkGenie()){    
        $('#errorResponse').html("Failed to change password");
        $('#errorResponseMsg').html("You do not have access to change password");
            // $("#showDownloadStatementError #showDownloadStatementErrorMsg").text("You do not have access to download employee statement");
        } else {  
            $('#errorResponse').html(responseError.statusText);
            let message = errorObj.message;
            if (errorObj.message.indexOf('CannotSetOTPException') !== -1) {
                message = 'Password already in use, please enter new password';
            } else if (errorObj.type == "IllegalStateException" && errorObj.message.indexOf('Server') !== -1) {
                message = 'Something went wrong/password already in use';
            }
            $('#errorResponseMsg').html(message);
        }
        $('#responseBtn').html('DONE');
        $('#requestResponseModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#requestResponseModal').modal('show');
    });
};

let validateChangeAccessType = function() {
    const selectedValue = $('#accessTypeEdit option:selected').val();
    if (selectedValue.length !== 0 && selectedValue !== $('#accessTypeEdit').data('access-type')) {
        $('#changeAccessBtn').removeClass('btn-disabled');
        $('#changeAccessBtn').prop('disabled', false);
        $(this).attr('data-touched', true);
        $('#accessTypeEdit').removeClass('input-has-error');
        $('#accessTypeEdit').parents('.form-group').find('.error-message').html('');
        return true;
    } else {
        $('#accessTypeEdit').addClass('input-has-error');
        $('#accessTypeEdit').parents('.form-group').find('.error-message').html('User already has selected access');
        $('#changeAccessBtn').addClass('btn-disabled');
        $('#changeAccessBtn').prop('disabled', true);
        return false;
    }
};

let changePrivilege = function() {
    $('#changeAccessBtn').html("Changing Role");
    $('#changeAccessBtn').addClass('btn-disabled');
    $('#changeAccessBtn').prop('disabled', true);
    $('#changePrivilegeLoader').show();
    let requestPayload = {};
    requestPayload.corpId = $("meta[name='corpID']").attr("content");
    requestPayload.accountId = $(this).data('accountid');
    const companyids = $(this).data('companyids');
    if (companyids && companyids.length !== 0) {
        requestPayload.companyIds = companyids.split(',');
    }
    requestPayload.role = $('#accessTypeEdit option:selected').val();
    // console.log(requestPayload);
    AccessCtrlSvc.changeCorpAccountRole(requestPayload).then(function(responseData) {
    let message = "Role has been changed successfully";
        $('#editPrivilegesModal').modal('hide');
        $('#success-img').show();
        $('#errorResponse').html("");
        $('#errorResponseMsg').html(message);
        $('#responseBtn').html('DONE');
        $('#requestResponseModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#requestResponseModal').modal('show');
    }, function(responseError) {
        let errorObj = JSON.parse(responseError.responseText);
        $('#editPrivilegesModal').modal('hide');
        $('#error-img').show();
        if(Util.checkGenie()){    
        $('#errorResponse').html("Failed to change role");
        $('#errorResponseMsg').html("You do not have access to change role");
            // $("#showDownloadStatementError #showDownloadStatementErrorMsg").text("You do not have access to download employee statement");
        } else {  
            $('#errorResponse').html(responseError.statusText);
            $('#errorResponseMsg').html(errorObj.message);
        }
        $('#responseBtn').html('DONE');
        $('#requestResponseModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#requestResponseModal').modal('show');
    });
};

let changeStatus = function() {
    let currStatus = $(this).data('status');
    let requestPayload = {};
    if (currStatus == 'active') {
        requestPayload.status = 'suspended';
        requestPayload.corpId = $("meta[name='corpID']").attr("content");
        requestPayload.accountId = $(this).data('accountid');
    } else if (currStatus == 'suspended') {
        requestPayload.status = 'active';
        requestPayload.corpID = $("meta[name='corpID']").attr("content");
        requestPayload.accountID = $(this).data('accountid');
    }
    $('#changeAccessLoader').show();
    $('#changeStatusBtn').addClass('btn-disabled');
    $('#changeStatusBtn').prop('disabled', true);
    AccessCtrlSvc.changeAccountStatus(requestPayload).then(function(responseData) {
        let userName = $('#changeStatusBtn').data("name")
        let message;
        if (currStatus == 'active') {
            message = userName + " no longer has access to " + $('#currCorp').html() + "\’s corporate account";
        } else if (currStatus == 'suspended') {
            message = userName + "\'s access to " + $('#currCorp').html() + "\’s corporate account has been restored";
        }
        $('#changeStatusModal').modal('hide');

        $('#success-img').show();
        $('#errorResponse').html("");
        $('#errorResponseMsg').html(message);
        $('#responseBtn').html('DONE');
        $('#requestResponseModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#requestResponseModal').modal('show');
    }, function(responseError) {
        let errorObj = JSON.parse(responseError.responseText);

        $('#changeStatusModal').modal('hide');
        $('#error-img').show();
        if(Util.checkGenie()){    
            $('#errorResponse').html("Failed!");
            $('#errorResponseMsg').html("You do not have access to perform this action.");
                $("#showDownloadStatementError #showDownloadStatementErrorMsg").text("You do not have access to download employee statement");
            } else {  
                $('#errorResponse').html(responseError.statusText);
                $('#errorResponseMsg').html(errorObj.message);
            }
        $('#responseBtn').html('DONE');
        $('#requestResponseModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#requestResponseModal').modal('show');
    });
};

let reloadPage = function() {
    location.reload();
};

export
default {
    changeStatus: changeStatus,
    reloadPage: reloadPage,
    generatePassword: generatePassword,
    validateFields: validateFields,
    addUser: addUser,
    changePassword: changePassword,
    changePrivilege: changePrivilege,
    validateChangePassword: validateChangePassword,
    validateChangeAccessType: validateChangeAccessType,
    setTouched: setTouched,
    accessTypeChange: accessTypeChange
};
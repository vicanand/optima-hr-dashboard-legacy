import Storage from '../common/webStorage';
import Constant from '../common/constants';
import DomEventHandler from '../common/domEventHandler.js';
import ServiceConnector from '../common/serviceConnector';
import Util from '../common/util';
import BulkCloseSvc from '../service/bulkCloseSvc';

let downloadClaimReport = function(e){
    e.preventDefault();
    $("#downloadReportModalLoader").modal("show");
    let closeOrderID = $(this).attr('data-closeOrderID');
    BulkCloseSvc.claimReport(closeOrderID).then(function(respData) {
        $("#downloadReportModalLoader").modal("hide");
        var win = window.open(Util.htmlDecoder(respData.url), '_blank');
        win.focus();
    },function(respErr){
        $("#downloadReportModalLoader").modal("hide");
    });
};

let downloadRevokeReport = function(e){
    e.preventDefault();
    $("#downloadReportModalLoader").modal("show");
    let closeOrderID = $(this).attr('data-closeOrderID');
    BulkCloseSvc.revokeReport(closeOrderID).then(function(respData) {
        $("#downloadReportModalLoader").modal("hide");
        var win = window.open(Util.htmlDecoder(respData.url), '_blank');
        win.focus();
    },function(respErr){
        $("#downloadReportModalLoader").modal("hide");
    });
};
let downloadCardStatusReport = function(e){
    e.preventDefault();
    $("#downloadReportModalLoader").modal("show");
    let closeOrderID = $(this).attr('data-closeOrderID');
    BulkCloseSvc.cardStatusReport(closeOrderID).then(function(respData) {
        $("#downloadReportModalLoader").modal("hide");
        var win = window.open(Util.htmlDecoder(respData.url), '_blank');
        win.focus();
    },function(respErr){
        $("#downloadReportModalLoader").modal("hide");
    });
};
let downloadFileReport = function(e){
    e.preventDefault();
    $("#downloadReportModalLoader").modal("show");
    let closeOrderID = $(this).attr('data-closeOrderID');
    BulkCloseSvc.fileReport(closeOrderID).then(function(respData) {
        $("#downloadReportModalLoader").modal("hide");
        var win = window.open(Util.htmlDecoder(respData.downloadUrl), '_blank');
        win.focus();
    },function(respErr){
        $("#downloadReportModalLoader").modal("hide");
    });
};



export default {
    downloadClaimReport: downloadClaimReport,
    downloadRevokeReport : downloadRevokeReport,
    downloadCardStatusReport:downloadCardStatusReport,
    downloadFileReport:downloadFileReport
};

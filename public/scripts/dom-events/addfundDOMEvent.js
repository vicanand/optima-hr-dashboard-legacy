import FundingSvc from '../service/fundingSvc.js';
import Storage from '../common/webStorage.js';
import Urls from '../common/urls';
import Constants from '../common/constants';
import ServiceConnector from "../common/serviceConnector";
import DomEventHandler from '../common/domEventHandler.js';
import authorizedBankAccntsModal from "../../template/authorized-bank-accounts.ejs";
import CacheDataStorage from '../common/cacheDataStorage';
import benefitSvc from '../service/benefitSvc';

let changeTransferTxt = function (event) {
    let transferVal = $('input[name=inlineRadioOptions]:checked').val();
    const country_configs = CacheDataStorage.getItem(Constants.COUNTRY_CONFIGS);
    if (country_configs && _.get(country_configs, 'addFundDOMEvent.onTransferChange')) {
        const onTransferTextChange = _.get(country_configs, 'addFundDOMEvent.onTransferChange');
        onTransferTextChange(transferVal);
        clearInputs();
    } else {
        if ($('input[name=inlineRadioOptions]:checked').val() == "NEFT") {
            $('label[for="chequeNumber"]').text('Transaction Number');
            $('label[for="transferDate"]').text('Transfer Date');
        } else {
            $('label[for="chequeNumber"]').text('Cheque Number');
            $('label[for="transferDate"]').text('Cheque Date');
        }
    }
    validationRequest();
}

let clearInputs = function() {
    $(':input:not([name=inlineRadioOptions])').val('');
    $('input[type="file"]').removeAttr('data-url');
}

let addFundsToAcc = function () {
    _loadingPopUp();
    let transactionDateArr = $('#transactionDate').val() ? $('#transactionDate').val().split('/') : '';
    let transactionDate = transactionDateArr ? transactionDateArr[2] + '-' + transactionDateArr[1] + '-' + transactionDateArr[0]: '';
    let paymentMode = $('input[name=inlineRadioOptions]:checked').val();
    let updateRequest;
    const country_configs = CacheDataStorage.getItem(Constants.COUNTRY_CONFIGS);
    if (country_configs) {
        updateRequest = _.get(country_configs, 'addFundDOMEvent.updateRequestObj');
        const paymentModeConfig = _.get(country_configs, 'addFundDOMEvent.addFundsToAcc.paymentMode');
        if (paymentModeConfig) {
            paymentMode = paymentModeConfig;
        }
    }
    
    var reqObj = {
        "paymentMode": paymentMode,
        "amount": $('#transactionAmount').val().replace(/,/g, ""),
        "paymentDetails": {
            "transactionRefNumber": $('#transactionRefNumber').val(),
            "transactionDate":  transactionDate,
        },
        "fundingAccountID": Storage.get('ACCOUNT_ID'),
        "corpID": parseInt($('#corpIDForAddFund').text()),
        "referenceNumber": $('#transactionRefNumber').val(),
        "comments": $('#fundReqComment').val(),
    }
    if(_.isFunction(updateRequest)) {
        reqObj = updateRequest(reqObj, paymentMode, transactionDate);
    }
    FundingSvc.addFundstoAcc(reqObj);
}

let formatCurrencyValue = function () {
    const transactionAmount = $('#transactionAmount').val();
    const country_configs = CacheDataStorage.getItem(Constants.COUNTRY_CONFIGS);
    if (transactionAmount && parseFloat(transactionAmount) && country_configs) {
        const validationConfig = _.get(country_configs, 'addFundDOMEvent.validationRequest');
        if (validationConfig && _.isFunction(validationConfig.formatCurrencyValue)) {
            validationConfig.formatCurrencyValue({elemId: '#transactionAmount'});
        }
    }
}


let validationRequest = function () {
    document.getElementById('transactionAmount').addEventListener('keydown', function(e) {
        if (e.which === 38 || e.which === 40) {
            e.preventDefault();
        }
    });
    $('.ph-add-fund input[type="text"]:not("#transactionAmount")').bind('keydown', function(e) {
        let regex = /^[A-Za-z0-9 ]+$/
        let textVal = e.target.value + e.key;
        if(!regex.test(textVal) && e.which != 8) {
            e.preventDefault();
        }
    });
    const transactionAmount = $('#transactionAmount').val().replace(/,/g, "");
    let inputIDList = ['transactionRefNumber', 'transactionDate'];
    const country_configs = CacheDataStorage.getItem(Constants.COUNTRY_CONFIGS);
    let validateAmountFunc;
    if (country_configs) {
        const validationConfig = _.get(country_configs, 'addFundDOMEvent.validationRequest');
        if (validationConfig && _.isFunction(validationConfig.validateAmount)) {
            validateAmountFunc = validationConfig.validateAmount;
        }
        if(validationConfig && _.isFunction(validationConfig.getRequiredInputList)) {
            const getInputList = validationConfig.getRequiredInputList;
            inputIDList = getInputList();
        }
    }
    let isInputInvalid = inputIDList.find(function(inputID) { //checks for if any one required input is empty 
        let inputValue;
        if(inputID === 'POAttachment' || inputID === 'paymentProof') {
            inputValue = $(`#${inputID}`).attr('data-url');
        } else {
            inputValue = $(`#${inputID}`).val();
        }
        return inputValue === '' || !inputValue; // returns undefined if all the required inputs have value
    });
    const transactionAmountValidate = transactionAmount != '' && parseFloat(transactionAmount) && (validateAmountFunc ? validateAmountFunc({amt: transactionAmount}) : true);
    if (!isInputInvalid && transactionAmountValidate) {
        $('#submitReq').removeAttr('disabled');
        $('#submitReq').removeClass('submit-btn-disabled');
    } else {
        $('#submitReq').attr('disabled', 'disabled');
        $('#submitReq').addClass('submit-btn-disabled');
    }
    formatCurrencyValue();
}

let uploadAttachmentFile = function(event) {
    let fileInput = $(this);
    let file = fileInput[0].files[0];
    if(file) {
        benefitSvc.getAssetUploadEndPoint().then(function(respData) {
            let cardLogoData = new FormData();
            let cardLogoUploadUrl = respData.postAction;
            cardLogoData.append('x-amz-algorithm', respData.xAmzAlgorithm);
            cardLogoData.append('x-amz-signature', respData.xAmzSignature);
            cardLogoData.append('acl', respData.acl);
            cardLogoData.append('success_action_status', respData.successActionStatus);
            cardLogoData.append('key', respData.key);
            cardLogoData.append('policy', respData.policy);
            cardLogoData.append('x-amz-date', respData.xAmzDate);
            cardLogoData.append('x-amz-credential', respData.xAmzCredential);
            cardLogoData.append('content-type', file.type);
            cardLogoData.append('file', file);
            let headers = {
                'content-Type': 'multipart/form-data'
            };
            ServiceConnector.postWithCORS(cardLogoData, cardLogoUploadUrl, headers).then(function (UploadData) {
                let key = UploadData.childNodes[0].childNodes[2].textContent;
                let tag = UploadData.childNodes[0].childNodes[3].textContent.replace('"','').replace('"','');
                let cardLogoLocation = UploadData.childNodes[0].childNodes[0].textContent + '?key=' + key + '&etag=' + tag+'&fileName='+file.name;
                fileInput.attr('data-url', cardLogoLocation);
                validationRequest();
            })
        }, function (respErr) {
            // $('#loaderModal').modal('hide');
            console.error(respErr);
        });
    } else {
        fileInput.attr('data-url', '');
    }
    validationRequest();
}

let validateAmount = function () {
    this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
}

let _loadingPopUp = function () {
    $('#myModalNewAccount').modal('hide');
    $('#myModalLoaderFund').modal('show');
}

let successPopUp = function () {
    $('#myModalLoaderFund').modal('hide');
    $('#myModalInitiatedSuccessFund').modal('show');
}

let FailPopUp = function () {
    $('#myModalLoaderFund').modal('hide');
    $('#myModalOopsFund').modal('show');
}

let addAccName = function () {
    $('#fund_acc_name strong').text(Storage.get('ACCOUNT_NAME'));
}

let showAuthorizedBankAccnts = function () {
    $('#authorizedBankAccntsModalLoading').modal();
    let url = Urls.getOMSApiUrl().GET_WHITELISTED_BANK_ACCOUNTS.replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
        .replace(":API_KEY:", Storage.get('authToken'));
    ServiceConnector.get(url, {}).then(function (respData) {
        $('#authorizedBankAccntsModalLoading').modal('hide');
        $('#authorizedBankAccntsModalWrpr').modal();
        DomEventHandler.renderMyTemplate('authorizedBankAccntsModalWrpr', authorizedBankAccntsModal, respData);
    }, function (respErr) {
        $('#authorizedBankAccntsModalLoading').modal('hide');
        if(respErr.error == 'Request with same reference number already exists'){
            $('#duplicateFundError').modal('show');
        }
        else{
            $('#myModalOopsFund').modal('show');
        }
    });

}

export default {
    addAccName: addAccName,
    successPopUp: successPopUp,
    FailPopUp: FailPopUp,
    validationRequest: validationRequest,
    addFundsToAcc: addFundsToAcc,
    changeTransferTxt: changeTransferTxt,
    showAuthorizedBankAccnts: showAuthorizedBankAccnts,
    validateAmount: validateAmount,
    uploadAttachmentFile: uploadAttachmentFile
}
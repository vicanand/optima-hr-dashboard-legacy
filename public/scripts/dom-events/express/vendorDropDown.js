let openDropdown = function () {
    $('.dropdown--list').addClass('dropdown--list__active');
};

let closeDropdown = function (e) {
    if ($(e.target).closest($('.vendor--details')).length === 0) {
        $('.dropdown--list').removeClass('dropdown--list__active');
    }
};

export default {
    openDropdown,
    closeDropdown
};

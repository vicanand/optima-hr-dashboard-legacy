import Storage from '../common/webStorage';
import Constant from '../common/constants';
import ServiceConnector from '../common/serviceConnector';
import BulkNewTransferSvc from '../service/bulkTransferSvc';
import Util from "../common/util";

let downloadBulkOrderFile = function(e){
    e.preventDefault();
    $("#downloadReportModalLoader").modal("show");
    let bulkOrderID = $(this).attr('data-bulkorderID');
    BulkNewTransferSvc.downloadBulkOrderFile(bulkOrderID).then(function(respData) {
        $("#downloadReportModalLoader").modal("hide");
        var win = window.open(Util.htmlDecoder(respData.downloadUrl), '_blank');
        win.focus();
    },function(respErr){
        $("#downloadReportModalLoader").modal("hide");
    });
};

let downloadBulkReport = function(e){
    e.preventDefault();
    $("#downloadReportModalLoader").modal("show");
    let bulkOrderID = $(this).attr('data-bulkorderID');
    BulkNewTransferSvc.downloadReport(bulkOrderID).then(function(respData) {
        $("#downloadReportModalLoader").modal("hide");
        var win = window.open(Util.htmlDecoder(respData.location), '_blank');
        win.focus();
    },function(respErr){
        $("#downloadReportModalLoader").modal("hide");
    });
};

export default {
    downloadBulkOrderFile : downloadBulkOrderFile,
    downloadBulkReport : downloadBulkReport
};

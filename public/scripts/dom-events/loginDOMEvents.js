import ServiceConnector from '../common/serviceConnector';
import Storage from '../common/webStorage';
import Urls from '../common/urls';
import Util from '../common/util';
import Constants from '../common/constants';
import Env from '../common/env';
import {COUNTRY_CONFIGS} from '../common/countryConfig/configMappings';

export
default {
    init: function () {
        Storage.remove("SELECTED_CORP_ID");
    },
    emailIdChange: function (e) {
        if (e.keyCode == 13) {
            $('#emailNext').trigger('click');
        }
    },
    emailNextClick: function () {
        let emailId = $('#emailID').val();
        if (Util.isEmail(emailId)) {
            $(this).addClass('loading');
            $('#emailID').removeClass('input-error');
            $('.error-msg').hide();
            let url = Urls.getCorpApiUrl().LOGIN_MECHANISM.replace(":emailID:", encodeURIComponent(emailId));
            ServiceConnector.get(url).then(function (respData) {
                if (respData.supportedMechanism == 'Password') {
                    //TODO Redirect to old Dashboard
                    //window.location = '/signin?emailId='+emailId;
                    window.location = Urls.getOldLoginUrl();
                } else {
                    let url = '/checkAuthTokenExist';
                    let headers = {};
                    ServiceConnector.get(url, headers).then(function (respData) {
                        window.location.pathname = '/dashboard';
                    }, function (respErr) {
                        if (!window.location.origin) { // Some browsers (mainly IE) does not have this property, so we need to build it manually...
                            window.location.origin = window.location.protocol + '//' + window.location.hostname + (window.location.port ? (':' + window.location.port) : '');
                        }
                        window.location = Urls.getLoginUrl() + window.location.origin + '/dashboard'; //&user-contact='+ encodeURIComponent(emailId);
                    });
                }
            }, function (respErr) {
                $('#emailNext').removeClass('loading');
                $('#emailID').addClass('input-error');
                $('#loginError').html(JSON.parse(respErr.responseText).error).addClass('error-txt');
            });
        } else {
            $('#emailID').addClass('input-error');
            $('.error-msg').html('Please enter proper email ID').addClass('error-txt');
        }
    },
    signUpLink: function () {
        let url = '/checkAuthTokenExist';
        let headers = {};
        ServiceConnector.get(url, headers).then(function (respData) {
            window.location.pathname = '/signup';
        }, function (respErr) {
            if (!window.location.origin) { // Some browsers (mainly IE) does not have this property, so we need to build it manually...
                window.location.origin = window.location.protocol + '//' + window.location.hostname + (window.location.port ? (':' + window.location.port) : '');
            }
            window.location = Urls.getLoginUrl() + window.location.origin + '/signup';
        });
    },
    loginToOldDashboard: function () {
        console.log("loginToOldDashboard");
        let password = $('#password').val();
        if (password.length <= 0) {

        } else {
            let url = Urls.getCorpApiUrl().OLD_DASHBOARD_LOGIN;
            let inputJson = {
                "email": Util.getUrlParameter('emailId'),
                "password": password
            };
            let headers = {
                "Content-Type": "application/json"
            };
            ServiceConnector.post(inputJson, url, headers).then(function (respData) {
                console.log(respData);
            }, function (respErr) {
                console.log(respErr);
            });
        }
    },
    selectCorporate: function () {
        $(".list-group-item").removeClass("list-group-item-activeSelect  active");
        $(".list-group-item-text").removeClass("active");

        $(this).addClass("list-group-item-activeSelect active");
        $(".list-group-item-text").addClass("active");

        $(".signup__form___button--switch").addClass('signup__form___button--switch-background');
        let selectedCorporate = $(this).attr('data-key')
        console.log(Storage.get("SELECTED_CORP_ID"));
        Storage.set("SELECTED_CORP_ID", selectedCorporate);
    },
    corporateSwitchWhileLogin: function () {
        let requestedSource = $('#requestedSource').val()
        if (Storage.get("SELECTED_CORP_ID")) {
            $('#corporateSwitcherModal').modal();
            let url = '/setSelectedCorporatesID?corpID=' + Storage.get("SELECTED_CORP_ID");
            let headers = {};
            ServiceConnector.get(url, headers).then(function (respData) {
                if(!requestedSource){
                    if (respData.goToCorpDetails) {
                        window.location.href = '/account/corpDetails';
                    } else {
                        let isSodexoPhpSm = COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.SM_CORP_IDS[Env.CURR_ENV.toLowerCase()].indexOf(parseInt(Storage.get("SELECTED_CORP_ID"))) > -1;
                        if (isSodexoPhpSm) {
                            window.location.href = '/companies/' + respData.id + '/' + 'sm-corp/gift-pass';
                        } else if(respData.product=="optima"){
                            window.location.href = '/companies/' + respData.id + '/' + respData.product;
                        } else if(respData.product=="SPOTLIGHT"){
                            window.location.href = '/companies/' + respData.id + '/' + 'optima';
                        // to do  window.location.href = '/companies/' + respData.id + '/spotlight';
                        } else if(respData.product=="EXPRESS"){
                            window.location.href = '/companies/'+respData.id+'/cashless';
                        } else if(respData.product=="EXPENSE"){
                            window.location.href = '/companies/' + respData.id + '/' + 'optima';
                            // to do window.location.href = '/companies/' + respData.id + '/expense';
                        } else {
                            window.location.href = '/companies/' + respData.id + '/' + respData.product;
                        }
                    }
                }else{
                    window.location.href = '/documents-drive?source='+requestedSource;
                }
            }, function (respErr) {
                console.log(respErr);
                if(respErr.status === 401){
                    $('#corporateSwitcherModal').modal('hide');
                    $('#corporateSwitcherErrorModal').modal();
                }
            });
        }
    }
};
import FundHistoryCtrl from "../controller/fundHistoryCtrl";
import FundsService from '../service/fundsSvc';
import util from '../common/util';
export default {
    fundAccSelect: function() {
        $("#funding-name").html($(this).html());
        $("#accountId").val($(this).attr("data-value"));
        FundHistoryCtrl.changeAccountId();
    },
    downloadReport:function(){
        let name = $(this).attr('data-name');
        let accountID = $('#accountId').val();
        let startDate, endDate;
        if (name && (name == 'account' || name == 'fund') && accountID) {
            let dateInput;
            dateInput = name == 'account' ? $('#tranferPeriodAccount') : $('#tranferPeriodFund');
            let x = dateInput.val().split(' - ');
            startDate = util.dateToepochTime(x[0], '-', 'after').toString();
            endDate = util.dateToepochTime(x[1], '-', 'before').toString();
            if (startDate && endDate) {
                $('#loaderModal').modal('show');
                FundsService.downloadReportPhp(name, accountID, startDate, endDate).then(function(responseData) {
                    if (responseData && responseData.url) {
                        const decodedURL = $("<div/>").html(responseData.url).text();
                        window.open(decodedURL , '_blank');
                        let message = "Report has been downloaded successfully";
                        // dateInput.val('');
                        $('#loaderModal').modal('hide');
                        $('#success-img').show();
                        $('#errorResponse').html("");
                        $('#errorResponseMsg').html(message);
                        $('#responseBtn').html('DONE');
                        $('#requestResponseModal').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#requestResponseModal').modal('show');
                    }
                }, function(responseError) {
                    let errorObj = {}; let errorData = {message: 'Something went wrong, failed to download report', statusText: 'Failed to Download Report'};
                    if (responseError){
                        errorObj = JSON.parse(responseError.responseText);
                    }
                    if (errorObj && errorObj.message) {
                        // errorData.message = errorObj.message;
                    }
                    if (errorObj && errorObj.statusText) {
                        errorData.statusText = errorObj.statusText;
                    }
                    $('#error-img').show();
                    if(util.checkGenie()){    
                        $('#errorResponse').html("Failed to Download Report");
                        $('#errorResponseMsg').html("You do not have access to download report");
                    } else {  
                        $('#errorResponse').html(errorData.statusText);
                        $('#errorResponseMsg').html(errorData.message);
                    }
                    $('#loaderModal').modal('hide');
                    $('#responseBtn').html('DONE');
                    $('#requestResponseModal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#requestResponseModal').modal('show');
                });
            }
        }
    }


}

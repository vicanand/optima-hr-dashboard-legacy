import TransferDetailCtrl from "../controller/expense/escalations-detail-ctrl";
import EscalationSvc from '../service/expense/escalations-svc';

export default {
    approveClaim: function(e) {
        let claimId = $(this).data("claim-id"),
            programID = $('#transferId').val(),
            cancelPayoutLoader = $("#escalationLoader"),
            approveClaimSucess = $("#escalationProgramSucess"),
            approveClaimError = $("#escalationApproveError");
        $("#escalationProgramApprove").modal("hide");
        cancelPayoutLoader.modal("show");
        approveClaimError.hide();
        EscalationSvc.approveEscalatedClaim(programID, claimId).then(function(respData) {
            cancelPayoutLoader.modal("hide");
            approveClaimSucess
                .find('#msg')
                .text('The escalation has been approved.');
            approveClaimSucess.modal("show");
        }, function(respErr) {
            cancelPayoutLoader.modal("hide");
            approveClaimError.modal("show");
        });
    },
    declineClaim: function(e) {
        let claimId = $(this).data("claim-id"),
            programID = $('#transferId').val(),
            declineClaimLoader = $("#escalationLoader"),
            declineClaimSuccess = $("#escalationProgramSucess"),
            declineClaimError = $("#escalationDeclineError");
        $("#revokePayoutConfirm").modal("hide");
        declineClaimLoader.modal("show");
        $("#escalationProgramDecline").hide();
        EscalationSvc.declineEscalatedClaim(programID, claimId).then(function(respData) {
            declineClaimLoader.modal("hide");
            declineClaimSuccess
                .find('#msg')
                .text('The escalation has been declined.');
            declineClaimSuccess.modal("show");
        }, function(respErr) {
            declineClaimLoader.modal("hide");
            declineClaimError.modal("show");
        });
    },
    approveClaimConfirm: function(e) {
        let claimId = $(this).closest('tr').data("claim-id"),
            modalEle = $('#escalationProgramApprove'),
            name = $(this).closest('tr').data("name"),
            amount = $(this).closest('tr').data("amount");
        modalEle.find('.name').text(name);
        modalEle.find('.amount').text(amount);
        $("#escalationProgramApprove").modal("show");
        $('#escalationProgramApprove .approved')
            .attr('data-claim-id', claimId);
    },
    declineClaimConfirm: function(e) {
        let claimId = $(this).closest('tr').data("claim-id"),
            modalEle = $('#escalationProgramDecline'),
            name = $(this).closest('tr').data("name"),
            amount = $(this).closest('tr').data("amount");
        modalEle.find('.name').text(name);
        modalEle.find('.amount').text(amount);
        $("#escalationProgramDecline").modal("show");
        $('#escalationProgramDecline .decline')
            .attr('data-claim-id', claimId);
    },
    reload: function(e) {
        window.location.reload();
    }
}

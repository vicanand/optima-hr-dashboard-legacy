import ServiceConnector from '../common/serviceConnector';
import yearEndClosure from '../service/year-end-closure.js';
import Util from '../common/util';
import yearEndClosureDates from '../../template/tables/year-end-closure-dates.ejs';

var showYearEndClosureDate = function(respData){
    let yearEndClosureTemplate = ejs.compile(yearEndClosureDates),
    lastIssuanceDate = Util.timeStampToDate(respData.lastIssuanceDate),
    lastClaimUploadDate = Util.timeStampToDate(respData.lastClaimUploadDate),
    lastClaimProcessDate = Util.timeStampToDate(respData.lastClaimProcessDate),
    cardCloseDate = Util.timeStampToDate(respData.cardCloseDate),
    reportGenerationDate = Util.timeStampToDate(respData.reportGenerationDate),
    yearEndClosureHtml;
    let datesParam = {
        lastIssuanceDate: {
            formatted: lastIssuanceDate,
            iso: respData.lastIssuanceDate
        },
        lastClaimUploadDate: {
            formatted: lastClaimUploadDate,
            iso: respData.lastClaimUploadDate
        },
        lastClaimProcessDate: {
            formatted: lastClaimProcessDate,
            iso: respData.lastClaimProcessDate
        },
        cardCloseDate: {
            formatted: cardCloseDate,
            iso: respData.cardCloseDate
        },
        reportGenerationDate: {
            formatted: reportGenerationDate,
            iso: respData.reportGenerationDate
        }
    };
    yearEndClosureHtml = yearEndClosureTemplate(datesParam);
    $('.yearEndClosuretable').html(yearEndClosureHtml);
}

export
default {
    createProgram: function() {
        $('#createProgramLoader').modal();
        let url = '/createCashlessProgram?productType=' + $(this).attr('data-programType') +
            '&currency=' + $('input[name="currency"]:checked').val();
        let headers = {
            'X-program-description' : $('#programDescription').val(),
            'programname' : $('#programName').val()
        };

        ServiceConnector.get(url, headers).then(function(respData) {
            $('#createProgramLoader').modal('hide');
            $('#setupProgramModal').modal('hide');
            $('#createProgramSuccess').modal('show');
            $("#viewProgramBtnSuccess").attr("href", "cashless/" + respData.id);


        }, function(respErr) {
          //DUMMY COMMIT
            console.log('Error');
            $('#createProgramLoader').modal('hide');
            $('#createProgramError').modal('show');

        })
    },
    scheduleYearDone: function() {
        $('#myModalScheduleYearSuccessful').modal('hide');
        location.reload();
    },
    closureDate: function() {
        let $scheduleYear = $('#scheduleYear');
        let $tableLoader = $(".tableLoader");
        $tableLoader.show();
        var report_generation_date = $(this).val().split("/");
        report_generation_date = new Date(report_generation_date[2], report_generation_date[1] - 1, report_generation_date[0], 23, 59, 59);
        report_generation_date = report_generation_date.toISOString();
        let params = {
            reportGenerationDate: report_generation_date
        };
        yearEndClosure.getProgramClosureSuggestion(params).then(function(respData) {
            $tableLoader.hide();
            showYearEndClosureDate(respData);
        });

        if ($('#closureDate').val().length === 0) {
            $scheduleYear.attr('disabled', 'disabled');
            $scheduleYear.addClass('btn-disabled');
        } else {
            $scheduleYear.removeAttr('disabled');
            $scheduleYear.removeClass('btn-disabled');
        }
    },
    scheduleYear: function() {
        var obj = {
            lastIssuanceDate: $('.lastIssuanceDate').data('issuance-date') || '',
            lastClaimUploadDate: $('.lastClaimUploadDate').data('claim-upload-date') || '',
            lastClaimProcessDate: $('.lastClaimProcessDate').data('claim-process-date') || '',
            cardCloseDate: $('.cardCloseDate').data('card-close-date') || '',
            reportGenerationDate: $('.reportGenerationDate').data('report-generation-date') || ''
        };
        yearEndClosure.scheduleProgramClosure(obj).then(function(respData) {
            $('#myModalScheduleYear').modal('hide');
            $('#myModalScheduleYearSuccessful').css('display','block');
            $('.last-issuance-date').hide();
        });
    },
    programNameChange: function() {
        let $createProgram = $('#createProgram');
        if ($(this).val().length === 0 || $(this).val().length > 40) {
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $createProgram.removeAttr('disabled');
            $createProgram.removeClass('btn-disabled');
        }
    },
    close_Me:function(){
        location.reload();
        console.log("hello");
    }
   
}

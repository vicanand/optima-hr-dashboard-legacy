import ServiceConnector from '../common/serviceConnector';

export
default {
    createProgram: function() {
        let createProgram = $('#createProgramLoader');
        createProgram.modal();
        let url = '/createProgram?fundingAccID=' + $('#fundingAccountID').val() + '&productType=' + $(this).attr('data-programType') +
            '&programName=' + $('#programName').val();
        let headers = {};
        ServiceConnector.get(url, headers).then(function(respData) {
            createProgram.modal('hide');
            $('#setupProgramModal').modal('hide');
            $('#createProgramSuccess').modal('show');

            $("#viewProgramBtnSuccess").attr("href", "optima/" + respData.id)

        }, function(respErr) {
            console.log('Error');
            createProgram.modal('hide');
            $('#createProgramError').modal('show');

        })
    },
    fundingAccountIDChange: function() {
        let $createProgram = $('#createProgram');
        if ($(this).val() == '0') {
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $createProgram.removeAttr('disabled');
            $createProgram.removeClass('btn-disabled');
        }
    },

    programNameChange: function() {
        let $createProgram = $('#createProgram');
        if ($(this).val().length === 0 || $(this).val().length > 40) {
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $createProgram.removeAttr('disabled');
            $createProgram.removeClass('btn-disabled');
        }
    }
}

import DomEventHandler from "../common/domEventHandler.js";
import FundingSvc from "../service/fundingSvc.js";
import Env from "../common/env";
import Constants from "../common/constants";
import ServiceConnector from "../common/serviceConnector";
import Storage from "../common/webStorage.js";
import programLinkedModal from "../../template/program-linked-modal.ejs";
import Meta from "../common/metaStorage.js";
import Util from "../common/util";
import CacheDataStorage from "../common/cacheDataStorage";

let openNewFundAcc = function() {
  _loadingPopUp();
  var reqObj = {
    corpID: $.trim($("#corpIDForFund").text()),
    name: Util.htmlEntities($("#recipient-name").val())
  };
  FundingSvc.openNewFundAccSvc(reqObj).then(
    function(respData) {
      $(".fundingAccName").html(Util.htmlDecoder(respData.name));
      $(".fundingAccDescription").html(respData.description);
      if (respData.transferInDetails && respData.transferInDetails[0]) {
        $(".fundingAccNumber").html(
          respData.transferInDetails[0].accountNumber
        );
        $(".fundingAccIFSC").html(respData.transferInDetails[0].ifscCode);
        $("#newAccDetailsWrpr").show();
      } else {
        $("#newAccDetailsWrpr").hide();
      }
      successPopUp();
    },
    function() {
      FailPopUp();
    }
  );
  // console.log(reqObj);
};
let _loadingPopUp = function() {
  $("#myModalNewAccount").modal("hide");
  $("#myModalLoaderFund").modal("show");
};
let successPopUp = function() {
  $("#myModalLoaderFund").modal("hide");
  $("#myModalInitiatedSuccessFund").modal("show");
};
let FailPopUp = function(respErr) {
  $("#myModalLoaderFund").modal("hide");
  let checkGenie = 0;
  if (localStorage.getItem("checkGenie")) {
    checkGenie = parseInt(localStorage.getItem("checkGenie"));
  }
  if (typeof respErr === "undefined" && checkGenie != 0) {
    // for genie login error response coming as undefined.
    $("#myModalOopsFundGenie").modal("show");
  } else {
    if (
      JSON.parse(respErr.responseText).message ==
      "Request with same reference number already exists"
    ) {
      $("#duplicateFundError").modal("show");
    } else {
      $("#myModalOopsFund").modal("show");
    }
  }
};

let ifiBankName = "";
let ac_name = "";

let routeToAddFund = function() {
  initGAEvent(
    "event",
    "Funding Accounts",
    "click",
    "Raise Fund Addition Request, corpId = " +
      Meta.getMetaElement("corpID") +
      ", compId=" +
      Meta.getMetaElement("companyID") +
      ", userId=" +
      Meta.getMetaElement("userID") +
      ", ifiBankName=" +
      ifiBankName +
      ", accountName=" +
      ac_name +
      ", timestamp=" +
      new Date().toLocaleString() +
      ", Success"
  );
  window.location.href = "/admin/funding-accounts/add-funds-details";
};

let addFundsOnHome = function() {
  $("#addFundsLoader").modal("show");
  let accountId = $(this).attr("data-attr-accountid");
  let ifiId = $(this).attr("data-attr-ifiid");
  let name = $(this).attr("data-attr-name");
  let url =
    "/setSelectedFundingAcc?accountID=" +
    accountId +
    "&ifiId=" +
    ifiId +
    "&name=" +
    name;
  let headers = {};

  Storage.set("ACCOUNT_ID", accountId);
  ServiceConnector.get(url, headers).then(
    function(respData) {
      $("#addFundsLoader").modal("hide");
      console.log("respDataInFundingAccoutHome", respData);
      if (
        respData.ifiID == Constants.KOTAK_FUNDING_ACCS[Env.CURR_ENV] ||
        respData.ifiID == Constants.HDFC_FUNDING_ACCS[Env.CURR_ENV]
      ) {
        $("#acNameModalWrpr,#addFundsBtnWrpr,.list-number-circle").hide();
        $("#addFundDetailsHdg").text(
          "Please use convenient channels to pay to the following bank account."
        );
        $("#ifiAccNo").text(
          respData.transferInDetails[0]
            ? respData.transferInDetails[0].accountNumber
            : ""
        );
        if (respData.ifiID == Constants.KOTAK_FUNDING_ACCS[Env.CURR_ENV]) {
          $("#ifiBankName").text("Kotak Mahindra Bank");
          $("#ifiBankBranch").text("MUMBAI-NARIMAN POINT");
          ifiBankName = "Kotak Mahindra Bank";
        } else {
          $("#ifiBankName").text("HDFC Bank");
          $("#ifiBankBranch").text("Mumbai - Mistry Bhavan Extn Ctr");
          ifiBankName = "HDFC Bank";
        }
        $("#ifiIfscCode").text(
          respData.transferInDetails[0]
            ? respData.transferInDetails[0].ifscCode
            : ""
        );
        $("#acc_name strong").text(Util.htmlDecoder(respData.name));
        ac_name = Util.htmlDecoder(respData.name);
      } else {
        let addFundDetailsHeader =
          "Drop a cheque/transfer to Zeta’s Bank Account";

        const country_configs = CacheDataStorage.getItem(
          Constants.COUNTRY_CONFIGS
        );
        if (country_configs) {
          const addFundDetailsHeaderConfig = _.get(
            country_configs,
            "fundingHomeDOMEvent.addFundsOnHome.addFundDetailsHeader"
          );
          if (addFundDetailsHeaderConfig) {
            addFundDetailsHeader = addFundDetailsHeaderConfig;
          }
        }
        $("#acNameModalWrpr,#addFundsBtnWrpr,.list-number-circle").show();
        $("#addFundDetailsHdg").text(addFundDetailsHeader);
        $("#ifiAccNo").text(respData.ifiDetails.bankDetails.accountNumber);
        $("#ifiBankName").text(respData.ifiDetails.bankDetails.bankName);
        $("#ifiBankBranch").text(respData.ifiDetails.bankDetails.bankAddress);
        $("#ifiIfscCode").text(respData.ifiDetails.bankDetails.IFSC);
        $("#acc_name strong").text(Util.htmlDecoder(respData.name));
        ifiBankName = respData.ifiDetails.bankDetails.bankName;
        ac_name = Util.htmlDecoder(respData.name);
      }

      //Storage.set('ACCOUNT_ID', accountId);
      Storage.set("ACCOUNT_NAME", Util.htmlDecoder(respData.name));

      const country_configs = CacheDataStorage.getItem(
        Constants.COUNTRY_CONFIGS
      );
      if (
        country_configs &&
        _.get(
          country_configs,
          "fundingHomeDOMEvent.addFundsOnHome.skipFundChequeTransferModal"
        )
      ) {
        // Skip fundChequeTransfer modal
        routeToAddFund();
      } else {
        $("#fundChequeTransfer").modal("show");
      }
      initGAEvent(
        "event",
        "Funding Accounts",
        "click",
        "Add Funds Clicked, corpId = " +
          Meta.getMetaElement("corpID") +
          ", compId=" +
          Meta.getMetaElement("companyID") +
          ", userId=" +
          Meta.getMetaElement("userID") +
          ", ifiBankName=" +
          ifiBankName +
          ", accountName=" +
          ac_name +
          ", timestamp=" +
          new Date().toLocaleString() +
          ", Success"
      );
    },
    function(respErr) {
      initGAEvent(
        "event",
        "Funding Accounts",
        "click",
        "Add Funds Clicked, corpId = " +
          Meta.getMetaElement("corpID") +
          ", compId=" +
          Meta.getMetaElement("companyID") +
          ", userId=" +
          Meta.getMetaElement("userID") +
          ", ifiBankName=" +
          ifiBankName +
          ", accountName=" +
          ac_name +
          ", timestamp=" +
          new Date().toLocaleString() +
          ", Failed, Reason=" +
          JSON.stringify(respErr)
      );
      $("#addFundsLoader").modal("hide");
      console.log("Error", respErr);
    }
  );
};

$(document).on("click", ".addFundsOnHome", addFundsOnHome);

let enableOpenAccBtn = function() {
  if (Util.htmlEntities($("#recipient-name").val()).length != 0) {
    $("#openNewFundAcc").removeAttr("disabled");
    $("#openNewFundAcc").removeClass("btn-disabled");
  } else {
    $("#openNewFundAcc").attr("disabled", "disabled");
    $("#openNewFundAcc").addClass("btn-disabled");
  }
};
let clearInpVal = function() {
  $("#recipient-name").val("");
  //$('#message-text').val('');
  enableOpenAccBtn();
};
let captureFundID = function() {
  Storage.set("ACCOUNT_ID", $(this).attr("data-attr-accountid"));
};
let showProgramsFundedModal = function() {
  $("#programsLinkedModalLoader").modal("show");
  let url = "/programsFunded?fundingAccID=" + $(this).data("funding-id");
  let headers = {};
  ServiceConnector.get(url, headers).then(
    function(respData) {
      $("#programsLinkedModalLoader").modal("hide");
      DomEventHandler.renderMyTemplate(
        "programsLinkedModalWrpr",
        programLinkedModal,
        respData
      );
      $("#programsLinkedModalWrpr").modal();
    },
    function(respErr) {
      $("#programsLinkedModalLoader").modal("hide");
      $("#myModalOopsFund").modal("show");
    }
  );
};
let validateTransferTo = function() {
  let selectedAc = $("#transferTo").val();
  if (selectedAc == "select") {
    $("#fundTransferBtn").prop("disabled", true);
    $("#fundTransferBtn").addClass("btn-disabled");
    $("#transferTo").addClass("input-has-error");
    $("#transferTo")
      .parents(".form-group")
      .find(".error-message")
      .html("Please select an account to transfer the funds");
    return false;
  } else {
    $("#transferTo").removeClass("input-has-error");
    $("#transferTo")
      .parents(".form-group")
      .find(".error-message")
      .html("");
    return true;
  }
};
let restrictInvalidAmount = function(e) {
    if (e.originalEvent.key === '-') {
        return false;
    }
}
let activateFundsTransfer = function() {
  let transferAmount = $("#transferAmount").val();
  let transferAcValidation = validateTransferTo();
  let transferLimit = $("#accDetails").data("accbalpaise") / 100;
  if (transferAmount.length == 0) {
    $("#transferAmount").removeClass("input-has-error");
    $("#transferAmount")
      .parents(".form-group")
      .find(".error-message")
      .html("");
    $("#fundTransferBtn").prop("disabled", true);
    $("#fundTransferBtn").addClass("btn-disabled");
  } else {
    if (transferAmount <= 0 || transferLimit < transferAmount) {
      if (transferAmount <= 0) {
        $("#transferAmount")
          .parents(".form-group")
          .find(".error-message")
          .html("Please enter a positive amount");
      } else {
        $("#transferAmount")
          .parents(".form-group")
          .find(".error-message")
          .html("Oops! The amount entered is greater than the current balance");
      }
      $("#transferAmount").addClass("input-has-error");
      $("#fundTransferBtn").prop("disabled", true);
      $("#fundTransferBtn").addClass("btn-disabled");
    } else {
      $("#transferAmount").removeClass("input-has-error");
      $("#transferAmount")
        .parents(".form-group")
        .find(".error-message")
        .html("");
      if (transferAcValidation === true) {
        $("#fundTransferBtn").prop("disabled", false);
        $("#fundTransferBtn").removeClass("btn-disabled");
      }
    }
  }
};
let transferFunds = function() {
  $("#fundTransferBtn").text("TRANSFERRING FUNDS");
  $("#fundTransferBtn").addClass("btn-disabled");
  $("#fundTransferBtn").prop("disabled", true);
  $("#fundsTransferLoader").show();
  let requestPayload = {};
  let recepientName = $("#transferTo")
    .find(":selected")
    .data("accname");
  let senderName = $("#accDetails").data("accname");
  requestPayload.corpID = $("meta[name='corpID']").attr("content");
  requestPayload.requestID =
    "id" +
    Math.random()
      .toString(36)
      .substr(2, 5) +
    "un" +
    Math.random()
      .toString(36)
      .substr(2, 5) +
    "no" +
    Math.random()
      .toString(36)
      .substr(2, 5);
  requestPayload.remarks =
    "Funds transferred from " + senderName + " to " + recepientName;
  requestPayload.sourceFA = $("#accDetails").data("accountid");
  requestPayload.recipientFA = $("#transferTo").val();
  requestPayload.amount = $("#transferAmount").val() * 100;

  FundingSvc.transferFunds(requestPayload).then(
    function(successResponse) {
      console.log(successResponse);
      let message = "Funds transferred successfully";
      $("#transferFundsModal").modal("hide");
      $("#success-img").show();
      $("#errorResponse").html("");
      $("#errorResponseMsg").html(message);
      $("#responseBtn").html("DONE");

      $("#requestResponseModal").modal({
        backdrop: "static",
        keyboard: false
      });
      $("#requestResponseModal").modal("show");
    },
    function(responseError) {
      console.log(responseError);
      let errorObj = JSON.parse(responseError.responseText);
      $("#transferFundsModal").modal("hide");
      $("#error-img").show();
      if (Util.checkGenie()) {
        $("#errorResponse").text("Oops!");
        $("#errorResponseMsg").text("You do not have access to transfer funds");
      } else {
        $("#errorResponse").html(responseError.statusText);
        $("#errorResponseMsg").html(errorObj.message);
      }
      $("#responseBtn").html("DONE");

      $("#requestResponseModal").modal({
        backdrop: "static",
        keyboard: false
      });
      $("#requestResponseModal").modal("show");
    }
  );
};
let reloadPage = function() {
  $("#responseBtn").addClass("btn-disabled");
  $("#responseBtn").prop("disabled", true);
  location.reload();
};

export default {
    captureFundID: captureFundID,
    clearInpVal: clearInpVal,
    routeToAddFund: routeToAddFund,
    addFundsOnHome: addFundsOnHome,
    openNewFundAcc: openNewFundAcc,
    successPopUp: successPopUp,
    FailPopUp: FailPopUp,
    enableOpenAccBtn: enableOpenAccBtn,
    showProgramsFundedModal: showProgramsFundedModal,
    validateTransferTo: validateTransferTo,
    activateFundsTransfer: activateFundsTransfer,
    transferFunds: transferFunds,
    reloadPage: reloadPage,
    restrictInvalidAmount: restrictInvalidAmount
}

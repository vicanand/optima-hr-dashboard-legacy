import DomEventHandler from '../common/domEventHandler.js';
import Table from '../widgets/table';
import EmployeeDetailsSvc from '../service/employeeDetailsSvc.js';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import EmployeeSvc from '../service/employeeSvc';

pdfMake.vfs = pdfFonts.pdfMake.vfs;
import DocumentCardTemplate from "../../template/section/employee-details/singleProgramDetails.ejs";
import DocumentOverviewCardTemplate from "../../template/section/employee-details/overview.ejs";
import URLs from '../common/urls'
import Utils from '../common/util'
import Storage from '../common/webStorage.js';
import Constants from '../common/constants';
import CacheDataStorage from '../common/cacheDataStorage';
import Modal from '../../template/Modal/statusModal.ejs';

function openPDF(s3FileUrl, fileName) {
    var link = document.createElement('a');
    link.href = s3FileUrl;
    link.download = fileName;
    link.target = "_blank";
    link.click();
    $('#downloadStatement').attr('disabled', false).removeClass('submit-btn-disabled');
    $('#downloadStatementSingle').attr('disabled', false).removeClass('submit-btn-disabled');
    $('#downloadStatementLoader').hide();
    $('#beneficiaryStatement').modal('hide');
}

export default {
    toggleEmployeeState: function() {
        let zetaUserID = $(this).data('zetauserid');
        let username = $(this).data('username');
        let checked = $(this).prop('checked');
        if(checked) {
            EmployeeSvc.addUserToGroup(zetaUserID, username).then((res) => {
                DomEventHandler.renderMyTemplate('statusModal', Modal, {
                    status: 'success',
                    message: 'User Added Successfully'
                })
                $('#status-modal').modal('show');
            }, (err) => {
                $(this).prop('checked', false);
                DomEventHandler.renderMyTemplate('statusModal', Modal, {
                    status: 'error',
                    message: 'Failed to Add user'
                })
                $('#status-modal').modal('show');
            });
        }
        else {
            EmployeeSvc.removeUserFromGroup(zetaUserID).then((res) => {
                DomEventHandler.renderMyTemplate('statusModal', Modal, {
                    status: 'success',
                    message: 'User Removed Successfully'
                })
                $('#status-modal').modal('show');
            }, (err) => {
                $(this).prop('checked', true);
                DomEventHandler.renderMyTemplate('statusModal', Modal, {
                    status: 'error',
                    message: 'Failed to Remove user'
                })
                $('#status-modal').modal('show');
            });
        }
    },
    toggleReimbursement: function (e) {
        var icon = $(this).find('.glyphicon');
        $('#reimbursementSection').slideToggle(400);
        if (icon.hasClass('glyphicon-menu-up')) {
            icon.removeClass('glyphicon-menu-up')
                .addClass('glyphicon-menu-down');
        } else if (icon.hasClass('glyphicon-menu-down')) {
            icon.removeClass('glyphicon-menu-down')
                .addClass('glyphicon-menu-up');
        }
    },
    cardProgramIndividualBeneficiary: function (programid) {
        $("#searchingProgramDetails").show(); // loaderemployee-program-details
        $("#employee-program-details").hide();
        Table.init();
        let corpId = $("meta[name='corpID']").attr("content");
        let employeeId = $("#employeeId").val();
        let companyId = $("#selected-company-id").val();
        $('meta[name=companyID]').remove();
        $('head').append('<meta name="companyID" content= ' + companyId + ' >');
        let programId = '';
        if (($(this).attr('id')) != null) {
            programId = $(this).attr('id');
            $('.hideshow li.active').removeClass('active');
            $('.hideshow .active-more').css("color", "#190f27");
            $('#more-down-img').attr('src', '../../../images/down-arrow-black.svg');
            let programDetailsUrl = URLs.getOMSApiUrl().GET_BENEFIT_DETAILS_PROGRAM
                .replace(":authToken:", Storage.get('authToken'))
                .replace(":corpID:", corpId)
                .replace(":companyID:", companyId)
                .replace(":programID:", programId);
            EmployeeDetailsSvc.getCardProgramDetails(programDetailsUrl).then(function (programDetails) {
                sessionStorage.setItem('d-cardname', programDetails.cardProgramObject.cardName);
            })
        } else {
            programId = programid;
        }

        // getting scheduled amount
        let commonParam = {
            productID: programId,
            employeeID: employeeId
        };
        let noOfActiveCards = 0;
        // checking if tab is overview then render overview 
        if (programId === 'overview-tab') {
            EmployeeDetailsSvc.getBenefitIndividualEmployee(commonParam).then(function (respData) {
                let overviewData = respData.employeeWithCardDetails[0];
                overviewData.noOfActiveSuperCards = 0;
                overviewData.noOfActiveSuperTags = 0;
                if (overviewData.cards) {
                    for (let k = 0; k < overviewData.cards.length; k++) {
                        if (overviewData.cards[k].status == 'ACTIVE') {
                            noOfActiveCards++;
                        }
                    }
                }
                overviewData.noOfActiveCards = noOfActiveCards;
                DomEventHandler.renderMyTemplate("employee-program-details", DocumentOverviewCardTemplate, overviewData);
                $("#searchingProgramDetails").hide();
                $("#employee-program-details").show();
                let _loadGraph = function (graphId, graphData, graphLabels, colors, graphType) {
                    var ctx = document.getElementById(graphId);
                    var myChart = new Chart(ctx, {
                        type: graphType,
                        data: {
                            labels: graphLabels,
                            datasets: [{
                                data: graphData,
                                backgroundColor: colors,
                                borderWidth: 0
                            }]
                        },
                        options: {
                            legend: {
                                display: true
                            },
                            tooltips: {
                                callbacks: {
                                    label: function (tooltipItems, data) {
                                        return '₹ ' + Util.formatINR(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index]);
                                    }
                                }
                            }
                        }
                    });
                };

                let data = [],
                    labels = [],
                    colors = [];
                $('#spendDataLoader').hide();
                $('#cardsTransferWrpr').show();
                $('.employee-program-details .graph-wrpr .card-info-hidden').each(function () {
                    let transferAmount = $(this).data('transferamount').toString();
                    if (transferAmount) {
                        var cardname;
                        data.push(transferAmount.replace(',', ''));
                        labels.push($(this).data('cardname'));
                        cardname = $(this).data('cardname').toLowerCase();
                        colors.push($(this).data('color'));
                    }
                });
                if (data.length < 1 || (data.length === 1 && data[0] === '0')) {
                    $('#cardsTransferEmpty').show();
                    $('#cardsTransferWrpr').hide();
                } else {
                    _loadGraph("cardsTransferAmount", data, labels, colors, 'doughnut');
                }
            });
        } else { // if the tab is not overview tab
            let scheduledCount = 0;
            let scheduledAmount = 0;
            let waitingCount = 0;
            let waitingAmount = 0;
            let cancelledCount = 0;
            let cancelledAmount = 0;
            let currency = '';
            let noOfCards = 0;
            // let cards = [];
            // let benefits = [];
            let empCardDetails = {};
            let singleCardDetails = {};
            let dataArray = [];
            let programDetails = {};
            programDetails.scheduledCount = '';
            programDetails.scheduledAmount = '';
            programDetails.waitingCount = '';
            programDetails.waitingAmount = '';
            programDetails.cancelledCount = '';
            programDetails.cancelledAmount = '';
            programDetails.noOfCards = noOfCards;
            programDetails.currencySymobol = '';
            programDetails.allCardDetails = {};

            programDetails.programId = programId;
            let req1 = EmployeeDetailsSvc.getPayoutStats(commonParam);
            let req2 = EmployeeDetailsSvc.getBenefitIndividualEmployee(commonParam);
            $.when(req1, req2).done(function (respData, empCardDetails) {
                if (respData.payoutStatsByStatus.payoutStatsByStatus.scheduled) { //schedule-amount-id

                    if (respData.payoutStatsByStatus.payoutStatsByStatus.scheduled.payoutsCount) {
                        scheduledCount = respData.payoutStatsByStatus.payoutStatsByStatus.scheduled.payoutsCount;
                        programDetails.scheduledCount = scheduledCount;
                    }
                    if (respData.payoutStatsByStatus.payoutStatsByStatus.scheduled.totalAmount) {
                        scheduledAmount = respData.payoutStatsByStatus.payoutStatsByStatus.scheduled.totalAmount.amount;
                        currency = respData.payoutStatsByStatus.payoutStatsByStatus.scheduled.totalAmount.currency;
                        scheduledAmount = Utils.formatCurrency(scheduledAmount / 100, currency);
                        programDetails.scheduledAmount = scheduledAmount;
                    }
                }
                if (respData.payoutStatsByStatus.payoutStatsByStatus.temporarily_failed) {
                    if (respData.payoutStatsByStatus.payoutStatsByStatus.temporarily_failed.payoutsCount) {
                        waitingCount = respData.payoutStatsByStatus.payoutStatsByStatus.temporarily_failed.payoutsCount;
                        programDetails.waitingCount = waitingCount;
                    }
                    if (respData.payoutStatsByStatus.payoutStatsByStatus.temporarily_failed.totalAmount) {
                        waitingAmount = respData.payoutStatsByStatus.payoutStatsByStatus.temporarily_failed.totalAmount.amount;
                        currency = respData.payoutStatsByStatus.payoutStatsByStatus.temporarily_failed.totalAmount.currency;
                        waitingAmount = Utils.formatCurrency(waitingAmount / 100, currency);
                        programDetails.waitingAmount = waitingAmount;
                    }
                }
                if (respData.payoutStatsByStatus.payoutStatsByStatus) {
                    if (typeof respData.payoutStatsByStatus.payoutStatsByStatus.scheduled === "undefined" && typeof respData.payoutStatsByStatus.payoutStatsByStatus.temporarily_failed === "undefined") {
                        if (respData.payoutStatsByStatus.payoutStatsByStatus.cancelled) {
                            if (respData.payoutStatsByStatus.payoutStatsByStatus.cancelled.payoutsCount) {
                                cancelledCount = respData.payoutStatsByStatus.payoutStatsByStatus.cancelled.payoutsCount;
                                programDetails.cancelledCount = cancelledCount;
                            }
                            if (respData.payoutStatsByStatus.payoutStatsByStatus.cancelled.totalAmount) {
                                cancelledAmount = respData.payoutStatsByStatus.payoutStatsByStatus.cancelled.totalAmount.amount;
                                currency = respData.payoutStatsByStatus.payoutStatsByStatus.cancelled.totalAmount.currency;
                                cancelledAmount = Utils.formatCurrency(cancelledAmount / 100, currency);
                                programDetails.cancelledAmount = cancelledAmount;
                            }
                        }
                    }
                }
                // single card details
                empCardDetails = empCardDetails.employeeWithCardDetails;
                if (empCardDetails) { //no-of-cardIssued
                    if (empCardDetails.length > 0) {
                        noOfCards = empCardDetails[0].benefits.length;
                        programDetails.noOfCards = noOfCards;
                    }
                    if (empCardDetails[0].benefits.length) {
                        for (let i = 0; i < empCardDetails[0].benefits.length; i++) {
                            singleCardDetails.companyName = empCardDetails[0].companyName;
                            singleCardDetails.benefits = empCardDetails[0].benefits[i];
                            singleCardDetails.cards = empCardDetails[0].cards[i];
                            dataArray.push(singleCardDetails);
                            singleCardDetails = {};
                        }
                        // DomEventHandler.renderTemplateArray("all-cards-areaId", DocumentCardTemplate, dataArray);
                        programDetails.allCardDetails = dataArray;
                    }
                }
                DomEventHandler.renderMyTemplate("employee-program-details", DocumentCardTemplate, programDetails);
                $("#searchingProgramDetails").hide();
                $("#employee-program-details").show();
                $("#schedule-amount-id").show();
                $("#waiting-amount-id").show();
                $("#canceled-amount-id").show();
            });
        }
    },

    downloadStatementBtn: function () {
        let dataValue = $("#tranferPeriod").val();
        dataValue != "" ? $('#downloadStatement').attr('disabled', false).removeClass('submit-btn-disabled') : $('#downloadStatement').attr('disabled', true).addClass('submit-btn-disabled')
        dataValue != "" ? $('#downloadStatementSingle').attr('disabled', false).removeClass('submit-btn-disabled') : $('#downloadStatementSingle').attr('disabled', true).addClass('submit-btn-disabled')
        $('#showDownloadStatementError').hide()
    },
    downloadStatement: function (params) {
        $('#downloadStatement').attr('disabled', true).addClass('submit-btn-disabled');
        $('#downloadStatementSingle').attr('disabled', true).addClass('submit-btn-disabled');
        $('#downloadStatementLoader').show();
        $('#showDownloadStatementError').hide();
        let authToken = Storage.get('authToken');
        let selectedStatement = $("input[name='typeofstatement']:checked").val();
        let dateRange = $("#tranferPeriod").val();
        dateRange = dateRange.split('-');
        let startFromDate = $.trim(dateRange[0]);
        let endFromDate = $.trim(dateRange[1]);
        startFromDate = startFromDate.split(" ");
        endFromDate = endFromDate.split(" ");
        dateRange = startFromDate[2] + '-' + Utils.monthNameToMonth(startFromDate[1]) + '-' + startFromDate[0] + ' - ' + endFromDate[2] + '-' + Utils.monthNameToMonth(endFromDate[1]) + '-' + endFromDate[0];
        let statementName = selectedStatement + Date() + '.pdf';
        let corpID = $("meta[name='corpID']").attr("content");
        let zetaUserID = $(this).data('zetauserid') || sessionStorage.getItem('d-zetauserid');
        let cardProgramID = sessionStorage.getItem('d-cardprogramid');
        let cardID = sessionStorage.getItem('d-card-id');
        let productType = sessionStorage.getItem('d-producttype');
        let userName = $(this).data('username') || sessionStorage.getItem('d-beneficiary-name');
        let userEmail = $(this).data('useremail') || sessionStorage.getItem('d-beneficiary-email');
        if (!userEmail || userEmail == 'N/A') {
            userEmail = $(this).data('phone') || sessionStorage.getItem('d-beneficiary-phone');
        }
        if (!userEmail) {
            userEmail = $("#user_email").text();
            if (!userEmail || userEmail == "N/A") {
                userEmail = $("#user_phone").text();
            }
        }

        if (!zetaUserID) {
            $('#downloadStatement').attr('disabled', false).removeClass('submit-btn-disabled');
            $('#downloadStatementLoader').hide();
            if(Utils.checkGenie()){
                $('#showDownloadStatementError').show()
                $("#showDownloadStatementError #showDownloadStatementErrorMsg").text("You do not have access to download employee statement");
            } else {
                $('#showDownloadStatementError').show()
            }
            $('#showDownloadStatementErrorMsg').html('Failed. Zeta user id not available');
            return false;
        }
        let companyName = $(this).data('companyname') || sessionStorage.getItem('d-company-name');
        let fromDate = Utils.dateToepochTime(dateRange.slice(0, 10), '-', 'after')
        let toDate = Utils.dateToepochTime(dateRange.slice(13, 23), '-', 'before')
        let fromToDate = Utils.timestampToDate(fromDate) + ' to ' + Utils.timestampToDate(toDate);
        let thisCardStatus = sessionStorage.getItem('d-cardstatus');
        let imageUrl = $(".profile-img-cell img").attr('src');
        let url1 = URLs.getOMSApiUrl().GET_TRANSACTIONS
            .replace(":token:", Storage.get('authToken'))
            .replace(":zetaUserID:", zetaUserID)
            .replace(":corpID:", corpID)
            .replace(":cardID:", cardID)
            .replace(":after:", fromDate)
            .replace(":before:", toDate);

        let cardName = sessionStorage.getItem('d-cardname');
        if (!$.trim(cardName)) {
            cardName = $("#benefitCardName").text();
        }
        let paramsObject = {
            'corpID': corpID,
            'zetaUserID': zetaUserID,
            'cardID': cardID,
            'cardStatus': thisCardStatus,
            'companyName': companyName,
            'userEmail': userEmail,
            'productType': productType,
            'cardProgramID': cardProgramID,
            'imageUrl': imageUrl,
            'userName': userName,
            'cardName': cardName,
            'dateRange': dateRange
        }
        var fileName = userName + "_" + companyName + "_" + fromToDate;
        var req = new XMLHttpRequest();
        req.open("POST", '/employeestatement', true);
        req.responseType = "blob";
        req.setRequestHeader('Content-Type', 'application/json')
        req.onload = function (event) {
            var blob = req.response;
            if (blob.size > 100) {
                EmployeeDetailsSvc.getAssestUploadEndPoint(authToken, corpID).then(function (response) {
                    let uploadDetails = response;
                    var formData = new FormData();
                    formData.append("acl", uploadDetails.acl);
                    formData.append("key", uploadDetails.key);
                    formData.append("Policy", uploadDetails.policy);
                    formData.append("X-Amz-Algorithm", uploadDetails.xAmzAlgorithm);
                    formData.append("X-Amz-Credential", uploadDetails.xAmzCredential);
                    formData.append("X-Amz-Signature", uploadDetails.xAmzSignature);
                    formData.append("X-Amz-Date", uploadDetails.xAmzDate);
                    formData.append("success_action_status", uploadDetails.successActionStatus);
                    formData.append("Content-Type", "application/pdf");
                    formData.append("file", new File([blob], fileName, {type: ".pdf", lastModified: new Date()}));
                    var xhr = new XMLHttpRequest();

                    try {
                        xhr.open('POST', uploadDetails.postAction, true);
                        xhr.send(formData);
                        xhr.onreadystatechange = function (respData) {
                            if (xhr.readyState == 4) {
                                let s3FileUrl = uploadDetails.postAction + "/" + uploadDetails.key;
                                
                                const country_configs = CacheDataStorage.getItem(Constants.COUNTRY_CONFIGS);
                                if (country_configs && _.get(country_configs, 'employeeBenefitsDomEvents.downloadStatement.skipSignedDocument')) {
                                    openPDF(s3FileUrl, fileName);
                                } else {
                                 
                                    EmployeeDetailsSvc.getSignedDocument(s3FileUrl).then(function (resp) {
                                        resp = JSON.parse(Utils.htmlDecoder(resp))
                                        if (resp.pdfURL) {
                                            openPDF(resp.pdfURL, fileName);
                                        } else {
                                            $("#showDownloadStatementErrorMsg").text(resp.message);
                                            $('#downloadStatement').attr('disabled', false).removeClass('submit-btn-disabled');
                                            $('#downloadStatementSingle').attr('disabled', false).removeClass('submit-btn-disabled');
                                            $('#downloadStatementLoader').hide();
                                            if(Utils.checkGenie()){
                                                $('#showDownloadStatementError').show()
                                                $("#showDownloadStatementError #showDownloadStatementErrorMsg").text("You do not have access to download employee statement");
                                            } else {
                                                $('#showDownloadStatementError').show()
                                            }
                                        }
                                    }, function (err) {
                                        $("#showDownloadStatementErrorMsg").text(err.message);
                                        $('#downloadStatement').attr('disabled', false).removeClass('submit-btn-disabled');
                                        $('#downloadStatementSingle').attr('disabled', false).removeClass('submit-btn-disabled');
                                        $('#downloadStatementLoader').hide();
                                        if(Utils.checkGenie()){
                                            $('#showDownloadStatementError').show()
                                            $("#showDownloadStatementError #showDownloadStatementErrorMsg").text("You do not have access to download employee statement");
                                        } else {
                                            $('#showDownloadStatementError').show()
                                        }
                                    })
                                }
                            }
                        };
                    } catch (err) {
                        $("#showDownloadStatementErrorMsg").text(respData.error);
                        $('#downloadStatement').attr('disabled', false).removeClass('submit-btn-disabled');
                        $('#downloadStatementSingle').attr('disabled', false).removeClass('submit-btn-disabled');
                        $('#downloadStatementLoader').hide();
                        if(Utils.checkGenie()){
                            $('#showDownloadStatementError').show()
                            $("#showDownloadStatementError #showDownloadStatementErrorMsg").text("You do not have access to download employee statement");
                        } else {
                            $('#showDownloadStatementError').show()
                        }
                    }
                })
            } else {
                var reader = new FileReader();
                reader.onload = function () {
                    let response = JSON.parse(reader.result);
                    $("#showDownloadStatementErrorMsg").text(response.error);
                    $('#downloadStatement').attr('disabled', false).removeClass('submit-btn-disabled');
                    $('#downloadStatementSingle').attr('disabled', false).removeClass('submit-btn-disabled');
                    $('#downloadStatementLoader').hide();
                    if(Utils.checkGenie()){
                        $('#showDownloadStatementError').show()
                        $("#showDownloadStatementError #showDownloadStatementErrorMsg").text("You do not have access to download employee statement");
                    } else {
                        $('#showDownloadStatementError').show()
                    }
                }
                reader.readAsText(blob);
            }
        };
        req.send(Utils.htmlDecoder(JSON.stringify({ params: paramsObject })));
    }
}
import ServiceConnector from '../common/serviceConnector';
import SignupSvc from '../service/signupSvc';
import Urls from '../common/urls';
import Util from '../common/util';
let cinInputData = new FormData();
let cinUploadUrl = "";
let isCinUploaded = false;
let cinDocument = '';
let isPanUploaded = false;
let panInputData = new FormData();
let panUploadUrl = "";
let panDocument = '';
let isGstinUploaded = false;
let gstinInputData = new FormData();
let gstinUploadUrl = "";
let gstinDocument = '';


let _createCorpAcc = function (inputJson) {
  // let url = Urls.getOMSApiUrl().SIGNUP;
  // let headers = {
  //     "Content-Type": "application/json"
  // };
  // ServiceConnector.post(inputJson, url, headers).then(function(respData) {
  //     console.log(respData);
  //     $('.signup__form__wrpr').hide();
  //     $('.signup__form__success__wrpr').css('display', 'table-cell');
  // }, function(respErr) {
  //     $('#createAccount').removeClass('loading');
  //     respErr = JSON.parse(respErr.responseText);
  //     if (respErr.type == 'CorporateCreationException') {
  //         $('#emailID').addClass('input-error').siblings('.error-msg').html('Email already in use').show();
  //     } else {
  //         $('#signupError').html(respErr.message);
  //     }
  // });
  let headers = {
    "Content-Type": "application/json"
  };
  ServiceConnector.post(inputJson, '/signin-corp', headers).then(function (respData) {
    console.log(respData);
    $('.signup__form__wrpr').hide();
    $('.signup__form__success__wrpr').css('display', 'table-cell');
  }, function (respErr) {
    console.log('Error', respErr);
    grecaptcha.reset();
    $('#createAccount').removeClass('loading');
    respErr = JSON.parse(respErr.responseText);
    if (respErr.type == 'CorporateCreationException') {
      $('#emailID').addClass('input-error').siblings('.error-msg').html('Email already in use').show();
    } else {
      $('#signupError').html(respErr.message);
    }
  });
}

export
default {
  uploadFileDetails: {},

  createAccount: function () {

    let $fullName = $('#fullName');
    let $emailID = $('#emailID');
    let $companyName = $('#companyName');
    let $contactNo = $('#contactNo');
    let $noOfEmployee = $('#noOfEmployees');
    let $city = $('#city');
    let $address = $('#address');
    let $panNo = $('#panNo');
    let $cinNo = $('#cinNo');
    let $gstNo = $('#gstNo');
    let $pinCode = $('#pinCode');
    let $cinImageFile = $('#cinImageFile');
    let $panImageFile = $('#panImageFile');
    let $gstinImageFile = $('#gstinImageFile');
    let $captchaToken = $('#captchaToken');
    let $signUpTandC = $('#signUpTandC');

    let fullName = $fullName.val();
    let emailID = $emailID.val();
    let companyName = $companyName.val();
    let contactNo = $contactNo.val();
    let noOfEmployee = $noOfEmployee.val();
    let city = $city.val();
    // let a = location.split(',')
    // let city = a[0].trim();
    //let state = a[1].trim();
    // let country = a[2].trim();
    let address = $address.val();
    let panNo = $panNo.val();
    let cinNo = $cinNo.val();
    let gstNo = $gstNo.val();
    let pinCode = $pinCode.val();
    let cinImageFile = $cinImageFile.val();
    let panImageFile = $panImageFile.val();
    let gstinImageFile = $gstinImageFile.val();

    let captchaToken = $captchaToken.val();

    let isValid = true;
    let regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;


    if (fullName == '') {
      $fullName.addClass('input-error');
      $fullName.siblings('.error-msg').html('Please enter your name').show();
      isValid = false;
    }
    if (!Util.isEmail(emailID)) {
      $emailID.addClass('input-error');
      $emailID.siblings('.error-msg').html('Please enter valid email ID').show();
      isValid = false;
    }
    if (companyName == '') {
      $companyName.addClass('input-error');
      $companyName.siblings('.error-msg').html('Please enter company name').show();
      isValid = false;
    }
    if (contactNo.length != 10) {
      $contactNo.addClass('input-error');
      $contactNo.siblings('.error-msg').html('Please enter contact no.').show();
      isValid = false;
    }
    if (noOfEmployee == '0') {
      $noOfEmployee.addClass('input-error');
      $noOfEmployee.siblings('.error-msg').html('Please select no. of employees').show();
      isValid = false;
    }
    if (city == '') {
      $city.addClass('input-error');
      $city.siblings('.error-msg').html('Please enter city').show();
      isValid = false;
    }
    if (!regpan.test(panNo)) {
      $panNo.addClass('input-error');
      $panNo.siblings('.error-msg').html('Please enter valid PAN no.').show();
      isValid = false;
    }
    if (cinNo.length > 0 && cinNo.length !== 21) {
      $cinNo.addClass('input-error');
      $cinNo.siblings('.error-msg').html('Please enter valid CIN no.').show();
      isValid = false;
    }
    if(gstNo > 0 && gstNo.length !== 15){
      $gstNo.addClass('input-error');
      $gstNo.siblings('.error-msg').html('Please enter valid GSTN no.').show();
      isValid = false;
    }
    if (address == '') {
      $address.addClass('input-error');
      $address.siblings('.error-msg').html('Please enter address').show();
      isValid = false;
    }
    if (pinCode.length != 6) {
      $pinCode.addClass('input-error');
      $pinCode.siblings('.error-msg').html('Please enter valid pincode').show();
      isValid = false;
    }
    if (!$('#signUpTandC').is(":checked")) {
      $('.signUpTandC').children('.error-msg').html('Accept the terms and conditions').show();
      isValid = false;
    }

    /*if(cinImageFile == ''){
        $('#cinFilename').html('Upload CIN Certificate').addClass('error');
        isValid = false;
    }
    if(panImageFile == ''){
        $('#panFilename').html('Upload PAN Certificate').addClass('error');
        isValid = false;
    }*/
    if (isValid) {
      grecaptcha.execute();
    }
  },
  signUpSubmit: function () {

    let $fullName = $('#fullName');
    let $emailID = $('#emailID');
    let $companyName = $('#companyName');
    let $contactNo = $('#contactNo');
    let $noOfEmployee = $('#noOfEmployees');
    let $city = $('#city');
    let $address = $('#address');
    let $panNo = $('#panNo');
    let $cinNo = $('#cinNo');
    let $gstNo = $('#gstNo');
    let $pinCode = $('#pinCode');
    let $cinImageFile = $('#cinImageFile');
    let $panImageFile = $('#panImageFile');
    let $gstinImageFile = $('#gstinImageFile');
    let $captchaToken = $('#captchaToken');
    // let $signUpTandC = $('#signUpTandC');

    let fullName = $fullName.val();
    let emailID = $emailID.val();
    let companyName = $companyName.val();
    let contactNo = $contactNo.val();
    let noOfEmployee = $noOfEmployee.val();
    let city = $city.val();
    // let a = location.split(',')
    // let city = a[0].trim();
    //let state = a[1].trim();
    // let country = a[2].trim();
    let address = $address.val();
    let panNo = $panNo.val();
    let cinNo = $cinNo.val();
    let gstNo = $gstNo.val();
    let pinCode = $pinCode.val();
    let cinImageFile = $cinImageFile.val();
    let panImageFile = $panImageFile.val();
    let gstinImageFile = $gstinImageFile.val();

    let captchaToken = $captchaToken.val();



    $('#createAccount').addClass('loading');
    let inputJson = {
      "name": fullName,
      "email": emailID,
      "companyName": companyName,
      "employeeCount": noOfEmployee,
      "address": address,
      "city": city,
      "pincode": pinCode,
      "phoneNumber": contactNo,
      "pan": panNo,
      "cin": cinNo,
      "gstNumber": gstNo,
      "cinURL": cinDocument,
      "panURL": panDocument,
      "gstinURL": gstinDocument,
      "gRecaptchaResponse": captchaToken
      // "state":state,
      //"country":country
    };
    let headers = {
      "Content-Type": "application/json"
    };
    let cinFile = $('#cinImageFile')[0].files[0];
    let panFile = $('#panImageFile')[0].files[0];
    let gstinFile = $('#gstinImageFile')[0].files[0];
    console.log("signupDOMEvents -> cinFile", cinFile);
    console.log("signupDOMEvents -> panFile", panFile);
    if (cinFile != undefined) {
      if (cinFile.size > 2100000) {
        alert("CIN file size exceeded. Max size is 2MB");
        return;
      }
    }

    if (panFile != undefined) {
      if (panFile.size > 2100000) {
        alert("PAN file size exceeded. Max size is 2MB");
        return;
      }
    }
    if (gstinFile != undefined) {
      if (gstinFile.size > 2100000) {
        alert("PAN file size exceeded. Max size is 2MB");
        return;
      }
    }
    cinInputData.append('file', cinFile);
    panInputData.append('file', panFile);
    gstinInputData.append('file', gstinFile);
    if (!isCinUploaded && !isPanUploaded && !isGstinUploaded) {
      if (cinImageFile != '' && panImageFile != '' && gstinImageFile != '') {
        ServiceConnector.postWithCORS(cinInputData, cinUploadUrl, headers).then(function (cinFileUploadData) {
          let cinFileLocation = cinFileUploadData.childNodes[0].childNodes[0].textContent;
          let cinFileKey = cinFileUploadData.childNodes[0].childNodes[2].textContent;
          let cinFileEtag = cinFileUploadData.childNodes[0].childNodes[3].textContent.replace('"', '').replace('"', '');
          cinDocument = cinFileLocation + '?key=' + cinFileKey + '&etag=' + cinFileEtag;
          inputJson.cinURL = cinDocument;
          isCinUploaded = true;
          ServiceConnector.postWithCORS(panInputData, panUploadUrl, headers).then(function (panFileUploadData) {
            let panFileLocation = panFileUploadData.childNodes[0].childNodes[0].textContent;
            let panFileKey = panFileUploadData.childNodes[0].childNodes[2].textContent;
            let panFileEtag = panFileUploadData.childNodes[0].childNodes[3].textContent.replace('"', '').replace('"', '');
            panDocument = panFileLocation + '?key=' + panFileKey + '&etag=' + panFileEtag;
            inputJson.panURL = panDocument;
            isPanUploaded = true;
            // _createCorpAcc(inputJson);
            ServiceConnector.postWithCORS(gstinInputData, gstinUploadUrl, headers).then(function (gstinFileUploadData) {
              let gstinFileLocation = gstinFileUploadData.childNodes[0].childNodes[0].textContent;
              let gstinFileKey = gstinFileUploadData.childNodes[0].childNodes[2].textContent;
              let gstinFileEtag = gstinFileUploadData.childNodes[0].childNodes[3].textContent.replace('"', '').replace('"', '');
              gstinDocument = gstinFileLocation + '?key=' + gstinFileKey + '&etag=' + gstinFileEtag;
              inputJson.gstinURL = gstinDocument;
              isGstinUploaded = true;
              _createCorpAcc(inputJson);
            })
          }, function (fileUploadErr) {
            $('#createAccount').removeClass('loading');
          });
        }, function (fileUploadErr) {
          $('#createAccount').removeClass('loading');
        });
      } else {
        if (cinImageFile == '' && panImageFile == '' && gstinImageFile == '') {
          _createCorpAcc(inputJson);
        } else if (panImageFile != '' && cinImageFile != '') {
          ServiceConnector.postWithCORS(panInputData, panUploadUrl, headers).then(function (panFileUploadData) {
            let panFileLocation = panFileUploadData.childNodes[0].childNodes[0].textContent;
            let panFileKey = panFileUploadData.childNodes[0].childNodes[2].textContent;
            let panFileEtag = panFileUploadData.childNodes[0].childNodes[3].textContent.replace('"', '').replace('"', '');
            panDocument = panFileLocation + '?key=' + panFileKey + '&etag=' + panFileEtag;
            inputJson.panURL = panDocument;
            isPanUploaded = true;
            // _createCorpAcc(inputJson);

            ServiceConnector.postWithCORS(cinInputData, cinUploadUrl, headers).then(function (cinFileUploadData) {
              let cinFileLocation = cinFileUploadData.childNodes[0].childNodes[0].textContent;
              let cinFileKey = cinFileUploadData.childNodes[0].childNodes[2].textContent;
              let cinFileEtag = cinFileUploadData.childNodes[0].childNodes[3].textContent.replace('"', '').replace('"', '');
              cinDocument = cinFileLocation + '?key=' + cinFileKey + '&etag=' + cinFileEtag;
              inputJson.cinURL = cinDocument;
              isCinUploaded = true;
              _createCorpAcc(inputJson);
            }, function (fileUploadErr) {
              $('#createAccount').removeClass('loading');
            });
          }, function (fileUploadErr) {
            $('#createAccount').removeClass('loading');
          });
        } else if (panImageFile != '' && gstinImageFile != '') {
          ServiceConnector.postWithCORS(panInputData, panUploadUrl, headers).then(function (panFileUploadData) {
            let panFileLocation = panFileUploadData.childNodes[0].childNodes[0].textContent;
            let panFileKey = panFileUploadData.childNodes[0].childNodes[2].textContent;
            let panFileEtag = panFileUploadData.childNodes[0].childNodes[3].textContent.replace('"', '').replace('"', '');
            panDocument = panFileLocation + '?key=' + panFileKey + '&etag=' + panFileEtag;
            inputJson.panURL = panDocument;
            isPanUploaded = true;
            // _createCorpAcc(inputJson);

            ServiceConnector.postWithCORS(gstinInputData, gstinUploadUrl, headers).then(function (gstinFileUploadData) {
              let gstinFileLocation = gstinFileUploadData.childNodes[0].childNodes[0].textContent;
              let gstinFileKey = gstinFileUploadData.childNodes[0].childNodes[2].textContent;
              let gstinFileEtag = gstinFileUploadData.childNodes[0].childNodes[3].textContent.replace('"', '').replace('"', '');
              gstinDocument = gstinFileLocation + '?key=' + gstinFileKey + '&etag=' + gstinFileEtag;
              inputJson.gstinURL = gstinDocument;
              isGstinUploaded = true;
              _createCorpAcc(inputJson);
            }, function (fileUploadErr) {
              $('#createAccount').removeClass('loading');
            });
          }, function (fileUploadErr) {
            $('#createAccount').removeClass('loading');
          });
        } else if (cinImageFile != '' && gstinImageFile != '') {
          ServiceConnector.postWithCORS(cinInputData, cinUploadUrl, headers).then(function (cinFileUploadData) {
            let cinFileLocation = cinFileUploadData.childNodes[0].childNodes[0].textContent;
            let cinFileKey = cinFileUploadData.childNodes[0].childNodes[2].textContent;
            let cinFileEtag = cinFileUploadData.childNodes[0].childNodes[3].textContent.replace('"', '').replace('"', '');
            cinDocument = cinFileLocation + '?key=' + cinFileKey + '&etag=' + cinFileEtag;
            inputJson.cinURL = cinDocument;
            isCinUploaded = true;

            // _createCorpAcc(inputJson);

            ServiceConnector.postWithCORS(gstinInputData, gstinUploadUrl, headers).then(function (gstinFileUploadData) {
              let gstinFileLocation = gstinFileUploadData.childNodes[0].childNodes[0].textContent;
              let gstinFileKey = gstinFileUploadData.childNodes[0].childNodes[2].textContent;
              let gstinFileEtag = gstinFileUploadData.childNodes[0].childNodes[3].textContent.replace('"', '').replace('"', '');
              gstinDocument = gstinFileLocation + '?key=' + gstinFileKey + '&etag=' + gstinFileEtag;
              inputJson.gstinURL = gstinDocument;
              isGstinUploaded = true;
              _createCorpAcc(inputJson);
            }, function (fileUploadErr) {
              $('#createAccount').removeClass('loading');
            });
          }, function (fileUploadErr) {
            $('#createAccount').removeClass('loading');
          });
        } else if (panImageFile != '') {
          ServiceConnector.postWithCORS(panInputData, panUploadUrl, headers).then(function (panFileUploadData) {
            let panFileLocation = panFileUploadData.childNodes[0].childNodes[0].textContent;
            let panFileKey = panFileUploadData.childNodes[0].childNodes[2].textContent;
            let panFileEtag = panFileUploadData.childNodes[0].childNodes[3].textContent.replace('"', '').replace('"', '');
            panDocument = panFileLocation + '?key=' + panFileKey + '&etag=' + panFileEtag;
            inputJson.panURL = panDocument;
            isPanUploaded = true;
            _createCorpAcc(inputJson);
          }, function (fileUploadErr) {
            $('#createAccount').removeClass('loading');
          });
        } else if (cinImageFile != '') {
          ServiceConnector.postWithCORS(cinInputData, cinUploadUrl, headers).then(function (cinFileUploadData) {
            let cinFileLocation = cinFileUploadData.childNodes[0].childNodes[0].textContent;
            let cinFileKey = cinFileUploadData.childNodes[0].childNodes[2].textContent;
            let cinFileEtag = cinFileUploadData.childNodes[0].childNodes[3].textContent.replace('"', '').replace('"', '');
            cinDocument = cinFileLocation + '?key=' + cinFileKey + '&etag=' + cinFileEtag;
            inputJson.cinURL = cinDocument;
            isCinUploaded = true;
            _createCorpAcc(inputJson);
          }, function (fileUploadErr) {
            $('#createAccount').removeClass('loading');
          });
        } else if (gstinImageFile != '') {
          ServiceConnector.postWithCORS(gstinInputData, gstinUploadUrl, headers).then(function (gstinFileUploadData) {
            let gstinFileLocation = gstinFileUploadData.childNodes[0].childNodes[0].textContent;
            let gstinFileKey = gstinFileUploadData.childNodes[0].childNodes[2].textContent;
            let gstinFileEtag = gstinFileUploadData.childNodes[0].childNodes[3].textContent.replace('"', '').replace('"', '');
            gstinDocument = gstinFileLocation + '?key=' + gstinFileKey + '&etag=' + gstinFileEtag;
            inputJson.gstinURL = gstinDocument;
            isGstinUploaded = true;
            _createCorpAcc(inputJson);
          }, function (fileUploadErr) {
            $('#createAccount').removeClass('loading');
          });
        } else {
          _createCorpAcc(inputJson);
        }
      }
    } else if (!isPanUploaded) {
      if (panImageFile != '') {
        ServiceConnector.postWithCORS(panInputData, panUploadUrl, headers).then(function (panFileUploadData) {
          let panFileLocation = panFileUploadData.childNodes[0].childNodes[0].textContent;
          let panFileKey = panFileUploadData.childNodes[0].childNodes[2].textContent;
          let panFileEtag = panFileUploadData.childNodes[0].childNodes[3].textContent.replace('"', '');
          isPanUploaded = true;
          _createCorpAcc(inputJson);
        }, function (fileUploadErr) {
          $('#createAccount').removeClass('loading');
        });
      } else {
        _createCorpAcc(inputJson);
      }
    } else if (!isCinUploaded) {
      if (cinImageFile != '') {
        ServiceConnector.postWithCORS(cinInputData, cinUploadUrl, headers).then(function (cinFileUploadData) {
          let cinFileLocation = cinFileUploadData.childNodes[0].childNodes[0].textContent;
          let cinFileKey = cinFileUploadData.childNodes[0].childNodes[2].textContent;
          let cinFileEtag = cinFileUploadData.childNodes[0].childNodes[3].textContent.replace('"', '').replace('"', '');
          cinDocument = cinFileLocation + '?key=' + cinFileKey + '&etag=' + cinFileEtag;
          inputJson.cinURL = cinDocument;
          isCinUploaded = true;
          _createCorpAcc(inputJson);
        }, function (fileUploadErr) {
          $('#createAccount').removeClass('loading');
        });
      } else {
        _createCorpAcc(inputJson);
      }
    } else if (!isGstinUploaded) {
      if (gstinImageFile != '') {
        ServiceConnector.postWithCORS(gstinInputData, cinUploadUrl, headers).then(function (gstinFileUploadData) {
          let gstinFileLocation = gstinFileUploadData.childNodes[0].childNodes[0].textContent;
          let gstinFileKey = gstinFileUploadData.childNodes[0].childNodes[2].textContent;
          let gstinFileEtag = gstinFileUploadData.childNodes[0].childNodes[3].textContent.replace('"', '').replace('"', '');
          gstinDocument = gstinFileLocation + '?key=' + gstinFileKey + '&etag=' + gstinFileEtag;
          inputJson.gstinURL = gstinDocument;
          isGstinUploaded = true;
          _createCorpAcc(inputJson);
        }, function (fileUploadErr) {
          $('#createAccount').removeClass('loading');
        });
      } else {
        _createCorpAcc(inputJson);
      }
    } else {
      _createCorpAcc(inputJson);
    }
  },
  inputChange: function (e) {
    if ($(this).val() == '') {
      $(this).addClass('input-error');
      $(this).siblings('.error-msg').show();
    } else {
      $(this).removeClass('input-error');
      $(this).siblings('.error-msg').hide();
    }
    if (e.keyCode == 13) {
      $('#createAccount').trigger('click');
    }
  },

  panNoChange: function (e) {
    let regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
    if (!regpan.test($(this).val())) {
      $(this).addClass('input-error');
      $(this).siblings('.error-msg').show();
    } else {
      $(this).removeClass('input-error');
      $(this).siblings('.error-msg').hide();
    }
    if (e.keyCode == 13) {
      $('#createAccount').trigger('click');
    }
  },
  codeChange: function () {
    $('#errMsg').hide();
    if ($(this).val().length < 4) {
      $(this).addClass('input-error');
    } else {
      $(this).removeClass('input-error');
    }
  },
  verifyEmail: function () {
    let code = $('#code').val();
    if (code.length < 4) {
      $('#code').addClass('input-error');
    } else {
      $(this).addClass('loading');
      $('#errMsg').hide();
      let inputJson = {
        "token": Util.getUrlParameter('token')
      };
      let url = Urls.getCorpApiUrl().OTP_VERIFY + '/' + code;
      let headers = {
        "Content-Type": "application/json"
      };
      ServiceConnector.post(inputJson, url, headers).then(function (respData) {
        window.location = '/email';
      }, function (respErr) {
        console.log(respErr);
        $('#errMsg').html(JSON.parse(respErr.responseText).error).show();
        $('#emailVerifyBtn').removeClass('loading');
      });
    }
  },
  resendCode: function () {
    let inputJson = {
      "email": encodeURI(Util.getUrlParameter('emailId'))
    };
    let url = Urls.getCorpApiUrl().RESEND_EMAIL;
    let headers = {
      "Content-Type": "application/json"
    };
    ServiceConnector.post(inputJson, url, headers).then(function (respData) {
      console.log(respData);
    }, function (respErr) {
      console.log(respErr);
    });
  },
  acceptTandC: function () {
    if ($('#signUpTandC').is(":checked")) {
      $('.signUpTandC').children('.error-msg').html('');
    }
  },
  noOfEmployeesChange: function () {
    if ($(this).val() == '') {
      $(this).addClass('input-error');
      $(this).siblings('.error-msg').show();
    } else {
      $(this).removeClass('input-error');
      $(this).siblings('.error-msg').hide();
    }
  },
  uploadCINFile: function () {
    let file = $(this)[0].files[0];
    if (file) {
      let that = this;
      $('#cinFilename').html(file.name);
      if (!cinInputData.has('acl')) {
        SignupSvc.getAssetUploadEndpoint().then(function (respData) {
          cinUploadUrl = respData.postAction;
          cinInputData.append('x-amz-algorithm', respData.xAmzAlgorithm);
          cinInputData.append('x-amz-signature', respData.xAmzSignature);
          cinInputData.append('acl', respData.acl);
          cinInputData.append('success_action_status', respData.successActionStatus);
          cinInputData.append('key', respData.key);
          cinInputData.append('policy', respData.policy);
          cinInputData.append('x-amz-date', respData.xAmzDate);
          cinInputData.append('x-amz-credential', respData.xAmzCredential);
          cinInputData.append('content-type', 'application/image');
          if (file) {
            $('#cinFilename').html(file.name).removeClass('error');
          }
        }, function (respErr) {

        });
      } else {
        if (file) {
          $('#cinFilename').html(file.name).removeClass('error');
        }
      }
    }
  },
  uploadPANFile: function () {
    let file = $(this)[0].files[0];
    if (file) {
      let that = this;
      $('#panFilename').html(file.name);
      if (!panInputData.has('acl')) {
        SignupSvc.getAssetUploadEndpoint().then(function (respData) {
          panUploadUrl = respData.postAction;
          panInputData.append('x-amz-algorithm', respData.xAmzAlgorithm);
          panInputData.append('x-amz-signature', respData.xAmzSignature);
          panInputData.append('acl', respData.acl);
          panInputData.append('success_action_status', respData.successActionStatus);
          panInputData.append('key', respData.key);
          panInputData.append('policy', respData.policy);
          panInputData.append('x-amz-date', respData.xAmzDate);
          panInputData.append('x-amz-credential', respData.xAmzCredential);
          panInputData.append('content-type', 'application/image');
          if (file) {
            $('#panFilename').html(file.name).removeClass('error');
          }
        }, function (respErr) {

        });
      } else {
        if (file) {
          $('#panFilename').html(file.name).removeClass('error');
        }
      }
    }
  },
  uploadGSTINFile: function () {
    let file = $(this)[0].files[0];
    if (file) {
      let that = this;
      $('#gstinFileName').html(file.name);
      if (!gstinInputData.has('acl')) {
        SignupSvc.getAssetUploadEndpoint().then(function (respData) {
          gstinUploadUrl = respData.postAction;
          gstinInputData.append('x-amz-algorithm', respData.xAmzAlgorithm);
          gstinInputData.append('x-amz-signature', respData.xAmzSignature);
          gstinInputData.append('acl', respData.acl);
          gstinInputData.append('success_action_status', respData.successActionStatus);
          gstinInputData.append('key', respData.key);
          gstinInputData.append('policy', respData.policy);
          gstinInputData.append('x-amz-date', respData.xAmzDate);
          gstinInputData.append('x-amz-credential', respData.xAmzCredential);
          gstinInputData.append('content-type', 'application/image');
          if (file) {
            $('#gstinFileName').html(file.name).removeClass('error');
          }
        }, function (respErr) {

        });
      } else {
        if (file) {
          $('#gstinFileName').html(file.name).removeClass('error');
        }
      }
    }
  }
}

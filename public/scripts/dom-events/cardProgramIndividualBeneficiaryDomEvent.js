import DomEventHandler from '../common/domEventHandler.js';
import Table from '../widgets/table';
import ExpressTable from '../widgets/expressTable';
import ScheduledTable from '../../template/section/benefit-details/individualBeneficiaryDetails/tables/scheduleTable.ejs';
import cardProgramIndividualBeneficiarySvc from '../service/cardProgramIndividualBeneficiarySvc.js';
import DocumentCardTemplate from "../../template/section/benefit-details/individualBeneficiaryDetails/singleCardDetails.ejs";
import BlankCardTemplate from "../../template/section/benefit-details/individualBeneficiaryDetails/blankCard.ejs";
import TransferOrderHistory from "../../template/Modal/tranfer-order-history.ejs";
import Util from "../common/util";
import OrderSvc from "../service/ordersService";
import Constants from "../common/constants";
import CacheDataStorage from '../common/cacheDataStorage';


let cardProgramIndividualBeneficiary = function() {
    if($(this).data('read')){
        return false;
    }
    Table.init();

    let name = $(this).data("beneficiary-name");
    let company = $(this).data("beneficiary-companyname");
    let profilePic = $(this).data("beneficiary-dp");
    let employeeId = $(this).data("beneficiary-id");
    let companyId = $(this).data("beneficiary-companyid");
    let email = $(this).data('beneficiary-email')
    let programId = $("#transferId").val(); // getting program id from page hidden field
    let phone = $(this).data('phone');
    if ($("#all-cards-areaId .custom-table")[0]) {
        // $(".employee-report-table").table("destroy");
        $(".employee-report-table").find("custom-table").remove();
    } else {
        Table.init();
    }

    sessionStorage.setItem('d-beneficiary-name', name);
    sessionStorage.setItem('d-company-name', company);
    sessionStorage.setItem('d-zetauserid', $(this).data("zetauserid"));
    sessionStorage.setItem('d-beneficiary-email', email);
    sessionStorage.setItem('d-cardstatus', $(this).data("cardstatus"));
    sessionStorage.setItem('d-beneficiary-phone', phone);
    // schedule and waiting table setup all-cards-areaId
    // if($("#scheduled-amount-table .custom-table")[0]) {
    //     $("#scheduled-amount-table").table("destroy");
    // }else{
    //     Table.init();
    // }
    // making empty of schedule table area
    if ($("#waiting-amount-table .custom-table")[0]) {
        $("#waiting-amount-table").table("destroy");
    } else {
        Table.init();
    }
    // making empty of waiting table area
    let text = $("#schedule-table-id").html();
    if (text === 'Hide Details') {
        $("#scheduled-amount-table").hide();
        $("#schedule-table-id").html('View Details');
    }
    let text1 = $("#waiting-table-id").html();
    if (text1 === 'Hide Details') {
        $("#waiting-amount-table").hide();
        $("#waiting-table-id").html('View Details');
    }
    let text3 = $("#canceled-table-id").html();
    if (text3 === 'Hide Details') {
        $("#canceled-amount-table").hide();
        $("#canceled-table-id").html('View Details');
    }
    // end of table setup
    let url = '/beneficiaries/' + employeeId + '?companyID=' + companyId;
    $('#all-cards-areaId').html('');
    $("#selected-employee-id").val(employeeId);
    $("#selected-company-id").val(companyId);
    $('#beneficiary-Name').text(name);
    $('#beneficiary-companyName').text(company);
    $("#beneficiary-pic").attr("src", profilePic);
    $("#profileUrl").attr('href', url);
    $("#no-of-cardIssued").html('');
    $("#schedule-amount-id").hide();
    $("#waiting-amount-id").hide();
    $("#canceled-amount-id").hide();

    // getting scheduled amount
    let commonParam = {
        productID: programId,
        employeeID: employeeId
    };
    let scheduledCount = 0;
    let scheduledAmount = 0;
    let waitingCount = 0;
    let waitingAmount = 0;
    let cancelledCount = 0;
    let cancelledAmount = 0;
    let currency = '';
    // let cards = [];
    // let benefits = [];
    let empCardDetails = {};
    let singleCardDetails = {};
    let req1 = cardProgramIndividualBeneficiarySvc.getPayoutStats(commonParam);
    $.when(req1).done(function(respData) {
        console.log(respData);
        if (respData.payoutStatsByStatus.payoutStatsByStatus.scheduled) { //schedule-amount-id

            if (respData.payoutStatsByStatus.payoutStatsByStatus.scheduled.payoutsCount) {
                scheduledCount = respData.payoutStatsByStatus.payoutStatsByStatus.scheduled.payoutsCount;
            }
            if (respData.payoutStatsByStatus.payoutStatsByStatus.scheduled.totalAmount) {
                scheduledAmount = respData.payoutStatsByStatus.payoutStatsByStatus.scheduled.totalAmount.amount;
                currency = respData.payoutStatsByStatus.payoutStatsByStatus.scheduled.totalAmount.currency;
                scheduledAmount = Utils.formatCurrency(scheduledAmount / 100, currency);
            }
            $("#schedule-amount-id").show();
        }
        if (respData.payoutStatsByStatus.payoutStatsByStatus.temporarily_failed) {
            if (respData.payoutStatsByStatus.payoutStatsByStatus.temporarily_failed.payoutsCount) {
                waitingCount = respData.payoutStatsByStatus.payoutStatsByStatus.temporarily_failed.payoutsCount;
            }
            if (respData.payoutStatsByStatus.payoutStatsByStatus.temporarily_failed.totalAmount) {
                waitingAmount = respData.payoutStatsByStatus.payoutStatsByStatus.temporarily_failed.totalAmount.amount;
                currency = respData.payoutStatsByStatus.payoutStatsByStatus.temporarily_failed.totalAmount.currency;
                waitingAmount = Utils.formatCurrency(waitingAmount / 100, currency);
            }
            $("#waiting-amount-id").show();
        }
        // if( (!respData.payoutStatsByStatus.payoutStatsByStatus.scheduled) &&  (!respData.payoutStatsByStatus.payoutStatsByStatus.temporarily_failed)){
        //     $("#canceled-amount-id").show();
        // }
        if (respData.payoutStatsByStatus.payoutStatsByStatus) {
            if (typeof respData.payoutStatsByStatus.payoutStatsByStatus.scheduled === "undefined" && typeof respData.payoutStatsByStatus.payoutStatsByStatus.temporarily_failed === "undefined") {
                if (respData.payoutStatsByStatus.payoutStatsByStatus.cancelled) {
                    if (respData.payoutStatsByStatus.payoutStatsByStatus.cancelled.payoutsCount) {
                        cancelledCount = respData.payoutStatsByStatus.payoutStatsByStatus.cancelled.payoutsCount;
                    }
                    if (respData.payoutStatsByStatus.payoutStatsByStatus.cancelled.totalAmount) {
                        cancelledAmount = respData.payoutStatsByStatus.payoutStatsByStatus.cancelled.totalAmount.amount;
                        currency = respData.payoutStatsByStatus.payoutStatsByStatus.cancelled.totalAmount.currency;
                        cancelledAmount = Utils.formatCurrency(cancelledAmount / 100, currency);
                    }
                    $("#canceled-amount-id").show();
                }
            }
        }
        $("#scheduled-amount").text(scheduledAmount);
        $("#scheduled-count").text(scheduledCount);
        $("#waiting-amount").text(waitingAmount);
        $("#waiting-count").text(waitingCount);
        $("#canceled-amount").text(cancelledAmount);
        $("#canceled-count").text(cancelledCount);
    });
    cardProgramIndividualBeneficiarySvc.getBenefitIndividualEmployee(commonParam).then(function(empCardDetails) {
        empCardDetails = empCardDetails.employeeWithCardDetails;
        let dataArray = [];
        // singleCardDetails.cards = empCardDetails[0].cards;
        // singleCardDetails.benefits = empCardDetails[0].benefits;
        // singleCardDetails.companyName = empCardDetails[0].companyName;
        if (empCardDetails) { //no-of-cardIssued
            if (empCardDetails.length > 0) {
                if (empCardDetails.length === 1) {
                    $("#no-of-cardIssued").html(empCardDetails[0].benefits.length + ' Card Issued');
                } else {
                    $("#no-of-cardIssued").html(empCardDetails[0].benefits.length + ' Cards Issued');
                }

            }
            if (empCardDetails[0].benefits.length) {
                for (let i = 0; i < empCardDetails[0].benefits.length; i++) {
                    singleCardDetails.companyName = empCardDetails[0].companyName;
                    singleCardDetails.benefits = empCardDetails[0].benefits[i];
                    singleCardDetails.cards = empCardDetails[0].cards[i];
                    console.log('single', singleCardDetails);
                    dataArray.push(singleCardDetails);
                    singleCardDetails = {};
                    //  DomEventHandler.appendToMyTemplate("all-cards-areaId", DocumentCardTemplate, singleCardDetails);

                }
                DomEventHandler.renderTemplateArray("all-cards-areaId", DocumentCardTemplate, dataArray);
            }
        } else {
            DomEventHandler.renderMyTemplate("all-cards-areaId", BlankCardTemplate);
        }
    });

    $('#cardProgramIndividualBeneficiary').modal('show');

};

let loadTableScheduled = function(empId, searchFilter) {
    let transactionTable = new ExpressTable('#scheduled-amount-table', {
        tableTemplate: ScheduledTable,
        source: cardProgramIndividualBeneficiarySvc.getEmployeePayoutsScheduled,
        pageSize: 5,
        pageNumber: 1,
        onloadStarted: function() {
            // $("#uniqueTransactionsChartLoading").show();
        },
        onLoadCompleted: (respData) => {
            // $("#uniqueTransactionsChartLoading").hide();
            // this.loadChart(respData);
        }
    }, {
        employeeID: empId,
        statusFilters: searchFilter
            // search: ''
    });

    // this.transactionTable.triggerRefresh({
    //     search 'ab'
    // })


};
let loadTableWaiting = function(empId, searchFilter) {
    let transactionTable = new ExpressTable('#waiting-amount-table', {
        tableTemplate: ScheduledTable,
        source: cardProgramIndividualBeneficiarySvc.getEmployeePayoutsWaiting,
        pageSize: 5,
        pageNumber: 1,
        onloadStarted: function() {},
        onLoadCompleted: (respData) => {}
    }, {
        employeeID: empId,
        statusFilters: searchFilter
    });
};
let loadTableCanceled = function(empId, searchFilter) {
    let transactionTable = new ExpressTable('#canceled-amount-table', {
        tableTemplate: ScheduledTable,
        source: cardProgramIndividualBeneficiarySvc.getEmployeePayoutsCanceled,
        pageSize: 5,
        pageNumber: 1,
        onloadStarted: function() {},
        onLoadCompleted: (respData) => {}
    }, {
        employeeID: empId,
        statusFilters: searchFilter
    });
};

let showHideScheduledTable = function() {
    Table.init();
    let text = $("#schedule-table-id").html();
    if (text === 'View Details') {
        $("#scheduled-amount-table").slideDown(600);
        $("#schedule-table-id").html('Hide Details');
        let empId = $("#selected-employee-id").val();
        loadTableScheduled(empId, 'scheduled');
    } else {
        $("#scheduled-amount-table").slideUp(600);
        $("#schedule-table-id").html('View Details');
    }
};
let showHideWaitingTable = function() {
    Table.init();
    let text = $("#waiting-table-id").html();
    if (text === 'View Details') {
        $("#waiting-amount-table").slideDown(600);
        $("#waiting-table-id").html('Hide Details');
        let empId = $("#selected-employee-id").val();
        loadTableWaiting(empId, 'temporarily_failed');
    } else {
        $("#waiting-amount-table").slideUp(600);
        $("#waiting-table-id").html('View Details');
    }
};
let showHideCanceledTable = function() {
    Table.init();
    let text = $("#canceled-table-id").html();
    if (text === 'View Details') {
        $("#canceled-amount-table").slideDown(600);
        $("#canceled-table-id").html('Hide Details');
        let empId = $("#selected-employee-id").val();
        loadTableCanceled(empId, 'cancelled');
    } else {
        $("#canceled-amount-table").slideUp(600);
        $("#canceled-table-id").html('View Details');
    }
};
let showHideCardTable = function(cardId, text) {
    Table.init();
    // let cardId = $(this).data("card-id"); //cardTransferReportTable-5809588924436242000
    // let text =$(this).text();
    // alert(cardId);
    
    let tableTemplate = '/template/section/benefit-details/individualBeneficiaryDetails/tables/common-transferReport.ejs'
    let rowTemplate = '/template/section/benefit-details/individualBeneficiaryDetails/tables/common-transferReport-row.ejs' 
    const country_configs = CacheDataStorage.getItem(Constants.COUNTRY_CONFIGS);
    if (country_configs) {
        const tablewidgetData = _.get(country_configs, 'cardProgramIndividualBeneficiaryDomEvent.showHideCardTable');
        if (tablewidgetData && tablewidgetData.tableTemplate && tablewidgetData.rowTemplate) {
            tableTemplate = tablewidgetData.tableTemplate;
            rowTemplate = tablewidgetData.rowTemplate;
        }
    }
    if (text === 'View Details') {
        $("#cardDetailsHead-" + cardId).slideDown(600);
        $("#cardDetailsLink-" + cardId).html('Hide Details');
        $("#cardTransferReportTable-" + cardId).table({
            source: cardProgramIndividualBeneficiarySvc.getIndividualEmployeeCardPayout,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: tableTemplate,
            rowTemplate: rowTemplate,
            extraParam: {
                employeeID: $("#selected-employee-id").val(),
                cardID: cardId
            },
        });
    } else {
        $("#cardDetailsHead-" + cardId).slideUp(600);
        $("#cardDetailsLink-" + cardId).html('View Details');
    }
    DomEventHandler.bindClassEvent('employee-report-table', 'click', '.view-history', showOrderHistory);
};

let showRevokeModal = function(cardId, text) {
    $("#revokePayoutConfirm").modal("hide");
};
let revokePayout = function(e) {
    let payoutId = $(this).data("payout-id"),
        orderId = $(this).data("order-id"),
        revokePayoutLoader = $("#revokePayoutLoader"),
        revokePayoutSuccess = $("#revokePayoutSuccess"),
        revokePayoutError = $("#revokePayoutError");
    $("#revokePayoutConfirm").modal("hide");
    revokePayoutLoader.modal("show");
    $("#error-msg").hide();
    $("#cannot-revoke-msg").hide();
    TransferDetailCtrl.revokePayout(orderId, payoutId).then(function(respData) {
        revokePayoutLoader.modal("hide");
        if (respData.status == 200 || respData.success) {
            revokePayoutSuccess.modal("show");
        } else {
            revokePayoutError.modal("show");
            $("#cannot-revoke-msg").show();
        }
    }, function(respErr) {
        revokePayoutLoader.modal("hide");
        if (respErr.status == 200 || respErr.success) {
            revokePayoutSuccess.modal("show");
        } else {
            console.log("genie msg outside");
            if(Util.checkGenie()) {
                revokePayoutError.modal("show");
                $("#error-msg").show();
                console.log("genie msg");
                $("#error-msg").text("You do not have access to initiate transfer.");
            } else {
                revokePayoutError.modal("show");
                $("#error-msg").show();
                console.log("genie msg normal");
            }
        }
    });
};

let closeModal = function() {
    $('#cardProgramIndividualBeneficiary').modal('hide');
};

function fetchHistoryData(params) {
    return OrderSvc.getOrderHistory(params.orderId, "order")
    .then(response => {
        let finalData = OrderSvc.getFormattedOrderDetails(response.auditDataList);
        if(finalData.records && finalData.records.length > 0) {
            $('.updated-by-name').text(finalData.records[0].name);
        } else {
            $('.latest-order-update').hide();
        }
        return finalData;
    })
}
let showOrderHistory = function(event) {
    DomEventHandler.renderMyTemplate('showOrderHistoryListModal', TransferOrderHistory, {});
    $('#showOrderHistoryListModal').modal('show');
    const orderId = $(this).closest('tr').data('orderid');
    $("#orderHistory").table({
        source: fetchHistoryData,
        tableTemplate: "/template/tables/transfer-order-history.ejs",
        rowTemplate: "/template/tables/transfer-order-history-row.ejs",
        extraParam: {
            orderId: orderId
        },
    });
};

export default {
    cardProgramIndividualBeneficiary: cardProgramIndividualBeneficiary,
    showHideScheduledTable: showHideScheduledTable,
    showHideWaitingTable: showHideWaitingTable,
    showHideCanceledTable: showHideCanceledTable,
    showHideCardTable: showHideCardTable,
    showRevokeModal: showRevokeModal,
    closeModal: closeModal,
    showOrderHistory: showOrderHistory
}
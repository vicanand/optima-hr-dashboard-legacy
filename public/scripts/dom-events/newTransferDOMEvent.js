import NewTransferSvc from '../service/newTransferSvc';
import Storage from '../common/webStorage';
import Constant from '../common/constants';
import NewTransferCtrl from '../controller/newTransferCtrl.js';
import Meta from '../common/metaStorage';
import Util from "../common/util";
import moment from '../../lib/moment/moment';
import domEventHandler from '../common/domEventHandler';
import fileErrorTemplate from '../../template/section/benefit-details/fileErrorTemplate.ejs'
import BulkCloseSvc from '../service/bulkCloseSvc';
import TableWidget from '../widgets/table-widget';

// Table.init();
TableWidget.init();
let initiateTransfer = function () {
  var uploadDetails = $('#uploadDetails').val();

  // CODE ADDED BY PARYUL JAIN WITH REF. TO TASK B)REPEAT ORDERS
  // START
  var orderType = $('#repeatAction').val(),
      validTill = $('#repeatUntilDate').val(),
      orderRepetition = $('#frequencyTypeOptions').val(),
      orderRepetitionFrequency = $('#frequencyTypeOptions').val(),
      month = $('#payPeriodMonth').val();
  // payoutCycleStartDate = $('#payrollCycleStartDate').val(),
  // payoutCycleEndDate = $('#payrollCycleEndDate').val();
  let purchaseOrderNo = $('#purchaseOrderNo').val()
  let scheduledDate = $('#scheduleTransferDate').val()
  let inputObj = {
    fileName: Storage.get(Constant.STORAGE_FILE_NAME),
    //poNumber: $('#purchaseOrderNo').val(),
    companyID: $("meta[name='companyID']").attr("content"),
    corpID: $("meta[name='corpID']").attr("content"),
    type: $("#currentProductType").val(), // this is available in res.locals.currProgram take it from there
    key: JSON.parse(uploadDetails).key,
    //allowedMerchants: [],
    programID: $('#programId').val(),
    location: Storage.get(Constant.STORAGE_FILE_URL),
    poNumber: $("#purchaseOrderNo").val(),

    // CODE ADDED BY PARYUL JAIN WITH REF. TO TASK B)REPEAT ORDERS
    // START
    orderType: orderType,
    month: month
    // payoutCycleStartDate: payoutCycleStartDate,
    // payoutCycleEndDate: payoutCycleEndDate,

  };

  if (orderType == "recurring") {
    inputObj.orderRepetitionFrequency = orderRepetitionFrequency;
    inputObj.validTill = validTill;
    // numberOfOrders = NewTransferCtrl.calculateNumberOfOrders(payoutCycleStartDate, validTill, orderRepetition);
  }
  else {
    $('#create-order').removeAttr('disabled');
    $('#create-order').removeClass('btn-disabled');
    inputObj.validTill = validTill;
    inputObj.orderRepetition = orderRepetition;
    //  numberOfOrders = NewTransferCtrl.calculateNumberOfOrders(payoutCycleStartDate, validTill, orderRepetition);
  }

  if ($("#currentProductType").val() !== 'gift' && $("#currentProductType").val() !== 'reward') {
    inputObj.payoutDate = $('#scheduleTransferDate').val() + " 12:00:00";;
  }

  $("#myModalOops").modal("hide");
  $("#createOrderModalOops").modal("hide");
  $("#myModalLoader").modal("show");
  createOrderV1(inputObj)
};

let createOrderV2 = function(inputObj){
  NewTransferSvc.createOrderV2(inputObj).then(function (respData) {
    $("#myModalLoader").modal("hide");
    if(respData.fileProcessingSummary){
      Storage.set('orderId', respData.orderID)
      Storage.set('noOfBeneficiary', respData.fileProcessingSummary.validRows)
      Storage.set('totalAmount', respData.fileProcessingSummary.amountOfValidRows)
      Storage.set(Constant.STORAGE_INVALID_BULK_ENTRIES, respData.fileProcessingSummary.invalidRows)
      if(respData.fileProcessingSummary.isDuplicateFile){
        $('#myModalValidationPopup').modal("show");
      }else if(respData.fileProcessingSummary.invalidRows > 0){
        $('#fileUploadContent').hide();
        renderFileError(respData.fileProcessingSummary, respData.orderID)
      }else{
        continueTransfer();
      }
    }
  }, function (respErr) {
    // initGAEvent('event', 'Zeta Optima', 'click', 'Create Transfer Order, corpId=' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', programId=' + programId + ', programName=' + programName + ', scheduledDate=' + scheduledDate + ', fileType=' + fileType + ', purchaseOrderNo=' + purchaseOrderNo + ', timestamp=' + new Date().toLocaleString() + ', Failed, Reason=' + JSON.stringify(respErr));
    $("#myModalLoader").modal("hide");
    $("#myModalOops").modal("show");
    if ((Util.checkGenie()) && (respErr.responseJSON.message == "Unauthorized")) {
      $("#myModalOops .heading-sub-text").text("You do not have access to initiate transfer.");
    } 
    if (respErr.responseJSON && respErr.responseJSON.type === "FileHeadersIncorrectException") {
      $("#myModalOops").modal("hide");
      $('#fileHeaderError').text('Invalid or missing headers');
      $('#fileHeaderErrorDesc').text('Please check the file for missing headers or spelling mistakes. These are expected headers:')
      let errorMessage = respErr.responseJSON.message.slice(52).replace(/([A-Z])/g, ' $1').trim().split(',');
      let mainOrderList = '<ol id="onlyHeaderList" align="left">';
      for (let j = 0; j < errorMessage.length; j++) {
        mainOrderList += '<li>' + errorMessage[j] + '</li>';
      }
      $('#appenErrors').html(mainOrderList);
      $("#createOrderModalOops").modal("show");
    }
  });
}

let createOrderV1 = function(inputObj){
  NewTransferSvc.createOrder(inputObj).then(function (respData) {
    // initGAEvent('event', 'Zeta Optima', 'click', 'Create Transfer Order, corpId=' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', programId=' + programId + ', programName=' + programName + ', scheduledDate=' + scheduledDate + ', fileType=' + fileType + ', purchaseOrderNo=' + purchaseOrderNo + ', timestamp=' + new Date().toLocaleString() + ', Success');
    var viewProgressBtn = $('#myModalInitiatedSuccess').find('.flexible-button .btn'),
        orderID = respData.id,
        progressUrl = '';
    if (inputObj.type == 'incentive') {
      progressUrl = '/companies/' + inputObj.companyID + '/spotlight/' + inputObj.programID + '#transferOrdersTab';
    } else if ($('#currentProductType').val() == "cashless_cafeteria") {
      progressUrl = '/companies/' + inputObj.companyID + '/cashless/' + inputObj.programID;
    } else {
      progressUrl = '/companies/' + inputObj.companyID + '/optima/' + inputObj.programID + '#transferOrders';
    }
    $('#newOrderID').html(orderID);
    if ($("#currentProductType").val() !== 'gift' && $("#currentProductType").val() !== 'reward') {
      // FIXME: This has to be passed as a JS variable
      let scheduleTransferDate = moment($('#scheduleTransferDate').val());
      $('#newScheduleDate').html(scheduleTransferDate.format('dddd, DD MMM YYYY'));
    } else {
      $('#newScheduleDate').parent().hide();
    }


    var repeatAction = $('#repeatAction').val();
    var selectedFrequency = $('#frequencyTypeOptions').val();
    if (repeatAction && repeatAction == 'recurring') {
      $('#newTransferType').html(capitalizeFirstChar(lowerCaseAllWordsExceptFirstLetters(repeatAction)) + ' - ' + capitalizeFirstChar(lowerCaseAllWordsExceptFirstLetters(selectedFrequency)));
      // $('#numberOfOrders').html('(' + numberOfOrders).append(' more orders scheduled' + ')');
    } else if(repeatAction) {
      let text = capitalizeFirstChar(lowerCaseAllWordsExceptFirstLetters(repeatAction));

      let firstStr = text.substr(0, 3);
      let secondStr = text.substr(3, 7);
      let formattedStr = capitalizeFirstChar(lowerCaseAllWordsExceptFirstLetters(secondStr));
      var finalRepeatActionText = firstStr + '-' + formattedStr;

      $('#newTransferType').html(finalRepeatActionText);
      $('#numberOfOrders').html("");
    }else{
      $('#newTransferType').html("One time");
      $('#numberOfOrders').html("");
    }

    $('#newStatus').html(capitalizeFirstChar(lowerCaseAllWordsExceptFirstLetters(respData.orderStatus)));
    $("#myModalLoader").modal("hide");
    $("#myModalInitiatedSuccess").modal('show');
    viewProgressBtn.attr('href', progressUrl);

  }, function (respErr) {
    initGAEvent('event', 'Zeta Optima', 'click', 'Create Transfer Order, corpId=' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', programId=' + programId + ', programName=' + programName + ', scheduledDate=' + scheduledDate + ', fileType=' + fileType + ', purchaseOrderNo=' + purchaseOrderNo + ', timestamp=' + new Date().toLocaleString() + ', Failed, Reason=' + JSON.stringify(respErr));
    $("#myModalLoader").modal("hide");
    if ((Util.checkGenie()) && (respErr.responseJSON.message == "Unauthorized")) {
      $("#myModalOops").modal("show");
      $("#myModalOops .heading-sub-text").text("You do not have access to initiate transfer.");
    } else {
      $("#myModalOops").modal("show");
    }
  });
}


let validateFile = function () {
  validationForTransfer();
  let productType = $('#currentProductType').val();
}

let validationForTransfer = function () {
  console.log("validationForTransfer: ", this);

  //FOR ADD PAYOUT TRANSFER PAGE

  $('#add-payout-order').removeAttr('disabled');
  $('#add-payout-order').removeClass('btn-disabled');


  if ($('#scheduleTransferDate').val() != ''
      && NewTransferCtrl.setFlag() != 0) {

    $('#create-order').removeAttr('disabled');
    $('#create-order').removeClass('btn-disabled');
  }
  let productType = $('#currentProductType').val();
}

let capitalizeFirstChar = function (word) {
  if (word){
    let input = word.replace(/_/g, "");
    return input.charAt(0).toUpperCase() + input.slice(1)
  }else{
    return word
  }
}

let lowerCaseAllWordsExceptFirstLetters = function (word) {
  if (word){
    return word.charAt(0) + word.slice(1).toLowerCase()
  }else{
    return word
  }
}

//start
function getUrlParameter(param) {
  let url;
  if (!url) url = window.location.href;
  alert(url);

  var varUrl = url.split('&');
  for (var i = 0; i < varUrl.length; i++) {

    var paramval = varUrl[i].split('=');
    if (paramval[0] == param) {
      return paramval[1];
    }l

  }

};

let payoutInitiateTransfer = function () {

  let orderIDList = $('#orderId').val().split(',');
  let orderIDListInt = orderIDList.map(Number);
  let recurringOrderID = $('#recurringOrderID').val();
  let inputObj = {

    "fileLocation": Storage.get(Constant.STORAGE_FILE_URL),
    "corpID": Meta.getMetaElement("corpID"),
    "companyID": Meta.getMetaElement("companyID"),
    "fileName": Storage.get(Constant.STORAGE_FILE_NAME)

  }
  if(recurringOrderID){
    inputObj['recurringOrderID'] = recurringOrderID;
  }
  else{
    inputObj['orderIDs'] = orderIDListInt;
  }

  $("#myModalOops").modal("hide");
  $("#myModalLoader").modal("show");

  NewTransferSvc.addPayout(inputObj).then(function (respData) {
    console.log('respData', respData)
    console.log('inputObj', inputObj)
    // initGAEvent('event', 'Zeta Optima', 'click', 'Add New Payout Transfer Order, corpId=' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', programId=' + programId + ', programName=' + programName + ', scheduledDate=' + scheduledDate + ', fileType=' + fileType + ', purchaseOrderNo=' + purchaseOrderNo + ', timestamp=' + new Date().toLocaleString() + ', Success');


    $('#newStatus').text(orderStatus);


    $("#myModalLoader").modal("hide");
    $("#myModalInitiatedSuccess").modal({
      backdrop: 'static',
      keyboard: false
    });


  }, function (respErr) {
    // initGAEvent('event', 'Zeta Optima', 'click', 'Add New Payout Transfer Order, corpId=' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', programId=' + programId + ', programName=' + programName + ', scheduledDate=' + scheduledDate + ', fileType=' + fileType + ', purchaseOrderNo=' + purchaseOrderNo + ', timestamp=' + new Date().toLocaleString() + ', Failed, Reason=' + JSON.stringify(respErr));
    $("#myModalLoader").modal("hide");
    if ((Util.checkGenie()) && (respErr.responseJSON.message == "Unauthorized")) {
      $("#myModalOops").modal("show");
      $("#myModalOops .heading-sub-text").text("You do not have access to initiate transfer.");
    } else {
      $("#myModalOops").modal("show");
    }

    $('#newOrderID').text(orderID);
    $('#newScheduleDate').text(payoutScheduledDate);
    $('#newStatus').text(orderStatus);


    $("#myModalLoader").modal("hide");
    $("#myModalOops").modal("show");
  });
};

let renderFileError = function (params, orderID) {
  // NewTransferSvc.getErrorRows(orderID).then(function(respData){
  domEventHandler.renderMyTemplate('fileErrorContent', fileErrorTemplate, {params: params});
  // })
  $("#table-error").tablewidget({
    source: BulkCloseSvc.getErrorRows,
    pageSize: 10,
    pageNumber: 1,
    tableTemplate: '/template/section/bulk-action/tables/error-entries.ejs',
    rowTemplate: '/template/section/bulk-action/tables/error-entries-row.ejs',
    extraParam: {
        bulkOrderID: orderID,
        dynamicHeader: true
    }
  });
}

let switchAction = function(){
  if($(this).attr('id') == "discardOrder"){
    $('#cancelInfo').show()
    $('#continueInfo').hide()
    $('.orderOptions').removeClass('active');
    $(this).addClass('active');
  }else{
    $('#cancelInfo').hide()
    $('#continueInfo').show()
    $('.orderOptions').removeClass('active');
    $(this).addClass('active');
  }
}

let continueTransfer = function(){
  let inputObj = {
    "corpID" : Meta.getMetaElement("corpID"),
    "action" : "PROCEED",
    "orderID" : Storage.get('orderId')
  }
  $(".modal").modal("hide")
  NewTransferSvc.actionOnOrder(inputObj).then(function(respData){
    var viewProgressBtn = $('#myModalInitiatedSuccess').find('.flexible-button .btn'),
        orderID = inputObj.orderID,
        progressUrl = '',
        type = $("#currentProductType").val(),
        programID = $('#programId').val(),
        companyID = Meta.getMetaElement("companyID"),
        numberOfOrders = 0
    if (type == 'incentive') {
      progressUrl = '/companies/' + companyID + '/spotlight/' + programID + '#transferOrdersTab';
    } else if ($('#currentProductType').val() == "cashless_cafeteria") {
      progressUrl = '/companies/' + companyID + '/cashless/' + programID;
    } else {
      progressUrl = '/companies/' + companyID + '/optima/' + programID + '#transferOrders';
    }
    $('#newOrderID').html(orderID);
    if ($("#currentProductType").val() !== 'gift' && $("#currentProductType").val() !== 'reward') {
      // FIXME: This has to be passed as a JS variable
      let scheduleTransferDate = moment($('#scheduleTransferDate').val());
      $('#newScheduleDate').html(scheduleTransferDate.format('dddd, DD MMM YYYY'));
    } else {
      $('#newScheduleDate').parent().hide();
    }


    var repeatAction = $('#repeatAction').val();
    var selectedFrequency = $('#frequencyTypeOptions').val();
    if (repeatAction && repeatAction == 'recurring') {
      $('#newTransferType').html(capitalizeFirstChar(lowerCaseAllWordsExceptFirstLetters(repeatAction)) + ' - ' + capitalizeFirstChar(lowerCaseAllWordsExceptFirstLetters(selectedFrequency)));
      $('#numberOfOrders').html('(' + numberOfOrders).append(' more orders scheduled' + ')');
    } else if(repeatAction) {
      let text = capitalizeFirstChar(lowerCaseAllWordsExceptFirstLetters(repeatAction));

      let firstStr = text.substr(0, 3);
      let secondStr = text.substr(3, 7);
      let formattedStr = capitalizeFirstChar(lowerCaseAllWordsExceptFirstLetters(secondStr));
      var finalRepeatActionText = firstStr + '-' + formattedStr;

      $('#newTransferType').html(finalRepeatActionText);
      $('#numberOfOrders').html("");
    }else{
      $('#newTransferType').html("One time");
      $('#numberOfOrders').html("");
    }

    $('#noOfBeneficiary').text(Storage.get('noOfBeneficiary'))
    $('#totalAmount').text(Storage.get('totalAmount'))

    $('#newStatus').html(capitalizeFirstChar(lowerCaseAllWordsExceptFirstLetters(respData.status)));
    $("#myModalInitiatedSuccess").modal({
      backdrop: 'static',
      keyboard: false
    });
    viewProgressBtn.attr('href', progressUrl);
  })
}

let downloadErrorReport = function(){
  NewTransferSvc.downloadErrorReport(Storage.get('orderId')).then(function(response){
    var win = window.open(Util.htmlDecoder(response.location), '_blank');
    win.focus();
  }, function (respErr) {
    
  });
}

//end
export default {
  validationForTransfer: validationForTransfer,
  initiateTransfer: initiateTransfer,
  validateFile: validateFile,
  payoutInitiateTransfer: payoutInitiateTransfer,
  switchAction: switchAction,
  continueTransfer:continueTransfer,
  downloadErrorReport: downloadErrorReport
};


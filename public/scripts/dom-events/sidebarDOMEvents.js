import ServiceConnector from '../common/serviceConnector';
import AnalyticsSvc from '../service/analytics/analytics-svc';
import Constants from '../common/constants';
import Storage from '../common/webStorage';
import Meta from '../common/metaStorage';

var toggleSidebar = function (e) {
	$('.nav-btn-cntr').toggleClass('active');
	$('.zeta-logo').toggleClass('zeta-logo-toggle');
	$('.sidebar-container').toggleClass('sidebar-toggle');
	$('.nav-company-toggle').toggleClass('nav-comp-toggle');
	$('.companyHide').toggleClass('hideThisEl');
	$('.companyShow').toggleClass('hideThisEl');
	$('.heading-cntr-toggle').toggleClass('heading-txt-toggle');
	$('.global-heading-toggle').toggleClass('global-heading-txt-toggle');
	$('.new-program-toggle').toggleClass('new-pgm-toggle');
	$('.tooltip').toggleClass('tooltip-inner-sidebar');
	$('li.corpTools > span').toggleClass('textToggle');
	if(!$('.nav-btn-cntr').hasClass('active')){
		$('.sideBar_subcon').slideUp(100);
		$('.openProgList').addClass('closed');
		$('span.arrowBounce').hide();
		$('.sideMenuNav .tooltiptext').show();
	}else{
		$('.openProgList').removeClass('closed');
		$('.sideMenuNav .tooltiptext').hide();
	}
}
 
let activeElementTop = '';

export
default {
	toggleSidebar:toggleSidebar,
	compnayClick: function () {
		let url = '/setSelectedCompanyID?companyID=' + $(this).attr('data-company-id');
		let headers = {};
		ServiceConnector.get(url, headers).then(function (respData) {
			if (Meta.getMetaElement(Constants.COUNTRY_VAR) === 'isSodexoPhpSm') {
				window.location.href = '/companies/' + respData.id + '/' + 'sm-corp/gift-pass?routeTo=' + encodeURIComponent(window.location.pathname);
			} else if(respData.product=="optima"){
				window.location.href = '/companies/' + respData.id + '/' + respData.product;
			} else if(respData.product=="SPOTLIGHT"){
				window.location.href = '/companies/' + respData.id + '/spotlight';
			  // to do  window.location.href = '/companies/' + respData.id + '/spotlight';
			} else if(respData.product=="EXPRESS"){
				window.location.href = '/express';
			} else if(respData.product=="EXPENSE"){
				window.location.href = '/companies/' + respData.id + '/' + 'optima';
				// to do window.location.href = '/companies/' + respData.id + '/expense';
			} else {
				window.location.href = '/companies/' + respData.id + '/' + respData.product;
			}
		}, function (respErr) {
			console.log('Error');

		})
	},
	spotlightAnalytics: function () {
		let eventName = "spotlight.click";
		let sessionID = Storage.get('sessionID');
		let userID = $("meta[name='userID']").attr("content");
		let corp_ID = $("meta[name='corpID']").attr("content");
		let company_ID = $("meta[name='companyID']").attr("content");
		let pitchID = "1";
		let eventLogs = [{
			"serviceName": "instagift",
			"eventName": eventName,
			"sessionID": sessionID,
			"userID_longValue": userID,
			"corpID_longValue": corp_ID,
			"companyID_longValue": company_ID,
			"pitchID_longValue": pitchID
		}];
		AnalyticsSvc.recordEvents(eventLogs);

	},
	openProgDropDown: function(){
		let anchorLink = $(this).parent().attr('href');
		$('.sideMenuNav .tooltiptext').hide();
		if(anchorLink == 'javascript:void(0)'){
			let media = window.matchMedia("(min-width:1170px)").matches;
			let sideBarSubCss = $(this).parent().parent().children('.sideBar_subcon'); 
			if(!media){
				if(!$('.nav-btn-cntr').hasClass('active')){
					toggleSidebar();
				}
			}
			$(this).parent().parent().children('.sideBar_subcon').slideToggle();
			$('.sideBar_subcon').not($(this).parent().parent().children('.sideBar_subcon')).slideUp();
			let currentActiveElement = $(this).parent().parent().children('.sideBar_subcon').children().children('a.activeSubItm');
			if(currentActiveElement.length === 1){
				                                                                                                                                           
				let currentActChildElm = currentActiveElement.offset().top;
				let currentParElm = currentActiveElement.parent().parent('.sideBar_subcon').offset().top;                                                      
				activeElementTop = (currentActChildElm - currentParElm) + 55;   
				$('span.arrowBounce').css('top',activeElementTop+'px');                                                                        
			}else{
				
				//activeElementTop = activeElementTop + 10;                                         
				//console.log("Inside the else block",activeElementTop);                                                                                               
				//$('span.arrowBounce').css('top'+activeElementTop+'px');                                                                                                                                                                                                  
			}                                                                                                                 
			           
			if(sideBarSubCss.length >= 1){
				$('span.arrowBounce').toggle();
				$('span.arrowBounce').not($(this).parent().parent().children('span.arrowBounce').hide());     
			}else{
				$('span.arrowBounce').hide();
			}
		}else{
			
		}
	}, 
	toolTipPos: function(e){
		let currentLi = ($(this).offset().top - $(document).scrollTop())+4;
		let currentToolTip = currentLi+'px';
		$(this).find('.tooltiptext').css('top',currentToolTip); 
	},
	disableBodyScroll: function(e){
		$('body, html').addClass('bodyScroll');
	},
	EnableBodyScroll: function(e){
		$('body, html').removeClass('bodyScroll');
	},
	// checkHoverState: function(e){
	// 	let childEl = $(this).offset().top;
	// 	let parentEl = $(this).parent().parent('.sideBar_subcon').offset().top;
	// 	let finalheight = (childEl - parentEl) + 54;
	// 	$(this).parent().parent().parent().children('span.arrowBounce').css('top',finalheight+'px');
	// 	$(this).parent().parent().parent().children('span.arrowBounce').fadeIn();
	// 	$('.arrowBounce.activeState').fadeIn();
	// },
	// removeHoverState :function(e){
	// 	if($(this).find('.activeSubItm').length == 1){
	// 		$('span.arrowBounce').css('top',activeElementTop+'px'); 
	// 	}else{
	// 		$('span.arrowBounce').hide(); 
	// 	}
	// } 
}  

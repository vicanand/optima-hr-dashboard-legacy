import CardorderSvc from '../service/cardorderSvc';
import CardorderCtrl from '../controller/cardorderCtrl';
import Storage from '../common/webStorage';
import Env from '../common/env';
import Constant from '../common/constants';
import Util from '../common/util';

const funcs = {

    selectProgramCode: function () {
        $('.rewardGift').removeClass('active');
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
        let cardMasterID = $(this).data('cardmasterid');
        let img = $(this).data('img');
        Storage.setSessionCollection('cardMasterID', cardMasterID);
        Storage.setSessionCollection('img', img);
        $('.rewardPImg').attr('src', img);
        $('.rewardPrvwTxt').show();
        $('.rewardMobileDemo').show();
        $('#rewardTemplateSelected').removeAttr('disabled').removeClass('btn-disabled');
    },

    showBenefits: function () {
        $('#chooseDesign').hide();
        $('.needHelp').hide();
        $('.rewardPrvwTxt').css('margin-top', '0px');
        $('#addBenefitsBulk').show();
        $('#addBenefits').hide();
        $('#chooseStep').addClass('done');
        $('#uploadStep').addClass('active');
        var targetUrl = $(this).data('url');
        window.history.pushState({
            url: "" + targetUrl + ""
        }, targetUrl);
        window.location.hash = targetUrl;
    },

    personalization: function () {
        let test = $(this).val();
        $(this).attr('checked', true);
        $("div.personalDv").hide();
        $(".verifyOrder").hide();
        $("#persoanlised" + test).show();
        $("#verify-order" + test).show();
    },

    selectaddress: function () {
        let index = $(this).val();
        var selectedAddress = JSON.parse($(this).data('address'));
        Storage.setSessionCollection('selectedAddress', selectedAddress);
        var companyDetailsNew = $('#addressDetails').data('address');
        companyDetailsNew = JSON.parse(companyDetailsNew);
        console.log(companyDetailsNew);
        var contactDetails = companyDetailsNew.contactDetails[0];
        var state = companyDetailsNew.state;
        var country = companyDetailsNew.country;
        Storage.setSessionCollection('contactDetails', contactDetails);
        Storage.setSessionCollection('state', state);
        Storage.setSessionCollection('country', country);
    },

    validationForManualTransfer: function () {
        if ($('#referenceName').val() != '' && $('#noOfCards').val() != '') {
            $('#verify-order2').removeAttr('disabled').removeClass('btn-disabled');
        } else {
            $('#verify-order2').attr('disabled', 'disabled').addClass('btn-disabled');
        }
    },

    validateFile: function () {
        if (CardorderCtrl.setFlag() != 0 && $('#referenceName').val() != '') {
            $('#verify-order1').removeAttr('disabled').removeClass('btn-disabled');
        } else {
            $('#verify-order1').addClass('btn-disabled').attr('disabled', 'disabled');
        }
    },

    adressPopup: function () {
        $('#myModalAddressDetail').modal('show');
    },
    getNewAddress: function () {
        $('#myModalLoader').modal('show');
        $('#showBtnLoader').show();

        let email = $('#email').val();
        let name = $('#name').val();
        let phoneNumber = $('#phoneNumber').val();
        let address = $('#address').val();
        let pincode = $('#zipcode').val();
        let city = $('#city').val();
        let state = $('#state').val();
        let country = $('#country').val();
        let addressDetail = $('#address').val();

        let selectedAddress = {
            'addressDetail': address,
            'city': city,
            'pincode': pincode,
        };
        let contactDetails = {
            'name': name,
            'phoneNumber': phoneNumber,
            'email': email
        };
        Storage.setSessionCollection('state', state);
        Storage.setSessionCollection('country', country);
        Storage.setSessionCollection('selectedAddress', selectedAddress);
        Storage.setSessionCollection('contactDetails', contactDetails);
        var x = `<label class='control control--radio'>${addressDetail}  <br>${city}
        , ${state},
                                                        ${country}, ${pincode}<br>
                                                        <input data-address=' <%- JSON.stringify(selectedAddress) %>'
                                                            class='address' value="3" type='radio' name='address'
                                                            checked >
                                                        <div class=' control__indicator'></div>
                                                    </label>`;
        $(x).insertBefore('.newAddDv');
    },


    takeCardAction: function () {
        var typeOfAction = Storage.getSessionCollection('typeOfAction');
        var superCardID = Storage.getSessionCollection('superCardID');
        var email = $('#emailid').val() != '' ? $('#emailid').val() : Storage.getSessionCollection('email');
        $('#actionConfirm').modal('hide');
        $('#actionLoader').modal('show');
        CardorderSvc.cardAction(typeOfAction, superCardID, email).then(function (respData) {
            $('#actionLoader').modal('hide');
            if (typeOfAction == 'RESET_PIN') {
                $('#actionLoader').modal('show');
                let superCardID = respData.superCardID;
                let nonce = respData.nonce;
                var newpin = Util.generateOtp();

                CardorderSvc.resetPin(newpin, nonce, superCardID).then(function (responseData) {
                    $('#resetpinModal').modal('show');
                    $('#actionLoader').modal('hide');
                    $('.resetpinTxt').text(newpin);
                }, function (respError) {
                    let resDataError = JSON.stringify(respError);

                    if (resDataError == "{}") {
                        $('#resetpinModal').modal('show');
                        $('#actionLoader').modal('hide');
                        let newtext = `NEW PIN : ${newpin}`;
                        $('.resetpinTxt').text(newtext);
                    } else {
                        $('#actionLoader').modal('hide');
                        $('#cardActionError').modal('show');
                    }

                });

            } else {
                $('#cardActionSuccess').modal('show');
            };

            //console.log(respData);
        }, function (respErr) {
            $('#actionLoader').modal('hide');
            $('#cardActionError').modal('show');
            console.log(respErr);
        });
    },

    // showBenefitsBulk: function () {
    //     localStorage.setItem('selected_Bulk', true);
    //     if ($('#chooseStep').hasClass('done')) {
    //         $('#chooseDesign').hide();
    //         $('#addBenefits').hide();
    //         $('#addBenefitsBulk').show();
    //     }
    // },
    // showManually: function () {
    //     localStorage.setItem('selected_Bulk', false);
    //     if ($('#chooseStep').hasClass('done')) {
    //         $('#chooseDesign').hide();
    //         $('#addBenefits').show();
    //         $('#addBenefitsBulk').hide();
    //     }
    // },

    createNxtBulk: function () {
        // manual = false;
        // bulk = true;
        // var allowDuplicates;
        // Storage.setSessionCollection('manual', manual);
        // Storage.setSessionCollection('bulk', bulk);
        // if (Storage.get('duplicateBulk') == 'true') {
        //     allowDuplicates = true;
        // } else {
        //     allowDuplicates = false;
        // }
        console.log($(this));
        var filename = Storage.get('filename');
        var key = Storage.getSessionCollection('key');
        var fileurl = Storage.getSessionCollection('fileurl');
        var cardMasterID = Storage.getSessionCollection('cardMasterID');
        var refName = $('#referenceName').val();
        var payloadType = 'FILE';
        var addressFlag = $('#deliveryAddCheckbox').val();
        $('#fundingAccountID option:selected').val() == 0 ? $('.fundCurrntBalance').hide() : $('.fundCurrntBalance').show();
        $('#orderCompletedNew').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');


        $('.create-order-spinner').show();
        $('#verify-order1').attr('disabled', true).css('background', '#ccc');

        // $('#card-selection-loader').modal('show');
        CardorderSvc.orderVerifyBulk(filename, fileurl, key, refName, cardMasterID, payloadType, addressFlag).then(function (respData) {
            // Storage.setSessionCollection('bulkOrder', true);
            // Storage.setSessionCollection('manualOrder', false);
            $('#totalBenefits').html(respData.totalPayout);
            $('#totalAmount').html("<b>₹</b> " + (respData.totalAmount.amount / 100));
            $('#totalfeepercard').html("<b>₹</b>" + (respData.feePerCard.amount) / 100);
            $('#totalcardcost').html("<b>₹</b>" + (respData.feeAmount.amount) / 100);
            $('#totalTaxAmount').html("<b>₹</b>" + (respData.taxAmount.amount) / 100);
            Storage.setSessionCollection('requestID', respData.requestID);
            Storage.setSessionCollection('totalPayout', respData.totalPayout);
            Storage.setSessionCollection('feeAmount', (respData.feeAmount.amount / 100));
            Storage.setSessionCollection('feePerCard', (respData.feePerCard.amount / 100));
            Storage.setSessionCollection('taxAmount', (respData.taxAmount.amount / 100));
            Storage.setSessionCollection('totalAmount', (respData.totalAmount.amount / 100));

            $('#card-selection-loader').modal('hide');
            $('.create-order-spinner').hide();
            $('#verify-order1').attr('disabled', false).css('background', '#ffcb39');
            $('#uploadStep').addClass('done');
            $('#verifyStep').addClass('active');
            $('#addBenefitsBulk').hide();
            $('#addBenefits').hide();
            $('#createOrder').show();
            $('#fileErrorWrpr').hide();
            $('#virtualGiftContainer').show();


            var fundingaccountidX = Storage.getSessionCollection('fundingaccountid');
            $('#fundingAccountID option[value=' + fundingaccountidX + ']').attr('selected', 'selected');
            var newBalance = $('#fundingAccountID option:selected').attr('data-balance');
            var fundingaccountname = $('#fundingAccountID option:selected').attr('data-name');
            Storage.setSessionCollection('fundingaccountbalance', newBalance);
            Storage.setSessionCollection('fundingaccountname', fundingaccountname);
            $('#fundingAccountID').on('change', function () {
                var newBalance = $('#fundingAccountID option:selected').attr('data-balance');
                // if (newBalance < respData.totalAmount.amount / 100) {
                //     $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').show();
                //     $('.placeOrderEnd').hide();
                // } else {
                $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').hide();
                $('.placeOrderEnd').show();
                // }


                $('#fundingAccountID option:selected').val() == 0 ? $('.fundCurrntBalance').hide() : $('.fundCurrntBalance').show();
                if (($('#createGiftCheckbox').is(':checked')) && ($('#fundingAccountID option:selected').val() != 0)) {
                    $('#orderCompleted').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                    $('#orderCompletedNew').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                    $('.fundCurrntBalance').show();
                    $('#balanceFund').html(newBalance);
                } else {
                    $('#orderCompleted').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                    $('#orderCompletedNew').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                }
            });

            $('#createGiftCheckbox').on('click', function () {
                if (($(this).is(':checked')) && ($('#fundingAccountID option:selected').val() != 0)) {
                    $('#orderCompleted').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                    $('#orderCompletedNew').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                } else {
                    $('#orderCompleted').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                    $('#orderCompletedNew').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                }
            });
        }, function (respErr) {

            console.log(respErr);
            // $('#virtualGiftContainer').hide();
            $('.create-order-spinner').hide();
            $('#verify-order1').attr('disabled', false).css('background', '#ffcb39')
            $('#card-selection-loader').modal('hide');
            if (respErr) {
                if (respErr.status == 500) {
                    $('#server-error-oops').modal('show');
                    return;
                }
            }
        });

    },

    createNxtManual: function () {
        // manual = true;
        // bulk = false;
        // var allowDuplicates;
        // Storage.setSessionCollection('manual', manual);
        // Storage.setSessionCollection('bulk', bulk);
        // if (Storage.get('duplicateManual') == 'true') {
        //     allowDuplicates = true;
        // } else {
        //     allowDuplicates = false;
        // }

        $('#spot-show-loader').show();
        $('#verify-order2').attr('disabled', true).css('background', '#ccc');
        $('#card-selection-loader').modal('hide');
        var companyDetailsNew = $('#addressDetails').data('address');
        let addressSelected = Storage.getSessionCollection('selectedAddress') || companyDetailsNew.address[0];
        var benefitSuperCardOrderBeneficiary = {};
        var refName = $('#referenceName').val();
        var noOfCards = $('#noOfCards').val();
        var payloadType = 'JSON';
        var contactDetails = Storage.getSessionCollection('contactDetails');
        var name = contactDetails.name;
        var mobile = contactDetails.phoneNumber;
        var email = contactDetails.email;
        var state = Storage.getSessionCollection('state') || companyDetailsNew.state;
        var country = Storage.getSessionCollection('country') || companyDetailsNew.country;
        var cardMasterID = Storage.getSessionCollection('cardMasterID');
        benefitSuperCardOrderBeneficiary = {
            "name": name,
            "mobile": mobile,
            "email": email,
            "addressLine4": addressSelected.addressDetail,
            "pinCode": addressSelected.pincode,
            "city": addressSelected.city,
            "state": state,
            "country": country,
            "noOfCards": noOfCards
        };

        // $('.rewardBenefitInputDv').each(function () {
        //     var beneficiaryObj = {
        //         "name": $(this).find('input.inputField:eq(0)').val(),
        //         "amount": $(this).find('input.inputField:eq(1)').val(),
        //         "contact": $(this).find('input.inputField:eq(4)').val(),
        //         "email": $(this).find('input.inputField:eq(3)').val(),
        //         "transferDate": $(this).find('input.inputField:eq(2)').val(),
        //     }
        //     beneficiaryList.push(beneficiaryObj);
        // });

        // for (var i = 0; i < beneficiaryList.length; i++) {
        //     for (var key in beneficiaryList[i]) {
        //         if (beneficiaryList[i][key] == "") {
        //             $('#spot-show-loader').hide();
        //             $('#verify-order2').attr('disabled', false).css('background', '#ffcb39')
        //             $("#error-required-oops").modal("show");
        //             return;
        //         }
        //     }
        // }
        $('#fundingAccountID option:selected').val() == 0 ? $('.fundCurrntBalance').hide() : $('.fundCurrntBalance').show();
        $('#orderCompletedNew').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');

        Storage.setSessionCollection('benefitSuperCardOrderBeneficiary', benefitSuperCardOrderBeneficiary);
        CardorderSvc.orderVerifyManual(benefitSuperCardOrderBeneficiary, refName, cardMasterID, payloadType).then(function (respData) {
            $('#spot-show-loader').hide();
            $('#verify-order2').attr('disabled', false).css('background', '#ffcb39');
            $('#totalBenefits').html(respData.totalPayout);
            $('#totalAmount').html("<b>₹</b>" + (respData.totalAmount.amount) / 100);

            $('#totalfeepercard').html("<b>₹</b>" + (respData.feePerCard.amount) / 100);
            $('#totalcardcost').html("<b>₹</b>" + (respData.feeAmount.amount) / 100);
            $('#totalTaxAmount').html("<b>₹</b>" + (respData.taxAmount.amount) / 100);
            Storage.setSessionCollection('requestID', respData.requestID);
            // Storage.setSessionCollection('bulkOrder', false);
            // Storage.setSessionCollection('manualOrder', true);
            Storage.setSessionCollection('feeAmount', (respData.feeAmount.amount / 100));
            Storage.setSessionCollection('feePerCard', (respData.feePerCard.amount / 100));
            Storage.setSessionCollection('taxAmount', (respData.taxAmount.amount / 100));
            Storage.setSessionCollection('requestID', respData.requestID);
            Storage.setSessionCollection('totalPayout', respData.totalPayout);
            $('#card-selection-loader').modal('hide');
            $('.spinner-loader').hide();
            $('#uploadStep').addClass('done');
            $('#verifyStep').addClass('active');
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#fileErrorWrpr').hide();
            $('#virtualGiftContainer').show();
            $('#createOrder').show();

            var fundingaccountidX = Storage.getSessionCollection('fundingaccountid');
            $('#fundingAccountID option[value=' + fundingaccountidX + ']').attr('selected', 'selected');
            var newBalance = $('#fundingAccountID option:selected').attr('data-balance');
            var fundingaccountname = $('#fundingAccountID option:selected').attr('data-name');
            Storage.setSessionCollection('fundingaccountbalance', newBalance);
            Storage.setSessionCollection('fundingaccountname', fundingaccountname);
            $('#fundingAccountID').on('change', function () {
                var newBalance = $('#fundingAccountID option:selected').attr('data-balance');
                $('#balanceFund').html(newBalance);
                // if (newBalance < respData.totalAmount / 100) {
                //     $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').show();
                //     $('.placeOrderEnd').hide();
                // } else {
                $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').hide();
                $('.placeOrderEnd').show();
                // }
                $('#fundingAccountID option:selected').val() == 0 ? $('.fundCurrntBalance').hide() : $('.fundCurrntBalance').show()
                if (($('#createGiftCheckbox').is(':checked')) && ($('#fundingAccountID option:selected').val() != 0)) {
                    $('#orderCompleted').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                    $('#orderCompletedNew').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                    $('.fundCurrntBalance').show()
                    $('#balanceFund').html(newBalance);
                } else {
                    $('#orderCompleted').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                    $('#orderCompletedNew').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                }
            });
            $('#createGiftCheckbox').on('click', function () {
                if (($(this).is(':checked')) && ($('#fundingAccountID option:selected').val() != 0)) {
                    $('#orderCompleted').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                    $('#orderCompletedNew').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                } else {
                    $('#orderCompleted').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                    $('#orderCompletedNew').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                }
            });
            $('#spot-show-loader').hide();
            $('#verify-order2').attr('disabled', false).css('background', '#ffcb39');

        }).catch(respErr => {
            console.log(respErr);
            $('#card-selection-loader').modal('hide');
            $('#spot-show-loader').hide();
            $('#verify-order2').attr('disabled', false).css('background', '#ffcb39');
            if (respErr) {
                if (respErr.status == 500) {
                    $('#server-error-oops').modal('show');
                    return;
                }
            }
            // if (respErr.responseJSON.attributes.orderVerifyErrorStatesSet.length === 1 && respErr.responseJSON.attributes.orderVerifyErrorStatesSet[0].toLowerCase() === 'duplicate') {
            //     //var duplicateBulk = true;
            //     Storage.set('duplicateManual', true);
            //     $('.duplicateOr, .duplicateEntries').show();
            //     $('#duplicateEntries').attr('data-url', 'Step3Manual');
            // } else {
            //     Storage.set('duplicateManual', false);
            //     $('.duplicateOr, .duplicateEntries').hide();
            // }
            // $("#createOrderModalSGOops").modal("show");
            // $('#virtualGiftContainer').hide();
            // $('#addBenefits').hide();
            // $('#errorVirtualGiftingTransfers').hide();
            // if (respErr.responseJSON.attributes.fileProcessingSummary) {
            //     let responseData = respErr.responseJSON.attributes.rows;
            //     $('#noOfInvalidEntries').html(respErr.responseJSON.attributes.fileProcessingSummary.invalidRows);

            //     $('.file-details-cntr').hide();


            //     //Build an array containing Customer records.
            //     var customers = new Array();
            //     var ifi = $("meta[name='ifi']").attr('content');
            //     if (ifi == "HDFC") {
            //         customers.push(["Name", "Email", "Mobile", "Amount (₹)", "Transfer Date", "Reason"]);
            //         for (var i = 0; i < responseData.length; i++) {
            //             customers.push([responseData[i].row[0], responseData[i].row[1], responseData[i].row[2], responseData[i].row[3], responseData[i].row[4], allErrors(responseData[i])]);
            //         }
            //     } else {
            //         customers.push(["Name", "Email or Mobile", "Amount (₹)", "Transfer Date", "Reason"]);
            //         for (var i = 0; i < responseData.length; i++) {
            //             customers.push([responseData[i].row[0], responseData[i].row[1], responseData[i].row[2], responseData[i].row[3], allErrors(responseData[i])]);
            //         }
            //     }

            //     function allErrors(error) {
            //         let slash = '<br/>';
            //         let errorsReturn = '';
            //         error.errors[0] != undefined ? errorsReturn += error.errors[0][0] + slash : '';
            //         error.errors[1] != undefined ? errorsReturn += error.errors[1][0] + slash : '';
            //         error.errors[2] != undefined ? errorsReturn += error.errors[2][0] + slash : '';
            //         error.errors[3] != undefined ? errorsReturn += error.errors[3][0] + slash : '';
            //         error.errors[4] != undefined ? errorsReturn += error.errors[4][0] + slash : '';
            //         return errorsReturn;
            //     }

            //     //Create a HTML Table element.
            //     var table = $("<table class='custom-table hover-state error-table custom-table-error' />");
            //     table[0].border = "0";

            //     //Get the count of columns.
            //     var columnCount = customers[0].length;

            //     //Add the header row.
            //     var row = $(table[0].insertRow(-1));
            //     for (var i = 0; i < columnCount; i++) {
            //         var headerCell = $("<th />");
            //         headerCell.html(customers[0][i]);
            //         row.append(headerCell);
            //     }

            //     //Add the data rows.
            //     for (var i = 1; i < customers.length; i++) {
            //         row = $(table[0].insertRow(-1));
            //         for (var j = 0; j < columnCount; j++) {
            //             if (responseData[i - 1].errors[j] == undefined) {
            //                 var cell = $("<td class='' />");
            //                 cell.html(customers[i][j]);
            //                 row.append(cell);
            //             } else {
            //                 var cell = $("<td class='error-column' />");
            //                 cell.html(customers[i][j]);
            //                 row.append(cell);
            //             }
            //         }
            //     }

            //     var dvTable = $("#errorVirtualGiftingTransfers1");
            //     dvTable.html("");
            //     dvTable.append(table);
            //     $('#fileErrorWrpr').show();
            //     $('#uploadAgain').html('RETRY');
            //     //  $('#uploadAgain').attr('data-type', 'manual');
            // }
        });

    },

    orderCreate: function () {
        $('.spinner-loader').show();
        $('#orderCompletedNew').attr('disable', true).css('background', '#ccc')
        $('#orderCompleted').attr('disable', true).css('background', '#ccc')
        var requestID = Storage.getSessionCollection('requestID');
        var fundingaccountid = Storage.getSessionCollection('fundingaccountid');
        var fundingAccountBalance = Storage.getSessionCollection('fundingaccountbalance');
        var fundingAccountName = Storage.getSessionCollection('fundingaccountname');
        var requestID = Storage.getSessionCollection('requestID');
        // CardorderSvc.getAccDetails(authToken, corpID, fundingaccountid).then(function (fundRespData) {
        //     let fundingAcc = fundRespData;

        //     Storage.setSessionCollection('ifiID', (fundingAcc.ifiID));
        // }, function (respErr) {
        //     console.log(respErr);
        // });
        let source = 'Web';

        CardorderSvc.orderCreateBulk(source, requestID, fundingaccountid).then(function (respData) {
            console.log(respData);
            $('.spinner-loader').hide();
            $('#orderCompletedNew').attr('disable', false).css('background', '#ffcb39')
            $('#orderCompleted').attr('disable', false).css('background', '#ffcb39')
            $('#verifyStep').addClass('done');
            $('#createOrderId').html(respData.superCardOrderID);
            $('.createhideIt').hide();
            $('.createhideIt2').show();
            $("#createOrderModalShow").modal("show");
            $('#verifyStep').addClass('done');

            let totalAmount = respData.totalAmount.amount;
            let totalPayout = Storage.getSessionCollection('totalPayout');
            let feePerCard = Storage.getSessionCollection('feePerCard');
            let feeAmount = Storage.getSessionCollection('feeAmount');
            let taxAmount = Storage.getSessionCollection('taxAmount');
            $('#createTotalOrderAmt').html(totalAmount / 100);
            $('#createTotalAmt').html(respData.totalAmount.amount / 100);
            $('#createOrderTotalPayout').html(totalPayout);
            $('#createOrderAmtPerCard').html(feePerCard);
            $('#createOrderTaxAmount').html(taxAmount);
            $('#createOrderCardCost').html(feeAmount);

            $('#createFundName').html(fundingAccountName);
            $('#createFundBalance').html(fundingAccountBalance);
            $('#createOrderDone').data('fundingaccountid', Storage.getSessionCollection('fundingaccountid'));

            if ((totalAmount / 100) > fundingAccountBalance) {
                $('.createhideIt').show();
                $('.createhideIt2').hide();
                let createFundTobAdded = Math.round((totalAmount / 100) - fundingAccountBalance);
                $('#createFundTobAdded').html(createFundTobAdded);
            }
            Storage.removeItem('beneficiaryList');
            Storage.removeItem('cardname');
            Storage.removeItem('cardprogramid');
            Storage.removeItem('greeting');
            Storage.removeItem('message');
            Storage.remove('fileName');
            Storage.remove('storage-filename');
            Storage.remove('totalAmount');
            Storage.remove('totalPayout');

        }, function (respErr) {
            console.log(respErr);
            $('.spinner-loader').hide();
            $('#orderCompletedNew').attr('disable', false).css('background', '#ffcb39');
            $('#orderCompleted').attr('disable', false).css('background', '#ffcb39');
            $("#createOrderModalLoader").modal("hide");
            $("#createOrderModalSGOops").modal("show");
        });



    },


    showManuallyOnStep: function () {
        if ($('#chooseStep').hasClass('done')) {
            $('#chooseStep').addClass('done');
            $('#uploadStep').addClass('active');
            $('#uploadStep').removeClass('done');
            $('#verifyStep').removeClass('active');
            if (localStorage.getItem('selected_Bulk') == 'true') {
                $('#addBenefits').hide();
                $('#addBenefitsBulk').show();
            } else {
                $('#addBenefits').show();
                $('#addBenefitsBulk').hide();
            }
            $('#chooseDesign').hide();
            $('#createOrder').hide();
        }
    },


    showChooseDesign: function () {
        if ($('#chooseStep').hasClass('done')) {
            $('#chooseDesign').show();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#createOrder').hide();
        }
    },

    showVerifyAndPay: function () {
        if ($('#uploadStep').hasClass('done')) {
            $('#chooseDesign').hide();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#createOrder').show();
        }
    },

    addFundsSG: function () {
        $('#createOrderModalShow').modal('hide');
        $('#selectGiftOrderLoader').modal('show');
        let accountId = $(this).attr('data-fundingaccountids');
        let ifiID = Storage.getSessionCollection('ifiID');

        if (accountId == undefined) {
            accountId = Storage.get('ACCOUNT_ID');
            Storage.set('ACCOUNT_ID', accountId);
            Storage.set('ACCOUNT_NAME', Storage.getSessionCollection('fundingaccountname'));
        } else {
            accountId = accountId;
            Storage.set('ACCOUNT_ID', accountId);
            Storage.set('ACCOUNT_ID', accountId);
            Storage.set('ACCOUNT_NAME', Storage.getSessionCollection('fundingaccountname'));
        }
        let corporateId = $("meta[name='corpID']").attr("content");
        let authToken = Storage.get('authToken');
        CardorderSvc.getIfiDetails(authToken, corporateId, ifiID).then(function (getIfisData) {
            // console.log(getIfisData)
            let getIfisBankDetails = getIfisData;
            $('#selectGiftOrderLoader').modal('hide');
            if (ifiID == Constant.KOTAK_FUNDING_ACCS[Env.CURR_ENV]) {
                $('#addFundsBtnWrpr,.list-number-circle').hide();
                $('#addFundDetailsHdg').text('Please use convenient channels to pay to the following bank account.');
                $('#ifiAccNo').text(getIfisBankDetails.bankDetails.accountNumber);
                $('#ifiBankName').text('Kotak Mahindra Bank');
                $('#ifiBankBranch').text('MUMBAI-NARIMAN POINT');
                $('#ifiIfscCode').text(getIfisBankDetails.bankDetails.IFSC);
                $('#acc_name strong').text(Storage.getSessionCollection('fundingaccountname'));
                $('#ifiAccName').text(getIfisBankDetails.bankDetails.accountName);
            } else {
                $('#acNameModalWrpr,#addFundsBtnWrpr,.list-number-circle').show();
                $('#addFundDetailsHdg').text('Drop a cheque/transfer to Zeta’s Bank Account');
                $('#ifiAccNo').text(getIfisBankDetails.bankDetails.accountNumber);
                $('#ifiBankName').text(getIfisBankDetails.bankDetails.bankName);
                $('#ifiBankBranch').text(getIfisBankDetails.bankDetails.bankAddress);
                $('#ifiIfscCode').text(getIfisBankDetails.bankDetails.IFSC);
                $('#acc_name strong').text(Storage.getSessionCollection('fundingaccountname'));
                $('#ifiAccName').text(getIfisBankDetails.bankDetails.accountName);
            }
            $('#fundChequeTransferx').modal('show');
            Storage.set('ACCOUNT_NAME', Storage.getSessionCollection('fundingaccountname'));
        }, function (respErr) {
            $('#selectGiftOrderLoader').modal('hide');
            $('#selectGiftErrorLoader').modal('show');
        });
    },

    routeToAddFund: function () {
        window.location.href = '/admin/funding-accounts/add-funds-details';
    },


    addNewTemplateCode: function () {
        $('#newTemplateCreate').modal('show');
    },

    validationForTransfer: function () {
        if (CardorderCtrl.setFlag() != 0) {
            $('#verify-order1').removeAttr('disabled');
            $('#verify-order1').removeClass('btn-disabled');
        }
    },


    retryErrorModal: function () {
        $('#createBulkOrderModalOops').modal('hide');
    },

    prepareGAEvent: function () {
        initGAEvent('event', $(this).attr('id'), 'click', 'Spotlight');
    },


    fundingAccountIDChange: function () {
        let $createProgram = $('#changeAccSG'),
            //$programName = $('#programName'),
            $accountID = $('#fundingAccountID'),
            $showHideNewFundingAcc = $('#shownewfundingaccname'),
            $NewFundingAccName = $('#newfundingaccname');

        if ($accountID.find(":selected").text() == 'Create New') {
            $showHideNewFundingAcc.show();
            $('#changeAccSG').hide();
            $('#setAccSG').show();
            if ($accountID.val() == 0 || !$NewFundingAccName.val() || $NewFundingAccName.val().length > 40) {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
                $('#newfundingaccname').val(null);
            } else {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
                $('#newfundingaccname').val(null);
            }
        } else {
            $showHideNewFundingAcc.hide();
            $('#changeAccSG').show();
            $('#setAccSG').hide();
            if ($accountID.val() == 0) {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
                $('#newfundingaccname').val(null);
            } else {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
                $('#newfundingaccname').val(null);
            }
        }
    },
    newFundingAccountName: function () {
        let $createProgram = $('#setAccSG'),
            $accountID = $('#fundingAccountID'),
            $showHideNewFundingAcc = $('#shownewfundingaccname'),
            $NewFundingAccName = $('#newfundingaccname');

        if ($accountID.val() == 0 || !$NewFundingAccName.val() || $NewFundingAccName.val().length > 40) {
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $createProgram.removeAttr('disabled');
            $createProgram.removeClass('btn-disabled');
        }
        let accountName = $NewFundingAccName.val();
        let allfundingAccountName = JSON.parse(localStorage.getItem('allFundingAccountName'));
        if (allfundingAccountName.indexOf(accountName) > -1) {
            $('#showfundingAccDuplicate').show();
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $('#showfundingAccDuplicate').hide();
        }
    },


    validateEmail: function () {
        console.log('called');

    },

    redirectUrl: function () {
        $('#createOrderModalShow').modal('hide');
        $('#cardActionSuccess').modal('hide');
        $('#resetFilter').trigger('click');
        $('#chooseDesign').show();
        $('#createOrder').hide();
        window.location.reload();
    },
    // duplicateEnrty: function () {
    //     if (Storage.get('duplicateBulk') == 'true') {
    //         // var targetUrl = 'Step3Bulk';
    //         // window.history.pushState({url: "" + targetUrl + ""}, targetUrl);
    //         // window.location.hash=targetUrl;
    //         funcs.createNxtBulk();

    //     } else if (Storage.get('duplicateManual') == 'true') {
    //         // var targetUrl = 'Step3Manual';
    //         // window.history.pushState({url: "" + targetUrl + ""}, targetUrl);
    //         // window.location.hash=targetUrl;
    //         funcs.createNxtManual();
    //     }
    // },

};
export default funcs;
import AccessCtrlSvc from "../service/accessCtrlSvc.js";
import LocalConstants from "../common/constants";
import Storage from "../common/webStorage.js";
import Util from "../common/util";
import CacheDataStorage from "../common/cacheDataStorage";
import Constants from "../common/constants";

let clearConfirmEmailField = function() {
  $("#confirmEmail").val("");
  $("#invalidEmail").hide();
  $("#validEmail").hide();
  $("#invalidConfirmEmail").hide();
  $("#validConfirmEmail").hide();
  $("#email").removeClass("input-has-error");
  $("#email")
    .parents(".form-group")
    .find(".error-message")
    .html("");
  $("#confirmEmail").removeClass("input-has-error");
  $("#confirmEmail")
    .parents(".form-group")
    .find(".error-message")
    .html("");
};

let validateExistingUser = function() {
  let email = $("#email").val();
  let corpUserList = Storage.getCollection(
    LocalConstants.STORAGE_CORP_ACCOUNTS
  );

  var pos = corpUserList.indexOf(email);
  if (pos >= 0) {
    return false;
  } else {
    return true;
  }

  // if(corpUserList.includes(email)){
  //     return false;
  // }
  // else{
  //     return true;
  // }
};

let validateUserName = function() {
  if (Util.htmlEntities($("#userName").val()).length == 0) {
    $("#userName").addClass("input-has-error");
    $("#userName")
      .parents(".form-group")
      .find(".error-message")
      .html("Please enter a User Name");
    return false;
  } else {
    $("#userName").removeClass("input-has-error");
    $("#userName")
      .parents(".form-group")
      .find(".error-message")
      .html("");
    return true;
  }
};

let validateEmail = function() {
  let emailField = $("#email").val();
  let emailFieldValidity = validateEmailRegex(emailField);
  if (emailField.length == 0) {
    $("#invalidEmail").hide();
    $("#validEmail").hide();
    $("#email").removeClass("input-has-error");
    $("#email")
      .parents(".form-group")
      .find(".error-message")
      .html("");
  } else {
    let existingUser = validateExistingUser();
    if (existingUser) {
      $("#invalidEmail").hide();
      $("#validEmail").hide();
      $("#email").removeClass("input-has-error");
      $("#email")
        .parents(".form-group")
        .find(".error-message")
        .html("");

      if (emailFieldValidity) {
        $("#invalidEmail").hide();
        $("#validEmail").show();
        $("#email").removeClass("input-has-error");
        $("#email")
          .parents(".form-group")
          .find(".error-message")
          .html("");
      } else {
        $("#validEmail").hide();
        $("#invalidEmail").show();
        $("#email").addClass("input-has-error");
        $("#email")
          .parents(".form-group")
          .find(".error-message")
          .html("Invalid email address");
      }
    } else {
      $("#invalidEmail").show();
      $("#email").addClass("input-has-error");
      $("#email")
        .parents(".form-group")
        .find(".error-message")
        .html(
          emailField +
            " already has access to another Corporate account. Please use a different email address."
        );
    }
  }
};

let validateConfirmEmail = function() {
  let matchValidation = validateEmailMatch();
  let confirmEmail = $("#confirmEmail").val();

  if (matchValidation == true && confirmEmail.length > 3) {
    $("#invalidConfirmEmail").hide();
    $("#validConfirmEmail").show();
    $("#confirmEmail").removeClass("input-has-error");
    $("#confirmEmail")
      .parents(".form-group")
      .find(".error-message")
      .html("");
  } else if (matchValidation == false && confirmEmail.length > 3) {
    $("#validConfirmEmail").hide();
    $("#invalidConfirmEmail").show();
    $("#confirmEmail").addClass("input-has-error");
    $("#confirmEmail")
      .parents(".form-group")
      .find(".error-message")
      .html("Email address does not match");
  }
};

let validateEmailMatch = function() {
  let emailField = $("#email").val();
  let emailFieldValidity = validateEmailRegex(emailField);
  let confirmEmailField = $("#confirmEmail").val();

  if (
    emailField.length != 0 &&
    confirmEmailField.length != 0 &&
    emailField == confirmEmailField &&
    emailFieldValidity == true
  ) {
    return true;
  } else {
    return false;
  }
};

let validateFields = function() {
  let userName = Util.htmlEntities($("#userName").val());
  let emailField = $("#email").val();
  let confirmEmailField = $("#confirmEmail").val();
  let userNameValidation = validateUserName();
  let existingUserValidation = validateExistingUser();
  let emailValidation = validateEmailRegex(emailField);
  let confirmEmailValidation = validateEmailRegex(confirmEmailField);
  let emailMatchValidation = validateEmailMatch();
  if (
    userNameValidation == true &&
    emailValidation == true &&
    confirmEmailValidation == true &&
    existingUserValidation == true &&
    emailMatchValidation == true
  ) {
    const country_configs = CacheDataStorage.getItem(Constants.COUNTRY_CONFIGS);
    if (
      country_configs &&
      country_configs.userAccessDOMEvent &&
      country_configs.userAccessDOMEvent.accessControl
    ) {
      if (
        $("#adminYes").is(":checked") ||
        [...$(".access-checkbox:radio:checked")].length > 0
      ) {
        $("#addNewUserBtn").removeClass("btn-disabled");
        $("#addNewUserBtn").prop("disabled", false);
      } else {
        $("#addNewUserBtn").addClass("btn-disabled");
        $("#addNewUserBtn").prop("disabled", true);
      }
    } else {
      $("#addNewUserBtn").removeClass("btn-disabled");
      $("#addNewUserBtn").prop("disabled", false);
    }
  } else {
    $("#addNewUserBtn").addClass("btn-disabled");
    $("#addNewUserBtn").prop("disabled", true);
  }
};

let validateEmailRegex = function(fieldValue) {
  let regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regExp.test(fieldValue);
};

let addUser = function() {
    $('#addNewUserBtn').html("Adding User");
    $('#addNewUserBtn').addClass('btn-disabled');
    $('#addNewUserBtn').prop('disabled', true);
    $('#addUserLoader').show();

    let countryName = $("meta[name='ifi']").attr("content")
    let userName = $('#userName').val()
    let userEmail = $('#email').val()
    let selectedCompId = $("meta[name='companyID']").attr("content")
    let corpID = $("meta[name='corpID']").attr("content")

    let requestPayload = {
        corpID : corpID,
        name : userName,
        email : userEmail,
        companyIds : [selectedCompId],
        role : 'ADMIN'
    }

    AccessCtrlSvc.addCorpAccount(requestPayload).then(function(responseData) {
        const country_configs = CacheDataStorage.getItem(Constants.COUNTRY_CONFIGS);
        if(country_configs && country_configs.userAccessDOMEvent && country_configs.userAccessDOMEvent.accessControl) {
            let roleArray = [];
            if($('#adminYes').is(':checked')) {
                roleArray = ['ADMIN'];
            } else {
                let selectedRole = [...$('.access-checkbox:radio:checked')];
                roleArray = selectedRole.map(role => role.id);
            }
            // let companyId = $("meta[name='companyIDs']").attr("content");
            // companyId = companyId.split(',').map(id => parseInt(id));
            let priviledgePayload = {
                corpId: $("meta[name='corpID']").attr("content"),
                roles: roleArray,
                emailId: userEmail,
                companyIds: [selectedCompId]
            }
            AccessCtrlSvc.givePrivilegeToUser(priviledgePayload).then(function(responseData) {
                addUserSuccess();
            },function(responseError){
                if(responseError.responseText){
                    let message = JSON.parse(responseError.responseText).message
                    let payload = {
                        "userProfile": {
                            "name": {
                                "firstName": userName.split(" ")[0],
                                "lastName": userName.split(" ")[1] ? userName.split(" ")[1] : ""
                            },
                            "emails":[userEmail],
                            "phoneNumbers": []
                        }
                    }
                    if(message && message.indexOf("User does not exist") != -1){
                        AccessCtrlSvc.createUserWithDetails(payload).then(function(resp) {
                            AccessCtrlSvc.givePrivilegeToUser(priviledgePayload).then(function(responseData) {
                                addUserSuccess();
                            })
                        }, function(responseError){
                            addUserFail(responseError);
                        })
                    }
                }else{
                    addUserFail(responseError);
                }
            });
        } else {
            addUserSuccess();
        }
    },
    function(responseError) {
      addUserFail(responseError);
    });
};

let addUserSuccess = function() {
  let message =
    "A new user named " +
    Util.htmlEntities($("#userName").val()) +
    " has been added successfully";
  $("#addUserModal").modal("hide");
  $("#success-img").show();
  $("#errorResponse").html("");
  $("#errorResponseMsg").html(message);
  $("#responseBtn").html("DONE");
  $("#requestResponseModal").modal({
    backdrop: "static",
    keyboard: false
  });
  $("#requestResponseModal").modal("show");
};

let addUserFail = function(responseError) {
  let errorObj = JSON.parse(responseError.responseText);
  $("#addUserModal").modal("hide");
  $("#error-img").show();
  if (Util.checkGenie()) {
    $("#errorResponse").html("Failed to add user");
    $("#errorResponseMsg").html("You do not have access to add user");
    $("#showDownloadStatementError #showDownloadStatementErrorMsg").text(
      "You do not have access to download employee statement"
    );
  } else {
    $("#errorResponse").html(responseError.statusText);
    $("#errorResponseMsg").html(errorObj.message);
  }
  $("#responseBtn").html("DONE");
  $("#requestResponseModal").modal({
    backdrop: "static",
    keyboard: false
  });
  $("#requestResponseModal").modal("show");
};

let changeStatus = function() {
  let currStatus = $(this).data("status");
  let requestPayload = {};
  if (currStatus == "active") {
    requestPayload.status = "suspended";
  } else if (currStatus == "suspended") {
    requestPayload.status = "active";
  }
  requestPayload.corpId = $("meta[name='corpID']").attr("content");
  requestPayload.accountId = $(this).data("accountid");
  let isSodexoPh = $("meta[name='countryVar']").attr("content") == "isSodexoPh" ? true : false;
  if(isSodexoPh){
    requestPayload.companyIds = [$("meta[name='companyID']").attr("content")]
  }
  $("#changeAccessLoader").show();
  $("#changeStatusBtn").addClass("btn-disabled");
  $("#changeStatusBtn").prop("disabled", true);
  AccessCtrlSvc.changeAccountStatus(requestPayload).then(
    function(responseData) {
      let userName = $("#changeStatusBtn").data("name");
      let message;
      if (currStatus == "active") {
        message =
          userName +
          " no longer has access to " +
          $("#currCorp").html() +
          "’s corporate account";
      } else if (currStatus == "suspended") {
        message =
          userName +
          "'s access to " +
          $("#currCorp").html() +
          "’s corporate account has been restored";
      }
      $("#changeStatusModal").modal("hide");

      $("#success-img").show();
      $("#errorResponse").html("");
      $("#errorResponseMsg").html(message);
      $("#responseBtn").html("DONE");
      $("#requestResponseModal").modal({
        backdrop: "static",
        keyboard: false
      });
      $("#requestResponseModal").modal("show");
    },
    function(responseError) {
      let errorObj = JSON.parse(responseError.responseText);

      $("#changeStatusModal").modal("hide");
      $("#error-img").show();
      if (Util.checkGenie()) {
        $("#errorResponse").html("Failed!");
        $("#errorResponseMsg").html(
          "You do not have access to perform this action."
        );
        $("#showDownloadStatementError #showDownloadStatementErrorMsg").text(
          "You do not have access to download employee statement"
        );
      } else {
        $("#errorResponse").html(responseError.statusText);
        $("#errorResponseMsg").html(errorObj.message);
      }
      $("#responseBtn").html("DONE");
      $("#requestResponseModal").modal({
        backdrop: "static",
        keyboard: false
      });
      $("#requestResponseModal").modal("show");
    }
  );
};

let reloadPage = function() {
  location.reload();
};

let roleSelectShowHide = function() {
  let adminRole = $("#adminYes").is(":checked");
  let roleSelectOption = $(".role-select-wrap");
  if (adminRole) {
    roleSelectOption.hide();
  } else {
    roleSelectOption.show();
  }
  validateFields();
};

let onRoleCheck = function(event) {
  if (event.target.id === "COMPANY_ADMIN_READ" && event.target.checked) {
    $(".other-roles .access-checkbox:checkbox").prop("checked", false);
    $(".other-roles").addClass("role-select-disabled");
  } else if (
    event.target.id === "COMPANY_ADMIN_READ" &&
    !event.target.checked
  ) {
    $(".other-roles").removeClass("role-select-disabled");
  }

  $("input[name="+$(this).attr('name')+"]").removeClass('checked');
  if($(this).prop('checked')){
    $(this).addClass('checked')
  }

  validateFields();
};

let toggleRadioBtn = function(){
  if($(this).hasClass('checked')){
    $(this).prop('checked', false);
    $(this).removeClass('checked');
  }
  
  if($(this).prop('checked')){
    $(this).addClass('checked')
  }
}

export default {
  addUser: addUser,
  clearConfirmEmailField: clearConfirmEmailField,
  validateUserName: validateUserName,
  validateExistingUser: validateExistingUser,
  validateFields: validateFields,
  validateEmail: validateEmail,
  validateConfirmEmail: validateConfirmEmail,
  validateEmailRegex: validateEmailRegex,
  validateEmailMatch: validateEmailMatch,
  changeStatus: changeStatus,
  reloadPage: reloadPage,
  roleSelectShow: roleSelectShowHide,
  onRoleCheck: onRoleCheck,
  toggleRadioBtn: toggleRadioBtn
};

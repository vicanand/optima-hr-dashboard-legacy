import ServiceConnector from "../common/serviceConnector";
import SignupSvc from "../service/signupSvc";
import Urls from "../common/urls";
import Util from "../common/util";
import Env from "../common/env";

let dataArr = [{"city":"Cần Thơ","province":"Cần Thơ"},{"city":"Đà Nẵng","province":"Đà Nẵng"},{"city":"Hải Phòng","province":"Hải Phòng"},{"city":"Hà Nội","province":"Hà Nội"},{"city":"Hồ Chí Minh","province":"Hồ Chí Minh"},{"city":"Bà Rịa","province":"Bà Rịa–Vũng Tàu"},{"city":"Bạc Liêu","province":"Bạc Liêu"},{"city":"Bắc Giang","province":"Bắc Giang"},{"city":"Bắc Ninh","province":"Bắc Ninh"},{"city":"Bảo Lộc","province":"Lâm Đồng"},{"city":"Biên Hòa","province":"Đồng Nai"},{"city":"Bến Tre","province":"Bến Tre"},{"city":"Buôn Ma Thuột","province":"Đắk Lắk"},{"city":"Cà Mau","province":"Cà Mau"},{"city":"Cẩm Phả","province":"Quảng Ninh"},{"city":"Cao Lãnh","province":"Đồng Tháp"},{"city":"Châu Đốc","province":"An Giang"},{"city":"Đà Lạt","province":"Lâm Đồng"},{"city":"Điện Biên Phủ","province":"Điện Biên"},{"city":"Đông Hà","province":"Quảng Trị"},{"city":"Đồng Hới","province":"Quảng Bình"},{"city":"Hà Tĩnh","province":"Hà Tĩnh","area":"56.19","population":"117,546"},{"city":"Hạ Long","province":"Quảng Ninh"},{"city":"Hải Dương","province":"Hải Dương"},{"city":"Hòa Bình","province":"Hòa Bình"},{"city":"Hội An","province":"Quảng Nam"},{"city":"Huế","province":"Thừa Thiên–Huế"},{"city":"Hưng Yên","province":"Hưng Yên"},{"city":"Kon Tum","province":"Kon Tum"},{"city":"Lạng Sơn","province":"Lạng Sơn"},{"city":"Lào Cai","province":"Lào Cai"},{"city":"Long Xuyên","province":"An Giang"},{"city":"Móng Cái","province":"Quảng Ninh"},{"city":"Mỹ Tho","province":"Tiền Giang"},{"city":"Nam Định","province":"Nam Định"},{"city":"Ninh Bình","province":"Ninh Bình"},{"city":"Nha Trang","province":"Khánh Hòa"},{"city":"Cam Ranh","province":"Khánh Hòa"},{"city":"Phan Rang–Tháp Chàm","province":"Ninh Thuận"},{"city":"Phan Thiết","province":"Bình Thuận","area":"206.0","population":"255,000"},{"city":"Phủ Lý","province":"Hà Nam","area":"34.27","population":"121,350"},{"city":"Pleiku","province":"Gia Lai","area":"260.61","population":"186,763"},{"city":"Quảng Ngãi","province":"Quảng Ngãi","area":"37.12","population":"134,400"},{"city":"Quy Nhơn","province":"Bình Định","area":"284.28","population":"311,000"},{"city":"Rạch Giá","province":"Kiên Giang","area":"97.75","population":"228,360"},{"city":"Sa Đéc","province":"Đồng Tháp","area":"59.81","population":"152,237"},{"city":"Sóc Trăng","province":"Sóc Trăng","area":"76.15","population":"173,922"},{"city":"Sơn La","province":"Sơn La","area":"324.93","population":"107,282"},{"city":"Tam Kỳ","province":"Quảng Nam","area":"92.63","population":"120,256"},{"city":"Tân An","province":"Long An"},{"city":"Thái Bình","province":"Thái Bình"},{"city":"Thái Nguyên","province":"Thái Nguyên"},{"city":"Thanh Hóa","province":"Thanh Hóa"},{"city":"Trà Vinh","province":"Trà Vinh"},{"city":"Tuy Hòa","province":"Phú Yên"},{"city":"Tuyên Quang","province":"Tuyên Quang"},{"city":"Uông Bí","province":"Quảng Ninh"},{"city":"Việt Trì","province":"Phú Thọ"},{"city":"Vinh","province":"Nghệ An"},{"city":"Vĩnh Yên","province":"Vĩnh Phúc"},{"city":"Vĩnh Long","province":"Vĩnh Long"},{"city":"Vũng Tàu","province":"Bà Rịa–Vũng Tàu"},{"city":"Yên Bái","province":"Yên Bái"},{"city":"Cao Bằng","province":"Cao Bằng"}]
let provinces = [];
let cities = [];

let _createCorpAcc = function (inputJson) {
  console.log("RequestPayload----", inputJson);
  $('#createAccount').addClass('loading');
  let headers = {
    "Content-Type": "application/json"
  };
  let url = Urls.getOMSApiUrl().CORP_SIGNUP;
  ServiceConnector.post(inputJson, url, headers).then(
    function(respData) {
      console.log("success",respData);
      $(".signup__form__wrpr").hide();
      $(".signup__form__success__wrpr").css("display", "table-cell");
    },
    function(respErr) {
      console.log("Error", respErr);
      
       $("#createAccount").removeClass("loading");
      respErr = JSON.parse(respErr.responseText);
      if (respErr.type == "CorporateCreationException") {
        $("#emailID")
          .addClass("input-error")
          .siblings(".error-msg")
          .html("Email already in use")
          .show();
      } else if(respErr.message){
        $("#signupError").html(respErr.message);
      } else {
        $("#signupError").html("Server error. Please try again later.");
      }
    }
  );
};
let getIFI = function() {
  const ifiList = {
    STAGE: '151515',
    PREPROD: '156924',
    PROD: '290831'
  }
  return ifiList[Env.CURR_ENV];
};
let getInputJson = function() {
  let nameTitle = $("#nameTitle").val();
  let fullName = $("#fullName").val();
  let designation = $("#designation").val();
  let emailID = $("#emailID").val();
  let contactNo = $("#contactNo").val();
  let companyName = $("#companyName").val();
  let taxCode = $("#taxCode").val();
  let bill_address = $("#bill_address").val();
  let bill_city = $("#bill_city").val();
  let bill_state = $("#bill_province").val();
  let bill_pinCode = $("#bill_pinCode").val();
  let comm_address = $("#comm_address").val();
  let comm_city = $("#comm_city").val();
  let comm_state = $("#comm_province").val();
  let comm_pinCode = $("#comm_pinCode").val();

  if (nameTitle == "") {
    nameTitle = "mr";
  }

  let data = {
    ifiId: getIFI(),
    ifiSpecificAttrs: {
      title: nameTitle,
      name: fullName,
      email: emailID,
      companyName: companyName,
      phoneNumber:  '+84' + contactNo,
      designation: designation,
      employeeCount : "1",
      communicationAddress: {
        addressDetail: comm_address,
        city: comm_city,
        state: comm_state,
        country: "Vietnam",
        addressType: "communication",
        pincode: comm_pinCode
      },
      billingAddress: {
        addressDetail: bill_address,
        city: bill_city,
        state: bill_state,
        country: "Vietnam",
        addressType: "billing",
        pincode: bill_pinCode
      },
      taxIDNumber: taxCode
    }
  };
  return data;
};
let validateForm = function() {
  let $fullName = $("#fullName");
  let $designation = $("#designation");
  let $emailID = $("#emailID");
  let $contactNo = $("#contactNo");
  let $companyName = $("#companyName");
  let $comm_address = $("#comm_address");
  let $comm_city = $("#comm_city");
  let $comm_state = $("#comm_province");
  let $comm_pinCode = $("#comm_pinCode");

  let sameAsBillingAddrs = $("#sameAsBillingAddrs").is(":checked");

  let fullName = $fullName.val();
  let designation = $designation.val();
  let contactNo = $contactNo.val();
  let emailID = $emailID.val();
  let companyName = $companyName.val();
  let comm_address = $comm_address.val();
  let comm_city = $comm_city.val();
  let comm_state = $comm_state.val();
  let comm_pinCode = $comm_pinCode.val();

  let isValid = true;

  if (fullName == "") {
    $fullName.addClass("input-error");
    $fullName.siblings(".error-msg").html("Please enter your name").show();
    isValid = false;
  }
  if (designation == "") {
    $designation.addClass("input-error");
    $designation
      .siblings(".error-msg")
      .html("Please enter your title")
      .show();
    isValid = false;
  }
  if (!Util.isEmail(emailID)) {
    $emailID.addClass("input-error");
    $emailID
      .siblings(".error-msg")
      .html("Please enter valid email ID")
      .show();
    isValid = false;
  }
  if (contactNo.length != 9) {
    $contactNo.addClass("input-error");
    $contactNo
      .siblings(".error-msg")
      .html("Please enter valid phone number")
      .show();
    isValid = false;
  }

  if (companyName == "") {
    $companyName.addClass("input-error");
    $companyName
      .siblings(".error-msg")
      .html("Please enter company name")
      .show();
    isValid = false;
  }
  if (comm_address == "") {
    $comm_address.addClass("input-error").attr("disabled", false);
    $comm_address
      .siblings(".error-msg")
      .html("Please enter communication address")
      .show();
    isValid = false;
  }
  if (comm_state == "") {
    $comm_state.attr("disabled", false);
    $comm_state.parent().addClass("input-error");
    $comm_state
      .parent()
      .siblings(".error-msg")
      .html("Please select province")
      .show();
    isValid = false;
  }
  if (comm_city == "") {
    $comm_city.attr("disabled", false);
    $comm_city.parent().addClass("input-error");
    $comm_city
      .parent()
      .siblings(".error-msg")
      .html("Please select city")
      .show();
    isValid = false;
  }
  if (comm_pinCode.length != 6) {
    $comm_pinCode.addClass("input-error").attr("disabled", false);
    $comm_pinCode
      .siblings(".error-msg")
      .html("Please enter valid pincode")
      .show();
    isValid = false;
  }
  return isValid;
};

let updateDropDown = function (el, values) {
  $.each(values, function (key, value) {
    el.append($("<option>" + value + "</option>")).insertAfter("value", value);
  });
};

let updateCityDropDown = function (el, values, type) {
  $(el).empty();
  let defaultText = "City*";
  if (type == 'BILL') {
    defaultText = 'City'
  }
  $(el).append($('<option>', {
    value: "",
    text: defaultText
  }));
  updateDropDown(el, values)
};

let updateProvinceDropDown = function (el, values, type) {
  $(el).empty();
  let defaultText = "Province*";
  if (type == 'BILL') {
    defaultText = 'Province'
  }
  $(el).append($('<option>', {
    value: "",
    text: defaultText
  }));
  updateDropDown(el, values)
};

let filterCities = function(ele) {
  var selectedProvince = ele.val();
  cities = [];
  dataArr.forEach(function (data) {
    if (selectedProvince == "") {
      cities.push(data.city);
    }
    else if (data.province == selectedProvince) {
      cities.push(data.city);
    }
  });
  return cities;
};

export default {
  createAccount: function() {
    if (validateForm()) {
      _createCorpAcc(getInputJson());
    }
  },
  inputChange: function(e) {
    if ($(this).val() == "") {
      $(this).addClass("input-error");
      $(this)
        .siblings(".error-msg")
        .show();
    } else {
      $(this).removeClass("input-error");
      $(this)
        .siblings(".error-msg")
        .hide();
    }
    if (e.keyCode == 13) {
      $("#createAccount").trigger("click");
    }
  },
  copyBillingToCommAddrs: function() {
    let comm_address = $("#comm_address");
    let comm_province = $("#comm_province");
    let comm_city = $("#comm_city");
    let comm_pinCode = $("#comm_pinCode");

    if ($("#sameAsBillingAddrs").is(":checked")) {
    
      comm_address.val($("#bill_address").val());
      comm_province.val($("#bill_province").val());
      comm_city.val($("#bill_city").val());
      comm_pinCode.val($("#bill_pinCode").val());
      
      comm_address.removeClass("input-error").attr("disabled", true);
      comm_address.siblings(".error-msg").hide();

      comm_province.attr("disabled", true);
      comm_province.parent().removeClass("input-error");
      comm_province.parent().siblings(".error-msg").hide();

      comm_city.attr("disabled", true);
      comm_city.parent().removeClass("input-error");
      comm_city.parent().siblings(".error-msg").hide();

      comm_pinCode.removeClass("input-error").attr("disabled", true);
      comm_pinCode.siblings(".error-msg").hide();
    } else {
      comm_address.val('').attr("disabled", false);
      comm_province.val('').attr("disabled", false);
      comm_city.val('').attr("disabled", false);
      comm_pinCode.val('').attr("disabled", false);
    }
  },
  filterCitiesForBillAddress: function () {
    let cities = filterCities($("#bill_province"));
    let billCityDrop = $("#bill_city");
    updateCityDropDown(billCityDrop, cities,  'BILL');
  },

  filterCitiesForCommAddress: function () {
    let cities = filterCities($("#comm_province"));
    let commCityDrop = $("#comm_city");
    updateCityDropDown(commCityDrop, cities, 'COMM');
  },

  getCities: function () {
    console.log('dummy');
    // let url = Urls.getOMSApiUrl().GET_CITIES;
    // let headers = {
    //   "Content-Type": "application/json"
    // };
    // let url = 'https://raw.githubusercontent.com/hienvd/vietnam-cities-list/master/cities.json'
    // ServiceConnector.post({}, url, headers).then(
    //   function(respData) {
    //     console.log("respData",respData);
    //     let provinces = [];
    //     let cities = [];
    //     respData.forEach(function(data){
    //       provinces.push(data.province);
    //       cities.push(data.city);
    //     });

    //     let billProvinceDrop = $("#bill_province");
    //     let commProvinceDrop = $("#comm_province");
    //     let billCityDrop = $("#bill_city");
    //     let commCityDrop = $("#comm_city");
    //     updateDropDown(billProvinceDrop, provinces);
    //     updateDropDown(commProvinceDrop, provinces);
    //     updateDropDown(billCityDrop, cities);
    //     updateDropDown(commCityDrop, cities);
    //   },
    //   function(respErr) {
    //     console.log("Error", respErr);
    //   }
    // );
    provinces = [];
    cities = [];

    dataArr.forEach(function (data) {
      if (provinces.indexOf(data.province) < 1) {
        provinces.push(data.province);
      }
      cities.push(data.city);
    });

    provinces.sort();
    cities.sort();
    
    let billProvinceDrop = $("#bill_province");
    let commProvinceDrop = $("#comm_province");
    let billCityDrop = $("#bill_city");
    let commCityDrop = $("#comm_city");
    updateProvinceDropDown(billProvinceDrop, provinces, 'BILL');
    updateProvinceDropDown(commProvinceDrop, provinces, 'COMM');
    updateCityDropDown(billCityDrop, cities, 'BILL');
    updateCityDropDown(commCityDrop, cities, 'COMM');
  },
  enableSameAsBillingAddrs: function () {
    let bill_address = $("#bill_address").val();
    let bill_city = $("#bill_city").val();
    let bill_state = $("#bill_province").val();
    let bill_pinCode = $("#bill_pinCode").val();
    if (bill_address != "" && bill_state != "" && bill_city != "" && bill_pinCode != "") {
      $('#sameAsBillingAddrs').attr("disabled", false);
    } else {
      $('#sameAsBillingAddrs').attr("disabled", true);
    }
  }
};

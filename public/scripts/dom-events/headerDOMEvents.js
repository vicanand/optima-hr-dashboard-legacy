import ServiceConnector from '../common/serviceConnector';
import Storage from '../common/webStorage';
import Urls from '../common/urls';
import Utils from '../common/util';
import { setTimeout } from 'timers';
import Meta from '../common/metaStorage';
import Constants from '../common/constants';
import Env from '../common/env';
import {COUNTRY_CONFIGS} from '../common/countryConfig/configMappings';

let _disableChangePasswordBtn = function () {
  $('#changePasswordBtn').attr('disabled', 'disabled');
  $('#changePasswordBtn').addClass('btn-disabled');
  return;
};

let _clearPasswordBoxes = function () {
  $('#currentPassword,#newPassword,#retypeNewPassword').val('');
};

var selectCorporates = function () {
  $(".list-group-item").removeClass("list-group-item-activeSelect  active");
  $(".list-group-item-text").removeClass("active");

  $(this).addClass("list-group-item-activeSelect active");
  $(".list-group-item-text").addClass("active");

  $(".signup__form___button--switch").addClass('signup__form___button--switch-background');
  var selectedCorporate = $(this).attr('data-key');
  Storage.set("SELECTED_CORP_ID", selectedCorporate);
  return selectedCorporate;
};

function redirect(redirectUrl){
    if(redirectUrl != "#"){
        setTimeout(function() {
            window.location.href = redirectUrl;
        }, 100);
    }
}

export
default {
  selectCorporates: selectCorporates,
  validate: function () {
    let currPassword = $('#currentPassword').val();
    let newPassword = $('#newPassword').val();
    let confirmNewPassword = $('#retypeNewPassword').val();
    if (currPassword.length < 1) {
      _disableChangePasswordBtn();

    } else if (newPassword.length < 1 && confirmNewPassword.length < 1) {
      _disableChangePasswordBtn();
    } else if (newPassword != confirmNewPassword) {
      _disableChangePasswordBtn();
    } else {
      $('#changePasswordBtn').removeAttr('disabled');
      $('#changePasswordBtn').removeClass('btn-disabled');
    }
  },
  searchBussiness:function(){
    var value = $(this).val().toLowerCase();
    $(".dropdown-menu li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
    });
  },
  changePasswordBtn: function () {
    $('#loaderModal').modal();
    let inputJSON = {
      "oldPassword": $('#currentPassword').val(),
      "newPassword": $('#newPassword').val(),
      "confirmNewPassword": $('#retypeNewPassword').val()
    };
    let url = Urls.getCorpApiUrl().CHANGE_PASSWORD;
    let headers = {
      "token": Storage.get('authToken'),
      "Content-Type": "application/json"
    };
    ServiceConnector.put(inputJSON, url, headers).then(function (respData) {
      $('#loaderModal').modal('hide');
      $('#changePasswordSuccess').modal('hide');
    }, function (respErr) {
      $('#loaderModal').modal('hide');
      if (respErr.status == 200) {
        $('#changePassword').modal('hide');
        $('#changePasswordSuccess').modal('show');
        _clearPasswordBoxes();
        return;
      }
      $('#errorMsg').html(JSON.parse(respErr.responseText).error);
      $('#changePasswordError').modal('show');
    })
  },
  dropDownSelect: function () {
    var value = $(".companyName").html();
    var element = $("li[data-company='" + value + "']");
    element.addClass("active");
  },

  corporateSwitch: function() {
    $('#corporateSwitcherModal').modal();
    let requestedSource = Utils.getUrlParameter('source') || '';
    let url = '/setSelectedCorporatesID?corpID=' + Storage.get("SELECTED_CORP_ID");
    let headers = {};
    ServiceConnector.get(url, headers).then(function(respData) {
      if(!requestedSource){
        let isSodexoPhpSm = COUNTRY_CONFIGS.SODEXO_PHILIPPINES_CONFIGS.SM_CORP_IDS[Env.CURR_ENV.toLowerCase()].indexOf(parseInt(Storage.get("SELECTED_CORP_ID"))) > -1;
        if (isSodexoPhpSm) {
            window.location.href = '/companies/' + respData.id + '/' + 'sm-corp/gift-pass';
        } else if(respData.product=="optima"){
            window.location.href = '/companies/' + respData.id + '/' + respData.product;
        } else if(respData.product=="SPOTLIGHT"){
            window.location.href = '/companies/' + respData.id + '/' + 'optima';
          // to do  window.location.href = '/companies/' + respData.id + '/spotlight';
        } else if(respData.product=="EXPRESS"){
            window.location.href = '/companies/'+respData.id+'/cashless';
        } else if(respData.product=="EXPENSE"){
            window.location.href = '/companies/' + respData.id + '/' + 'optima';
            // to do window.location.href = '/companies/' + respData.id + '/expense';
        } else {
            window.location.href = '/companies/' + respData.id + '/' + respData.product;
        }
      }else{
        window.location.href = '/documents-drive?source='+requestedSource;
      }
    }, function(respErr) {
      if(respErr.status === 401){
        $('#corporateSwitcherModal').modal('hide');
        $('#corporateSwitcherErrorModal').modal();
      }
        console.log('Error');
    });
  },
  companySwitch: function() {
      if($(this).attr('href')){
          initGAEvent('event', 'CompanySwitch', 'change', 'CompanySwitch (From: ' + window.location.pathname + ')');
      }
  },
  sideMenuNavigation: function(){
      initGAEvent('event', $(this).data('pagename'), 'click', 'Navigated to '+$(this).data('pagename')+' (From: ' + window.location.pathname + '), corpId = ' + Meta.getMetaElement("corpID") + ', compId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', timestamp='+new Date().toLocaleString());
  },
  selectGiftingLandingEvents: function(){
      var setupParams = "";
      if($(this).data('programType')){
          setupParams = ', programType='+$(this).data('programType') + 'programName=' + $(this).data('programName');
      }
      initGAEvent('event', 'Select Gifting', 'click', $(this).data('label') + ' Clicked, corpId = ' + Meta.getMetaElement("corpID") + ', compId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString() + setupParams);
  },
  newGiftOrder: function(){
      initGAEvent('event', 'Select Gifting', 'click', 'New Gift Order Clicked, corpId = ' + Meta.getMetaElement("corpID") + ', compId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());
  },
  spotlightLearnMore: function(){
      initGAEvent('event', 'Spotlight Learn More', 'click', 'spotlight banner clicked, corpId = ' + Meta.getMetaElement("corpID") + ', compId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());
  },
  recieveMessage: function(event) {
    const allowedOrigin = {
      "PROD":'https://php-merchant-pos.zetaapps.in',
      "PREPROD": 'https://php-merchant-pos-pp.zetaapps.in',
      "STAGE":  'https://php-merchant-pos-stage.zetaapps.in',
    };
    if (event && event.originalEvent) {
      if (allowedOrigin[Env.CURR_ENV] !== event.originalEvent.origin)
        return;
        const eventData = event.originalEvent.data;
      if (eventData && eventData.type) {
        if (eventData.type === 'triggerLogout') {
          window.location.href = '/logout';
        } else if (eventData.type === 'reloadPage') {
          location.reload();
        }
      }
    }
  }
}

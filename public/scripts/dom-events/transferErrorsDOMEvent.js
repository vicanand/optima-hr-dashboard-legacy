import Storage from '../common/webStorage';
import ServiceConnector from '../common/serviceConnector';
import URL from '../common/urls';
import Util from '../common/util';


let showDetails = function(){
    $('.errors-entries-type-cnt').hide();
    $('.errors-entries-type').removeClass('active');
    $(this).parent().find('.errors-entries-type-cnt').show();
    $(this).parent().find('.errors-entries-type').addClass('active');
};

let cancelOrder = function(){
    $('#cancelLoader').modal("show");
    let url = URL.getCorpApiUrl().CANCEL_ERROR_STATE_ORDER
            .replace(':COMPANY_ID:',$("meta[name='companyID']").attr("content"))
            .replace(':ORDER_ID:',$('#orderID').val())
            .replace(':authToken:',Storage.get('authToken'));
    let inputObj = {
        "corpID" : $("meta[name='corpID']").attr("content")
    }
    ServiceConnector.post(inputObj, url, {}).then(function(respData){
        $('#cancelLoader').modal("hide");
        $('#cancelOrderSuccess').modal({
            backdrop: 'static',
            keyboard: false
        });
    },function(respErr){
        $('#cancelLoader').modal("hide");
        $('#myModalOops').modal("show");
    });

};

let initiateTransferOrder = function(){
    $('#myModalLoader').modal("show");
    let url = URL.getOMSApiUrl().PROCESS_ERROR_STATE_ORDER
            .replace(':authToken:',Storage.get('authToken'));
    let inputObj = {
        "corporateID" : $("meta[name='corpID']").attr("content"),
        "companyID" : $("meta[name='companyID']").attr("content"),
        "orderID" : $('#orderID').val()
    };
    ServiceConnector.post(inputObj, url, {}).then(function(respData){
        $('#myModalLoader').modal("hide");
        $('#orderInitiateSuccess').on('show.bs.modal', function(e) {
            $('#newOrderID').html(respData.orderID);
            if(respData.scheduledTransferDate){
                $('#newScheduleDate').html(Util.timeStampToDate(new Date(respData.scheduledTransferDate).getTime()));
            }
            else{
                $('#newScheduleDate').parent().hide();
            }
            $('#totalBeneficiaries').html(respData.totalScheduledBeneficiaries);
            $('#totalAmt').html(respData.amount);
            $('#newStatus').html(respData.orderStatus)
            if(respData.orderStatus == 'scheduled'){
                $('#newStatus').addClass('status-scheduled');
            }
            else if(respData.orderStatus == 'stalled'){
                $('#newStatus').addClass('status-stalled');
            }
        });
        $('#orderInitiateSuccess').modal({
            backdrop: 'static',
            keyboard: false
        });
    },function(respErr){
        $('#myModalLoader').modal("hide");
        $('#myModalOops').modal("show");
    });
}

export default {
    showDetails: showDetails,
    cancelOrder: cancelOrder,
    initiateTransferOrder : initiateTransferOrder
};

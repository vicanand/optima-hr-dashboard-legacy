import PointbasedSvc from '../../service/spotlight/pointbasedSvc';
import PointbasedCtrl from '../../controller/spotlight/pointbasedCtrl';
import Storage from '../../common/webStorage';
import Meta from '../../common/metaStorage';


export default {

    showMorePrograms:function(){
        $('#showMorePrograms').hide();
        let pageNo = $(this).data('pageno');        
        let companyID = $("meta[name='companyID']").attr("content");
        PointbasedSvc.loadActiveProgramsList(pageNo).then(function(respData){               
            $('#showMorePrograms').data('pageno',pageNo+1);    
            respData.programDetail.length != 10 ? $('#showMorePrograms').hide() : $('#showMorePrograms').show();  
            let appendData = '';
            for(let i=0;i<respData.programDetail.length;i++){
                appendData +=  `<div class="col-lg-3 col-md-3 col-sm-3 padding-left-5">                                                    
                    <div class="pointCardProgram">
                        <a href="/companies/${companyID}/spotlight/pointbased/employee/${respData.programDetail[i].programID}/${respData.programDetail[i].name}/pointsprogram#overview" >
                            <div class="pointCardImage"><img src="https://card-program-files.s3.amazonaws.com/hrDashboard/images/pointbasedRr.svg" /></div>
                            <div class="pointCardName">${respData.programDetail[i].name}</div>
                            <div class="pointCardCurrTxt">Currency: ${respData.programDetail[i].currencyName}</div>
                            <div class="pointCardCurrTxt mg-bottom-thirty">No. of Beneficiaries: <span>${respData.programDetail[i].noOfBeneficiary}</span></div>
                        <a href="" ></a>
                    </div>
                 </div>`
            }            

            $('.appendActivePrograms').append(appendData);

        },function(error){
            $('#showMorePrograms').show();

        })
    },

    validateFile: function () {
        if (PointbasedCtrl.setFlag() != 0) {
            let refValue = $('#scheduleTransferDate').val(),
                fundingAcc = $('#fundingAccountID').val();
            if (fundingAcc !== "0") {
                $('#order-verify-file').removeAttr('disabled').removeClass('btn-disabled');
            } else {
                $('#order-verify-file').attr('disabled', 'disabled').addClass('btn-disabled');
            }
        }
    },

    createGiftCheckbox: function () {
        if (($(this).is(':checked')) && ($('#fundingAccountID option:selected').val() != 0)) {
            $('#orderCompleted').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
        } else {
            $('#orderCompleted').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
        }
    },

    issuePointsVerify: function () {
        $('#showBtnLoader').show();        
        $('#order-verify-file').attr('disabled',true);
        var filename = Storage.get('filename');
        var fileurl = Storage.getSessionCollection('fileurl');
        var refName = $('#reference-name').val();
        localStorage.setItem('refName', refName);
        PointbasedSvc.orderVerifyIssuePoint(filename, fileurl, refName).then(function (respData) {
            $('#order-verify-file').attr('disabled',false);
            let data = respData;
            if (data.errorSet && data.errorSet.length) {
                let headers = data.errorHeader;
                headers.push("Error");
                showErrorTable(data, headers, 'points');
            } else {
                showSummaryTable(data);
                $('#uploadFileCntr').hide();
                $('#createOrderShow').show();
                sessionStorage.setItem('pointrequestID', data.requestID);
                sessionStorage.setItem('pointPayload', JSON.stringify(data));
            }
            console.log(respData);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            } else {
                createULlist(error.responseJSON);
                // $('.displayErrorMsg').text(error.responseJSON.message)
            }            
            $('#createOrderModalSGOops').modal('show');
            $('#order-verify-file').attr('disabled',false);
            console.log(error);
        });
    },

    createBudget: function () {
        let requestID = sessionStorage.getItem('pointrequestID');
        $('#orderCompleted').attr('disabled',true);
        PointbasedSvc.createBudgetSvc(requestID).then(function (respData) {           
            $('.showBtnLoader').hide();
            $('#orderCompleted').attr('disabled',false);            
            $('#orderId').html(respData.fileUploadOrderID);
            $('#totalBeneficiary').html(respData.totalBeneficiaries);
            $('#totalRewards').html(respData.totalAwardsIssued);
            $('#totalAmount').html(respData.value.amount/100);
            $('#show-success-modal').modal('show');
            $('#orderCompleted').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            } else {
                createULlist(error.responseJSON);
            }
            $('#orderCompleted').attr('disabled',false);
            $('.showBtnLoader').hide();
            $('#createOrderModalSGOops').modal('show');
            initGAEvent('event', 'Point Based Gift Card', 'click', 'Creat bulk order Error , corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());
        });
    },

    uploadBeneficiary: function () {
        $('#showBtnLoader').show();
        $('#create-order').attr('disable', true).css('background', '#e9e9e9');
        var filename = Storage.get('filename');
        var fileurl = Storage.getSessionCollection('fileurl');
        var refName = $('#reference-name').val();
        localStorage.setItem('refName', refName);
        PointbasedSvc.upsertEmployees(filename, fileurl, refName).then(function (respData) {
            let data = respData;
            if (data.errorSet && data.errorSet.length) {
                let headers = data.errorHeader;
                headers.push("Error");
                showErrorTable(data, headers, 'beneficiary');
            } else {
                $('#show-success-modal').modal('show');
            }            
            console.log(respData);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            } else {
                createULlist(error.responseJSON);
                // $('.displayErrorMsg').text(error.responseJSON.message)
            }            
            $('#createOrderModalSGOops').modal('show');
            console.log(error);
        });
    },

    uploadAgain: function () {
        $('#fileErrorWrpr').hide();
        $('#uploadFileCntr').show();
    },

    revokeBadgesCall: function () {
        let issuerEmployeeID = Storage.get('issuerEmployeeID');
        let programID = Storage.get('programID');
        let badgeCountMap = Storage.get('badgeCountMap');
        PointbasedSvc.revokebadges(issuerEmployeeID, programID, badgeCountMap).then(function (respData) {
            $('#revokeBadgePayoutModal').modal('hide');
            $('#revokeid').html(respData.revokeID);
            $('#amountRevoked').html((respData.totalAmount)/100);
            $('#pointsRevoked').html(respData.totalPoints);
            $('#revokeBadgePayoutModalSuccess').modal('show');

        }, function (error) {
            $('#revokeBadgePayoutModal').modal('hide');
            $('#revokeBadgesPayoutError').modal('show');
        });
        Storage.remove('issuerEmployeeID');
        Storage.remove('programID');
        Storage.remove('badgeCountMap');         
    }

};

function showSummaryTable(data) {
    let table = ''; 
    let quantity = 0;   

    for(let i in data.badgeTypeSegregationDataCountMap)    {
        quantity += data.badgeTypeSegregationDataCountMap[i];
        table += `<tr>
        <td>${i}</td>
        <td style="text-align:  right;">${data.badgeTypeSegregationDataCountMap[i]}</td>
        <td style="text-align:  right;">${data.badgeTypeSegregationDataAmountMap[i].amount/100}</td>
    </tr>`
    }
    table += `<tr>
                <td>Total</td>
                <td style="text-align:  right;">${quantity}</td>
                <td style="text-align:  right;">${data.value.amount/100}</td>
             </tr>`

    $('.appendSummary').append(table);
}

function showErrorTable(data,header, type) {
    try{
    $('#fileErrorWrpr').show();
    $('#uploadFileCntr').hide();
    $('#uploadAgain').html('UPLOAD AGAIN');
    $('.file-name').html(localStorage.getItem('filename'));
    var fileName = localStorage.getItem('filename');
    fileName = fileName.substring(fileName.lastIndexOf(".") + 1);
    switch (fileName.toLocaleLowerCase()) {
        case "csv":
            $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-12.png");
            break;
        case "xlsx":
            $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xlsx.png");
            break;
        case "xls":
            $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
            break;
        default:
            $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
            break;
    }    
    var rows = [];
    var m = data.errorRows;
    var n = data.errors.sort((a,b) => a[0] - b[0])
    for (let i in m) {
        rows[i] = {
            row: m[i],
            errors: {},
        };
    }

    for (let j = 0; j < n.length; j++) {        
        let r = n[Number(j)][0];
        let k = n[Number(j)][1];
        rows[r].errors[k] = n[j][2];
    }
  
    let responseData = rows.filter((e) => e != null);
    // let columns = rows[0].row.length;
    $('#noOfInvalidEntries').html(responseData.length);
    //Build an array containing Customer records.
    var customers = new Array();
    customers.push(header);
    for (var i = 0; i < responseData.length; i++) {
        let td = responseData[i].row;
        td.push(allErrors(responseData[i]));
        console.log(td);
        customers.push(td);
    }

    function allErrors(error) {
        let slash = '<br/>'
        let errorsReturn = '';
        Object.values(error.errors).forEach(a => {
            errorsReturn += a + slash;
        })       
        return errorsReturn;
    }

    //Create a HTML Table element.
    var table = $("<table class='custom-table hover-state error-table custom-table-error custom-table-rnr' />");
    table[0].border = "0";

    //Get the count of columns.
    var columnCount = customers[0].length;

    //Add the header row.
    var row = $(table[0].insertRow(-1));
    for (var i = 0; i < columnCount; i++) {
        var headerCell = $("<th />");
        headerCell.html(customers[0][i]);
        row.append(headerCell);
    }

    //Add the data rows.
    for (var i = 1; i < customers.length; i++) {
        row = $(table[0].insertRow(-1));
        for (var j = 0; j < columnCount; j++) {
            if (responseData[i - 1].errors[j] == undefined) {
                var cell = $("<td class='' />");
                cell.html(customers[i][j]);
                row.append(cell);
            } else {
                var cell = $("<td class='error-column' />");
                cell.html(customers[i][j]);
                row.append(cell);
            }
        }
    }

    var dvTable = $("#errorSelectGiftingTransfers");
    dvTable.html("");
    dvTable.append(table);
    $('#fileErrorWrpr').show();
}
catch(e){
    $('#createOrderModalSGOops').modal('show');
}
}



function createULlist(data) {
    try {
        $('.header-error-msg').text('Invalid or missing headers.');
        let allErrors = data.message;
        if(allErrors.toLocaleLowerCase() === 'insufficient funds') {
            $('.header-error-msg').text('Error while processing the request.');
            $('.displayErrorMsg').html('<p>Insufficient Funds. Please add or choose another account');
        } else {
            allErrors = allErrors.slice(0, allErrors.indexOf(']') + 1);
            if(allErrors === ""){
                $('.header-error-msg').text('Error while processing the request.')
                $('.displayErrorMsg').hide();
            } else {
                allErrors = allErrors.replace('[','').replace(']','');
                allErrors = allErrors.split(',');    
                allErrors = allErrors.map((e, index) => `<li><strong> ${e}</strong></li>`)
                console.log('allErrors', allErrors);
                $('.appendError').html(allErrors.join().replace(/,/g,''));
                $('.displayErrorMsg').show();
            }        
        }
    } catch(e) {
        $('.header-error-msg').text('Error while processing the request.')
        $('.displayErrorMsg').hide();
    }
}

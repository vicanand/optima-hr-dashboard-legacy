import VirtualSvc from '../../service/spotlight/virtualSvc';
import DomEventHandler from '../../common/domEventHandler.js';
import LocalConstants from '../../common/constants';
import ServiceConnector from '../../common/serviceConnector';
import Storage from '../../common/webStorage.js';
import Util from '../../common/util'
import { parse } from 'query-string';



let validateExistingUser = function() {
    let email = $('#email').val()
    let corpUserList = Storage.getCollection(LocalConstants.STORAGE_CORP_ACCOUNTS);



    var pos = corpUserList.indexOf(email);
    if (pos >= 0) {
        return false;
    } else {
        return true;
    }


    // if(corpUserList.includes(email)){
    //     return false;
    // }
    // else{
    //     return true;
    // }
};

let validateUserName = function() {
    if ($('#userName').val().length == 0) {
        $('#userName').addClass('input-has-error');
        $('#userName').parents('.form-group').find('.error-message').html('Please enter a User Name');
        return false;
    } else {
        $('#userName').removeClass('input-has-error');
        $('#userName').parents('.form-group').find('.error-message').html('');
        return true;
    }
};

let validateEmail = function() {
    let emailField = $('#email').val();
    let emailFieldValidity = validateEmailRegex(emailField);
    if (emailField.length == 0) {
        $('#invalidEmail').hide();
        $('#validEmail').hide();
        $('#email').addClass('input-has-error');
        $('#email').parents('.form-group').find('.error-message').html('Please enter a email address.');
    } else {      
            if(emailFieldValidity){
                $('#invalidEmail').hide();
                $('#email').removeClass('input-has-error');
                $('#email').parents('.form-group').find('.error-message').html('');                
            } else{
                $('#invalidEmail').show();
                $('#email').addClass('input-has-error');
                $('#email').parents('.form-group').find('.error-message').html('Please enter a valid email address.');
            }           
        }
    };

let validateConfirmEmail = function() {
    let matchValidation = validateEmailMatch();
    let confirmEmail = $('#confirmEmail').val();

    if ((matchValidation == true) && (confirmEmail.length > 3)) {
        $('#invalidConfirmEmail').hide();
        $('#validConfirmEmail').show();
        $('#confirmEmail').removeClass('input-has-error');
        $('#confirmEmail').parents('.form-group').find('.error-message').html('');
    } else if ((matchValidation == false) && (confirmEmail.length > 3)) {
        $('#validConfirmEmail').hide();
        $('#invalidConfirmEmail').show();
        $('#confirmEmail').addClass('input-has-error');
        $('#confirmEmail').parents('.form-group').find('.error-message').html('Email address does not match');
    }
}

let validateEmailMatch = function() {
    let emailField = $('#email').val();
    let emailFieldValidity = validateEmailRegex(emailField);
    let confirmEmailField = $('#confirmEmail').val();

    if ((emailField.length != 0) && (confirmEmailField.length != 0) && (emailField == confirmEmailField) && (emailFieldValidity == true)) {
        return true;
    } else {
        return false;
    }
};

let mobileValidation = function(isMobile = true) {
    if(!isMobile){        
        $('#mobile').addClass('input-has-error');
        $('#mobile').parents('.form-group').find('.error-message').html('Please enter a Mobile Number');
        return false;
    }else{
        $('#mobile').removeClass('input-has-error');
        $('#mobile').parents('.form-group').find('.error-message').html('');
        return true;
    }
}

let validateFields = function() {
    let ifi = $('#SpotlightIFIName').val();
    $('#show-error-msg').html('');
    let userName = $('#userName').val();
    let emailField = $('#email').val();
    let mobile = $('#mobile').val().toString().length == 10 ? true : false;
    let emailormobile = $('#emailormobile').val();
    let userNameValidation = validateUserName(userName);    
    let emailValidation = validateEmailRegex(emailField);    
   
    if(ifi.toLowerCase() === 'hdfc'){
        mobileValidation(mobile);
        validateEmail()
        if ((userNameValidation == true) && (emailValidation == true) && (mobile == true)) {
            $('#addNewUserBtn').removeClass('btn-disabled');
            $('#addNewUserBtn').prop('disabled', false);
            return true;
        }else{
            $('#reissuePayoutDOM').removeClass('btn-disabled');
            $('#reissuePayoutDOM').prop('disabled', false);
            $('#addUserLoader').hide();
            throw new Error('all inputs are required');
        }       
    } else {
        if(Number(emailormobile)){
            let mobileno = $('#emailormobile').val().toString().length == 10 ? true : false;
            if(!mobileValidation(mobileno)){
                $('#emailormobile').addClass('input-has-error');
                $('#emailormobile').parents('.form-group').find('.error-message').html('Please enter a Mobile Number');
                $('#reissuePayoutDOM').removeClass('btn-disabled');
                $('#reissuePayoutDOM').prop('disabled', false);
                $('#addUserLoader').hide();
                throw new Error('all inputs are required');
            } else{
                $('#emailormobile').removeClass('input-has-error');
                $('#emailormobile').parents('.form-group').find('.error-message').html('');
            }
        }else{
            if(!validateEmailRegex(emailormobile)){
                $('#emailormobile').addClass('input-has-error');
                $('#emailormobile').parents('.form-group').find('.error-message').html('Please enter a Email Address');
                $('#reissuePayoutDOM').removeClass('btn-disabled');
                $('#reissuePayoutDOM').prop('disabled', false);
                $('#addUserLoader').hide();
                throw new Error('all inputs are required');
            }else{
                $('#emailormobile').removeClass('input-has-error');
                $('#emailormobile').parents('.form-group').find('.error-message').html('');
            }
        }


    }
    
};

let validateEmailRegex = function(fieldValue) {
    let regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regExp.test(fieldValue);
};

let reissue = function() {   
    $('#reissuePayoutDOM').addClass('btn-disabled');
    $('#reissuePayoutDOM').prop('disabled', true);
    $('#addUserLoader').show();
    $('#show-error-msg').css('opacity',0);

    let name = $('#userName').val();
    let email = $('#email').val();
    let phone = $('#mobile').val();
    let comments = $('#reason').val();
    let corpID = $("meta[name='corpID']").attr("content");
    let companyID = $("meta[name='companyID']").attr("content");
    let orderID = $('#get-oreder-id').html();
    let ifi = $('#SpotlightIFIName').val();
    let emailormobile = $('#emailormobile').val();

    if(ifi.toLowerCase() === 'hdfc'){
        phone = $('#mobile').val();
        email = $('#email').val();
        validateFields('hdfc');
    } else {
        validateFields('other');
        if(Number(emailormobile)){
            phone = $('#emailormobile').val();
        }else{
            email = $('#emailormobile').val()            
        }        
    }

    VirtualSvc.reissePayout(name,email,phone,comments,corpID,companyID).then(function(responseData) {        
        $('#reissuePayoutModalShow').modal('hide');              
        $('#reissuePayoutSuccess').modal({
            backdrop: 'static',
            keyboard: false
        });
        
        $('#reissuePayoutDOM').removeClass('btn-disabled');
        $('#reissuePayoutDOM').prop('disabled', false);
        $('#addUserLoader').hide();

        $('#reissueOrderId').html(orderID);
        $('#reissueName').html(name);
        
        if(ifi.toLowerCase() === 'hdfc'){
            $('#reissueEmail').html(email);
            $('.reissueEmail').show();
            $('#reissueMobile').html(phone);
            $('.reissueMobile').show();
        }else{
            $('#reissueEmailorMobile').html($('#emailormobile').val());
            $('.reissueEmailorMobile').show();
        }                

        $('#reissuePayoutSuccess').modal('show');

    }, function(responseError) {              
        $('#reissuePayoutDOM').removeClass('btn-disabled');
        $('#reissuePayoutDOM').prop('disabled', false);
        $('#addUserLoader').hide();
        if(responseError.responseJSON){
            if(responseError.responseJSON.message === 'ERR_REQUEST_FAILED'){
                $('#show-error-msg').css('opacity',1).html('Oops. Re-issue Gift is failed. Please try again');
            }else{
                $('#show-error-msg').css('opacity',1).html(responseError.responseJSON.message);
            }            
        }else{
            $('#show-error-msg').css('opacity',1).html('Oops. Re-issue Gift is failed. Please try again');
        }        
    });
};

let reloadPage = function() {
    window.location.reload();
};


export
default {
    reissue: reissue,    
    validateUserName: validateUserName,
    validateExistingUser: validateExistingUser,
    validateFields: validateFields,
    mobileValidation:mobileValidation,
    validateEmail: validateEmail,
    validateConfirmEmail: validateConfirmEmail,
    validateEmailRegex: validateEmailRegex,
    validateEmailMatch: validateEmailMatch,    
    reloadPage: reloadPage
};

import VirtualSvc from '../../service/spotlight/virtualSvc';
import VirtualCtrl from '../../controller/spotlight/virtualCtrl';
import Storage from '../../common/webStorage';
import Meta from '../../common/metaStorage';
import Env from '../../common/env';
import Constant from '../../common/constants';
import Util from '../../common/util';

var manual, bulk = false;

const funcs = {


    selectProgramCode: function () {
        $('.rewardGift').removeClass('active');
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
        let cardprogramid = $(this).data('cardprogramid');
        let id = $(this).data('id');
        let fundingaccountid = $(this).data('fundingaccountid');
        let cardname = $(this).data('cardname');
        let templategroupid = $(this).data('templategroupid');
        let img = $(this).data('img');
        let message = $(this).data('message');
        let greeting = $(this).data('greeting');
        Storage.setSessionCollection('cardprogramid', cardprogramid);
        Storage.setSessionCollection('id', id);
        Storage.setSessionCollection('fundingaccountid', fundingaccountid);
        Storage.setSessionCollection('cardname', cardname);
        Storage.setSessionCollection('templategroupid', templategroupid);
        Storage.setSessionCollection('img', img);
        Storage.setSessionCollection('message', message);
        Storage.setSessionCollection('greeting', greeting);
        Storage.set('duplicateManual', false);
        Storage.set('duplicateBulk', false);

        initGAEvent('event', 'Vitual Gift Card', 'click', 'Template Selected, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', cardname=' + cardname + ', cardprogramid=' + cardprogramid + ',timestamp=' + new Date().toLocaleString());

        $('.rewardPImg').attr('src', img);
        $('.rewardPEImg').attr('src', img);
        $('.rewardPtitle').html(cardname);
        $('.rewardPEtitle').html(greeting);
        $('.rewardPEmessage').html(message);
        $('.rewardBlankState').hide();
        $('.rewardMobileDemo').show();
        $('.rewardEmailDemo').show();
        if (cardprogramid == 0 && fundingaccountid == 0) {
            $('#card-selection-loader').modal('show');
            VirtualSvc.createNewProgram(id, greeting, message, cardname, img, templategroupid).then(function (respData) {
                console.log(respData);
                cardprogramid = respData.instagiftProgram.corpbenProgramID;
                fundingaccountid = respData.instagiftProgram.fundingAccountID;
                Storage.setSessionCollection('cardprogramid', cardprogramid);
                Storage.setSessionCollection('fundingaccountid', fundingaccountid);
                $('#createOrderDone').attr('data-fundingaccountids', fundingaccountid);
                $('#rewardTemplateSelected').removeAttr('disabled').removeClass('btn-disabled');
                $('#card-selection-loader').modal('hide');
                initGAEvent('event', 'Vitual Gift Card', 'click', 'set up new program , corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', cardname=' + cardname + ', cardprogramid=' + cardprogramid + ',timestamp=' + new Date().toLocaleString());

            }, function (respErr) {
                console.log(respErr);
                $('#card-selection-loader').modal('hide');
                $("#createOrderModalSGOops").modal("show");
                $('#rewardTemplateSelected').attr('disabled', 'disabled').addClass('btn-disabled');
                initGAEvent('event', 'Vitual Gift Card', 'click', 'set up new program order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', cardname=' + cardname + ', respErr=' + respErr + ',cardprogramid=' + cardprogramid + ',timestamp=' + new Date().toLocaleString());
            });
        } else {
            $('#card-selection-loader').modal('hide');
            $('#rewardTemplateSelected').removeAttr('disabled').removeClass('btn-disabled');

        }

    },

    createNxtBulk: function () {
        manual = false;
        bulk = true;
        var allowDuplicates;
        Storage.setSessionCollection('manual', manual);
        Storage.setSessionCollection('bulk', bulk);
        if (Storage.get('duplicateBulk') == 'true') {
            allowDuplicates = true;
        } else {
            allowDuplicates = false;
        }
        console.log($(this));
        var filename = Storage.get('filename');
        var key = Storage.getSessionCollection('key');
        var fileurl = Storage.getSessionCollection('fileurl');

        $('#fundingAccountID option:selected').val() == 0 ? $('.fundCurrntBalance').hide() : $('.fundCurrntBalance').show();
        $('#orderCompletedNew').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');


        $('.create-order-spinner').show();
        $('#create-order').attr('disabled', true).css('background', '#ccc');

        // $('#card-selection-loader').modal('show');
        VirtualSvc.orderVerifyBulk(filename, fileurl, key, allowDuplicates).then(function (respData) {
            $('#totalBenefits').html(respData.totalPayout);
            $('#totalAmount').html("<b>₹</b> " + (respData.totalAmount / 100));
            Storage.setSessionCollection('requestID', respData.requestID);
            Storage.setSessionCollection('bulkOrder', true);
            Storage.setSessionCollection('manualOrder', false);
            Storage.setSessionCollection('requestID', respData.requestID);

            $('#card-selection-loader').modal('hide');
            $('.create-order-spinner').hide();
            $('#create-order').attr('disabled', false).css('background', '#ffcb39')
            $('#uploadStep').addClass('done');
            $('#verifyStep').addClass('active');
            $('#addBenefitsBulk').hide();
            $('#addBenefits').hide();
            $('#createOrder').show();
            $('#fileErrorWrpr').hide();
            $('#virtualGiftContainer').show();


            var fundingaccountidX = Storage.getSessionCollection('fundingaccountid');
            $('#fundingAccountID option[value=' + fundingaccountidX + ']').attr('selected', 'selected');
            var newBalance = $('#fundingAccountID option:selected').attr('data-balance');
            var fundingaccountname = $('#fundingAccountID option:selected').attr('data-name');
            Storage.setSessionCollection('fundingaccountbalance', newBalance);
            Storage.setSessionCollection('fundingaccountname', fundingaccountname);
            $('#fundingAccountID').on('change', function () {
                var newBalance = $('#fundingAccountID option:selected').attr('data-balance');
                if (newBalance < respData.totalAmount / 100) {
                    $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').show();
                    $('.placeOrderEnd').hide();
                } else {
                    $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').hide();
                    $('.placeOrderEnd').show();
                }


                $('#fundingAccountID option:selected').val() == 0 ? $('.fundCurrntBalance').hide() : $('.fundCurrntBalance').show();
                if (($('#createGiftCheckbox').is(':checked')) && ($('#fundingAccountID option:selected').val() != 0)) {
                    $('#orderCompleted').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                    $('#orderCompletedNew').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                    $('.fundCurrntBalance').show();
                    $('#balanceFund').html(newBalance);
                } else {
                    $('#orderCompleted').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                    $('#orderCompletedNew').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                }
            });

            $('#createGiftCheckbox').on('click', function () {
                if (($(this).is(':checked')) && ($('#fundingAccountID option:selected').val() != 0)) {
                    $('#orderCompleted').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                    $('#orderCompletedNew').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                } else {
                    $('#orderCompleted').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                    $('#orderCompletedNew').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                }
            });
            initGAEvent('event', 'Vitual Gift Card', 'click', 'Verify bulk order , corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', fundingaccountid=' + fundingaccountidX + ', filename=' + filename + ',fileurl=' + fileurl + ',timestamp=' + new Date().toLocaleString());
        }, function (respErr) {

            console.log(respErr);
            // $('#virtualGiftContainer').hide();
            $('.create-order-spinner').hide();
            $('#create-order').attr('disabled', false).css('background', '#ffcb39')
            $('#card-selection-loader').modal('hide');
            if (respErr) {
                if (respErr.status == 500) {
                    $('#server-error-oops').modal('show');
                    return;
                }
            }
            initGAEvent('event', 'Vitual Gift Card', 'click', 'Verify bulk order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', fundingaccountid=' + fundingaccountidX + ', respErr=' + respErr + ',fileurl=' + fileurl + ',timestamp=' + new Date().toLocaleString());
        });

    },

    createNxtManual: function () {
        manual = true;
        bulk = false;
        var allowDuplicates;
        Storage.setSessionCollection('manual', manual);
        Storage.setSessionCollection('bulk', bulk);
        if (Storage.get('duplicateManual') == 'true') {
            allowDuplicates = true;
        } else {
            allowDuplicates = false;
        }

        $('#spot-show-loader').show();
        $('#rewardSingleSelected').attr('disabled', true).css('background', '#ccc')
        $('#card-selection-loader').modal('hide');
        var arrayBeneficiary = [];
        let csvContent = [];
        let fileUploadUrl;
        $('.rewardBenefitInputDv').each(function () {
            var beneficiaryObj =
                $('#SpotlightIFIName').val() == 'HDFC' ? {
                    "name": Util.htmlEntities($(this).find('input.inputField:eq(0)').val()),
                    "amount": $(this).find('input.inputField:eq(1)').val(),
                    "contact": $(this).find('input.inputField:eq(4)').val(),
                    "email": Util.htmlEntities($(this).find('input.inputField:eq(3)').val()),
                    "transferDate": $(this).find('input.inputField:eq(2)').val(),
                } : {
                    "name": Util.htmlEntities($(this).find('input.inputField:eq(0)').val()),
                    "amount": $(this).find('input.inputField:eq(1)').val(),
                    "emailOrMobile": Util.htmlEntities($(this).find('input.inputField:eq(2)').val()),
                    "transferDate": $(this).find('input.inputField:eq(3)').val()
                };
            arrayBeneficiary.push(beneficiaryObj);
        });

        for (var i = 0; i < arrayBeneficiary.length; i++) {
            for (var key in arrayBeneficiary[i]) {
                if (arrayBeneficiary[i][key] == "") {
                    $('#spot-show-loader').hide();
                    $('#rewardSingleSelected').attr('disabled', false).css('background', '#ffcb39')
                    $("#error-required-oops").modal("show");
                    return;
                }
            }
        }
        $('#fundingAccountID option:selected').val() == 0 ? $('.fundCurrntBalance').hide() : $('.fundCurrntBalance').show();
        $('#orderCompletedNew').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
        Storage.setSessionCollection('arrayBeneficiary', arrayBeneficiary);
        VirtualSvc.orderVerifyManual(arrayBeneficiary, allowDuplicates).then(function (respData) {
            $('#spot-show-loader').hide();
            $('#rewardSingleSelected').attr('disabled', false).css('background', '#ffcb39');
            $('#totalBenefits').html(respData.totalPayout);
            $('#totalAmount').html("<b>₹</b>" + (respData.totalAmount) / 100);
            Storage.setSessionCollection('requestID', respData.requestID);
            Storage.setSessionCollection('bulkOrder', false);
            Storage.setSessionCollection('manualOrder', true);
            Storage.setSessionCollection('requestID', respData.requestID);
            $('#card-selection-loader').modal('hide');
            $('.spinner-loader').hide();
            $('#uploadStep').addClass('done');
            $('#verifyStep').addClass('active');
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#fileErrorWrpr').hide();
            $('#virtualGiftContainer').show();
            $('#createOrder').show();

            var fundingaccountidX = Storage.getSessionCollection('fundingaccountid');
            $('#fundingAccountID option[value=' + fundingaccountidX + ']').attr('selected', 'selected');
            var newBalance = $('#fundingAccountID option:selected').attr('data-balance');
            var fundingaccountname = $('#fundingAccountID option:selected').attr('data-name');
            Storage.setSessionCollection('fundingaccountbalance', newBalance);
            Storage.setSessionCollection('fundingaccountname', fundingaccountname);
            $('#fundingAccountID').on('change', function () {
                var newBalance = $('#fundingAccountID option:selected').attr('data-balance');
                $('#balanceFund').html(newBalance);
                if (newBalance < respData.totalAmount / 100) {
                    $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').show();
                    $('.placeOrderEnd').hide();
                } else {
                    $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').hide();
                    $('.placeOrderEnd').show();
                }
                $('#fundingAccountID option:selected').val() == 0 ? $('.fundCurrntBalance').hide() : $('.fundCurrntBalance').show()
                if (($('#createGiftCheckbox').is(':checked')) && ($('#fundingAccountID option:selected').val() != 0)) {
                    $('#orderCompleted').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                    $('#orderCompletedNew').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                    $('.fundCurrntBalance').show()
                    $('#balanceFund').html(newBalance);
                } else {
                    $('#orderCompleted').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                    $('#orderCompletedNew').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                }
            });
            $('#createGiftCheckbox').on('click', function () {
                if (($(this).is(':checked')) && ($('#fundingAccountID option:selected').val() != 0)) {
                    $('#orderCompleted').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                    $('#orderCompletedNew').removeAttr('disabled').removeClass('btn-disabled').css('background', '#FFCB39');
                } else {
                    $('#orderCompleted').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                    $('#orderCompletedNew').attr('disabled', 'disabled').addClass('btn-disabled').css('background', '#ccc');
                }
            });
            initGAEvent('event', 'Vitual Gift Card', 'click', 'Verify manual order  , corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', arrayBeneficiary=' + arrayBeneficiary + ',timestamp=' + new Date().toLocaleString());
            $('#spot-show-loader').hide();
            $('#rewardSingleSelected').attr('disabled', false).css('background', '#ffcb39')

        }).catch(respErr => {
            console.log(respErr);
            $('#card-selection-loader').modal('hide');
            $('#spot-show-loader').hide();
            $('#rewardSingleSelected').attr('disabled', false).css('background', '#ffcb39')
            if (respErr) {
                if (respErr.status == 500) {
                    $('#server-error-oops').modal('show');
                    return;
                }
            }
            if (respErr.responseJSON.attributes.orderVerifyErrorStatesSet.length === 1 && respErr.responseJSON.attributes.orderVerifyErrorStatesSet[0].toLowerCase() === 'duplicate') {
                //var duplicateBulk = true;
                Storage.set('duplicateManual', true);
                $('.duplicateOr, .duplicateEntries').show();
                $('#duplicateEntries').attr('data-url', 'Step3Manual');
            } else {
                Storage.set('duplicateManual', false);
                $('.duplicateOr, .duplicateEntries').hide();
            }
            $('#rewardSingleSelected').attr('disabled', false).css('background', '#ffcb39')
            // $("#createOrderModalSGOops").modal("show");
            $('#virtualGiftContainer').hide();
            $('#addBenefits').hide();
            $('#errorVirtualGiftingTransfers').hide();
            if (respErr.responseJSON.attributes.fileProcessingSummary) {
                let responseData = respErr.responseJSON.attributes.rows;
                $('#noOfInvalidEntries').html(respErr.responseJSON.attributes.fileProcessingSummary.invalidRows);
                $('.file-details-cntr').hide();


                //Build an array containing Customer records.
                var customers = new Array();
                var ifi = $("meta[name='ifi']").attr('content');
                if (ifi == "HDFC") {
                    customers.push(["Name", "Email", "Mobile", "Amount (₹)", "Transfer Date", "Reason"]);
                    for (var i = 0; i < responseData.length; i++) {
                        customers.push([responseData[i].row[0], responseData[i].row[1], responseData[i].row[2], responseData[i].row[3], responseData[i].row[4], allErrors(responseData[i])]);
                    }
                } else {
                    customers.push(["Name", "Email or Mobile", "Amount (₹)", "Transfer Date", "Reason"]);
                    for (var i = 0; i < responseData.length; i++) {
                        customers.push([responseData[i].row[0], responseData[i].row[1], responseData[i].row[2], responseData[i].row[3], allErrors(responseData[i])]);
                    }
                }

                function allErrors(error) {
                    let slash = '<br/>';
                    let errorsReturn = '';
                    error.errors[0] != undefined ? errorsReturn += error.errors[0][0] + slash : '';
                    error.errors[1] != undefined ? errorsReturn += error.errors[1][0] + slash : '';
                    error.errors[2] != undefined ? errorsReturn += error.errors[2][0] + slash : '';
                    error.errors[3] != undefined ? errorsReturn += error.errors[3][0] + slash : '';
                    error.errors[4] != undefined ? errorsReturn += error.errors[4][0] + slash : '';
                    return errorsReturn;
                }
                //Create a HTML Table element.
                var table = $("<table class='custom-table hover-state error-table custom-table-error' />");
                table[0].border = "0";
                //Get the count of columns.
                var columnCount = customers[0].length;
                //Add the header row.
                var row = $(table[0].insertRow(-1));
                for (var i = 0; i < columnCount; i++) {
                    var headerCell = $("<th />");
                    headerCell.html(customers[0][i]);
                    row.append(headerCell);
                }
                //Add the data rows.
                for (var i = 1; i < customers.length; i++) {
                    row = $(table[0].insertRow(-1));
                    for (var j = 0; j < columnCount; j++) {
                        if (responseData[i - 1].errors[j] == undefined) {
                            var cell = $("<td class='' />");
                            cell.html(customers[i][j]);
                            row.append(cell);
                        } else {
                            var cell = $("<td class='error-column' />");
                            cell.html(customers[i][j]);
                            row.append(cell);
                        }
                    }
                }
                var dvTable = $("#errorVirtualGiftingTransfers1");
                dvTable.html("");
                dvTable.append(table);
                $('#fileErrorWrpr').show();
                $('#uploadAgain').html('RETRY');
                $('#uploadAgain').attr('data-type', 'manual');
            }
            initGAEvent('event', 'Vitual Gift Card', 'click', 'Verify manual order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', arrayBeneficiary=' + arrayBeneficiary + ', error=' + error + ',timestamp=' + new Date().toLocaleString());
        });

    },

    orderCreate: function () {
        $('.spinner-loader').show();
        $('#orderCompletedNew').attr('disable', true).css('background', '#ccc')
        $('#orderCompleted').attr('disable', true).css('background', '#ccc')
        var requestID = Storage.getSessionCollection('requestID');
        var bulkOrder = Storage.getSessionCollection('bulkOrder');
        let corpID = $("meta[name='corpID']").attr("content");
        let authToken = Storage.get('authToken');
        var fundingaccountid = Storage.getSessionCollection('fundingaccountid');
        var fundingAccountBalance = Storage.getSessionCollection('fundingaccountbalance');
        var fundingAccountName = Storage.getSessionCollection('fundingaccountname');
        var requestID = Storage.getSessionCollection('requestID');
        VirtualSvc.getAccDetails(authToken, corpID, fundingaccountid).then(function (fundRespData) {
            let fundingAcc = fundRespData;

            Storage.setSessionCollection('ifiID', (fundingAcc.ifiID));
        }, function (respErr) {
            console.log(respErr);
        });
        if (bulkOrder == true) {

            var fileName = Storage.get('filename');
            var key = Storage.getSessionCollection('key');
            var programId = Storage.getSessionCollection('cardprogramid');
            var fileurl = Storage.getSessionCollection('fileurl');
            VirtualSvc.orderCreateBulk(fundingaccountid, fileName, key, programId, fileurl, requestID).then(function (respData) {
                console.log(respData);
                $('.spinner-loader').hide();
                $('#orderCompletedNew').attr('disable', false).css('background', '#ffcb39')
                $('#orderCompleted').attr('disable', false).css('background', '#ffcb39')
                $('#verifyStep').addClass('done');
                $('#createOrderId').html(respData.orderId);
                $('.createhideIt').hide();
                $('.createhideIt2').show();
                $("#createOrderModalShow").modal("show");
                $('#verifyStep').addClass('done');
                $('#createOrderId').html(respData.orderID);
                $('#createFundName').html(fundingAccountName);
                $('#createFundBalance').html(fundingAccountBalance);
                $('#createTotalAmt').html(respData.totalAmount / 100);
                $('#createOrderDone').data('fundingaccountid', Storage.getSessionCollection('fundingaccountid'));
                let totalAmount = respData.totalAmount;
                $('#createTotalOrderAmt').html(totalAmount / 100);
                if ((totalAmount / 100) > fundingAccountBalance) {
                    $('.createhideIt').show();
                    $('.createhideIt2').hide();
                    let createFundTobAdded = Math.round((totalAmount / 100) - fundingAccountBalance);
                    $('#createFundTobAdded').html(createFundTobAdded);
                }
                Storage.removeItem('arrayBeneficiary');
                Storage.removeItem('cardname');
                Storage.removeItem('cardprogramid');
                Storage.removeItem('greeting');
                Storage.removeItem('message');
                Storage.remove('fileName');
                Storage.remove('storage-filename');
                Storage.remove('totalAmount');
                Storage.remove('totalPayout');
                initGAEvent('event', 'Vitual Gift Card', 'click', 'Create bulk order , corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', fundingaccountid=' + fundingaccountidX + ', filename=' + fileName + ',fileurl=' + fileurl + ',timestamp=' + new Date().toLocaleString());
            }, function (respErr) {
                console.log(respErr);
                $('.spinner-loader').hide();
                $('#orderCompletedNew').attr('disable', false).css('background', '#ffcb39');
                $('#orderCompleted').attr('disable', false).css('background', '#ffcb39');
                $("#createOrderModalLoader").modal("hide");
                $("#createOrderModalSGOops").modal("show");
                initGAEvent('event', 'Vitual Gift Card', 'click', 'Create bulk order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', respErr=' + respErr + ', fileurl=' + fileurl + ',timestamp=' + new Date().toLocaleString());
            });
        } else {
            var arrayBeneficiary = Storage.getSessionCollection('arrayBeneficiary');
            var programId = Storage.getSessionCollection('cardprogramid');
            VirtualSvc.orderCreateManual(arrayBeneficiary, fundingaccountid, programId, requestID).then(function (respData) {
                console.log(respData);
                $('.spinner-loader').hide();
                $('#orderCompletedNew').attr('disable', false).css('background', '#ffcb39')
                $('#orderCompleted').attr('disable', false).css('background', '#ffcb39')
                $('#verifyStep').addClass('done');
                $('#createOrderId').html(respData.orderId);
                $('.createhideIt').hide();
                $('.createhideIt2').show();
                $("#createOrderModalShow").modal("show");
                $('#verifyStep').addClass('done');
                $('#createOrderId').html(respData.orderID);
                $('#createFundName').html(fundingAccountName);
                $('#createFundBalance').html(fundingAccountBalance);
                $('#createTotalAmt').html(respData.totalAmount / 100);
                $('#createOrderDone').data('fundingaccountid', Storage.getSessionCollection('fundingaccountid'));
                let totalAmount = respData.totalAmount;
                $('#createTotalOrderAmt').html(totalAmount / 100);
                if ((totalAmount / 100) > fundingAccountBalance) {
                    $('.createhideIt').show();
                    $('.createhideIt2').hide();
                    let createFundTobAdded = (totalAmount / 100) - fundingAccountBalance;
                    $('#createFundTobAdded').html(createFundTobAdded);
                }
                Storage.removeItem('arrayBeneficiary');
                Storage.removeItem('cardname');
                Storage.removeItem('cardprogramid');
                Storage.removeItem('greeting');
                Storage.removeItem('message');
                Storage.remove('fileName');
                Storage.remove('storage-filename');
                Storage.remove('totalAmount');
                Storage.remove('totalPayout');

                initGAEvent('event', 'Vitual Gift Card', 'click', 'Create manual order  , corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', arrayBeneficiary=' + arrayBeneficiary + ',timestamp=' + new Date().toLocaleString());
            }, function (respErr) {
                console.log(respErr);
                $('.spinner-loader').hide();
                $('#orderCompletedNew').attr('disable', false).css('background', '#ffcb39')
                $('#orderCompleted').attr('disable', false).css('background', '#ffcb39')
                $("#createOrderModalLoader").modal("hide");
                $("#createOrderModalSGOops").modal("show");
                initGAEvent('event', 'Vitual Gift Card', 'click', 'Verify manual order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', arrayBeneficiary=' + arrayBeneficiary + ', respErr=' + respErr + ',timestamp=' + new Date().toLocaleString());
            });
        }


    },

    showBenefitsBulk: function () {
        localStorage.setItem('selected_Bulk', true);
        if ($('#chooseStep').hasClass('done')) {
            $('#chooseDesign').hide();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').show();
        }
    },
    showManually: function () {
        localStorage.setItem('selected_Bulk', false);
        if ($('#chooseStep').hasClass('done')) {
            $('#chooseDesign').hide();
            $('#addBenefits').show();
            $('#addBenefitsBulk').hide();
        }
    },
    showManuallyOnStep: function () {
        if ($('#chooseStep').hasClass('done')) {
            $('#chooseStep').addClass('done');
            $('#uploadStep').addClass('active');
            $('#uploadStep').removeClass('done');
            $('#verifyStep').removeClass('active');
            if (localStorage.getItem('selected_Bulk') == 'true') {
                $('#addBenefits').hide();
                $('#addBenefitsBulk').show();
            } else {
                $('#addBenefits').show();
                $('#addBenefitsBulk').hide();
            }
            $('#chooseDesign').hide();
            $('#createOrder').hide();
        }
    },
    showBenefits: function () {
        // if($('#chooseDesign').hasClass('done')){
        $('#chooseDesign').hide();
        $('#addBenefitsBulk').show();
        $('#addBenefits').hide();
        $('#chooseStep').addClass('done');
        $('#uploadStep').addClass('active');
        var targetUrl = $(this).data('url');
        window.history.pushState({
            url: "" + targetUrl + ""
        }, targetUrl);
        window.location.hash = targetUrl;
        // } 
    },

    showChooseDesign: function () {
        if ($('#chooseStep').hasClass('done')) {
            $('#chooseDesign').show();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#createOrder').hide();
        }
    },

    showVerifyAndPay: function () {
        if ($('#uploadStep').hasClass('done')) {
            $('#chooseDesign').hide();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#createOrder').show();
        }
    },

    addFundsSG: function () {
        $('#createOrderModalShow').modal('hide');
        $('#selectGiftOrderLoader').modal('show');
        let accountId = $(this).attr('data-fundingaccountids');
        let ifiID = Storage.getSessionCollection('ifiID');

        if (accountId == undefined) {
            accountId = Storage.get('ACCOUNT_ID');
            Storage.set('ACCOUNT_ID', accountId);
            Storage.set('ACCOUNT_NAME', Storage.getSessionCollection('fundingaccountname'));
        } else {
            accountId = accountId;
            Storage.set('ACCOUNT_ID', accountId);
            Storage.set('ACCOUNT_ID', accountId);
            Storage.set('ACCOUNT_NAME', Storage.getSessionCollection('fundingaccountname'));
        }
        let corporateId = $("meta[name='corpID']").attr("content");
        let authToken = Storage.get('authToken');
        let headers = {};
        VirtualSvc.getIfiDetails(authToken, corporateId, ifiID).then(function (getIfisData) {
            initGAEvent('event', 'Vitual Gift Card', 'click', 'Add funds , corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', ifiID=' + ifiID + ',timestamp=' + new Date().toLocaleString());
            // console.log(getIfisData)
            let getIfisBankDetails = getIfisData;
            $('#selectGiftOrderLoader').modal('hide');
            if (ifiID == Constant.KOTAK_FUNDING_ACCS[Env.CURR_ENV]) {
                $('#addFundsBtnWrpr,.list-number-circle').hide();
                $('#addFundDetailsHdg').text('Please use convenient channels to pay to the following bank account.');
                $('#ifiAccNo').text(getIfisBankDetails.bankDetails.accountNumber);
                $('#ifiBankName').text('Kotak Mahindra Bank');
                $('#ifiBankBranch').text('MUMBAI-NARIMAN POINT');
                $('#ifiIfscCode').text(getIfisBankDetails.bankDetails.IFSC);
                $('#acc_name strong').text(Storage.getSessionCollection('fundingaccountname'));
                $('#ifiAccName').text(getIfisBankDetails.bankDetails.accountName);
            } else {
                $('#acNameModalWrpr,#addFundsBtnWrpr,.list-number-circle').show();
                $('#addFundDetailsHdg').text('Drop a cheque/transfer to Zeta’s Bank Account');
                $('#ifiAccNo').text(getIfisBankDetails.bankDetails.accountNumber);
                $('#ifiBankName').text(getIfisBankDetails.bankDetails.bankName);
                $('#ifiBankBranch').text(getIfisBankDetails.bankDetails.bankAddress);
                $('#ifiIfscCode').text(getIfisBankDetails.bankDetails.IFSC);
                $('#acc_name strong').text(Storage.getSessionCollection('fundingaccountname'));
                $('#ifiAccName').text(getIfisBankDetails.bankDetails.accountName);
            }
            $('#fundChequeTransferx').modal('show');
            Storage.set('ACCOUNT_NAME', Storage.getSessionCollection('fundingaccountname'));
        }, function (respErr) {
            $('#selectGiftOrderLoader').modal('hide');
            $('#selectGiftErrorLoader').modal('show');
            initGAEvent('event', 'Vitual Gift Card', 'click', 'Add funds error , corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', respErr=' + respErr + ',timestamp=' + new Date().toLocaleString());
        });
    },

    routeToAddFund: function () {
        window.location.href = '/admin/funding-accounts/add-funds-details';
    },


    addNewTemplateCode: function () {
        $('#newTemplateCreate').modal('show');
    },

    validationForTransfer: function () {
        if (VirtualCtrl.setFlag() != 0) {
            $('#create-order').removeAttr('disabled');
            $('#create-order').removeClass('btn-disabled');
        }
    },

    validationForManualTransfer: function () {
        let ifi = $("meta[name='ifi']").attr('content');
        if (ifi.toLowerCase() == 'hdfc') {
            if (($('.input-date-validate').val() != '') && $('#rightHdfcAmount').val() == 'true') {
                $('#rewardSingleSelected').removeAttr('disabled').removeClass('btn-disabled');
            } else {
                $('#rewardSingleSelected').attr('disabled', 'disabled').addClass('btn-disabled');
            }
        } else {
            if (($('.input-date-validate').val() != '')) {
                $('#rewardSingleSelected').removeAttr('disabled').removeClass('btn-disabled');
            } else {
                $('#rewardSingleSelected').attr('disabled', 'disabled').addClass('btn-disabled');
            }
        }
    },

    validateFile: function () {
        if (VirtualCtrl.setFlag() != 0) {
            $('#create-order').removeAttr('disabled');
            $('#create-order').removeClass('btn-disabled');
        }
        $("#myModalSuccess").modal({
            "backdrop": "static",
            "show": true,
            keyboard: false
        });
    },
    retryErrorModal: function () {
        $('#createBulkOrderModalOops').modal('hide');
    },

    prepareGAEvent: function () {
        initGAEvent('event', $(this).attr('id'), 'click', 'Spotlight');
    },


    fundingAccountIDChange: function () {
        let $createProgram = $('#changeAccSG'),
            //$programName = $('#programName'),
            $accountID = $('#fundingAccountID'),
            $showHideNewFundingAcc = $('#shownewfundingaccname'),
            $NewFundingAccName = $('#newfundingaccname');

        if ($accountID.find(":selected").text() == 'Create New') {
            $showHideNewFundingAcc.show();
            $('#changeAccSG').hide();
            $('#setAccSG').show();
            if ($accountID.val() == 0 || !$NewFundingAccName.val() || $NewFundingAccName.val().length > 40) {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
                $('#newfundingaccname').val(null);
            } else {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
                $('#newfundingaccname').val(null);
            }
        } else {
            $showHideNewFundingAcc.hide();
            $('#changeAccSG').show();
            $('#setAccSG').hide();
            if ($accountID.val() == 0) {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
                $('#newfundingaccname').val(null);
            } else {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
                $('#newfundingaccname').val(null);
            }
        }
        initGAEvent('event', 'Vitual Gift Card', 'click', 'funding account change , corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', fundingAccountID=' + $accountID + ',timestamp=' + new Date().toLocaleString());
    },
    newFundingAccountName: function () {
        let $createProgram = $('#setAccSG'),
            $accountID = $('#fundingAccountID'),
            $showHideNewFundingAcc = $('#shownewfundingaccname'),
            $NewFundingAccName = $('#newfundingaccname');

        if ($accountID.val() == 0 || !$NewFundingAccName.val() || $NewFundingAccName.val().length > 40) {
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $createProgram.removeAttr('disabled');
            $createProgram.removeClass('btn-disabled');
        }
        let accountName = $NewFundingAccName.val();
        let allfundingAccountName = JSON.parse(localStorage.getItem('allFundingAccountName'));
        if (allfundingAccountName.indexOf(accountName) > -1) {
            $('#showfundingAccDuplicate').show();
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $('#showfundingAccDuplicate').hide();
        }
    },


    validateEmail: function () {
        console.log('called');

    },

    redirectUrl: function () {
        $('#createOrderModalShow').modal('hide');
        $('#resetFilter').trigger('click');
        $('#chooseDesign').show();
        $('#createOrder').hide();

        window.location.reload();
    },
    duplicateEnrty: function () {
        if (Storage.get('duplicateBulk') == 'true') {
            // var targetUrl = 'Step3Bulk';
            // window.history.pushState({url: "" + targetUrl + ""}, targetUrl);
            // window.location.hash=targetUrl;
            funcs.createNxtBulk();

        } else if (Storage.get('duplicateManual') == 'true') {
            // var targetUrl = 'Step3Manual';
            // window.history.pushState({url: "" + targetUrl + ""}, targetUrl);
            // window.location.hash=targetUrl;
            funcs.createNxtManual();
        }
    },

};
export default funcs;
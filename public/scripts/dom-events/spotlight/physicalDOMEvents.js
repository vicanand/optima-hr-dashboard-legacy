import ServiceConnector from "../../common/serviceConnector";
import PhysicalSvc from '../../service/spotlight/physicalSvc';
import PhysicalCtrl from '../../controller/spotlight/physicalCtrl';
import Storage from '../../common/webStorage';
import FundinSvc from '../../service/fundingSvc';
import Meta from '../../common/metaStorage';
import Env from '../../common/env';
import Constant from '../../common/constants';
import Urls from '../../common/urls';


export default {

    validateFile : function() {
        // validationForTransfer();
         if ( PhysicalCtrl.setFlag() != 0) {            
             let refValue = $('#reference-name').val()
                if(refValue != ''){
                    $('#create-order').removeAttr('disabled').removeClass('btn-disabled');
                } else{
                    $('#create-order').attr('disabled', 'disabled').addClass('btn-disabled');
                }    
         }
         //let productType = $('#currentProductType').val();
             $("#myModalSuccess").modal({
               "backdrop": "static",
                "show": true,
                keyboard: false
             });
     },
     createIssueBulkOrderVerify:function(){
         $('#showBtnLoader').show();
         var allowDuplicates;
        $('#create-order').attr('disable',true).css('background','#e9e9e9');
        var filename = Storage.get('filename');
        var key = Storage.getSessionCollection('key');
        var fileurl = Storage.getSessionCollection('fileurl');
        var refName = $('#reference-name').val();
        localStorage.setItem('refName',refName);
        if(Storage.get('duplicate') == 'true'){
            allowDuplicates = true;
        }
        else{
            allowDuplicates = false;
        }
        PhysicalSvc.orderVerifyBulk(filename,fileurl,key,refName,allowDuplicates).then(function(respData){
            console.log(respData)
            $('#showBtnLoader').hide();
            $('#create-order').attr('disable',false).css('background','#ffbc00');
            localStorage.setItem('totalAmount',respData.totalAmount);
            localStorage.setItem('totalPayout',respData.totalPayout);
            localStorage.setItem('requestID',respData.requestID);
            let host = window.location.host;
            initGAEvent('event', 'Physical Gift Card', 'click', 'Verify bulk order , corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', filename='+ filename +',fileurl='+ fileurl +',timestamp=' + new Date().toLocaleString());
            window.location = 'https://'+ host + '/companies/'+Number($("meta[name='companyID']").attr("content"))+'/spotlight/physical/payout'
        }),function(error){
            console.log(error)
            $('#showBtnLoader').hide();
            $('#create-order').attr('disable',false).css('background','#ffbc00');
            $('#createOrderModalSGOops').modal('show')
            initGAEvent('event', 'Physical Gift Card', 'click', 'Verify bulk order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', filename='+ filename +',fileurl='+ fileurl +',timestamp=' + new Date().toLocaleString());
        }
     },

     createIssueBulkOrder:function(){
        $('.showBtnLoader').show();
        $('#orderCompletedNew').attr('disable',true).css('background','#e9e9e9');
        $('#orderCompleted').attr('disable',true).css('background','#e9e9e9');
        $('#orderCompleted').attr('disabled', 'disabled').addClass('btn-disabled').css('background','#ccc');     
        $('#orderCompletedNew').attr('disabled', 'disabled').addClass('btn-disabled').css('background','#ccc');   


        var fileName = Storage.get('filename');
        var key = Storage.getSessionCollection('key');
        var fileurl = Storage.getSessionCollection('fileurl');
        var refName =  localStorage.getItem('refName')
        var fundingAccountId = $('#fundingAccountID option:selected').val();
        var requestID = localStorage.getItem('requestID');
        var programId = '';
        var requestID = localStorage.getItem('requestID');

        PhysicalSvc.orderCreateBulk(fundingAccountId,fileName,key,programId,fileurl,refName,requestID).then(function(respData){
            $('.showBtnLoader').hide();
            $('#orderCompletedNew').attr('disable',false).css('background','#ffbc00');
            $('#orderCompleted').attr('disable',false).css('background','#ffbc00');
            $('#createOrderId').html(respData.orderID);
            $('#refName').html(refName);
            $('#noOfBenefit').html(respData.totalPayout);
            // $('#totalAmount').html(respData.totalAmount);            
            $('#createOrderModalShow').modal('show');
            $('#orderCompleted').removeAttr('disabled').removeClass('btn-disabled').css('background','#FFCB39');
            $('#orderCompletedNew').removeAttr('disabled').removeClass('btn-disabled').css('background','#FFCB39');    
            initGAEvent('event', 'Physical Gift Card', 'click', 'Creat bulk order , corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', orderID='+ respData.orderID +',refName='+ refName +',timestamp=' + new Date().toLocaleString());     
        }),function(error){          
            $('.showBtnLoader').hide();
            $('#orderCompletedNew').attr('disable',false).css('background','#ffbc00');  
            $('#orderCompleted').attr('disable',false).css('background','#ffbc00');  
            $('#createOrderModalSGOops').modal('show');
            initGAEvent('event', 'Physical Gift Card', 'click', 'Creat bulk order Error , corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+',timestamp=' + new Date().toLocaleString());     
        }
     },
    






};



import Constant from '../../common/constants';
import SelectGiftSvc from '../../service/spotlight/selectgiftSvc';
import ServiceConnector from "../../common/serviceConnector";
import SpotLightCtrl from '../../controller/spotlight/selectgiftCtrl';
import Util from '../../common/util';
import Storage from '../../common/webStorage';
import Env from '../../common/env';
import Meta from '../../common/metaStorage';


   //terms and condition
  var termsAndCondition =  function(brandName) {
      let isExpand = $(this).data('isexpand');
      if(!isExpand){            
          $('.gift-card-terms').slideDown('slow').show();          
          $('.show-hide-terms').data('isexpand',true);
          $('.change-arrow').addClass('active');
          $('.btn-toggle-text').text('Hide terms and conditions');
      }else{
          $('.gift-card-terms').slideUp('slow').show();          
          $('.show-hide-terms').data('isexpand',false);
          $('.change-arrow').removeClass('active');
          $('.btn-toggle-text').text('Show terms and conditions');
      }               
        // e.preventDefault();
        // $('#selectGiftOrderLoader').modal('show');
        // let productId = Storage.getSessionCollection('productId');
        // SelectGiftSvc.getProductDeatils(productId).then(function(res) {
        //     let response = res;
        //     //response.tnc = Util.htmlDecoder(response.tnc);
        //     $('#selectGiftOrderLoader').modal('hide');
        //     $('.tandc').html(Util.htmlDecoder(response.tnc));
        //     $('#myModalBrandDetail').modal('show');
        // }, function(error) {
        //     $("#selectGiftOrderLoader").modal("hide");
        //     $("#createOrderModalSGOops").modal("show");
        // });
};

var showTnc = function(tnc){
    let response = Util.htmlDecoder(tnc); 
    let htmlParse = $('<div/>').html(response);
    $('.gift-card-terms__list').append(htmlParse);  
}

var getTermsAndConditions = function(brandName){         
    SelectGiftSvc.getListProduct().then(function(res){        
        let response = Util.htmlDecoder(res);        
        let productsLength=0;
        let isFound= false;
        while(res.products.length !== productsLength && !isFound){
            if(res.products[productsLength].brandName == brandName){
                let tnc =  Util.htmlDecoder(res.products[productsLength].tnc);
                console.log(tnc.split('<br/>'));
                let newTermsAndConditions;
                let modifyTnc = tnc.split('<br/>');
                var j=0,i="";
                modifyTnc.map(data=>{
                    j++;
                    i+= j +'. '+ data +'<br/><br/>';
                });
                let htmlParse = $('<div/>').html(i);
                $('.gift-card-terms__list').append(htmlParse);  
                isFound = true;
                return true;
            }else{
                productsLength++;                    
            }
        }
    },function(error){

    })
}

var termsNCon = function(e){     
        $('#selectGiftOrderLoader').modal('show');
        let productId = Storage.getSessionCollection('productId');
        SelectGiftSvc.getProductDeatils(productId).then(function(res) {
            let response = res;
            response.tnc = Util.htmlDecoder(response.tnc);
            $('#selectGiftOrderLoader').modal('hide');
            $('.tandc').html(response.tnc);
            $('#myModalBrandDetail').modal('show');
        }, function(error) {
            $("#selectGiftOrderLoader").modal("hide");
            $("#createOrderModalSGOops").modal("show");
        });
        e.preventDefault();
}

export default {
    termsAndCondition:termsAndCondition,
    termsNCon:termsNCon,
    // cominSoonBrands: function() {
    //     let brandName = $(this).data('brandname');
    //     let brandIconUrl = $(this).data('iconurl');
    //     let productId = $(this).data('brandid');
    //     $('#brandLogosSoon').html('<img src="' + brandIconUrl + '" />');
    //     document.getElementById('brandNameSoon').innerHTML = brandName;
    //     $('#comingSoonBrandsPop').modal('show');
    //     initGAEvent('event', 'Select Gifting', 'click', 'Brand Interest, corpId = ' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', brandName=' + brandName + ',timestamp=' + new Date().toLocaleString());
    //     SelectGiftSvc.addProductRequest(productId).then(function(respData) {});
    // },

    chooseBrandName: function() {
        let productId = $(this).data('brandid');
        let brandName = $(this).data('brandname');
        let brandLogoUrl = $(this).data('logourl');
        let brandIconUrl = $(this).data('iconurl');
        let discount = $(this).data('discount');
        let purchaseMin = $(this).data('purchasemin');
        let purchaseMax = $(this).data('purchasemax');
        var denomination = $(this).data('denomination');
        let purchasetype = $(this).data('purchasetype');
        
        if($(this).hasClass('active')){
            //$(this).removeClass('active');
        }
        else{
            $('.chooseBrandName').removeClass('active');
            $(this).addClass('active');
            $('div[data-brandid = ' + productId + ']').addClass('active');
        }

        $('#chooseStep').addClass('done');
        $('#uploadStep').addClass('active');
        $('#chooseShortcodeCntr').hide();
        $('#fileErrorWrpr').hide();
        $('#chooseTxt').html();
        $('.denominationCont').hide();
        SpotLightCtrl.resetForm();
        
        initGAEvent('event', 'Select Gifting', 'click', 'Brand Selected, corpId = ' + Meta.getMetaElement("corpID") + ', compId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', brandName='+brandName+ ',timestamp=' + new Date().toLocaleString());
        var arraydenom= [];
        if(denomination != undefined){
            if (typeof denomination === 'string') { 
                arraydenom = denomination.split(',');
                for (var i in arraydenom) {
                    arraydenom[i] = arraydenom[i] / 100;
                }
                arraydenom = arraydenom.toString().replace(/,/g , ", ");
             }
             else{
                arraydenom = denomination/100;
             }
        }
        // Setting the sessionValue 
        Storage.setSessionCollection('productId', productId);
        Storage.setSessionCollection('brandName', brandName);
        Storage.setSessionCollection('discount', discount);
        Storage.setSessionCollection('brandLogoUrl', brandLogoUrl);
        Storage.setSessionCollection('brandIconUrl', brandIconUrl);
        Storage.setSessionCollection('purchaseMin', purchaseMin / 100);
        Storage.setSessionCollection('purchaseMax', purchaseMax / 100);
        Storage.setSessionCollection('denomination', arraydenom);
        Storage.setSessionCollection('purchasetype', purchasetype);
        $('.denominationMin').text(Storage.getSessionCollection('purchaseMin'));
        $('.denominationMax').text(Storage.getSessionCollection('purchaseMax'));
        $('.brandLogoSg').attr('src', Storage.getSessionCollection('brandLogoUrl'));
        if (purchasetype.toLowerCase() != 'range') {
            $('.denominationCont').show();
            $('.denominationCont1').hide();
            $('.denomination').text(Storage.getSessionCollection('denomination'));
        } else {
            $('.denominationCont').hide();
            $('.denominationCont1').show();
        }
        $('#uploadFileCntr').show();
        var targetUrl = "uploadfile";
        window.history.pushState({url: "" + targetUrl + ""}, targetUrl);
        window.location.hash=targetUrl;
    },

    validationForTransfer: function() {
        if ($('#scheduleTransferDate').val() != '' && SpotLightCtrl.setFlag() != 0) {
            $('#create-order').removeAttr('disabled');
            $('#create-order').removeClass('btn-disabled');
        }

    },

    initiateTransfer: function() {
        
        var uploadDetails = $('#uploadDetails').val();
        let scheduledDate = $('#scheduleTransferDate').val();
        Storage.setSessionCollection('scheduledDate', scheduledDate);
        $("#createOrderModalSGOops").modal("hide");
        $("#createOrderModalLoader").modal("show");

        let bodyObj = {
            "scheduledDate": scheduledDate,
            "key": JSON.parse(uploadDetails).key,
            "fileName": Storage.get(Constant.STORAGE_FILE_NAME),
            "fileUrl": Storage.get(Constant.STORAGE_FILE_URL),
            "productId": Storage.getSessionCollection(Constant.PRODUCT_ID),
            "spotlightType": "SELECT_GIFTING",
            "token": Storage.get('authToken')
        }
        var fileName = $('.dz-filename span').text().split('.');
        var fileExt = fileName[fileName.length - 1];
        var brandName = Storage.getSessionCollection('brandName');
        SelectGiftSvc.orderVerify(bodyObj).then(function(respData) {
            var targetUrl = "payment";
            window.history.pushState({url: "" + targetUrl + ""}, targetUrl);
            window.location.hash=targetUrl;
            initGAEvent('event', 'Select Gifting', 'click', 'Order Details Submitted, corpId = ' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',scheduledDate=' + scheduledDate + ',fileType=' + fileExt + ',brandName=' + brandName + ',timestamp=' + new Date().toLocaleString() + ',Success');
            let corpID = $("meta[name='corpID']").attr("content");
            let authToken = Storage.get('authToken');
            let fundingAccountId = respData.fundingAccountId;
            let pgProgramId = respData.pgProgramId;
            Storage.setSessionCollection('pgProgramId', pgProgramId);
            Storage.setSessionCollection('fundingAccountId', fundingAccountId);
            $('#createSelectGiftingOrder').attr('data-fundingaccountid', Storage.getSessionCollection('fundingAccountId'));
            $('#createSelectGiftingOrder').attr('data-pgprogramid', Storage.getSessionCollection('pgProgramId'));
            $('#createOrderDone').attr('data-fundingaccountids', Storage.getSessionCollection('fundingAccountId'));
            SelectGiftSvc.getAccDetails(authToken, corpID, fundingAccountId).then(function(fundRespData) {
                let fundingAcc = fundRespData;
                $('.accBalance').html((fundingAcc.balance) / 100);
                $('.accName').html(fundingAcc.name);
                Storage.setSessionCollection('fundingAccountName', fundingAcc.name);
                Storage.setSessionCollection('fundingAccountBalance', (fundingAcc.balance) / 100);
                Storage.setSessionCollection('ifiID', (fundingAcc.ifiID));
                //return fundingAcc;
            }, function(respErr) {
                console.log(respErr);
            });
            Storage.setSessionCollection('reuploadStatus', false);
            Storage.setSessionCollection('selectBrandStatus', true);
            let brandIconUrl = Storage.getSessionCollection('brandIconUrl');
            $("#createOrderModalLoader").modal("hide");
            $('#uploadFileCntr').hide();
            $('#verifyOrderCntr').show();
            $('#verifyStep').addClass('active');
            $('#uploadStep').addClass('done');
            $('#brandId').html(Storage.getSessionCollection('brandName'));
            $('#brandLogo').html('<img src="' + brandIconUrl + '" style="width:32px;height:32px;"/>');
            $('#noOfBeneficiaries').html(respData.totalPayout);
            $('#discountProvidedValue').html(respData.totalDiscount / 100);
            $('#discountProvided').html(Storage.getSessionCollection('discount'));
            $('#totalGiftValue').html(respData.totalAmount / 100);
            let dates = Util.dateFormatWithDay(Storage.getSessionCollection('scheduledDate'));
            $('#scheduledTransferDate').html(dates);
            let totalAmount = (respData.totalAmount) - (respData.totalDiscount);
            $('#totalAmount').html(totalAmount / 100);
            $('#createGiftCheckbox').on('click', function() {
                if ($(this).is(':checked')) {
                    $('#createSelectGiftingOrder').removeAttr('disabled');
                    $('#createSelectGiftingOrder').removeClass('btn-disabled');
                } else {
                    $('#createSelectGiftingOrder').attr('disabled', 'disabled');
                    $('#createSelectGiftingOrder').addClass('btn-disabled');
                }
            });
            
        }, function(respErr) {
            initGAEvent('event', 'Select Gifting', 'click', 'Order Details Submitted, corpId = ' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',scheduledDate=' + scheduledDate + ',fileType=' + fileExt + ',brandName=' + brandName + ',timestamp=' + new Date().toLocaleString() + ',Failed, Reason=' + JSON.stringify(respErr));
            Storage.setSessionCollection('reuploadStatus', true);
            Storage.setSessionCollection('selectBrandStatus', false);
            respErr = JSON.parse(respErr.responseText);
            if (respErr.attributes.fileProcessingSummary) {
                if (respErr.attributes.fileProcessingSummary.invalidRows > 0) {
                    $("#createOrderModalLoader").modal("hide");
                    Storage.set(Constant.STORAGE_INVALID_SELECTGIFT_ENTRIES, respErr.attributes.fileProcessingSummary.invalidRows);
                    $('#noOfInvalidEntries').html(respErr.attributes.fileProcessingSummary.invalidRows);
                    $('#fileName').html(Storage.get(Constant.STORAGE_FILE_NAME));
                    switch (Storage.get(Constant.STORAGE_FILE_TYPE)) {
                        case "text/csv":
                            $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-12.png");
                            break;
                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                            $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xlsx.png");
                            break;
                        case "application/vnd.ms-excel":
                            $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                            break;
                        default:
                            $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                            break;     
                    }

                    let responseData = {
                        records: respErr.attributes.rows,
                        totalCount: respErr.attributes.rows.length
                    };
                    $("#errorSelectGiftingTransfers").table({
                        source: responseData,
                        pageSize: 10,
                        pageNumber: 1,
                        tableTemplate: '../../../../template/section/spotlight/selectgifting/table/error-entries.ejs',
                        rowTemplate: '../../../../template/section/spotlight/selectgifting/table/error-entries-row.ejs',
                        extraParam: {
                            dynamicHeader: true
                        }
                    });
                    $('#uploadFileCntr').hide();
                    $('#createBulkOrderModalOops').modal('hide');
                    $('#fileErrorWrpr').show();
                    $('#downloadSGReport').attr('href', Storage.get(Constant.STORAGE_FILE_URL))
                } else {
                    $("#createOrderModalLoader").modal("hide");
                    $("#createBulkOrderModalOops").modal("show");
                }
            } else {
                $("#createOrderModalLoader").modal("hide");
                $("#createBulkOrderModalOops").modal("show");
            }
        });
    },


    createSelectGiftOrder: function() {
        $('#createSelectGiftingOrder').attr('disable',true).css('background','#ccc');
        $('.spinner-loader').show()
        var uploadDetails = $('#uploadDetails').val();
        let fundingAccountId = Storage.getSessionCollection('fundingAccountId');
        let pgprogramid = $(this).data('pgprogramid');
        let inputObj = {
            "scheduledDate": Storage.getSessionCollection('scheduledDate'),
            "key": JSON.parse(uploadDetails).key,
            "fileName": localStorage.getItem('storage-filename'),
            "fileUrl": localStorage.getItem('storage-fileurl'),
            "fundingAccountId": fundingAccountId,
            "token": Storage.get('authToken'),
            "productId": Storage.getSessionCollection(Constant.PRODUCT_ID),
            "pgProgramId": pgprogramid,
            "spotlightType": "SELECT_GIFTING",
        }
        var scheduledDate = $("#scheduledTransferDate").text();
        var beneficiaries = $("#noOfBeneficiaries").text();
        var totalGiftValue = $("#totalGiftValue").text();
        var discountValue = $("#discountProvidedValue").text();
        var accBalance = $(".accBalance").text();
        SelectGiftSvc.orderCreate(inputObj).then(function(respData) {
            initGAEvent('event', 'Select Gifting', 'click', 'Order Placed, BrandName=' + $('#brandId').text() + ', scheduledDate=' + scheduledDate + ',noOfBeneficiaries=' + beneficiaries + ',totalGiftValue=' + totalGiftValue + ',discountValue=' + discountValue + ',FAbalance=' + accBalance + ',corpId = ' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString() + ',Success');
            let fundingAccountName = Storage.getSessionCollection('fundingAccountName');
            let fundingAccountBalance = Storage.getSessionCollection('fundingAccountBalance');
            $('.createhideIt').hide();
            $('.createhideIt2').show();
            $("#createOrderModalShow").modal("show");
            $('#verifyStep').addClass('done');
            $('#createOrderId').html(respData.orderId);
            $('#createFundName').html(fundingAccountName);
            $('#createFundBalance').html(fundingAccountBalance);
            $('#createTotalAmt').html(respData.totalAmount / 100);
            $('#createOrderDone').data('fundingaccountid', Storage.setSessionCollection('fundingAccountId'));
            let dates = Util.dateFormatWithDay(Storage.getSessionCollection('scheduledDate'));
            $('#createScheduleDate').html(dates);
            let totalAmount = (respData.totalAmount) - (respData.totalDiscountAmount);
            $('#createTotalOrderAmt').html(totalAmount / 100);
            if ((totalAmount / 100) > fundingAccountBalance) {
                $('.createhideIt').show();
                $('.createhideIt2').hide();
                let createFundTobAdded = (totalAmount / 100) - fundingAccountBalance;
                $('#createFundTobAdded').html(createFundTobAdded);              
            }
            $('#createSelectGiftingOrder').attr('disable',false).css('background','#ffbc00');
            $('.spinner-loader').hide()
        }, function(respErr) {
            initGAEvent('event', 'Select Gifting', 'click', 'Order Placed, BrandName=' + $('#brandId').text() + ', scheduledDate=' + scheduledDate + ',noOfBeneficiaries=' + beneficiaries + ',totalGiftValue=' + totalGiftValue + ',discountValue=' + discountValue + ',FAbalance=' + accBalance + ',corpId = ' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString() + ',Failed,Reason=' + JSON.stringify(respErr));
            console.log(respErr);
            $('#createSelectGiftingOrder').attr('disable',false).css('background','#ffbc00');
        $('.spinner-loader').hide()
            $("#createOrderModalLoader").modal("hide");
            $("#createOrderModalSGOops").modal("show");

        });

    },

    addFundsSG: function() {
        $('#createOrderModalShow').modal('hide');
        $('#selectGiftOrderLoader').modal('show');
        let accountId = $(this).attr('data-fundingaccountids');
        let ifiID =  Storage.getSessionCollection('ifiID');
        
         if(accountId == undefined){
             accountId = Storage.get('ACCOUNT_ID');
             Storage.set('ACCOUNT_ID', accountId);
             Storage.set('ACCOUNT_NAME', Storage.getSessionCollection('fundingAccountName'));
         }
         else{
             accountId = accountId;
             Storage.set('ACCOUNT_ID', accountId);
             Storage.set('ACCOUNT_ID', accountId);
             Storage.set('ACCOUNT_NAME', Storage.getSessionCollection('fundingAccountName'));
         }
        let corporateId = $("meta[name='corpID']").attr("content");
        let authToken = Storage.get('authToken');
        let headers = {};
        SelectGiftSvc.getIfiDetails(authToken, corporateId, ifiID).then(function(getIfisData) {
           // console.log(getIfisData)
            let getIfisBankDetails = getIfisData;
            $('#selectGiftOrderLoader').modal('hide');
            if (ifiID == Constant.KOTAK_FUNDING_ACCS[Env.CURR_ENV]) {
                $('#addFundsBtnWrpr,.list-number-circle').hide();
                $('#addFundDetailsHdg').text('Please use convenient channels to pay to the following bank account.');
                $('#ifiAccNo').text(getIfisBankDetails.bankDetails.accountNumber);
                $('#ifiBankName').text('Kotak Mahindra Bank');
                $('#ifiBankBranch').text('MUMBAI-NARIMAN POINT');
                $('#ifiIfscCode').text(getIfisBankDetails.bankDetails.IFSC);
                $('#acc_name strong').text(Storage.getSessionCollection('fundingAccountName'));
                $('#ifiAccName').text(getIfisBankDetails.bankDetails.accountName);
            } else {
                $('#acNameModalWrpr,#addFundsBtnWrpr,.list-number-circle').show();
                $('#addFundDetailsHdg').text('Drop a cheque/transfer to Zeta’s Bank Account');
                $('#ifiAccNo').text(getIfisBankDetails.bankDetails.accountNumber);
                $('#ifiBankName').text(getIfisBankDetails.bankDetails.bankName);
                $('#ifiBankBranch').text(getIfisBankDetails.bankDetails.bankAddress);
                $('#ifiIfscCode').text(getIfisBankDetails.bankDetails.IFSC);
                $('#acc_name strong').text(Storage.getSessionCollection('fundingAccountName'));
                $('#ifiAccName').text(getIfisBankDetails.bankDetails.accountName);
            }
            $('#fundChequeTransferx').modal('show');
            Storage.set('ACCOUNT_NAME', Storage.getSessionCollection('fundingAccountName'));
        }, function(respErr) {
            $('#selectGiftOrderLoader').modal('hide');
            $('#selectGiftErrorLoader').modal('show');
            console.log('Error', respErr);
        });
    },

    routeToAddFund: function() {
        window.location.href = '/admin/funding-accounts/add-funds-details'
    },

    generateGift: function() {
        $('.loading-gif').show();
        let ifiName = Util.getParameterByName('ifi');
        ifiName == 'hdfc' ? $('.hdfcLogo').show() : $('.hdfcLogo').hide() 
        SelectGiftSvc.generateGift().then(function(respData) {
            $('.loading-gif').hide();
            $('.mainloader').hide();
            $('#showGiftGenerator').show();
                if(respData.brandName.toLowerCase()=='amazon'){
                    $('.cmnVoucher').hide();
                    $('.azVoucher').show();
                    $('#amazonAccountImg').attr('href','https://www.amazon.in/ap/signin?_encoding=UTF8&openid.assoc_handle=inflex&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.mode=checkid_setup&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&openid.pape.max_auth_age=900&openid.return_to=https%3A%2F%2Fwww.amazon.in%2Fgp%2Fcss%2Fgc%2Fpayment%3Fie%3DUTF8%26actionType%3Dadd%26claimCode%3D'+respData.claimCode+'%26ref_%3Dgc_ya_subnav_balance%26txtLeftButton%3DApplytoYourAccount')
                    }
                else if(respData.brandName.toLowerCase()=='bookmyshow'){
                    $('.cmnVoucher').hide();
                    $('.bmsVoucher').show();
                    }
                else{
                    $('.cmnVoucher').show();
                    }
            $('#giftCardId').html(respData.cardNumber);
            $('#giftImage').attr('src', respData.brandUrl);
            $('#giftBrand').html(respData.brandName);
            $('.giftCardCode').html(respData.claimCode);
            $('.giftCardNum').html(respData.cardNumber);
            $('#giftAmount').html('₹' + respData.cardPrice);
            let brandDate = Util.timeStampToDate(respData.expiryDate);
            $('.brandExDate').html(brandDate);
            let htmlParse = $('<div/>').html(respData.claimSteps).text()
            $('.generate-gift-footer').append(htmlParse);   
            respData.tnc ? showTnc(respData.tnc) : getTermsAndConditions(respData.brandName);            
        }, function(respError) {
            let respErr = respError.responseJSON;
            $('.mainloader').hide();
            if(respErr.message){
                if(respErr.message.toLowerCase() == 'link expired'){
                    $("#errorExpirefetchingGift").show();
                } else if(respErr.message.toLowerCase() == 'invalid resource'){
                    $("#errorinvalidfetchingGift").show();
                }                 
                else {
                    $("#errorfetchingGift").show();
                }
            }
            else{
                $("#errorfetchingGift").show();
            }
        });
    },

    reUploadOrder: function() {
        Storage.setSessionCollection('reuploadStatus', true);
        window.location.reload();
    },

    retryErrorModal: function() {
        $('#createBulkOrderModalOops').modal('hide');
    },

    csvDownlaodURL: function(url) {
        ServiceConnector.get(url).then(function(respData) {
            let x = Util.htmlDecoder(respData.fileDownloadUrl);
            window.open(Util.htmlDecoder(x), "_blank");
        }, function(respErr) {
            console.log(respErr);
        });
    },

    prepareGAEvent: function() {
        initGAEvent('event', $(this).attr('id'), 'click', 'Spotlight');
    },
    fundingAccountIDChange: function() {
        let $createProgram = $('#changeAccSG'),
            //$programName = $('#programName'),
            $accountID = $('#fundingAccountID'),
            $showHideNewFundingAcc = $('#shownewfundingaccname'),
            $NewFundingAccName = $('#newfundingaccname');

        if ($accountID.find(":selected").text() == 'Create New') {
            $showHideNewFundingAcc.show();
            $('#changeAccSG').hide();
            $('#setAccSG').show();
            if ($accountID.val() == 0 || !$NewFundingAccName.val() || $NewFundingAccName.val().length > 40) {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
                $('#newfundingaccname').val(null)
            } else {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
                $('#newfundingaccname').val(null)
            }
        } else {
            $showHideNewFundingAcc.hide();
            $('#changeAccSG').show();
            $('#setAccSG').hide();
            if ($accountID.val() == 0) {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
                $('#newfundingaccname').val(null)
            } else {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
                $('#newfundingaccname').val(null)
            }
        }
    },
    newFundingAccountName: function() {
        let $createProgram = $('#setAccSG'),
            //$programName = $('#programName'),
            $accountID = $('#fundingAccountID'),
            $showHideNewFundingAcc = $('#shownewfundingaccname'),
            $NewFundingAccName = $('#newfundingaccname');

        if ($accountID.val() == 0 || !$NewFundingAccName.val() || $NewFundingAccName.val().length > 40) {
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $createProgram.removeAttr('disabled');
            $createProgram.removeClass('btn-disabled');
        }
       // Storage.set('changeFundingAccName', $NewFundingAccName.val())
        let accountName = $NewFundingAccName.val();
        let allfundingAccountName = JSON.parse(localStorage.getItem('allFundingAccountName'));
        if (allfundingAccountName.indexOf(accountName) > -1) {
            $('#showfundingAccDuplicate').show();
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $('#showfundingAccDuplicate').hide();
        }
    },

    changeFundingAccount: function(){
        console.log('hi');
        let authToken = Storage.get('authToken');
        let fundingAccID = $("#fundingAccountID option:selected").val();
        let corpID = $("meta[name='corpID']").attr("content");
        Storage.setSessionCollection('fundingAccountId', fundingAccID);
        $('#createSelectGiftingOrder').attr('data-fundingaccountid', Storage.getSessionCollection('fundingAccountId'));
        $('#createOrderDone').attr('data-fundingaccountids', Storage.getSessionCollection('fundingAccountId'));
        SelectGiftSvc.getAccDetails(authToken, corpID, fundingAccID).then(function(fundRespData) {
            let fundingAcc = fundRespData;
               $('.accBalance').html((fundingAcc.balance)/100);
               $('.accName').html(fundingAcc.name);
               Storage.setSessionCollection('fundingAccountName', fundingAcc.name);
               Storage.setSessionCollection('fundingAccountBalance', (fundingAcc.balance)/100);
               Storage.setSessionCollection('ifiID', (fundingAcc.ifiID));
               $('#changeFundAccSg').modal('hide');
               $('#changedFundAccountName').html(fundingAcc.name);
               $('#changeFundAccProgramSuccess').modal('show');
               //return fundingAcc;
       }, function(respErr) {
           console.log(respErr);
       });
      
    },

    setFundingAccount: function(){
        let fundingAccName = $('#newfundingaccname').val();
        let inputObj = {
            'corpID': $("meta[name='corpID']").attr("content"),
            'name': fundingAccName
        }
        let authToken = Storage.get('authToken');
        SelectGiftSvc.setFundAcc(inputObj).then(function(respData) {
            
            Storage.setSessionCollection('fundingAccountId', respData.accountID);
            Storage.setSessionCollection('fundingAccountName', respData.name);
            Storage.setSessionCollection('fundingAccountBalance', 0);
            Storage.setSessionCollection('ifiID', (respData.ifiID));
            $('#createSelectGiftingOrder').attr('data-fundingaccountid', Storage.getSessionCollection('fundingAccountId'));
            $('#createOrderDone').attr('data-fundingaccountids', Storage.getSessionCollection('fundingAccountId'));

            $('.accBalance').html(0);
            $('.accName').html(respData.name);
            $('#changeFundAccSg').modal('hide');
             
        }, function(respErr) {
           
        });
    },

    showManuallyOnStep: function () {
        if($('#chooseStep').hasClass('done')){
            $('#chooseStep').addClass('done');
            $('#uploadStep').addClass('active');
            $('#uploadStep').removeClass('done');
         $('#verifyStep').removeClass('active');
         $('#chooseShortcodeCntr').hide()
         $('#uploadFileCntr').show();
         $('#verifyOrderCntr').hide();
        }
    },

    showChooseDesign: function () {
        if($('#chooseStep').hasClass('done')){
        $('#chooseShortcodeCntr').show()
        $('#uploadFileCntr').hide();
        $('#verifyOrderCntr').hide();
    }         
    },

    showVerifyAndPay: function () {
        if($('#uploadStep').hasClass('done')){
            $('#chooseShortcodeCntr').hide()
            $('#uploadFileCntr').hide();
            $('#verifyOrderCntr').show();
    }         
    },

    learnMoreGA:function(){
        initGAEvent('event', 'Brand Vouchers', 'click', 'What does discount mean? Learn more, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+',timestamp=' + new Date().toLocaleString());
    }

}

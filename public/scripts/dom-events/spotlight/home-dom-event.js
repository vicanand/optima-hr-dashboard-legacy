import ServiceConnector from '../../common/serviceConnector';
import ResetPinCrt from '../../controller/spotlight/instagift/reset-pin-request';
import InstagiftSvc from '../../service/spotlight/instagift-svc';
import PhysicalSvc from '../../service/spotlight/physicalSvc';
import AnalyticsSvc from '../../service/analytics/analytics-svc';
import Meta from '../../common/metaStorage';
import Storage from '../../common/webStorage'

export
default {
    createProgram: function() {
        var self = this;
        $(self).addClass('instagift-loader');
        $(self).parent().find('.error').hide();
        let programType = $(this).attr('data-programType')
        let fundingAccountID = $('#fundingAccountID').val();
        InstagiftSvc.setupProgram(programType, fundingAccountID).then(function(respData) {
            initGAEvent('event', 'Spotlight', 'click', 'Setup InstaGift Program, corpId = ' + Meta.getMetaElement("corpID") + ', compId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+ ', programType='+programType+', fundingAccountID='+fundingAccountID+', Success');
            $(self).removeClass('instagift-loader');
            $('#setupInstagiftProgramModal').modal('hide');
            $('#InstagiftcreateProgramSuccess').modal('show');
            $("#viewProgramBtnSuccess").attr("href", "spotlight/instagift/" + respData.programID);
        }, function(respErr) {
            initGAEvent('event', 'Spotlight', 'click', 'Setup InstaGift Program, corpId = ' + Meta.getMetaElement("corpID") + ', compId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID') + ', programType='+programType+', fundingAccountID='+fundingAccountID+', Failed');
            $(self).removeClass('instagift-loader');
            $(self).parent().find('.error').show();
        });
    },

    cashIncentiveCreateProgram: function() {
        $('#createProgramLoader').modal();
        let url = '/createCashIncentiveProgram?fundingAccID=' + $('#cashIncentiveFundingAccountID').val() +
                  '&productType=' + $(this).attr('data-programType');
       
        ServiceConnector.get(url).then(function(respData) {
            $('#createProgramLoader').modal('hide');
            $('#setupCashIncentiveModal').modal('hide');
            $('#cashIncentivecreateProgramSuccess').modal('show');
        }, function(respErr) {
            console.log('Error');
            $('#createProgramLoader').modal('hide');
            $('#createProgramError').modal('show');

        })
    },

    selectGiftingCreateProgram: function() {
        $('#createProgramLoader').modal();
        let url = '/createSelectGiftingProgram?fundingAccountId=' + $('#selectGiftingFundingAccountID').val() +
                  '&spotlightType=' + $(this).attr('data-programType') + '&companyId=' + $("meta[name='companyID']").attr("content") + 
                  '&corporateId=' + $("meta[name='corpID']").attr("content") + '&token=' + Storage.get('authToken');
       
        ServiceConnector.get(url).then(function(respData) {
            $('#createProgramLoader').modal('hide');
            $('#setupSelectGiftingModal').modal('hide');
            $('#selectGiftingcreateProgramSuccess').modal('show');
           
        }, function(respErr) {
            console.log('Error');
            $('#createProgramLoader').modal('hide');
            $('#createProgramError').modal('show');

        })
    },

    closeSG:function(){
        window.location.reload();
    },

    fundingAccountIDChange: function() {
        let $createProgram = $('#createProgram');
        if ($(this).val() == '0') {
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $createProgram.removeAttr('disabled');
            $createProgram.removeClass('btn-disabled');
        }
    },
    cashIncentviefundingAccountIDChange : function(){
        let $createProgram = $('#cashIncentiveCreateProgram');
        if ($(this).val() == '0') {
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $createProgram.removeAttr('disabled');
            $createProgram.removeClass('btn-disabled');
        }
    },
    selectGiftingfundingAccountIDChange : function(){
        let $createProgram = $('#selectGiftingCreateProgram');
        if ($(this).val() == '0') {
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $createProgram.removeAttr('disabled');
            $createProgram.removeClass('btn-disabled');
        }
    },
    programNameChange: function() {
        let $createProgram = $('#createProgram');
        if ($(this).val().length === 0 || $(this).val().length > 40) {
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $createProgram.removeAttr('disabled');
            $createProgram.removeClass('btn-disabled');
        }
    },

    vitualCardSendGift:function(){
        initGAEvent('event', 'Vitual Gift Card', 'click', 'Spotlight home send gift, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+',timestamp=' + new Date().toLocaleString());
    },
    vitualCardListOrder:function(){
        initGAEvent('event', 'Vitual Gift Card', 'click', 'Spotlight home list order, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+',timestamp=' + new Date().toLocaleString());
    },
}

$('.btn-more').on('click', function(e) {
    let eventName   = "learnMore.click";
    let sessionID   = Storage.get('sessionID');
    let userID      = $("meta[name='userID']").attr("content");
    let corp_ID     = $("meta[name='corpID']").attr("content");
    let company_ID  = $("meta[name='companyID']").attr("content");
    let programType = $(this).data('programtype');
    let eventLogs   = [
        {
            "serviceName": "instagift",
            "eventName": eventName,
            "sessionID": sessionID,
            "userID_longValue": userID,
            "corpID_longValue": corp_ID,
            "companyID_longValue": company_ID,
            "programType": programType
        }
    ];
    AnalyticsSvc.recordEvents(eventLogs);
});

$('#approvePinRequest').on('click', function() {
    let requestid = $(this).closest('#approvePinReset').data('requestid');
    ResetPinCrt.approvePinRequest(requestid);
});

$('#declinePinRequest').on('click', function() {
    let requestid = $(this).closest('#declinePinReset').data('requestid');
    ResetPinCrt.declinePinRequest(requestid);
});

$('.InstagiftsaveProgram').on('click', function(e) {
    // let programType = this.dataset.programtype;
    let programType = "INSTAGIFT";
    let accountID = $('#accountID').val();
    let loading = $("#loading");
    loading.modal('show');
    InstagiftSvc.saveProgramInterest(accountID, programType).then(function(respData) {
        if (respData && respData.salesManager) {
            let managerName = respData.salesManager.salesPersonName;
            let managerEmail = respData.salesManager.salesPersonEmail;
            $('.instagiftManagerName').html(managerName);
            $('#instagiftManagerEmail').html(managerEmail);
            let mailTo = 'mailto:' + managerEmail;
            $('#instagiftManagerEmaillink').attr("href", mailTo);
            loading.modal('hide');
            $('#instagiftModal').modal('show');
        } else {
            loading.modal('hide');
            $("#instagiftInterest").html('A Zeta Sales Representative will be in touch with you within the next 2 business days to let you know more about this program.');
            $("#instagiftContact").hide();
            $('#instagiftModal').modal('show');
        }
    }, function(respErr) {
        loading.modal('hide');
        $('#Errorstate').modal('show');
    });
});
$('.digigift-request-demo').on('click', function(e) {
    var self = this;
    $(self).addClass('instagift-loader');
    $(self).html('REGISTERING YOUR REQUEST...');
    $(self).parent().find('.error').hide();
    let programType = this.dataset.programtype;
    let accountID = $('#accountID').val();
    let prgramDemoWrap = $(self).parent().parent();
    InstagiftSvc.saveProgramInterest(accountID, programType).then(function(respData) {
        if (respData && respData.salesManager) {
            let managerName = respData.salesManager.salesPersonName;
            let managerEmail = respData.salesManager.salesPersonEmail;
            $('.digigiftManagerName').html(managerName);
            $(self).removeClass('instagift-loader');
            $(self).hide();
            prgramDemoWrap.find('.program-demo-wrapper').show();
        } else {
            $(self).removeClass('instagift-loader');
            $(self).hide();
            prgramDemoWrap.find('.program-demo-wrapper').hide();
            prgramDemoWrap.find('.program-demo-default-text').show();
            prgramDemoWrap.find('.program-demo-default-text').find('.progrem-info p').html('A Zeta Sales Representative will be in touch with you within the next 2 business days to let you know more about this program.');
        }
    }, function(respErr) {
        $(self).removeClass('instagift-loader');
        $(self).parent().find('.error').show();
        $(self).html('REQUEST A DEMO');
    });
});

$('.incentives-request-demo').on('click', function(e) {
    var self = this;
    $(self).addClass('instagift-loader');
    $(self).html('REGISTERING YOUR REQUEST...');
    $(self).parent().find('.error').hide();
    let programType = this.dataset.programtype;
    let accountID = $('#accountID').val();
    let prgramDemoWrap = $(self).parent().parent();
    InstagiftSvc.saveProgramInterest(accountID, programType).then(function(respData) {
        if (respData && respData.salesManager) {
            let managerName = respData.salesManager.salesPersonName;
            let managerEmail = respData.salesManager.salesPersonEmail;
            $('.incentivesManagerName').html(managerName);
            $(self).removeClass('instagift-loader');
            $(self).hide();
            prgramDemoWrap.find('.program-demo-wrapper').show();
        } else {
            $(self).removeClass('instagift-loader');
            $(self).hide();
            prgramDemoWrap.find('.program-demo-wrapper').hide();
            prgramDemoWrap.find('.program-demo-default-text').show();
            prgramDemoWrap.find('.program-demo-default-text').find('.progrem-info p').html('A Zeta Sales Representative will be in touch with you within the next 2 business days to let you know more about this program.');
        }
    }, function(respErr) {
        $(self).removeClass('instagift-loader');
        $(self).parent().find('.error').show();
        $(self).html('REQUEST A DEMO');
    });
});

$('.rnr-request-demo').on('click', function(e) {
    var self = this;
    $(self).addClass('instagift-loader');
    $(self).html('REGISTERING YOUR REQUEST...');
    $(self).parent().find('.error').hide();
    let programType = this.dataset.programtype;
    let accountID = $('#accountID').val();
    let prgramDemoWrap = $(self).parent().parent();
    InstagiftSvc.saveProgramInterest(accountID, programType).then(function(respData) {
        if (respData && respData.salesManager) {
            let managerName = respData.salesManager.salesPersonName;
            let managerEmail = respData.salesManager.salesPersonEmail;
            $('.rnrManagerName').html(managerName);
            $(self).removeClass('instagift-loader');
            $(self).hide();
            prgramDemoWrap.find('.program-demo-wrapper').show();
        } else {
            $(self).removeClass('instagift-loader');
            $(self).hide();
            prgramDemoWrap.find('.program-demo-wrapper').hide();
            prgramDemoWrap.find('.program-demo-default-text').show();
            prgramDemoWrap.find('.program-demo-default-text').find('.progrem-info p').html('A Zeta Sales Representative will be in touch with you within the next 2 business days to let you know more about this program.');
        }
    }, function(respErr) {
        $(self).removeClass('instagift-loader');
        $(self).parent().find('.error').show();
        $(self).html('REQUEST A DEMO');
    });
});

function customCarousel() {
    var counter = 4;
    var interval;
    var box = $('.boxes');
    interval = setInterval(function() {
        $(box).removeClass('current');
        $(box).eq(counter++ % $(box).length).addClass('current');
        if (counter >= $(box).length) {
            counter = 0;
        }
    }, 1800);

    $(".boxes").hover(function() {
        $(box).removeClass('current');
        clearInterval(interval);
        $(this).addClass('current');
    }, function() {
        counter = $(this).index() + 1;
        $(this).removeClass('current');
        interval = setInterval(function() {
            $(box).removeClass('current');
            $(box).eq(counter++ % $(box).length).addClass('current');
            if (counter >= $(box).length) {
                counter = 0;
            }
        }, 1800);
    });
}

// physcial order card
$('.createPhysicalOrder').on('click', function(e) {
    $('#myModalLoader').modal('show');
    $('#showBtnLoader').show();
    $('#create-order').attr('disable',true).css('background','#e9e9e9');
    let body = {
         numberOfCards : $('#numberOfCards').val(),
         name : $('#name').val(),
         phoneNumber : $('#phoneNumber').val(),
         address : $('#address').val(),
         zipcode : $('#zipcode').val(),
         city : $('#city').val(),
         state : $('#state').val(),
         country : $('#country').val(),
         referenceName : $('#referenceName').val(),
    };

    PhysicalSvc.createPhysicalCardOrder(body).then(function(respData) {
        console.log(respData);
        $('#showBtnLoader').hide();
        $('#create-order').attr('disable',false).css('background','#ffbc00');
        $('#myModalLoader').modal('hide');
        $('#createOrderModalShow').modal('show');
        $("#createOrderId").text(respData.cardOrderID);
        $("#refName").text(body.referenceName);
        $("#noOfCards").text(body.numberOfCards);
        initGAEvent('event', 'Physical Gift Card', 'click', 'Create Order Cards, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', refname='+ body.referenceName +',numberOfCards='+ body.numberOfCards +',cardOrderID='+respData.cardOrderID,timestamp+'', + new Date().toLocaleString());
        
    }, function(respErr) {
        $('#showBtnLoader').hide();
        $('#create-order').attr('disable',false).css('background','#ffbc00');
        $('#myModalLoader').modal('hide');
        $('#ErrorstateX').modal('show');
        $('.errorState').html(respErr.responseJSON.message);
        initGAEvent('event', 'Physical Gift Card', 'click', 'Create Order Cards, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', error='+respErr.responseJSON.message+ '',timestamp+'', + new Date().toLocaleString());
    });
});



$('.addUserFields').keyup(function(){
    let $referenceName = $('#referenceName');
    let $numberOfCards = $('#numberOfCards');
    let $name = $('#name');
    let $phoneNumber = $('#phoneNumber');
    let $address = $('#address');
    let $zipcode = $('#zipcode');
    let $city = $('#city');
    let $state = $('#state');
    let $country = $('#country');
    let ifi = $("meta[name='ifi']").attr("content");
    
    let referenceName = $referenceName.val();
    let numberOfCards = $numberOfCards.val();
    let name = $name.val();
    let phoneNumber = $phoneNumber.val();
    let address = $address.val();
    let zipcode = $zipcode.val();
    let state = $state.val();
    let city = $city.val();
    let country = $country.val();
    let referenceNameisValid,numberOfCardsisValid,nameisValid,phoneNumberisValid,addressisValid,cityisValid,stateisValid,countryisValid,zipcodeisValid;
      if (referenceName == '' && $(this).attr('id') == 'referenceName') {
        $referenceName.addClass('input-has-error');
        $referenceName.siblings('.error-message').html('Please enter your reference name').show();
        referenceNameisValid = false;
      }else{
        $referenceName.removeClass('input-has-error');
        $referenceName.siblings('.error-message').html('Please enter your reference name').hide();
        referenceNameisValid = true;
      };
    
      if (numberOfCards == '' && $(this).attr('id') == 'numberOfCards') {
        $numberOfCards.addClass('input-has-error');
        $numberOfCards.siblings('.error-message').html('Please enter no of cards required').show();
        numberOfCardsisValid = false;
      }else if (numberOfCards.slice(0,1) == '-') { 
        $numberOfCards.addClass('input-has-error');
        $numberOfCards.siblings('.error-message').html('Please enter valid card value').show();
        numberOfCardsisValid = false;
      }
      else if(ifi == 'HDFC' && numberOfCards < 50){
        $numberOfCards.addClass('input-has-error');
        $numberOfCards.siblings('.error-message').html('Please order a minimum of 50 cards').show();
        numberOfCardsisValid = false;
      }
      else{
        $numberOfCards.removeClass('input-has-error');
        $numberOfCards.siblings('.error-message').html('Please enter your reference name').hide();
        numberOfCardsisValid = true;
      };
    
      if (name == '' && $(this).attr('id') == 'name') {
        $name.addClass('input-has-error');
        $name.siblings('.error-message').html('Please enter your name').show();
        nameisValid = false;
      }
      else{
        $name.removeClass('input-has-error');
        $name.siblings('.error-message').html('Please enter your reference name').hide();
        nameisValid = true;
      };
    
      if (phoneNumber == '' && $(this).attr('id') == 'phoneNumber') {
        $phoneNumber.addClass('input-has-error');
        $('.error-message-phone').html('Please enter your phone number').show();
        phoneNumberisValid = false;
      }
      else if (phoneNumber != '' && phoneNumber.length < 10){
        $phoneNumber.addClass('input-has-error');
        // $phoneNumber.siblings('.error-message').html('Please enter valid 10 digit phone number').show();
        $('.error-message-phone').html('Please enter valid 10 digit phone number').show();
        phoneNumberisValid = false;
      }
      else{
        $phoneNumber.removeClass('input-has-error');
        $('.error-message-phone').hide();
        phoneNumberisValid = true;
      };
    
      if (address == '' && $(this).attr('id') == 'address') {
            addError('Please enter your address');
      }
      else{
            if(address != ''){
                var fullAddressArray = $('#address').val().replace(/\n/g,'/n').split('/n')
                if(fullAddressArray.length <= 4){
                for(var i=0;i<fullAddressArray.length;i++){
                    if(fullAddressArray[i].length >= 50){
                        addError('Please enter maximum of 50 characters in each line');                        
                        return;
                    } else {
                        $address.removeClass('input-has-error');
                        $address.siblings('.error-message').hide();
                        addressisValid = true;
                    }
                }
            } else {
                addError('Please enter your address');
            }
        }         
      }; 
      
      function addError(error){        
        $address.addClass('input-has-error');
        $address.siblings('.error-message').html(error).show();
        addressisValid = false;
      }

      if (city == '' && $(this).attr('id') == 'city') {
        $city.addClass('input-has-error');
        $city.siblings('.error-message').html('Please enter your city').show();
        cityisValid = false;
      }
      else{
        $city.removeClass('input-has-error');
        $city.siblings('.error-message').hide();
        cityisValid = true;
      };
    
      if (state == '' && $(this).attr('id') == 'state') {
        $state.addClass('input-has-error');
        $state.siblings('.error-message').html('Please enter your state').show();
        stateisValid = false;
      }
      else{
        $state.removeClass('input-has-error');
        $state.siblings('.error-message').hide();
        stateisValid = true;
      };
    
      if (country == '') {
        $country.addClass('input-has-error');
        $country.siblings('.error-message').html('Please enter your country').show();
        countryisValid = false;
      }
      else{
        $country.removeClass('input-has-error');
        $country.siblings('.error-message').hide();
        countryisValid = true;
      };
    
      if (zipcode == '' && $(this).attr('id') == 'zipcode') {
        $zipcode.addClass('input-has-error');
        $zipcode.siblings('.error-message').html('Please enter your pincode').show();
        zipcodeisValid = false;
      }
      else if (zipcode.slice(0,1) == '-') {
        $zipcode.addClass('input-has-error');
        $zipcode.siblings('.error-message').html('Please enter valid pincode').show();
        zipcodeisValid = false;
      }
      else if (zipcode != '' && zipcode.length < 6) {
        $zipcode.addClass('input-has-error');
        $zipcode.siblings('.error-message').html('Please enter valid 6 digit pincode').show();
        zipcodeisValid = false;
      }
      else{
        $zipcode.removeClass('input-has-error');
        $zipcode.siblings('.error-message').hide();
        zipcodeisValid = true;
      };
    
    if ((referenceNameisValid== true) &&
        (numberOfCardsisValid== true)&&
        (nameisValid== true) && 
        (phoneNumberisValid== true) &&      
        (addressisValid== true )&&
        (cityisValid== true )&&
        (stateisValid== true) &&
        (countryisValid == true )&&
        (zipcodeisValid == true)) {
        $('.createPhysicalOrder').removeClass('btn-disabled');
        $('.createPhysicalOrder').prop('disabled', false);
    } else {
        $('.createPhysicalOrder').addClass('btn-disabled');
        $('.createPhysicalOrder').prop('disabled', true);
    }
    
    });
customCarousel();

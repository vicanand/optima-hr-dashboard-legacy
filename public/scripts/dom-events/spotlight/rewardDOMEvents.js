import RewardSvc from '../../service/spotlight/rewardSvc';
import RewardCtrl from '../../controller/spotlight/rewardCtrl';
import Storage from '../../common/webStorage';
import FundinSvc from '../../service/fundingSvc';
import Meta from '../../common/metaStorage';
var manual, bulk = false;


export default {

     selectProgramCode : function () {
        $('#card-selection-loader').modal();
        $('.rewardGift').removeClass('active');
        $('.rewardGiftDummy').removeClass('active');
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        }
        else{
            $(this).addClass('active');
        }
        let cardProgramIdX = $(this).data('id');
        let bg = $(this).data('img');
        let title = $(this).data('title'); 
        let fundingID = $(this).data('fundingid');
        Storage.setSessionCollection('bg', bg);
        Storage.setSessionCollection('title', title);
        Storage.setSessionCollection('fundingID', fundingID);

        initGAEvent('event', 'Reward - Card-based', 'click', 'Template Selected, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', cardname='+title+ ', cardprogramid='+ cardProgramIdX +',timestamp=' + new Date().toLocaleString());

        $('.rewardPImg').attr('src',Storage.getSessionCollection('bg'));
        $('.rewardPEImg').attr('src',Storage.getSessionCollection('bg'));
        $('.rewardPtitle').html(Storage.getSessionCollection('title'));
        RewardSvc.getTemplateByIdDetails(cardProgramIdX).then(function(respData) {
            $('#card-selection-loader').modal('hide')
            $('#rewardTemplateSelected').removeAttr('disabled').removeClass('btn-disabled');
            $('.rewardBlankState').hide();
            $('.rewardMobileDemo').show();
            $('.rewardEmailDemo').show();
            console.log(respData);
            var response = respData.cardProgramTemplateData;
            var templateID = respData.cardProgramTemplateData.templateID;
            var message = respData.cardProgramTemplateData.message;
            var greeting = respData.cardProgramTemplateData.greeting;
            var cardProgramID = respData.cardProgramTemplateData.cardProgramID;
            Storage.setSessionCollection('templateID', templateID);
            Storage.setSessionCollection('message', message);
            Storage.setSessionCollection('greeting', greeting);
            Storage.setSessionCollection('cardProgramID', cardProgramID);
            $('.rewardPEtitle').html(Storage.getSessionCollection('greeting') + '&lt;Beneficiary Name&gt;');
            $('.rewardPEmessage').html(Storage.getSessionCollection('message'));
            initGAEvent('event', 'Reward - Card-based', 'click', 'set up new program , corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', cardname='+respData.cardProgramTemplateData.title+ ', cardprogramid='+ cardProgramID +',timestamp=' + new Date().toLocaleString());
            
        }, function(respErr) {
            $('#card-selection-loader').modal('hide')
            $('#rewardTemplateSelected').attr('disabled','disabled').addClass('btn-disabled');
            $('.rewardBlankState').show();
            $('.rewardMobileDemo').hide();
            $('.rewardEmailDemo').hide();
            $("#createOrderModalSGOops").modal("show");
                console.log(respErr);
                initGAEvent('event', 'Reward - Card-based', 'click', 'set up new program error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', cardname='+title+ ', respErr='+ respErr +',cardprogramid='+ cardProgramIdX +',timestamp=' + new Date().toLocaleString());
            });
            
      },

      createNxt : function () {
        manual = false;
        bulk = true;
        Storage.setSessionCollection('manual',manual);
        Storage.setSessionCollection('bulk',bulk);
        $('#uploadStep').addClass('done');
        $('#verifyStep').addClass('active');
        $('#addBenefitsBulk').hide();
        $('#createOrder').show();
        var fundingID = Storage.getSessionCollection('fundingID');
        var uploadDetails = $('#uploadDetails').val();
        console.log(uploadDetails);
        $('#fundingAccountID option[value='+fundingID+']').attr('selected', 'selected');
        var newBalance = $('#fundingAccountID option:selected').attr('data-balance');
        $('#balanceFund').html(newBalance);
            if(newBalance < 1 ){
                $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').show();
                $('.placeOrderEnd').hide();
            }
            else{
                $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').hide();
                $('.placeOrderEnd').show();
            }
            var targetUrl = $(this).data('url');
            //var targetTitle = $(this).data('url');
            window.history.pushState({url: "" + targetUrl + ""}, targetUrl);
            window.location.hash=targetUrl;
      },

      createOrder: function () {
        $('.spinner-loader').show();
        var fundingAccountId = Storage.getSessionCollection('fundingID');
        var fileName = Storage.getSessionCollection('fileName');
        var key= Storage.getSessionCollection('key');
        var programId= (Storage.getSessionCollection('cardProgramID')).toString();
        $('#fundingAccountID option[value='+ fundingAccountId +']').attr('selected', 'selected');
         RewardSvc.orderCreate(fundingAccountId,fileName,key,programId).then(function(respData) {
             console.log(respData);
             $('.spinner-loader').hide();
             $("#createOrderModalShow").modal("show");
            $('#verifyStep').addClass('done');
            $('#createOrderId').html(respData.id);
            Storage.setSessionCollection('fundingID','');
           
            
         }, function(respErr) {
                 console.log(respErr);
                 $('.spinner-loader').hide();
                 $("#createOrderModalLoader").modal("hide");
                 $("#createOrderModalSGOops").modal("show");
        });
      },

      createDummyCustomCard:function () {
          $('#card-selection-loader').modal();
        $('.rewardGift').removeClass('active');
        $('.rewardGiftDummy').removeClass('active');
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        }
        else{
            $(this).addClass('active');
        }
        var dummyCardName = $(this).data('title');
        var dummyBg=$(this).data('img');
        var dummyMsg=$(this).data('msg');
        var dummyGreeting=$(this).data('gretting');
        Storage.setSessionCollection('bg', dummyBg);
        Storage.setSessionCollection('title', dummyCardName);
        Storage.setSessionCollection('cutomCardGreeting', dummyGreeting);
        Storage.setSessionCollection('cutomCardMessage', dummyMsg);
        Storage.setSessionCollection('greeting', dummyGreeting);
        Storage.setSessionCollection('message', dummyMsg);
        var cardName = Storage.getSessionCollection('title');
        var bg=Storage.getSessionCollection('bg');
        var fundingAccountID;
        var message= Storage.getSessionCollection('cutomCardMessage');
        var greeting = Storage.getSessionCollection('cutomCardGreeting');
        let authToken = Storage.get('authToken');
        let corpID= $("meta[name='corpID']").attr("content");
        let name = "reward";
        let fundingAccountDetails = [];

       FundinSvc.getFundingAccount(authToken, corpID, 1, 10).then(function(fundRespData) {
            
        fundingAccountDetails.fundingAccounts = fundRespData;
        let totalFundCount = fundingAccountDetails.fundingAccounts.count;
        let pageNum = 2; 
        let totalPages = Math.ceil(totalFundCount/10); 
        let fundingAccPromise = [];   
        for(pageNum; pageNum<totalPages+1; pageNum++){
            fundingAccPromise.push(FundinSvc.getFundingAccount(authToken, corpID, pageNum, 10));
        }
        Promise.all(fundingAccPromise).then(function(respArr){
            respArr.forEach(function(fundResData){
                let fundAccDet = fundResData;
                fundAccDet.fundingAccounts.forEach(function(accDetails){
                    //fundingAccountDetails.push(accDetails);
                    fundingAccountDetails["fundingAccounts"].fundingAccounts.push(accDetails);
                });
            });

            console.log(fundingAccountDetails);
            for(var i=0;i<fundingAccountDetails.fundingAccounts.fundingAccounts.length;i++){
                if(fundingAccountDetails.fundingAccounts.fundingAccounts[i].name && fundingAccountDetails.fundingAccounts.fundingAccounts[i].name.toLowerCase()=='reward')
                {
                    fundingAccountID = fundingAccountDetails.fundingAccounts.fundingAccounts[i].accountID;
                    Storage.setSessionCollection('fundingID', fundingAccountID);
                    break;
                }
                //else{fundingAccountID=null;}
            } 
            if(fundingAccountID==undefined){

                RewardSvc.setFundAcc(name).then(function(respDataX) {
                    console.log(respDataX);
                    fundingAccountID= respDataX.accountID;
                    Storage.setSessionCollection('fundingID', fundingAccountID);
                    RewardSvc.createCustomCard(cardName,bg,fundingAccountID,message,greeting).then(function(respData) {
                        var programId= respData.id;
                        Storage.setSessionCollection('cardProgramID', programId);
                        $('.rewardPImg').attr('src',Storage.getSessionCollection('bg'));
                        $('.rewardPEImg').attr('src',Storage.getSessionCollection('bg'));
                        $('.rewardPtitle').html(Storage.getSessionCollection('title'));
                        $('#rewardTemplateSelected').removeAttr('disabled').removeClass('btn-disabled');
                        $('.rewardBlankState').hide();
                        $('.rewardMobileDemo').show();
                        $('.rewardEmailDemo').show();
                        $('.rewardPEtitle').html(Storage.getSessionCollection('greeting') + '&lt;Beneficiary Name&gt;');
                        $('.rewardPEmessage').html(Storage.getSessionCollection('message'));
                        $('#card-selection-loader').modal('hide');
                       
                    }, function(respErr) {
                            console.log(respErr);
                            $('#card-selection-loader').modal('hide');
                            $("#createOrderModalSGOops").modal("show");
                   });
                 }, function(respErrX) {
                     console.log(respErrX);
                     $('#card-selection-loader').modal('hide');
                     $("#createOrderModalSGOops").modal("show");
                 });
            }
            else{
                RewardSvc.createCustomCard(cardName,bg,fundingAccountID,message,greeting).then(function(respData) {
                    console.log(respData);
                    var programId= respData.id;
                    Storage.setSessionCollection('cardProgramID', programId);
                        $('.rewardPImg').attr('src',Storage.getSessionCollection('bg'));
                        $('.rewardPEImg').attr('src',Storage.getSessionCollection('bg'));
                        $('.rewardPtitle').html(Storage.getSessionCollection('title'));
                        $('#rewardTemplateSelected').removeAttr('disabled').removeClass('btn-disabled');
                        $('.rewardBlankState').hide();
                        $('.rewardMobileDemo').show();
                        $('.rewardEmailDemo').show();
                        $('.rewardPEtitle').html(Storage.getSessionCollection('greeting') + '&lt; Beneficiary Name &gt;');
                        $('.rewardPEmessage').html(Storage.getSessionCollection('message'));
                        $('#card-selection-loader').modal('hide');
                }, function(respErr) {
                        console.log(respErr);
                        $('#card-selection-loader').modal('hide');
                        $("#createOrderModalSGOops").modal("show");
               });
            }
           
        }).catch(function(err){
            console.log('error',err)
        });
    }, function(respErr) {
        console.log(respErr);
        $("#createOrderModalSGOops").modal("show");
    });
    },

      createCustomCard: function () {
        $('#custom-design-loader').show();
        var cutomCardTitle = $('#cutomCardTitle').val();
        Storage.setSessionCollection('cutomCardTitle', cutomCardTitle);
        var cutomCardMessage = $.trim($('#cutomCardMessage').val());
        Storage.setSessionCollection('cutomCardMessage', cutomCardMessage);
        var cardName = Storage.getSessionCollection('cutomCardName');
        var bg=Storage.getSessionCollection('custombg');
        var fundingAccountID;
        var message= Storage.getSessionCollection('cutomCardMessage');
        var greeting = Storage.getSessionCollection('cutomCardTitle');
        let authToken = Storage.get('authToken');
        let corpID= $("meta[name='corpID']").attr("content");
        let name = "reward";
        let fundingAccountDetails = [];
        
       FundinSvc.getFundingAccount(authToken, corpID, 1, 10).then(function(fundRespData) {
            
        fundingAccountDetails.fundingAccounts = fundRespData;
        let totalFundCount = fundingAccountDetails.fundingAccounts.count;
        let pageNum = 2; 
        let totalPages = Math.ceil(totalFundCount/10); 
        let fundingAccPromise = [];   
        for(pageNum; pageNum<totalPages+1; pageNum++){
            fundingAccPromise.push(FundinSvc.getFundingAccount(authToken, corpID, pageNum, 10));
        }
        Promise.all(fundingAccPromise).then(function(respArr){
            $('#custom-design-loader').hide()
            respArr.forEach(function(fundResData){
                let fundAccDet = fundResData;
                fundAccDet.fundingAccounts.forEach(function(accDetails){
                    //fundingAccountDetails.push(accDetails);
                    fundingAccountDetails["fundingAccounts"].fundingAccounts.push(accDetails);
                });
            });
            console.log(fundingAccountDetails);
            for(var i=0;i<fundingAccountDetails.fundingAccounts.fundingAccounts.length;i++){
                if(fundingAccountDetails.fundingAccounts.fundingAccounts[i].name && fundingAccountDetails.fundingAccounts.fundingAccounts[i].name.toLowerCase()=='reward')
                {
                    fundingAccountID = fundingAccountDetails.fundingAccounts.fundingAccounts[i].accountID;
                    Storage.setSessionCollection('fundingID', fundingAccountID);
                    break;
                }
                //else{fundingAccountID=null;}
            } 
            if(fundingAccountID==undefined){
                RewardSvc.setFundAcc(name).then(function(respDataX) {
                    console.log(respDataX);
                    fundingAccountID= respDataX.accountID;
                    Storage.setSessionCollection('fundingID', fundingAccountID);
                    RewardSvc.createCustomCard(cardName,bg,fundingAccountID,message,greeting).then(function(respData) {
                        console.log(respData);
                        $('#newTemplateCreate').modal('hide');
                        $("#createOrderModalShowCustom").modal("show");
                        initGAEvent('event', 'Rewards Card Based', 'click', 'create Custom Card BUTTON clicked, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', cardname='+cardName+ ', cardprogramid='+ respData.cardProgramID +',timestamp=' + new Date().toLocaleString());
                    }, function(respErr) {
                            console.log(respErr);
                            //$("#createOrderModalLoader").modal("hide");
                            $("#createOrderModalSGOops").modal("show");
                            initGAEvent('event', 'Rewards Card Based', 'click', 'create Custom Card Error , corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', cardname='+cardName+ ',timestamp=' + new Date().toLocaleString());
                   });
                 }, function(respErrX) {
                     console.log(respErrX);
                     $("#createOrderModalSGOops").modal("show");
                 });
            }
            else{
                RewardSvc.createCustomCard(cardName,bg,fundingAccountID,message,greeting).then(function(respData) {
                    console.log(respData);
                    $('#newTemplateCreate').modal('hide');
                    $('#custom-design-loader').hide();
                    $("#createOrderModalShowCustom").modal("show");
                    initGAEvent('event', 'Rewards Card Based', 'click', 'create Custom Card BUTTON clicked, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', cardname='+cardName+ ', cardprogramid='+ respData.cardProgramID +',timestamp=' + new Date().toLocaleString());
                   
                }, function(respErr) {
                        console.log(respErr);
                        //$("#createOrderModalLoader").modal("hide");
                        $('#custom-design-loader').hide();
                        $("#createOrderModalSGOops").modal("show");
                        initGAEvent('event', 'Rewards Card Based', 'click', 'create Custom Card Error , corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', cardname='+cardName+ ',timestamp=' + new Date().toLocaleString());
               });
            }
           
        }).catch(function(err){
            console.log('error',err);
            $('#custom-design-loader').hide();
            initGAEvent('event', 'Rewards Card Based', 'click', 'create Custom Card Error , corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', cardname='+cardName+ ',timestamp=' + new Date().toLocaleString());
        });
    }, function(respErr) {
        console.log(respErr);
        $('#custom-design-loader').hide();
        $("#createOrderModalSGOops").modal("show");
        initGAEvent('event', 'Rewards Card Based', 'click', 'create Custom Card Error , corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', cardname='+cardName+ ',timestamp=' + new Date().toLocaleString());
    });
            

       

    },

      addNewTemplateCode: function () {
        $('#newTemplateCreate').modal('show');
        initGAEvent('event', 'Reward - Card-based', 'click', 'new Template Create Selected, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+',' +',timestamp=' + new Date().toLocaleString());
      },

      addNewTemplateChoose: function () {
        let bg = $(this).data('bg');
        let id = $(this).data('id');
        Storage.setSessionCollection('custombg', bg);
        Storage.setSessionCollection('customid', id);
        $('.templateChoose').removeClass('active');
        $('.chooseTick').hide();
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).find('img.chooseTick').hide();
        }
        else{
            $(this).addClass('active');
            $(this).find('img.chooseTick').show();
        }
        $('.imageonPhoneCard').attr('src',Storage.getSessionCollection('custombg'));
        RewardCtrl.validationForCustomOrder();
      },

      closePhone: function () {
        $('#demoX').removeClass('in');
        $('#previewBtn1').removeClass('openCollapse');
        $('#previewBtn1').addClass('collapsed');
        $('#previewBtn2').addClass('openCollapse');
        $('#demoY').addClass('in');
        $('.editGear').show();
        $('.circleTick').show();
        $('#circleNumber').hide();
        initGAEvent('event', 'Reward - Card-based', 'click', 'Create your own custom design - Email card title done, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', cutomCardName='+$('#cutomCardName').val()+ ', cardprogramid='+ Storage.getSessionCollection('cutomCardName') +',timestamp=' + new Date().toLocaleString());
      },

      showBenefits: function () {
    //setCurrentPage(targetUrl);
        $('#chooseDesign').hide();
        $('#addBenefits').show();
        $('#chooseStep').addClass('done');
        $('#uploadStep').addClass('active');
        var targetUrl = $(this).data('url');
        //var targetTitle = $(this).data('url');
        window.history.pushState({url: "" + targetUrl + ""}, targetUrl);
        window.location.hash=targetUrl;
      },

      showBenefitsBulk: function () {
        $('#chooseDesign').hide();
        $('#addBenefits').hide();
        $('#addBenefitsBulk').show();
       
      },

      showManually: function () {
        $('#chooseDesign').hide();
        $('#addBenefits').show();
        $('#addBenefitsBulk').hide();
        
      },

     validationForTransfer: function() {
         if ( RewardCtrl.setFlag() != 0) {
             $('#create-order').removeAttr('disabled');
             $('#create-order').removeClass('btn-disabled');
         }
     },

     validationForManualTransfer: function() { 
            if(($('#is-all-fields-filled').val() != '')) {
                    $('#rewardSingleSelected').removeAttr('disabled').removeClass('btn-disabled');            
            }else{
                $('#rewardSingleSelected').attr('disabled','disabled').addClass('btn-disabled');       
            }     
       
    },

   validateFile : function() {
       // validationForTransfer();
        if ( RewardCtrl.setFlag() != 0) {
            $('#create-order').removeAttr('disabled');
            $('#create-order').removeClass('btn-disabled');
        }
        //let productType = $('#currentProductType').val();
            $("#myModalSuccess").modal({
              "backdrop": "static",
               "show": true,
               keyboard: false
            });
    },
    retryErrorModal: function() {
        $('#createBulkOrderModalOops').modal('hide');
    },

    prepareGAEvent: function() {
        initGAEvent('event', $(this).attr('id'), 'click', 'Spotlight');
    },

    
    fundingAccountIDChange: function() {
        let $createProgram = $('#changeAccSG'),
            //$programName = $('#programName'),
            $accountID = $('#fundingAccountID'),
            $showHideNewFundingAcc = $('#shownewfundingaccname'),
            $NewFundingAccName = $('#newfundingaccname');

        if ($accountID.find(":selected").text() == 'Create New') {
            $showHideNewFundingAcc.show();
            $('#changeAccSG').hide();
            $('#setAccSG').show();
            if ($accountID.val() == 0 || !$NewFundingAccName.val() || $NewFundingAccName.val().length > 40) {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
                $('#newfundingaccname').val(null)
            } else {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
                $('#newfundingaccname').val(null)
            }
        } else {
            $showHideNewFundingAcc.hide();
            $('#changeAccSG').show();
            $('#setAccSG').hide();
            if ($accountID.val() == 0) {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
                $('#newfundingaccname').val(null)
            } else {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
                $('#newfundingaccname').val(null)
            }
        }
    },
    newFundingAccountName: function() {
        let $createProgram = $('#setAccSG'),
            //$programName = $('#programName'),
            $accountID = $('#fundingAccountID'),
            $showHideNewFundingAcc = $('#shownewfundingaccname'),
            $NewFundingAccName = $('#newfundingaccname');

        if ($accountID.val() == 0 || !$NewFundingAccName.val() || $NewFundingAccName.val().length > 40) {
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $createProgram.removeAttr('disabled');
            $createProgram.removeClass('btn-disabled');
        }
       // Storage.set('changeFundingAccName', $NewFundingAccName.val())
        let accountName = $NewFundingAccName.val();
        let allfundingAccountName = JSON.parse(localStorage.getItem('allFundingAccountName'));
        if (allfundingAccountName.indexOf(accountName) > -1) {
            $('#showfundingAccDuplicate').show();
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $('#showfundingAccDuplicate').hide();
        }
    },

    redirectUrl: function(){    
        $('#createOrderModalShow').modal('hide');
        $('#resetFilter').trigger('click');
        //$('#chooseDesign').show();
        //$('#createOrder').hide();

        window.location.reload();
    },

    showManuallyOnStep: function () {
        if($('#chooseStep').hasClass('done')){
            $('#chooseStep').addClass('done');
            $('#uploadStep').addClass('active');
            $('#uploadStep').removeClass('done');
        $('#verifyStep').removeClass('active');
        if(sessionStorage.getItem('bulk') == 'true'){
            $('#addBenefits').hide( );
            $('#addBenefitsBulk').show();
        }else{
            $('#addBenefits').show();
            $('#addBenefitsBulk').hide( );
        }
        $('#chooseDesign').hide( );
       
        $('#createOrder').hide();
        }
    },

    showChooseDesign: function () {
        if($('#chooseStep').hasClass('done')){
        $('#chooseDesign').show();
        $('#addBenefits').hide();
        $('#addBenefitsBulk').hide();
        $('#createOrder').hide();
    }         
    },

    showVerifyAndPay: function () {
        if($('#uploadStep').hasClass('done')){
        $('#chooseDesign').hide( );
        $('#addBenefits').hide( );
        $('#addBenefitsBulk').hide( );
        $('#createOrder').show();
    }         
    },

    createManualFile: function(){
        manual = true;
        bulk = false;
        Storage.setSessionCollection('manual',manual);
        Storage.setSessionCollection('bulk',bulk);
         
    $('.spinner-loader').show();
    var arrayBeneficiary = [];
    let csvContent = [];
    let fileUploadUrl;
    let authToken = Storage.get('authToken');
    let corpID= $("meta[name='corpID']").attr("content");
    let companyID= $("meta[name='companyID']").attr("content");
        $('.rewardBenefitInputDv').each(function(){
            var beneficiaryObj = {
                "name" : $(this).find('input:eq(0)').val(),
                "emailOrMobile": $(this).find('input:eq(3)').val(),
                "amount": $(this).find('input:eq(1)').val(),
                "transferDate": $(this).find('input:eq(4)').val(),
                "panOrAadhaar": $(this).find('input:eq(2)').val()
            };
            arrayBeneficiary.push(beneficiaryObj);
        });
        
        for(var i=0;i<arrayBeneficiary.length;i++){
            for (var key in arrayBeneficiary[i]) {
                if (arrayBeneficiary[i][key] == ""){  
                    $('.spinner-loader').hide();
                    $("#error-required-oops").modal("show");
                    return ;
                }                   
            } 
        }
                      
        var targetUrl = $(this).data('url');
        window.history.pushState({url: "" + targetUrl + ""}, targetUrl);
        window.location.hash=targetUrl;  
        // var array = arrayBeneficiary;
        // let headers = "Name,Email or Mobile,Amount,Transfer Date,PAN or Aadhaar";
        // csvContent.push(headers);
        // array.forEach(function (obj) {
        //     let row = "";
        //     row = [obj.name, obj.emailOrMobile, obj.amount, obj.transferDate, obj.panOrAadhaar].join(",");
        //     JSON.stringify(row);
        //     console.log(row);
        //     csvContent.push(row);
        // });
        // csvContent = csvContent.join(",\n");
        // console.log(csvContent);
        // var uploadDetails = $('#uploadDetails').val();
        
        // uploadDetails = JSON.parse(uploadDetails);
        // fileUploadUrl = uploadDetails.postAction;
        // console.log(fileUploadUrl);

        // var formData = new FormData();
        // formData.append("key", uploadDetails.key);
        // formData.append("acl", uploadDetails.acl);
        // formData.append("x-amz-credential", uploadDetails["xAmzCredential"]);
        // formData.append("x-amz-algorithm", uploadDetails["xAmzAlgorithm"]);
        // formData.append("success_action_status", uploadDetails.successActionStatus);
        // formData.append("x-amz-date", uploadDetails["xAmzDate"]);
        // formData.append("policy", uploadDetails.policy);
        // formData.append("x-amz-signature", uploadDetails["xAmzSignature"]);
        // formData.append('content-type', 'text/csv');      

        // var fileName = companyID + "rewardFile.csv";
        // var file = new File([csvContent], fileName, { type: ".csv", lastModified: new Date() });
        // formData.append('file', file);

        // var xhr = new XMLHttpRequest();
        try{
             
        var array = arrayBeneficiary;
        let headers = "Name,Email or Mobile,Amount,Transfer Date,PAN or Aadhaar";
        csvContent.push(headers);
        array.forEach(function (obj) {
            let row = "";
            row = [obj.name, obj.emailOrMobile, obj.amount, obj.transferDate, obj.panOrAadhaar].join(",");
            JSON.stringify(row);
            console.log(row);
            csvContent.push(row);
        });
        csvContent = csvContent.join(",\n");
        console.log(csvContent);
        var uploadDetails = $('#uploadDetails').val();
        
        uploadDetails = JSON.parse(uploadDetails);
        fileUploadUrl = uploadDetails.postAction;
        console.log(fileUploadUrl);

        var formData = new FormData();
        formData.append("key", uploadDetails.key);
        formData.append("acl", uploadDetails.acl);
        formData.append("x-amz-credential", uploadDetails["xAmzCredential"]);
        formData.append("x-amz-algorithm", uploadDetails["xAmzAlgorithm"]);
        formData.append("success_action_status", uploadDetails.successActionStatus);
        formData.append("x-amz-date", uploadDetails["xAmzDate"]);
        formData.append("policy", uploadDetails.policy);
        formData.append("x-amz-signature", uploadDetails["xAmzSignature"]);
        formData.append('content-type', 'text/csv');      

        var fileName = companyID + "rewardFile.csv";
        var file = new File([csvContent], fileName, { type: ".csv", lastModified: new Date() });
        formData.append('file', file);

        var xhr = new XMLHttpRequest();

            xhr.open('POST', uploadDetails.postAction, true);
            xhr.setRequestHeader("Cache-Control", "");
            xhr.setRequestHeader("X-Requested-With", "");
            xhr.setRequestHeader("X-File-Name", "");
            // xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
            // xhr.setRequestHeader("Access-Control-Allow-Headers", "*");
            xhr.send(formData);           
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    var responseDoc = xhr.responseXML;
                    Storage.setSessionCollection('fileName', file.name);
                    Storage.setSessionCollection('key', uploadDetails.key);
                    console.log(responseDoc)
                    let key = xhr.response.split('<Key>')[1].split('</Key>')[0];
                    let etag = xhr.response.split('<ETag>')[1].split('</ETag>')[0].replace('"','').replace('"','');
                    let fileUrl = xhr.response.split('<Location>')[1].split('</Location>')[0] + '?key=' + key + '&etag=' + etag;
                    Storage.setSessionCollection('storage-fileurl', fileUrl);

                    console.log(Storage.getSessionCollection('key'));
                    $('#uploadStep').addClass('done');
                    $('#verifyStep').addClass('active');
                    $('#addBenefits').hide();
                    $('#createOrder').show();
                    var fundingID = Storage.getSessionCollection('fundingID');
                    //var uploadDetails = $('#uploadDetails').val();
                    console.log(uploadDetails);
                    $('#fundingAccountID option[value='+fundingID+']').attr('selected', 'selected');
                    var newBalance = $('#fundingAccountID option:selected').attr('data-balance');
                    $('#balanceFund').html(newBalance);
                    if(newBalance < 1 ){
                        $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').show();
                        $('.placeOrderEnd').hide();
                    }
                    else{
                        $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').hide();
                        $('.placeOrderEnd').show();
                    }
                }
            };
            $('.spinner-loader').hide()
        }catch(err){
            console.log(err);
            $('.spinner-loader').hide()
            $("#createOrderModalSGOops").modal("show");
        }
    
    },

    validateEmail: function(){
        console.log('called')
       
    },

    // closeOpenAccordian: function(){
    //     if($('#demoX').hasClass('in')){
    //         $('#demoY').addClass('in');
    //         $('#demoX').removeClass('in');
    //         $('#previewBtn1').removeClass('openCollapse');
    //         $('#previewBtn2').addClass('openCollapse');
    //         $('#previewBtn1').addClass('collapsed');
    //     }
    //     else{
    //         $('#demoX').addClass('in');
    //         $('#demoY').removeClass('in');
    //         $('#previewBtn1').addClass('openCollapse');
    //         $('#previewBtn2').removeClass('openCollapse');
    //         $('#previewBtn1').removeClass('collapsed');
    //     }
    // },
    // closeOpenAccordianY: function(){
    //     if($('#demoY').hasClass('in')){
    //         $('#demoX').addClass('in');
    //         $('#demoY').removeClass('in');
    //         $('#previewBtn2').removeClass('openCollapse');
    //         $('#previewBtn1').addClass('openCollapse');
    //         $('#previewBtn2').addClass('collapsed');
    //     }
    //     else{
    //         $('#demoY').addClass('in');
    //         $('#demoX').removeClass('in');
    //         $('#previewBtn2').addClass('openCollapse');
    //         $('#previewBtn1').removeClass('openCollapse');
    //         $('#previewBtn2').removeClass('collapsed');
    //     }
    // },

    // changeFundingAccount: function(){
    //     console.log('hi');
    //     let authToken = Storage.get('authToken');
    //     let fundingAccID = $("#fundingAccountID option:selected").val();
    //     let corpID = $("meta[name='corpID']").attr("content");
    //     Storage.setSessionCollection('fundingAccountId', fundingAccID);
    //     $('#createSelectGiftingOrder').attr('data-fundingaccountid', Storage.getSessionCollection('fundingAccountId'));
    //     $('#createOrderDone').attr('data-fundingaccountids', Storage.getSessionCollection('fundingAccountId'));
    //     SelectGiftSvc.getAccDetails(authToken, corpID, fundingAccID).then(function(fundRespData) {
    //         let fundingAcc = fundRespData;
    //            $('.accBalance').html((fundingAcc.balance)/100);
    //            $('.accName').html(fundingAcc.name);
    //            Storage.setSessionCollection('fundingAccountName', fundingAcc.name);
    //            Storage.setSessionCollection('fundingAccountBalance', (fundingAcc.balance)/100);
    //            Storage.setSessionCollection('ifiID', (fundingAcc.ifiID));
    //            $('#changeFundAccSg').modal('hide');
    //            //return fundingAcc;
    //    }, function(respErr) {
    //        console.log(respErr);
    //    });
      
    // },

    // setFundingAccount: function(){
    //     let fundingAccName = $('#newfundingaccname').val();
    //     let inputObj = {
    //         'corpID': $("meta[name='corpID']").attr("content"),
    //         'name': fundingAccName
    //     }
    //     let authToken = Storage.get('authToken');
    //     SelectGiftSvc.setFundAcc(inputObj).then(function(respData) {
            
    //         Storage.setSessionCollection('fundingAccountId', respData.accountID);
    //         Storage.setSessionCollection('fundingAccountName', respData.name);
    //         Storage.setSessionCollection('fundingAccountBalance', 0);
    //         Storage.setSessionCollection('ifiID', (respData.ifiID));
    //         $('#createSelectGiftingOrder').attr('data-fundingaccountid', Storage.getSessionCollection('fundingAccountId'));
    //         $('#createOrderDone').attr('data-fundingaccountids', Storage.getSessionCollection('fundingAccountId'));

    //         $('.accBalance').html(0);
    //         $('.accName').html(respData.name);
    //         $('#changeFundAccSg').modal('hide');
             
    //     }, function(respErr) {
           
    //     });
    // },


};

import DocumentDetailsLtaSvc from "../service/docDetailsLtaSvc";
import DocumentItemTemplate from "../../template/documentDetailsLTA.ejs";
import DomEventHandler from '../common/domEventHandler';
import Util from '../common/util';
import Storage from '../common/webStorage'

let _changeChecklist = function(token, docId) {
    DocumentDetailsLtaSvc.getSearchDocs(token, docId).then(function(respData) {
        $('#searchingTxtDetails').hide();
        let pageData = {};
        let checkList = [];
        let billUrls = [];
        let billDetails = respData;
        let ltaType = $('#ltaType').val();
        console.log(ltaType);
        if (ltaType != "pol") {
            if (billDetails.trips && billDetails.trips[ltaType] && billDetails.trips[ltaType].tripAttrs && billDetails.trips[ltaType].tripAttrs.checklist) {
                checkList = JSON.parse(Util.htmlDecoder(billDetails.trips[ltaType].tripAttrs.checklist));
            }
            if (billDetails.trips && billDetails.trips[ltaType].supportingDocuments) {
                for (var i = 0; i < billDetails.trips[ltaType].supportingDocuments.length; i++) {
                    for (var j = 0; j < billDetails.trips[ltaType].supportingDocuments[i].imageUrls.length; j++) {
                        billUrls.push((billDetails.trips[ltaType].supportingDocuments[i].imageUrls[j]).replace(/&amp;/g, '&'));
                    }
                }
            }
        } else {
            if (billDetails.attrs && billDetails.attrs.checkList) {
                checkList = JSON.parse(Util.htmlDecoder(billDetails.attrs.checkList));
            }
            for (var i = 0; i < billDetails.proofOfLeave.length; i++) {
                //for(var j = 0; j< billDetails.trips[ltaType].supportingDocuments[i].imageUrls.length; j++){
                billUrls.push((billDetails.proofOfLeave[i]).replace(/&amp;/g, '&'));
                //}
            }
        }
        pageData.checkList = checkList;
        pageData.billUrls = billUrls;
        console.log('bill', billUrls, checkList);
        DomEventHandler.renderMyTemplate("ckeckListDetails", DocumentItemTemplate, pageData);
    });

}

export default {

    changeChecklist: function(e) {
        $('#searchingTxtDetails').show();
        let documents = 'test';
        let docId = $('#docId').val().replace('biller/claims/','');
        let token = Storage.get('authToken');
        _changeChecklist(token, docId);
    }
}
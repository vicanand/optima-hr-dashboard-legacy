import ServiceConnector from '../common/serviceConnector';
import ReportCentreSvc from '../service/reportCentreSvc.js';
import Urls from '../common/urls.js';
import cacheDataStorage from '../common/cacheDataStorage.js';
import Meta from '../common/metaStorage';
import Util from "../common/util";
import ExpressTable from '../widgets/expressTable';
import listTable from '../../template/report-centre/table/report-list.ejs';
import Storage from '../common/webStorage'

var uiElements = {
  $closeModalBtn: $('#closeModalBtn'),
  $fundingAccount: $('#fundingAccount'),
  $programs: $('#programs'),
  $datePicker: $('.date-picker'),
  $downloadReport: $('#downloadReport'),
  $inProcessReport: $('#inProcessReport'),
  $generateReport: $('#generateReport'),
  $tryAgainBtn: $('#tryAgainBtn'),
  $reportModalHeading: $('#reportModalHeading'),
  $loadingReportText: $('#loadingReportText'),
  $reportModal: $('#reportModal'),
  $loadingImage: $('.loading-image'),
  $errorIcon: $('.error-icon'),
  $successIcon: $('.success-icon'),
  $inProcessIcon: $('.inprocess-icon'),
  $inTriggeredIcon: $('.triggered-icon'),
  $reportId: $('#reportId'),
  $modalboxFormSection: $('.modalbox-form-section'),
  FUNDING_ACCOUNT_IDS: $('#fundingAccountId'),
  PROGRAM_IDS: $('#programId'),
  PROGRAM_IDS_MULTIPLE: $('#programIdMultiple'),
  START_DATE: $('#start_date'),
  END_DATE: $('#end_date'),
  $reportNote: $('#reportNote'),
  // new RB
  $companies: $('#companies'),
  $COMPANY_IDS: $('#companiesId'),
  $CORP_ID: $("meta[name='corpID']").attr("content"),
  $PRODUCT_TYPEID: $('#productTypeId'),
  $producttype: $('#producttype'),
  $onlyCompanyId: $('#onlyCompanyId'),
  $onlyCompanyIds: $('#onlyCompanyIds')
};

var reportName = "";
var reportParameters = "";

function btnState(programId, startDate, endDate, fundingAccountId, productTypeId, companiesId, onlyCompanyIds) {
  for (var i = 0; i < arguments.length; i++) {
    if (arguments[i] == '') {
      uiElements.$generateReport.attr('disabled', 'disabled')
        .addClass('btn-disabled');
      return;
    } else {
      uiElements.$generateReport.removeAttr('disabled')
        .removeClass('btn-disabled');
    }
  }
  if (arguments.length == 0) {
    uiElements.$generateReport.removeAttr('disabled')
      .removeClass('btn-disabled');
  }

}

function loadTable() {
  uiElements.$reportModal.modal('hide');
  let text = $("#filterReportOnState")[0].value;
  let filters = {
    reportStates: []
  };
  if (!text || text === 'ALL') {
    filters.reportStates = ["AVAILABLE", "TRIGGERED", "FAILED"];
  } else {
    filters.reportStates.push(text);
  }

  if ($(".nav-tabs li").length > 1) {
    $('.nav-tabs a[href="#' + "listReport" + '"]').tab('show');
    var target = $(this).attr("data-tab");
    $('#' + target).siblings().removeClass('active');
    $('#' + target).addClass('active');

    let transactionTable = new ExpressTable('#listReportTable', {
      tableTemplate: listTable,
      source: ReportCentreSvc.getAllReportList,
      pageNumber: 1,
      pageSize: 10,
      onloadStarted: function () {},
      onLoadCompleted: (respData) => {}
    }, filters)
  }

}

function preFillDates(year) {
  let finYearStart, finYearEnd;
  let currentDate = new Date();
  if (year) {
    finYearStart = '01 Apr ' + year;
    finYearEnd = '31 Mar ' + (year + 1);
  } else {
    finYearStart = '01 Apr ' + (currentDate.getFullYear() - 1);
    finYearEnd = '31 Mar ' + currentDate.getFullYear();
  }

  uiElements.START_DATE.val(finYearStart);
  uiElements.END_DATE.val(finYearEnd);
  uiElements.START_DATE.datepicker("setDate", new Date(uiElements.START_DATE.val()));
  uiElements.START_DATE.datepicker("setEndDate", new Date(uiElements.END_DATE.val()));
  uiElements.END_DATE.datepicker("setStartDate", new Date(uiElements.START_DATE.val()));
  uiElements.END_DATE.datepicker("setDate", new Date(uiElements.END_DATE.val()));
  uiElements.START_DATE.datepicker('update');
  uiElements.END_DATE.datepicker('update');
}

function reportModalShow(respData, id, reportName) {
  // new RB
  if(id != "ESCROW_BALANCES_PROGRAM_LEVEL"){
    $('#companiesId').multiselect({
      columns: 1,
      search: true,
      selectAll: true,
      texts: {
        placeholder: '- Multi select companies -',
        search: 'Search companies'
      }
    });
    $('#programId').show();
    $('#programIdMultiple').hide();
    $('#programIdMultiple').parent().find('.ms-options-wrap').hide();
    $('#companiesId').multiselect('loadOptions', [{}]);
  }else{
    $('#programId').hide();
    setTimeout(function(){
      $('#programIdMultiple').multiselect('reset', []);
    },200)
  }
  
  $('.multiselect button').attr('disabled', 'disabled');
  
  $('.ms-selectall').html('select all');
  $('#productTypeId').change(function () {
    $('.ms-selectall').html('select all');
    var selectedProgramVal = ($(this).val());
    if (selectedProgramVal) {
      $('.multiselect button').removeAttr('disabled');
    }
    var productTypes = JSON.parse($('#pTypes').val());
    var companyArr = [];
    var companyObj = productTypes[selectedProgramVal];
    for (var key in companyObj) {
      var newCpmayObj = {};
      newCpmayObj["name"] = companyObj[key];
      newCpmayObj["value"] = key;
      companyArr.push(newCpmayObj);
    }
    $('#companiesId').multiselect('loadOptions', companyArr);
  });

  let isStartDateInCompulsoryParam = respData.allReports[id].compulsoryParams.indexOf('START_DATE'),
    isStartDateInSupportParam = respData.allReports[id].supportedParams.indexOf('START_DATE'),
    isProgramIds = respData.allReports[id].compulsoryParams.indexOf('PROGRAM_IDS'),
    isFundingAccountIds = respData.allReports[id].compulsoryParams.indexOf('FUNDING_ACCOUNT_IDS'),
    isCorpId = respData.allReports[id].compulsoryParams.indexOf('CORP_ID'),
    isCompanyIds = respData.allReports[id].compulsoryParams.indexOf('COMPANY_IDS'),
    isProductType = respData.allReports[id].compulsoryParams.indexOf('PRODUCT_TYPE'),
    isCompanyId = respData.allReports[id].compulsoryParams.indexOf('COMPANY_ID');
  //new RB
  reportModalShowContent(reportName);
  if (isProgramIds > -1 && isStartDateInCompulsoryParam > -1) {
    if (isCompanyIds > -1) {
      uiElements.$datePicker.show();
      uiElements.$reportNote.show();
      uiElements.$producttype.show();
      uiElements.$companies.show();
      uiElements.$programs.hide();
      $('#start_date, #end_date, #productTypeId, #companiesId').on('change', function () {
        btnState(uiElements.PROGRAM_IDS.val(), uiElements.START_DATE.val(), uiElements.END_DATE.val(), uiElements.$PRODUCT_TYPEID.val(), uiElements.$COMPANY_IDS.val());
      });
    } else {
      uiElements.$programs.show();
      uiElements.$fundingAccount.hide();
      uiElements.$reportNote.show();
      uiElements.$companies.hide();
      uiElements.$producttype.hide();
      $('#programId, #start_date, #end_date').on('change', function () {
        btnState(uiElements.PROGRAM_IDS.val(), uiElements.START_DATE.val(), uiElements.END_DATE.val());
      });
    }
  } else if (isCompanyIds > -1 && isStartDateInCompulsoryParam > -1 && isProgramIds == -1) {
    uiElements.$datePicker.show();
    uiElements.$reportNote.show();
    uiElements.$companies.show();
    uiElements.$companies.prop('disabled', false);
    uiElements.$programs.hide();
    $('.multiselect button').removeAttr('disabled');
    var companies = [];
    $('#inner-content-div1 li').each(function () {
      companies.push({
        name: $(this).data('company'),
        value: $(this).attr('data-company-id')
      });
    })
    $('#companiesId').multiselect('loadOptions', companies);
    $('#start_date, #end_date, #companiesId').on('change', function () {

      btnState(uiElements.START_DATE.val(), uiElements.END_DATE.val(), uiElements.$PRODUCT_TYPEID.val(), uiElements.$COMPANY_IDS.val());
    });
  } else if (isProgramIds > -1) {
    uiElements.$programs.show();
    uiElements.$fundingAccount.hide();
    uiElements.$reportNote.show();
    uiElements.$companies.hide();
    uiElements.$producttype.hide();
    if(id == "ESCROW_BALANCES_PROGRAM_LEVEL"){
        $('#programIdMultiple').multiselect({
          columns: 1,
          search: true,
          selectAll: true,
          nonSelectedText: 'Multi select card program',
          texts: {
            placeholder: '- Multi select card program -',
            search: 'Search card program'
          }
        });
        $('#programIdMultiple').on('change', function (selection) {
          btnState(uiElements.PROGRAM_IDS_MULTIPLE.val());
        });
    }else{
      $('#programId').on('change', function (selection) {
          let year = $(this).find(':selected').data('year')
          if (uiElements.START_DATE.val() === '' || uiElements.END_DATE.val() === '') {
            preFillDates(parseInt(year.substr(0, 4)));
          }
        if($(this).val() != "0"){
          btnState(uiElements.PROGRAM_IDS.val());
        }else{
          btnState('');
        }
          
      });
    }
  } else if (isFundingAccountIds > -1 && isStartDateInCompulsoryParam > -1) {
    uiElements.$programs.hide();
    uiElements.$fundingAccount.show();
    uiElements.$companies.hide();
    uiElements.$producttype.hide();
    $('#fundingAccountId, #start_date, #end_date').on('change', function () {
      btnState(uiElements.FUNDING_ACCOUNT_IDS.val(), uiElements.START_DATE.val(), uiElements.END_DATE.val());
    });
  } else if (isFundingAccountIds > -1) {
    uiElements.$programs.hide();
    uiElements.$fundingAccount.show();
    uiElements.$companies.hide();
    uiElements.$producttype.hide();
    $('#fundingAccountId').on('change', function (selection) {
      if (uiElements.START_DATE.val() === '' || uiElements.END_DATE.val() === '') {
        preFillDates();
      }
      btnState(uiElements.FUNDING_ACCOUNT_IDS.val());
    });
  } else {
    uiElements.$programs.hide();
    uiElements.$fundingAccount.hide();
    uiElements.$companies.hide();
    uiElements.$producttype.hide();
    btnState();
  }

  if (isCompanyId > -1) {
    uiElements.$onlyCompanyId.show();
    uiElements.$programs.hide();
    uiElements.$fundingAccount.hide();
    uiElements.$companies.hide();
    uiElements.$producttype.hide();

    btnState(0);
    $('#onlyCompanyIds').on('change', function () {
      if (uiElements.$onlyCompanyIds.val() != 0) {
        btnState(uiElements.$onlyCompanyIds.val());
      } else {
        btnState(0);
      }
    });
    if (uiElements.$onlyCompanyIds.val() != 0) {
      btnState(uiElements.$onlyCompanyIds.val());
    }
  }

  if (isStartDateInSupportParam > -1) {
    uiElements.$datePicker.show();
  } else {
    uiElements.$datePicker.hide();
  };
}

function reportModalShowContent(reportName) {
  uiElements.$closeModalBtn.show();
  uiElements.$loadingImage.hide();
  uiElements.$loadingReportText.hide();
  uiElements.FUNDING_ACCOUNT_IDS.val('0');
  uiElements.PROGRAM_IDS.val('0');
  uiElements.$reportModalHeading.text(reportName);
  uiElements.START_DATE.val('');
  uiElements.END_DATE.val('');
  uiElements.$PRODUCT_TYPEID.val('');
  uiElements.END_DATE.datepicker('setStartDate', null);
  uiElements.START_DATE.datepicker('setEndDate', new Date());
  uiElements.$downloadReport.hide();
  uiElements.$inProcessReport.hide();
  uiElements.$tryAgainBtn.hide();
  uiElements.$generateReport.show()
    .attr('disabled', 'disabled')
    .addClass('btn-disabled');
}

if(uiElements.$onlyCompanyIds.val() != '0' && uiElements.$onlyCompanyIds.find('option').length > 1){
  uiElements.$onlyCompanyIds.val('0');
}

function reportLoaderContent(reportName, id) {
  uiElements.$closeModalBtn.show();
  uiElements.$reportModal.modal('show');
  uiElements.$loadingImage.show();
  uiElements.$reportModalHeading.text("Please wait...");
  uiElements.$loadingReportText.hide();
  uiElements.$successIcon.hide();
  uiElements.$inProcessIcon.hide();
  uiElements.$inTriggeredIcon.hide();
  uiElements.$modalboxFormSection.show();
  uiElements.$errorIcon.hide();
  uiElements.$reportId.val(id);
  uiElements.$datePicker.hide();
  uiElements.$fundingAccount.hide();
  uiElements.$programs.hide();
  uiElements.$downloadReport.hide();
  uiElements.$inProcessReport.hide();
  uiElements.$generateReport.hide();
  uiElements.$tryAgainBtn.hide();
  uiElements.$reportNote.hide();
  uiElements.$companies.hide();
  uiElements.$producttype.hide();
  uiElements.$onlyCompanyId.hide();

}

function generateReportContent() {
  uiElements.$closeModalBtn.show();
  uiElements.$loadingImage.show();
  uiElements.$reportModalHeading.text("Generating your report ");
  uiElements.$modalboxFormSection.hide();
  uiElements.$datePicker.hide();
  uiElements.$fundingAccount.hide();
  uiElements.$programs.hide();
  uiElements.$downloadReport.hide();
  uiElements.$inProcessReport.hide();
  uiElements.$generateReport.hide();
  uiElements.$tryAgainBtn.hide();
  uiElements.$loadingReportText.text("Please wait while the requested report is being generated");
  uiElements.$loadingReportText.show();
  uiElements.$reportNote.hide();
  uiElements.$companies.hide();
  uiElements.$producttype.hide();
  uiElements.$onlyCompanyId.hide();
}

function downloadReportContent(respReportData) {
  uiElements.$closeModalBtn.show();
  uiElements.$loadingImage.hide();
  uiElements.$datePicker.hide();
  uiElements.$programs.hide();
  uiElements.$companies.hide();
  uiElements.$producttype.hide();
  uiElements.$fundingAccount.hide();
  uiElements.$onlyCompanyId.hide();
  uiElements.$generateReport.hide();
  uiElements.$inProcessReport.hide();
  uiElements.$tryAgainBtn.hide();
  uiElements.$successIcon.show();
  uiElements.$inProcessIcon.hide();
  uiElements.$inTriggeredIcon.hide();

  uiElements.$reportModalHeading.text("Report generated");
  uiElements.$modalboxFormSection.hide();
  uiElements.$reportNote.hide();
  uiElements.$loadingReportText.text("You can now view or download the report on the ‘Generated Reports’ section.");
  uiElements.$loadingReportText.show();

  uiElements.$downloadReport.show();
  // .attr("href", Util.htmlDecoder(respReportData.url));
}

function inProcessReportContent(respReportData, status) {
  uiElements.$closeModalBtn.show();
  uiElements.$loadingImage.hide();
  uiElements.$datePicker.hide();
  uiElements.$programs.hide();
  uiElements.$companies.hide();
  uiElements.$producttype.hide();
  uiElements.$fundingAccount.hide();
  uiElements.$onlyCompanyId.hide();
  uiElements.$generateReport.hide();
  uiElements.$tryAgainBtn.hide();
  if (status == 'triggered') {
    uiElements.$inTriggeredIcon.show();
    uiElements.$reportModalHeading.text("Previous report is still being generated");
    uiElements.$loadingReportText.text("It seems you have already requested for this report. You can check the status of this report on the ‘Generated Reports’ section.");
  } else if (status == 'inprocess') {
    uiElements.$inProcessIcon.show();
    uiElements.$reportModalHeading.text("Generating report");
    uiElements.$loadingReportText.text("This report will be generated in a few minutes. You can check the status of this report on the 'Generated Reports' section.");
  }
  uiElements.$loadingReportText.show();
  uiElements.$modalboxFormSection.hide();
  uiElements.$reportNote.hide();
  uiElements.$successIcon.hide();
  uiElements.$downloadReport.hide()
  uiElements.$inProcessReport.show();
  if ($(".nav-tabs li").length == 1) {
    $("#inProcessReport").html("Report Generation inprocess");
  }
}

function errorContent(type) {
  uiElements.$closeModalBtn.hide();
  uiElements.$reportModal.modal('show');
  uiElements.$loadingImage.hide();
  uiElements.$reportModalHeading.hide();
  uiElements.$loadingReportText.hide();
  uiElements.$successIcon.hide();
  uiElements.$inProcessIcon.hide();
  uiElements.$inTriggeredIcon.hide();
  uiElements.$modalboxFormSection.hide();
  uiElements.$errorIcon.hide();
  uiElements.$reportId.hide();
  uiElements.$datePicker.hide();
  uiElements.$fundingAccount.hide();
  uiElements.$programs.hide();
  uiElements.$downloadReport.hide();
  uiElements.$generateReport.hide();
  uiElements.$inProcessReport.hide();
  uiElements.$tryAgainBtn.hide();
  uiElements.$reportNote.hide();
  uiElements.$companies.hide();
  uiElements.$producttype.hide();
  uiElements.$onlyCompanyId.hide();
  if(type == 'generalError'){
      uiElements.$reportModalHeading.text("Generating your report");
      uiElements.$reportModalHeading.show();
      uiElements.$loadingReportText.text("Looks like the report is not ready yet. You can check the status on the 'Generated Reports' section");      
      uiElements.$loadingReportText.show();
      uiElements.$downloadReport.show();
      uiElements.$inProcessIcon.show();
  }
  else{
    uiElements.$tryAgainBtn.show();
    uiElements.$errorIcon.show();
    uiElements.$reportModalHeading.show();    
    uiElements.$reportModalHeading.text("Something went wrong");
    uiElements.$loadingReportText.show();    
    uiElements.$loadingReportText.text("Please try again after sometime!");
  }
}

function setReportName(name) {
  reportName = name;
}

function getReportName() {
  return reportName;
}

function getReportParameters() {
  return reportParameters;
}

function setReportParameters(params) {
  reportParameters = params;
}

export default {
  reportModal: function () {
    var id = $(this).attr('id'),
      reportName = $(this).data().reportName,
      respData = cacheDataStorage.getItem('respData');
    reportLoaderContent(reportName, id);
    if (!respData) {
      let product = Util.getQueryParamFromURL(window.location, 'product');
      product = product ? product.toUpperCase() : 'OPTIMA';

      ReportCentreSvc.getAvailableReports(product).then(function (respData) {
        cacheDataStorage.setItem('respData', respData);
        initGAEvent('event', 'Report Center', 'click', 'Report Selected, ' + reportName + ', corpId = ' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', timestamp=' + new Date().toLocaleString() + ', Success');
        reportModalShow(respData, id, reportName);
      }, function (respErr) {
        uiElements.$loadingImage.hide();
        uiElements.$errorIcon.show();
        if (respErr.responseJSON.type.toLowerCase().includes("invalidauthtoken")) {
          uiElements.$reportModalHeading.text("Renewing session");
          window.location.reload();
        } else {
          errorContent();
          uiElements.$reportModalHeading.text("Oops. Report Centre is currently unavailable");
          uiElements.$loadingReportText.text('This error is usually temporary. Trying within few minutes should work. If the problem still persists please try again later or contact us on support@zeta.in');
        }
        initGAEvent('event', 'Report Center', 'click', 'Report Selected, ' + reportName + ', corpId = ' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', timestamp=' + new Date().toLocaleString() + ', Failed');
      });
    } else {
      initGAEvent('event', 'Report Center', 'click', 'Report Selected, ' + reportName + ', corpId = ' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', timestamp=' + new Date().toLocaleString() + ', Success');
      setTimeout(function () {
        reportModalShow(respData, id, reportName);
      }, 1000);
    }

  },

  generateReport: function () {
    let reportName = $("#reportModalHeading").text();
    var selectedCompList = $('#companiesId').val();
    selectedCompList = selectedCompList || [];
    var responesData = JSON.parse($('#rData').val());
    var selectedProgram = $("#productTypeId").val();
    var ProgramIds = [];
    $.each(selectedCompList, function (i, value) {
      var programs = responesData["cardProgramSummaryPerCompany"][value] ? responesData["cardProgramSummaryPerCompany"][value]["programs"] : [];

      $.each(programs, function (index, proval) {
        if (proval.productType == selectedProgram) {
          ProgramIds.push(JSON.stringify(proval.programID));
        }
      })
    });

    let reportId = $('#reportId').val(),
      inputJson = {},
      parameters = {},
      START_DATE = '',
      END_DATE = '',
      respData = cacheDataStorage.getItem('respData'),
      isStartDateInSupportParam = respData.allReports[reportId].supportedParams.indexOf('START_DATE'),
      isProgramIds = respData.allReports[reportId].compulsoryParams.indexOf('PROGRAM_IDS'),
      isFundingAccountIds = respData.allReports[reportId].compulsoryParams.indexOf('FUNDING_ACCOUNT_IDS'),
      isCorpId = respData.allReports[reportId].compulsoryParams.indexOf('CORP_ID'),
      isCompanyIds = respData.allReports[reportId].compulsoryParams.indexOf('COMPANY_IDS'),
      isCompanyId = respData.allReports[reportId].compulsoryParams.indexOf('COMPANY_ID'),
      isProductType = respData.allReports[reportId].compulsoryParams.indexOf('PRODUCT_TYPE'),
      isAsync = respData.allReports[reportId].async,
      url = Urls.getOMSApiUrl().DOWNLOAD_REPORT;

    generateReportContent();

    if (isFundingAccountIds > -1) {
      parameters.FUNDING_ACCOUNT_IDS = [uiElements.FUNDING_ACCOUNT_IDS.val()];
    }

    if (isCompanyId > -1) {
      parameters.COMPANY_ID = [uiElements.$onlyCompanyIds.val()];
    }

    if (isStartDateInSupportParam > -1) {
      START_DATE = new Date(uiElements.START_DATE.val());
      END_DATE = new Date(uiElements.END_DATE.val());
      END_DATE = END_DATE.setDate(END_DATE.getDate() + 1);
      END_DATE = END_DATE - 1000;
      START_DATE = JSON.stringify(START_DATE.getTime());
      END_DATE = JSON.stringify(END_DATE);
      parameters.START_DATE = [START_DATE];
      parameters.END_DATE = [END_DATE];
    }

    if (isProgramIds > -1) {
      if (isCompanyIds > -1 && isProductType > -1) {
        parameters.COMPANY_IDS = selectedCompList;
        parameters.PROGRAM_IDS = ProgramIds;
        parameters.PRODUCT_TYPE = [selectedProgram];
      } else {
        if(reportId != "ESCROW_BALANCES_PROGRAM_LEVEL"){
          parameters.PROGRAM_IDS = [uiElements.PROGRAM_IDS.val()];
        }else{
          parameters.PROGRAM_IDS = uiElements.PROGRAM_IDS_MULTIPLE.val();
        }
      }

    }
    if (isCorpId > -1) {
      parameters.CORP_ID = [$("meta[name='corpID']").attr("content")];
    }

    if (isCompanyIds > -1 && isProductType > -1) {
      parameters.COMPANY_IDS = selectedCompList;
      parameters.PROGRAM_IDS = ProgramIds;
      parameters.PRODUCT_TYPE = [selectedProgram];
    }

    if (isCompanyIds > -1 && isProgramIds == -1) {
      parameters.COMPANY_IDS = selectedCompList;
    }

    inputJson = {
      "token": Storage.get('authToken'),
      "corpID": $("meta[name='corpID']").attr("content"),
      "reportParameters": {
        "reportID": reportId,
        "reportContentType": respData.allReports[reportId].supportedContentTypes[0],
        "parameters": parameters
      }
    };

    let headers = {
      "Content-Type": "application/json"
    };

    setReportName(reportName);
    setReportParameters(parameters);

    ServiceConnector.post(inputJson, url, headers).then(function (respReportData) {
      if (respReportData.message.indexOf('already') >= 0 && isAsync) {
        inProcessReportContent(respReportData, 'triggered');
      } else if (respReportData.state == 'PROCESSING' && isAsync) {
        inProcessReportContent(respReportData, 'inprocess');
      } else {
        downloadReportContent(respReportData);
      }
      initGAEvent('event', 'Report Center', 'click', 'Report Generated, ' + reportName + ', corpId = ' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', reportParams' + JSON.stringify(parameters) + ', timestamp=' + new Date().toLocaleString() + ', Success');
    },(respErr) => {
      initGAEvent('event', 'Report Center', 'click', 'Report Generated, ' + reportName + ', corpId = ' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', reportParams' + JSON.stringify(parameters) + ', timestamp=' + new Date().toLocaleString() + ', Failed');
      uiElements.$loadingImage.hide();
      uiElements.$errorIcon.show();
      try {
        if (respErr.responseJSON.type.toLowerCase().includes("unauthorizedexception")) {
          uiElements.$reportModalHeading.text("You are not allowed to access this resource");
        } else if (respErr.responseJSON.type.toLowerCase().includes("reportdownloadfailedexception")) {
          errorContent();
          uiElements.$reportModalHeading.text("Your report did not download successfully");
          uiElements.$loadingReportText.text('This error is usually temporary. Trying within few minutes should work. If the problem still persists please try again later or contact us on support@zeta.in');
        } else if (respErr.responseJSON.type.toLowerCase().includes("illegalargumentexception")) {
          errorContent();
          uiElements.$reportModalHeading.text("Invalid Report Parameters");
          uiElements.$loadingReportText.text('Oops, it seems you have entered incorrect parameters. Please re-enter and try generating the report again');
          uiElements.$tryAgainBtn.text("TRY AGAIN");
        } else if(respErr.statusCode == 401){
          uiElements.$reportModalHeading.text("Session expired, Please login again");
        } else {
          errorContent();
        }
        uiElements.$modalboxFormSection.hide();
      } catch (e) {
        errorContent("generalError");
      }
    });

  },
  hideModal: function () {
    uiElements.$reportModal.modal('hide');
  },
  loadTable: loadTable,
}

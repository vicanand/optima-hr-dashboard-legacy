import ServiceConnector from "../common/serviceConnector";
import Util from "../common/util";
import yearEndClosure from "../service/year-end-closure.js";
import yearEndClosureDates from "../../template/tables/year-end-closure-dates.ejs";
import Meta from "../common/metaStorage";
import Storage from "../common/webStorage"; 
import programStaticData from '../../../app/common/programStaticData';
import BenefitService from '../service/benefitSvc';
import IfiConfig from '../common/ifiConfig';
import domEventHandler from "../common/domEventHandler";
import Constants from '../common/constants';
import TimeFrames from '../../template/section/benefits/time-frame.ejs';
import benefitSvc from "../service/benefitSvc";
import AccessCtrlSvc from "../service/accessCtrlSvc";

var showYearEndClosureDate = function(respData) {
    let yearEndClosureTemplate = ejs.compile(yearEndClosureDates),
        lastIssuanceDate = Util.timeStampToDate(respData.lastIssuanceDate),
        lastClaimUploadDate = Util.timeStampToDate(respData.lastClaimUploadDate),
        lastClaimProcessDate = Util.timeStampToDate(respData.lastClaimProcessDate),
        cardCloseDate = Util.timeStampToDate(respData.cardCloseDate),
        reportGenerationDate = Util.timeStampToDate(respData.reportGenerationDate),
        yearEndClosureHtml;
    let datesParam = {
        lastIssuanceDate: {
            formatted: lastIssuanceDate,
            iso: respData.lastIssuanceDate
        },
        lastClaimUploadDate: {
            formatted: lastClaimUploadDate,
            iso: respData.lastClaimUploadDate
        },
        lastClaimProcessDate: {
            formatted: lastClaimProcessDate,
            iso: respData.lastClaimProcessDate
        },
        cardCloseDate: {
            formatted: cardCloseDate,
            iso: respData.cardCloseDate
        },
        reportGenerationDate: {
            formatted: reportGenerationDate,
            iso: respData.reportGenerationDate
        }
    };
    yearEndClosureHtml = yearEndClosureTemplate(datesParam);
    $('.yearEndClosuretable').html(yearEndClosureHtml);
}

var scrollToTab = function(id) {
    $('html,body').animate({
        scrollTop: $("#" + id).offset().top - 170
    }, 'slow');
}

var initProgramSetupEvents = function() {
    $('input[type=radio][name=programType]').change(function() {
        if (this.value == 'allowance') {
            $('#allowancePara').show();
            $('#reimbursementPara').hide();
            $('#claimRequiredAction').show()
            if($('input[name=claimUpload]:checked').val() == 'Yes'){
                $('div[data-value="#multiCollapseExample4"]').show()
                $('#multiCollapseExample3').find('.nextSection').data('target', '#multiCollapseExample4');
                $('div[data-value="#multiCollapseExample5"]').find('.stepCount').text("5");
                $('div[data-value="#multiCollapseExample6"]').find('.stepCount').text("6");
            }else{
                $('div[data-value="#multiCollapseExample4"]').hide()
                $('#multiCollapseExample4').removeClass('in');
                $('#multiCollapseExample3').find('.nextSection').data('target', '#multiCollapseExample5');
                $('div[data-value="#multiCollapseExample5"]').find('.stepCount').text("4");
                $('div[data-value="#multiCollapseExample6"]').find('.stepCount').text("5");
            }
        } else {
            $('#reimbursementPara').show();
            $('#allowancePara').hide();
            $('#claimRequiredAction').hide()

            $('div[data-value="#multiCollapseExample4"]').show()
            $('#multiCollapseExample3').find('.nextSection').data('target', '#multiCollapseExample4');
            $('div[data-value="#multiCollapseExample5"]').find('.stepCount').text("5");
            $('div[data-value="#multiCollapseExample6"]').find('.stepCount').text("6");

            // $('div[data-target="#multiCollapseExample4"]').show();
        }
    });
    $('input[type=radio][name=remainingbalance]').change(function() {
        if (this.value == 'moveToUserCashCard') {
            $('#cashPara').show();
            $('#fundingPara').hide();
        } else {
            $('#fundingPara').show();
            $('#cashPara').hide();
        }
    });
    $('input[type=radio][name=closureremainingbalance]').change(function() {
        if (this.value == 'MOVE_TO_CASH_CARD') {
            $('#closurecashPara').show();
            $('#closurefundingPara').hide();
        } else {
            $('#closurefundingPara').show();
            $('#closurecashPara').hide();
        }
    });

    $('input[name=to-be-closed]').change(function(){
        if(this.value == 'Yes'){
            $('.optionalClosure').removeClass('hideMe');
        }else{
            $('.optionalClosure').addClass('hideMe');
        }
    })

    $('input[name=expire-balance]').change(function(){
        if(this.value == 'Yes'){
            $('.optionalExpiry').removeClass('hideMe');
        }else{
            $('.optionalExpiry').addClass('hideMe');
        }
    })

    $('input[name=claimUpload]').change(function(){
        if(this.value == 'Yes'){
            $('div[data-value="#multiCollapseExample4"]').show()
            $('#multiCollapseExample3').find('.nextSection').data('target', '#multiCollapseExample4');
            $('div[data-value="#multiCollapseExample5"]').find('.stepCount').text("5");
            $('div[data-value="#multiCollapseExample6"]').find('.stepCount').text("6");
        }else{
            $('div[data-value="#multiCollapseExample4"]').hide()
            $('#multiCollapseExample4').removeClass('in');
            $('#multiCollapseExample3').find('.nextSection').data('target', '#multiCollapseExample5');
            $('div[data-value="#multiCollapseExample5"]').find('.stepCount').text("4");
            $('div[data-value="#multiCollapseExample6"]').find('.stepCount').text("5");
        }
    })

    // $('.multiSelectOpts option').prop('selected', true);
    $('.multiSelectOpts').multiselect({
        columns: 1,
        search: true,
        selectAll: true,
        nonSelectedText: 'Select sub categories',
        texts: {
            placeholder: '- Select sub categories -',
            nonSelectedText: 'Select suncategories',
            search: 'Search categories'
        }
    });

    $('#spendDays').multiselect({
        columns: 1,
        search: true,
        selectAll: true,
        nonSelectedText: 'Select days',
        texts: {
            placeholder: 'Select days',
            nonSelectedText: 'Select days',
            search: 'Search days'
        }
    });

    $('#allowedMerchants, #disallowedMerchants').multiselect({
        columns: 1,
        search: true,
        selectAll: true,
        nonSelectedText: 'Select merchants',
        texts: {
            placeholder: 'Select merchants',
            nonSelectedText: 'Select merchants',
            search: 'Search merchants'
        }
    });

    addTimeSliceRow('false', '', '')

    $('#addMoreRow').click(function() {
        if($('.removeRow').length <= 4){
            addTimeSliceRow('true', '', '')
        }
    })

    $('.nextSection').click(function() {
        var section = $(this).data('value');
        var target = $(this).data('target');
        if (validateSection(section)) {
            $(this).closest('.heading').attr('aria-expanded', false);
            $(this).closest('.multi-collapse').removeClass('in');
            $('div[data-target="' + target + '"]').attr('aria-expanded', true);
            $('' + target).addClass('in')
            $('div[data-value="' + target + '"]').removeClass('disabled');
            $('html,body').animate({
                scrollTop: $("" + target).offset().top - 170
            }, 'slow');
        }
    })

    $('#fundingAccountID').change(function(){
        if($(this).val() == 'createNew'){
            $('#fundingAccName').show();
        }else{
            $('#fundingAccName').hide();
        }
    })
    
    $('#closeProgramOn').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true
    });
}

var validateSection = function(section) {
    let validated = true;
    if (section == "general") {
        let programName = $('#programName').val();
        let fundingAccountId = $('#fundingAccountID').val();
        
        if (!programName) {
            $('#programName').addClass('errorField');
            scrollToTab('multiCollapseExample3');
            return false
        } else {
            $('#programName').removeClass('errorField');
        }
        if (!fundingAccountId) {
            $('#fundingAccountID').addClass('errorField');
            scrollToTab('multiCollapseExample3');
            return false
        } else {
            $('#fundingAccountID').removeClass('errorField');
        }

        if(fundingAccountId == 'createNew' && !$.trim($('#fundingAccName').val())){
            $('#fundingAccName').addClass('errorField');
            scrollToTab('multiCollapseExample3');
            return false;
        }

        $('div[data-value="#multiCollapseExample1"]').addClass('complete');
        $('div[data-value="#multiCollapseExample1"]').attr('data-target', '#multiCollapseExample1')
    }

    if (section == 'expiry') {
        let expireBalance = $('input[name="expire-balance"]:checked').val();
        let expireOn = $('#expireBalanceOn').val();
        if (expireBalance == 'Yes' && !expireOn) {
            $('#expireBalanceOn').addClass('errorField')
            validated = false
        } else {
            $('#expireBalanceOn').removeClass('errorField')
        }
        $('div[data-value="#multiCollapseExample5"]').addClass('complete');
        $('div[data-value="#multiCollapseExample5"]').attr('data-target', '#multiCollapseExample5')
    }

    if (section == 'spend') {
        let maxTxnLimit = $('#maxTxnLimit').val();
        let minTxnLimit = $('#minTxnLimit').val();
        let maxTxnPerDay = $('#maxTxnPerDay').val();
        let maxTxnAmtPerDay = $('#maxTxnAmtPerDay').val();
        let maxTxnPerMonth = $('#maxTxnPerMonth').val();
        let maxTxnAmtPerMonth = $('#maxTxnAmtPerMonth').val();

        let validated = true;

        if (minTxnLimit == '') {
            $('#minTxnLimit').addClass('errorField');
            scrollToTab('multiCollapseExample3');
            validated = false
        }

        if (!maxTxnLimit) {
            $('#maxTxnLimit').addClass('errorField');
            scrollToTab('multiCollapseExample3');
            validated = false
        }

        if (!maxTxnPerDay) {
            $('#maxTxnPerDay').addClass('errorField');
            scrollToTab('multiCollapseExample3');
            validated = false
        }

        if (!maxTxnAmtPerDay) {
            $('#maxTxnAmtPerDay').addClass('errorField');
            scrollToTab('multiCollapseExample3');
            validated = false
        }

        if (!maxTxnPerMonth) {
            $('#maxTxnPerMonth').addClass('errorField');
            scrollToTab('multiCollapseExample3');
            validated = false
        }

        if (!maxTxnAmtPerMonth) {
            $('#maxTxnAmtPerMonth').addClass('errorField');
            scrollToTab('multiCollapseExample3');
            validated = false
        }
        $('#timeSliceContainer .selectRows').each(function(i, element) {
            $('select').removeClass('errorField');
            var timeFrom = parseInt($(element).find('.timeFrom').val())
            var timeTo = parseInt($(element).find('.timeTo').val())
            if ((timeFrom != 'select' && timeTo == 'select') || (timeTo != 'select' && timeFrom == 'select')) {
                $(this).find('select').addClass('errorField');
                validated = false;
            }
        })

        let customAllowedMerchants = $('#customAllowedMerchants').val() != "" ? $('#customAllowedMerchants').val().split(',') : []
        let customDisallowedMerchants = $('#customDisallowedMerchants').val() != "" ? $('#customDisallowedMerchants').val().split(',') : []

        let merchantValidation = validateMerchant(customAllowedMerchants, customDisallowedMerchants)
        if(!merchantValidation.validAllowedMerchants){
            $('#customAllowedMerchants').addClass('errorField')
            validated = false;
        }else{
            $('#customAllowedMerchants').removeClass('errorField')
        }
        if(!merchantValidation.validDisallowedMerchants){
            $('#customDisallowedMerchants').addClass('errorField')
            validated = false;
        }else{
            $('#customDisallowedMerchants').removeClass('errorField')
        }

        $('div[data-value="#multiCollapseExample3"]').addClass('complete');
        $('div[data-value="#multiCollapseExample3"]').attr('data-target', '#multiCollapseExample3')

        return validated;
    }

    if (section == 'claim') {
        $('div[data-value="#multiCollapseExample4"]').addClass('complete');
        $('div[data-value="#multiCollapseExample4"]').attr('data-target', '#multiCollapseExample4')
    }

    if (section == 'closure') {
        let allowProgramClosureBeneficiary = $('input[name="to-be-closed"]:eq(0)').prop('checked');
        let closeOn = $('#closeProgramOn').val();
        if (allowProgramClosureBeneficiary) {
            if (!closeOn) {
                $('#closeProgramOn').addClass('errorField');
                return false
            }
        }
        $('div[data-value="#multiCollapseExample6"]').addClass('complete');
        $('div[data-value="#multiCollapseExample6"]').attr('data-target', '#multiCollapseExample6')
    }

    if (section == 'address') {
        $('#addressSection input').each(function(){
            if($(this).attr('id') != "address2"){
                if($.trim($(this).val())){
                    $(this).removeClass('errorField');
                }else{
                    $(this).addClass('errorField');
                    validated = false;
                }
            }
        })

        if(!Util.validateEmail($('#email').val())){
            $('#email').addClass('errorField');
            validated = false;
        }else{
            $('#email').removeClass('errorField');
        }

        $('div[data-value="#multiCollapseExample2"]').addClass('complete');
        $('div[data-value="#multiCollapseExample2"]').attr('data-target', '#multiCollapseExample2')
    }    

    return validated;
}

var addTimeSliceRow = function(newRow, timeFrom, timeTo) {
    domEventHandler.appendToMyTemplate('timeSliceContainer', TimeFrames, { newRow: newRow, timeFrom: timeFrom, timeTo: timeTo });
    domEventHandler.bindClassEvent('removeRow', 'click', function() {
        $(this).closest('.selectRows').remove();
    })
}

var prepareProgramConfig = function(productType) {
    /* General Settings */
    let corpId = Meta.getMetaElement('corpID'),
        companyID = Meta.getMetaElement('companyID'),
        companyName = $.trim($('#selectedCompanyName').text());
    let programName = $('#programName').val();
    let fundingAccountId = $('#fundingAccountID').val();
    let programType = $('input[name="programType"]:checked').val();
    let claimUploadRequired = $('input[name="claimUpload"]:checked').val();
    
    /* End */

    /* Billing Address */
    let firstName = $('#firstName').val(),
    lastName = $('#lastName').val(),
    email = $('#email').val(),
    countryCode = $('#countryCode').val(),
    phone = $('#phone').val(),
    address1 = $('#address1').val(),
    address2 = $('#address2').val(),
    city = $('#city').val(),
    state = $('#state').val(),
    pincode = $('#pincode').val()
    /* End */

    /* Spend Settings */
    let spendDeclaration = $('input[name="declaration"]:checked').val();
    let spendDeclarationText = $('#declarationText').val()
        //Spend channels
    let zetaApp = $('#zeta-app').prop('checked');
    let superCard = $('#super-card').prop('checked');
    let superTag = $('#super-tag').prop('checked');

    let allowedMerchants = $('#allowedMerchants').val() || [];
    let disallowedMerchants = $('#disallowedMerchants').val() || [];

    let spendDays = $('#spendDays').val();

    let customAllowedMerchants = $.trim($('#customAllowedMerchants').val());
    let customDisallowedMerchants = $.trim($('#customDisallowedMerchants').val());

    //Spend time slices
    // let timeFrom = $('#timeFrom').val();
    // let timeTo = $('#timeTo').val();

    //transaction limits
    let maxTxnLimit = $('#maxTxnLimit').val();
    let minTxnLimit = $('#minTxnLimit').val();
    let maxTxnPerDay = $('#maxTxnPerDay').val();
    let maxTxnAmtPerDay = $('#maxTxnAmtPerDay').val();
    let maxTxnPerMonth = $('#maxTxnPerMonth').val();
    let maxTxnAmtPerMonth = $('#maxTxnAmtPerMonth').val();
    /* End */
    let programConfig = programStaticData[productType];

    let billingObj = {
        "firstName": firstName,
        "lastName": lastName,
        "email": email,
        "telephone": phone,
        "line1": address1,
        "line2": address2,
        "city": city,
        "region": state,
        "countryID": countryCode,
        "postCode": pincode
    }

    programConfig.cardProgram.businessDetails.billingAddress = billingObj;

    if(customAllowedMerchants != ""){
        customAllowedMerchants = customAllowedMerchants.split(',');
        programConfig.cardProgram.spendConfiguration['allowedMerchants'] = allowedMerchants.concat(customAllowedMerchants)
    }else{
        programConfig.cardProgram.spendConfiguration['allowedMerchants'] = allowedMerchants
    }
    if(customDisallowedMerchants != ""){
        customDisallowedMerchants = customDisallowedMerchants.split(',');
        programConfig.cardProgram.spendConfiguration['disallowedMerchants'] = disallowedMerchants.concat(customDisallowedMerchants)
    }else{
        programConfig.cardProgram.spendConfiguration['disallowedMerchants'] = disallowedMerchants
    }

    /* Claim Settings */
    let optedUniformCategories = [];
    if (productType == 'uniform') {
        $('#uniformOpts input:checked').each(function(index, element) {
            optedUniformCategories.push($(this).val());
        })
    }

    let optedBooksCategories = [];
    if (productType == 'books') {
        $('.booksSection select').each(function(index, element) {
            let selectedValue = $(this).val();
            optedBooksCategories = optedBooksCategories.concat(selectedValue);
        })
    }
    /* End */

    /* Expiry Settings */
    let expireBalance = $('input[name="expire-balance"]:checked').val();
    let expireOn = $('#expireBalanceOn').val();
    let moveBalanceTo = $('input[name="remainingbalance"]:checked').val();
    /* End */

    /* Year End Program Closure Settings */
    let allowProgramClosureBeneficiary = $('input[name="to-be-closed"]:eq(0)').prop('checked');
    let closeOn = $('#closeProgramOn').val();
    let moveBalanceOnClosure = $('input[name="closureremainingbalance"]:checked').val();
    /* End */
    programConfig.cardProgram.cardName = programConfig.cardProgram.cardDesign.cardName = programName
    programConfig.corpID = corpId
    programConfig.companyID = companyID
    if (fundingAccountId) {
        programConfig.fundingAccountID = fundingAccountId
    } else {
        // delete programConfig.fundingAccountID;
    }

    programConfig.cardProgram.ifi = IfiConfig.getIfiId(Meta.getMetaElement('ifi'));
    programConfig.cardProgram.ifiName = Meta.getMetaElement('ifi');
    programConfig.cardProgram.businessID = $('#bussinessId').val();
    programConfig.cardProgram.issuanceConfiguration.issuer = $('#bussinessId').val() + '@business.zeta.in'
        // programConfig.cardProgram.businessName = companyName
        // programConfig.voucherProgram.voucherName = programName

    if (spendDeclaration == 'Yes') {
        programConfig.cardProgram.spendConfiguration.spendDeclaration = spendDeclarationText
    } else {
        programConfig.cardProgram.spendConfiguration.spendDeclaration = ''
    }

    programConfig.cardProgram.spendConfiguration.channels['allowSuperCard'] = superCard
    programConfig.cardProgram.spendConfiguration.channels['allowZetaApp'] = zetaApp
    programConfig.cardProgram.spendConfiguration.channels['allowSuperTag'] = superTag
    $('#timeSliceContainer .selectRows').each(function(i, element) {
        var timeFrom = $(element).find('.timeFrom').val()
        var timeTo = $(element).find('.timeTo').val()
        if (timeFrom != 'select' && timeTo != 'select') {
            let cronExp = "* * " + timeFrom + "-" + timeTo + " ? * ";
            if(spendDays.length > 0){
                cronExp += spendDays.join(',') + " *";
            }else{
                cronExp += "* *";
            }
            programConfig.cardProgram.spendConfiguration.allowedTimeSlices.push(cronExp);
        }
    })

    programConfig.cardProgram.spendConfiguration.transactionConstraints.minTransactionValue = minTxnLimit * 100
    programConfig.cardProgram.spendConfiguration.transactionConstraints.maxTransactionValue = maxTxnLimit * 100
        // programConfig.cardProgram.spendConfiguration.limits.transactionLimit = maxTxnLimit * 100
    programConfig.cardProgram.spendConfiguration.limits.dailyTxnCount = maxTxnPerDay
    programConfig.cardProgram.spendConfiguration.limits.dailyLimit = maxTxnAmtPerDay * 100
    programConfig.cardProgram.spendConfiguration.limits.monthlyTxnCount = maxTxnPerMonth
    programConfig.cardProgram.spendConfiguration.limits.monthlyLimit = maxTxnAmtPerMonth * 100

    if (productType != 'asset' && productType != 'gift') {
        $('#rulesTable tbody tr').each(function(index, element) {
            let rule = $(this).find('input:eq(0)').attr('name');
            let checkedValue = $(this).find('input:eq(0)').prop('checked');
            programConfig.cardProgram.reimbursementProgram.extraReimbursementProgramConfig.allowRules.forEach(element => {
                if (element.rule == rule) {
                    element.value = checkedValue;
                }
            });

            programConfig.cardProgram.reimbursementProgram.extraReimbursementProgramConfig.requireRules.forEach(element => {
                if (element.rule == rule) {
                    element.value = checkedValue;
                }
            });

        });
    }

    if (programType == "allowance") {
        programConfig['programModel'] = 'ALLOWANCE';
    } else {
        programConfig['programModel'] = 'REIMBURSEMENT';
    }

    if (programType == "allowance") {
        if(claimUploadRequired == 'Yes'){
            programConfig.cardProgram.spendConfiguration.channels.allowReimbursement = true;
        }else{
            programConfig.cardProgram.spendConfiguration.channels.allowReimbursement = false;
        }
    }else{
        programConfig.cardProgram.spendConfiguration.channels.allowReimbursement = true;
    }

    if (productType == "uniform") {
        programConfig.cardProgram.reimbursementProgram.extraReimbursementProgramConfig.uniformCategories = optedUniformCategories;
    }
    if (productType == "books") {
        programConfig.cardProgram.reimbursementProgram.extraReimbursementProgramConfig.bookCategories = optedBooksCategories;
    }

    if (expireBalance == 'Yes') {
        programConfig.cardProgram.voucherProgram.voucherValidity = expireOn;
        programConfig.cardProgram.issuanceConfiguration.expiryBehaviour = moveBalanceTo
    } else {
        programConfig.cardProgram.voucherProgram.voucherValidity = '';
        programConfig.cardProgram.issuanceConfiguration.expiryBehaviour = ''
    }

    if (allowProgramClosureBeneficiary) {
        let closeOnDate = closeOn.split('/');
        closeOn = closeOnDate[2] + "-" + closeOnDate[1] + "-" + closeOnDate[0]
        programConfig.closeCardConfiguration.closeBehaviour = moveBalanceOnClosure
        programConfig.closeCardConfiguration.closeCardDate = closeOn
    } else {
        delete programConfig.closeCardConfiguration;
    }

    let voucherDenomination = $('#voucherDenomination').val()
    if(voucherDenomination){
        programConfig.cardProgram.reimbursementProgram.extraReimbursementProgramConfig.voucherDenomination = voucherDenomination
    }

    return programConfig;
}

let validateMerchant = function(allowedMerchants, disallowedMerchants){
    let validMerchants = ["mid", "mcc", "amc"]
    let validAllowedMerchants = true
    let validDisallowedMerchants = true
    allowedMerchants.forEach(function(merchant){
        if(validMerchants.indexOf(merchant.split(':')[0]) < 0){
            validAllowedMerchants = false
        }
    })
    disallowedMerchants.forEach(function(merchant){
        if(validMerchants.indexOf(merchant.split(':')[0]) < 0){
            validDisallowedMerchants = false
        }
    })

    return {
        validAllowedMerchants: validAllowedMerchants,
        validDisallowedMerchants: validDisallowedMerchants
    }
}

let setupCustomProgram = function(programConfig){
    $('#createProgram').attr('disabled', 'disabled');
    $('#confirmSetup').modal('hide');
    $('#createProgramLoader').modal('show');
    $('#createProgram').addClass('btn-disabled');
    BenefitService.createCustomCardProgram(programConfig).then(function(respData) {
        $('#createProgramLoader').modal('hide');
        $('#setupProgramModal').modal('hide');
        $('#createProgramSuccess').modal('show');
        $("#viewProgramBtnSuccess").attr("href", "/companies/" + Meta.getMetaElement('companyID') + "/optima/" + respData.id + "#settings");
    }, function(err) {
        $('#createProgram').removeAttr('disabled');
        $('#createProgram').removeClass('btn-disabled');
        $('#createProgramLoader').modal('hide');
        $('#createProgramError').modal('show');
        $('#errorMsg').text(err.message || err.responseText || 'Something went wrong');
    })
}

export
default {
    showConfirmModal: function(){
        let checkGenie = $(this).data('genie');
        if(!checkGenie){
            $('#confirmSetup').modal('show');
        }else{
            $('#createProgramError').modal('show');
            $('#errorMsg').text('You are not authorized to perform this operation');
        }
    },
    createCustomProgram: function() {
        let productType = $('#productType').val();
        if (validateSection('closure')) {
            let programConfig = prepareProgramConfig(productType);
            if(programConfig.fundingAccountID == 'createNew'){
                let requestBody = {
                    'corpID': Meta.getMetaElement("corpID"),
                    'name': $.trim($('#fundingAccName').val())
                }
                benefitSvc.createFundingAccount(requestBody).then(function(resp){
                    programConfig.fundingAccountID = resp.accountID;
                    setupCustomProgram(programConfig);
                })
            }else{
                setupCustomProgram(programConfig);
            }
        }
        
    },
        createProgram: function (e) {
            $('#createProgramLoader').modal('show');
            $('#createProgram').attr('disabled', 'disabled');
            $('#createProgram').addClass('btn-disabled');
            $('#createProgramLoader').show();
            $('#showFundAccountError').hide(); 
            $('#showGenieFundAccountError').hide();
            let isNewAccount;
            let fundValue = $('#fundingAccountID').val();
            let TypeofRequest;
            if (fundValue == 'createNew') {
                TypeofRequest = '/createNewFundAndProgram';
                isNewAccount = '';
            } else {
                TypeofRequest = '/createProgram';
                isNewAccount = 'frombenefit';
            }
            
            let currency = 'INR';
            const getCurrencyName = Meta.getMetaElement(Constants.CURRENCY_NAME);
            if (getCurrencyName) {
                currency = getCurrencyName;
            }
           
            let url = TypeofRequest + '?fundingAccID=' + $('#fundingAccountID').val() + '&productType=' + $(this).attr('data-programType') +
                '&financialYear=' + $('#financialYear').val() + '&currency='+ currency + '&newFundAccount=' + $('#newfundingaccname').val() + '&isNewAccount=' + isNewAccount;
            
            const productName = $(this).attr('data-programType');
            const country = Meta.getMetaElement('countryVar');
            if(country == 'isSodexoVn' && productName == 'sodexo_cafeteria'){
                url = url + '&country=' + country + '&url=' + '/cardprograms/createCardProgram'
            }

            let headers = {
                'X-program-description': $('#programDescription').val(),
                'programname': $('#programName').val()
            };
    
            ServiceConnector.get(url, headers).then(function(respData) {
                $('#createProgramLoader').modal('hide');
                $('#setupProgramModal').modal('hide');
                $('#createProgramSuccess').modal('show');
                $("#viewProgramBtnSuccess").attr("href", "optima/" + respData.id);
            }, function(respErr) {
                console.log(respErr);
                $('#createProgram').removeAttr('disabled');
                $('#createProgram').removeClass('btn-disabled');
                if((Util.checkGenie()) && (respErr.responseJSON.body.message == "Unauthorized")){
                    $('#showGenieFundAccountError').show();
                } else {
                    $('#showFundAccountError').show();
                }
                $('#createProgramLoader').modal('hide');
            });
    },
    scheduleYearDone: function() {
        $('#myModalScheduleYearSuccessful').modal('hide');
        location.reload();
    },
    closureDate: function() {
        let $scheduleYear = $('#scheduleYear');
        let $tableLoader = $(".tableLoader");
        $tableLoader.show();
        var report_generation_date = $(this).val().split("/");
        report_generation_date = new Date(report_generation_date[2], report_generation_date[1] - 1, report_generation_date[0], 23, 59, 59);
        report_generation_date = report_generation_date.toISOString();
        let params = {
            reportGenerationDate: report_generation_date
        };
        yearEndClosure.getProgramClosureSuggestion(params).then(function(respData) {
            $tableLoader.hide();
            showYearEndClosureDate(respData);
        });

        if ($('#closureDate').val().length === 0) {
            $scheduleYear.attr('disabled', 'disabled');
            $scheduleYear.addClass('btn-disabled');
        } else {
            $scheduleYear.removeAttr('disabled');
            $scheduleYear.removeClass('btn-disabled');
        }
    },
    showClosedCards: function() {
        let companyID = $("meta[name='companyID']").attr("content");
        let url = "/companies/" + companyID + "/optimaPrograms";
        ServiceConnector.get(url, {}).then(function(data) {
            $('.closed-cards-tab').html(Util.htmlDecoder(data));
            // let totalClosedExistingCardsUpdated =$('.closed-cards-tab .benefit-card-closed').length;
            // let pagesizeUpdated = totalClosedExistingCardsUpdated/10;
            let totalClosedProgramPages = '';
            let closedProgramPageNumber = '';
            if (localStorage.getItem('totalClosedProgramPages')) {
                totalClosedProgramPages = parseInt(localStorage.getItem('totalClosedProgramPages'));
            }
            if (localStorage.getItem('closedProgramPageNumber')) {
                closedProgramPageNumber = parseInt(localStorage.getItem('closedProgramPageNumber'));
            }
            if ((totalClosedProgramPages == 0) || closedProgramPageNumber >= totalClosedProgramPages - 1) {
                $("#closedProgramsTab").hide();
            }
        }, function(error) {
            console.log("Some error fetching closd cards", error);
        });
    },
    loadActiveCards: function() {
        let companyID = $("meta[name='companyID']").attr("content");
        // let totalActiveExistingCards =$('.active-cards-tab .benefit-card-active').length;
        // let pagesize = totalActiveExistingCards/10;   // params - pageNumber, pageSize (pagenumberstarts from 0 )
        let totalActiveProgramPages = '';
        let activeProgramPageNumber = '';
        if (localStorage.getItem('totalActiveProgramPages')) {
            totalActiveProgramPages = parseInt(localStorage.getItem('totalActiveProgramPages'));
        }
        if (localStorage.getItem('activeProgramPageNumber')) {
            activeProgramPageNumber = parseInt(localStorage.getItem('activeProgramPageNumber'));
        }
        if (activeProgramPageNumber < totalActiveProgramPages - 1) {
            activeProgramPageNumber = activeProgramPageNumber + 1;
            localStorage.setItem('activeProgramPageNumber', activeProgramPageNumber);
            // integer value
            let url = "/companies/" + companyID + "/loadActivePrograms/" + activeProgramPageNumber;
            ServiceConnector.get(url, {}).then(function(data) {
                $('.active-cards-tab').append(Util.htmlDecoder(data));
                // let totalActiveExistingCardsUpdated =$('.active-cards-tab .benefit-card-active').length;
                // let pagesizeUpdated = totalActiveExistingCardsUpdated/10;
                if (activeProgramPageNumber >= totalActiveProgramPages - 1) {
                    $("#activeProgramsTab").hide();
                    $("#no-more-active-text").show();
                }
            }, function(error) {
                console.log("Some error fetching closd cards", error);
            });
        } else {
            $("#activeProgramsTab").hide();
            $("#no-more-active-text").show();
        }

    },
    loadClosedCards: function() {
        let companyID = $("meta[name='companyID']").attr("content");

        let totalClosedProgramPages = '';
        let activeProgramPageNumber = '';
        let closedProgramPageNumber = '';
        if (localStorage.getItem('totalClosedProgramPages')) {
            totalClosedProgramPages = parseInt(localStorage.getItem('totalClosedProgramPages'));
        }
        if (localStorage.getItem('closedProgramPageNumber')) {
            closedProgramPageNumber = parseInt(localStorage.getItem('closedProgramPageNumber'));
        }
        if (closedProgramPageNumber < totalClosedProgramPages - 1) {
            // integer value
            closedProgramPageNumber = closedProgramPageNumber + 1;
            localStorage.setItem('closedProgramPageNumber', closedProgramPageNumber);
            let url = "/companies/" + companyID + "/loadClosedPrograms/" + closedProgramPageNumber;
            ServiceConnector.get(url, {}).then(function(data) {
                $('.closed-cards-tab').append(Util.htmlDecoder(data));
                // let totalClosedExistingCardsUpdated =$('.closed-cards-tab .benefit-card-closed').length;
                // let pagesizeUpdated = totalClosedExistingCardsUpdated/10;
                if (closedProgramPageNumber >= totalClosedProgramPages - 1) {
                    $("#closedProgramsTab").hide();
                    $("#no-more-closed-text").show();
                }

            }, function(error) {
                console.log("Some error fetching closd cards", error);
            });
        } else {
            $("#closedProgramsTab").hide();
            $("#no-more-closed-text").show();
        }

    },
    scheduleYear: function() {
        var obj = {
            lastIssuanceDate: $('.lastIssuanceDate').data('issuance-date') || '',
            lastClaimUploadDate: $('.lastClaimUploadDate').data('claim-upload-date') || '',
            lastClaimProcessDate: $('.lastClaimProcessDate').data('claim-process-date') || '',
            cardCloseDate: $('.cardCloseDate').data('card-close-date') || '',
            reportGenerationDate: $('.reportGenerationDate').data('report-generation-date') || ''
        };
        yearEndClosure.scheduleProgramClosure(obj).then(function(respData) {
            $('#myModalScheduleYear').modal('hide');
            $('#myModalScheduleYearSuccessful').css('display', 'block');
            $('.last-issuance-date').hide();
        });
    },
    fundingAccountIDChange: function() {
        let $createProgram = $('#createProgram'),
            $programName = $('#programName'),
            $accountID = $('#fundingAccountID'),
            $showHideNewFundingAcc = $('#shownewfundingaccname'),
            $NewFundingAccName = $('#newfundingaccname');
        let $programType = $("#createProgram").data('programtype');
        let sety = $('#setupLtaCheckbox').is(':checked');

        if ($accountID.find(":selected").text() == 'Create New') {
            $showHideNewFundingAcc.show();
            if (($accountID.val() == 0 || !$programName.val() || $programName.val().length > 40) || !$NewFundingAccName.val() || $NewFundingAccName.val().length > 40) {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
                $('#newfundingaccname').val(null)
            } else {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
                $('#newfundingaccname').val(null)
            }
        } else {
            $showHideNewFundingAcc.hide();
            if (($accountID.val() == 0 || !$programName.val() || $programName.val().length > 40)) {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
                $('#newfundingaccname').val(null)
            } else {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
                $('#newfundingaccname').val(null)
            }
        }

        if ($programType == 'LTA') {
            if ($accountID.find(":selected").text() == 'Create New') {
                if (sety == true && $NewFundingAccName.val().length > 0 && $programName.val().length > 0) {
                    $createProgram.removeAttr('disabled');
                    $createProgram.removeClass('btn-disabled');
                } else {
                    $createProgram.attr('disabled', 'disabled');
                    $createProgram.addClass('btn-disabled');
                }
            } else {
                if ($accountID.val() > 0 && sety == true && $programName.val().length > 0) {
                    $createProgram.removeAttr('disabled');
                    $createProgram.removeClass('btn-disabled');
                } else {
                    $createProgram.attr('disabled', 'disabled');
                    $createProgram.addClass('btn-disabled');
                }

            }
        }
    },
    newFundingAccountName: function() {
        let $createProgram = $('#createProgram'),
            $programName = $('#programName'),
            $accountID = $('#fundingAccountID'),
            $showHideNewFundingAcc = $('#shownewfundingaccname'),
            $NewFundingAccName = $('#newfundingaccname');

        if (($accountID.val() == 0 || !$programName.val() || $programName.val().length > 40) || !$NewFundingAccName.val() || $NewFundingAccName.val().length > 40) {
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $createProgram.removeAttr('disabled');
            $createProgram.removeClass('btn-disabled');
        }

        console.log($NewFundingAccName.val())

        let accountName = $NewFundingAccName.val();
        let allfundingAccountName = JSON.parse(localStorage.getItem('allFundingAccountName'));
        if (allfundingAccountName.indexOf(accountName) > -1) {
            $('#showfundingAccDuplicate').show();
            $createProgram.attr('disabled', 'disabled');
            $createProgram.addClass('btn-disabled');
        } else {
            $('#showfundingAccDuplicate').hide();
        }



    },
    programNameChange: function() {
        let $createProgram = $('#createProgram'),
            $programName = $('#programName'),
            $accountID = $('#fundingAccountID'),
            $showHideNewFundingAcc = $('#shownewfundingaccname'),
            $NewFundingAccName = $('#newfundingaccname');

        if ($accountID.find(":selected").text() == 'Create New') {
            $showHideNewFundingAcc.show('slow');
            if (($accountID.val() == 0 || !$programName.val() || $programName.val().length > 40) || !$NewFundingAccName.val() || $NewFundingAccName.val().length > 40) {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
            } else {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
            }
        } else {
            $showHideNewFundingAcc.hide('slow');
            if (($accountID.val() == 0 || !$programName.val() || $programName.val().length > 40)) {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
            } else {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
            }
        }
    },

    setupLtaCheckbox: function() {
        let $createProgram = $('#createProgram'),
            $programName = $('#programName'),
            $accountID = $('#fundingAccountID'),
            $accountName = $('#newfundingaccname');
        let setz = $('#setupLtaCheckbox').is(':checked');

        if ($accountID.find(":selected").text() == 'Create New') {
            if (setz == true && $accountName.val().length > 0 && $programName.val().length > 0) {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
            } else {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
            }
        } else {
            if ($accountID.val() > 0 && setz == true && $programName.val().length > 0) {
                $createProgram.removeAttr('disabled');
                $createProgram.removeClass('btn-disabled');
            } else {
                $createProgram.attr('disabled', 'disabled');
                $createProgram.addClass('btn-disabled');
            }

        }
    },

    close_Me: function() {
        location.reload();
    },
    benefitSelected: function() {
        var programStatus = $(this).data('type');
        var programName = $(this).find('h3').text();
        initGAEvent('event', 'Zeta Optima', 'click', 'Benefit Program Clicked, corpId=' + Meta.getMetaElement("corpID") + ', compId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', programStatus=' + programStatus + ', programName=' + programName + ', timestamp=' + new Date().toLocaleString());
    },
    saveCompanyInfoToSession: function(){
        Storage.setCollection('selected_comp_info', $('#selected_comp_info').data('value'))
    },
    
    refreshCorpAccountsInfo: function() {
        AccessCtrlSvc.getCorpAccounts();
    },

    initProgramSetupEvents: initProgramSetupEvents
}
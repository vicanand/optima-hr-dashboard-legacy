import Storage from '../../common/webStorage';
import DomEventHandler from '../../common/domEventHandler';
import OfficeTemplate from '../../../template/section/feedback/office-feedback-dropdown.ejs';
import StoreTemplate from '../../../template/section/feedback/store-feedback-dropdown.ejs';
import FeedbackController from '../../controller/feedback/feedbackCtrl';
import FeedbackModal from '../../controller/feedback/feedbackModal';
import DropDownService from '../../service/feedback/dropDownSvc';
let cityMenuSelect = function(){
    $(this).find('.city-multiselect-option .city-tick').toggleClass('city-tick-visible');
  //  $(this).find('.city-multiselect-values').toggleClass('ischecked'); 
    var data = $(this).find('.city-multiselect-values');
    console.log('bpresentValue: ',  data.data("check"));
    if(data.data("check") == "1"){ 
        
       data.attr("data-check","0");
       data.data("check","0");
    }else{
       data.attr("data-check","1");
       data.data("check","1");
    }
    var getallcities =   $(this).parent().find('.city-multiselect-container .city-multiselect-values');
   
    var getcheckcities =  $(this).parent().find('.city-multiselect-container .city-multiselect-values');
   // console.log('getcheckcities: ', getcheckcities);
    var checked_values = [];
    
    var filtered_offices=[];
    
    var filtered_stores=[];
    
    getcheckcities.each(function( index ) {
        
       if($( this ).data('check') == "1"){
            var cityValue = $(this).text();
            checked_values.push(cityValue);
       }
       
    });
    console.log('getcheckcities: ', getcheckcities);
    console.log('checked_values: ', checked_values);
    if(getallcities.length == checked_values.length){
        $('.city-title').text('ALL CITIES');
    }else if(checked_values.length == 0){
        $('.city-title').text('NO CITIES SELECTED');
    }else if(checked_values.length == 1){
        $('.city-title').text(checked_values[0]);
    }else{
        $('.city-title').text(checked_values.length+' CITIES SELECTED');
    }
    var all_stores =JSON.parse(Storage.get('allStores'));
    var clean_offices = getCleanOffices(all_stores);
    checked_values.forEach(function(element){
        var city = element;
        clean_offices.forEach(function(stores){
            if(stores.city == city){
                var office = {};
                office.officeName = stores.officeName;
                office.check = "1";
                office.businessID = stores.businessID;
                filtered_offices.push(office);
            }
        })
    })
    checked_values.forEach(function(element) {
        var city = element;
        all_stores.forEach(function(stores){
            if(stores.city == city){
                var store_value={};
                store_value.storeName = stores.storeName;
                store_value.check ="1";
                store_value.storeId = stores.businessID+':'+stores.storeID;
                filtered_stores.push(store_value);
            }
        })
    });
    
    console.log('filtered_offices: ', filtered_offices);
    console.log('filtered_stores: ', filtered_stores);
    var stores = storeId(filtered_stores);
    console.log('stores: ', stores);
    FeedbackModal.set_stores(stores);
    DomEventHandler.renderMyTemplate('officePicker',OfficeTemplate,{offices:filtered_offices});
    DomEventHandler.renderMyTemplate('storePicker',StoreTemplate,{stores:filtered_stores});
    DropDownService.render_all(stores);
    if(filtered_offices.length == 0){
        $('.office-title').text('NO OFFICES');
        $('.store-title').text('NO STORES');
    }else if(filtered_offices.length == 1){
        $('.office-title').text(filtered_offices[0].officeName);
       
    }
    if(filtered_stores.length == 0){
        $('.store-title').text('NO STORES');
    }else if(filtered_stores.length == 1){
        $('.store-title').text(filtered_stores[0].storeName);
    }
    FeedbackController.officeinit();
    FeedbackController.storeinit();


   
};

let getCleanOffices = function(allstores){
    var offices = new Set();
    var offices_names = new Set();
    var filteredOffices = [];
    var offices_values = [];
    var final_offices = [];
    
    
    allstores.forEach(function(stores){
        offices.add(stores.businessID);
    });
    offices.forEach(function(offices){
        console.log('offices: ', offices);
       filteredOffices.push(offices);
    });
    filteredOffices.forEach(function(stores){
        allstores.forEach(function(all){
            if(all.businessID == stores){
                offices_names.add(all.office);
            }
        });
    });
    offices_names.forEach(function(offices){
        offices_values.push(offices);
    });
    for(var i = 0;i < filteredOffices.length;){
        console.log("aset_arr",filteredOffices[i]);
          for(var j = 0;j < allstores.length;j++){
          console.log("a",allstores[j].businessID);
            if(allstores[j].businessID == filteredOffices[i]){
              final_offices.push({officeName:allstores[j].office,businessID:allstores[j].businessID,check:"1",city:allstores[j].city});
            i++;
          }
        }
      }
      
    console.log('offices_values: ', final_offices);
   // filteredOffices.forEach();
   return final_offices;

};
var storeId = function(params){
    var arr = [];
    
    params.forEach(function(res){
        arr.push(res.storeId.toString());
    });
    arr = arr.join(",");
    console.log('arr: ', arr);
    return arr;
};
export default{
     cityMenu (e){
        
            e.stopPropagation();
            $('.office-triangle-container').css('display','none');
            $('.office-list').css('display','none');
            $('.store-triangle-container').css('display','none');
            $('.store-list').css('display','none');

            $('.city-triangle-container').css('display','block');
            $('.city-list').css('display','block');

         
    },
    cityMenuSelect
}
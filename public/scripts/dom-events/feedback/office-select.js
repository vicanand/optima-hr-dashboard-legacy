import Storage from '../../common/webStorage';
import DomEventHandler from '../../common/domEventHandler';
import OfficeTemplate from '../../../template/section/feedback/office-feedback-dropdown.ejs';
import StoreTemplate from '../../../template/section/feedback/store-feedback-dropdown.ejs';
import FeedbackController from '../../controller/feedback/feedbackCtrl';
import FeedbackModal from '../../controller/feedback/feedbackModal';
import DropDownService from '../../service/feedback/dropDownSvc';
let officeMenuSelect = function(){
    $(this).find('.office-multiselect-option .office-tick').toggleClass('office-tick-visible');
    $(this).find('.office-multiselect-values').toggleClass('ischecked'); 
    var data = $(this).find('.office-multiselect-values');
    console.log('data: ', data);
    if(data.data("check") == "1"){
        data.attr("data-check","0");
        data.data("check","0");
     }else{
        data.attr("data-check","1");
        data.data("check","1");
     }
     var getalloffices =   $(this).parent().find('.office-multiselect-container .office-multiselect-values');
     var getcheckoffices =  $(this).parent().find('.office-multiselect-container .office-multiselect-values');
     var checked_values = [];
     var filtered_stores=[],filtered_offices=[];
     getcheckoffices.each(function( index ) {
         
        if($( this ).data('check') == "1"){
             var officeValue = $(this).data("businessid");
             checked_values.push(officeValue);  
             var some_value = $(this).html();
             filtered_offices.push(some_value);     
        }
        
     });
       
     var all_stores =JSON.parse(Storage.get('allStores'));
     checked_values.forEach(function(element) {
         
         var businessId = element;
         all_stores.forEach(function(stores){
            if(stores.businessID == businessId){
                var store_value={};
                store_value.storeName = stores.storeName;
                store_value.check ="1";
                store_value.storeId = stores.businessID+':'+stores.storeID;
                filtered_stores.push(store_value);
            }
         })
     });
     console.log('checked_values: ', checked_values);
     if(getalloffices.length == checked_values.length){
        $('.office-title').text('ALL OFFICES');
    }else if(checked_values.length == 0){
        $('.office-title').text('NO OFFICES SELECTED');
    }else if(checked_values.length == 1){
        $('.office-title').text(filtered_offices[0]);
    }else{
        $('.office-title').text(checked_values.length+' OFFICES SELECTED');
    }
     console.log('checked_values: ', checked_values);
     console.log('filtered_stores: ', filtered_stores);
     var stores = storeId(filtered_stores);
     FeedbackModal.set_stores(stores);
     DomEventHandler.renderMyTemplate('storePicker',StoreTemplate,{stores:filtered_stores});
     if(filtered_stores.length == 0){
        $('.store-title').text('NO STORES');
    }else if(filtered_stores.length == 1){
        $('.store-title').text(filtered_stores[0].storeName);
    }
     DropDownService.render_all(stores);
     FeedbackController.storeinit();
     
}
var storeId = function(params){
    var arr = [];
    
    params.forEach(function(res){
        arr.push(res.storeId.toString());
    });
    arr = arr.join(",");
    console.log('arr: ', arr);
    return arr;
};
export default{
     officeMenu (e){
        
            e.stopPropagation();
            $('.city-triangle-container').css('display','none');
            $('.city-list').css('display','none');
            $('.store-triangle-container').css('display','none');
            $('.store-list').css('display','none');

            $('.office-triangle-container').css('display','block');
            $('.office-list').css('display','block');
         
    },
    officeMenuSelect
}
import Storage from '../../common/webStorage';
import DomEventHandler from '../../common/domEventHandler';
import OfficeTemplate from '../../../template/section/feedback/office-feedback-dropdown.ejs';
import StoreTemplate from '../../../template/section/feedback/store-feedback-dropdown.ejs';
import FeedbackController from '../../controller/feedback/feedbackCtrl';
import FeedbackModal from '../../controller/feedback/feedbackModal';
import DropDownService from '../../service/feedback/dropDownSvc';
let storeMenuSelect = function(){
    $(this).find('.store-multiselect-option .store-tick').toggleClass('store-tick-visible');
    $(this).find('.store-multiselect-values').toggleClass('ischecked'); 
    var data = $(this).find('.store-multiselect-values');
    console.log('data: ', data);
    if(data.data("check") == "1"){
        data.attr("data-check","0");
        data.data("check","0");
     }else{
        data.attr("data-check","1");
        data.data("check","1");
     }
     var getallstores =   $(this).parent().find('.store-multiselect-container .store-multiselect-values');
     var getcheckstores =  $(this).parent().find('.store-multiselect-container .store-multiselect-values');
     var checked_values = [];
     
     var filtered_stores=[],names = [];
     getcheckstores.each(function( index ) {
         
        if($( this ).data('check') == "1"){
             var storeValue = $(this).data("storeid");
             checked_values.push(storeValue);   
             var some_value = $(this).html();
             names.push(some_value);
        }
        
     });
     console.log('names',names);
     if(getallstores.length == checked_values.length){
        $('.store-title').text('ALL STORES');
    }else if(checked_values.length == 0){
        $('.store-title').text('NO STORES SELECTED');
    }else if(checked_values.length == 1){
        $('.store-title').text(names[0]); 
    }else{
        $('.store-title').text(checked_values.length+' STORES SELECTED');
    }
     console.log('checked_values: ', checked_values);
     var stores = storeId(checked_values);
     FeedbackModal.set_stores(stores);
     DropDownService.render_all(stores);
     
     console.log('stores: ', stores);

}
var storeId = function(params){
    var arr = [];
    
    params.forEach(function(res){
        arr.push(res.toString());
    });
    arr = arr.join(",");
    console.log('arr: ', arr);
    return arr;
};
export default{
     storeMenu (e){
        
            e.stopPropagation();
            $('.city-triangle-container').css('display','none');
            $('.city-list').css('display','none');
            $('.office-triangle-container').css('display','none');
            $('.office-list').css('display','none');
            $('.store-triangle-container').css('display','block');
            $('.store-list').css('display','block');
         
    },
    storeMenuSelect
}
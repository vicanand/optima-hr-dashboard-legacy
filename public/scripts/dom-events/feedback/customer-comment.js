import Storage from '../../common/webStorage';
import DomEventHandler from '../../common/domEventHandler';
import CommentFeedback from '../../../template/section/feedback/comment-data-feedback.ejs';
import FeedbackService from '../../service/feedback/feedbackSvc';
import FeedbackModal from '../../controller/feedback/feedbackModal';

let starSelected = function(){
    $('.customer-rating').css({"border-bottom":"none","color":"#000000"});
    $(this).css({"border-bottom":"2px solid #6e4db7","color":"#6e4db7"});
    var rating = $(this).data('star');
    FeedbackModal.set_rating(rating);
    FeedbackModal.set_scroll_flag(true);
    FeedbackModal.set_processing(false);
    console.log('rating: ', rating);
    var stores = FeedbackModal.get_stores();
    var fromDate = FeedbackModal.get_from_date() || moment().subtract(30, 'days').format('YYYY-MM-DD');
    var toDate = FeedbackModal.get_to_date() || moment().format('YYYY-MM-DD hh:mm:ss');
    var authToken = Storage.get('authToken');
    var corpID = $("meta[name='corpID']").attr("content");
    let params = {
        corporateID:corpID,
        stores:stores,
        fromDate:fromDate,
        toDate:toDate,
        pageSize:10,
        rating:rating
    }
    FeedbackService.getCustomerComments(authToken,params).then(function(comments){
        var comments = comments;
        console.log('comments: ', comments);
        if(comments.comments.length > 0){
            var new_object = JSON.stringify(comments.comments);
            var new_comments = JSON.parse(new_object);
            var toDate = new_comments[new_comments.length - 1].timeStamp;
            console.log('toDate: ', toDate);
            FeedbackModal.set_last_modified_date(toDate);
        }else{
            FeedbackModal.set_scroll_flag(false);
        }
        formatTime(comments.comments);
        DomEventHandler.renderMyTemplate('comment-details-container',CommentFeedback,comments);
    });

};

var formatTime = function(time){
    
    time.forEach(function(item){
        item.timeStamp = moment(item.timeStamp).format('YYYY-MM-DD,hh:mm:ss a');
       
    });
    
}
export default{
    starSelected
}
let _initializeEnv = function () {
    let env;
    if (window.location.port == '4000' || window.location.host.indexOf('stage') > -1) {
        env = 'STAGE';
    } else if (window.location.port == '3000' || window.location.host.indexOf('preprod') > -1 || window.location.host.indexOf('-pp') > -1 || window.location.host.indexOf('uat-') > -1) {
        env = 'PREPROD';
    } else {
        env = 'PROD';
    }
    return env;
}


export default {
    CURR_ENV: _initializeEnv()
}

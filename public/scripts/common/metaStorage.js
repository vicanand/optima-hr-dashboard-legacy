export
default {
    getMetaElement: function(name){
        if($("meta[name="+name+"]")){
            return $("meta[name="+name+"]").attr("content");
        }else{
            return false;
        }
    }
}
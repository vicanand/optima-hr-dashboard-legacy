import Env from '../common/env';

let IFIConfig = {
    PROD: {
        platformIFI: {
            RBL: 156699,
            IDFC: 158326,
            KOTAK: 163924,
            SODEXO: 163925,
            HDFC: 233203
        },
        corpbenIFI:{
            RBL: 1,
            IDFC: 2,
            KOTAK: 50,
            SODEXO: "",
            HDFC: 132
        }
    },
    PREPROD: {
        platformIFI: {
            RBL: 140793,
            IDFC: 140827,
            KOTAK: 141618,
            SODEXO: 141617,
            HDFC: 151613
        },
        corpbenIFI:{
            RBL: 289,
            IDFC: 290,
            KOTAK: 291,
            SODEXO: "",
            HDFC: 295
        }
    },
    STAGE: {
        platformIFI: {
            RBL: 140793,
            IDFC: 140827,
            KOTAK: 141618,
            SODEXO: 141617,
            HDFC: 147340
        },
        corpbenIFI:{
            RBL: 1,
            IDFC: 2,
            KOTAK: 112,
            HDFC: 264,
            SODEXO: ""
        }
    }
}

let getIfiId = function(ifiName){
    return IFIConfig[Env.CURR_ENV].platformIFI[ifiName];
}

let corpbenIfiId = function(ifiName) {
    return IFIConfig[Env.CURR_ENV].corpbenIFI[ifiName];
}

export default {
    getIfiId: getIfiId,
    corpbenIfiId: corpbenIfiId
}
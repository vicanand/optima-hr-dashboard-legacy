export const PHILIPPINES = {
  // public/script/controller/benefitDetailsCtrl.js
  "benefitDetailsCtrl": {
    "drawRegionsMap": {
      region: "VN"
    },
    "spendChannels": {
      "AppLabel": "QRCode/ShopId"
    }
  },
  // public/script/dom-events/addFundDOMEvent.js
  "addFundDOMEvent": {
    "validationRequest": { 
      "formatCurrencyValue": function({elemId}) {
        if (elemId) {
          $(elemId).val(function(index, value) {
            var parts = value.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
          });
        }
      },
      "getRequiredInputList": function() {
        let inputIDList = ['transactionRefNumber', 'orderId', 'transactionAmount'];
        if($('input[name=inlineRadioOptions]:checked').val() === 'CREDIT_TERM') {
          inputIDList.push('transactionDate', 'POText', 'POAttachment')
        } else {
          inputIDList.push('paymentProof')
        }
        return inputIDList;
      }
    },
    "updateRequestObj" : function(requestObject, paymentMode, transactionDate) {
      let extraInfo = {
        "transferType" : paymentMode,
        "sfQuotationNumber" : $('#transactionRefNumber').val(),
        "orderID" : $('#orderId').val(),
        "creditAmount" : $('#transactionAmount').val(),
        "poTextField" : $('#POText').val(),
        "poAttachment" : $('#POAttachment').attr('data-url'),
        "comments" : $('#fundReqComment').val()
        }
        if(paymentMode === 'CREDIT_TERM') {
          extraInfo.dueDate = transactionDate;
        } else {
          extraInfo.proofOfAttachment = $('#paymentProof').attr('data-url');
        }
      requestObject.extraInfo = extraInfo;

      return requestObject;
    },
    "onTransferChange": function(transferType) {
      let reqClass;
      if (transferType === "CREDIT_TERM") {
            $('.payment-proof-wrap').addClass('hide');
            $('.due-date-wrap').removeClass('hide');
            reqClass = ['quotationNumber', 'orderId', 'due-date-wrap', 'chequeAmount', 'POText', 'POAttachment'];
        } else {
            $('.payment-proof-wrap').removeClass('hide');
            $('.due-date-wrap').addClass('hide');
            reqClass = ['quotationNumber', 'orderId', 'payment-proof-wrap', 'chequeAmount'];
        }
        $('.required-field').removeClass('required-field');
        
        reqClass.map(requiredField => {
          $('.' + requiredField).addClass('required-field');
        })

    },
  },
  // public/script/dom-events/employee-benefits-dom-events.js
  "employeeBenefitsDomEvents": {
    "downloadStatement": {
      "skipSignedDocument": true
    }
  },
  "fundsSvc": {
    "getFundingAccounts": {
      hideDropDown: true,
    }
  },
  // public/script/dom-events/fundingHomeDOMEvent.js
  "fundingHomeDOMEvent": {
    "addFundsOnHome": {
      "addFundDetailsHeader": "Drop a cheque/transfer to Sodexo’s Bank Account"
    }
  },
  //accessControl - Admin console
  "userAccessDOMEvent": {
    "accessControl": true
  },
  // public/script/module/employee.js
  "employee": {
    "tablewidgetData": {
      "tableTemplateURL": "/template/tables/employees/sodexo-employees.ejs",
      "rowTemplateURL": "/template/tables/employees/sodexo-employees-row.ejs"
    }
  },
  // public/script/module/employeeBenefits.js
  "employeeBenefits": {
    "initSodexo": true
  },
  // public/scripts/dom-events/fundingHomeDOMEvent.js
  "fundingHomeDOMEvent": {
    "addFundsOnHome": {
      "skipFundChequeTransferModal": true
    }
  }
}
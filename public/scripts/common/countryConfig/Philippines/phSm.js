export const PHILIPPINES_SM = {
  // public/script/service/fundsSvc.js
  "fundsSvc": {
    "getFundingAccounts": {
      "passCompanyId": true,
      "disableAddFundsByName": function({name}) {
        return name.indexOf('Issuance') != -1;
      },
      "disableFundTransferByName": function({name}) {
        return name.indexOf('Issuance') != -1;
      },
      "disableFooterInfo": true
    }
  },
  // public/script/dom-events/addFundDOMEvent.js
  "addFundDOMEvent": {
    "validationRequest": {
      "formatCurrencyValue": function({elemId}) {
        if (elemId) {
          $(elemId).val(function(index, value) {
            var parts = value.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
          });
        }
      }
    }
  },
  // public/scripts/dom-events/fundingHomeDOMEvent.js
  "fundingHomeDOMEvent": {
    "addFundsOnHome": {
      "skipFundChequeTransferModal": true
    }
  }
}
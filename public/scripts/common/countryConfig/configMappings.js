const COUNTRY_CONFIGS = {
  // Sodexo Philippines Configs
  SODEXO_PHILIPPINES_CONFIGS: {
    IFINAME:'Sodexo_Philippines',
    SM_CORP_IDS: {
      'stage': [11660],
      'prod': [57546, 57863],
      'preprod': [16183, 16378, 16623],
    },
    CORP_IFI: {
      'stage': 329,
      'preprod': 308,
      'production': 181
    },
    PLATFORM_IFI: {
        'stage': 151287,
        'production': 285134,
        'preprod': 155103,
    },
    REGION: 'PH',
  },
  // Sodexo Vietnam Configs
  SODEXO_VIETNAM_CONFIGS: {
    IFINAME:'Sodexo_Vietnam',
    CORP_IFI: {
        'STAGE': '333',
        'PREPROD': '309',
        'PROD': '178',
    },
    PLATFORM_IFI: {
        'STAGE': '151515',
        'PREPROD': '156924',
        'PROD': '290831',
    },
    REGION: 'VN',
  }
};

/**
 * KEY = Currency
 * VALUE = Currency Config
 */
const CURRENCY_CONFIGS = {
    USD: {
      symbol: '$',
      languages: {'en-US': {displaySymbolPosition: 'PREFIX'}},
    }, // US Dollar
    EUR: {
      symbol: '€',
      languages: {'en-US': {displaySymbolPosition: 'PREFIX'}}
    }, // Euro
    INR: {
      symbol: '₹',
      languages: {'en-IN': {displaySymbolPosition: 'PREFIX'}}
    }, // Indian Rupee
    PHP: {
      symbol: '₱',
      languages: {'en-PH': {displaySymbolPosition: 'PREFIX'}},
      minimumFractionDigits: 2
    }, // Philippine Peso
    VND: {
      symbol: '₫',
      languages: {'en-VN': {displaySymbolPosition: 'PREFIX'}}
    }, // Vietnamese Dong
}

/**
 * KEY = Country Name as per Country Var
 * VALUE = Exported variable name of config file
 */
const CONFIG_NAME = {
  "isSodexoPhpSm": "PHILIPPINES_SM", // Philippines SM
  "isSodexoPh": "PHILIPPINES", // Philippines
  "isSodexoVn": "VIETNAM" // Vietnam
}


export {
  COUNTRY_CONFIGS,
  CURRENCY_CONFIGS,
  CONFIG_NAME
};
/**
 * Config files loader
 * [IMPORTANT]: [FILES] Exported variable name should be same as CONFIG_NAME defined in configMappings file
 */

import Meta from "../metaStorage";
import { CONFIG_NAME } from "./configMappings";
import CacheDataStorage from '../cacheDataStorage';
import Constants from '../constants';

// [FILES] Country Config files imports
import { PHILIPPINES_SM } from './Philippines/phSm';
import { PHILIPPINES } from './Philippines/ph';
import { VIETNAM } from './Vietnam/vn';

const FILES = {
  PHILIPPINES_SM,
  PHILIPPINES,
  VIETNAM
}

const ConfigLoader = (() => {
  /**
   * 
   * @param {*} path - Config file path Example: ./Philippines/ph.js
   * @param {*} cb - Callback function 
   */
  const initConfigLoader = (cb) => {
    // debugger
    const countryVar = Meta.getMetaElement(Constants.COUNTRY_VAR);
    const name = countryVar && CONFIG_NAME[countryVar];
    if (name) {
      const configData = FILES[name];
      if (configData) {
        CacheDataStorage.setItem(Constants.COUNTRY_CONFIGS, configData);
        cb(null);
        return;
      }
      cb("Config data not found");
    }
  };

  return {
    initConfigLoader,
  }

})();

export default ConfigLoader;
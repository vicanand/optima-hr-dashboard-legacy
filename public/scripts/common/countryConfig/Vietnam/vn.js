export const VIETNAM = {
  // public/script/controller/benefitDetailsCtrl.js
  "benefitDetailsCtrl": {
    "drawRegionsMap": {
      region: "VN"
    },
    "spendChannels": {
      "AppLabel": "QRCode/ShopId"
    }
  },
  // public/script/dom-events/addFundDOMEvent.js
  "addFundDOMEvent": {
    "addFundsToAcc": {
      "paymentMode": "CHEQUE"
    },
    "validationRequest": {
      "validateAmount": function({amt}) {
        if (amt) {
          return (Number.isInteger(parseInt(amt)) && amt%1000 == 0);
        }
        return true;
      }
    }
  },
  // public/script/dom-events/employee-benefits-dom-events.js
  "employeeBenefitsDomEvents": {
    "downloadStatement": {
      "skipSignedDocument": true
    }
  },
  // public/script/dom-events/fundingHomeDOMEvent.js
  "fundingHomeDOMEvent": {
    "addFundsOnHome": {
      "addFundDetailsHeader": "Drop a cheque/transfer to Sodexo’s Bank Account"
    }
  },
  // public/script/module/employee.js
  "employee": {
    "tablewidgetData": {
      "tableTemplateURL": "/template/tables/employees/sodexo-employees.ejs",
      "rowTemplateURL": "/template/tables/employees/sodexo-employees-row.ejs"
    }
  },
  // public/script/module/employeeBenefits.js
  "employeeBenefits": {
    "initSodexo": true
  },
  // public/script/controller/transferDetailsCtrl.js
  "transferDetailsCtrl": {
    "init": {
      "tableTemplate": "/template/tables/transfer-detail-payoutV2/transfer-detail-payouts.ejs",
      "rowTemplate": "/template/tables/transfer-detail-payoutV2/transfer-details-payout-row.ejs"
    }
  },
  // public/script/dom-events/cardProgramIndividualBeneficiaryDomEvent.js
  "cardProgramIndividualBeneficiaryDomEvent": {
    "showHideCardTable": {
      "tableTemplate": '/template/section/benefit-details/individualBeneficiaryDetails/tables/transfer-reportV2/common-transferReport.ejs',
      "rowTemplate": '/template/section/benefit-details/individualBeneficiaryDetails/tables/transfer-reportV2/common-transferReport-row.ejs'
    }
  },
  // public/script/module/benefitDetails.js
  "benefitDetailsModule": {
    "start": {
      "ordersTableTemplate": '/template/tables/transfer-orderV2/transfer-orders.ejs',
      "ordersRowTemplate": '/template/tables/transfer-orderV2/transfer-orders-row.ejs'
    }
  }
}
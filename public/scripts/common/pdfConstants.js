let generateTime, userEmail, fromToDate, companyName, cardName, userName, transactionOpeningBalance, transactionClosingBalance, transactionCredits, transactionDebits;

export
default {
    statementTableHeader: [
        [{
                text: 'Date',
                margin: [5, 5, 0, 5],
                fillColor: 'F7F6F8',
                bold: true,
                font: 'Roboto',
                border: [true, true, true, true]
            },
            {
                text: 'Txn Ref. ID',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Particulars',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Debit (₹)',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            },
            {
                text: 'Credit (₹)',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            },
            {
                text: 'Balance (₹)',
                margin: [0, 5, 5, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            }
        ]
    ],
    transactionSummaryInfo: [{
            type: 'rect',
            x: -20,
            y: 20,
            w: 140,
            h: 70,
            lineColor: '#e5e3e9'
        },
        {
            type: 'rect',
            x: 120,
            y: 20,
            w: 140,
            h: 70,
            lineColor: '#e5e3e9'
        },
        {
            type: 'rect',
            x: 260,
            y: 20,
            w: 140,
            h: 70,
            lineColor: '#e5e3e9'
        },
        {
            type: 'rect',
            x: 400,
            y: 20,
            w: 140,
            h: 70,
            lineColor: '#e5e3e9'
        },
        {
            type: 'ellipse',
            x: 120,
            y: 55,
            color: 'white',
            lineColor: '#e5e3e9',
            fillOpacity: 1,
            fillColor: 'white',
            r1: 15,
            r2: 15
        },
        {
            type: 'ellipse',
            x: 260,
            y: 55,
            color: 'white',
            lineColor: '#e5e3e9',
            fillOpacity: 1,
            fillColor: 'white',
            r1: 15,
            r2: 15
        },
        {
            type: 'ellipse',
            x: 400,
            y: 55,
            color: 'white',
            lineColor: '#e5e3e9',
            fillOpacity: 1,
            fillColor: 'white',
            r1: 15,
            r2: 15
        }
    ],
    hrLine: {
        canvas: [{
            type: 'line',
            x1: -20,
            y1: 10,
            x2: 540,
            y2: 10,
            lineWidth: 3,
            lineColor: '#ccc8d4',

        }]
    },
    allClaimsHeader: [
        [{
                text: 'Claim Date',
                margin: [5, 5, 0, 5],
                fillColor: '#F7F6F8',
                bold: true,
                font: 'Roboto',
                border: [true, true, true, true]
            },
            {
                text: 'Claim ID',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Merchant',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Status',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Claimed (₹)',
                margin: [0, 5, 0, 5],
                fillColor: '#F7F6F8',
                bold: true,
                font: 'Roboto',
                alignment: 'right'
            },
            {
                text: 'Approved (₹)',
                margin: [0, 5, 0, 5],
                fillColor: '#F7F6F8',
                bold: true,
                font: 'Roboto',
                alignment: 'right'
            },
            {
                text: 'Reimbursed (₹)',
                margin: [0, 5, 5, 5],
                fillColor: '#F7F6F8',
                bold: true,
                font: 'Roboto',
                alignment: 'right'
            }
        ],
        [{
                text: 'Claim Date',
                margin: [5, 5, 0, 5],
                fillColor: '#F7F6F8',
                bold: true,
                font: 'Roboto',
                border: [true, true, true, true]
            },
            {
                text: 'Claim ID',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Status',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Claimed (₹)',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            },
            {
                text: 'Approved (₹)',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            },
            {
                text: 'Reimbursed (₹)',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            },
            {
                text: 'Tax Exemption (₹)',
                margin: [0, 5, 5, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            }
        ],
        [{
                text: 'Claim Date',
                margin: [5, 5, 0, 5],
                fillColor: '#F7F6F8',
                bold: true,
                font: 'Roboto',
                border: [true, true, true, true]
            },
            {
                text: 'Claim ID',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Towards',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Service Provider',
                bold: true,
                font: 'Roboto',
                margin: [0, 5, 0, 5],
                fillColor: '#F7F6F8'
            },
            {
                text: 'Status',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Claimed (₹)',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            },
            {
                text: 'Approved (₹)',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            },
            {
                text: 'Reimbursed (₹)',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            }
        ],
        [{
                text: 'Claim Date',
                margin: [5, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                border: [true, true, true, true]
            },
            {
                text: 'Claim ID',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Category',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Merchant',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Status',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8'
            },
            {
                text: 'Claimed (₹)',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            },
            {
                text: 'Approved (₹)',
                margin: [0, 5, 0, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            },
            {
                text: 'Reimbursed (₹)',
                margin: [0, 5, 5, 5],
                bold: true,
                font: 'Roboto',
                fillColor: '#F7F6F8',
                alignment: 'right'
            }
        ]
    ],
}
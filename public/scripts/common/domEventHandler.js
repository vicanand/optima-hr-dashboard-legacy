// import Utils from '../../../../bill-approval-dashboard/app/scripts/common/utils.js';
export default {
    renderMyTemplate: function (id, inputTemplate, data) {
        var templateOptionsObj = {
            imports: {
                htmlDecoder: function htmlDecoder(str) {
                    return _.unescape(str);
                }
            }

        };
        var template = _.template(inputTemplate, templateOptionsObj);
        if (data) {
            $('#' + id).html(template(data));
        } else {
            $('#' + id).html(template());
        }
    },
    appendToMyTemplate: function (id, inputTemplate, data) {
        var templateOptionsObj = {
            imports: {
                htmlDecoder: function htmlDecoder(str) {
                    return _.unescape(str);
                }
            }
        }

        var template = _.template(inputTemplate, templateOptionsObj);
        if (data) {
            $('#' + id).append(template(data));
        } else {
            $('#' + id).append(template());
        }
    },
    renderTemplateArray: function (id, inputTemplate, dataArray) {
        var templateOptionsObj = {
            imports: {
                htmlDecoder: function htmlDecoder(str) {
                    return _.unescape(str);
                }
            }
        }
        $('#' + id).html('');
        let template = _.template(inputTemplate, templateOptionsObj);

        let templateArray = dataArray.map(function (item) {
            return template(item);
        });

        $('#' + id).html(templateArray.join(''));
    },

    bindEvent: function (id, currEvent, currData, currEventHandler) {
        $('#' + id).on(currEvent, currData, currEventHandler);
    },

    bindClassEvent: function (className, currEvent, currData, currEventHandler) {
        $('.' + className).on(currEvent, currData, currEventHandler);
    },

    bind: function (selector, currEvent, currData, currEventHandler) {
        $(selector).on(currEvent, currData, currEventHandler);
    },
    bindTable: function (nearestId, tableId, currEvent, currData, currEventHandler) {
        $('#' + nearestId).on(currEvent, '#' + tableId + ' tr', currData, currEventHandler);
    },
    /*  bindTable works like below way.Here nearestId is the above table id. And tableId is current Table id.
    $("#benifitsEmployees").on("click", "#cardProgramBeneficiaryTable tr", function(e) {
            });
    */

    renderView: function (view, data) {
        if (data) {
            view.render(data);
        } else {
            view.render();
        }
    }
}

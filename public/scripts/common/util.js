import {CURRENCY_CONFIGS} from './countryConfig/configMappings';
import Meta from '../common/metaStorage';
export
default {
    formatINR: function (number) {
        var totalAmtSplitArr = Number(number).toString().split('.');
        var totalAmtINR = '';
        if (totalAmtSplitArr && totalAmtSplitArr.length) {
            totalAmtINR += totalAmtSplitArr[0].replace(/(\d)(?=(\d\d)+\d$)/g, '$1,');
            // Not considering the decimals i.e. paise
            /*if (totalAmtSplitArr.length === 2) {
                totalAmtINR += '.' + totalAmtSplitArr[1];
            }*/
        }
        return totalAmtINR;
    },
    formatINRwithDecimal: function (number) {
        var totalAmtSplitArr = Number(number).toString().split('.');
        var totalAmtINR = '';
        if (totalAmtSplitArr && totalAmtSplitArr.length) {
            totalAmtINR += totalAmtSplitArr[0].replace(/(\d)(?=(\d\d)+\d$)/g, '$1,');
            // Not considering the decimals i.e. paise
            if (totalAmtSplitArr.length === 2) {
                totalAmtINR += '.' + totalAmtSplitArr[1];
            }
        }
        return totalAmtINR;
    },
    formatNumber: function (number) {
        var totalValSplitArr = Number(number).toString().split('.');
        var totalVal = '';
        if (totalValSplitArr && totalValSplitArr.length) {
            totalVal += totalValSplitArr[0].replace(/(\d)(?=(\d\d)+\d$)/g, '$1,');
        }
        return totalVal;
    },
    validateMobile: function (value) {

        if (isNaN(value)) {
            return false;
        }

        return value.match(/\d/g).length === 10;
    },
    validateEmail: function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },
    dateFormat: function (date, month, year) {
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return date + ' ' + months[month - 1] + ' ' + year;
    },
    dateFormatWithDay: function (date) {
        let dateNew = new Date(date);
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][dateNew.getMonth()];
        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][dateNew.getDay()];
        return days + ', ' + dateNew.getDate() + ' ' + months + ' ' + dateNew.getFullYear();
    },
    timestampToDate: function (timestamp) {
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        let currDate = new Date(timestamp);
        return currDate.getDate() + ' ' + months[currDate.getMonth()] + ' ' + currDate.getFullYear();
    },
    transferPeriodFormat: function (month, year) {
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return months[month - 1] + ' ' + year;
    },
    endDateForTrnasfer: function (year, month) {
        if (year % 4 == 0) {
            if (month == 2) {
                return year + '/' + month + '/' + '29';
            } else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                return year + '/' + month + '/' + '31';
            } else if (month == 4 || month == 6 || month == 9 || month == 11) {
                return year + '/' + month + '/' + '30';
            }
        } else {
            if (month == 2) {
                return year + '/' + month + '/' + '28';
            } else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                return year + '/' + month + '/' + '31';
            } else if (month == 4 || month == 6 || month == 9 || month == 11) {
                return year + '/' + month + '/' + '30';
            }
        }
    },
    getUrlParameter: function (sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    },
    timeStampToDate: function (timestamp) {
        let currDate = new Date(timestamp);
        return this.dateFormat(currDate.getDate(), currDate.getMonth() + 1, currDate.getFullYear());
    },
    timeStampToDateAndTime: function (timestamp) {
        var dateObj = new Date(timestamp);
        var date = this.dateFormat(dateObj.getDate(), dateObj.getMonth() + 1, dateObj.getFullYear());
        var hours = dateObj.getHours();
        hours = (hours < 10) ? "0" + hours.toString() : hours.toString();
        var minutes = dateObj.getMinutes();
        minutes = (minutes < 10) ? "0" + minutes.toString() : minutes.toString();
        var time = hours + ":" + minutes;
        return {
            date: date,
            time: time
        }
    },
    isEmail: function (email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    },
    monthtoMonthName: function (monthVal) {
        let inputVal = monthVal.split('-');
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return months[inputVal[0] - 1] + ' ' + inputVal[1];
    },
    monthNameToMonth: function (monthVal) {
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return months.indexOf(monthVal) > 8 ? (parseInt(months.indexOf(monthVal)) + 1) : '0' + (parseInt(months.indexOf(monthVal)) + 1).toString();
    },
    timeStampToTime: function (date) {
        return new Date(date).toLocaleTimeString(navigator.language, {
            hour: '2-digit',
            minute: '2-digit'
        });
    },
    getParameterByName: function (name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    showErrmsg: function (text, ele) {
        let error = $('.error');
        error.text(text.toUpperCase());
        $('.success').css('display', 'none');
        error.css('display', 'block');
        $(ele).css('border-color', 'red');
        $(ele).focus();
    },
    hideErrmsg: function (ele) {
        $(ele).css('border-color', '#ccc');
        $('.error').css('display', 'none');
    },
    isValidEmail: function (email) {
        return /[^ ]+@[^ ]+\.[^ ][^ ]+/.test(email);
    },
    isValidMobile: function (number) {
        return /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.exec(number);
    },
    isValidPincode: function (pin) {
        return /^([1-9])([0-9]){5}$/.test(pin);
    },
    isValidAmount: function (amount) {
        if (amount && /^[a-zA-Z0-9- ]*$/.test(amount) && amount.indexOf(' ') === -1) {
            return true;
        } else {
            return false;
        }
    },
    isValidContact: function (contact) {
        if (this.isValidEmail(contact) || this.isValidMobile(contact)) {
            return true
        } else {
            return false;
        }
    },
    isFilesRowValid: function (data) {
        return data.length <= 500 ? true : false;
    },
    showMessage: function (text, type) {
        let success = $('.success');
        let error = $('.error');
        if (type === 'error') {
            success.css('display', 'none');
            error.css('display', 'block');
            error.append('<div class="msg">' + text + '</div>');
        } else if (type === 'success') {
            error.css('display', 'none');
            success.append('<div class="msg">' + text + '</div>');
            success.css('display', 'block');
        }
    },
    epochToIstDate: function (epoch) {
        var date = new Date(epoch);
        var dateFormat;
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var monthIndex = date.getMonth();
        dateFormat = date.getDate() + " " + months[monthIndex] + " " + date.getFullYear();
        return dateFormat;
    },
    epochToIstTime: function (epoch) {
        var date = new Date(epoch);
        var timeFormat;
        timeFormat = date.getHours() + ":" + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
        return timeFormat;
    },
    // Convert epoch date to -> date: "dd month year" and time: "hr min sec"
    epochDateFormat: function (utcSeconds) {
        var dateObj = new Date(0); // The 0 there is the key, which sets the date to the epoch
        dateObj.setUTCSeconds(utcSeconds);
        var date = this.dateFormat(dateObj.getDate(), dateObj.getMonth() + 1, dateObj.getFullYear());
        var time = dateObj.getHours() + ":" + dateObj.getMinutes();
        return {
            date: date,
            time: time
        }
    },
    calculateAge: function (birthday) { // birthday is a date
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    },
    checkGenie: function () {
        let checkGenie = 0;
        if (localStorage.getItem('checkGenie')) {
            checkGenie = parseInt(localStorage.getItem('checkGenie'));
        }
        if (checkGenie == 0) {
            return false;
        } else {
            return true;
        }
    },
    renewAuth: function () {
        let checkGenie = 0;
        if (localStorage.getItem('checkGenie')) {
            checkGenie = parseInt(localStorage.getItem('checkGenie'));
        }
        if (checkGenie == 0) {
            let currentPage = encodeURIComponent(window.location.pathname);
            document.location.href = '/logout';
            // ?redirectUrl=' + window.location.origin + currentPage;
        } else {
            alert("You may not have access to this feature");
        }
    },
    redirectToUrl: function (url) {
        setTimeout(function () {
            window.location.href = url;
        }, 100);
    },
    dateToepochTime: function (date, separate, beforeAfter) {
        var myDate = date;
        myDate = myDate.split(separate);
        let currenthours = new Date().getHours();
        let currentMinutes = new Date().getMinutes();
        var newDate = myDate[1] + "/" + myDate[2] + "/" + myDate[0];
        if (beforeAfter == 'before') {
            return new Date(newDate + ' ' + currenthours + ':' + currentMinutes).getTime();
        }
        if (beforeAfter == 'after') {
            return new Date(newDate).getTime();
        }
    },
    htmlDecoder: function (encodedStr) {
        return $('<div/>').html(encodedStr).text();
    },
    htmlEntities: function (str) {
        let encodedStr = str.replace(/[\u00A0-\u9999<>\&]/gim, function (i) {
            return '&#' + i.charCodeAt(0) + ';';
        });
        return encodedStr.replace(/&/gim, '&amp;');
    },
    sortArrayOfObject: function (obj = [], field = "", type = 'ASC') {
        // debugger;
        let sorted;
        if (type == 'ASC') {
            sorted = _.sortBy(obj, field);
        } else {
            sorted = _.sortBy(obj, field).reverse();
        }
        return sorted;
    },
    imageCarousel: function () {
        var $this;
        $this = $("#bills-carousel");
        let numItems = $('#bills-carousel .carousel-inner .item').length;
        if (numItems == 1) {
            $this.children(".left").hide();
            $this.children(".right").hide();
        } else {
            if ($("#bills-carousel .carousel-inner .item:first").hasClass("active")) {
                $this.children(".left").hide();
                $this.children(".right").show();
            } else if ($("#bills-carousel .carousel-inner .item:last").hasClass("active")) {
                $this.children(".right").hide();
                $this.children(".left").show();
            } else {
                $this.children(".carousel-control").show();
            }
        }
    },
    getQueryParamFromURL: function (url, field) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    },
    generatePassword: function(p_length) {
        var length = p_length || 10,
            charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
            numbers = "0123456789",
            characters = "!@#$%^&*",
            retVal = "";
            const charactersLength = characters.length;
            const numbersLength = numbers.length;
        for (var i = 0, n = charset.length; i < length; ++i) {
            if (i == 3) {
                retVal += characters.charAt(Math.floor(Math.random() * charactersLength)); 
            } else if (i == 4) {
                retVal += numbers.charAt(Math.floor(Math.random() * numbersLength));
            } else {
                retVal += charset.charAt(Math.floor(Math.random() * n));
            }
        }
        return retVal;
    },
    getYearEndDates: function () {
        let dateObj = new Date();
        let financialYear = ''
        if (dateObj.getMonth() <= 2) {
            financialYear = dateObj.getFullYear()
        } else {
            financialYear = parseInt(dateObj.getFullYear()) + 1;
        }
        return {
            startDate: '15/03/' + financialYear,
            endDate: '31/03/' + financialYear
        }
    },
    generateOtp: function () {
        let digits = '0123456789';
        let otpLength = 4;
        let otp = '';
        for (let i = 1; i <= otpLength; i++) {
            var index = Math.floor(Math.random() * (digits.length));
            otp = otp + digits[index];
        }
        return otp;
    },
    currencyNameToSymbol: function(currency_name) {
        let name = currency_name || 'INR';
        if(CURRENCY_CONFIGS[name] !== undefined) {
            return CURRENCY_CONFIGS[name].symbol;
        }
    },
    getLanguagesByCurrency: function(currency_name) {
        let name = currency_name || 'INR';
        if(CURRENCY_CONFIGS[name] !== undefined) {
            return CURRENCY_CONFIGS[name].languages;
        }
    },
    formatCurrency: function(number, currency_name, language, currencyConfig) {
        currency_name = currency_name && CURRENCY_CONFIGS[currency_name] !== undefined ? currency_name : 'INR'; // get currency name
        currencyConfig = currency_name && CURRENCY_CONFIGS[currency_name];
        const languages = this.getLanguagesByCurrency(currency_name); // get languages object
        language = languages && Object.keys(languages).length ? language && languages[language] && language || Object.keys(languages)[0] : 'en-IN'; // select language
        let config = {currency: currency_name, minimumFractionDigits: 0}; // default config
        // checkout more about config options: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toLocaleString#Parameters
        if (currencyConfig && Object.keys(currencyConfig).length) { // extend default config
            config = _.extend(config, currencyConfig);
        }
        let amountValue = parseFloat(number).toLocaleString(language, config);
        if (!config.style || (config.style && config.style === 'decimal') && languages[language].displaySymbolPosition) {
            amountValue = (languages[language].displaySymbolPosition === 'PREFIX' ? this.currencyNameToSymbol(currency_name) : '') + amountValue + (languages[language].displaySymbolPosition === 'POSTFIX' ? this.currencyNameToSymbol(currency_name): '');
        }
        return amountValue;
    },
    checkPriviledge: function(rbac, action){
        let companyID = Meta.getMetaElement('companyID');
        let hasPriviledge = true;
        let newAction = action+'_'+companyID
        if (rbac && Object.keys(rbac).length > 1){
            if(!rbac[newAction]){
                hasPriviledge = false;
            }
        }
    
        return hasPriviledge;
    },
    isSodexoPhilipines: function(){
        return Meta.getMetaElement('countryVar') == 'isSodexoPh' ? true : false;
    },
    isProgramManagerRead: function(rbac) {
        let companyID = Meta.getMetaElement('companyID');
        let isProgramManagerRead = false
        let action = "listOrders_"+companyID
        if (rbac && Object.keys(rbac).length > 1){
            if(rbac[action]){
                isProgramManagerRead = true;
            }
        }
    
        return isProgramManagerRead
    },
    
    isProgramManagerWrite: function(rbac) {
        let companyID = Meta.getMetaElement('companyID');
        let isProgramManagerWrite = false
        let action = "createOrder_"+companyID
        if (rbac && Object.keys(rbac).length > 1){
            if(rbac[action]){
                isProgramManagerWrite = true;
            }
        }
    
        return isProgramManagerWrite
    }
}
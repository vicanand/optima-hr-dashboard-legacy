var _storage = {};
export
default {
    getItem: function(itemName) {
        return _storage[itemName] ? _storage[itemName] : false;
    },
    setItem: function(itemName, data) {
        _storage[itemName] = data;
    }
}

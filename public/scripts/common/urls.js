import Env from './env.js';

export
default {
    LOGIN_BASE_URL: {
        STAGE: 'http://login-stage.zetaapps.in?cat=op&from=',
        PROD: 'https://login.zetaapps.in?cat=op&from='
    },

    SSO_LOGIN_BASE_URL: {
        STAGE: 'https://sso-stage.zetaapps.in/?redirectUrl=',
        PREPROD: 'http://sso-pp.zetaapps.in/?redirectUrl=',
        PROD: 'https://sso.zetaapps.in/?redirectUrl='
    },

    SSO_LOGOUT_BASE_URL: {
        STAGE: 'https://sso-stage.zetaapps.in/logout?redirectUrl=',
        PREPROD: 'http://sso-pp.zetaapps.in/logout?redirectUrl=',
        PROD: 'https://sso.zetaapps.in/logout?redirectUrl='
    },

    LOGGING_BASE_URL: {
        PROD: 'https://api.zeta.in/zeta.in/analytics-datacollector/1.0/recordEvents'
    },

    NEW_CORP_URl: {
        STAGE: 'https://api.stage.zeta.in/zeta.in/',
        PROD: 'https://api.zeta.in/zeta.in/'
    },

    CORP_BASE_URL: {
        STAGE: 'http://mv-stage.dash.zeta.in',
        PREPROD: 'https://corp-pp.zetaapps.in',
        PROD: 'https://corp.zetaapps.in'
    },

    OLD_LOGIN_URLS: {
        STAGE: 'http://mv-stage.dash.zeta.in/#/login',
        PREPROD: 'https://corp-pp.zetaapps.in/#/login',
        PROD: 'https://corp.zetaapps.in/#/login'
    },

    OMS_BASE_URL: {
        STAGE: 'https://api.stage.zeta.in',
        PREPROD: 'https://api.preprod.zeta.in',
        PROD: 'https://api.gw.zetapay.in'
    },

    AVAIL_FINANCE_BASE_URL: {
        STAGE: 'http://staging1-sg-1b.aws.zeta.in:7595',
        PREPROD: 'https://avail-finance.preprod.zeta.in',
        PROD: 'https://avail-finance.gw.zetapay.in'
    },

    RESET_PIN_BASE_URL: {
        STAGE: 'https://acspropercert.stage.zeta.in',
        PREPROD: 'https://supercard-gw.preprod.zeta.in',
        PROD: 'https://supc.gw.zetapay.in'
    },

    SODEXO_VN_BASE_URL: {
        STAGE: 'http://sodexo-vn.stage.esg.zeta.in',
        PREPROD: 'https://sodexo-vn.preprod.zeta.in',
        PROD: 'https://sodexovn.gw.zetapay.in'
    },

    AVAIL_FINANCE_API_URL: {
        GET_EMPLOYEESV2_STATUS: ':AVAIL_FINANCE_BASE_URL:/avail-finance/user/getUserStatus?token=:authToken:',
        ADD_USER_STATUS: ':AVAIL_FINANCE_BASE_URL:/avail-finance/user/addUserGroup?userID=:USER_ID:&userName=:USER_NAME:&token=:authToken:',
        REMOVE_USER_STATUS: ':AVAIL_FINANCE_BASE_URL:/avail-finance/user/removeUserGroup?userID=:USER_ID:&token=:authToken:',
    },

    OMS_API_URL: {
        ADD_FUNDS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/addFunds?token=:API_KEY:',
        GET_ORDER_REPORT: ':OMS_BASE_URL:/zeta.in/corpben/1.0/downloadPayoutsForOrder',
        GET_ORDER_FILE: ':OMS_BASE_URL:/zeta.in/corpben/1.0/downloadOrderFile',
        GET_EACH_FUND_ACCOUNT_DETAIL: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getAddFundTicketsForCorp?fundingAccountId=:ACCOUNT_ID:',
        GET_ACTIVE_BENEFIT_PROGRAMS_PAGINATION_POST: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getBenefitPrograms',
        GET_ACTIVE_BENEFIT_PROGRAMS_PAGINATION: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getBenefitPrograms?token=:authToken:&corpID=:corpID:&companyID=:companyID:&programStatus=:programStatus:&pageNumber=:pageNumber:&pageSize=:pageSize:',
        GET_ACTIVE_BENEFIT_PROGRAMS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getBenefitPrograms?token=:authToken:&corpID=:corpID:&companyID=:companyID:&programStatus=:programStatus:',
        GET_BENEFIT_PROGRAMS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getBenefitPrograms?token=:API_KEY:&corpID=:CORP_ID:&companyID=:COMPANY_ID:',
        ADD_ACCOUNTS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/addFundingAccount?token=:API_KEY:',
        GET_EMPLOYEES: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getEmployees?corpID=:CORP_ID:&token=:API_KEY:',
        GET_EMPLOYEESV2: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getEmployeesV2?corpID=:CORP_ID:&token=:API_KEY:',
        CLOSE_CARD: ':OMS_BASE_URL:/zeta.in/corpben/1.0/closeCard?token=:API_KEY:',
        GET_PROGRAMDETAILSV2: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getProgramDetails?token=:API_KEY:',
        GET_INDIVIDUAL_EMPLOYEE: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getEmployeeDetails?corpID=:CORP_ID:&token=:API_KEY:&employeeID=:EMPLOYEE_ID:',
        GET_YEAR_END_CLOSURE: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getProgramClosureSuggestion?corpID=:CORP_ID:&companyID=:COMPANYID:&expectedFinalClosureDate=:CLOSURE_DATE:&token=:API_KEY:',
        SCHEDULE_PROGRAM_CLOSURE: ':OMS_BASE_URL:/zeta.in/corpben/1.0/scheduleProgramClosure',
        GET_ACCOUNT_STATEMENT: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getFundingAccountTransactions?corpID=:CORP_ID:&accountID=:ACCOUNT_ID:&token=:API_KEY:',
        GET_DOCS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getDocGroups',
        DOWNLOAD_DOCS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/exportDocuments',
        GET_DOC: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getDocGroup',
        GET_BILL_DETAILS: ':OMS_BASE_URL:/zeta.in/biller/1.0/getBill',
        DOWNLOAD_REPORT: ':OMS_BASE_URL:/zeta.in/corpben/1.0/downloadReport',
        GET_AVAILABLE_REPORTS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getAvailableReports?token=:authToken:&corpID=:CORP_ID:',
        GET_AVAILABLE_REPORTS_FOR_PRODUCTS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getAvailableReportsForProduct?token=:authToken:&corpID=:CORP_ID:&corpProductType=:product:',
        GET_WHITELISTED_BANK_ACCOUNTS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getWhitelistedBankAccounts?corpID=:CORP_ID:&token=:API_KEY:',
        GET_AGGREGATE_PAYOUTS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getAggregatePayouts?corpID=:CORPID:&token=:API_KEY:&companyID=:COMPANYID:&programID=:PROGRAMID:',
        GET_EMPLOYEE_PAYOUTS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getPayouts?corpID=:CORPID:&token=:API_KEY:&companyID=:COMPANYID:&programID=:PROGRAMID:&employeeID=:EMPLOYEEID:',
        GET_PAYOUTS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getPayouts?corpID=:CORPID:&token=:API_KEY:&companyID=:COMPANYID:&programID=:PROGRAMID:',
        GET_AGGREGATIONS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getAggregations?token=:authToken:&corpID=:corpID:&companyID=:companyID:&programID=:programID:&aggregationType=:aggregationType:',
        GET_PAYOUT_STATS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getPayoutStats?corpID=:CORPID:&token=:API_KEY:&companyID=:COMPANYID:&programID=:PROGRAMID:',
        GET_RESET_PIN_REQUEST: ':OMS_BASE_URL:/zeta.in/instagift/1.0/getResetPinRequests?token=:API_KEY:&companyID=:COMPANYID:&corpID=:CORPID:',
        PROCESS_RESETPIN_REQUEST: ':OMS_BASE_URL:/zeta.in/instagift/1.0/processResetPinRequest?token=:API_KEY:&companyID=:COMPANYID:&corpID=:CORPID:&requestID=:REQUEST_ID:&action=:REJECT:',
        GET_INSTAGIFT_ORDERS: ':OMS_BASE_URL:/zeta.in/instagift/1.0/getOrders?token=:API_KEY:&companyID=:COMPANYID:&corpID=:CORPID:&programID=:PROGRAMID:',
        GET_INSTAGIFT_PAYOUTS: ':OMS_BASE_URL:/zeta.in/instagift/1.0/getPayouts?token=:API_KEY:&companyID=:COMPANYID:&corpID=:CORPID:&orderID=:ORDER_ID:',
        GET_FUNDING_ACCOUNTS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getFundingAccounts?corpID=:corpID:&token=:authToken:',
        GET_ORDER_CARDS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getSalesManagers?corpID=:corpID:&companyID=:COMPANYID:&token=:authToken:',
        SAVE_PROGRAM_INTEREST: ':OMS_BASE_URL:/zeta.in/instagift/1.0/saveProgramInterest?token=:authToken:',
        SETUP_NEW_PROGRAM: ':OMS_BASE_URL:/zeta.in/instagift/1.0/setupNewProgram?token=:authToken:',
        // changed createOrder to loadMoneyByCardNumber for insta gift
        LOAD_CARDS: ':OMS_BASE_URL:/zeta.in/instagift/1.0/loadMoneyByCardNumber?token=:authToken:',
        RECORD_EVENTS: ':OMS_BASE_URL:/zeta.in/analytics-datacollector/1.0/recordEvents',
        GET_ESCALATED_CLAIMS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getEscalatedClaims',
        APPROVE_ESCALATED_CLAIM: ':OMS_BASE_URL:/zeta.in/corpben/1.0/approveEscalatedClaim',
        DECLINE_ESCALATED_CLAIM: ':OMS_BASE_URL:/zeta.in/corpben/1.0/declineEscalatedClaim',
        GET_BILLS_FOR_PROGRAM: ':OMS_BASE_URL:/zeta.in/biller/1.0/getBillsForCardprogram',
        GET_APPROVER_DEPENDANTS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getCorpConfig?token=:authToken:&cardID=:cardID:',
        //DOWNLOAD_REPORTS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/downloadReport?token=:authToken:',
        DOWNLOAD_LOADREPORTS: ':OMS_BASE_URL:/zeta.in/instagift/1.0/getCardsFileDownloadUrl?token=:token:&companyID=:companyID:&corpID=:corpID:&programID=:programID:&cardOrderID=:cardOrderID:',
        DOWNLOAD_REPORTS: ':OMS_BASE_URL:/zeta.in/instagift/1.0/downloadPayoutReport?token=:token:',
        GET_CORP_ACCOUNTS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getCorpAccounts?corpID=:corpID:&token=:authToken:',
        GET_CORP_ACCOUNTSV2: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getCorpAccountsV2?corpID=:corpID:&token=:authToken:',
        ADD_CORP_ACCOUNT: ':OMS_BASE_URL:/zeta.in/corpben/1.0/addCorpAccount?token=:authToken:',
        GIVE_PRIVELEGES_TO_USER: ':OMS_BASE_URL:/zeta.in/corpben/1.0/givePrivilegeToUser?token=:authToken:',
        CREATE_USER_WITH_DETAILS: ':OMS_BASE_URL:/zeta.in/zetauser/1.0/createUserWithDetails?token=:authToken:',
        ADD_ACCOUNT_FOR_USER: ':OMS_BASE_URL:/zeta.in/corpben/1.0/addAccount?token=:authToken:',
        ADD_ACCOUNT_FOR_EMPLOYEE: ':OMS_BASE_URL:/zeta.in/corpben/1.0/addAccountForEmployee?token=:authToken:',
        CHANGE_CORP_ACCOUNT_ROLE: ':OMS_BASE_URL:/zeta.in/corpben/1.0/changeCorpAccountRole?token=:authToken:',
        CHANGE_ACCOUNT_STATUS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/changeAccountStatus?token=:authToken:',
        CHANGE_CORP_ACCOUNT_PASSSWORD: ':OMS_BASE_URL:/zeta.in/corpben/1.0/changeCorpAccountPassword?token=:authToken:',
        REMOVE_CORP_ACCOUNT: ':OMS_BASE_URL:/zeta.in/corpben/1.0/removeCorpAccount?token=:authToken:',
        GET_ORDER_SUMMARY: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getOrderSummary?token=:authToken:',
        PROCESS_ERROR_STATE_ORDER: ':OMS_BASE_URL:/zeta.in/corpben/1.0/processErrorStateOrder?token=:authToken:',
        MOVE_FUNDS_BETWEEN_FAS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/moveFundsBetweenFAs?token=:authToken:',
        UPDATE_COMPANY_DETAILS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/updateCompanyDetails?token=:authToken:',
        UPDATE_CORPORATE_DETAILS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/verifyNewCorporateDetails?token=:authToken:',
        SIGNUP: ':OMS_BASE_URL:/zeta.in/corpben/1.0/corporateSignUpRequest',
        REVOKE_PAYMENT: ':OMS_BASE_URL:/zeta.in/instagift/1.0/revokePayout?token=:authToken:',
        GET_ASSET_UPLOAD_ENDPOINT: ':OMS_BASE_URL:/zeta.in/locker/1.0/getAssetUploadEndpoint',
        GET_SIGNED_DOCUMENT: ':OMS_BASE_URL:/zeta.in/corpben/1.0/signPDF?token=:authToken:&pdfURL=:s3FileUrl:&apiKey=:apiKey:&x=:x:&y=:y:&width=:width:&height=:height:',
        CREATE_ORDER: ':OMS_BASE_URL:/zeta.in/corpben/1.0/createOrder?token=:token:&corpID=:corpID:',
        CREATE_ORDER_V2: ':OMS_BASE_URL:/zeta.in/corpben/1.0/createOrderV2?token=:token:&corpID=:corpID:',
        ACTION_ON_ORDER: ':OMS_BASE_URL:/zeta.in/corpben/1.0/actionOnOrder?token=:token:',
        CREATE_ORDER_RECURRING: ':OMS_BASE_URL:/zeta.in/corpben/1.0/createRecurringOrder?token=:token:&corpID=:corpID:',
        ADD_PAYOUT: ':OMS_BASE_URL:/zeta.in/corpben/1.0/addPayouts?token=:token:',
        GET_PRODUCT_DETAILS: ':OMS_BASE_URL:/zeta.in/partner-gifting/1.0/getProductDetail?corporateId=:corporateId:&companyId=:companyId:&spotlightType=SELECT_GIFTING&token=:token:&productId=:productId:',
        GET_LIST_PRODUCT: ':OMS_BASE_URL:/zeta.in/partner-gifting/1.0/listProduct?spotlightType=:spotlightType:',
        SELECT_GIFT_ORDER_VERIFY: ':OMS_BASE_URL:/zeta.in/partner-gifting/1.0/orderVerify',
        SELECT_GIFT_ORDER_CREATE: ':OMS_BASE_URL:/zeta.in/partner-gifting/1.0/createOrder',
        SELECT_GIFT_GET_PROGRAM: ':OMS_BASE_URL:/zeta.in/partner-gifting/1.0/getProgram?corporateId=:corporateId:&companyId=:companyId:&spotlightType=SELECT_GIFTING&token=:token:',
        GET_SELECT_GITFTING_GENERATE: ':OMS_BASE_URL:/zeta.in/partner-gifting/1.0/generateGiftCard?claimResource=:claimResource:',
        GET_SELECT_GITFTING_ORDERLIST: ':OMS_BASE_URL:/zeta.in/partner-gifting/1.0/listOrder?',
        GET_SELECT_GITFTING_CSVURL: ':OMS_BASE_URL:/zeta.in/partner-gifting/1.0/getDownloadOrderFileUrl?',
        CREATE_BULK_ORDER: ':OMS_BASE_URL:/zeta.in/corpben/1.0/createBulkOrder?token=:authToken:',
        GET_ERROR_ROWS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getErrorRows?token=:authToken:&corpID=:CORP_ID:&bulkOrderID=:bulkOrderID:',
        BULK_DOWNLOAD_REPORT: ':OMS_BASE_URL:/zeta.in/corpben/1.0/downloadBulkOrderErrorReport?token=:authToken:&corpID=:CORP_ID:&bulkOrderID=:bulkOrderID:',
        DOWNLOAD_ERROR_REPORT: ':OMS_BASE_URL:/zeta.in/corpben/1.0/downloadOrderErrorReport?token=:authToken:&corpID=:CORP_ID:&orderID=:bulkOrderID:',
        ACTION_ON_BULK_ORDER: ':OMS_BASE_URL:/zeta.in/corpben/1.0/actionOnBulkOrder?token=:authToken:',
        ACTION_ON_CLOSE_BULK_ORDER: ':OMS_BASE_URL:/zeta.in/corpben/1.0/actionOnCloseOrder?token=:authToken:',
        GET_BULK_ORDERS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getBulkOrders?token=:authToken:&corpID=:CORP_ID:&orderByDate=true',
        LIST_INDIVIDUAL_ORDERS_IN_BULK_ORDER: ':OMS_BASE_URL:/zeta.in/corpben/1.0/listIndividualOrdersInBulkOrder?token=:authToken:&corpID=:CORP_ID:&bulkOrderID=:BULK_ORDER_ID:',
        GET_COMPANY_SHORTCODES: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getCompanies?token=:authToken:&corpID=:CORP_ID:',
        GET_BULK_ORDER_SUMMARY: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getBulkOrderSummary?token=:authToken:&corpID=:CORP_ID:&bulkOrderID=:BULK_ORDER_ID:',
        CLOSE_BULK_CARDS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/createCloseCardOrder?token=:authToken:',
        GET_CLOSE_ORDERS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getCloseCardOrders?token=:authToken:&corpID=:CORP_ID:&orderByDate=true',
        DONWLOAD_BULK_ORDER_FILE: ':OMS_BASE_URL:/zeta.in/corpben/1.0/downloadBulkOrderFile?token=:authToken:&corpID=:CORP_ID:&bulkOrderID=:BULK_ORDER_ID:',
        GET_CLOSE_CARD__ORDER_REPORT: ':OMS_BASE_URL:/zeta.in/corpben/1.0/downloadReport?token=:authToken:',
        GET_CLOSE_FILE_REPORT: ':OMS_BASE_URL:/zeta.in/corpben/1.0/downloadCloseCardOrderFile?token=:authToken:&corpID=:corpID:&closeCardOrderID=:closeOrderID:',
        GET_FUNDING_ACCOUNT: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getFundingAccountDetails?corpID=:corpID:&accountID=:accountID:&token=:authToken:',
        GET_ADDPRODUCT_REQUEST: ':OMS_BASE_URL:/zeta.in/partner-gifting/1.0/addProductRequest?corporateId=:corporateId:&companyId=:companyId:&spotlightType=:spotlightType:&token=:token:&productId=:productId:',
        SET_PROGRAM_SHORTCODE: ':OMS_BASE_URL:/zeta.in/corpben/1.0/updateCompanyDetails?token=:token:',
        //GET_ADDPRODUCT_REQUEST: ':OMS_BASE_URL:/zeta.in/partner-gifting/1.0/addProductRequest?corporateId=:corporateId:&companyId=:companyId:&spotlightType=:spotlightType:&token=:token:&productId=:productId:',
        GET_IFIS_ACCOUNTS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getIFIDetails?corpID=:corpID:&ifiID=:ifiID:&type=:type:&token=:authToken:',
        CANCEL_PAYOUT: ':OMS_BASE_URL:/zeta.in/corpben/1.0/cancelPayout?token=:token:',
        GET_CUSTOMER_RATINGS: ':OMS_BASE_URL:/zeta.in/quickshop/1.0/getCustomerRatings',
        GET_CUSTOMER_FEEDBACK: ':OMS_BASE_URL:/zeta.in/quickshop/1.0/getCustomerFeedback',
        GET_CUSTOMER_COMMENTS: ':OMS_BASE_URL:/zeta.in/quickshop/1.0/getCustomerComments',
        GET_ORDERS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/listOrders?token=:token:',
        CREAT_INSTA_ORDER: ':OMS_BASE_URL:/zeta.in/instagift/1.0/createCardOrder?token=:authToken:',
        GET_INSTAGIFT_LOADORDERS: ':OMS_BASE_URL:/zeta.in/instagift/1.0/listCardOrder?token=:API_KEY:',
        GET_LEDGERID: ':OMS_BASE_URL:/zeta.in/wallet/1.0/getLedgerID?token=:token:&walletID=:walletID:&cardID=:cardID:',
        GET_TRANSACTIONS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getTransactionsForCardID?token=:token:&cardID=:cardID:&corpID=:corpID:&offset=0&order=OLDEST_FIRST&pageSize=200&recordTypes=DEBIT&recordTypes=CREDIT&zetaUserID=:zetaUserID:&after=:after:&before=:before:',
        GET_BILL_FOR_CARDPROGRAM: ':OMS_BASE_URL:/zeta.in/biller/1.0/getBillsForCardprogram?token=:token:',
        GET_CARD_ELIGIBILITY: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getCorpConfig?token=:authToken:&corpID=:corpID:&cardID=:cardID:',
        GET_FUNDING_ACCOUNTSS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getFundingAccounts?corpID=:corpID:&pageNo=:pageNo:&pageSize=:pageSize:&type=:type:&token=:authToken:',
        CANCEL_ORDER: ':OMS_BASE_URL:/zeta.in/corpben/1.0/cancelOrder?token=:token:&corpID=:corpID:&orderID=:orderID:',
        GET_FILE_UPLOAD_DETAILS: 'getFileUploadDetails',

        //Reward APIS
        GET_REWARD_DETAILS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/listOrders?token=:token:',
        GET_TEMLATEBYID_DETAILS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/cardProgramTemplateDataById?token=:token:&programId=:programId:&corporateID=:corporateID:&companyID=:companyID:',
        CREATE_CUSTOM_CARD: ':OMS_BASE_URL:/zeta.in/corpben/1.0/createCustomCardProgram?token=:token:',
        GET_EMPLOYEESV2_REWARD: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getEmployeesByProgramType?token=:token:&corpID=:corpID:&companyID=:companyID:&programType=:programType:',
        GET_BENEFIT_DETAILS_PROGRAM: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getBenefitProgramDetails?token=:authToken:&corpID=:corpID:&companyID=:companyID:&programID=:programID:',
        GET_FUNDING_ACCOUNTS_REWARD: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getFundingAccounts?corpID=:corpID:&pageNo=:pageNo:&pageSize=:pageSize:&token=:authToken:',

        //Virtual APIS
        GET_VIRTUAl_ORDER_DETAILS: ':OMS_BASE_URL:/zeta.in/instagift/1.0/getOrders?token=:token:&companyID=:companyID:&corpID=:corpID:&spotlightType=:spotlightType:',
        GET_VIRTUAl_INDIVIDUAL_ORDER: ':OMS_BASE_URL:/zeta.in/instagift/1.0/getPayouts?corpID=:corpID:&companyID=:companyID:&token=:authToken:&orderID=:orderID:',
        CREATE_NEW_PROGRAM_VIRTUAL: ':OMS_BASE_URL:/zeta.in/instagift/1.0/setupNewProgram?token=:token:',
        ORDER_VERIFY_BULK_VIRTUAL: ':OMS_BASE_URL:/zeta.in/instagift/1.0/orderVerifyV2',
        ORDER_CREATE_VIRTUAL: ':OMS_BASE_URL:/zeta.in/instagift/1.0/createOrderV2?token=:token:',

        // PHYSICAL APIS
        CREAT_PHYSICAL_ORDER: ':OMS_BASE_URL:/zeta.in/instagift/1.0/createCardOrder?token=:authToken:',
        GET_PHYSICAL_LOADORDERS: ':OMS_BASE_URL:/zeta.in/instagift/1.0/listCardOrder?token=:API_KEY:',
        GET_PHYSICAL_ORDERS: ':OMS_BASE_URL:/zeta.in/instagift/1.0/getOrders?token=:API_KEY:&companyID=:COMPANYID:&corpID=:CORPID:',
        CREATE_ISSUE_BULK_ORDER_VERIFY: ':OMS_BASE_URL:/zeta.in/instagift/1.0/loadMoneyByZetaUserVerify?token=:token:',
        CREATE_ISSUE_BULK_ORDER: ':OMS_BASE_URL:/zeta.in/instagift/1.0/loadMoneyByZetaUser?token=:token:',
        GET_ALL_REPORTS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getArchivedReports?corpID=:corpID:&token=:authToken:&pageNo=:pageNo:&pageSize=:pageSize:&latestFirst=true',
        GET_REPORT_BY_ID: ':OMS_BASE_URL:/zeta.in/corpben/1.0/downloadReportByID?token=:authToken:&corporateID=:corporateId:&reportID=:reportId:',

        //new revoke API
        REVOKE_API: ':OMS_BASE_URL:/zeta.in/corpben/1.0/bulkRevokePayouts?token=:token:',
        REISSUE_API: ':OMS_BASE_URL:/zeta.in/instagift/1.0/reissuePayout?token=:token:&payoutID=:payoutID:&corpID=:corpID:&companyID=:companyID:&orderID=:orderID:&email=:email:&phone=:phone:&comments=:comments:&name=:name:',

        //revoke for virtual and physical
        REVOKE_API_NEW_INSTA: ':OMS_BASE_URL:/zeta.in/instagift/1.0/revokePayout?token=:token:',

        //invoice
        GET_SELECT_GITFTING_INVOICEURL: ':OMS_BASE_URL:/zeta.in/partner-gifting/1.0/getOrderInvoice?token=:token:&corporateId=:corpID:&companyId=:companyID:&orderId=:order:',
        GET_VIRTUAL_INVOICE: ':OMS_BASE_URL:/zeta.in/instagift/1.0/getOrderInvoice?token=:token:&corporateID=:corpID:&companyID=:companyID:&orderID=:order:',
        GET_PHYSICAL_CARD_INVOICE: ':OMS_BASE_URL:/zeta.in/instagift/1.0/getCardOrderInvoice?token=:token:&corporateID=:corpID:&companyID=:companyID:&cardOrderID=:order:',
        GET_INVOICES: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getInvoicesForCorp?token=:TOKEN:&corpId=:CORP_ID:&pageNo=:PAGE_NO:&pageSize=:PAGE_SIZE:',
        GET_VIRTUAL_INVOICE: ':OMS_BASE_URL:/zeta.in/instagift/1.0/getOrderInvoice?token=:token:&corporateID=:corpID:&companyID=:companyID:&orderID=:order:',

        //RnR API
        GET_REWARD_ISSUED_SUMMARY: ':OMS_BASE_URL:/zeta.in/sirius/1.0/getRewardIssuedSummary?token=:token:&programID=:programID:',
        LIST_PROGRAM_DETAILS: ':OMS_BASE_URL:/zeta.in/sirius/1.0/listProgramDetails?companyID=:companyID:&corpID=:corpID:&token=:token:',
        GET_REWARD_LIST: ':OMS_BASE_URL:/zeta.in/sirius/1.0/rewardActivity?companyID=:companyID:&corpID=:corpID:&programID=:programId:&token=:token:',
        VERIFY_BUDGET: ':OMS_BASE_URL:/zeta.in/sirius/1.0/verifyBudget?corpID=:corpID:&companyID=:companyID:&programID=:programID:&fileUrl=:fileUrl:&fileName=:fileName:&refName=:refName:&fundingAccountID=:fundingAccountID:&expireAt=1894883164000',
        CREATE_BUDGET: ':OMS_BASE_URL:/zeta.in/sirius/1.0/createBudget',
        UPSERT_EMPLOYEES: ':OMS_BASE_URL:/zeta.in/sirius/1.0/upsertEmployees',
        GET_FILE_UPLOADER_ORDER_HISTORY: ':OMS_BASE_URL:/zeta.in/sirius/1.0/getFileUploadOrderHistory?corpID=:corpID:&companyID=:companyID:&programID=:programID:&fileUploadOrderID=:fileUploadOrderID:&fileUploadOrderStatus=:fileUploadOrderStatus:',
        GET_ISSUANCE_HISTORY: ':OMS_BASE_URL:/zeta.in/sirius/1.0/getIssuanceHistoryByFileUploadID?corpID=:corpID:&companyID=:companyID:&programID=:programID:&fileUploadOrderID=:fileUploadOrderID:&fileUploadOrderStatus=:fileUploadOrderStatus:',
        GET_POINT_BASED_INVOICE: ':OMS_BASE_URL:/zeta.in/sirius/1.0/getFileOrderInvoice?token=:token:&corpID=:corpID:&companyID=:companyID:&fileUploadID=:order:&programID=:programID:',
        GET_BENEFICIARY: ':OMS_BASE_URL:/zeta.in/sirius/1.0/getBeneficiariesDetails?token=:token:&programID=:programID:&corpID=:corpID:&companyID=:companyID:',
        GET_REVOKE_REQUESTS: ':OMS_BASE_URL:/zeta.in/sirius/1.0/getRevokeBadgeRequests?token=:token:&programID=:programID:&corpID=:corpID:&companyID=:companyID:',
        GET_REVOKE_TRANSACTION_HISTORY: ':OMS_BASE_URL:/zeta.in/sirius/1.0/getExpiryLedgerTransactions?token=:token:&programID=:programID:&corpID=:corpID:&companyID=:companyID:',
        REVOKE_BADGES: ':OMS_BASE_URL:/zeta.in/sirius/1.0/revokeBadgeRequest',

        //corp signup
        CORP_SIGNUP: ':OMS_BASE_URL:/zeta.in/corpben/1.0/upsertCorporateSignUpRequest',

        // card cost api
        ORDER_VERIFY_BULK_CARDCOST: ':OMS_BASE_URL:/zeta.in/instagift/1.0/verifySuperCardOrder?token=:token:',
        ORDER_CREATE_CARDCOST: ':OMS_BASE_URL:/zeta.in/instagift/1.0/placeSuperCardOrder?token=:token:',
        GET_CARD_ORDER_DETAILS: ':OMS_BASE_URL:/zeta.in/instagift/1.0/getSuperCardOrderDetails?token=:token:&companyID=:companyID:&corpID=:corpID:',
        GET_DISBURSED_CARD_ORDER_DETAILS: ':OMS_BASE_URL:/zeta.in/instagift/1.0/getSuperCards?token=:token:&companyID=:companyID:&corpID=:corpID:',
        CARD_ACTIONS: ':OMS_BASE_URL:/zeta.in/instagift/1.0/cardAction?',
        GET_CARD_ORDER_INVOICEURL: ':OMS_BASE_URL:/zeta.in/instagift/1.0/getOrderInvoice?token=:token:&corporateID=:corpID:&companyID=:companyID:&orderID=:order:&spotlightProgram=:spotlightProgram:',

        //create custom card program
        CREATE_CUSTOM_CARD_PROGRAM: ':OMS_BASE_URL:/zeta.in/corpben/1.0/createCustomCardProgram?token=:token:',
        //update custom card program
        UPDATE_CUSTOM_CARD_PROGRAM: ':OMS_BASE_URL:/zeta.in/corpben/1.0/updateCardProgram?token=:token:',
        ADD_FUND_ACCOUNT: ':OMS_BASE_URL:/zeta.in/corpben/1.0/addFundingAccount?token=:token:',
        GET_INVOICE_CONFIGS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getInvoiceConfig?token=:token:&companyID=:companyID:&corpID=:corpID:',
        GET_CARD_MASTER_DETAILS: ':OMS_BASE_URL:/zeta.in/instagift/1.0/getCardMasterDetails?token=:token:&ifiID=:ifiID:',
        GET_COMMERCIALS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getCommercials?token=:token:&ifiID=:ifiID:',
        GET_AUDIT_LOGS: ':OMS_BASE_URL:/zeta.in/corpben/1.0/getAuditLogs?token=:token:&entityName=:entity_name:&entityID=:entity_id:&companyID=:company_id:&corpID=:corp_id:',
        UPDATE_INVOICE_CONFIG: ':OMS_BASE_URL:/zeta.in/corpben/1.0/updateInvoiceConfig?token=:token:'
    },

    CORP_API_URLS: {
        //GET_ORDERS: ':BASE_URL:/account/companies/:COMPANY_ID:/orders?corpID=:CORP_ID:&programID=:PROGRAMID:&token=:API_KEY:',
        GET_ORDER_PAYOUTS: ':BASE_URL:/account/companies/:COMPANY_ID:/orders/:ORDER_ID:/payouts',
        GET_FUNDS_HISTORY: ':BASE_URL:/account/companies/:COMPANY_ID:/requests',
        GET_LEGACY_ACCOUNT_STATEMENT: ':BASE_URL:/account/companies/:COMPANY_ID:/transaction',
        CHANGE_PASSWORD: ':BASE_URL:/account/password',
        GET_FILE_UPLOAD_DETAILS: ':BASE_URL:/getFileUploadDetails',
        //CREATE_ORDER: ':BASE_URL:/account/companies/:COMPANY_ID:/orders',
        // ADD_FUNDS: ':BASE_URL:/corporate/addFunds',
        //  GET_EACH_FUND_ACCOUNT_DETAIL: ':BASE_URL:/corporate/listFundRequests?fundingAccountId=:ACCOUNT_ID:',
        //CANCEL_PAYOUT: ':BASE_URL:/account/companies/:COMPANY_ID:/orders/:ORDER_ID:/payout/:PAYOUT_ID:/cancel',
        //CANCEL_ORDER: ':BASE_URL:/account/companies/:COMPANY_ID:/orders/:ORDER_ID:/cancel',
        REVOKE_PAYOUT: ':BASE_URL:/account/companies/:COMPANY_ID:/orders/:ORDER_ID:/payout/:PAYOUT_ID:/revoke',
        VIEW_STATEMENT: ':BASE_URL:/account/',
        GET_INVOICE: ':BASE_URL:/account/orders/:orderId/invoice',
        GET_REPORT_ORDER: ':BASE_URL:/account/orders/',
        GET_PERFORMA_INVOICE: ':BASE_URL:/account/orders/:orderId/proforma-invoice',
        CANCEL_PROGRAM: ':BASE_URL:/account/companies/:COMPANY_ID:/employees/:EMPLOYEE_ID:/closeProgram',
        LOGIN_MECHANISM: ':BASE_URL:/account/getLoginMechanism?email=:emailID:',
        SIGNUP: ':BASE_URL:/account/signup',
        HDFC_SIGNUP: ':BASE_URL:/account/hdfcSignup',
        OTP_VERIFY: ':BASE_URL:/account/OTP_verify',
        RESEND_EMAIL: ':BASE_URL:/account/resend-activation-mail',
        OLD_DASHBOARD_LOGIN: ':BASE_URL:/account/login',
        CANCEL_ERROR_STATE_ORDER: ':BASE_URL:/account/companies/:COMPANY_ID:/orders/:ORDER_ID:/cancel?token=:authToken:',
        ORDER_CREATE: ':BASE_URL:/account/companies/:COMPANY_ID:/orders?token=:token:',
    },

    RESETPIN_API_URLS: {
        RESET_PIN: ':BASE_URL:/tenants/:ifi:/cards/:superCardID:/setPin',
    },

    SODEXO_VN_URLS: {
        CREATE_ORDER: ':SODEXO_VN_BASE_URL:/orders/createOrder?token=:token:&corpID=:corpID:',
        GET_ORDER_FILE: ':SODEXO_VN_BASE_URL:/orders/downloadOrderFile',
        GET_AGGREGATIONS: ':SODEXO_VN_BASE_URL:/cardprograms/getAggregations?token=:authToken:&corpID=:corpID:&companyID=:companyID:&programID=:programID:&aggregationType=:aggregationType:',
        UPDATE_SPEND_SETTING: ':SODEXO_VN_BASE_URL:/cardprograms/updateTimeSliceTxnPolicy',
        GET_SPEND_SETTINGS: ':SODEXO_VN_BASE_URL:/cardprograms/getTimeSliceTxnPolicy?cardProgramID=:cardProgramID:'
    },

    getSodexoVnUrl: function() {
        for (var prop in this.SODEXO_VN_URLS) {
            this.SODEXO_VN_URLS[prop] = this.SODEXO_VN_URLS[prop]
                .replace(':SODEXO_VN_BASE_URL:', this.SODEXO_VN_BASE_URL[Env.CURR_ENV])
        }
        return this.SODEXO_VN_URLS;
    },

    getOMSApiUrl: function() {
        for (var prop in this.OMS_API_URL) {
            this.OMS_API_URL[prop] = this.OMS_API_URL[prop].replace(':OMS_BASE_URL:', this.OMS_BASE_URL[Env.CURR_ENV]);
        }
        return this.OMS_API_URL;
    },

    getAvailFinanceApiUrl: function() {
        for (var prop in this.AVAIL_FINANCE_API_URL) {
            this.AVAIL_FINANCE_API_URL[prop] = this.AVAIL_FINANCE_API_URL[prop].replace(':AVAIL_FINANCE_BASE_URL:', this.AVAIL_FINANCE_BASE_URL[Env.CURR_ENV]);
        }
        return this.AVAIL_FINANCE_API_URL;
    },

    getCorpApiUrl: function() {
        for (var prop in this.CORP_API_URLS) {
            this.CORP_API_URLS[prop] = this.CORP_API_URLS[prop].replace(':BASE_URL:', this.CORP_BASE_URL[Env.CURR_ENV]);
        }
        return this.CORP_API_URLS;
    },

    getApiUrl: function() {
        for (var prop in this.API_URLS) {
            this.API_URLS[prop] = this.API_URLS[prop].replace(':BASE_URL:', this.BASE_URL[Env.CURR_ENV]);
        }
        return this.API_URLS;
    },

    getLoginUrl: function() {
        // return this.LOGIN_BASE_URL[Env.CURR_ENV];
        return this.SSO_LOGIN_BASE_URL[Env.CURR_ENV];
    },

    getLogoutBaseurl: function() {
        return this.SSO_LOGOUT_BASE_URL[Env.CURR_ENV];
    },

    getLoggingUrl: function() {
        return this.LOGGING_BASE_URL[Env.CURR_ENV];
    },

    getOldLoginUrl: function() {
        return this.OLD_LOGIN_URLS[Env.CURR_ENV];
    },

    getresetPinUrl: function() {
        for (var prop in this.RESETPIN_API_URLS) {
            this.RESETPIN_API_URLS[prop] = this.RESETPIN_API_URLS[prop].replace(':BASE_URL:', this.RESET_PIN_BASE_URL[Env.CURR_ENV]);
        }
        return this.RESETPIN_API_URLS;
    },
}
export default {
    ORDER_STATUS_PROCESSING: 'processing',
    ORDER_STATUS_PROCESSING_FAILED: 'processing_failed',
    ORDER_STATUS_INITIATED: 'initiated',
    ORDER_STATUS_SCHEDULED: 'scheduled',
    ORDER_STATUS_COMPLETED: 'completed',
    ORDER_STATUS_CANCELLED: 'cancelled',
    ORDER_STATUS_STALLED: 'stalled',

    ORDER_STATUS_LABELS: {
        processing: "Processing",
        processing_failed: "Processing failed",
        initiated: "Initiated",
        scheduled: "Scheduled",
        completed: "Completed",
        cancelled: "Cancelled",
        stalled: "Stalled"
    },

    PROGRAM_TYPES: {
        meal: 'Meal Vouchers',
        gift: 'Gift Cards',
        medical: 'Medical Reimbursements',
        fuel: 'Fuel Cards',
        asset: 'Asset Cards',
        communication: 'Communication Reimbursements',
        cashless_cafeteria: 'Cashless Cafeteria',

        reward: 'Rewards & Recognition',
        incentive: 'Cash Deposits',
        partner_gifting: 'Partner Gifting',
    },


    PAYOUT_STATUS_SCHEDULED: 'scheduled',
    PAYOUT_STATUS_INITIATED: 'initiated',
    PAYOUT_STATUS_SUCCESS: 'success',
    PAYOUT_STATUS_INVALID: 'invalid',
    PAYOUT_STATUS_FAILED: 'failed',
    PAYOUT_STATUS_CANCELLED: 'cancelled',
    PAYOUT_STATUS_REVOKED: 'revoked',
    PAYOUT_STATUS_PARTIALLY_REVOKED: 'partially_revoked',
    PAYOUT_STATUS_FUND_LOCKED: 'fund_locked',
    PAYOUT_STATUS_TEMPORARILY_FAILED: 'temporarily_failed',
    PAYOUT_STATUS_ISSUED: 'issued',
    PAYOUT_STATUS_CLAIMED: 'claimed',

    PAYOUT_STATUS_LABELS: {
        'scheduled': "Scheduled",
        'initiated': "Initiated",
        'invalid': "Invalid",
        'failed': "Failed",
        'cancelled': "Cancelled",
        'revoked': "Revoked",
        'partially_revoked': "Partially Revoked",
        'fund_locked': "Funding Account Debited",
        'temporarily_failed': "Account Not Created",
        'issued': "Issued",
        'claimed': "Successful",
    },
}

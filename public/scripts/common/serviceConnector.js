const escapeHtmlInJson = require('escape-html-in-json');
require('./../javascript');

export
default {
  ajaxConfig: {
    contentType: 'application/json',
    crossDomain: true
  },

  post: function (input, url, header) {
    if (input != null) {
      this.ajaxConfig.data = JSON.stringify(input);
    }
    return this.serviceRequest('POST', url, header);
  },

  postWithCORS: function (input, url, header) {
    return $.ajax({
      type: "POST",
      url: url,
      data: input,
      processData: false,
      contentType: false,
      crossDomain: true
    });
  },

  put: function (input, url, header) {
    this.ajaxConfig.data = JSON.stringify(input);
    return this.serviceRequest('PUT', url, header);
  },

  get: function (url, header = {}) {
    this.ajaxConfig.data = null;
    const urlParts = url.split("?");
    let token;
    if (urlParts[1]) {
      const params = urlParts[1].split("&");
      const filteredParams = params.filter(t => {
        const qpParts = t.split("=");
        if (qpParts[0] === 'token') {
          token = qpParts[1];
          return false;
        }
        return true;
      });
      urlParts[1] = filteredParams.join("&");
    }
    return this.serviceRequest('GET', urlParts.join("?"), Object.assign({},
      header,
      token ? { 'X-Zeta-AuthToken': token } : {}
    ));
  },

  serviceRequest: function (httpMethod, httpURL, headers) {
    this.ajaxConfig.url = httpURL;
    this.ajaxConfig.type = httpMethod;
    this.ajaxConfig.headers = headers;

    // for(var key in headers){
    //     this.ajaxConfig[key] = headers[key];
    // }

    return $.ajax(this.ajaxConfig)
      .then(function (response) {
        return JSON.parse(JSON.stringify(response, escapeHtmlInJson));
      });
  }
}

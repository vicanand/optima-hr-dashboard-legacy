(function() {
  $(function() {
    return $('.ellipsis').mouseenter(function() {
      var innerW, offset, spanW;
      innerW = $('.inner', this).width();
      spanW = $('span', this).width();
      if (spanW > innerW + 1) {
        offset = spanW - innerW;
        return $('.inner', this).css('text-indent', -offset);
      }
    }).mouseleave(function() {
      return $('.inner', this).removeAttr('style');
    });
  });
}).call(this);

import Constant from '../common/constants';
import DomEventHandler from '../common/domEventHandler';
import NewTransferDomEvent from '../dom-events/newTransferDOMEvent';
import Storage from '../common/webStorage';
import moment from '../../lib/moment/moment';

let dropFlag = 0;

let FREQENCY_MOMENT_KEY_MAPPING = {
    "WEEKLY": 'weeks',
    "MONTHLY": 'months',
    "FORT_NIGHTLY": 'weeks',
    "QUARTERLY": "quarters",
    "YEARLY": "years"
};

let FREQENCY_DISPLAY_TEXT_MAPPING = {
    "WEEKLY": 'Weekly',
    "MONTHLY": 'Monthly',
    "FORT_NIGHTLY": 'Fortnightly',
    "QUARTERLY": "Quarterly",
    "YEARLY": "Yearly"
};

let countryName = $('#countryName').val();

let acceptedFiles = ".csv,.xls,.xlsx"

if(countryName == 'Sodexo_Philippines'){
    acceptedFiles = ".csv"
}

let fileInit = function (uploadDetails, uploadBtn) {
    Storage.set(Constant.STORAGE_FILE_URL, '');
    var uploadDetails = $('#uploadDetails').val();
    uploadDetails = JSON.parse(uploadDetails);
    $.Dropzone.options.dragDrop = {
        addRemoveLinks: true,
        paramName: "file",
        previewsContainer: null,
        // clickable: true,
        acceptedFiles: acceptedFiles,
        // error: true,
        dictDefaultMessage: '',
        uploadMultiple: false,
        maxFiles: 1,
        previewTemplate: document.getElementById('preview-template').innerHTML,
        url: uploadDetails.postAction,
        headers: {
            "Cache-Control": "",
            "X-Requested-With": "",
            "X-File-Name": ""
        },
        accept: function(file, done) {
            dropFlag = 1;
            console.log('uploaded');
            done();
        },
        init: function() {
            var uploadBtn = $('#dragDrop .browse-wrap');
            uploadBtn.removeClass('btn-disabled');
            this.on('addedfile', function(file) {
                console.log(file);
                if (this.files[1] != null) {
                    this.removeFile(this.files[0]);
                }
            });
            this.on("uploadprogress", function(file, progress) {
                console.log("File progress", progress);
                console.log('hey this is new transfer');
            });
            this.on('canceled', resetForm);
            this.on('removedfile', resetForm);
            this.on('success', NewTransferDomEvent.validateFile);
            this.on('success', function(respData) {
                console.log('>>>>', respData);
                NewTransferDomEvent.validateFile
                let key = respData.xhr.response.split('<Key>')[1].split('</Key>')[0];
                let etag = respData.xhr.response.split('<ETag>')[1].split('</ETag>')[0].replace('"', '').replace('"', '');
                let fileUrl = respData.xhr.response.split('<Location>')[1].split('</Location>')[0] + '?key=' + key + '&etag=' + etag;
                Storage.set(Constant.STORAGE_FILE_URL, fileUrl);

            });
            this.on('error', function(file, message) {
                var errorEle = $('.dz-error-message');
                errorEle.find('span')
                    .text(Constant.FILE_UPLOAD_ERROR);
                errorEle.show();
                //  alert(message);
                // this.removeFile(file);
            });
        },
        sending: function(file, xhr, formData) {
            console.log(file.type);

            // dropzone chrome xls type issue
            if (!file.type) {
                var ext = file.name.split('.').pop();
                if (ext === 'xls') {
                Object.defineProperty(file, 'type', { value: 'application/vnd.ms-excel' }); // MIME type
             }
            }

            switch (file.type) {
                case "text/csv":
                    $(".fileType").attr("src", "/images/group-12.png");
                    break;
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    $(".fileType").attr("src", "/images/group-xlsx.png");
                    break;
                case "application/vnd.ms-excel":
                    $(".fileType").attr("src", "/images/group-xls.png");
                    break;
            }

            Storage.set(Constant.STORAGE_FILE_NAME, file.name);

            formData.append("key", uploadDetails.key);
            formData.append("acl", uploadDetails.acl);
            formData.append("x-amz-credential", uploadDetails["xAmzCredential"]);
            formData.append("x-amz-algorithm", uploadDetails["xAmzAlgorithm"]);
            formData.append("success_action_status", uploadDetails.successActionStatus);
            formData.append("x-amz-date", uploadDetails["xAmzDate"]);
            formData.append("policy", uploadDetails.policy);
            formData.append("x-amz-signature", uploadDetails["xAmzSignature"]);
            formData.append('content-type', file.type);
        }
    };
}

let formInit = function () {
    $("input[type=text], textarea").val("");
    $("select option:first-child").attr("selected", "selected");
};

let init = function () {
    let uploadDetails = $('#uploadDetails').val();
    uploadDetails = JSON.parse(uploadDetails);
    var uploadBtn = $('#dragDrop .browse-wrap');
    fileInit(uploadDetails, uploadBtn)
    formInit()


    DomEventHandler.bindEvent('add-payout-order', 'click', NewTransferDomEvent.payoutInitiateTransfer);
    DomEventHandler.bindEvent('create-order', 'click', NewTransferDomEvent.initiateTransfer);
    DomEventHandler.bindEvent('retryBtn', 'click', NewTransferDomEvent.initiateTransfer);
    DomEventHandler.bindEvent('fileErrorContent', 'click', '.orderOptions',  NewTransferDomEvent.switchAction)
    DomEventHandler.bindEvent('fileErrorContent', 'click', '#continueTransfer', NewTransferDomEvent.continueTransfer)
    DomEventHandler.bindClassEvent('validation-process-button', 'click', NewTransferDomEvent.continueTransfer)
    DomEventHandler.bindEvent('fileErrorContent', 'click', '#download_error_report', NewTransferDomEvent.downloadErrorReport)

    $('#scheduleTransferDate').datepicker(getBasicDatePickerConfig())
        .change(NewTransferDomEvent.validationForTransfer)
        .on('changeDate', NewTransferDomEvent.validationForTransfer);

    $('#repeatUntilDate').datepicker(getBasicDatePickerConfig()).on('changeDate', handleStartDateFrequencyRepeatDateChanges);
    $('#payrollCycleStartDate').datepicker(getBasicDatePickerConfig()).on('changeDate', handleStartDateFrequencyRepeatDateChanges);
    $('#payrollCycleEndDate').datepicker(getBasicDatePickerConfig());
};



let getBasicDatePickerConfig = function () {

    let dateRange = [],
        startDate, endDate, blockdates = [];
    try {
        blockdates = JSON.parse($('#blockDates').val());
    } catch (error) {
        console.log(error);
        blockdates = [];
    }

    for (let i = 0; i < blockdates.length; i++) {
        let start = blockdates[i]['periodStartDDMM'].split('-').reverse().join('-');
        let end = blockdates[i]['periodEndDDMM'].split('-').reverse().join('-')

        startDate = new Date(start);
        endDate = new Date(end);
        if (startDate == 'Invalid Date' || endDate == 'Invalid Date') {
            continue;
        }

        for (var d = new Date(startDate); d <= new Date(endDate); d.setDate(d.getDate() + 1)) {
            dateRange.push($.datepicker.formatDate('dd-mm', d));
        }
    }

    var disableDates = function(dt) {
        var dateString = jQuery.datepicker.formatDate('dd-mm', dt);
        return (dateRange.indexOf(dateString) == -1);
    };

    $('#scheduleTransferDate').datepicker({
        format: "yyyy-mm-dd",
        startDate: new Date(),
        autoclose: true,
        beforeShowDay: disableDates
    })
        .change(NewTransferDomEvent.validationForTransfer)
        .on('changeDate', NewTransferDomEvent.validationForTransfer);

    return {
        format: "yyyy-mm-dd",
        startDate: new Date(),
        autoclose: true,
        beforeShowDay: disableDates
    }
}

let resetForm = function () {
    dropFlag = 0;
    $('#create-order, #add-payout-order')
        .attr('disabled', 'disabled')
        .addClass('btn-disabled');

    $('#scheduleTransferDate,#purchaseOrderNo').val('');
};

let setFlag = function() {
    return dropFlag;
};



// CODE ADDED BY PARYUL JAIN WITH REF. TO TASK B)REPEAT ORDERS
// START


$('#repeatAction').on('change', function (e) {
    let isRecurring = (this.value == 'recurring');
    setVisibilityBasedOnTransferOrderType(isRecurring);
    // validationForRepeatAction(isRecurring);

});

let setVisibilityBasedOnTransferOrderType = function (isVisbile) {
    if (isVisbile) {
        // $('#payrollSpan').text('*');
        $('#poOrderNumber').hide();
        $('#repeatDate, #frequencyType, #repeatFreqText, #payrollSpan').show();
        // $('#repeatFreqText').text('(' + 'Repeats' + " " + 'Weekly' + ')');

        // $('#payrollCycleEndDate').attr('disabled', 'disabled');
        $('#labelPayrollCycle').text('First Payment is for the month of');
        $('#frequencyTypeOptions').val("MONTHLY");
        $('#frequencyTypeOptions').attr('disabled', 'disabled');

        $('#scheduledTransferText').text('Transfer date for the first order');


    } else {
        $('#poOrderNumber').show();
        $('#repeatDate, #frequencyType, #repeatFreqText, #payrollSpan, #orderScheduledText').hide();
        // $('#payrollCycleEndDate').removeAttr('disabled');
        $("select option:first-child").attr("selected", "selected");
        $('#labelPayrollCycle').text('Order for the month of');
        $('#frequencyTypeOptions').removeAttr('disabled');
        $('#scheduledTransferText').text('Transfer date');
    }
}


$('#frequencyTypeOptions').on("change", function () {
    let currentFreq = $('#frequencyTypeOptions').val();
    $('#repeatFreqText').text('(' + 'Repeats' + " " + FREQENCY_DISPLAY_TEXT_MAPPING[currentFreq] + ')');
    $('#repeatFreqText').removeClass("hideRepeatText");

    handleStartDateFrequencyRepeatDateChanges();
});

let handleStartDateFrequencyRepeatDateChanges = function () {
    let currentStartDate = $('#payrollCycleStartDate').val();
    let currentFreq = $('#frequencyTypeOptions').val();
    let repeatUntilDate = $('#repeatUntilDate').val();
    if (!currentStartDate) {
        return;
    }
    updateEndDateFromStartDateAndFrequency(currentStartDate, currentFreq);
    updateNumberOfOrdersBasedOnRepeatUntil(currentStartDate, currentFreq, repeatUntilDate);
}

let updateEndDateFromStartDateAndFrequency = function (startDate, frequency) {
    let updatedEndDate = calculatePayrollEndDateFromStartDate(startDate, frequency);
    $('#payrollCycleEndDate').val(updatedEndDate.format('YYYY-MM-DD'));
}

let updateNumberOfOrdersBasedOnRepeatUntil = function (startDate, frequency, repeatUntilDate) {
    if (frequency && repeatUntilDate) {
        let numberOfOrders = calculateNumberOfOrders(startDate, repeatUntilDate, frequency);
        if (numberOfOrders >= 0) {
            $('#orderScheduledText').show();
            $('#numberOfOrdersScheduled').text(numberOfOrders);
        }
        else {
            alert('Repeat Until Date should be selected later than Payroll Cycle Start Date');
        }
    }
}

let calculatePayrollEndDateFromStartDate = function (startDate, frequencySelected) {
    startDate = moment(startDate);
    let endDate = startDate.clone();
    if (FREQENCY_MOMENT_KEY_MAPPING[frequencySelected]) {
        endDate = moment(startDate).add(1, FREQENCY_MOMENT_KEY_MAPPING[frequencySelected]);
        if (frequencySelected == 'FORT_NIGHTLY') {
            endDate = moment(startDate).add(2, 'weeks');
        }
    }
    return endDate;
};

let calculateNumberOfOrders = function (startDate, repeatUntilDate, frequencySelected) {
    startDate = moment(startDate);
    repeatUntilDate = moment(repeatUntilDate);
    if (startDate < repeatUntilDate) {
        let daysDifference = getDateDifferenceInDays(repeatUntilDate, startDate);
        let frequencyMap = { "WEEKLY": 7, "MONTHLY": 30, "YEARLY": 365, "QUARTERLY": 90, "FORT_NIGHTLY": 14 };
        let divisionFactor = frequencyMap[frequencySelected] || 7;
        let ordersScheduled = daysDifference / divisionFactor;
        return Math.floor(ordersScheduled) + 1;
    }
    else {
        return 0;
    }
}

// Moved to Utils
let getDateDifferenceInDays = function (fromDate, toDate) {
    return moment(fromDate).diff(moment(toDate), 'days');
}

//END

export default {
    setFlag: setFlag,
    init: init,
    calculateNumberOfOrders: calculateNumberOfOrders
}
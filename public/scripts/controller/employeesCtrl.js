import DomEvents from "../common/domEventHandler";
import TransferDetailsDomEvents from "../dom-events/transferDetailsDomEvents";
import EmployeeBenefitsDomEvents from "../dom-events/employee-benefits-dom-events";
import Util from '../common/util';

export
default {
    init: function() {
        DomEvents.bindClassEvent("employee-report-table", "click", ".payoutOutCancelBtn", TransferDetailsDomEvents.confirmCancelPayout);
        DomEvents.bindClassEvent("employee-report-table", "click", ".payoutOutRevokeBtn", TransferDetailsDomEvents.confirmRevokePayout);
        DomEvents.bindEvent("cancelPayoutConfirm", "click", ".reset", TransferDetailsDomEvents.resetCancelPayout);
        DomEvents.bindEvent("cancelPayoutConfirm", "click", ".confirm", TransferDetailsDomEvents.cancelPayout);
        DomEvents.bindEvent("revokePayoutConfirm", "click", ".reset", TransferDetailsDomEvents.resetRevokePayout);
        DomEvents.bindEvent("revokePayoutConfirm", "click", ".confirm", TransferDetailsDomEvents.revokePayout);
        DomEvents.bindClassEvent("employee-program-details", "click", ".hide-reimbursement", EmployeeBenefitsDomEvents.toggleReimbursement); // program
        DomEvents.bindClassEvent("programTab", "click", EmployeeBenefitsDomEvents.cardProgramIndividualBeneficiary);
        DomEvents.bindEvent("downloadStatement", "click", EmployeeBenefitsDomEvents.downloadStatement);
        DomEvents.bindClassEvent("statement-btn", "change", EmployeeBenefitsDomEvents.downloadStatementBtn);
        DomEvents.bindClassEvent("employee-report-table", "click", ".view-history", TransferDetailsDomEvents.viewOrderHistoryList);
        DomEvents.bindEvent("get-employees-widget", "click", ".employee-toggle-switch",EmployeeBenefitsDomEvents.toggleEmployeeState);
    }
}
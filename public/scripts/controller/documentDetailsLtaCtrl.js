import Util from '../common/util';
import DomEventHandler from '../common/domEventHandler';
import DocumentDomDetailsEvents from '../dom-events/documentDetailsLTADOM-Events';

export
default {
    init: function() {
        DomEventHandler.bindEvent("ltaType", "change", DocumentDomDetailsEvents.changeChecklist);
    }
}
/**
 * Created by SRV .
 */

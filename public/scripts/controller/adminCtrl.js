import DomEventHandler from '../common/domEventHandler';
import adminDOMEvents from '../dom-events/adminDOMEvents';

export
default {
    init: function() {
        DomEventHandler.bindEvent('createNewProgram', 'click', adminDOMEvents.newProgram);
        DomEventHandler.bindEvent('saveCompanyCodes', 'click', adminDOMEvents.addCompanyCode);
        DomEventHandler.bindClassEvent('companyCode', 'keyup', adminDOMEvents.validateInputBtn);
        DomEventHandler.bindClassEvent('AddCompany_code', 'click', adminDOMEvents.openCompanyCode);
        DomEventHandler.bindClassEvent('buttonbtn', 'click', adminDOMEvents.successAddedCode);
        DomEventHandler.bindEvent('manage_companies', 'click', adminDOMEvents.redirect);
        DomEventHandler.bindEvent('access_control', 'click', adminDOMEvents.redirect);
        DomEventHandler.bindEvent('createBulkOrder', 'click', adminDOMEvents.createBulkTransferOrder);
        DomEventHandler.bindEvent('bulk_close_card_request', 'click', adminDOMEvents.bulkCloseCardsRequest);
    }
}
import DomEventHandler from '../common/domEventHandler.js';
import FundHistoryDomEvents from '../dom-events/fundHistoryDomEvents';
import TableWidget from '../widgets/table-widget';
import FilterWidget from '../widgets/filter-widget';
import FundsService from '../service/fundsSvc';
import daterangepicker from '../../../node_modules/bootstrap-daterangepicker/daterangepicker';
import moment from '../../lib/moment/moment';

export default {
    init: function() {
        TableWidget.init();
        FilterWidget.init();
        this.daterangeInit();
        $("#currPage").val("Accounts");

        this.getFundAdditionsHistory();

        let _this = this;
        $('#fundsTab').click(function(e) {
            console.log("Called");
            $("#currPage").val("Accounts");
            if ($("#fundingAcc").data('created')===true) {
                $("#fundingAcc").tablewidget("destroy").attr('data-created','false');
            }
            _this.getFundAdditionsHistory();
        });


        $('#accTab').click(function(e) {
            $("#currPage").val("Additions");
            if ($("#accountStatement").data('created')===true) {
                $("#accountStatement").tablewidget("destroy").attr('data-created','false');
            }
            _this.getAccStatementHistory();
        });

        DomEventHandler.bindEvent("fundsList", "click", "a", FundHistoryDomEvents.fundAccSelect);  
        DomEventHandler.bindClassEvent("phDwnldReport", "click", FundHistoryDomEvents.downloadReport);
    },
    daterangeInit:function(){
        var start = moment().startOf('day')
        var end = moment().endOf('day');
        
        function cb(start, end) {
            // $('input[name="dateranges"]').val('');
        }
        $('input[name="dateranges"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        $('input[name="dateranges"]').daterangepicker({
            "autoApply": true,
            locale: {
                format: 'YYYY-MM-DD',
                "customRangeLabel": "Choose From Calendar",
            },
            "opens": "left",
            "alwaysShowCalendars": true,
            maxDate: new Date(),
            startDate: start,
            endDate: end,
            "alwaysShowCalendars": true,
            ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(0, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }     
        }, cb);
        cb(start, end); 
    },
    changeAccountId: function() {
        if ($("#currPage").val() == "Accounts") {
            if ($("#fundingAcc").data('created')===true) {
                $("#fundingAcc").tablewidget("destroy").attr('data-created','false');
            }
            this.getFundAdditionsHistory();
        } else {
            
            if ($("#accountStatement").data('created')===true) {
                $("#accountStatement").tablewidget("destroy").attr('data-created','false');
            }
            this.getAccStatementHistory();
        }
    },
    getFundAdditionsHistory: function() {
        $("#fundingAcc").tablewidget({
            source: FundsService.getEachFundHistory,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: "/template/tables/funds-history.ejs",
            rowTemplate: "/template/tables/funds-history-row.ejs",
            extraParam: {
                accountId: $("#accountId").val()
            },
            startDate: $("#tranferPeriodFund")
        }).attr('data-created','true');
    },
    getAccStatementHistory: function() {
        $("#accountStatement").tablewidget({
            source: FundsService.getAccountStatementV2,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: "/template/tables/account-statement.ejs",
            rowTemplate: "/template/tables/account-statement-row.ejs",
            extraParam: {
                accountId: $("#accountId").val(),
            },
            startDate: $("#tranferPeriodAccount")
        }).attr('data-created','true');
    }
}

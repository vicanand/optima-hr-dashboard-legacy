import TransferDetailsDomEvents from "../dom-events/transferDetailsDomEvents";
import DomEvents from "../common/domEventHandler";
import OrderSvc from '../service/ordersService';
import Util from '../common/util';

export
default {
    init: function() {
        let currencyType = $('#currencyType').val();
        let transferDetailPayouts;
        if(currencyType == 'INR'){
            transferDetailPayouts = "/template/tables/transfer-detail-payouts.ejs";
        }
        else{
            transferDetailPayouts = "/template/tables/transfer-detail-payouts-token.ejs";
        }
        let revokeFunds = $('#payoutstable').data('revoke');
        let isProgramManagerRW = $('#payoutstable').data('pmrw');
        $("#payoutstable").tablewidget({
            source: OrderSvc.getPayouts,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: transferDetailPayouts,
            rowTemplate: "/template/tables/transfer-details-payout-row.ejs",
            extraParam: {
                startDate: Util.getUrlParameter("startDate"),
                endDate: Util.getUrlParameter("endDate"),
                statusFilters: '',
                orderId:window.location.pathname.split('/').pop(),
                compareDates: $('#compare-date').data('compare-date'),
                revokeFunds: revokeFunds,
                isProgramManagerRW: isProgramManagerRW
            },
            searchSelector: $("#employeeSearch"),
            searchButton: $("#searchEmployeeBtn"),
            filterSelect: $("#statusFilter"),
            dateSearch: $('#tranferPeriod'),
            payoutFilter: $('#payoutFilters')
        });

        $("#tranferPeriod").datepicker({
            format: "m-yyyy",
            viewMode: "months",
            minViewMode: "months",
            autoclose: true
        });

        DomEvents.bindEvent("payoutstable", "click", ".payoutOutCancelBtn", TransferDetailsDomEvents.confirmCancelPayout);
        DomEvents.bindEvent("payoutstable", "click", ".payoutOutRevokeBtn", TransferDetailsDomEvents.confirmRevokePayout);
        DomEvents.bindEvent("cancelPayoutConfirm", "click", ".reset", TransferDetailsDomEvents.resetCancelPayout);
        DomEvents.bindEvent("cancelPayoutConfirm", "click", ".confirm", TransferDetailsDomEvents.cancelPayout);
        DomEvents.bindEvent("revokePayoutConfirm", "click", ".reset", TransferDetailsDomEvents.resetRevokePayout);
        DomEvents.bindEvent("revokePayoutConfirm", "click", ".confirm", TransferDetailsDomEvents.revokePayout);
        DomEvents.bindEvent("payoutstable", "click", ".select-chkbox", TransferDetailsDomEvents.rowSelect);
        DomEvents.bindEvent("payoutstable", "click", ".selectall", TransferDetailsDomEvents.selectAllChkbox);
        DomEvents.bindEvent("bulk-action-section", "click", ".action-revoke", TransferDetailsDomEvents.confirmRevokePayout);
        DomEvents.bindEvent("bulk-action-section", "click", ".clear", TransferDetailsDomEvents.cancelBulkRevokePayout);
        DomEvents.bindEvent("payoutstable", "click", ".view-details", TransferDetailsDomEvents.viewOrderHistoryList);
    },

    cancelPayout: function(orderId, payoutId) {
        return OrderSvc.cancelPayout(orderId, payoutId);
    },

    revokePayout: function(orderId, payoutId) {
        return OrderSvc.revokePayout(orderId, payoutId);
    }
}

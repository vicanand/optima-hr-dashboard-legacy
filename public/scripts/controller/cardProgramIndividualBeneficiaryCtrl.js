import DomEventHandler from '../common/domEventHandler.js';
import BeneficiaryDOMEvents from '../dom-events/cardProgramIndividualBeneficiaryDomEvent.js';


export
default {
    init: function() {
        this.registerDOMEvents();
    },
    registerDOMEvents: function() {
      //  DomEventHandler.bindClassEventAlt('click', BeneficiaryDOMEvents.showHideCardTable);
        DomEventHandler.bindTable('benifitsEmployees','cardProgramBeneficiaryTable', 'click', BeneficiaryDOMEvents.cardProgramIndividualBeneficiary); // binding table tr click event
        DomEventHandler.bindEvent('schedule-table-id', 'click', BeneficiaryDOMEvents.showHideScheduledTable);    // bindEvent for id 
        DomEventHandler.bindEvent('waiting-table-id', 'click', BeneficiaryDOMEvents.showHideWaitingTable);    // canceled-table-id
        DomEventHandler.bindEvent('canceled-table-id', 'click', BeneficiaryDOMEvents.showHideCanceledTable);
        DomEventHandler.bindClassEvent('cardDetailsLink', 'click', BeneficiaryDOMEvents.showHideCardTable);
        DomEventHandler.bindClassEvent('reset', 'click', BeneficiaryDOMEvents.showRevokeModal);
        DomEventHandler.bindClassEvent('confirm', 'click', BeneficiaryDOMEvents.revokePayout);
        DomEventHandler.bindEvent('custom-modal-close', 'click', BeneficiaryDOMEvents.closeModal);
         
    }
}
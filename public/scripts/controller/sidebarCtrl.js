import DomEventHandler from '../common/domEventHandler';
import SidebarDOMEvents from '../dom-events/sidebarDOMEvents';
import slimScroll from '../../../node_modules/jquery-slimscroll/jquery.slimscroll';
function resizeSidebar() {
    if (matchMedia) {
        var media = window.matchMedia("(min-width:1170px)").matches;
        if (!media) {
            $('.sidebar-container').addClass('sidebar-toggle');
            $('.inner-container').addClass('main-cntr-toggle');
            $('.header-container').addClass('header-cntr-toggle');
            $('.nav-company-toggle').addClass('nav-comp-toggle');
            $('.heading-cntr-toggle').addClass('heading-txt-toggle');
            $('.global-heading-toggle').addClass('global-heading-txt-toggle');
            $('.new-program-toggle').addClass('new-pgm-toggle');    
            $('li.corpTools > span').addClass('textToggle');
            $('.companyHide').addClass('hideThisEl');
            $('.companyShow').removeClass('hideThisEl');
            if($('.nav-btn-cntr').hasClass('active')){
                SidebarDOMEvents.toggleSidebar();
            }
            SidebarDOMEvents.openProgDropDown();
            $('.sideMenuNav .tooltiptext').show();
            $('.sideBar_subcon').hide();
            $('.arrowBounce').hide();
            
        } else {
            $('.sidebar-container').removeClass('sidebar-toggle');
            $('.inner-container').removeClass('main-cntr-toggle');
            $('.header-container').removeClass('header-cntr-toggle');
            $('.nav-company-toggle').removeClass('nav-comp-toggle');
            $('.heading-cntr-toggle').removeClass('heading-txt-toggle');
            $('.global-heading-toggle').removeClass('global-heading-txt-toggle');
            $('.new-program-toggle').removeClass('new-pgm-toggle');
            $('li.corpTools > span').removeClass('textToggle');
            $('.companyHide').removeClass('hideThisEl');
            $('.companyShow').addClass('hideThisEl');
            $('.sideBar_subcon').slideUp(100); 
            $('span.arrowBounce').hide();
            $('.sideMenuNav .tooltiptext').hide();
        } 
    } 
} 

resizeSidebar();

export
default {

    init: function () {
        DomEventHandler.bindClassEvent('sidebar-company-item', 'click', SidebarDOMEvents.compnayClick); 
        DomEventHandler.bindClassEvent('nav-btn-cntr', 'click', SidebarDOMEvents.toggleSidebar); 
        DomEventHandler.bindEvent('spotlight-sidebar', 'click', SidebarDOMEvents.spotlightAnalytics); 
        DomEventHandler.bindClassEvent('openProgList', 'click', SidebarDOMEvents.openProgDropDown)
        DomEventHandler.bind(window, 'resize', function () {
            resizeSidebar();
        }); 
        // DomEventHandler.bindClassEvent('subListItem', 'mouseover', SidebarDOMEvents.checkHoverState);
        // DomEventHandler.bindClassEvent('sideBar_subcon', 'mouseleave', SidebarDOMEvents.removeHoverState);
        var mySideBar = document.querySelector('#sidebar-cntr-overflow') ? document.querySelector('#sidebar-cntr-overflow').getElementsByTagName('li') : [];
        for(let i=0; i< mySideBar.length; i++ ){
            mySideBar[i].addEventListener('mouseover', SidebarDOMEvents.toolTipPos); 
        }
        DomEventHandler.bindClassEvent('sidebar-container', 'mouseenter', SidebarDOMEvents.disableBodyScroll); 
        DomEventHandler.bindClassEvent('sidebar-container', 'mouseleave', SidebarDOMEvents.EnableBodyScroll);
        var media = window.matchMedia("(max-width:1170px)").matches;
        if (!media) {
            if($('.sideMenuNav.cur-page').attr('href') == 'javascript:void(0)'){
                $('.sideMenuNav.cur-page').children('.openProgList').trigger('click');  
            }
        } 
    }
}
 
window.onload = function(){
    $('.sidebar-container').removeClass('sidebar-container1'); 
    let setSelectedCompanyWidth = $('.sidebar-dropdown').width();
    let setSelectedCompanyWidth1 = $('.dropdown_selectedCompName').width();
    if(setSelectedCompanyWidth >setSelectedCompanyWidth1){ 
        $('.dropdown_selectedCompName').width(setSelectedCompanyWidth);
    }
    $('.companyShow').show();

    $(function(){
        $('#inner-content-div').slimScroll({
            color: 'rgba(255,255,255,.3)',
            size: '5px',
            height: '100vh',
            alwaysVisible: false,
            width:'',
            wheelStep: 5
        });
    });
    $(function(){
        $('#inner-content-div1').slimScroll({
            color: '#000',
            size: '5px',
            height: '279px',
            alwaysVisible: true,
            width:''
        });
    });


} 


import DomEventHandler from '../../common/domEventHandler';
import CityDomEvents from '../../dom-events/feedback/city-select';
import OfficeDomEvents from '../../dom-events/feedback/office-select';
import StoreDomEvents from '../../dom-events/feedback/store-select';
import CommentDomEvents from '../../dom-events/feedback/customer-comment';
import CustomerComments from '../../../template/section/feedback/comment-data-load-feedback.ejs';
import DropdownService from '../../service/feedback/dropDownSvc';
import FeedbackService from '../../service/feedback/feedbackSvc';
import FeedbackModal from './feedbackModal';
import Storage from '../../common/webStorage';
import CommentLoader from '../../../template/section/feedback/scroll-loader.ejs';
var processing = false;
export
default {
    cityinit: function() {
        $(window).click(function(){
            $('.city-triangle-container').css('display','none');       
            $('.city-list').css('display','none');
         });
         DomEventHandler.bindClassEvent('city-dropdown','click',CityDomEvents.cityMenu);
         DomEventHandler.bindClassEvent('city-list','click',CityDomEvents.cityMenu);             
         DomEventHandler.bindClassEvent('city-multiselect-container','click',CityDomEvents.cityMenuSelect);                
    },
    officeinit:function(){
        $(window).click(function(){
            $('.office-triangle-container').css('display','none');
            $('.office-list').css('display','none');
         });
         DomEventHandler.bindClassEvent('office-dropdown','click',OfficeDomEvents.officeMenu);
         DomEventHandler.bindClassEvent('office-list','click',OfficeDomEvents.officeMenu);             
         DomEventHandler.bindClassEvent('office-multiselect-container','click',OfficeDomEvents.officeMenuSelect);                
    },
    storeinit:function(){
        $(window).click(function(){
            $('.store-triangle-container').css('display','none');
            $('.store-list').css('display','none');
         });
         DomEventHandler.bindClassEvent('store-dropdown','click',StoreDomEvents.storeMenu);
         DomEventHandler.bindClassEvent('store-list','click',StoreDomEvents.storeMenu);             
         DomEventHandler.bindClassEvent('store-multiselect-container','click',StoreDomEvents.storeMenuSelect);                
    },
    datepicker:function(){
        $('#datePicker').daterangepicker({
            "autoApply": true,
            // "dateLimit": {
            //     "days": 90
            // },       
            "startDate": moment().subtract(30, 'days'),
            "endDate": moment(),
            "maxDate": moment().subtract(1,'days'),
            "opens": "left",
            "locale": {
                "format": "DD MMM, YYYY",
            }
        }, (start, end, label) => {
            var fromDate = start.format('YYYY-MM-DD');
            var toDate = end.format('YYYY-MM-DD HH:mm:ss');
            var date = {};
            date.fromDate = fromDate;
            date.toDate = toDate;
            FeedbackModal.set_from_date(fromDate);
            FeedbackModal.set_to_date(toDate);
            DropdownService.date_render_all(date);
            
        });
    },
    bar_graph:function(){
        var ratings = JSON.parse(Storage.get('ratings'));
        ratings.ratingPercentages.shift();
        console.log('ratings.ratingPercentages: ', ratings.ratingPercentages);
        var blah =ratings.ratingPercentages.map(function(item){return{name:item.field.toString()+' star',y:parseInt(item.percentage)};});
        console.log('blah: ', blah);
        Highcharts.chart('rating-stats-container', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            
            xAxis: {
                type: 'category',
            },
            yAxis: {
                min:0,
                max:100,
                title: {
                    text: ''
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },
        
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },
             credits: {
            enabled: false
          },
            series: [{
                name: 'Ratings',
                data: ratings.ratingPercentages.map(function(item){ return{name:item.field.toString()+' star',y:parseFloat(item.percentage)};}), 
                color: '#633ea5'
            }]
            }
        );
    },
    customer_comment:function(){
        var five = 5;
        var scroll_flag = true;
        FeedbackModal.set_rating(five);
        FeedbackModal.set_scroll_flag(scroll_flag);
        FeedbackModal.set_processing(false);
        var defaultElement = $(".customer-rating[data-star='" + five +"']").css({"border-bottom":"2px solid #6e4db7","color":"#6e4db7"});
        

        DomEventHandler.bindClassEvent('customer-rating','click',CommentDomEvents.starSelected);
    },
    good_bits:function(){
        $('.good-review-number').change(function(){
            var percentage = $(this).html();
            var element = $(this).parent().next().find('.loader-outer .loader-inner');
            element.css({"width":percentage+"%"});
      });
      
      $('.good-review-number').change();
    },
    bad_bits:function(){
        $('.bad-review-number').change(function(){
            var percentage = $(this).html();
            var element = $(this).parent().next().find('.loader-outer .loader-inner');
            element.css({"width":percentage+"%"});
      });
      
      $('.bad-review-number').change();
    },
    formatTime:function(time){
        time.forEach(function(item){
            item.timeStamp = moment(item.timeStamp).format('YYYY-MM-DD,hh:mm:ss a');
           
        });
    },
    scroll:function(){
        var self = this;
        $(window).scroll(function(){

                var scrollTop = $(window).scrollTop();
                // console.log('scrollTop: ', scrollTop);
                var documentHeight=$(document).height();
                // console.log('documentHeight: ', documentHeight);
                var windowHeight=$(window).height();
                // console.log('windowHeight: ', windowHeight);
                var toDate = FeedbackModal.get_last_modified_date();
                toDate = moment(toDate).subtract(1, 'seconds').format('YYYY-MM-DD HH:mm:ss');
                var fromDate = FeedbackModal.get_from_date() || moment().subtract(30, 'days').format('YYYY-MM-DD');
                var rating = FeedbackModal.get_rating();
                var stores = FeedbackModal.get_stores();
                var pageSize = 10;
                var corporateID = $("meta[name='corpID']").attr("content");
                var authToken = Storage.get('authToken');
                let params = {
                    fromDate:fromDate,
                    toDate:toDate,
                    stores:stores,
                    pageSize:pageSize,
                    rating:rating,
                    corporateID:corporateID
                }
                if ($(window).scrollTop()>= ($(document).height() - $(window).height())*0.7){
                   
                       if(!FeedbackModal.get_processing() && FeedbackModal.get_scroll_flag()){
                        DomEventHandler.appendToMyTemplate('comment-details-container',CommentLoader);
                            FeedbackService.getCustomerComments(authToken,params).then(function(comments){
                                var comments = comments;
                                FeedbackModal.set_processing(false);
                                if(comments.comments.length > 0){
                                    var new_object = JSON.stringify(comments.comments);
                                    var new_comments = JSON.parse(new_object);
                                    console.log('new_comments: ', new_comments);
                                    var toDate = new_comments[new_comments.length - 1].timeStamp;
                                    console.log('toDate: ', toDate);
                                    $('.scroll-loader').remove();
                                    FeedbackModal.set_last_modified_date(toDate);
                                   
                                }else{
                                    FeedbackModal.set_scroll_flag(false);
                                    $('.scroll-loader').remove();
                                }
                                console.log('comments: ', comments);
                                self.formatTime(comments.comments);
                                DomEventHandler.appendToMyTemplate('comment-details-container',CustomerComments,comments);  
                            });
                       }
                       FeedbackModal.set_processing(true);
                }
        });
    },
    save_stores:function(){
        var storeList = $('.store-multiselect-values');
        var stores = [];
        storeList.each(function(index){
            var store = $(this).data('storeid');
            stores.push(store.toString());
        });
        stores = stores.join(',');
        console.log('Initial stores: ', stores);
        FeedbackModal.set_stores(stores);
    },
    initial_date_capture:function(){
        var string = JSON.parse(Storage.get('comments'));
        console.log('string: ', string);
        var time_filtered = string;
        var length = time_filtered.length;
        if(length > 0){
            var last_value = time_filtered[length - 1].timeStamp ;
        }else{
            var last_value = moment().format('YYYY-MM-DD');
        }
        
        FeedbackModal.set_last_modified_date(last_value);
    }
    

}
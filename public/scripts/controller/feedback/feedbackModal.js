let from_date,to_date,stores,last_modified,rating,scroll_call_api,processing;
let set_from_date = function(date){
    from_date = date;
    return true;
}

let set_to_date = function(date){
   to_date = date;
   return true;
}

let get_from_date = function(){
    return from_date;
}

let get_to_date = function(){
    return to_date;
}

let set_stores = function(store){
    stores = store;
    return true;
}

let get_stores = function(){
    return stores;
}

let set_last_modified_date = function(date){
    last_modified = date;
    return true;
}

let get_last_modified_date = function(){
    return last_modified;
}

let set_rating = function(star){
    rating = star;
    console.log("Modal rating",rating);
    return true;
}

let get_rating = function(){
    console.log("getrating",rating);
    return rating;
}

let set_scroll_flag = function(flag){
    scroll_call_api = flag;
    console.log('scroll_call_api: ', scroll_call_api);
    return true;
}

let get_scroll_flag = function(){
    console.log('scroll_call_api: ', scroll_call_api);
    return scroll_call_api;
}

let set_processing = function(process){
    processing = process;
    return true;
}

let get_processing = function(){
    return processing;
}
export default{
    set_from_date,
    set_to_date,
    get_from_date,
    get_to_date,
    set_stores,
    get_stores,
    set_last_modified_date,
    get_last_modified_date,
    set_rating,
    get_rating,
    set_scroll_flag,
    get_scroll_flag,
    set_processing,
    get_processing
}
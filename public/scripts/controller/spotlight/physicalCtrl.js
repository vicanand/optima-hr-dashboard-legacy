import Constant from '../../common/constants';
import DomEventHandler from '../../common/domEventHandler';
import FilterWidget from '../../widgets/filter-widget';
import PhysicalDomEvent from '../../dom-events/spotlight/physicalDOMEvents';
import VirtualDomEvent from '../../dom-events/spotlight/virtualDOMEvents';
import Storage from '../../common/webStorage';
import Table from '../../widgets/table';
import TableWidget from '../../widgets/table-widget';
import PhysicalSvc from '../../service/spotlight/physicalSvc';


Table.init();
TableWidget.init();
FilterWidget.init();

let dropFlag = 0;
 let _initDropZone = function () {
  
    
    var uploadDetails = $('#uploadDetails').val();
    uploadDetails = JSON.parse(uploadDetails);
    $.Dropzone.options.dragDrop = {
        addRemoveLinks: true,
        paramName: "file",
        previewsContainer: null,
        // clickable: true,
        acceptedFiles: '.csv,.xls,.xlsx',
        // error: true,
        dictDefaultMessage: '',
        uploadMultiple: false,
        maxFiles: 1,
        previewTemplate: document.getElementById('preview-template').innerHTML,
        url: uploadDetails.postAction,
        headers: {
            "Cache-Control": "",
            "X-Requested-With": "",
            "X-File-Name": ""
        },
        accept: function(file, done) {
            dropFlag = 1;
            console.log('uploaded');
            done();
        },
        init: function() {
            var uploadBtn = $('#dragDrop .browse-wrap');
            uploadBtn.removeClass('btn-disabled');
            this.on('addedfile', function(file) {
                console.log(file);
                if (this.files[1] != null) {
                    this.removeFile(this.files[0]);
                }
            });
            this.on("uploadprogress", function(file, progress) {
                console.log("File progress", progress);
            });
            this.on('canceled', resetForm);
            this.on('removedfile', resetForm);
            this.on('success', function(respData) {
                console.log(respData);
                 PhysicalDomEvent.validateFile();
                let key = respData.xhr.response.split('<Key>')[1].split('</Key>')[0];
                let etag = respData.xhr.response.split('<ETag>')[1].split('</ETag>')[0].replace('"', '').replace('"', '');
                let fileUrl = respData.xhr.response.split('<Location>')[1].split('</Location>')[0] + '?key=' + key + '&etag=' + etag;
                Storage.setSessionCollection('fileurl', fileUrl);
                Storage.setSessionCollection('key', uploadDetails.key);

            });
            this.on('error', function(file, message) {
                var errorEle = $('.dz-error-message');
                errorEle.find('span')
                    .text(Constant.FILE_UPLOAD_ERROR);
                errorEle.show();
                //  alert(message);
                // this.removeFile(file);
            });
        },
        sending: function(file, xhr, formData) {
            Storage.set('filename', file.name);
            Storage.set('filetype', file.type);
            switch (file.type) {
                case "text/csv":
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-12.png");
                    break;
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xlsx.png");
                    break;
                case "application/vnd.ms-excel":
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                    break;
                default:
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                    break; 
            }
            formData.append("key", uploadDetails.key);
            formData.append("acl", uploadDetails.acl);
            formData.append("x-amz-credential", uploadDetails["xAmzCredential"]);
            formData.append("x-amz-algorithm", uploadDetails["xAmzAlgorithm"]);
            formData.append("success_action_status", uploadDetails.successActionStatus);
            formData.append("x-amz-date", uploadDetails["xAmzDate"]);
            formData.append("policy", uploadDetails.policy);
            formData.append("x-amz-signature", uploadDetails["xAmzSignature"]);
            formData.append('content-type', file.type);
        }
    };
 };

let resetForm = function() {
    dropFlag = 0;
    $('#create-order').attr('disabled', 'disabled').addClass('btn-disabled');
};


let init = function() {   
    Storage.set('duplicate',false);
    var uploadDetails = $('#uploadDetails').val();
    uploadDetails != undefined ?   _initDropZone() : '';
    DomEventHandler.bindClassEvent('rewardGift', 'click', PhysicalDomEvent.selectProgramCode);
    DomEventHandler.bindEvent('create-order', 'click', PhysicalDomEvent.createIssueBulkOrderVerify);
    DomEventHandler.bindEvent('orderCompletedNew', 'click', PhysicalDomEvent.createIssueBulkOrder);
    DomEventHandler.bindEvent('orderCompleted', 'click', PhysicalDomEvent.createIssueBulkOrder);
    DomEventHandler.bindEvent('routeToAddFund','click',VirtualDomEvent.routeToAddFund);
    DomEventHandler.bindEvent('duplicateEntries', 'click',  PhysicalDomEvent.createIssueBulkOrderVerify);
    //DomEventHandler.bindClassEvent('revokePaymentLink', 'click',  PhysicalDomEvent.revokepayoutpopup);
    
    
    $('body').on('click', '.addFundsSG', function() {
        let accountId = $(this).attr('data-fundingaccountids');
        Storage.set('ACCOUNT_ID', accountId);
        let corporateId = $("meta[name='corpID']").attr("content");
        let authToken = Storage.get('authToken');
        PhysicalSvc.getAccDetails(authToken, corporateId, accountId).then(function(fundRespData) {
            let fundingAcc = fundRespData;
            Storage.setSessionCollection('fundingAccountName', fundingAcc.name);
            Storage.setSessionCollection('fundingAccountBalance', (fundingAcc.balance)/100);
            Storage.setSessionCollection('ifiID', (fundingAcc.ifiID));
            VirtualDomEvent.addFundsSG();
       }, function(respErr) {
           console.log(respErr);
       });
        
    });

    $('#reference-name').keyup(function(){
        let refValue = $('#reference-name').val()
        if((refValue != '') && (dropFlag != 0)){
            $('#create-order').removeAttr('disabled').removeClass('btn-disabled');
        } else{
            $('#create-order').attr('disabled', 'disabled').addClass('btn-disabled');
        }        
    })

    $('body').on('change','.inputFieldValidate',function(){
        let typeOfValidator = $(this).data('validator');
        let allFieldsFilled = false;
        if(typeOfValidator == 'email'){
            let values = $(this).val();

                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/;
                if($(this).val().match(mailformat)){                       
                    $(this).removeClass('input-error');
                    $(this).next().css('display','none'); 
                    allFieldsFilled = true;
                } else{                 
                    $(this).addClass('input-error');
                    $(this).next().css('display','block').html('Invalid Email Address');
                    allFieldsFilled = false;
                }
        }
        else if(typeOfValidator == 'phone'){
            let values = ''+$(this).val()+'';
            let typeOfValue = Number($(this).val());
            
                if(values.length == 10){
                    $(this).removeClass('input-error');
                    $(this).next().css('display','none'); 
                    allFieldsFilled = true;
                }else{
                    $(this).addClass('input-error');
                    $(this).next().css('display','block').html('Invalid Mobile Number'); 
                    allFieldsFilled = false;
                }
            
        } 
        else if(typeOfValidator == 'amount'){
            if($(this).val() != ''){
                $(this).removeClass('input-error');
                $(this).next().css('display','none');
                allFieldsFilled = true; 
            }else{
                $(this).addClass('input-error');
                $(this).next().css('display','block'); 
                allFieldsFilled = false;
            }
        } else if(typeOfValidator == 'name'){
            if($(this).val() != ''){
                $(this).removeClass('input-error');
                $(this).next().css('display','none'); 
                allFieldsFilled = true;
            }else{
                $(this).addClass('input-error');
                $(this).next().css('display','block');
                allFieldsFilled = false; 
            }
        }   else if(typeOfValidator == 'date'){
            if($(this).val() != ''){
                $(this).removeClass('input-error');
                $(this).next().css('display','none'); 
                allFieldsFilled = true;
            }else{
                // $(this).addClass('input-error');
                $(this).next().css('display','block');
                allFieldsFilled = false; 
            }
        }    

        
        if(allFieldsFilled === true){
            $('#is-all-fields-filled').val('true'); 

            if(($('#is-all-fields-filled').val() != '')) {
                $('#rewardSingleSelected').removeAttr('disabled').removeClass('btn-disabled');
            }
        }else{
            $('#rewardSingleSelected').attr('disabled','disabled').addClass('btn-disabled');            
        }
    });  
    
    $('.scheduleTransferDate').datepicker({
        format: "yyyy-mm-dd",
        startDate: new Date(),
        autoclose: true
    });
    //Listen for the change even on the input
   //.change(PhysicalDomEvent.validationForManualTransfer)
   //.on('changeDate', PhysicalDomEvent.validationForManualTransfer);
   
    
};
let setFlag = function() {
    return dropFlag;
};

export
default {
    init: init,
    setFlag:setFlag,
    resetForm: resetForm,
    //validationForCustomOrder:validationForCustomOrder
};
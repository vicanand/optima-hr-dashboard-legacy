import Constant from '../../common/constants';
import DomEventHandler from '../../common/domEventHandler';
import FilterWidget from '../../widgets/filter-widget';
import HomeDOMEvents from '../../dom-events/spotlight/home-dom-event';
import SelectGiftDomEvent from '../../dom-events/spotlight/selectgiftDOMEvents';
import Storage from '../../common/webStorage';
import Table from '../../widgets/table';
import TableWidget from '../../widgets/table-widget';
import HeaderController from '../../controller/headerCtrl';
import SidebarController from '../../controller/sidebarCtrl';
import SelectGiftSvc from '../../service/spotlight/selectgiftSvc';

HeaderController.init();
SidebarController.init();
Table.init();
TableWidget.init();
FilterWidget.init();

let dropFlag = 0;
let _initDropZone = function() {

    let reUploadStatus = Storage.getSessionCollection('reuploadStatus');
    let selectBrandStatus = Storage.getSessionCollection('selectBrandStatus');
    if (reUploadStatus == true) {
        $('#chooseShortcodeCntr').hide();
        $('#uploadFileCntr').show();
        $('#fileErrorWrpr').hide();
        $('#chooseStep').addClass('done');
        $('#uploadStep').addClass('active');
        //$('#chooseShortcodeCntr').hide();
    }
    if (selectBrandStatus == true) {
        $('#chooseShortcodeCntr').show();
        $('#uploadFileCntr').hide();
        $('#fileErrorWrpr').hide();
        $('#chooseStep').addClass('active');
    }

    var uploadDetails = $('#uploadDetails').val();
    uploadDetails = JSON.parse(uploadDetails);
    $.Dropzone.options.dragDrop = {
        addRemoveLinks: true,
        paramName: "file",
        previewsContainer: null,
        // clickable: true,
        acceptedFiles: '.csv,.xls,.xlsx',
        // error: true,
        dictDefaultMessage: '',
        uploadMultiple: false,
        maxFiles: 1,
        previewTemplate: document.getElementById('preview-template').innerHTML,
        url: uploadDetails.postAction,
        headers: {
            "Cache-Control": "",
            "X-Requested-With": "",
            "X-File-Name": ""
        },
        accept: function(file, done) {
            dropFlag = 1;
            console.log('uploaded');
            done();
        },
        init: function() {
            var uploadBtn = $('#dragDrop .browse-wrap');
            uploadBtn.removeClass('btn-disabled');
            this.on('addedfile', function(file) {
                console.log(file);
                if (this.files[1] != null) {
                    this.removeFile(this.files[0]);
                }
            });
            this.on("uploadprogress", function(file, progress) {
                console.log("File progress", progress);
            });
            this.on('canceled', resetForm);
            this.on('removedfile', resetForm);
            this.on('success', function(respData) {
                console.log(respData);
                SelectGiftDomEvent.validationForTransfer();
                let key = respData.xhr.response.split('<Key>')[1].split('</Key>')[0];
                let etag = respData.xhr.response.split('<ETag>')[1].split('</ETag>')[0].replace('"', '').replace('"', '');
                let fileUrl = respData.xhr.response.split('<Location>')[1].split('</Location>')[0] + '?key=' + key + '&etag=' + etag;
                Storage.set(Constant.STORAGE_FILE_URL, fileUrl);

            });
            this.on('error', function(file, message) {
                var errorEle = $('.dz-error-message');
                errorEle.find('span')
                    .text(Constant.FILE_UPLOAD_ERROR);
                errorEle.show();
                //  alert(message);
                // this.removeFile(file);
            });
        },
        sending: function(file, xhr, formData) {
            Storage.set(Constant.STORAGE_FILE_TYPE, file.type);
            switch (file.type) {
                case "text/csv":
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-12.png");
                    break;
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xlsx.png");
                    break;
                case "application/vnd.ms-excel":
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                    break;
                default:
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                    break;     
            }
            Storage.set(Constant.STORAGE_FILE_NAME, file.name);
            formData.append("key", uploadDetails.key);
            formData.append("acl", uploadDetails.acl);
            formData.append("x-amz-credential", uploadDetails["xAmzCredential"]);
            formData.append("x-amz-algorithm", uploadDetails["xAmzAlgorithm"]);
            formData.append("success_action_status", uploadDetails.successActionStatus);
            formData.append("x-amz-date", uploadDetails["xAmzDate"]);
            formData.append("policy", uploadDetails.policy);
            formData.append("x-amz-signature", uploadDetails["xAmzSignature"]);
            formData.append('content-type', file.type);
        }
    };

    $('#scheduleTransferDate').datepicker({
            format: "yyyy-mm-dd",
            startDate: new Date(),
            autoclose: true
        })
        //Listen for the change even on the input
        .change(SelectGiftDomEvent.validationForTransfer)
        .on('changeDate', SelectGiftDomEvent.validationForTransfer);
};

let resetForm = function() {
    dropFlag = 0;
    $('#create-order').attr('disabled', 'disabled').addClass('btn-disabled');
    $('#scheduleTransferDate,#purchaseOrderNo').val('');
};


let init = function() {
    
    DomEventHandler.bindEvent('generateSelectGiftingOrder', 'click', SelectGiftDomEvent.generateGift);
    DomEventHandler.bindEvent('retryGerenateGift', 'click', SelectGiftDomEvent.generateGift);
    DomEventHandler.bindClassEvent('termsNcon', 'click', SelectGiftDomEvent.termsNCon);
    //DomEventHandler.bindEvent('chooseThisBrand', 'click', SelectGiftDomEvent.brandNameSelected);
    DomEventHandler.bindClassEvent('chooseBrandName', 'click', SelectGiftDomEvent.chooseBrandName);
    DomEventHandler.bindEvent('create-order', 'click', SelectGiftDomEvent.initiateTransfer);
    DomEventHandler.bindEvent('uploadAgain', 'click', SelectGiftDomEvent.reUploadOrder);
    DomEventHandler.bindEvent('retryBtn', 'click', SelectGiftDomEvent.retryErrorModal);
    DomEventHandler.bindEvent('createSelectGiftingOrder', 'click', SelectGiftDomEvent.createSelectGiftOrder);
    DomEventHandler.bindEvent('setup_select_gifting', 'click', SelectGiftDomEvent.prepareGAEvent);
    DomEventHandler.bindEvent('learn_more_select_gifting', 'click', SelectGiftDomEvent.prepareGAEvent);
    DomEventHandler.bindEvent('setup_cash_incentive', 'click', SelectGiftDomEvent.prepareGAEvent);

    //DomEventHandler.bindClassEvent('cominSoonBrands', 'click', SelectGiftDomEvent.cominSoonBrands);
    DomEventHandler.bindClassEvent('addFundsSG', 'click', SelectGiftDomEvent.addFundsSG);
    DomEventHandler.bindEvent('routeToAddFund', 'click', SelectGiftDomEvent.routeToAddFund);

    DomEventHandler.bindEvent('fundingAccountID', 'change', SelectGiftDomEvent.fundingAccountIDChange);
    DomEventHandler.bindEvent('newfundingaccname', 'keyup', SelectGiftDomEvent.newFundingAccountName);
    DomEventHandler.bindEvent('changeAccSG', 'click', SelectGiftDomEvent.changeFundingAccount);
    DomEventHandler.bindEvent('setAccSG', 'click', SelectGiftDomEvent.setFundingAccount);

    DomEventHandler.bindEvent('uploadStep','click',SelectGiftDomEvent.showManuallyOnStep);
    DomEventHandler.bindEvent('chooseStep','click',SelectGiftDomEvent.showChooseDesign);   
    DomEventHandler.bindEvent('verifyStep','click',SelectGiftDomEvent.showVerifyAndPay);
    DomEventHandler.bindClassEvent('learn-more-dom','click',SelectGiftDomEvent.learnMoreGA);
    DomEventHandler.bindClassEvent('show-hide-terms','click',SelectGiftDomEvent.termsAndCondition);

// maintaining state
(function(window) {
    // exit if the browser implements that event
      if ("onhashchange" in window) {  return; }
    
      var location = window.location,
        oldURL = location.href,
        oldHash = location.hash;
      // check the location hash on a 100ms interval
      setInterval(function() {
        var newURL = location.href,
          newHash = location.hash;
       // if the hash has changed and a handler has been bound...
        if (newHash != oldHash && typeof window.onhashchange === "function") {
          // execute the handler
          window.onhashchange({
            type: "hashchange",
            oldURL: oldURL,
            newURL: newURL
          });

          oldURL = newURL;
          oldHash = newHash;
        }
      }, 10);
    })(window);

    $(window).on('hashchange', function() {
        var urlHash = window.location.hash;
        if(urlHash == '#uploadfile'){
        $('#chooseShortcodeCntr').hide();
        $('#verifyOrderCntr').hide();
        $('#chooseStep').addClass('done');
        $('#uploadStep').addClass('active');
        $('#uploadFileCntr').show();
        $('#myModalBrandDetail').modal('hide');
        }
        else if(urlHash == '#payment'){
            $('#uploadFileCntr').hide();
            $('#fileErrorWrpr').hide();
            $('#verifyStep').addClass('active');
            $('#uploadStep').addClass('done');
            $('#verifyOrderCntr').show();
            
        }
        else{
             
              $('#verifyOrderCntr').hide();
              $('#fileErrorWrpr').hide();
              $('#uploadFileCntr').hide();
              $('#chooseShortcodeCntr').show();
              $('#myModalBrandDetail').modal('hide');
            
        }
    });





    let url = window.location.pathname;

    if (url.match('/newgiftorder')) {
        _initDropZone();
    };
    $('body').on('click', '.csvDownlaodURL', function() {
        let url = $(this).data('fileurl');
        SelectGiftDomEvent.csvDownlaodURL(url);
   });
   $('body').on('click', '.addFundsSG', function() {
    let accountId = $(this).attr('data-fundingaccountids');
    Storage.set('ACCOUNT_ID', accountId);
    let corporateId = $("meta[name='corpID']").attr("content");
    let authToken = Storage.get('authToken');
    SelectGiftSvc.getAccDetails(authToken, corporateId, accountId).then(function(fundRespData) {
        let fundingAcc = fundRespData;
        Storage.setSessionCollection('fundingAccountName', fundingAcc.name);
        Storage.setSessionCollection('fundingAccountBalance', (fundingAcc.balance)/100);
        Storage.setSessionCollection('ifiID', (fundingAcc.ifiID));
        SelectGiftDomEvent.addFundsSG();
   }, function(respErr) {
       console.log(respErr);
   });
    
   });
   
   $('.denominationMin').text(Storage.getSessionCollection('purchaseMin'));
   $('.denominationMax').text(Storage.getSessionCollection('purchaseMax'));
   $('.brandLogoSg').attr('src', Storage.getSessionCollection('brandLogoUrl'));
   let purchasetype = Storage.getSessionCollection('purchasetype');
   if(purchasetype != null && purchasetype.toLowerCase() != 'range'){
    $('.denominationCont').show();
    $('.denominationCont1').hide();
    $('.denomination').text(Storage.getSessionCollection('denomination'));
   }
   else{
    $('.denominationCont').hide();
    $('.denominationCont1').show();
   }
};

let setFlag = function() {
    return dropFlag;
};


export
default {
    setFlag: setFlag,
    init: init,
    resetForm: resetForm
};
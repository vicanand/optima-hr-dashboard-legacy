import Constant from '../../common/constants';
import DomEventHandler from '../../common/domEventHandler';
import FilterWidget from '../../widgets/filter-widget';
import VirtualDomEvent from '../../dom-events/spotlight/virtualDOMEvents';
import Storage from '../../common/webStorage';
import Table from '../../widgets/table';
import TableWidget from '../../widgets/table-widget';
import HeaderController from '../../controller/headerCtrl';
import SidebarController from '../../controller/sidebarCtrl';
import VirtualSvc from '../../service/spotlight/virtualSvc';

HeaderController.init();
SidebarController.init();
Table.init();
TableWidget.init();
FilterWidget.init();

let dropFlag = 0;
 let _initDropZone = function () {
  
   
    var uploadDetails = $('#uploadDetails').val();
    uploadDetails = JSON.parse(uploadDetails);
    $.Dropzone.options.dragDrop = {
        addRemoveLinks: true,
        paramName: "file",
        previewsContainer: null,
        // clickable: true,
        acceptedFiles: '.csv,.xls,.xlsx',
        // error: true,
        dictDefaultMessage: '',
        uploadMultiple: false,
        maxFiles: 1,
        previewTemplate: document.getElementById('preview-template').innerHTML,
        url: uploadDetails.postAction,
        headers: {
            "Cache-Control": "",
            "X-Requested-With": "",
            "X-File-Name": ""
        },
        accept: function(file, done) {
            dropFlag = 1;
            console.log('uploaded');
            done();
        },
        init: function() {
            var uploadBtn = $('#dragDrop .browse-wrap');
            uploadBtn.removeClass('btn-disabled');
            this.on('addedfile', function(file) {
                console.log(file);
                if (this.files[1] != null) {
                    this.removeFile(this.files[0]);
                }
            });
            this.on("uploadprogress", function(file, progress) {
                console.log("File progress", progress);
            });
            this.on('canceled', resetForm);
            this.on('removedfile', resetForm);
            this.on('success', function(respData) {
                console.log(respData);
                VirtualDomEvent.validateFile();
                let key = respData.xhr.response.split('<Key>')[1].split('</Key>')[0];
                let etag = respData.xhr.response.split('<ETag>')[1].split('</ETag>')[0].replace('"', '').replace('"', '');
                let fileUrl = respData.xhr.response.split('<Location>')[1].split('</Location>')[0] + '?key=' + key + '&etag=' + etag;
                Storage.setSessionCollection('fileurl', fileUrl);
                Storage.setSessionCollection('key', uploadDetails.key);

            });
            this.on('error', function(file, message) {
                var errorEle = $('.dz-error-message');
                errorEle.find('span')
                    .text(Constant.FILE_UPLOAD_ERROR);
                errorEle.show();
                //  alert(message);
                // this.removeFile(file);
            });
        },
        sending: function(file, xhr, formData) {
            Storage.set('filename', file.name);
            Storage.set('filetype', file.type);
            switch (file.type) {
                case "text/csv":
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-12.png");
                    break;
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xlsx.png");
                    break;
                case "application/vnd.ms-excel":
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                    break;
                default:
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                    break;     
            }
            formData.append("key", uploadDetails.key);
            formData.append("acl", uploadDetails.acl);
            formData.append("x-amz-credential", uploadDetails["xAmzCredential"]);
            formData.append("x-amz-algorithm", uploadDetails["xAmzAlgorithm"]);
            formData.append("success_action_status", uploadDetails.successActionStatus);
            formData.append("x-amz-date", uploadDetails["xAmzDate"]);
            formData.append("policy", uploadDetails.policy);
            formData.append("x-amz-signature", uploadDetails["xAmzSignature"]);
            formData.append('content-type', file.type);
        }
    };
    $('#resetFilter').hide()
 };

let resetForm = function() {
    dropFlag = 0;
    $('#create-order').attr('disabled', 'disabled').addClass('btn-disabled');
};


let init = function() {
    _initDropZone();
    DomEventHandler.bindClassEvent('rewardGift', 'click', VirtualDomEvent.selectProgramCode);
    DomEventHandler.bindEvent('create-order', 'click', VirtualDomEvent.createNxtBulk);
    DomEventHandler.bindEvent('rewardSingleSelected', 'click', VirtualDomEvent.createNxtManual);
    DomEventHandler.bindEvent('duplicateEntries', 'click', VirtualDomEvent.duplicateEnrty);
    DomEventHandler.bindEvent('rewardTemplateSelected', 'click', VirtualDomEvent.showBenefits);
    DomEventHandler.bindEvent('rewardManually', 'click', VirtualDomEvent.showManually);
    DomEventHandler.bindEvent('rewardBulkUpload', 'click', VirtualDomEvent.showBenefitsBulk);
    DomEventHandler.bindEvent('orderCompleted', 'click', VirtualDomEvent.orderCreate);
    DomEventHandler.bindEvent('orderCompletedNew', 'click', VirtualDomEvent.orderCreate);
    DomEventHandler.bindClassEvent('addFundsSG', 'click', VirtualDomEvent.addFundsSG);
    DomEventHandler.bindEvent('routeToAddFund', 'click', VirtualDomEvent.routeToAddFund);    
    DomEventHandler.bindClassEvent('done-redirect', 'click', VirtualDomEvent.redirectUrl);    
    DomEventHandler.bindEvent('uploadStep','click',VirtualDomEvent.showManuallyOnStep);
    DomEventHandler.bindEvent('chooseStep','click',VirtualDomEvent.showChooseDesign);   
    DomEventHandler.bindEvent('verifyStep','click',VirtualDomEvent.showVerifyAndPay);
    
    
   // maintaining state
   (function(window) {
    // exit if the browser implements that event
      if ("onhashchange" in window) {  return; }
    
      var location = window.location,
        oldURL = location.href,
        oldHash = location.hash;
      // check the location hash on a 100ms interval
      setInterval(function() {
        var newURL = location.href,
          newHash = location.hash;
       // if the hash has changed and a handler has been bound...
        if (newHash != oldHash && typeof window.onhashchange === "function") {
          // execute the handler
          window.onhashchange({
            type: "hashchange",
            oldURL: oldURL,
            newURL: newURL
          });

          oldURL = newURL;
          oldHash = newHash;
        }
      }, 10);
    })(window);
    
    $(window).on('hashchange', function() {
        var urlHash = window.location.hash;
        if(urlHash == '#Step2'){
        $('#chooseDesign').hide();
        $('#createOrder').hide();
        //$('#createOrder').hide();
        $('#uploadStep').addClass('active');
        $('#uploadStep').removeClass('done');
        $('#verifyStep').removeClass('active');
        $('#verifyStep').removeClass('done');
        $('#virtualGiftContainer').show();
            // if(Storage.getSessionCollection('bulk') == true){
            //     $('#addBenefits').hide();
            //     $('#addBenefitsBulk').show();
            // }else{
            //     $('#addBenefits').show();
            //     $('#addBenefitsBulk').hide();
            // }

        }
        else if(urlHash == '#orderHistory'){
            $(urlHash).trigger( "click" );
            $('#createOrder').hide();
        }
        else if(urlHash == '#beneficiaries'){
            $(urlHash).trigger( "click" );
        }
        else if(urlHash == '#sendGift'){
            $(urlHash).trigger( "click" );
            $('#chooseDesign').show();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#fileErrorWrpr').hide();
            $('#virtualGiftContainer').show();
            $('#createOrder').hide();
            $('#chooseStep').removeClass('done');
            $('#uploadStep').removeClass('active');
            $('#uploadStep').removeClass('done');
            Storage.setSessionCollection('bulk',false);
            Storage.setSessionCollection('manual',true);
        }
        else if(urlHash == '#Step3Manual'){
            $('#chooseDesign').hide();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#fileErrorWrpr').hide();
            $('#virtualGiftContainer').show();
            $('#createOrder').show();
        }
        else if(urlHash == '#Step3Bulk'){
            $('#chooseDesign').hide();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#fileErrorWrpr').hide();
            $('#virtualGiftContainer').show();
            $('#createOrder').show();
        }
        else{
            var targetUrl = 'sendGift';
            window.history.pushState({url: "" + targetUrl + ""}, targetUrl);
            window.location.hash=targetUrl;
            Storage.setSessionCollection('bulk',false);
            Storage.setSessionCollection('manual',true);
            $('#virtualGiftContainer').show();
            $('#chooseDesign').show();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#chooseStep').removeClass('done');
            $('#uploadStep').removeClass('active');
            $('#uploadStep').removeClass('done');
            $('#createOrder').hide();
        }
    });


    $('body').on('click', '.append-amount', function() {
        let amount = $(this).data('amount');
        let id = $(this).data('id');
        $('.input-amount-validate'+id).val(amount); 
        let ifi = $("meta[name='ifi']").attr("content").toLowerCase();
        if(ifi === "hdfc" || ifi === 'kotak'){
            if(ifi === 'hdfc' && amount < 100){
                 $('.input-amount-validate'+id).addClass('input-error');
                 $('#rightHdfcAmount').val('false'); 
             } else if(ifi === 'kotak' && amount < 1) {
                $('.input-amount-validate'+id).addClass('input-error');
                $('#rightHdfcAmount').val('false'); 
             }
             else if(amount == ''){
                 $('.input-amount-validate'+id).addClass('input-error');
                 $('#rightHdfcAmount').val('false'); 
             }
             else{
                 $('.input-amount-validate'+id).removeClass('input-error');
                 $('#rightHdfcAmount').val('true'); 
                 $('#rewardSingleSelected').removeAttr('disabled').removeClass('btn-disabled');
             }
         }
    });


    $('body').on('click', '.addFundsSG', function() {
        let accountId = $(this).attr('data-fundingaccountids');
        Storage.set('ACCOUNT_ID', accountId);
        let corporateId = $("meta[name='corpID']").attr("content");
        
        let authToken = Storage.get('authToken');
        VirtualSvc.getAccDetails(authToken, corporateId, accountId).then(function(fundRespData) {
            let fundingAcc = fundRespData;
            Storage.setSessionCollection('fundingAccountName', fundingAcc.name);
            Storage.setSessionCollection('fundingAccountBalance', (fundingAcc.balance)/100);
            Storage.setSessionCollection('ifiID', (fundingAcc.ifiID));
            VirtualDomEvent.addFundsSG();
       }, function(respErr) {
           console.log(respErr);
       });
        
       });

    // $('body').on('change','.inputFieldValidate',function(){
    //     $('#rewardSingleSelected').removeAttr('disabled').removeClass('btn-disabled');
    // });  
    $('body').on('change keyup keypress focus','.kotakAmountValidate, .hdfcAmountValidate',amountchange)
    $('.hdfcAmountValidate').change(amountchange);
    $('.kotakAmountValidate').change(amountchange);
    function amountchange(){
        let ifi = $("meta[name='ifi']").attr("content").toLowerCase();
        let allFieldsFilled = false;
        if(ifi =="hdfc" || ifi === 'kotak'){
              if(ifi === 'hdfc' && $(this).val() < 100){
                   $(this).addClass('input-error');
                   allFieldsFilled = false;
                   $('#rightHdfcAmount').val('false'); 
               } else if(ifi === 'kotak' && $(this).val() < 1){
                    $(this).addClass('input-error');
                    allFieldsFilled = false;
                    $('#rightHdfcAmount').val('false'); 
                }
               else if($(this).val() == ''){
                   $(this).addClass('input-error');
                   allFieldsFilled = false;
                   $('#rightHdfcAmount').val('false'); 
               }
               else{
                   $(this).removeClass('input-error');
                   allFieldsFilled = true;
                   $('#rightHdfcAmount').val('true'); 
               }
           }
           else{
                $('#is-all-fields-filled').val('true'); 
                $('#rewardSingleSelected').removeAttr('disabled').removeClass('btn-disabled');
            }
       
       if(ifi.toLowerCase()=="hdfc"){   
           if(allFieldsFilled == true){
                   $('#rewardSingleSelected').removeAttr('disabled').removeClass('btn-disabled');
           }else{
               $('#rewardSingleSelected').attr('disabled','disabled').addClass('btn-disabled');            
           }
       }else{
           $('#is-all-fields-filled').val('true'); 
           $('#rewardSingleSelected').removeAttr('disabled').removeClass('btn-disabled');
       }
    }
    
   

    $('body').on('click','.remove',function() {
        console.log('hi');
        $(this).parent().hasClass('rewardBenefitInputDv').remove();
    });
    
    $('.scheduleTransferDate').datepicker({
        format: "yyyy-mm-dd",
        startDate: new Date(),
        autoclose: true
    })
    //Listen for the change even on the input
   .change(VirtualDomEvent.validationForManualTransfer)
   .on('changeDate', VirtualDomEvent.validationForManualTransfer);
   
    
    var intId = 1;
    $("#add").click(function() {
        var ifiBankName = $("meta[name='ifi']").attr("content").toLowerCase();
        intId++; 
        var minValue = ifiBankName === 'hdfc' ? 100 : 1;
        var fieldWrapper = $('#SpotlightIFIName').val() == 'HDFC'  ?
            $(`<li class="rewardBenefitInputDv">
            <div class="manualBenefitHead">
                <span class="dottedLine dottedLineFull" style="width:84%"></span>
                <strong class="removeX" style="font-size:  12px !important;text-transform:  uppercase;cursor:  pointer;color: #8c8793;">
                <img src="https://card-program-files.s3.amazonaws.com/hrDashboard/images/close-delete.svg" style="margin-bottom: 2px;margin-right: 3px;"/>
                Delete</strong></div>                                     
            </div>

            <div class="zeta-col1">
                <input type="text" class="inputField inputFieldValidate input-name-validate" data-validator="name" placeholder="Name">
            </div>

            <div class="zeta-col3 " style="width: 24%">
                <div class="input-group tooltip-custom">
                    <div class="input-group-addon"> &nbsp; ₹ &nbsp; </div>
                    <input type="number"  onpaste="return false;" class="inputField inputFieldValidate custom-border `+ifiBankName+`AmountValidate input-amount-validate`+(intId)+`" placeholder="Amount" data-validator="amount"
                        maxlength="8" style="
                width: 100%;"
                onkeypress="return event.charCode >= 48 && event.charCode <= 57"
            >
                    <span class="tooltiptext" style="left: 37.5%;width:  305px;">
                        <div class="inner-suggestion-container">
                            <div class="inner-suggestion-p" style="color:red; opacity:0.8;">Minimum value for virtual card is Rs. `+minValue+`</div>
                            <div class="inner-suggestion-p">Suggested Amounts</div>
                            <div class="inner-suggestion-amount" style="width:  297px;">
                            <div class="inner-suggestion-list">
                            <div class="append-amount" data-amount="100" data-id="`+intId+`">100</div>
                            <div class="append-amount" data-amount="500" data-id="`+intId+`">500</div>
                            <div class="append-amount" data-amount="1000" data-id="`+intId+`">1,000</div>
                            <div class="append-amount" data-amount="2500" data-id="`+intId+`">2,500</div>
                            <div class="append-amount" data-amount="5000" data-id="`+intId+`">5,000</div>
                        </div>
                            </div>
                        </div>
                    </span>
                </div>
                <!--  <p>Please fill the amount</p> -->
            </div>


            <div class="zeta-col4" style="width:21%;">
                <div class="form-group date-picker date-picker-width" style="margin-bottom:unset">
                    <div class="input-group" style="width:100%;">
                        <input type="text" class="inputField inputFieldValidate input-date-validate form-control datepicker-img scheduleTransferDate"
                            data-validator="date" placeholder="Transfer Date" id="" readonly/>
                    </div>
                </div>
            </div>


            <div class="zeta-col3">
                <input type="text" class="inputField inputFieldValidate input-email-validate" data-validator="email" placeholder="Email">
                <!--   <p>Invalid Email Address</p> -->
            </div>

            <div class="zeta-col4" style="width:49.5%;">
                <div class="input-group">
                    <div class="input-group-addon"> +91</div>
                    <input type="number" onkeypress="if(this.value.length==10) return false;" class="inputField inputFieldValidate input-phone-validate custom-border"
                        data-validator="phone" placeholder="Mobile Number" style="width:96%" autocomplete="off">
                </div>
            </div>
            <!--  <p>Invalid Date</p> -->

</li>`)
        
        :$(`<li class="rewardBenefitInputDv `+ifiBankName.toLowerCase()+`Beneficiary">
                                <div class="manualBenefitHead"><span class="dottedLine"></span> 
                                <strong class="removeX" style="font-size:  12px !important;text-transform:  uppercase;cursor:  pointer;color: #8c8793;">
                                <img src="https://card-program-files.s3.amazonaws.com/hrDashboard/images/close-delete.svg" style="margin-bottom: 2px;margin-right: 3px;"/>
                                Delete</strong></div>                                                                    
                                    <div class="zeta-col1">
                                        <input type="text" class="inputField inputFieldValidate input-name-validate" data-validator="name" placeholder="Name">
                                        <!--  <p>Please fill the name</p> -->
                                    </div>
                                    <div class="zeta-col3 " style="width: 49%">
                                    <div class="input-group tooltip-custom">
                                    <div class="input-group-addon"> &nbsp; ₹ &nbsp; </div>
                                            <input type="number"  onpaste="return false;" class="inputField inputFieldValidate input-amount-validate`+(intId)+` custom-border `+ifiBankName+`AmountValidate" placeholder="Amount" data-validator="amount" maxlength="8">
                                            <span class="tooltiptext">
                                                <div class="inner-suggestion-container">
                                                    <div class="inner-suggestion-p inner-suggestion-p-kotak" style="color:red; opacity:0.8;">Minimum value for virtual card is Rs. 1</div>                                                 
                                                    <div class="inner-suggestion-p">Suggested Amounts</div>                                                
                                                    <div class="inner-suggestion-amount">
                                                        <div class="inner-suggestion-list">
                                                            <div class="append-amount" data-amount="500" data-id="`+intId+`">500</div>
                                                            <div class="append-amount" data-amount="1000" data-id="`+intId+`">1,000</div>
                                                            <div class="append-amount" data-amount="2500" data-id="`+intId+`">2,500</div>
                                                            <div class="append-amount" data-amount="5000" data-id="`+intId+`">5,000</div>
                                                            <div class="append-amount" data-amount="10000" data-id="`+intId+`">10,000</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </span>
                                    </div>
                                        <!--  <p>Please fill the amount</p> -->
                                        
                                    </div>
                                   
                                    <div class="zeta-col3">
                                            <input type="text" class="inputField inputFieldValidate input-email-validate" data-validator="email" placeholder="Email or Mobile Number">
                                           <!--   <p>Invalid Email Address</p> -->
                                    </div>
                                    <div class="zeta-col4" style="width:49%;">
                                    <div class="form-group date-picker date-picker-width" style="margin-bottom:unset">
                                    <div class="input-group" style="width:100%;">
                                        <input type="text" class="inputField inputFieldValidate input-date-validate form-control datepicker-img scheduleTransferDate" data-validator="date" placeholder="Transfer Date" id="" readonly/>
                                    </div>
                                    </div>
                                    <!--  <p>Invalid Date</p> -->
                                    </div>
                                </li>`);  
        fieldWrapper.data("idx", intId);
        $("#buildyourform").append(fieldWrapper);
        if(intId < 10){
            $('.benefitAddBtn').show();
        }else{
            $('.benefitAddBtn').hide();
        }
        $('.scheduleTransferDate').datepicker({
            format: "yyyy-mm-dd",
            startDate: new Date(),
            autoclose: true
        })
        //Listen for the change even on the input
       .change(VirtualDomEvent.validationForManualTransfer)
       .on('changeDate', VirtualDomEvent.validationForManualTransfer);
    });   

    $('body').on('click','.removeX',function() {
        $(this).parent().parent().remove();
        intId--;
        $('.benefitAddBtn').show();
    });

};
let setFlag = function() {
    return dropFlag;
};

export
default {
    init: init,
    setFlag:setFlag,
    resetForm: resetForm,
    //validationForCustomOrder:validationForCustomOrder
};

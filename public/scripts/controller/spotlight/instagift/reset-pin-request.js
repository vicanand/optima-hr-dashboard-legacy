
import DomEvents from "../../../common/domEventHandler";
import InstagiftSvc from '../../../service/spotlight/instagift-svc';
import Util from '../../../common/util';
import Table from '../../../widgets/table';
import InstagiftDOMEvent from '../../../dom-events/spotlight/home-dom-event';

Table.init();
export
default {
    init: function() {
    	$('#approvePinReset').on('show.bs.modal', function(e) {
    		var requestID = $('.approve-pin').closest('.request-id').data('requestid');
    		$('#approvePinReset').attr('data-requestid', requestID);
        });
        $('#declinePinReset').on('show.bs.modal', function(e) {
    		var requestID = $('.decline-pin').closest('.request-id').data('requestid');
    		$('#declinePinReset').attr('data-requestid', requestID);
        });
        $('#approvePinResetSuccess').on('hidden.bs.modal', function () {
            location.reload();
        });
        $('#declinePinResetSuccess').on('hidden.bs.modal', function () {
            location.reload();
        });
    },
    approvePinRequest: function(requestID) {
        InstagiftSvc.approvePinRequest(requestID).then(function(respData) {
            if (respData) {
                $("#approvePinReset").modal('hide');
                $("#approvePinResetSuccess").modal('show');
            }
        });
    },
    declinePinRequest: function(requestID) {
        InstagiftSvc.declinePinRequest(requestID).then(function(respData) {
            if (respData) {
                $("#declinePinReset").modal('hide');
                $("#declinePinResetSuccess").modal('show');
            }
        });
    }
}

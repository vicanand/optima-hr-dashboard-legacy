import Util from '../../../common/util';
import InstagiftSvc from '../../../service/spotlight/instagift-svc';
import AnalyticsSvc from '../../../service/analytics/analytics-svc';
export
default {
    init: function() {
        this.attachEventListeners();
        let statsParam = {};
    },
    browserSupportFileUpload: function() {
        var isCompatible = false;
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            isCompatible = true;
        }
        return isCompatible;
    },
    isValidfile: function(data) {
        for (var i = 0; i < data.length; i++) {
            if (data[i].length < 4) {
                return false;
            }
        }
        return true;
    },
    setViainput: function(value) {
        if (value) {
            this.inputData = true;
        } else {
            this.inputData = false;
        }
    },
    clearInput: function() {
        $('#purchaseOrderNo').val('');
        this.inputData = false;
    },
    clearFile: function() {
        $('#file').val('');
        this.isFiledata = false;
    },
    showfileName: function() {
        $('.dz-filename').text(this.fileName);
    },
    hidefileName: function() {
        $('.file-input').removeClass('hide');
        $('.file-info').addClass('hide');
        $('.fileName').text('');
        this.isFiledata = false;
    },
    getCardnumber: function(element) {
        var cardNumber = $(element).find('.custom1').val().trim();
        var target = $(element).find('.custom1');
        if ($.isEmptyObject(cardNumber)) {
            Util.showErrmsg('Please enter card number', target);
            return;
        } else if (cardNumber.length !== 10) {
            Util.showErrmsg('Please enter valid 10 digit card number', target);
            return;
        } else {
            return '532571' + cardNumber;
        }
    },
    getCardamt: function(element) {
        var amount = $(element).find('input[name="amt"]').val().trim();
        var target = $(element).find('input[name="amt"]');
        if ($.isEmptyObject(amount)) {
            Util.showErrmsg('Please enter amount', target);
        } else if (parseInt(amount) > 10000) {
            Util.showErrmsg('Note: According to recent RBI guidelines, gift cards cannot be loaded with more than INR 10,000 for a beneficiary. <br><br> Please enter amount between 500 and 10000', target);
        } else if (parseInt(amount) < 500) {
            Util.showErrmsg('Note: According to recent RBI guidelines, gift cards cannot be loaded with more than INR 10,000 for a beneficiary. <br><br> Please enter amount between 500 and 10000', target);
        } else {
            return amount;
        }
    },
    getCardholdername: function(element) {
        var name = $(element).find('input[name="holder_name"]').val().trim();
        var target = $(element).find('input[name="holder_name"]');
        if ($.isEmptyObject(name)) {
            Util.showErrmsg('Please enter name', target);
        } else {
            return name;
        }
    },
    getCardphone: function(element) {
        var phone = $(element).find('input[name="phone"]').val().trim();
        var target = $(element).find('input[name="phone"]');
        if ($.isEmptyObject(phone)) {
            Util.showErrmsg('Please enter contact', target);
        } else {
            return phone;
        }
    },
    hasBalance: function(data) {
        var blnc = 0;
        for (var i = 0; i < data.cardLoads.length; i++) {
            blnc += parseInt(data.cardLoads[i].amount);
        }
        var blncRupees = blnc / 100;
        var userBlnc = parseInt($("#fundingBlanc").val());
        if (blncRupees > userBlnc) {
            Util.showMessage('Insufficient fund, Please add fund into your account', 'error');
            var additionalBal = blncRupees - userBlnc;
            $("#additional-bal").html(additionalBal);
            $("#insufficient-fund").show();
            $("#fundChequeTransfer").removeClass('hide-btn');
            let createOrderBtn = $("#create-order");
            $(createOrderBtn).addClass('btn-disabled');
            $(createOrderBtn).prop('disabled', true);
            return false;
        }
        return true;
    },
    isCardrowempty: function(element) {
        var cardNumber, amount, name, contact, pin, expiry;
        cardNumber = $(element).find('.custom1').val().trim();
        amount = $(element).find('input[name="amt"]').val().trim();
        name = $(element).find('input[name="holder_name"]').val().trim();
        contact = $(element).find('input[name="phone"]').val().trim();
        if (cardNumber && amount && name && contact) {
            return false;
        } else {
            return true;
        }

    },
    getInputdata: function() {
        var rows = $('table tbody tr');
        var cardArr = {};
        cardArr.cardLoads = [];
        for (var i = 0; i < rows.length; i++) {
            var cards = {};
            if (this.isCardrowempty(rows[i])) {
                Util.showMessage('Please enter all card related data', 'error');
                return;
            } else {
                if ($.isEmptyObject(this.getCardnumber(rows[i]))) {
                    return;
                } else {
                    cards.cardNumber = this.getCardnumber(rows[i]);
                }
                if ($.isEmptyObject(this.getCardamt(rows[i]))) {
                    return;
                } else {
                    cards.amount = this.getCardamt(rows[i]);
                }
                if ($.isEmptyObject(this.getCardholdername(rows[i]))) {
                    return;
                } else {
                    cards.name = this.getCardholdername(rows[i]);
                }
                if ($.isEmptyObject(this.getCardphone(rows[i]))) {
                    return;
                } else {
                    cards.contact = this.getCardphone(rows[i]);
                }
            }
            if (!$.isEmptyObject(cards)) {
                cardArr.cardLoads.push(cards);
            }
        }
        return cardArr;
    },
    dataFromfile: function(data, self) {
        if (!Util.isFilesRowValid(data)) {
            Util.showMessage('You can not upload cards more than 500 at a time', 'error');
            self.showfileName();
            $('#file').val('');
            return false;
        }
        var cardArr = {};
        cardArr.cardLoads = [];
        var error = 0;
        let cardStartNumberError = 0;
        let cardValidDigitNumberError = 0;
        let amountValidationError = 0;
        let amountEmptyError = 0;
        let amountSpecialCharError = 0;
        let nameError = 0;
        let contactError = 0;
        let rowCount = data.length;
        for (var i = 0; i < data.length; i++) {
            var cards = {};
            var errorCount = 0;
            for (var j = 0; j < data[i].length; j++) {
                switch (j) {
                    case 0:
                        var cardNumber = data[i][j];
                        cardNumber = cardNumber.replace(/[^0-9\.]+/g, '');
                        if (cardNumber && cardNumber.length === 16) {
                            var cardNumberValidation = "53257120";
                            if (cardNumber.indexOf(cardNumberValidation) > -1) {
                                cards.cardNumber = cardNumber;
                            } else {
                                Util.showMessage('Row' + (i + 1) + ' ' + ' Column 1: Card number should start with 53257120 ', 'error');
                                errorCount++;
                                cardStartNumberError++;
                            }
                        } else {
                            Util.showMessage('Row' + (i + 1) + ' ' + ' Column 1: Enter valid 16-digit card number as plain text', 'error');
                            errorCount++;
                            cardValidDigitNumberError++;
                        }
                        break;
                    case 1:
                        var amount = data[i][j];
                        var trailAmount = amount.replace(/\s/g, '');
                        if (Util.isValidAmount(trailAmount)) {
                            amount = parseInt(trailAmount);
                            if (amount >= 500 && amount <= 10000) {
                                cards.amount = amount * 100;
                            } else {
                                Util.showMessage('Note: According to recent RBI guidelines, gift cards cannot be loaded with more than INR 10,000 for a beneficiary. <br><br> Row' + (i + 1) + ' ' + ' Column 2: Please enter amount between 500 and 10000', 'error');
                                errorCount++;
                                amountValidationError++;
                            }
                        } else if (trailAmount.length === 0) {
                            Util.showMessage('Row' + (i + 1) + ' ' + ' Column 2: Please enter amount ', 'error');
                            errorCount++;
                            amountEmptyError++;
                        } else {
                            Util.showMessage('Row' + (i + 1) + ' ' + ' Column 2: Please enter amount without special character', 'error');
                            errorCount++;
                            amountSpecialCharError++;
                        }
                        break;
                    case 2:
                        var name = data[i][j];
                        var letters = /^[A-Za-z\s]+$/;
                        if (name.match(letters) && name.length) {
                            if (name.length > 60) {
                                Util.showMessage('Row' + (i + 1) + ' ' + ' Column 3: Please enter valid name limit to 60 character', 'error');
                                errorCount++;
                                nameError++;
                            } else {
                                cards.name = name;
                            }
                        } else {
                            Util.showMessage('Row' + (i + 1) + ' ' + ' Column 3: Please enter valid name without special character', 'error');
                            errorCount++;
                            nameError++;
                        }
                        break;
                    case 3:
                        var contact = data[i][j];
                        if (contact.length) {
                            cards.contact = contact;
                            if (contact.length > 60) {
                                Util.showMessage('Row' + (i + 1) + ' ' + ' Column 4: Please enter valid Contact limit to 60 character', 'error');
                                errorCount++;
                                contactError++;
                            } else {
                                cards.contact = contact;
                            }
                        } else {
                            Util.showMessage('Row' + (i + 1) + ' ' + ' Column 4: Please enter contact details', 'error');
                            errorCount++;
                            contactError++;
                        }
                        break;
                }
            }
            if (errorCount === 0 && !$.isEmptyObject(cards)) {
                cardArr.cardLoads.push(cards);
            }
        }
        if(cardStartNumberError > 0 || cardValidDigitNumberError > 0 || amountValidationError > 0 || amountEmptyError > 0 || amountSpecialCharError > 0 || nameError > 0 || contactError > 0)  {
            let eventName   = "fileParse.error";
            let userID      = $("meta[name='userID']").attr("content");
            let corp_ID     = $("meta[name='corpID']").attr("content");
            let company_ID  = $("meta[name='companyID']").attr("content");
            let eventLogs   = [
                {
                    "serviceName": "instagift",
                    "eventName": eventName,
                    "userID_longValue": userID,
                    "corpID_longValue": corp_ID,
                    "companyID_longValue": company_ID,
                    "cardStartNumberError_longValue": cardStartNumberError.toString(),
                    "cardValidDigitNumberError_longValue": cardValidDigitNumberError.toString(),
                    "amountValidationError_longValue": amountValidationError.toString(),
                    "amountEmptyError_longValue": amountEmptyError.toString(),
                    "amountSpecialCharError_longValue":amountSpecialCharError.toString(),
                    "nameError_longValue":nameError.toString(),
                    "contactError_longValue":contactError.toString(),
                    "totalRows_longValue":rowCount.toString()
                }
            ];
            AnalyticsSvc.recordEvents(eventLogs);
        }
        if(self.hasBalance(cardArr) ) {
            self.cardsArray = cardArr;
        }
        self.showfileName();
    },
    upload: function(evt, self) {
        if (!self.browserSupportFileUpload()) {
            alert('The File Util are not fully supported in this browser!');
        } else {
            var data = null;
            self.isFiledata = true;
            var file = evt.target.files[0];
            self.fileName = file.name;
            var reader = new FileReader();
            reader.readAsText(file);
            reader.onload = function(event) {
                var csvData = event.target.result;
                data = $.csv.toArrays(csvData);
                if (data && data.length && self.isValidfile(data)) {
                    let dataLength = data.length;
                    self.dataFromfile(data, self);
                    if (self.cardsArray) {
                        let cardLoadLength = self.cardsArray.cardLoads.length;
                        if (dataLength === cardLoadLength) {
                            self.enableLoadButton();
                        }
                    }
                } else {
                    $('.load-cards-preview .dz-error-message').show();
                    Util.showMessage('Upload file with required data', 'error');
                    $(evt.target).val('');
                }
            };
            reader.onerror = function() {
                alert('Unable to read ' + file.fileName);
            };
        }
    },
    enableLoadButton: function() {
        let createOrderBtn = $("#create-order");
        createOrderBtn.removeClass('btn-disabled');
        createOrderBtn.prop('disabled', false);
        $(".dz-details").addClass('validFile');
    },
    toJson: function(workbook, self) {
        var result = {};
        var cardArr = {};
        workbook.SheetNames.forEach(function(sheetName) {
            var data = XLSX.utils.sheet_to_csv(workbook.Sheets[sheetName]);
            data = $.csv.toArrays(data);
            if (data && data.length && self.isValidfile(data)) {
                let dataLength = data.length;
                self.dataFromfile(data, self);
                let cardLoadLength = self.cardsArray.cardLoads.length;
                if (dataLength === cardLoadLength) {
                    self.enableLoadButton();
                }
            } else {
                Util.showMessage('Upload file with required data', 'error');
                $('.load-cards-preview .dz-error-message').show();
                $('#file').val('');
            }
        });
    },
    handleExcel: function(e, self) {
        var files = e.target.files;
        var i, f;
        self.isFiledata = true;
        self.fileName = files[0].name;
        for (i = 0, f = files[i]; i != files.length; ++i) {
            var reader = new FileReader();
            var name = f.name;
            reader.onload = function(e) {
                var data = e.target.result;

                var workbook = XLSX.read(data, { type: 'binary' });
                self.toJson(workbook, self);
            };
            reader.readAsBinaryString(f);
        }
    },
    attachEventListeners: function() {
        var self = this;
        var requestRunning = false;
        $('#create-order').on('click', function(event) {
            if (requestRunning) {
                return;
            }
            let purchaseOrderNo = $("#purchaseOrderNo");
            if (purchaseOrderNo.val().length === 0) {
                purchaseOrderNo.addClass('error-bdr');
                $("#refNumber").show();
                return;
            } else {
                purchaseOrderNo.removeClass('error-bdr');
                $("#refNumber").hide();
            }
            var cardArr = {};
            if (self.isFiledata && self.inputData) {
                Util.showMessage('Please select one option, either via file or inputs', 'error');
                return;
            } else if (self.isFiledata) {
                cardArr = self.cardsArray;
            } else {
                cardArr = self.getInputdata();
            }
            if (!$.isEmptyObject(cardArr) && self.hasBalance(cardArr)) {
                requestRunning = true;
                var $btn = $(this);
                let loading = $("#loading");
                $(loading).modal('show');
                var refName = $('#purchaseOrderNo').val();
                var programID = $('#programID').val();
                let createOrderBtn = $("#create-order");
                let company_ID       = parseInt($("meta[name='companyID']").attr("content"));
                InstagiftSvc.loadCards(programID, refName, cardArr).then(function(response) {
                    $(loading).modal('hide');
                    $("#myModalInitiatedSuccess").modal('show');
                    $("#myModalInitiatedSuccess #viewCardProgram").attr("href", "/companies/"+company_ID +"/spotlight/instagift/"+ programID +"/transfers/" + response.orderID);
                    self.clearInput();
                    self.clearFile();
                    requestRunning = false;
                    self.cardsArray = {};
                    $('.load-cards-preview .dz-error-message').hide();
                    $('.file-upload-container').show();
                    $('.load-cards-preview').hide();
                    $(createOrderBtn).addClass('btn-disabled');
                    $(createOrderBtn).prop('disabled', true);
                }, function(error) {
                    self.clearFile();
                    $("#Errorstate").modal('show');
                    $(loading).modal('hide');
                    $('.load-cards-preview .dz-error-message').hide();
                    $('.file-upload-container').show();
                    $('.load-cards-preview').hide();
                    $(createOrderBtn).addClass('btn-disabled');
                    $(createOrderBtn).prop('disabled', true);
                    requestRunning = false;
                    self.cardsArray = {};
                });
            } else if (self.isFiledata) {
                $('#file').val('');
                self.cardsArray = {};
            }
        });

        $('input[name="phone"]').on('change', function(event) {
            self.setViainput($(this).val().trim());
            Util.hideErrmsg(this);
            if (!Util.isValidMobile($(this).val().trim()) && !Util.isValidEmail($(this).val().trim())) {
                Util.showErrmsg('Enter valid phone number/email', this);
            }
        });

        $('.clear').on('click', function(event) {
            self.clearInput();
        });

        $('.remove').on('click', function(event) {
            self.hidefileName();
            $('#file').val('');
        });

        $('input[name="amt"]').on('change', function(event) {
            self.setViainput($(this).val().trim());
            Util.hideErrmsg(this);
        });

        $('input[name="holder_name"]').on('change', function(event) {
            self.setViainput($(this).val().trim());
            Util.hideErrmsg(this);
        });

        $('.custom1').on('change', function(event) {
            self.setViainput($(this).val().trim());
            Util.hideErrmsg(this);
            if ($(this).val().trim().length !== 10) {
                Util.showErrmsg('Please enter valid 10 digit card number', this);
            }
        });

        $('#file').on('change', function(event) {
            Util.hideErrmsg(this);
            $('.load-cards-preview .dz-error-message').hide();
            $('.file-upload-container').hide();
            $('.load-cards-preview').show();
            let createOrderBtn = $("#create-order");
            createOrderBtn.addClass('btn-disabled');
            createOrderBtn.prop('disabled', true);
            if (event.target.files[0].size > 1024 * 1024 * 2) {
                Util.showMessage('File size should not be greater than 2MB', 'error');
                var file = event.target.files[0];
                $('.dz-filename').text(file.name);
                $(this).val('');
            } else if ($(this).val().includes('csv')) {
                self.upload(event, self);
            } else if ($(this).val().includes('xls')) {
                self.handleExcel(event, self);
            } else {
                Util.showMessage('Upload file in CSV/XLSX format', 'error');
                var file = event.target.files[0];
                $('.dz-filename').text(file.name);
                $(this).val('');
            }
        });

        $('.removelink').on('click', function() {
            $('.file-upload-container').show();
            $('.load-cards-preview').hide();
            $('.error').html('');
            $('.success').html('');
            $('#file').val('');
            let createOrderBtn = $("#create-order");
            $(createOrderBtn).addClass('btn-disabled');
            $(createOrderBtn).prop('disabled', true);
            $(".dz-details").removeClass('validFile');
            $("#insufficient-fund").hide();
            $("#fundChequeTransfer").addClass('hide-btn');
        });

        $('.custom1').on('input', function(event) {
            Util.hideErrmsg(this);
        });

        $('.delete').on('click', function(event) {
            var allow = confirm('Are you sure want to delete this entity ?')
            if (allow) {
                event.currentTarget.closest('tr').remove();
            }
        });
    }
}

import TransferDetailsDomEvents from "../../../dom-events/transferDetailsDomEvents";
import DomEvents from "../../../common/domEventHandler";
import InstagiftSvc from '../../../service/spotlight/instagift-svc';
import Util from '../../../common/util';

export
default {
    init: function() {
        $("#instagiftPayoutstable").tablewidget({
            source: InstagiftSvc.getPayouts,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: "/template/section/spotlight/instagift/tab-content/tables/transfer-details.ejs",
            rowTemplate: "/template/section/spotlight/instagift/tab-content/tables/transfer-details-row.ejs",
            extraParam: {
                statusFilters: ''
            },
            searchSelector: $("#employeeSearch"),
            searchButton: $("#searchEmployeeBtn"),
            payoutFilter: $('#instagiftPayoutFilters')
        });
        this.registerDomEvents();
    },

    registerDomEvents: function(){
        DomEvents.bindEvent('instagiftPayoutstable', 'click', '.revokePaymentLink', TransferDetailsDomEvents.revokePaymentModalShow);
        DomEvents.bindEvent('confirmRevoke', 'click', TransferDetailsDomEvents.revokePayment);
        DomEvents.bindEvent('responseBtn', 'click', TransferDetailsDomEvents.reloadPage);
    }
};
import DomEvents from "../../../common/domEventHandler";
import Constants from "../../../common/constants";
import BenifitDomEvents from "../../../dom-events/benefitDetailsDOMEvent";
import InstagiftSvc from "../../../service/spotlight/instagift-svc";
import TopMerchantsTemplate from "../../../../template/section/spotlight/instagift/tab-content/top-merchants.ejs";
import Util from '../../../common/util';

export
default {
    init: function() {
        DomEvents.bindEvent("benifitsEmployees", "click", ".closeCardBtn", BenifitDomEvents.closeCard);

        DomEvents.bindEvent("transferOrderTable", "click", ".cancelOrderBtn", BenifitDomEvents.confirmCancel);

        DomEvents.bindEvent("cancelOrderConfirm", "click", ".reset", BenifitDomEvents.resetCancel);

        DomEvents.bindEvent("cancelOrderConfirm", "click", ".confirm", BenifitDomEvents.cancelOrder);

        DomEvents.bindEvent("cancelOrderSuccess", "click", ".success", BenifitDomEvents.cancelOrderSuccess);

        InstagiftSvc.getTopMerchants('GET_TOP_MERCHANTS').then(function(topMerchantsData) {
            $('#merchantGraphLoader').hide();
            let companyID = parseInt($("meta[name='companyID']").attr("content"));
            let programID = parseInt($("#programID").val());
            topMerchantsData.aggregations.totalUniqueMerchants = Util.formatNumber(topMerchantsData.aggregations.totalUniqueMerchants);
            topMerchantsData.companyID = companyID;
            topMerchantsData.programID = programID;
            for (var i = 0; i < topMerchantsData.aggregations.topLocations.length; i++) {
                topMerchantsData.aggregations.topLocations[i].count = parseFloat((topMerchantsData.aggregations.topLocations[i].count / topMerchantsData.aggregations.total) * 100).toFixed(2) + '%';
            }
            DomEvents.renderMyTemplate("merchantsWrpr", TopMerchantsTemplate, topMerchantsData);
        }, function(respErr) {
            $('#merchantGraphLoader').hide();
            $('#merchantGraphError').show();
        });
    },
    closeCard: function(companyId, employeeId) {
        return EmployeeSvc.closeCard(companyId, employeeId);
    },
    cancelOrder: function(orderId) {
        return InstagiftSvc.cancelOrder(orderId);
    }
}

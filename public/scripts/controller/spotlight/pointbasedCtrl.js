import Constant from '../../common/constants';
import DomEventHandler from '../../common/domEventHandler';
import PointbasedDomEvent from '../../dom-events/spotlight/pointbasedDOMEvents';
import Storage from '../../common/webStorage';
import daterangepicker from 'bootstrap-daterangepicker';
import moment from '../../../lib/moment/moment';

function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], {
        type: "text/csv"
    });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = $('#errorSelectGiftingTransfers')[0].querySelectorAll("table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [],
            cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++) {

            row.push('"' + cols[j].innerText.replace(/\n/g, ", ") + '"');
        }
        csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
}



let initDatePicker = function () {
    var start = moment().subtract(6, 'month').startOf('month');
    var end = moment();

    function cb(start, end) {
        $('input[name="daterange"]').val('');
    }
    $('input[name="daterange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

    $('input[name="daterange"]').daterangepicker({
        "autoApply": true,
        locale: {
            format: 'YYYY-MM-DD',
            "customRangeLabel": "Choose From Calendar",
        },
        "opens": "left",
        maxDate: new Date(),
        startDate: start,
        endDate: end,
        ranges: {
            'All': [moment().subtract(12, 'month').startOf('month'), moment()],
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
    cb(start, end);
};

let dropFlag = 0;
let _initDropZone = function () {
    var uploadType = $('.uploadType').val();
    var uploadDetails = $('#uploadDetails').val();
    uploadDetails = JSON.parse(uploadDetails);
    $.Dropzone.options.dragDrop = {
        addRemoveLinks: true,
        paramName: "file",
        previewsContainer: null,
        // clickable: true,
        acceptedFiles: '.csv,.xls,.xlsx',
        // error: true,
        dictDefaultMessage: '',
        uploadMultiple: false,
        maxFiles: 1,
        previewTemplate: document.getElementById('preview-template').innerHTML,
        url: uploadDetails.postAction,
        headers: {
            "Cache-Control": "",
            "X-Requested-With": "",
            "X-File-Name": ""
        },
        accept: function (file, done) {
            dropFlag = 1;
            console.log('uploaded');
            done();
        },
        init: function () {
            var uploadBtn = $('#dragDrop .browse-wrap');
            uploadBtn.removeClass('btn-disabled');
            this.on('addedfile', function (file) {
                console.log(file);
                if (this.files[1] != null) {
                    this.removeFile(this.files[0]);
                }
            });
            this.on("uploadprogress", function (file, progress) {
                console.log("File progress", progress);
            });
            this.on('canceled', resetForm);
            this.on('removedfile', resetForm);
            this.on('success', function (respData) {
                console.log(respData);
                if (uploadType === 'issuepoint') {
                    PointbasedDomEvent.validateFile();
                } else {
                    $('#uploadBeneficiary').attr('disabled', false).removeClass('btn-disabled');
                }
                let key = respData.xhr.response.split('<Key>')[1].split('</Key>')[0];
                let etag = respData.xhr.response.split('<ETag>')[1].split('</ETag>')[0].replace('"', '').replace('"', '');
                let fileUrl = respData.xhr.response.split('<Location>')[1].split('</Location>')[0] + '?key=' + key + '&etag=' + etag;
                Storage.setSessionCollection('fileurl', fileUrl);
                Storage.setSessionCollection('key', uploadDetails.key);

            });
            this.on('error', function (file, message) {
                var errorEle = $('.dz-error-message');
                errorEle.find('span')
                    .text(Constant.FILE_UPLOAD_ERROR);
                errorEle.show();                
            });
        },
        sending: function (file, xhr, formData) {
            Storage.set('filename', file.name);
            Storage.set('filetype', file.type);
            switch (file.type) {
                case "text/csv":
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-12.png");
                    break;
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xlsx.png");
                    break;
                case "application/vnd.ms-excel":
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                    break;
                default:
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                    break;
            }
            formData.append("key", uploadDetails.key);
            formData.append("acl", uploadDetails.acl);
            formData.append("x-amz-credential", uploadDetails["xAmzCredential"]);
            formData.append("x-amz-algorithm", uploadDetails["xAmzAlgorithm"]);
            formData.append("success_action_status", uploadDetails.successActionStatus);
            formData.append("x-amz-date", uploadDetails["xAmzDate"]);
            formData.append("policy", uploadDetails.policy);
            formData.append("x-amz-signature", uploadDetails["xAmzSignature"]);
            formData.append('content-type', file.type);
        }
    };
};

let resetForm = function () {
    dropFlag = 0;
    $('#order-verify-file').attr('disabled', 'disabled').addClass('btn-disabled');
    $('#uploadBeneficiary').attr('disabled', 'disabled').addClass('btn-disabled');
};

let init = function () {
    var uploadDetails = $('#uploadDetails').val();
    uploadDetails != undefined ? _initDropZone() : '';
    DomEventHandler.bindEvent('order-verify-file', 'click', PointbasedDomEvent.issuePointsVerify);
    DomEventHandler.bindEvent('orderCompleted', 'click', PointbasedDomEvent.createBudget);
    DomEventHandler.bindEvent('createGiftCheckbox', 'click', PointbasedDomEvent.createGiftCheckbox);
    DomEventHandler.bindEvent('uploadBeneficiary', 'click', PointbasedDomEvent.uploadBeneficiary);
    DomEventHandler.bindEvent('uploadAgain', 'click', PointbasedDomEvent.uploadAgain);
    DomEventHandler.bindEvent('showMorePrograms', 'click', PointbasedDomEvent.showMorePrograms);
    DomEventHandler.bindEvent('revokeBadgescall', 'click', PointbasedDomEvent.revokeBadgesCall);

    $('#scheduleTransferDate').datepicker({
        format: "yyyy-mm-dd",
        startDate: new Date(), 
        autoclose: true
    });

    $('body').on('click', '.generateCertificate', function () {
        let badgeID, badgeLogo, description, badgeName, certificateheaderimage, certificatefooterimage, certificategradientcolor1, certificategradientcolor2;
        badgeID = $(this).data('badgeid');
        badgeLogo = $(this).data('badgelogo');
        description = $(this).data('description');
        certificateheaderimage = $(this).data('certificateheaderimage');
        certificatefooterimage = $(this).data('certificatefooterimage');
        certificategradientcolor1 = $(this).data('certificategradientcolor1');
        certificategradientcolor2 = $(this).data('certificategradientcolor2');
        badgeName = $(this).data('badgename');

        $('.badge-logo').attr('src', badgeLogo);
        $('#descriptionAward').text(description);
        $('#certificateheaderimage').attr('src', certificateheaderimage);
        $('#certificatefooterimage').attr('src', certificatefooterimage);
        $('#badgename').text(badgeName);
        $('.body-award').attr('style', `background-image: linear-gradient(to right, ${certificategradientcolor1} , ${certificategradientcolor2})`);
        $('.award-member-name').attr('style', `color:${certificategradientcolor2}`);
        $('#show-cretificate').modal('show');
    });

    $("select#fundingAccountID").change(function () {
        var newBalance = $('#fundingAccountID option:selected').attr('data-balance');
        var fundingaccountname = $('#fundingAccountID option:selected').attr('data-name');
        var id = $('#fundingAccountID option:selected').val();
        $('#balanceFund').html(newBalance);
        sessionStorage.setItem('fundingaccountid', id);
        sessionStorage.setItem('fundingaccountbalance', newBalance);
        sessionStorage.setItem('fundingaccountname', fundingaccountname);
        $('#createOrderDone').attr('data-fundingaccountids', id);
        PointbasedDomEvent.validateFile();
    });

    $('#downloadSGReport').on("click", function () {        
        var html = $('#errorSelectGiftingTransfers')[0].innerHTML;        
        export_table_to_csv(html, "error-report.csv");
    });

};

let setFlag = function () {
    return dropFlag;
};

export
default {
    init: init,
    setFlag: setFlag,
    resetForm: resetForm,
    initDatePicker: initDatePicker
};
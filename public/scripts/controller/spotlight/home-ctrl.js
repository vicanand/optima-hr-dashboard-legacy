import DomEventHandler from '../../common/domEventHandler';
import HomeDOMEvents from '../../dom-events/spotlight/home-dom-event';

export
default {
    init: function() {        
        $('#setupInstagiftProgramModal').on('show.bs.modal', function(e) {
            $('#programType').html(e.relatedTarget.dataset.programname);
            $('#programName').val(e.relatedTarget.dataset.programname);
            $('#createProgram').attr('data-programType', e.relatedTarget.dataset.programtype);
        });
        DomEventHandler.bindEvent('fundingAccountID', 'change', HomeDOMEvents.fundingAccountIDChange);
        DomEventHandler.bindEvent('cashIncentiveFundingAccountID', 'change', HomeDOMEvents.cashIncentviefundingAccountIDChange);
        DomEventHandler.bindEvent('selectGiftingFundingAccountID', 'change', HomeDOMEvents.selectGiftingfundingAccountIDChange);
        //DomEventHandler.bindEvent('programName', 'keyup', HomeDOMEvents.programNameChange);
        DomEventHandler.bindEvent('createProgram', 'click', HomeDOMEvents.createProgram);
        DomEventHandler.bindEvent('cashIncentiveCreateProgram', 'click', HomeDOMEvents.cashIncentiveCreateProgram);
        DomEventHandler.bindEvent('selectGiftingCreateProgram', 'click', HomeDOMEvents.selectGiftingCreateProgram);
        DomEventHandler.bindEvent('closeSG', 'click', HomeDOMEvents.closeSG);
        DomEventHandler.bindEvent('vitualCardSendGift', 'click', HomeDOMEvents.vitualCardSendGift);
        DomEventHandler.bindEvent('vitualCardListOrder', 'click', HomeDOMEvents.vitualCardListOrder);
        
        // DomEventHandler.bindClassEvent('order-cards', 'click', HomeDOMEvents.orderCards);
        // DomEventHandler.bindClassEvent('request-demo', 'click', HomeDOMEvents.requestDemo);
    }
}

import Constant from '../../common/constants';
import ServiceConnector from "../../common/serviceConnector";
import DomEventHandler from '../../common/domEventHandler';
import FilterWidget from '../../widgets/filter-widget';
import RewardDomEvent from '../../dom-events/spotlight/rewardDOMEvents';
import Storage from '../../common/webStorage';
import Table from '../../widgets/table';
import TableWidget from '../../widgets/table-widget';
import HeaderController from '../../controller/headerCtrl';
import SidebarController from '../../controller/sidebarCtrl';
import ellipsis from '../../common/ellipsis';

HeaderController.init();
SidebarController.init();
Table.init();
TableWidget.init();
FilterWidget.init();

let dropFlag = 0;
let _initDropZone = function () {
  
    var uploadDetails = $('#uploadDetails').val();
    uploadDetails = JSON.parse(uploadDetails);
    $.Dropzone.options.dragDrop = {
      addRemoveLinks: true,
      paramName: "file",
      previewsContainer: null,
      // clickable: true,
      acceptedFiles: '.csv,.xls,.xlsx',
      // error: true,
      dictDefaultMessage: '',
      uploadMultiple: false,
      maxFiles: 1,
      previewTemplate: document.getElementById('preview-template').innerHTML,
      url: uploadDetails.postAction,
      headers : {
        "Cache-Control": "",
        "X-Requested-With": "",
        "X-File-Name" : ""
    },
      accept: function (file, done) {
        dropFlag = 1;
        console.log('uploaded');
        done();
      },
      init: function () {
        var uploadBtn = $('#dragDrop .browse-wrap');
        uploadBtn.removeClass('btn-disabled');
        this.on('addedfile', function (file) {
          console.log(file);
          if (this.files[1] != null) {
            this.removeFile(this.files[0]);
          }
        });
        this.on("uploadprogress", function (file, progress) {
          console.log("File progress", progress);
        });
        this.on('canceled', resetForm);
        this.on('removedfile', resetForm);
        this.on('success', function(respData){
            RewardDomEvent.validateFile() 
        
        let key = respData.xhr.response.split('<Key>')[1].split('</Key>')[0];
        let etag = respData.xhr.response.split('<ETag>')[1].split('</ETag>')[0].replace('"','').replace('"','');
        let fileUrl = respData.xhr.response.split('<Location>')[1].split('</Location>')[0] + '?key=' + key + '&etag=' + etag;
        Storage.set(Constant.STORAGE_FILE_URL, fileUrl);
        Storage.setSessionCollection('storage-fileurl', fileUrl);
         });
    
        this.on('error', function (file, message) {
          var errorEle = $('.dz-error-message');
          errorEle.find('span')
            .text(Constant.FILE_UPLOAD_ERROR);
          errorEle.show();
          //  alert(message);
          // this.removeFile(file);
        });
      },
      sending: function (file, xhr, formData) {
        console.log(file.type);
  
        switch (file.type) {
          case "text/csv":
            $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-12.png");
            break;
          case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
            $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xlsx.png");
            break;
          case "application/vnd.ms-excel":
            $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
            break;
          default:
            $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
            break;   
        }
        
        Storage.setSessionCollection('fileName', file.name);
        Storage.setSessionCollection('key', uploadDetails.key);

        formData.append("key", uploadDetails.key);
        formData.append("acl", uploadDetails.acl);
        formData.append("x-amz-credential", uploadDetails["xAmzCredential"]);
        formData.append("x-amz-algorithm", uploadDetails["xAmzAlgorithm"]);
        formData.append("success_action_status", uploadDetails.successActionStatus);
        formData.append("x-amz-date", uploadDetails["xAmzDate"]);
        formData.append("policy", uploadDetails.policy);
        formData.append("x-amz-signature", uploadDetails["xAmzSignature"]);
        formData.append('content-type', file.type);
      }
    }
};

let resetForm = function() {
    dropFlag = 0;
    $('#create-order').attr('disabled', 'disabled').addClass('btn-disabled');
};

let  validationForCustomOrder = function() {
   if(($('#cutomCardName').val().length > 0) && ($('.templateChoose').hasClass('active') == true)){
       $('#openEmail').removeAttr('disabled').removeClass('btn-disabled');
       console.log('nextac');
   }
   else{
       $('#openEmail').attr('disabled','disabled').addClass('btn-disabled');
   }

   if (($('#cutomCardName').val().length > 0) && ($('#cutomCardTitle').val().length > 0) && ($('#cutomCardMessage').val().length > 0) && ($('.templateChoose').hasClass('active') == true)) {
       $('#createCustomCard').removeAttr('disabled').removeClass('btn-disabled');
       console.log('creste');
   }
   else{
       $('#createCustomCard').attr('disabled','disabled').addClass('btn-disabled');
   }
};


let init = function() {
    _initDropZone();
    DomEventHandler.bindClassEvent('rewardGift', 'click', RewardDomEvent.selectProgramCode);
    DomEventHandler.bindClassEvent('addNewTemplate', 'click', RewardDomEvent.addNewTemplateCode);
    DomEventHandler.bindEvent('rewardBulkUpload', 'click', RewardDomEvent.showBenefitsBulk);
    DomEventHandler.bindEvent('rewardTemplateSelected', 'click', RewardDomEvent.showBenefits);
    DomEventHandler.bindEvent('rewardManually', 'click', RewardDomEvent.showManually);
    DomEventHandler.bindEvent('create-order', 'click', RewardDomEvent.createNxt);
    DomEventHandler.bindEvent('orderCompleted', 'click', RewardDomEvent.createOrder);
    DomEventHandler.bindEvent('orderCompletedNew', 'click', RewardDomEvent.createOrder);
    DomEventHandler.bindEvent('openEmail', 'click', RewardDomEvent.closePhone);
    DomEventHandler.bindClassEvent('templateChoose', 'click', RewardDomEvent.addNewTemplateChoose);
    DomEventHandler.bindEvent('createCustomCard', 'click', RewardDomEvent.createCustomCard);
    DomEventHandler.bindClassEvent('rewardGiftDummy', 'click', RewardDomEvent.createDummyCustomCard);
    DomEventHandler.bindEvent('rewardSingleSelected', 'click', RewardDomEvent.createManualFile);
    DomEventHandler.bindClassEvent('done-redirect', 'click', RewardDomEvent.redirectUrl);    
    DomEventHandler.bindEvent('uploadStep','click',RewardDomEvent.showManuallyOnStep);
    DomEventHandler.bindEvent('chooseStep','click',RewardDomEvent.showChooseDesign);   
    DomEventHandler.bindEvent('verifyStep','click',RewardDomEvent.showVerifyAndPay);

    // DomEventHandler.bindEvent('previewBtn1', 'click', RewardDomEvent.closeOpenAccordian);
    // DomEventHandler.bindEvent('previewBtn2', 'click', RewardDomEvent.closeOpenAccordianY);

    // DomEventHandler.bindClassEvent('', 'change', RewardDomEvent.validateEmail);
    $('body').on('click', '.append-amount', function() {
        let amount = $(this).data('amount');
        let id = $(this).data('id');
       $('.input-amount-validate'+id).val(amount);      
    }); 
   // maintaining state
   (function(window) {
    // exit if the browser implements that event
      if ("onhashchange" in window) {  return; }
    
      var location = window.location,
        oldURL = location.href,
        oldHash = location.hash;
      // check the location hash on a 100ms interval
      setInterval(function() {
        var newURL = location.href,
          newHash = location.hash;
       // if the hash has changed and a handler has been bound...
        if (newHash != oldHash && typeof window.onhashchange === "function") {
          // execute the handler
          window.onhashchange({
            type: "hashchange",
            oldURL: oldURL,
            newURL: newURL
          });

          oldURL = newURL;
          oldHash = newHash;
        }
      }, 10);
    })(window);
    
    $(window).on('hashchange', function() {
        var urlHash = window.location.hash;
        if(urlHash == '#Step2'){
        $('#chooseDesign').hide();
        $('#createOrder').hide();
        $('#uploadStep').addClass('active');
        $('#uploadStep').removeClass('done');
        $('#verifyStep').removeClass('active');
        $('#verifyStep').removeClass('done');
            if(Storage.getSessionCollection('bulk') == true){
                $('#addBenefits').hide();
                $('#addBenefitsBulk').show();
            }else{
                $('#addBenefits').show();
                $('#addBenefitsBulk').hide();
            }

        }
        else if(urlHash == '#orderHistory'){
            $(urlHash).trigger( "click" );
        }
        else if(urlHash == '#beneficiaries'){
            $(urlHash).trigger( "click" );
        }
        else if(urlHash == '#sendGift'){
            $(urlHash).trigger( "click" );
            $('#chooseDesign').show();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#chooseStep').removeClass('done');
            $('#uploadStep').removeClass('active');
            $('#uploadStep').removeClass('done');
            Storage.setSessionCollection('bulk',false);
            Storage.setSessionCollection('manual',true);
        }
        else if(urlHash == '#Step3Manual'){
            $('#chooseDesign').hide();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#createOrder').show();
        }
        else if(urlHash == '#Step3Bulk'){
            $('#chooseDesign').hide();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#createOrder').show();
        }
        else{
            var targetUrl = 'sendGift';
            window.history.pushState({url: "" + targetUrl + ""}, targetUrl);
            window.location.hash=targetUrl;
            Storage.setSessionCollection('bulk',false);
            Storage.setSessionCollection('manual',true);
            $('#chooseDesign').show();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#chooseStep').removeClass('done');
            $('#uploadStep').removeClass('active');
            $('#uploadStep').removeClass('done');
            $('#createOrder').hide();
        }
    });



   
    document.getElementById("cutomCardMessage").defaultValue = "Congratulations for completing 3 years with ACME! Thank you for your dedication & contributions along the journey. We wish you continued success with ACME family! In appreciation of your dedicated service, here is a token of recognition.";
    document.getElementById("cutomCardTitle").defaultValue = "Congratulations";
    $('body').on('keyup','.inputFieldValidate',function(){
        let typeOfValidator = $(this).data('validator');
        let allFieldsFilled = false;
        if(typeOfValidator == 'email'){
            let values = ''+$(this).val()+'';
            let typeOfValue = Number($(this).val());
            
            if(Number($(this).val())){
                if(values.length == 10){
                    $(this).removeClass('input-error');
                    $(this).next().css('display','none'); 
                    allFieldsFilled = true;
                }else{
                    $(this).addClass('input-error');
                    $(this).next().css('display','block').html('Invalid Mobile Number'); 
                    allFieldsFilled = false;
                }
            }else{
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/;
                if($(this).val().match(mailformat)){                       
                    $(this).removeClass('input-error');
                    $(this).next().css('display','none'); 
                    allFieldsFilled = true;
                } else{                 
                    $(this).addClass('input-error');
                    $(this).next().css('display','block').html('Invalid Email Address');
                    allFieldsFilled = false;
                }
            }
            
        } else if(typeOfValidator == 'pan'){
            let totalValue = $(this).val();
            if(totalValue.length == 10 || totalValue.length == 12){
                if(totalValue.length == 10){
                    var ObjVal = totalValue;
                    var pancardPattern = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
                    var patternArray = ObjVal.match(pancardPattern);
                    if (patternArray == null) {
                        $(this).addClass('input-error');
                        $(this).next().css('display','block'); 
                        allFieldsFilled = false;  
                    }
                    else{
                    $(this).removeClass('input-error');
                    $(this).next().css('display','none'); 
                    allFieldsFilled = true;
                    }
                }
                else if(totalValue.length == 12){
                    var ObjValA = totalValue;
                    var aadharPattern = /^([0-9]{12})$/;
                    var patternArrayA = ObjValA.match(aadharPattern);

                if (patternArrayA == null) {
                        $(this).addClass('input-error');
                        $(this).next().css('display','block'); 
                        allFieldsFilled = false;  
                    }
                    else{
                    $(this).removeClass('input-error');
                    $(this).next().css('display','none'); 
                    allFieldsFilled = true;
                    }
                }
                
            }else{
                $(this).addClass('input-error');
                $(this).next().css('display','block'); 
                allFieldsFilled = false;
            }
        } else if(typeOfValidator == 'amount'){
            if($(this).val() != ''){
                $(this).removeClass('input-error');
                $(this).next().css('display','none');
                allFieldsFilled = true; 
            }else{
                $(this).addClass('input-error');
                $(this).next().css('display','block'); 
                allFieldsFilled = false;
            }
        } else if(typeOfValidator == 'name'){
            if($(this).val() != ''){
                $(this).removeClass('input-error');
                $(this).next().css('display','none'); 
                allFieldsFilled = true;
            }else{
                $(this).addClass('input-error');
                $(this).next().css('display','block');
                allFieldsFilled = false; 
            }
        }    
        
        if(allFieldsFilled === true){
            $('#is-all-fields-filled').val('true'); 

            if($('#is-all-fields-filled').val() != '' && $('#scheduleTransferDate').val() != '') {
                $('#rewardSingleSelected').removeAttr('disabled');
                $('#rewardSingleSelected').removeClass('btn-disabled');
            }
        }else{
            $('#rewardSingleSelected').attr('disabled','disabled');
            $('#rewardSingleSelected').addClass('btn-disabled');
        }
    });  

    // $('body').on('click','.remove',function() {
    //     $(this).parent().hasClass('rewardBenefitInputDv').remove();
    // });
    
    $('.scheduleTransferDate').datepicker({
        format: "yyyy-mm-dd",
        startDate: new Date(),
        autoclose: true
    })
    //Listen for the change even on the input
   .change(RewardDomEvent.validationForManualTransfer)
   .on('changeDate', RewardDomEvent.validationForManualTransfer);
   
    
    var intId = 1;
    $("#add").click(function() {
        intId++;
        var fieldWrapper = $(`<li class="rewardBenefitInputDv">
        <div class="manualBenefitHead"><span class="dottedLine"></span> 
        <strong class="removeX" style="font-size:  12px !important;text-transform:  uppercase;cursor:  pointer;color: #8c8793;">
        <img src="https://card-program-files.s3.amazonaws.com/hrDashboard/images/close-delete.svg" style="margin-bottom: 2px;margin-right: 3px;"/> Delete</strong>  </div>                                    
            <div class="zeta-col1">
                <input type="text" class="inputField inputFieldValidate input-name-validate" data-validator="name" placeholder="Name">
                <!--  <p>Please fill the name</p> -->
            </div>
            <div class="zeta-col3 " style="width: 23%">
            <div class="input-group tooltip-custom">
            <div class="input-group-addon"> &nbsp; ₹ &nbsp; </div>
                    <input type="number" class="inputField inputFieldValidate input-amount-validate`+(intId)+` custom-border" placeholder="Amount" data-validator="amount" maxlength="8">
                    <span class="tooltiptext rewardTootltip" style="width: 320px;height: 87px">
                        <div class="inner-suggestion-container">                                                
                            <div class="inner-suggestion-p">Suggested Amounts</div>                                                
                            <div class="inner-suggestion-amount">
                                <div class="inner-suggestion-list">
                                    <div class="append-amount" data-amount="500" data-id="`+intId+`">500</div>
                                    <div class="append-amount" data-amount="1000" data-id="`+intId+`">1,000</div>
                                    <div class="append-amount" data-amount="2500" data-id="`+intId+`">2,500</div>
                                    <div class="append-amount" data-amount="5000" data-id="`+intId+`">5,000</div>
                                    <div class="append-amount" data-amount="10000" data-id="`+intId+`">10,000</div>
                                </div>
                            </div>
                        </div>
                    </span>
            </div>
                <!--  <p>Please fill the amount</p> -->
                
            </div>
            <div class="zeta-col4" style="width:22%;">
                                                    <input type="text" class="inputField inputFieldValidate  input-pan-validate" data-validator="pan" placeholder="Aadhar No. or PAN" maxlength="12">
                                                    <!-- <p>Invalid Aadhar No. or PAN</p> -->
                                            </div>
           
            <div class="zeta-col3">
                    <input type="text" class="inputField inputFieldValidate input-email-validate" data-validator="email" placeholder="Email or Mobile Number">
                   <!--   <p>Invalid Email Address</p> -->
            </div>
            <div class="zeta-col4" style="width:49%;">
            <div class="form-group date-picker date-picker-width" style="margin-bottom:unset">
            <div class="input-group" style="width:100%;">
                <input type="text" class="inputField inputFieldValidate input-date-validate form-control datepicker-img scheduleTransferDate" data-validator="date" placeholder="Transfer Date" id="" readonly style="width: 98%;" />
            </div>
            </div>
            <!--  <p>Invalid Date</p> -->
            </div>
        </li>`);
        fieldWrapper.data("idx", intId);
        $("#buildyourform").append(fieldWrapper);
        if(intId < 10){
            $('.benefitAddBtn').show();
        }else{
            $('.benefitAddBtn').hide();
        }
        $('.scheduleTransferDate').datepicker({
            format: "yyyy-mm-dd",
            startDate: new Date(),
            autoclose: true
        })
    });   
    $('body').on('click','.removeX',function() {
        $(this).parent().parent().remove();
        intId--;
        $('.benefitAddBtn').show();
    });
};
let setFlag = function() {
    return dropFlag;
};

export
default {
    init: init,
    setFlag:setFlag,
    resetForm: resetForm,
    validationForCustomOrder:validationForCustomOrder
};
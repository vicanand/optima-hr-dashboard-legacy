import DomEventHandler from '../../common/domEventHandler';
import Utils from '../../common/util';
import downloadPdfInvoiceSvc from '../../service/spotlight/downloadPdfInvoiceSvc';
import pdfMake  from '../../../../node_modules/pdfmake/build/pdfmake.js';
import pdfFonts from '../../common/customFontForPDFGenerator';
import imageConstant from '../../common/constantImagePDF';
import Storage from '../../common/webStorage'
// import pdfFonts from '../../../../node_modules/pdfmake/build/vfs_fonts.js';
// pdfMake.vfs = pdfFonts.pdfMake.vfs;
pdfMake.vfs = pdfFonts;
  
pdfMake.fonts = {
  ProximaNova: {
    normal: 'ProximaNova-Regular.ttf',
    bold: 'ProximaNova-Regular.ttf' 
  },
  Roboto:{
    normal:'Roboto-Regular.ttf',
    bold:'Roboto-Regular.ttf'
  }
}
 
let init = function(){
  $('body').on('click', '.downloadpdfinvoice', function() {
let orderID = $(this).data('orderid');
let requestFrom = $(this).data('from');
let spotlightType = $(this).data('spotlighttype');
let token, companyId, corporateId;

token = Storage.get('authToken');
companyId = $("meta[name='companyID']").attr("content");
corporateId = $("meta[name='corpID']").attr("content");
console.log(orderID,requestFrom);

downloadPdfInvoiceSvc.getSelectGiftingInvoice(token, companyId, corporateId ,orderID,spotlightType,requestFrom).then(function(resp){
    downloadInvoice(resp,requestFrom,orderID);
  },function(error){
    console.log(error);
  }
)});  

 $('body').on('click', '.downloadpointpdfinvoice', function() {
  let orderID = $(this).data('orderid');    
  let token, companyId, corporateId,programID;

  token = Storage.get('authToken');
  companyId = $("meta[name='companyID']").attr("content");
  corporateId = $("meta[name='corpID']").attr("content");
  programID = $('#getProgramID').val();  

    downloadPdfInvoiceSvc.getPointBasedInvoice(token, companyId, corporateId ,orderID,programID).then(function(resp){
        downloadInvoice(resp,'other',orderID);
      },function(error){
        console.log(error);
      }
    )});  
};

function downloadInvoice(respData,requestFrom,orderID){
  var GiftingProgramName;
  let ifi = $("meta[name='ifi']").attr("content");
  if(ifi.toLowerCase() == "hdfc" || ifi.toLowerCase() == "kotak"){
    GiftingProgramName = 'SpotGifting ';
  }
  else{
    GiftingProgramName = 'Zeta ';
  }
console.log(Utils.htmlDecoder(respData));

var kotakAddress = [
  'Kotak Address:',
  'Kotak​ ​Mahindra​ ​Bank​ ​Limited',
  'Gr.Floor, SCO No.2, District centre',
  'Sec.16, Faridabad,',
  'Haryana - 121002'
]

var zetaCompanyName = respData.zetaCompanyName;
var address =  respData.zetaAddress;
var logoPlace;
var companyPhoneContactDetails;
var zetaPhoneNo = respData.zetaPhoneNo;
var zetaEmail = respData.zetaEmail;
var zetaCin = respData.zetaCin;
var zetaGstRegNo = respData.zetaGstRegNo;
var zetaPan= respData.zetaPan;
var corporateCompanyName = respData.corporateCompanyName;
var corporateStreetAddress = respData.corporateStreetAddress;
var corporateCity = respData.corporateCity;
var corporateState = respData.corporateState;
var corporatePinCode = respData.corporatePinCode;
var corporateContactPerson = respData.corporateContactPerson;
var corporateCompanyGst = respData.corporateCompanyGst;
var invoiceId = respData.invoiceId;
var invoiceCreationDate = Utils.timestampToDate(respData.invoiceCreationDate);
var orderId = respData.orderId;
var creationDate = Utils.timestampToDate(respData.creationDate);
var reference = respData.reference;
var poNumber = respData.poNumber;
var orderTypeText = Utils.htmlDecoder(respData.orderTypeText);
var numberOfBooklets = respData.numberOfBooklets;
var totalFaceValue = respData.totalFaceValue/100;
var zetaSac = respData.zetaSac;
var totalDiscount = respData.totalDiscount/100;
var totalCharges = respData.totalCharges/100;
var cGst = respData.cGst/100;
var sGst = respData.sGst/100;
var iGst = respData.iGst/100;
var roundOffAmount = respData.roundOffAmount/100;
var totalAmount = respData.totalAmount/100;
var serviceCharge = respData.serviceCharge;
var ifiName = respData.ifiName;
var invoiceTable;
var signatoryLogo;

if(requestFrom == 'GET_SELECT_GITFTING_INVOICEURL' && respData.ifiName.toLowerCase() == 'kotak'){
  address = kotakAddress;

  logoPlace = {
      width:130,
      image:imageConstant.kotakLogoImage
      }

  companyPhoneContactDetails = [
    [{text:[{text:'\nKotak Bank PAN: ',font:'Roboto',bold:true,fontSize: 9,},{text:'AAACK4409J',style:'companyAddress'}]}],
    [{text:[{text:'Maharashtra GST No.: ',font:'Roboto',bold:true,fontSize: 9,},{text:'06AAACK4409J1ZO',style:'companyAddress'}]}],                 
]

signatoryLogo = {   alignment:'right',
width:150,
image:imageConstant.kotakSignatoryImage
}
}
else if(respData.ifiName.toLowerCase() === 'hdfc'){
  logoPlace = {
    width:130,
    image:imageConstant.hdfcLogoImage
    };

  address= address.replace(/&lt;/gi,'<').replace(/&gt;/gi, ">");
  address = address.replace(/<(.|\n)*?>/g, '');
  address = address.split(',');

  companyPhoneContactDetails = [
    [{text:[{text:'\nPhone: ',font:'Roboto',bold:true,fontSize: 9,},{text:zetaPhoneNo,style:'companyAddress'}]}],
    [{text:[{text:'Email: ',font:'Roboto',bold:true,fontSize: 9,},{text:zetaEmail,style:'companyAddress'}]}],
    [{text:[{text:'CIN: ',bold:true,font:'Roboto',fontSize: 9,},{text:zetaCin,style:'companyAddress'}]}],
    [{text:[{text:'GST No: ',bold:true,font:'Roboto',fontSize: 9,},{text:zetaGstRegNo,style:'companyAddress'}]}],
    [{text:[{text:'PAN: ',bold:true,font:'Roboto',fontSize: 9,},{text:zetaPan,style:'companyAddress'}]}],              
]

signatoryLogo = {   alignment:'right',
width:150,
image:imageConstant.hdfcSignatoryImage
}

}
else if(respData.ifiName.toLowerCase() === 'kotak'){
  logoPlace = {
    width:130,
    image:imageConstant.kotakLogoImage
    };

  address= address.replace(/&lt;/gi,'<').replace(/&gt;/gi, ">");
  address = address.replace(/<(.|\n)*?>/g, '');
  address = address.split(',');

  companyPhoneContactDetails = [
    [{text:[{text:'\nPhone: ',font:'Roboto',bold:true,fontSize: 9,},{text:zetaPhoneNo,style:'companyAddress'}]}],
    [{text:[{text:'Email: ',font:'Roboto',bold:true,fontSize: 9,},{text:zetaEmail,style:'companyAddress'}]}],
    [{text:[{text:'CIN: ',bold:true,font:'Roboto',fontSize: 9,},{text:zetaCin,style:'companyAddress'}]}],
    [{text:[{text:'GST No: ',bold:true,font:'Roboto',fontSize: 9,},{text:zetaGstRegNo,style:'companyAddress'}]}],
    [{text:[{text:'PAN: ',bold:true,font:'Roboto',fontSize: 9,},{text:zetaPan,style:'companyAddress'}]}],              
]

signatoryLogo = {   alignment:'right',
width:150,
image:imageConstant.kotakSignatoryImage
}

}
else{
  logoPlace = {
    width:130,
    image:imageConstant.defaultLogoImage
    };

  address= address.replace(/&lt;/gi,'<').replace(/&gt;/gi, ">");
  address = address.replace(/<(.|\n)*?>/g, '');
  address = address.split(',');

  companyPhoneContactDetails = [
    [{text:[{text:'\nPhone: ',font:'Roboto',bold:true,fontSize: 9,},{text:zetaPhoneNo,style:'companyAddress'}]}],
    [{text:[{text:'Email: ',font:'Roboto',bold:true,fontSize: 9,},{text:zetaEmail,style:'companyAddress'}]}],
    [{text:[{text:'CIN: ',bold:true,font:'Roboto',fontSize: 9,},{text:zetaCin,style:'companyAddress'}]}],
    [{text:[{text:'GST No: ',bold:true,font:'Roboto',fontSize: 9,},{text:zetaGstRegNo,style:'companyAddress'}]}],
    [{text:[{text:'PAN: ',bold:true,font:'Roboto',fontSize: 9,},{text:zetaPan,style:'companyAddress'}]}],              
]

signatoryLogo = {   alignment:'right',
width:150,
image:imageConstant.defaultSignatoryImage
}

}
     
if(corporateState.toLowerCase() === 'maharashtra'){  
invoiceTable = requestFrom == 'GET_SELECT_GITFTING_INVOICEURL' ? [
  [{text:'Product',margin:[0,5,0,5],bold:true,font:'Roboto',fontSize:10}, {text:'SAC',bold:true,font:'Roboto',fontSize:10,margin:[0,5,0,5]},
  {text:'Quantity',margin:[0,5,0,5],bold:true,font:'Roboto',fontSize:10,alignment:'right'},{text:'Amount (₹)',margin:[0,5,0,5],alignment:'right',fontSize:10,bold:true,font:'Roboto'}],
  
  [{text:'Zeta'+orderTypeText,style:'innerRowStyle'},{text: '',style:'innerRowStyle'}, {text: numberOfBooklets,style:'innerRowStyle',alignment:'right'}, 
  {text: totalFaceValue,style:'innerRowStyle',alignment:'right'}],
  
  ['','',{rowspan: 3,text:'Sub Total        ',alignment:'right',fontSize:10,margin:[0,5,-40,5]},
  {text:totalFaceValue,alignment:'right',fontSize:10,margin:[0,5,0,5]}],
  
  ['','',{	rowspan: 3,text:'Discount        ',alignment:'right',fontSize:10,margin:[0,5,-40,5]},
  {text:'-'+totalDiscount,alignment:'right',fontSize:10,margin:[0,5,0,5]}],
  
  [{text:'Service charges',style:'innerRowStyle'},{text: zetaSac,style:'innerRowStyle'}, {text: '',style:'innerRowStyle'}, {text: serviceCharge,style:'innerRowStyle',alignment:'right'}],

  [{text:'CGST @ 9% on Service Charges',style:'innerRowStyle'},{text: '',style:'innerRowStyle'}, {text: '',style:'innerRowStyle'}, {text: cGst,style:'innerRowStyle',alignment:'right'}],
  
  [{text:'SGST @ 9% on Service Charges',style:'innerRowStyle'},{text: '',style:'innerRowStyle'}, {text: '',style:'innerRowStyle'}, {text: sGst,style:'innerRowStyle',alignment:'right'}],
    
  ['','',{	rowspan: 3,text:'Round Off        ',alignment:'right',fontSize:10,margin:[0,5,-40,5]},
  {text:roundOffAmount,alignment:'right',fontSize:10,font:'Roboto',bold:true,margin:[0,5,0,5]}],
  
  ['','',{	rowspan: 3,text:'Total        ',bold:true,alignment:'right',fontSize:10,font:'Roboto',margin:[0,5,-40,5]},
  {	text:totalAmount,bold:true,alignment:'right',fontSize:10,font:'Roboto',margin:[0,5,0,5]}]
] : [
  [{text:'Product',margin:[0,5,0,5],bold:true,font:'Roboto',fontSize:10}, {text:'SAC',bold:true,font:'Roboto',fontSize:10,margin:[0,5,0,5]},
  {text:'Quantity',margin:[0,5,0,5],bold:true,font:'Roboto',fontSize:10,alignment:'right'},{text:'Amount (₹)',margin:[0,5,0,5],alignment:'right',fontSize:10,bold:true,font:'Roboto'}],
  
  [{text:'Zeta'+orderTypeText,style:'innerRowStyle'},{text: '',style:'innerRowStyle'}, {text: numberOfBooklets,style:'innerRowStyle',alignment:'right'}, 
  {text: totalFaceValue,style:'innerRowStyle',alignment:'right'}],
  
  ['','',{rowspan: 3,text:'Sub Total        ',alignment:'right',fontSize:10,margin:[0,5,-40,5]},
  {text:totalFaceValue,alignment:'right',fontSize:10,margin:[0,5,0,5]}],
      
  [{text:'Service charges',style:'innerRowStyle'},{text: zetaSac,style:'innerRowStyle'}, {text: '',style:'innerRowStyle'}, {text: serviceCharge,style:'innerRowStyle',alignment:'right'}],

  [{text:'CGST @ 9% on Service Charges',style:'innerRowStyle'},{text: '',style:'innerRowStyle'}, {text: '',style:'innerRowStyle'}, {text: cGst,style:'innerRowStyle',alignment:'right'}],
  
  [{text:'SGST @ 9% on Service Charges',style:'innerRowStyle'},{text: '',style:'innerRowStyle'}, {text: '',style:'innerRowStyle'}, {text: sGst,style:'innerRowStyle',alignment:'right'}],
    
  ['','',{	rowspan: 3,text:'Round Off        ',alignment:'right',fontSize:10,margin:[0,5,-40,5]},
  {text:roundOffAmount,alignment:'right',fontSize:10,font:'Roboto',bold:true,margin:[0,5,0,5]}],
  
  ['','',{	rowspan: 3,text:'Total        ',bold:true,alignment:'right',fontSize:10,font:'Roboto',margin:[0,5,-40,5]},
  {	text:totalAmount,bold:true,alignment:'right',fontSize:10,font:'Roboto',margin:[0,5,0,5]}]
]
 } else {
invoiceTable = requestFrom == 'GET_SELECT_GITFTING_INVOICEURL' ? [
  [{text:'Product',margin:[0,5,0,5],bold:true,font:'Roboto',fontSize:10}, {text:'SAC',bold:true,fontSize:10,font:'Roboto',margin:[0,5,0,5]},
  {text:'Quantity',margin:[0,5,0,5],bold:true,font:'Roboto',fontSize:10,alignment:'right'},{text:'Amount (₹)',fontSize:10,margin:[0,5,0,5],alignment:'right',bold:true,font:'Roboto'}],
  
  [{text:GiftingProgramName+orderTypeText,style:'innerRowStyle'},{text: '',style:'innerRowStyle'}, {text: numberOfBooklets,style:'innerRowStyle',alignment:'right'}, 
  {text: totalFaceValue,style:'innerRowStyle',alignment:'right'}],
  
  ['','',{rowspan: 3,text:'Sub Total        ',alignment:'right',fontSize:10,margin:[0,5,-40,5]},
  {text:totalFaceValue,alignment:'right',fontSize:10,margin:[0,5,0,5]}],
  
  ['','',{	rowspan: 3,text:'Discount        ',alignment:'right',fontSize:10,margin:[0,5,-40,5]},
  {text:'-'+totalDiscount,alignment:'right',fontSize:10,margin:[0,5,0,5]}],
  
  [{text:'Service charges',style:'innerRowStyle'},{text: zetaSac,style:'innerRowStyle'}, {text: '',style:'innerRowStyle'}, {text: serviceCharge,style:'innerRowStyle',font:'Roboto',alignment:'right'}],

  [{text:'IGST @ 18% on service charges',style:'innerRowStyle'},{text: '',style:'innerRowStyle'},{text: '',style:'innerRowStyle'}, {text: iGst,style:'innerRowStyle',font:'Roboto',alignment:'right'}],
    
  ['','',{	rowspan: 3,text:'Round Off        ',alignment:'right',fontSize:10,margin:[0,5,-40,5]},
  {text:roundOffAmount,alignment:'right',fontSize:10,font:'Roboto',bold:true,margin:[0,5,0,5]}],
  
  ['','',{	rowspan: 3,text:'Total        ',bold:true,alignment:'right',fontSize:10,font:'Roboto',margin:[0,5,-40,5]},
  {	text:totalAmount,bold:true,alignment:'right',fontSize:10,font:'Roboto',margin:[0,5,0,5]}]] : [
    [{text:'Product',margin:[0,5,0,5],bold:true,font:'Roboto',fontSize:10}, {text:'SAC',bold:true,fontSize:10,font:'Roboto',margin:[0,5,0,5]},
    {text:'Quantity',margin:[0,5,0,5],bold:true,font:'Roboto',fontSize:10,alignment:'right'},{text:'Amount (₹)',fontSize:10,margin:[0,5,0,5],alignment:'right',bold:true,font:'Roboto'}],
    
    [{text:GiftingProgramName+orderTypeText,style:'innerRowStyle'},{text: '',style:'innerRowStyle'}, {text: numberOfBooklets,style:'innerRowStyle',alignment:'right'}, 
    {text: totalFaceValue,style:'innerRowStyle',alignment:'right'}],
    
    ['','',{rowspan: 3,text:'Sub Total        ',alignment:'right',fontSize:10,margin:[0,5,-40,5]},
    {text:totalFaceValue,alignment:'right',fontSize:10,margin:[0,5,0,5]}],    
    
    [{text:'Service charges',style:'innerRowStyle'},{text: zetaSac,style:'innerRowStyle'}, {text: '',style:'innerRowStyle'}, {text: serviceCharge,style:'innerRowStyle',font:'Roboto',alignment:'right'}],
  
    [{text:'IGST @ 18% on service charges',style:'innerRowStyle'},{text: '',style:'innerRowStyle'},{text: '',style:'innerRowStyle'}, {text: iGst,style:'innerRowStyle',font:'Roboto',alignment:'right'}],
      
    ['','',{	rowspan: 3,text:'Round Off        ',alignment:'right',fontSize:10,margin:[0,5,-40,5]},
    {text:roundOffAmount,alignment:'right',fontSize:10,font:'Roboto',bold:true,margin:[0,5,0,5]}],
    
    ['','',{	rowspan: 3,text:'Total        ',bold:true,alignment:'right',fontSize:10,font:'Roboto',margin:[0,5,-40,5]},
    {	text:totalAmount,bold:true,alignment:'right',fontSize:10,font:'Roboto',margin:[0,5,0,5]}]]
 }



var companyAddressFull = [
  {text:zetaCompanyName+'\n',bold: true,fontSize: 9,alignment: 'left',lineHeight: 1.3,font:'Roboto'},
];

for(var l=0;l<address.length;l++){
  companyAddressFull.push({text:address[l]+'\n',style:'companyAddress'});
}





 
  var dd = {
    footer: function(page, pages) { 
    return { 
        text:'This is a system generated invoice. This payment does not entail any TDS since the value only indicates the face value of the electronic gift vouchers which are not "goods or services" but exchangeable prepaid payment instruments. There is no other charge/ service fee levied on provision of these instruments.',
        margin: [40,-20,10,0],
        fontSize:8,
        color:'gray'
    };
  },
  content: [
      {
      columns: [
        logoPlace,
      {
      width: 200,
      text: companyAddressFull,
      style: 'subheader'
        },
      {
      style: 'tableExample',
      table: {
        body: companyPhoneContactDetails
      },
      layout: 'noBorders'
    }
      ]
    },
    {
        margin:[-4,20,0,-5],
        text:'TAX INVOICE',bold: true,font:'Roboto',fontSize:26,alignment: 'left',lineHeight: 1.3
    },
    {
        columns:[
            {
      style: 'tableExample',
      table: {
        body: [
          [{text:corporateCompanyName,bold:true,font:'Roboto'},],
          [{text:corporateStreetAddress,style:'buyerAddress'}],
          [{text:corporateStreetAddress,style:'buyerAddress'}],
          [{text:corporateCity,style:'buyerAddress'}],
          [{text:corporateState +'-'+corporatePinCode,style:'buyerAddress'}],
          [{text:[{text:'State: ' ,bold:true,font:'Roboto',fontSize:10},{text:corporateState,style:'buyerAddress'}],	margin:[0,10,0,0],}],
          [{text:[{text:'GST No: ',bold:true,font:'Roboto',fontSize:10},{text:corporateCompanyGst,style:'buyerAddress'}]}],
          [{text:[{text:'Place of Supply: ',bold:true,font:'Roboto',fontSize:10},{text:'',style:'buyerAddress'}]}],
          [{text:[{text:'\nContact: ',bold:true,font:'Roboto',fontSize:10},{text:corporateContactPerson,style:'buyerAddress'}]}],
        ]
      },
      layout:'noBorders'
    },
            {
      margin:[90,10,0,0],
      style: 'tableExample',
      alignment:'left',
      table: {
        body: [
              [{text:[{text:'Invoice No: ',bold:true,font:'Roboto',fontSize:10},{text:invoiceId,style:'buyerDetails'}]}],  
              [{text:[{text:'Invoice Date: ',bold:true,font:'Roboto',fontSize:10},{text:invoiceCreationDate,style:'buyerDetails'}]}],  
              [{text:[{text:'\nOrder No: ',bold:true,font:'Roboto',fontSize:10},{text:orderId,style:'buyerDetails'}]}],  
              [{text:[{text:'Order Date: ',bold:true,font:'Roboto',fontSize:10},{text:creationDate,style:'buyerDetails'}]}],  
              [{text:[{text:'\nReference: ',bold:true,font:'Roboto',fontSize:10},{text:reference,style:'buyerDetails'}]}],  
              [{text:[{text:'PO Number: ',bold:true,font:'Roboto',fontSize:10},{text:poNumber,style:'buyerDetails'}]}],  
        ]
      },
      layout:'noBorders'
            }
            ],
    },
    {
      text:'\n'  
    },
    {
      style: 'tableExample',
      table: {
        widths: [200, '*', 100, 100],
        body: invoiceTable
      },
      layout: {
        hLineWidth: function (i, node) {
          return (i === 0 || i === node.table.body.length) ? 2 : 1;
        },
        vLineWidth: function (i, node) {
          return (i === 0 || i === node.table.widths.length) ? 2 : 0;
        },
        hLineColor: function (i, node) {
          return (i === 0 || i === node.table.body.length) ? '#fff' : '#e6e6e6';
        },
        vLineColor: function (i, node) {
          return (i === 0 || i === node.table.widths.length) ? '#fff' : '#e6e6e6';
        }
      }
    },
    {
        text:'\n'
    },
    signatoryLogo
  ],
  styles:{
      companyAddress:{fontSize: 9,alignment: 'left',lineHeight:1.3,color:'#5a5a5a'},
      buyerAddress:{fontSize: 10,alignment: 'left',color:'#5a5a5a'},
       buyerDetails:{fontSize: 10,alignment: 'left',color:'#5a5a5a'},
      innerRowStyle:{color: '#000',fillColor:'#f7f7f7',margin:[0,5,0,5],fontSize:10}
  },
    defaultStyle: {
    columnGap: 10,
    font:'ProximaNova'
  }
  }

  try{
    pdfMake.createPdf(dd).download('Invoice_orderID_'+ orderID);  
  }catch(error){
    console.log(error);
  }
  
}

export 
default {
  init:init
};
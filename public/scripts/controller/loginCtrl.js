import DomEventHandler from '../common/domEventHandler';
import LoginDOMEvents from '../dom-events/loginDOMEvents';
export
default {
    init: function() {

        LoginDOMEvents.init()
        DomEventHandler.bindEvent('emailID', 'keyup', LoginDOMEvents.emailIdChange);
        DomEventHandler.bindEvent('emailNext', 'click', LoginDOMEvents.emailNextClick);
        //DomEventHandler.bindEvent('signUp', 'click', LoginDOMEvents.signUpLink);
        DomEventHandler.bindEvent('login', 'click', LoginDOMEvents.loginToOldDashboard);
        DomEventHandler.bindClassEvent('list-group-item', 'click', LoginDOMEvents.selectCorporate);
        DomEventHandler.bindEvent('corporateSwitch', 'click', LoginDOMEvents.corporateSwitchWhileLogin);
    }
}

import DomEventHandler from '../common/domEventHandler';
import HeaderDOMEvents from '../dom-events/headerDOMEvents';
import Constants from '../common/constants';
import Storage from '../common/webStorage';
import CacheDataStorage from '../common/cacheDataStorage';
import Env from '../common/env';
import ConfigLoader from '../common/countryConfig/configLoader';
export
default {
    init: function() {
        DomEventHandler.bindEvent('currentPassword', 'keyup', HeaderDOMEvents.validate);
        DomEventHandler.bindEvent('newPassword', 'keyup', HeaderDOMEvents.validate);
        DomEventHandler.bindEvent('retypeNewPassword', 'keyup', HeaderDOMEvents.validate);
        DomEventHandler.bindEvent('changePasswordBtn', 'click', HeaderDOMEvents.changePasswordBtn);
        DomEventHandler.bindEvent('dropDown', 'click', HeaderDOMEvents.dropDownSelect);
        DomEventHandler.bindClassEvent('list-group-item', 'click', HeaderDOMEvents.selectCorporates);
        DomEventHandler.bindEvent('switchCorporate', 'click', HeaderDOMEvents.corporateSwitch);
        DomEventHandler.bindClassEvent('bussiness-list-pd', 'click', HeaderDOMEvents.companySwitch);
        DomEventHandler.bindClassEvent('sideMenuNav', 'click', HeaderDOMEvents.sideMenuNavigation);
        DomEventHandler.bindClassEvent('select-programs', 'click', HeaderDOMEvents.selectGiftingLandingEvents);
        DomEventHandler.bindEvent('new_gift_order', 'click', HeaderDOMEvents.newGiftOrder);
        DomEventHandler.bindEvent('spotlightLearnMore', 'click', HeaderDOMEvents.spotlightLearnMore);
        DomEventHandler.bind(window, 'message', HeaderDOMEvents.recieveMessage);
        //DomEventHandler.bindEvent('kyc_learn_more', 'click', HeaderDOMEvents.kycLearnMore);
        DomEventHandler.bindEvent('phCompanySearch', 'keyup', HeaderDOMEvents.searchBussiness);
        this.initConfigs();
    },
    initConfigs: function () {
        // Set role cache
        const getRoles = $('#rb-access-data').val();
        const companies = $('#companies-data').val();
        if (getRoles) {
            CacheDataStorage.setItem(Constants.ROLES_CONFIG, JSON.parse(getRoles));
        }
        if (companies) {
            CacheDataStorage.setItem(Constants.COMPANIES, JSON.parse(companies));
        }
        ConfigLoader.initConfigLoader((err) => {
            if (err) {
                console.log(err);
            }
        })
    }
}
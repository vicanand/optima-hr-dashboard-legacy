import EmployeeSvc from '../../service/express/employeeSvc';
import Utils from '../../common/util';

class AvgCustomerSpend {
    constructor(element, options) {
        this.options = options;
        this.loadGraph();
        this.loadDatePicker();
        this.options.filters.startDate = moment();
        this.options.filters.endDate = moment().add(30, 'days');
    }


    triggerRefresh(filters) {
        this.options.filters.store = filters.store;
        this.options.filters.office = filters.office;
        this.options.filters.city = filters.city;
        // this.options.filters = filters;
        // this.options.filters.startDate = moment();
        // this.options.filters.endDate = moment().add(30, 'days');
        this.loadGraph();
    }


    loadGraph() {
        $("#avgCustomerSpendLoading").show();
        $("#avgCustomerSpend").hide();
        let requestParams = {
            fromDate: this.options.filters.startDate.format('YYYY-MM-DD'),
            toDate: this.options.filters.endDate.format('YYYY-MM-DD'),
            businessID: this.options.filters.office,
            storeID: this.options.filters.store
        };

        $('#avgCustomerError').hide();
        $('#avgCustomerEmpty').hide();
        EmployeeSvc.getEmployeeInsightsTransactionDetails(requestParams).then((respData) => {
            $("#avgCustomerSpendLoading").hide();
            if (respData.length)
            {
                this.renderGraph(respData);
                $("#avgCustomerSpend").show();
            }
            else
            {
                $('#avgCustomerEmpty').show();
                $("#avgCustomerSpend").hide();
            }
        }, function () {
            $('#avgCustomerError').show();
            $("#avgCustomerSpendLoading").hide();
        });
    }


    loadDatePicker() {
        $('#avgSpendDatePicker').daterangepicker({
            "autoApply": true,
            "dateLimit": {
                "days": 30
            },
            "startDate": this.options.filters.startDate,
            "endDate": this.options.filters.endDate,
            "opens": "left",
            "locale": {
                "format": "DD MMM, YYYY",
            }
        }, (start, end, label) => {
            this.options.filters.startDate = start;
            this.options.filters.endDate = end;
            this.loadGraph();
        });
    }


    renderGraph(data) {

        // let graphData = _.map(data, (item) => {
        //     return [item.shortDateFormatted, item.averageSpent];
        // });
        //
        //
        // console.log(graphData);

        let categories = _.map(data, (item) => item.shortDateFormatted);
        let graphValues =  _.map(data, (item) => item.averageSpent);




        Highcharts.chart('avgCustomerSpend', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    // rotation: -45,
                    style: {
                        fontSize: '12px',
                    }
                },
                categories: categories
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    formatter: function () {
                        return '₹' + this.axis.defaultLabelFormatter.call(this);
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                // pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>'
                formatter: function () {
                    return `${this.x}<br/>Spend Amount: <b>₹${Utils.formatINR(this.y)}</b>`;
                },
                enabled: true
            },
            series: [{
                name: 'Stores',
                data: graphValues,
                color: '#633ea5'

            }]
        });
    }
}


export default AvgCustomerSpend;
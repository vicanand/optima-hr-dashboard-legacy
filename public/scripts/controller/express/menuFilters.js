import MultipleDropdown from '../../widgets/multipleDropdown';

class MenuFilters {
    constructor(element, options = {}) {

        this.options = options;
        this.list = JSON.parse($('#citiesList').html());


        this.cityPicker = new MultipleDropdown('#cityPicker', {
            isMultiple: false,
            data: [],
            label: 'City',
            onChange: () => {
                this.loadOffices();
                this.loadStores();
                this.triggerChange();
            }
        });

        this.officePicker = new MultipleDropdown('#officePicker', {
            isMultiple: false,
            data: [],
            label: 'Office',
            onChange: () => {
                this.loadStores();
                this.triggerChange();
            }
        });


    
        this.storePicker = new MultipleDropdown('#storePicker', {
            isMultiple: this.options.isStoreMultiple,     
            data: [],
            label: 'Stores',
            defaultLabel: 'All stores',
            allSelectedLabel: 'All stores',
            pluralLabel: 'Stores',
            onChange: () => {
                this.triggerChange();
            }
        });

        this.loadCities();
        this.loadOffices();
        this.loadStores();
        this.triggerChange();
    }


    loadCities() {
        let cities = _.uniqBy(this.list, function (item) {
            return item.city;
        });

        cities = _.map(cities, function (item) {
            return {
                label: item.city,
                value: item.city
            };
        });
        this.cityPicker.reset(cities);
    }

    loadOffices() {
        const city = this.cityPicker.getValue();

        let offices = _.filter(this.list, function (item) {
            return item.city == city;
        });

        offices = _.uniqBy(offices, function (item) {
            return item.office;
        });

        offices = _.map(offices, function (item) {
            return {
                label: item.office,
                value: item.office
            };
        });

        this.officePicker.reset(offices);
    }

    loadStores() {    
        const city = this.cityPicker.getValue(),
            office = this.officePicker.getValue();

        let offices = _.filter(this.list, function (item) {
            console.log("loadStores",item);
            return item.city == city && item.office == office;
        });

        offices = _.map(offices,  (item) => {
            
            let items = {
                label: item.storeName,
                value: item.businessID + ':' + item.storeID
            };

            if (this.options.isStoreMultiple)
            {
                items.checked = true;
            }
            
            return items;
            
        });

        this.storePicker.reset(offices);
    }


    triggerChange() {
        let stores = this.storePicker.getValue();

        if (this.options.isStoreMultiple && stores.length == 0)
        {
            const city = this.cityPicker.getValue(),
            office = this.officePicker.getValue();

            let offices = _.filter(this.list, function (item) {
                console.log('triggerChange: ', item);
                return item.city == city && item.office == office;
            });

            offices = _.map(offices, (item) => item.storeID);    
            stores = offices;
        }



        let responseData = {
            city: this.cityPicker.getValue(),
            office: _.split(stores, ':')[0],
            store: _.split(stores, ':')[1],
        };

        
        if (this.options.onChange) {
            this.options.onChange(responseData);
        }

    }
}


export default MenuFilters;
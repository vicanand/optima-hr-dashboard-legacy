import Component from './component';
import SalesSvc from '../../service/express/salesSvc';
import vendorInsightsSnapShotTemplate from '../../../template/section/express/subviews/vendorInsightSnapShot.ejs';



class vendorInsightsSnapShot extends Component {
    constructor(element, options = {}) {
        super();
        
       
        this.options = options;
        this.options.filters = this.options.filters  || {};
        this.options.filters.startDate = moment().subtract(90, 'days');
        this.options.filters.endDate = moment();

        this.loadDatePicker();
        this.loadStats();
    }

    triggerReload(filters) {
        // this.options.filters.city = filters.city;
        // this.options.filters.office = filters.office;
        // this.options.filters.stores = filters.stores;

        this.options.filters.startDate = moment();
        this.options.filters.endDate = moment().add(30, 'days');
        this.loadStats();
    }

    loadStats() {
        $('#vendorInsightsSnapShotLoading').show();
        $('#vendorInsightsSnapShotContainer').show();
        $('#vendorInsightsSnapShotError').hide();


        let requestParams = {
            // businessID: this.options.filters.office,
            fromDate: this.options.filters.startDate.format('YYYY-MM-DD'),
            toDate: this.options.filters.endDate.format('YYYY-MM-DD'),
            businessID: this.readCookie('businessID')
            // stores: this.options.filters.stores                
        };


        SalesSvc.getVendorInsightsSnapShot(requestParams).then((respData) => {

            console.log("respData",respData);
            // let pageData = {
            //     totalTransactionsCount: _.sumBy(respData.transactionDetails, "transactionCount"),
            //     totalTransactionAmt: _.sumBy(respData.transactionDetails, "totalSum")
            // };


            $('#vendorInsightsSnapShotLoading').hide();
            this.renderStats(respData);  
        }, function () {
            $('#vendorInsightsSnapShotContainer').hide();
            $('#vendorInsightsSnapShotLoading').hide();
            $('#vendorInsightsSnapShotError').show();
        });
    }

    loadDatePicker() {
        $('#vendorInsightsSnapShotDatePicker').daterangepicker({
            "autoApply": true,
            // "dateLimit": {
            //     "days": 90
            // },
            "startDate": this.options.filters.startDate,
            "endDate": this.options.filters.endDate,
            "opens": "left",
            "locale": {
                "format": "DD MMM, YYYY",
            }
        }, (start, end, label) => {
            this.options.filters.startDate = start;
            this.options.filters.endDate = end;
            this.loadStats();
        });
    }

    renderStats(data) {      
        data.mostPopularStore = data.mostPopularStore || "NA";       
        data.bestSellingItem = data.bestSellingItem || "NA";       
        
                         
        console.log("statesssss", data);
        this.renderTemplate('#vendorInsightsSnapShotContainer', vendorInsightsSnapShotTemplate, data);
    }
}

export default vendorInsightsSnapShot;      
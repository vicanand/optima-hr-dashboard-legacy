import Component from './component';
import SalesSvc from '../../service/express/salesSvc';
import SalesAmtAndTransTemplate from '../../../template/tables/express/SalesAmtAndTrans.ejs';
import ExpressTable from '../../widgets/expressTable';
import Utils from '../../common/util';

class SalesAmtAndTransaction extends Component {
    constructor(element, options) {
        super();
        this.options = options;
        this.options.filters.startDate = this.options.filters.startDate ||moment().subtract(30, 'days');
        this.options.filters.endDate = this.options.filters.endDate ||moment();

        this.loadDatePicker();
        this.loadTable();
    }

    triggerReload(filters) {
        this.options.filters.city = filters.city;
        this.options.filters.office = filters.office;
        this.options.filters.store = filters.store;
        // this.options.filters = filters;
        // this.options.filters.startDate = moment();
        // this.options.filters.endDate = moment().add(30, 'days');
        this.transactionTable.triggerRefresh(this.options.filters);
    }

    loadTable() {
        this.transactionTable = new ExpressTable('#transactionsTable', {
            tableTemplate: SalesAmtAndTransTemplate,
            source: SalesSvc.getSalesTable,
            onloadStarted: function () {
                $("#transactionGraphLoading").show();
                $('#transactionGraph').hide();
            },
            onLoadCompleted: (respData) => {
                $("#transactionGraphLoading").hide();
                if (respData.records.length > 0) {
                    this.loadChart(respData);
                    $('#transactionGraph').show();
                }
            },
            onLoadFailed: () => {
                $("#transactionGraphLoading").hide();
                $('#transactionGraph').hide();
            }
        }, this.options.filters);
    }

    loadDatePicker() {
        $('#salesAmtDatePicker').daterangepicker({   
            "autoApply": true,
            // "dateLimit": {
            //     "days": 30
            // },
            "startDate": this.options.filters.startDate,
            "endDate": this.options.filters.endDate,
            "opens": "left",
            "locale": {
                "format": "DD MMM, YYYY",
            }
        }, (start, end, label) => {
            this.options.filters.startDate = start;
            this.options.filters.endDate = end;
            this.options.filters.businessID =  this.options.filters.office;             
            this.transactionTable.triggerRefresh(this.options.filters);         
        });        
    }


    loadChart(data) {

        Highcharts.chart('transactionGraph', {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: ''
            },
            xAxis: [{
                categories: _.map(data.records, (item) => item.shortFormatedDate),
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '₹{value}',
                    style: {
                        color: '#838383'
                    }
                },
                title: {
                    text: 'Sales Amount',
                    style: {
                        color: '#000'
                    }
                },
            }, { // Secondary yAxis
                title: {
                    text: 'Sales Quantity',
                    style: {
                        color: '#000'
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: '#838383'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
                // formatter: function () {
                //     console.log(this);
                // }
            },
            series: [{
                name: 'Sales Quantity ',
                type: 'column',
                yAxis: 1,
                data: _.map(data.records, (item) => item.transactionCount),
                color: '#633ea5',
                tooltip: {

                }

            }, {
                name: 'Sales Amount',
                type: 'line',
                data: _.map(data.records, (item) => item.amount),
                color: '#eb3b8c',
                tooltip: {
                    valuePrefix: '₹',
                }
            }]
        });
    }
}

export default SalesAmtAndTransaction;
import VendorSvc from "../../service/express/vendorSvc";
import Dropdown from "../../widgets/singleDropdown";
import ExpressTable from '../../widgets/expressTable';
import VendorTemplate from '../../../template/tables/express/vendors.ejs';

export default {
    init: function () {
        google.charts.load('current', {'packages': ['bar']});
        google.charts.setOnLoadCallback(this.loadCharts);
        // Dropdown.init()
        this.loadTable();
    },

    loadCharts: function () {

        var data = new google.visualization.arrayToDataTable([
            ['Vendorss', 'Distance'],
            ['AA', 8000],
            ['AB', 24000],
            ['BB', 30000],
            ['CC', 5000],
            ['CD', 60000]
        ]);

        var options = {
            width: "100%",
            bars: 'vertical', // Required for Material Bar Charts.
            legend: {position: "none"},
            colors: ["#8446e3", '#6335aa'],
            vAxis: {
                title: 'Rating (scale of 1-10)'
            }
        };


        var chart = new google.charts.Bar(document.getElementById('chart_div'));
        var chart1 = new google.charts.Bar(document.getElementById('chart_div1'));
        chart.draw(data, options);
        chart1.draw(data, options);
    },

    loadTable: function () {

        var table = new ExpressTable('#vendorsTable', {
            tableTemplate: VendorTemplate,
            source: VendorSvc.getVendors,
        });


        var dropDown = new Dropdown('#sortVendor', {
            data : [
                {
                    label: 'Top',
                    value: 'top'
                },
                {
                    label: 'Bottom',
                    value: 'bottom'
                }
            ],
            onSelected: function () {

            }
        });

        // $("#vendorsTable").table({
        //     source: VendorSvc.getVendors,
        //     pageSize: 10,
        //     pageNumber: 1,
        //     tableTemplate: "/template/tables/express/vendors.ejs",
        //     rowTemplate: "/template/tables/express/vendors-table-row.ejs",
        //     extraParam: {}
        // });


        // let dropDown = $("#dropdown").dropdown({
        //     label: 'Stores',
        //     singleItemLabel: 'Store',
        //     multipleItemLabel: 'Stores',
        //     onSelected: function (e, value) {
        //         console.log(value);
        //     }
        // });


    }
};

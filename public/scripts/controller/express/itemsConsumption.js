import Component from "./component";
import VendorSvc from "../../service/express/vendorSvc";
import Dropdown from "../../widgets/singleDropdown";
import ExpressTable from '../../widgets/expressTable';
import ItemsConsumptionTemplate from '../../../template/tables/express/itemConsumptions.ejs';


class ItemsConsumption extends Component {
    constructor(element, options) {
        super(element);
        this.element = element;
        this.options = options;
        this.fromDate = moment().subtract(30, 'days');
        this.toDate = moment();

        this.loadDatePicker();
        this.loadTable();
        this.registerDomEvents();
        this.data = {};
    }


    registerDomEvents() {
        $('#excelDownload').on('click', () => {
            this.excelDownload();
        });
    }

    excelDownload() {

        let dataString;

        // Example data given in question text
        let data = [];


        data.push(['Category', 'Item', 'Quantity', 'Sale Amount']);

        _.forEach(this.data.itemTransactionDetailMap, function (category, key) {
            _.forEach(category, function (item) {
                data.push([item.categoryName, item.itemName, item.saleQuantity, item.saleAmount / 100]);
            });
        });


        // Building the CSV from the Data two-dimensional array
        // Each column is separated by ";" and new line "\n" for next row
        var csvContent = '';
        data.forEach(function (infoArray, index) {
            dataString = infoArray.join(',');
            csvContent += index < data.length ? dataString + '\n' : dataString;
        });

        // The download function takes a CSV string, the filename and mimeType as parameters
        // Scroll/look down at the bottom of this snippet to see how download is called
        var download = function (content, fileName, mimeType) {
            var a = document.createElement('a');
            mimeType = mimeType || 'application/octet-stream';

            if (navigator.msSaveBlob) { // IE10
                navigator.msSaveBlob(new Blob([content], {
                    type: mimeType
                }), fileName);
            } else if (URL && 'download' in a) { //html5 A[download]
                a.href = URL.createObjectURL(new Blob([content], {
                    type: mimeType
                }));
                a.setAttribute('download', fileName);
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
            } else {
                location.href = 'data:application/octet-stream,' + encodeURIComponent(content); // only this mime type is supported
            }
        }

        download(csvContent, 'dowload.csv', 'text/csv;encoding:utf-8');
    }


    loadDatePicker() {
        $('#datePicker').daterangepicker({
            "autoApply": true,
            // "dateLimit": {
            //     "days": 90
            // },
            "startDate": this.fromDate,
            "endDate": this.toDate,
            "opens": "left",
            "locale": {
                "format": "DD MMM, YYYY",
            }
        }, (start, end, label) => {
            this.fromDate = start;
            this.toDate = end;
            this.loadTable();
        });
    }


    loadTable() {
        $("#loadingBox").show();
        $("#errorBox").hide();
        $("#emptyBox").hide();
        $("#vendorsTable").show();

        let filters = {
            fromDate: this.fromDate,
            toDate: this.toDate,
            businessID: this.options.filters.office,
            storeID: this.options.filters.store
        };

        $("#excelDownload").hide();
        VendorSvc.getItemConsumptions(filters).then((data) => {
            this.data = data;

            $("#loadingBox").hide();

            if (_.size(data.itemTransactionDetailMap) == 0) {
                $("#emptyBox").show();
                $("#vendorsTable").html('');
            }
            else {
                this.renderTemplate('#vendorsTable', ItemsConsumptionTemplate, data);
                $("#excelDownload").show();
            }
        }, function () {
            $("#loadingBox").hide();
            $("#errorBox").show();
        });
    }


    triggerRefresh(options) {
        this.options.filters.office = options.office;
        this.options.filters.store = options.store;
        this.loadTable();
    }
}

export default ItemsConsumption;
import Component from './component';
import SalesSvc from '../../service/express/salesSvc';
import SnapTemplate from '../../../template/section/express/subviews/salesSnap.ejs';
import SalesFootfall from '../../controller/express/salesFootfall';
import SalesAmtAndTransaction from '../../controller/express/salesAmtAndTransaction';
class VendorSalesSnap extends Component {
    constructor(element, options = {}) {
        super();
        this.options = options;
        this.options.filters.startDate = moment().subtract(90, 'days');
        this.options.filters.endDate = moment();

        this.loadDatePicker();
        this.loadStats(); 
    }

    triggerReload(filters) {
        this.options.filters.city = filters.city;
        this.options.filters.office = filters.office;
        this.options.filters.store = filters.stores;

        // this.options.filters.startDate = moment();
        // this.options.filters.endDate = moment().add(30, 'days');
        this.loadStats();
    }

    loadStats() {
        $('#salesSnapLoading').show();
        $('#salesSnapContainer').show();
        $('#salesSnapError').hide(); 


        let requestParams = {
            businessID: this.options.filters.office,
            fromDate: this.options.filters.startDate.format('YYYY-MM-DD'),
            toDate: this.options.filters.endDate.format('YYYY-MM-DD'),
            storeID: this.options.filters.store
            

        };
        console.log('requestParams ', this.options); 

        SalesSvc.getSalesMetrics(requestParams).then((respData) => {


            let pageData = {
                totalTransactionsCount: _.sumBy(respData.transactionDetails, "transactionCount"),
                totalTransactionAmt: _.sumBy(respData.transactionDetails, "totalSum")
            };


            $('#salesSnapLoading').hide();
            this.renderStats(pageData);
        }, function () {
            $('#salesSnapContainer').hide();
            $('#salesSnapLoading').hide();
            $('#salesSnapError').show();
        });
    }

    loadDatePicker() {
        $('#snapDatePicker').daterangepicker({
            "autoApply": true,
            // "dateLimit": {
            //     "days": 90
            // },
            "startDate": this.options.filters.startDate,
            "endDate": this.options.filters.endDate,
            "opens": "left",
            "locale": {
                "format": "DD MMM, YYYY",
            }
        }, (start, end, label) => {
            var filters = {};
            let salesStats, vendorsStats, footfall;
            this.options.filters.startDate = start;
            this.options.filters.endDate = end;
            filters.startDate = this.options.filters.startDate ;
            filters.endDate = this.options.filters.endDate;
            filters.store = this.options.filters.store;
            filters.office = this.options.filters.office;
            salesStats = new SalesAmtAndTransaction(null, {filters: filters});
            // salesStats.triggerReload(filters);
            // footfall.triggerReload(filters);
            this.loadStats();
        });
    }

    renderStats(data) {
        this.renderTemplate('#salesSnapContainer', SnapTemplate, data);
    }
}


export default VendorSalesSnap;
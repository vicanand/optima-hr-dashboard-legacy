import Component from "./component";

import ExpressTable from '../../widgets/expressTable';
import UniTransaction from '../../../template/tables/express/uniTransactions.ejs';
import EmployeeSvc from '../../service/express/employeeSvc';


class EmployeeUniTans extends Component {
    constructor(element, options) {
        super(element);
        this.element = element;
        this.options = options;

        this.options.filters.startDate = moment().subtract(30, 'days');
        this.options.filters.endDate = moment();

        this.loadDatePicker();
        this.loadTable();
    }

    triggerUpdate(filters) {
        this.options.filters = filters;
        this.options.filters.startDate = moment().subtract(30, 'days');
        this.options.filters.endDate = moment();
        this.transactionTable.triggerRefresh(filters);
    }


    loadDatePicker() {
        $('#uniqTransDatePicker').daterangepicker({
            "autoApply": true,
            "dateLimit": {
                "days": 30
            },
            "startDate": this.options.filters.startDate,
            "endDate": this.options.filters.endDate,
            "opens": "left",
            "locale": {
                "format": "DD MMM, YYYY",
            }
        }, (start, end, label) => {
            this.options.filters.startDate = start;
            this.options.filters.endDate = end;
            this.loadTable();
        });
    }


    loadTable() {
        this.transactionTable = new ExpressTable(this.element.find('.uniqueTransactionsTable'), {
            tableTemplate: UniTransaction,
            source: EmployeeSvc.getUniqueTransactionsTable,
            onloadStarted: function () {
                $("#uniqueTransactionsChartLoading").show();
                $('#uniqueTransactionsChart').hide();
            },
            onLoadCompleted: (respData) => {
                $("#uniqueTransactionsChartLoading").hide();
                if (respData.records.length > 0) {
                    this.loadChart(respData);
                    $('#uniqueTransactionsChart').show();
                }
            },
            onLoadFailed: () => {
                $("#uniqueTransactionsChartLoading").hide();
            }
        }, this.options.filters);
    }

    loadChart(data) {
        let formatINR = this.formatINRwithDecimal;

        var myChart = Highcharts.chart('uniqueTransactionsChart', {
            chart: {
                type: 'line'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: _.map(data.records, item => item.shortDateFormatted)
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            tooltip: {
                formatter: function () {
                    return `${this.x}<br/>${this.series.name}: <b>${formatINR(this.y)}</b>`;
                },
                enabled: true
            },
            series: [{
                name: 'No. of Unique Users',
                lineColor: "#f0308c",

                marker: {
                    symbol: 'circle',
                    fillColor: "#f0308c",
                },
                data: _.map(data.records, item => item.uniqueUsers)
            }, {
                name: 'No. of Transactions',
                lineColor: "#00b998",

                marker: {
                    symbol: 'circle',
                    fillColor: "#00b998",
                },
                data: _.map(data.records, item => item.transactionCount)
            }]
        });


    }
}

export default EmployeeUniTans;
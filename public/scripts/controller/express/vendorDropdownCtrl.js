

import Component from "./component";

class VendorDropDown extends Component {
    constructor() {
        super();
        this.element = $('#vendorDropdown');
        this.registerDomEvents();
    }


    registerDomEvents() {
        $(this.element).find('.dropdown--list--link').on('click', function () {
            $('.dropdown--list').addClass('dropdown--list__active');
        });

        $(window).on('click', function (e) {
            if ($(e.target).closest($('.vendor--details')).length === 0) {
                $('.dropdown--list').removeClass('dropdown--list__active');
            }
        });
    }
}

export default VendorDropDown;


import Component from "./component";
import VendorSvc from "../../service/express/vendorSvc";
import Dropdown from "../../widgets/singleDropdown";
import ExpressTable from '../../widgets/expressTable';
import VendorTemplate from '../../../template/tables/express/vendors.ejs';
import Utils from '../../common/util';

class TopTenVendors extends Component {
    constructor(element, options) {
        super(element);
        this.element = element;

        this.options = options;
        this.options.filters.startDate =  this.options.filters.startDate || moment().subtract(90, 'days');
        this.options.filters.endDate = this.options.filters.endDate || moment();

        this.loadDatePicker();
        this.loadTable();
        this.initializeDropdowns();
    }

    loadTable() {
        this.vendorTable = new ExpressTable('#vendorsTable', {
            tableTemplate: VendorTemplate,
            source: VendorSvc.getVendorsTrends,
            onloadStarted: function () {
                $("#vendorGraph").show();
                $('#topTenVendorsChart').hide();
            },
            onLoadCompleted: (respData) => {
                $("#vendorGraph").hide();
                if (respData.records.length > 0) {
                    this.loadBarChart(respData);
                    $('#topTenVendorsChart').show();
                }
            },
            onLoadFailed: () => {
                $("#vendorGraph").hide();       
            }
        }, {
            order: this.options.filters.order || 'DESC',
            orderBy: this.options.filters.orderBy ||'SALES_AMOUNT',       
            fromDate: this.options.filters.startDate.format('YYYY-MM-DD'),
            toDate: this.options.filters.endDate.format('YYYY-MM-DD'),
            businessID: this.options.filters.office,
            stores: this.options.filters.stores  

        });
    }


    initializeDropdowns() {
        this.sortDropdown = new Dropdown('#sortVendor', {
            data: [
                {
                    label: 'Top',
                    value: 'DESC'
                },
                {
                    label: 'Bottom',
                    value: 'ASC'
                }
            ],
            onSelected: () => {
                this.changeData();
            }
        });

        this.sortBy = new Dropdown('#sortBy', {
            data: [
                {
                    label: 'Sales Amount',
                    value: 'SALES_AMOUNT'
                },
                {
                    label: 'Sales Quantity',
                    value: 'NUM_TRANSACTIONS'
                }
            ],
            onSelected: () => {
                this.changeData();
            }
        });
    }


    changeData() {
        console.log("topVendors");                
        let filters = {
            order: this.sortDropdown.getValue(),
            orderBy: this.sortBy.getValue(),
            fromDate: this.options.filters.startDate.format('YYYY-MM-DD'),
            toDate:this.options.filters.endDate.format('YYYY-MM-DD'),                                                              
                              
        };

        this.vendorTable.triggerRefresh(filters); 
    }

    loadDatePicker() {
        $('#vendorDatePicker').daterangepicker({
            "autoApply": true,
            // "dateLimit": {
            //     "days": 90
            // },
            "startDate": this.options.filters.startDate,
            "endDate": this.options.filters.endDate,
            "opens": "left",
            "locale": {
                "format": "DD MMM, YYYY",
            }
        }, (start, end, label) => {
            this.options.filters.startDate = start;
            this.options.filters.endDate = end;
            this.options.filters.order = this.sortDropdown.getValue();
            this.options.filters.orderBy = this.sortBy.getValue();
            this.loadTable();
        });
    }

    loadBarChart(data) {
        const sortBy = this.sortBy.getValue();
        // let graphData = _.map(data.records, function (item) {
        //     return [item.businessName, (sortBy == 'SALES_AMOUNT' ? item.totalSum : item.transactionCount)];
        // });

        let salesTransactionLabel = 'Sales Quantity';
        let formatINR = this.formatINRwithDecimal;


        Highcharts.chart('topTenVendorsChart', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    // rotation: -45,
                    style: {
                        fontSize: '12px',
                    }
                },
                categories: _.map(data.records, (item) => {
                    return item.businessName;
                })
            },
            yAxis: {
                min: 0,
                title: {
                    text: (sortBy == 'SALES_AMOUNT') ? 'Sales Amount' : salesTransactionLabel
                },
                labels: {
                    formatter: function () {
                        return ((sortBy == 'SALES_AMOUNT') ? '₹' : '') + this.axis.defaultLabelFormatter.call(this);
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function () {
                    if (sortBy == 'SALES_AMOUNT') {
                        return `${this.x}<br/>Sales Amount: <b>₹${formatINR(this.y)}</b>`;
                    }
                    else {
                        return `${this.x}<br/>${salesTransactionLabel}: <b>${formatINR(this.y)}</b>`;
                    }
                },
                enabled: true
            },
            plotOptions: {
                column: {
                    minPointLength: 3
                }
            },
            series: [{
                name: 'Vendors',
                // data: graphData,
                data: _.map(data.records, (item) => {
                    if (sortBy == 'SALES_AMOUNT') {
                        return item.totalSum == 0 ? null : item.totalSum;
                    }
                    else {
                        return item.transactionCount == 0 ? null : item.transactionCount;
                    }
                }),
                color: '#633ea5'
                // dataLabels: {
                //     enabled: true,
                //     rotation: -90,
                //     color: '#FFFFFF',
                //     align: 'right',
                //     format: '{point.y:.1f}', // one decimal
                //     y: 10, // 10 pixels down from the top
                //     style: {
                //         fontSize: '13px',
                //         fontFamily: 'Verdana, sans-serif'
                //     }
                // }
            }]
        });
    }
}

export default TopTenVendors;
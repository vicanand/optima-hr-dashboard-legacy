import MenuFilters from './menuFilters';
import EmployeeSvc from '../../service/express/employeeSvc';
import Component from './component';
import EmployeeStats from '../../../template/section/express/subviews/employeeStats.ejs';
import PaymentTable from '../../../template/section/express/subviews/paymentTable.ejs';
import Utils from '../../common/util';


class EmployeeSnapshot extends Component {
    constructor(element = null, options = {}) {
        super();
        this.options = options; 
        new MenuFilters(null, {
            onChange: (filters) => {
                console.log(filters);    
                this.options.filters = filters;   
                this.options.filters.startDate = moment().subtract(30, 'days');
                this.options.filters.endDate = moment();
                this.loadDatePicker();
                this.loadData(filters);
                this.loadPaymentPreference(filters);

                if (options.filterChange) {
                    options.filterChange(filters);
                }

            }
        });
    }

    loadDatePicker() {
        $('#employeeSnapDatePicker').daterangepicker({
            "autoApply": true,
            
            "startDate": this.options.filters.startDate,
            "endDate": this.options.filters.endDate,
            "opens": "left",
            "locale": {
                "format": "DD MMM, YYYY",
            }
        }, (start, end, label) => {
            console.log(this.options);
            this.options.filters.startDate = start;
            this.options.filters.endDate = end;
            this.loadData(this.options.filters);    
        });
    }
    loadData(filters) {
        console.log('filters: ', filters);
            
        
        let params = {
            fromDate: this.options.filters.startDate.format('YYYY-MM-DD'),
            toDate:this.options.filters.endDate.format('YYYY-MM-DD'),
            businessID: filters.office,
            storeID: filters.store
        };


        $('#employeeListLoading').show();
        $('#employeeSnapError').hide();       

        EmployeeSvc.getEmployeeStats(params).then((respData) => {
            console.log("EmployeeSnapshot",respData);        
            $('#employeeListLoading').hide();
            $('#employeeSnapError').hide();
            this.renderStats(respData);
        }, function () {
            $('#employeeSnapError').show();
            $('#employeeListLoading').hide();
        });
    }


    loadPaymentPreference(filters) {
        let params = {
            fromDate: moment().subtract(30, 'days').format('YYYY-MM-DD'),
            toDate: moment().format('YYYY-MM-DD'),
            businessID: filters.office,
            storeID: filters.store
        };

        $('#paymentPrefLoading').show();
        $('#paymentPrefContainer').hide();
        $('#paymentPrefError').hide();
        EmployeeSvc.getPaymentPreference(params).then((respData) => {
            this.renderPaymentTable(respData);
            this.renderPieChart(respData);
            $('#paymentPrefLoading').hide();
            $('#paymentPrefContainer').show();
        }, function () {
            $('#paymentPrefError').show();
            $('#paymentPrefLoading').hide();
        });
    }

    renderStats(data) {
        this.renderTemplate('#employeeList', EmployeeStats, data);
    }

    renderPaymentTable(data) {
        this.renderTemplate('#paymentTableStats', PaymentTable, data);
    }

    renderPieChart(data) {


        let picData = [];


        _.forEach(data, function (item) {
            _.forEach(item, function (typeItem, key) {
                picData.push({
                    name: key,
                    y: typeItem.count,
                    amount: typeItem.sum,
                    color: typeItem.color
                });
            });
        });


        let presentData = _.filter(picData, {y: 0});

        if (presentData.length == 7) {
            $("#pieChart").hide();
        }
        else {
            $("#pieChart").show();
        }


        Highcharts.chart('pieChart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: '',
                align: 'center',
                verticalAlign: 'middle',
                y: 40
            },
            tooltip: {
                // pointFormat: '{point.percentage:.1f}% - ₹{point.amount}'
                formatter: function () {
                    return `${this.point.name}
                    <br>
                    <b>${(this.percentage).toFixed(1)}% - ₹${Utils.formatINR(this.point.amount / 100)}</b>
                    `;
                }
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: 'white'
                        },
                        formatter: function () {
                            return this.percentage.toFixed(1) + '%';
                        }
                    },
                    // startAngle: -90,
                    // endAngle: 90,
                    center: ['50%', '50%']
                }
            },
            series: [{
                type: 'pie',
                name: 'Amount',
                innerSize: '40%',
                data: picData
            }]
        });
    }
}


export default EmployeeSnapshot;
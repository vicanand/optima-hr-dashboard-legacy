import Component from './component';
import SalesSvc from '../../service/express/salesSvc';
import TopTenVendors from '../../controller/express/topTenVendors';
import TopTenStores from '../../controller/express/topTenStores';
import PaymentPreference from '../../controller/express/paymentPreferences';
import SnapTemplate from '../../../template/section/express/subviews/cafeSnapShot.ejs';

class CafeteriaSnapShot extends Component {
    constructor(element, options = {}) {
        super();
        // console.log('cafeteriaSnapShot');
        var filters = {};
        var paymentPreference;
        console.log('options: ', options);
        this.options = options;
        this.options.filters = this.options.filters  || {};
        this.options.filters.startDate = moment().subtract(90, 'days');
        this.options.filters.endDate = moment();
        // filters.store = _.map(filters.store, (item) => item.toString());
        filters.stores = this.options.filters.stores;
       /// this.options.filters.stores = this.options.filters.stores;
        filters.startDate = this.options.filters.startDate;
        filters.endDate = this.options.filters.endDate;
        this.loadDatePicker();
        this.loadStats();
        new TopTenVendors('#topTenVendors',{filters:filters});
        new TopTenStores('#topTenStores',{filters:filters});
        paymentPreference = new PaymentPreference(null,
            {
                filters:filters
            });
    }

    triggerReload(filters) {
        this.options.filters.city = filters.city;
        this.options.filters.office = filters.office;
        this.options.filters.stores = filters.stores;

        // this.options.filters.startDate = moment();
        // this.options.filters.endDate = moment().add(30, 'days');
        this.loadStats();
    }

    loadStats() {
        $('#salesSnapLoading').show();
        $('#salesSnapContainer').show();
        $('#salesSnapError').hide();


        let requestParams = {
            businessID: this.options.filters.office,
            fromDate: this.options.filters.startDate.format('YYYY-MM-DD'),
            toDate: this.options.filters.endDate.format('YYYY-MM-DD'),
            stores: this.options.filters.stores                
        };


        SalesSvc.getCafeStatistics(requestParams).then((respData) => {

            console.log("respData",respData);
            // let pageData = {
            //     totalTransactionsCount: _.sumBy(respData.transactionDetails, "transactionCount"),
            //     totalTransactionAmt: _.sumBy(respData.transactionDetails, "totalSum")
            // };


            $('#salesSnapLoading').hide();
            this.renderStats(respData);  
        }, function () {
            $('#salesSnapContainer').hide();
            $('#salesSnapLoading').hide();
            $('#salesSnapError').show();
        });
    }

    loadDatePicker() {
        $('#snapDatePicker').daterangepicker({
            "autoApply": true,
            // "dateLimit": {
            //     "days": 90
            // },
            "startDate": this.options.filters.startDate,
            "endDate": this.options.filters.endDate,
            "opens": "left",
            "locale": {
                "format": "DD MMM, YYYY",
            }
        }, (start, end, label) => {
            var filters = {};
            var paymentPreference;
            this.options.filters.startDate = start;
            this.options.filters.endDate = end;
            filters.startDate = this.options.filters.startDate;
            filters.endDate = this.options.filters.endDate;
            filters.stores = this.options.filters.stores;
            new TopTenVendors('#topTenVendors',{filters:filters});
            new TopTenStores('#topTenStores',{filters:filters});
            paymentPreference = new PaymentPreference(null,
                {
                    filters:filters
                });
            this.loadStats();
        });
    }

    renderStats(data) {
        console.log("cafeteriaSnapShot========",data);  
             
        this.renderTemplate('#salesSnapContainer', SnapTemplate, data);
    }
}


export default CafeteriaSnapShot;
import ExpressTable from '../../widgets/expressTable';
import StoresSvc from '../../service/express/storesSvc';
import AllStoresTemplate from '../../../template/tables/express/allStores.ejs';
import Component from './component';

class AllStores extends Component{
    constructor() {
        super();
        this.loadTable();
    }

    loadTable() {
        this.transactionTable = new ExpressTable('#allStores', {
            tableTemplate: AllStoresTemplate,
            source: StoresSvc.getStoresTable,
            onloadStarted: function () {
                // $("#uniqueTransactionsChartLoading").show();
            },
            onLoadCompleted: (respData) => {
                // $("#uniqueTransactionsChartLoading").hide();
                // this.loadChart(respData);
            }
        }, {
            businessID: this.readCookie('businessID')
        });
    }
}

export default AllStores;



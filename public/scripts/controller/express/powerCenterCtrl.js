import SidebarDOMEvents from '../../dom-events/sidebarDOMEvents.js';

export default{
    init: function(){
        $('.sidebar-container').addClass('sidebar-toggle');
        $('.sidebar-container').addClass('sidebar-toggle-power');
        $('.inner-container').addClass('main-cntr-toggle');
        $('.header-container').addClass('header-cntr-toggle');
        $('.nav-company-toggle').addClass('nav-comp-toggle');
        $('.heading-cntr-toggle').addClass('heading-txt-toggle');
        $('.heading-cntr-toggle').addClass('heading-txt-toggle-power');
        $('.global-heading-toggle').addClass('global-heading-txt-toggle');
        $('.new-program-toggle').addClass('new-pgm-toggle');    
        $('li.corpTools > span').addClass('textToggle');
        $('.companyHide').addClass('hideThisEl');
        $('.companyShow').removeClass('hideThisEl');
        $('#nav-block-hide').css('display', 'block');
       if($('.nav-btn-cntr').hasClass('active')){
            SidebarDOMEvents.toggleSidebar();
        }
        SidebarDOMEvents.openProgDropDown();
        $('.sideMenuNav .tooltiptext').show();
        $('.sideBar_subcon').hide();
        $('.arrowBounce').hide();
        $('.sideMenuNav .tooltiptext').hide();
        $('.sideBar_subcon').slideUp(100);
		$('.openProgList').addClass('closed');
		$('span.arrowBounce').hide();
        
    }
}
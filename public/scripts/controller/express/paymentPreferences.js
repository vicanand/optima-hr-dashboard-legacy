import Component from './component';
import EmployeeSvc from '../../service/express/employeeSvc';
import Utils from '../../common/util';
import PaymentTable from '../../../template/section/express/subviews/paymentTable.ejs';
class PaymentPreference extends Component {
    constructor(element, options = {}) {
        super();
        this.paymentStats = this.paymentStats.bind(this);     
        this.options = options; 
        this.options.filters.startDate = this.options.filters.startDate || moment().subtract(90, 'days');
        this.options.filters.endDate = this.options.filters.endDate || moment();
    

        this.loadDatePicker();
        this.loadPaymentPreference(this.options.filters);       
            
    }


    triggerReload(filters) {
        // console.log('---=====---==',filters);
        this.options.filters.city = filters.city;
        this.options.filters.office = filters.office;
        this.options.filters.store = filters.store;    
        this.loadDatePicker();  
        // this.loadPaymentPreference(this.options.filters);       
        // this.options.filters.startDate = moment();
        // this.options.filters.endDate = moment().add(30, 'days');
       // this.loadStats();
    }
    loadDatePicker() {
        $('#paymentPreferenceDatePicker').daterangepicker({
            "autoApply": true,
            // "dateLimit": {
            //     "days": 90
            // },
            "startDate": this.options.filters.startDate,
            "endDate": this.options.filters.endDate,
            "opens": "left",
            "locale": {
                "format": "DD MMM, YYYY",
            }
        }, (start, end, label) => {
            this.options.filters.startDate = start;
            this.options.filters.endDate = end;
            this.loadPaymentPreference(this.options.filters)
            //this.loadStats();          
        });
    }
    paymentStats(){
        //console.log("Screw You");
        let dataString;
        
                // Example data given in question text
                let data = [];
                let totalSum=0;
                var self = this;
                var name;
                data.push(['PaymentType', 'Tool', 'Number of Transaction', 'Total Amount','Percentage (%)']);
                var keys = Object.keys(this.payment);
                keys.forEach(function(category){
                    console.log('category: ', category);
                    var innerKeys = Object.keys(self.payment[category]); 
                    innerKeys.forEach(function(item){
                        totalSum = totalSum + self.payment[category][item].count;
                        console.log('totalSum: ', totalSum);  
                       
                    });      
                });
                keys.forEach(function(category){
                    console.log('category: ', category);
                    var innerKeys = Object.keys(self.payment[category]);    
                    innerKeys.forEach(function(item){
                        console.log( self.payment[category][item]);
                        data.push([category,item,self.payment[category][item].count,((self.payment[category][item].sum)/100),((((self.payment[category][item].count)/totalSum)*100).toFixed(2))]); 
                        console.log(data);
                    }); 
                });
                var csvContent = '';
                data.forEach(function (infoArray, index) {
                    dataString = infoArray.join(',');
                    csvContent += index < data.length ? dataString + '\n' : dataString;
                });
                var download = function (content, fileName, mimeType) {
                   
                    var a = document.createElement('a');
                    mimeType = mimeType || 'application/octet-stream';
        
                    if (navigator.msSaveBlob) { // IE10
                        navigator.msSaveBlob(new Blob([content], {
                            type: mimeType
                        }), fileName);
                    } else if (URL && 'download' in a) { //html5 A[download]
                        a.href = URL.createObjectURL(new Blob([content], {
                            type: mimeType
                        }));
                        a.setAttribute('download', fileName);
                        document.body.appendChild(a);
                        a.click();
                        document.body.removeChild(a);     
                    } else {
                        location.href = 'data:application/octet-stream,' + encodeURIComponent(content); // only this mime type is supported
                    }
                }
                name = 'PaymentPreference_'+moment().format('YYYY-MM-DD hh:mm:ss a')+'.csv'; 
                download(csvContent, name, 'text/csv;encoding:utf-8');
    }
    loadPaymentPreference(filters) {
        let params = {
            fromDate:  this.options.filters.startDate.format('YYYY-MM-DD'),
            toDate:  this.options.filters.endDate.format('YYYY-MM-DD'),
            // businessID: filters.store,
            stores: filters.stores
        };

        $('#paymentPrefLoading').show();
        $('#paymentPrefContainer').hide();
        $('#paymentPrefError').hide();
        EmployeeSvc.getPaymentPreference(params).then((respData) => {                                     
            this.payment = respData;
            console.log(' paymentPreferences ----> this.respData: ',  this.payment);
            this.renderPaymentTable(respData);
            this.renderPieChart(respData);
           
            $('#paymentPrefLoading').hide();
            $('#paymentPrefContainer').show();
        }, function () {
            $('#paymentPrefError').show();
            $('#paymentPrefLoading').hide();
        });
    }
    renderPaymentTable(data) {
        this.renderTemplate('#paymentTableStats', PaymentTable, data);
        $('#paymentDownload').off('click').on('click',this.paymentStats);                                                                
    }


    renderPieChart(data) {


        let picData = [];


        _.forEach(data, function (item) {
            _.forEach(item, function (typeItem, key) {
                picData.push({
                    name: key,
                    y: typeItem.count,
                    amount: typeItem.sum,
                    color: typeItem.color                
                });
            });
        });


        let presentData = _.filter(picData, {y: 0});

        if (presentData.length == 10) {                                                            
            $("#pieChart").hide();
        }
        else {
            $("#pieChart").show();
        }


        Highcharts.chart('pieChart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: '',
                align: 'center',
                verticalAlign: 'middle',
                y: 40
            },
            tooltip: {
                // pointFormat: '{point.percentage:.1f}% - ₹{point.amount}'
                formatter: function () {
                    return `${this.point.name}
                    <br>
                    <b>${(this.percentage).toFixed(1)}% - ₹${Utils.formatINR(this.point.amount / 100)}</b>
                    `;
                }
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: 'white'
                        },
                        formatter: function () {
                            return this.percentage.toFixed(1) + '%';
                        }
                    },
                    // startAngle: -90,
                    // endAngle: 90,
                    center: ['50%', '50%']
                }
            },
            series: [{
                type: 'pie',
                name: 'Amount',
                innerSize: '40%',
                data: picData
            }]
        });
    }

}

export default PaymentPreference;
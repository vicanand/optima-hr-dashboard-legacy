import SettlementTableTemplate from '../../../template/tables/express/settlementsTable.ejs';
import ExpressTable from '../../widgets/expressTable';
import Component from './component';
import SettlementSvc from '../../service/express/settlementSvc';

class SettlementsTable extends Component {
    constructor(element, options = {}) {
        super();
        this.options = options;
        this.options.filters.startDate = this.options.filters.startDate || moment().subtract(30, 'days');
        this.options.filters.endDate = this.options.filters.endDate || moment();
        this.loadDatePicker();
        this.loadTable();
        this.registerDomEvents();
    }


    loadDatePicker() {
        $('#datePicker').daterangepicker({
            "autoApply": true,
            "startDate": this.options.filters.startDate,
            "endDate": this.options.filters.endDate,
            "opens": "left",
            "locale": {
                "format": "DD MMM, YYYY",
            }
        }, (start, end, label) => {
            this.options.filters.startDate = start;
            this.options.filters.endDate = end;
            this.transactionTable.triggerRefresh(this.options.filters);
        });
    }


    triggerLoad(filters) {
        this.options.filters.city = filters.city;
        this.options.filters.office = filters.office;
        this.options.filters.store = filters.store;
        this.transactionTable.triggerRefresh(this.options.filters);
    }


    loadTable() {
        this.transactionTable = new ExpressTable('#settlementTable', {
            tableTemplate: SettlementTableTemplate,
            source: SettlementSvc.getSettlementsTable,
        }, this.options.filters);
    }

    registerDomEvents() {
        $('#settlementTable').on('click', '.show-more', function () {
            $(this).parents('.settlement__row').toggleClass('settlement__row--active');
        });
    }
}

export default SettlementsTable;
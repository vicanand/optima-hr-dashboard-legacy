import Dropdown from '../../widgets/dropdown';

export default {
    init: function () {
        Dropdown.init();

        $("#cityDropdown").dropdown({
            label: 'City',
            singleItemLabel: 'City',
            multipleItemLabel: 'Cities',
            onSelected: function (e, value) {
                // console.log(value);
            }
        });

        $("#officeDropdown").dropdown({
            label: 'Office',
            singleItemLabel: 'Office',
            multipleItemLabel: 'Offices',
            onSelected: function (e, value) {
                // console.log(value);
            }
        });

        $("#storeDropdown").dropdown({
            label: 'Store',
            singleItemLabel: 'Store',
            multipleItemLabel: 'Stores',
            onSelected: function (e, value) {
                // console.log(value);
            }
        });

        this.loadGraph();
    },

    loadGraph: function () {
        var myChart = Highcharts.chart('container', {
            chart: {
                type: 'line'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: ['16 Jun', '17 Jun', '18 Jun', '19 Jun', '20 Jun', '21 Jun', '22 Jun', '23 Jun']
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            series: [{
                name: 'No of Unique Users',
                lineColor: "#f0308c",

                marker: {
                    symbol: 'circle',
                    fillColor: "#f0308c",
                },
                data: [0, 13, 14, 15, 40, 50, 60, 70]
            }, {
                name: 'No of Unique Transactions',
                lineColor: "#00b998",

                marker: {
                    symbol: 'circle',
                    fillColor: "#00b998",
                },
                data: [10, 3, 24, 25, 50, 60, 70, 80]
            }]
        });

        Highcharts.chart('barChart', {
            chart: {
                type: 'column'
            },
            color:"red",
            title: {
                text: 'Stacked column chart'
            },
            xAxis: {
                categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total fruit consumption'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -30,
                verticalAlign: 'top',
                y: 25,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false,
            },
            // tooltip: {
            //     headerFormat: '<b>{point.x}</b><br/>',
            //     // pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            // },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                }
            },
            series: [{
                name: 'John',
                data: [5, 3, 4, 7, 2],
                'color': '#8446e3'
            }]
        });

        Highcharts.chart('pieChart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: '',
                align: 'center',
                verticalAlign: 'middle',
                y: 40
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: 'white'
                        }
                    },
                    // startAngle: -90,
                    // endAngle: 90,
                    center: ['50%', '50%']
                }
            },
            series: [{
                type: 'pie',
                name: 'Browser share',
                innerSize: '40%',
                data: [
                    ['Firefox',   10.38],
                    ['IE',       56.33, "#FFFFFF"],
                    ['Chrome', 24.03],
                    ['Safari',    4.77],
                    ['Opera',     0.91],
                    // {
                    //     name: 'Proprietary or Undetectable',
                    //     y: 0.2,
                    //     dataLabels: {
                    //         enabled: false
                    //     }
                    // }
                ]
            }]
        });
    }
};
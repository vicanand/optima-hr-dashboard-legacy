import Component from './component';
import SalesSvc from '../../service/express/salesSvc';

class Footfall extends Component {
    constructor(element, options) {
        super();
        this.options = options;
        this.options.filters.date = this.options.filters.endDate || moment();
        this.loadDatePicker();
        this.loadGraph();
    }

    triggerReload(filters) {
        this.options.filters = filters;
        this.options.filters.date = this.options.filters.endDate || moment();
        this.loadGraph();
    }

    loadDatePicker() {
        $('#footfallDatePicker').datepicker({
            autoclose: true,
            format: 'd M yy',
            endDate: new Date()
        });
        $("#footfallDatePicker").datepicker("setDate", new Date());
        $("#footfallDatePicker").on('changeDate', () => {
            this.options.filters.date = moment($('#footfallDatePicker').datepicker("getDate"));
            this.loadGraph();
        });
    }

    loadGraph() {
        $("#footfallGraphLoading").show();
        $("#footfallGraph").hide();
        SalesSvc.getFootfall(this.options.filters).then((respData) => {
            $("#footfallGraphLoading").hide();
            $("#footfallGraph").show();
            this.renderGraph(respData);
        }, function () {
            $("#footfallGraphLoading").hide();
        });
    }



    renderGraph(data) {

        let graphValues = [];

        let graphCategories = _.map(data.footfall, function (item, key) {
            return Number(key);
        });
        graphCategories.push(21);
        graphCategories.push(22);

        let graphData = _.map(data.footfall, function (item, key) {
            return [Number(key) + 0.5, item];
        });

        let getTime = function (number) {
            if (number == 12) {
                return number.toFixed(2) + ' PM';
            }
            if (number > 12) {
                return (number - 12).toFixed(2) + ' PM';
            }
            else {
                return number.toFixed(2) + ' AM';
            }
        };


        Highcharts.chart('footfallGraph', {
            chart: {
                type: 'area',
                inverted: false
            },
            title: {
                text: ''
            },
            xAxis: {
                reversed: false,
                title: {
                    enabled: true,
                },
                labels: {
                    formatter: function () {
                        return getTime(this.value);
                    }
                },
                // maxPadding: 0.05,
                // showLastLabel: true,
                // categories: graphCategories
            },
            yAxis: {
                title: {
                    text: 'No of Transactions'
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                },
                lineWidth: 2
            },
            legend: {
                enabled: false
            },
            tooltip: {
                // headerFormat: '<b>{series.name}: {point.y}</b><br/>',
                // pointFormat: '{point.x}',
                formatter: function () {

                    return `<b>${this.series.name}: ${this.y}</b><br/> Between ${getTime(this.x - 0.5)} to ${getTime(this.x + 0.5)}`;
                    // return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
                }
            },
            plotOptions: {
                area: {
                    fillOpacity: 0.2,
                    color: '#eb3b8c'
                },
                spline: {
                    marker: {
                        enable: false
                    }
                }
            },
            series: [{
                name: 'No of Transactions',
                data: graphData
            }]
        });
    }
}

export default Footfall;
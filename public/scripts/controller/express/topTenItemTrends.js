import Component from "./component";
import VendorSvc from "../../service/express/vendorSvc";
import Dropdown from "../../widgets/singleDropdown";
import ExpressTable from '../../widgets/expressTable';
import ItemTrendsTemplate from '../../../template/tables/express/itemTrends.ejs';
import Utils from '../../common/util';

class TopTenItemTrends extends Component {
    constructor(element, options) {
        super(element);
        this.element = element;
        this.options = options;
        this.fromDate = moment().subtract(30, 'days');
        this.toDate = moment();

        this.loadTable();
        this.initializeDropdowns();
        this.loadDatePicker();
    }

    loadDatePicker() {
        $('#datePicker').daterangepicker({
            "autoApply": true,
            // "dateLimit": {
            //     "days": 90
            // },       
            "startDate": this.fromDate,
            "endDate": this.toDate,
            "opens": "left",
            "locale": {
                "format": "DD MMM, YYYY",
            }
        }, (start, end, label) => {
            this.fromDate = start;
            this.toDate = end;
            this.changeData();
        });
    }


    loadTable() {
        this.vendorTable = new ExpressTable('#vendorsTable', {
            tableTemplate: ItemTrendsTemplate,
            source: VendorSvc.getItemTrends,
            onloadStarted: function () {
                $("#vendorGraph").show();
                $('#topTenVendorsChart').hide();
            },
            onLoadCompleted: (respData) => {
                $("#vendorGraph").hide();
                if (respData.records.length > 0) {
                    this.loadBarChart(respData);
                    $('#topTenVendorsChart').show();
                }
            },
            onLoadFailed: () => {
                $("#vendorGraph").hide();
            }
        }, {
            order: 'DESC',
            orderBy: 'SALES_AMOUNT',
            fromDate: this.fromDate,
            toDate: this.toDate,
            businessID: this.options.filters.office,
            storeID: this.options.filters.store
        });
    }


    initializeDropdowns() {
        this.sortDropdown = new Dropdown('#sortVendor', {
            data: [
                {
                    label: 'Top',
                    value: 'DESC'
                },
                {
                    label: 'Bottom',
                    value: 'ASC'
                }
            ],
            onSelected: () => {
                this.changeData();
            }
        });

        this.sortBy = new Dropdown('#sortBy', {
            data: [
                {
                    label: 'Sales Amount',
                    value: 'SALES_AMOUNT'
                },
                {
                    label: 'Sales Quantity',
                    value: 'NUM_TRANSACTIONS'
                }
            ],
            onSelected: () => {
                this.changeData();
            }
        });
    }


    triggerRefresh(options) {
        this.options.filters.office = options.office;
        this.options.filters.store = options.store;
        this.changeData();
    }


    changeData() {

        let params = {
            order: this.sortDropdown.getValue(),
            orderBy: this.sortBy.getValue(),
            fromDate: this.fromDate,
            toDate: this.toDate,
            businessID: this.options.filters.office,
            storeID: this.options.filters.store
        };

        this.vendorTable.triggerRefresh(params);
    }


    loadBarChart(data) {

        const sortBy = this.sortBy.getValue();
        let salesTransactionLabel = 'Sales Quantity';

        let formatINR = this.formatINRwithDecimal;


        Highcharts.chart('topTenVendorsChart', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    // rotation: -45,
                    style: {
                        fontSize: '12px',
                    }
                },
                categories: _.map(data.records, (item) => {
                    return item.itemName;
                })
            },
            yAxis: {
                min: 0,
                title: {
                    text: (sortBy == 'SALES_AMOUNT') ? 'Sales Amount' : salesTransactionLabel
                },
                labels: {
                    formatter: function () {
                        return ((sortBy == 'SALES_AMOUNT') ? '₹' : '') + this.axis.defaultLabelFormatter.call(this);
                    }
                },
                stackLabels: {
                    enabled: false,
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function () {
                    if (sortBy == 'SALES_AMOUNT') {
                        return `${this.x}<br/>Sales Amount: <b>₹${formatINR(this.y)}</b>`;
                    }
                    else {
                        return `${this.x}<br/>${salesTransactionLabel}: <b>${formatINR(this.y)}</b>`;
                    }
                },
                enabled: true
            },
            plotOptions: {
                column: {
                    minPointLength: 3
                }
            },
            series: [{
                name: 'Vendors',
                data: _.map(data.records, (item) => {
                    if (sortBy == 'SALES_AMOUNT') {
                        return (item.saleAmount / 100) == 0 ? null : (item.saleAmount / 100);
                    }
                    else {
                        return item.saleQuantity == 0 ? null : item.saleQuantity;
                    }
                }),
                color: '#633ea5'
                // dataLabels: {
                //     enabled: true,
                //     rotation: -90,
                //     color: '#FFFFFF',
                //     align: 'right',
                //     format: '{point.y:.1f}', // one decimal
                //     y: 10, // 10 pixels down from the top
                //     style: {
                //         fontSize: '13px',
                //         fontFamily: 'Verdana, sans-serif'
                //     }
                // }
            }]
        });
    }
}

export default TopTenItemTrends;
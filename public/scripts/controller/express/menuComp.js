import Component from "./component";
import QuickshopInfoSvc from "../../service/express/quickshopInfoSvc";
import MenuTemplate from '../../../template/section/express/subviews/menu.ejs';
import MenuFilters from './menuFilters';


class MenuComp extends Component {
    constructor() {
        super();

        new MenuFilters(null, {
            onChange: (data) => {
                this.topFilters = data;
                // this.loadMenu();
                this.loadQuickShop(data);
            }
        });
    }


    loadQuickShop(filters) {

        $('#menuDatePicker').hide();
        $('.loading--box').show();
        $('.menu__container__empty').hide();
        $('.menu__container__error').hide();

        QuickshopInfoSvc.getQuickshopInfo(filters).then((respData) => {
            if (respData) {
                if (respData.menuInputType == 'MultiDayMenuFileUploadEnabled') {
                    this.loadDates(filters);
                }
                else {
                    this.render(respData);
                }
            }
            else {
                $('.menu__container__empty').show();
                $('#menuContainer').html('');
            }


            $('.loading--box').hide();
        }, function () {
            $('.menu__container__error').show();
            $('#menuContainer').html('');
            $('.loading--box').hide();
        });
    }


    loadDatePicker(enableDates) {
        $('#menuDatePicker').datepicker({
            locale: 'cs',
            autoclose: true,
            format: 'd M yy',
            beforeShowDay: function (date) {
                return _.indexOf(enableDates, moment(date).format('YYYY-MM-DD')) != -1;
            }
        });

        $("#menuDatePicker").datepicker("update", new Date(moment(this.topFilters.date, 'YYYY-MM-DD')));

        $("#menuDatePicker").on('changeDate', () => {
            this.topFilters.date = moment($('#menuDatePicker').datepicker("getDate")).format('YYYY-MM-DD');
            this.loadMenu();
        });
    }

    loadDates(data) {
        $('.loading--box').show();
        $('.menu__container__empty').hide();
        $('.menu__container__error').hide();
        QuickshopInfoSvc.getListOfUploadedMenus(data).then((respData) => {
            let dates = _.map(respData.menuFilesMap, function (item, key) {
                return key;
            });

            if (dates.length) {
                this.topFilters.date = dates[0];
                this.loadDatePicker(dates);
                this.loadMenu();
                $('#menuDatePicker').show();
            }
            else {
                $('.loading--box').hide();
                $('.menu__container__empty').show();
                $('#menuContainer').html('');
            }
        }, function () {
            $('.menu__container__error').show();
            $('#menuContainer').html('');
            $('.loading--box').hide();
        });
    }


    loadMenu() {
        $('.menu__container__empty').hide();
        $('.menu__container__error').hide();

        $('.loading--box').show();
        QuickshopInfoSvc.getQuickshopInfoDate(this.topFilters).then((respData) => {
            if (respData) {
                this.render(respData);
            }
            else {
                $('.menu__container__empty').show();
                $('#menuContainer').html('');
            }

            $('.loading--box').hide();
        }, function () {
            $('.menu__container__error').show();
            $('#menuContainer').html('');
            $('.loading--box').hide();
        });

    }


    render(data) {

        let categoriesList = data.menu.categories;
        $.each(categoriesList, (index) => {
            let itemList = categoriesList[index].items;
            $.each(itemList, (itemIndex) => {
                let initial = this.getInitialFromTheName(itemList[itemIndex].name);
                itemList[itemIndex].initial = initial;
            });

        });


        this.renderTemplate('#menuContainer', MenuTemplate, data);
        this.registerDomEvents();
    }


    registerDomEvents() {
        $('.category--link').on('click', function () {

            console.log("called");
            const elementTop = $($(this).attr('data-target')).position().top;
            const offsetTop = 400;

            // debugger;
            // $(window).scrollTop(elementTop + offsetTop);
        });
    }
}

export default MenuComp;
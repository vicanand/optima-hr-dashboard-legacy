import Utils from "../../common/util";

class Component {
    constructor(element) {
        this.element = element;
    }

     toHHMMSS  (seconds) {
        if (seconds == 0) {
            return 0;
        }
    
    
        var days = Math.floor(seconds / 86400);
        var hours = Math.floor((seconds % 86400) / 3600);
        var minutes = Math.floor(((seconds % 86400) % 3600) / 60);
        var secs = ((seconds % 86400) % 3600) % 60;
    
    
        // var sec_num = parseInt(sec, 10); // don't forget the second param
        // var days = Math.floor(sec_num / 86400);
        // var hours = Math.floor(sec_num / 3600);
        // var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        // var seconds = sec_num - (hours * 3600) - (minutes * 60);
    
        // if (hours   < 10) {hours   = "0"+hours;}
        // if (minutes < 10) {minutes = "0"+minutes;}
        // if (seconds < 10) {seconds = "0"+seconds;}
        // return hours+':'+minutes+':'+seconds;
    
        let formatedTime = '';
    
        if (days && days != 0) {
            formatedTime += days + '<sub>d</sub> ';
        }
        if (hours && hours != 0) {
            formatedTime += hours + '<sub>hr</sub> ';
        }
        if (minutes && minutes != 0) {
            formatedTime += minutes + '<sub>m</sub> ';
        }
        if (secs && secs != 0) {
            formatedTime += secs + '<sub>s</sub> ';
        }
    
        return formatedTime;
    };

    renderTemplate(element, template, data, imports = {}) {

        imports.getInitialFromTheName = this.getInitialFromTheName;
        imports.formatINR = this.formatINRwithDecimal;
        imports.toHHMMSS = this.toHHMMSS;
        imports.htmlDecoder = function htmlDecoder(str) {
            return _.unescape(str);
        };

        let templateOptionsObj = {
            imports: imports
        };

        let myTemplate = _.template(template, templateOptionsObj);
        $(element).html(myTemplate(data));
    }

    formatINRwithDecimal (number) {
        var totalAmtSplitArr = Number(number).toString().split('.');
        var totalAmtINR = '';
        if (totalAmtSplitArr && totalAmtSplitArr.length) {
            totalAmtINR += totalAmtSplitArr[0].replace(/(\d)(?=(\d\d)+\d$)/g, '$1,');
            // Not considering the decimals i.e. paise
            if (totalAmtSplitArr.length === 2) {
                totalAmtINR += '.' + totalAmtSplitArr[1].substring(0,2);
            }
        }
        return totalAmtINR;
    }


    getInitialFromTheName(name) {
        let itemInitials;
        let itemNames = name;
        let itemNameArr = itemNames.split(' ');
        let itemInitial1 = itemNameArr[0][0];

        itemInitials = itemInitial1;
        if (itemNameArr[1]) {
            let itemInitial2 = itemNameArr[1][0];
            itemInitials += itemInitial2;
        }

        return itemInitials;
    }

    readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    productsInUse(value) {
        let products = {
            SUPER_TAG: 'Super Tag',
            RFID: 'Super Id',
            QR_CODE: 'QR Code',
            SHOP_ID: 'Shop Id',
            ZETA_CODE: 'Zeta Code ',
            SUPER_CARD: 'Super Card',
            ZETA_EXPRESS_KIOSK: 'KIOSK'
        };

        return products[value];
    }

    createCookie(name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        }
        else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    eraseCookie(name) {
        this.createCookie(name, "", -1);
    }
}

export default Component;
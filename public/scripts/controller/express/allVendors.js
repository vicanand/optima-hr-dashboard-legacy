import Component from './component';
import VendorsSvc from '../../service/express/vendorSvc';
import VendorsList from '../../../template/section/express/subviews/vendorList.ejs';

class AllVendors extends Component {
    constructor() {
        super();
        this.companyID = this.readCookie('companyID');
        // this.eraseCookie('companyID');
    }

    triggerLoad(data) {
        this.loadVendors(data);
    }


    loadVendors(data) {
        let requestPayload = {};

        if (data.stores.length > 0) {
            requestPayload.stores = data.stores.join(',');
        }


        $('#vendorsLoading').show();
        $("#vendorsEmpty").hide();
        $("#vendorsError").hide();
        VendorsSvc.getVendors(requestPayload).then((respData) => {
            respData.companyID = this.companyID;
            this.renderVendors(respData);
            if (respData.vendorDetails.length == 0) {
                $("#vendorsList").html("");
                $("#vendorsEmpty").show();
            }

            $('#vendorsLoading').hide();
        }, function () {
            $('#vendorsLoading').hide();
            $("#vendorsError").show();
        });
    }


    renderVendors(data) {
        this.renderTemplate('#vendorsList', VendorsList, data, {productsInUse: this.productsInUse});
    }
}

export default AllVendors;
import Component from './component';
import MultipleDropdown from '../../widgets/multipleDropdown';

class VendorsFilters extends Component {
    constructor(element, options) {
        super();

        this.options = options;
        this.list = JSON.parse($('#citiesList').html());
        this.initDropdown();

        this.loadCities();
        this.loadOffices();
        this.loadStores();
        this.triggerChange();
    }


    initDropdown() {
        this.cityPicker = new MultipleDropdown('#cityPicker', {
            isMultiple: true,
            data: [],
            defaultLabel: 'All cities',
            allSelectedLabel: 'All cities',
            pluralLabel: 'Cities',
            label: 'City',
            onChange: (cities) => {


                this.loadOffices();
                this.loadStores();
                this.triggerChange();
            }
        });

        this.officePicker = new MultipleDropdown('#officePicker', {
            isMultiple: true,
            data: [],
            defaultLabel: 'All offices',
            allSelectedLabel: 'All offices',
            pluralLabel: 'Offices',
            label: 'Office',
            onChange: () => {
                this.loadStores();
                this.triggerChange();
            }
        });

        this.storePicker = new MultipleDropdown('#storePicker', {
            isMultiple: true,
            data: [],
            defaultLabel: 'All stores',
            allSelectedLabel: 'All stores',
            pluralLabel: 'Stores',
            label: 'Stores',
            onChange: () => {
                this.triggerChange();
            }
        });
    }


    loadCities() {
        let cities = _.uniqBy(this.list, function (item) {
            return item.city;
        });

        cities = _.map(cities, function (item) {
            return {
                label: item.city,
                value: item.city,
                checked: true
            };
        });
        this.cityPicker.reset(cities);
    }

    loadOffices() {
        const cities = this.cityPicker.getValue();
        var filter=[],finalOffices = [];
        var list= JSON.parse($('#citiesList').html());
        //console.log('cities: ', cities);
        let offices = [];

        if (cities.length > 0) {
            const officesArray = _.map(cities, (city) => {
                return _.uniqBy(_.filter(this.list, {city: city}), 'office');
            });


            _.forEach(officesArray, function (item) {
                _.forEach(item, function (office) {
                    offices.push(office);
                });
            });
        }
        else {
            offices = _.uniqBy(this.list, 'office');
        }


        for(var i=0;i<offices.length;i++){
            if(filter[offices[i].businessID] != 1 ){
                filter[offices[i].businessID] = 1;
                finalOffices.push(offices[i]);
                
            }
        }
        console.log('finalOffices: ', finalOffices);
        let dropDownList = _.map(finalOffices, function (item) {
            console.log('item: ', item);
            return {
                label: item.office,
                value: item,
                checked: true
            };
        });

        this.officePicker.reset(dropDownList);
    }

    loadStores() {
        const cities = this.cityPicker.getValue(),
            offices = this.officePicker.getValue();
            console.log('offices: ', offices);

        var list= JSON.parse($('#citiesList').html()),filteredList=[];
        var uniqueValues = new Set(),uniqueOffices=[];

        for(var m=0;m<list.length;m++){
            uniqueValues.add(list[m].businessID);              
        }
        
        uniqueValues.forEach(function(item){
            uniqueOffices.push(item);
        });

        // let storesList = _.filter(this.list, function (storeItem) {
           
        //     return storeItem.businessID == 

        //     // if (_.indexOf(cities, storeItem.city) != -1 && _.indexOf(offices, storeItem.office) != -1) {
        //     //     return true;
        //     // }
        // });


        // let dropDownList = _.map(offices, function (item) {
        //     return {
        //         label: item.storeName,
        //         value: item.businessID + ":" + item.storeID,
        //         checked: true
        //     };
        // });
        for(var i=0;i<uniqueOffices.length;i++){
            for(var j=0;j<list.length;j++){
                if(uniqueOffices[i] == list[j].businessID){
                    filteredList.push({label:list[j].storeName,value:list[j].businessID+":"+list[j].storeID,checked:true});
                    
                }
            }
        }
        console.log('filteredList: ', filteredList);
        this.storePicker.reset(filteredList);
    }


    triggerChange() {
        let responseData = {
            stores: this.storePicker.getValue()
        };

        if (this.options.onChange) {
            this.options.onChange(responseData);
        }
    }
}

export default VendorsFilters;
import Component from './component';
import SettlementSvc from '../../service/express/settlementSvc';
import SnapTemplate from '../../../template/section/express/subviews/settlementSnap.ejs';
import SettlementsTable from '../../controller/express/settlementsTable';
class VendorSettlementSnap extends Component {

    constructor(element, options = {}) {
        super();
        this.options = options;

        this.options.filters.startDate = this.options.filters.startDate || moment().subtract(30, 'days');
        this.options.filters.endDate = this.options.filters.endDate || moment();

        this.loadDatePicker();
        this.loadStats();
    }

    triggerReload(filters) {
        this.options.filters.city = filters.city;
        this.options.filters.office = filters.office;
        this.options.filters.store = filters.stores;
        this.loadStats();
    }

    loadStats() {
        $('#settlementSnapLoading').show();
        $('#settlementSnapContainer').show();
        $('#settlementSnapError').hide();


        let requestParams = {
            businessID: this.options.filters.office,
            fromDate: this.options.filters.startDate.format('YYYYMMDD'),
            toDate: this.options.filters.endDate.format('YYYYMMDD'),
            // currency: 'INR'
            //storeID: this.options.filters.stores,
        };
        var totalCount;
        var refer = this;
        SettlementSvc.getSettlements(requestParams).then(function(respData){
            
            console.log('vendorSettlements',respData);
            var count = 0;
            for(var i = 0; i < respData.fragments.length; i++){
                if((respData.fragments[i].isSettledExternally) && (respData.fragments[i].state == 'SUCCESS')){
                    count = count + 1;
                }
            }
            totalCount = count;
            SettlementSvc.getSettlementMetrics(requestParams).then((respData) => {
                $('#settlementSnapLoading').hide();
                // debugger;
                respData.totalCount = totalCount;
                console.log("respData vendorSettlementSnap",respData);
                refer.renderStats(respData);
            }, function () {
                $('#settlementSnapContainer').hide();
                $('#settlementSnapLoading').hide();
                $('#settlementSnapError').show();
            });
        });
       
    }


    loadDatePicker() {
        $('#snapDatePicker').daterangepicker({
            "autoApply": true,
            "startDate": this.options.filters.startDate,
            "endDate": this.options.filters.endDate,
            "opens": "left",
            "locale": {
                "format": "DD MMM, YYYY",
            }
        }, (start, end, label) => {
            let settlement;
            var filters = {};
            this.options.filters.startDate = start;
            this.options.filters.endDate = end;
            filters.startDate = this.options.filters.startDate;
            filters.endDate = this.options.filters.endDate;
            filters.store = this.options.filters.store;
            filters.city = this.options.filters.city;
            filters.office = this.options.filters.office;
            settlement = new SettlementsTable(null, {filters: filters});
            this.loadStats();
        });
    }

    renderStats(data) {
        console.log('vendorSettlementSnap',data);
        let pageData = {
            bankTranfersCount: data.totalCount,
            prepaidPayments: data.amountSettledExternally,
            transferedAmount: data.totalSettlementAmount
        };

        this.renderTemplate('#settlementSnapContainer', SnapTemplate, pageData);
    }
}


export default VendorSettlementSnap;

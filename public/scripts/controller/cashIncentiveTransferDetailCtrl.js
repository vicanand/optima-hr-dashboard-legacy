import TransferDetailsDomEvents from "../dom-events/transferDetailsDomEvents";
import DomEvents from "../common/domEventHandler";
import OrderSvc from '../service/ordersService';
import Util from '../common/util';

export
default {
    init: function() {
        $("#payoutstable").tablewidget({
            source: OrderSvc.getPayouts,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: "/template/tables/cash-incentive-transfer-detail-payouts.ejs",
            rowTemplate: "/template/tables/cash-incentive-transfer-details-payout-row.ejs",
            extraParam: {
                startDate: Util.getUrlParameter("startDate"),
                endDate: Util.getUrlParameter("endDate"),
                statusFilters: '',
                orderId:window.location.pathname.split('/').pop(),
                compareDates: $('#compare-date').data('compare-date')
            },
            searchSelector: $("#employeeSearch"),
            searchButton: $("#searchEmployeeBtn"),
            filterSelect: $("#statusFilter"),
            dateSearch: $('#tranferPeriod'),
            payoutFilter: $('#payoutFilters')
        });

        $("#tranferPeriod").datepicker({
            format: "m-yyyy",
            viewMode: "months",
            minViewMode: "months",
            autoclose: true
        });

        DomEvents.bindEvent("payoutstable", "click", ".payoutOutCancelBtn", TransferDetailsDomEvents.confirmCancelPayout);
        DomEvents.bindEvent("payoutstable", "click", ".payoutOutRevokeBtn", TransferDetailsDomEvents.confirmRevokePayout);
        DomEvents.bindEvent("cancelPayoutConfirm", "click", ".reset", TransferDetailsDomEvents.resetCancelPayout);
        DomEvents.bindEvent("cancelPayoutConfirm", "click", ".confirm", TransferDetailsDomEvents.cancelPayout);
        DomEvents.bindEvent("revokePayoutConfirm", "click", ".reset", TransferDetailsDomEvents.resetRevokePayout);
        DomEvents.bindEvent("revokePayoutConfirm", "click", ".confirm", TransferDetailsDomEvents.revokePayout);
    },

    cancelPayout: function(orderId, payoutId) {
        return OrderSvc.cancelPayout(orderId, payoutId);
    },

    revokePayout: function(orderId, payoutId) {
        return OrderSvc.revokePayout(orderId, payoutId);
    }
}

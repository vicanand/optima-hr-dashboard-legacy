import Constant from '../common/constants';
import DomEventHandler from '../common/domEventHandler';
import BulkCloseOrdersDOMEvent from '../dom-events/bulkCloseOrdersDOMEvent';
import Storage from '../common/webStorage';
import moment from '../../lib/moment/moment';

let init = function() {
    DomEventHandler.bindClassEvent("transferorder-row", "click", ".download-claim-report", BulkCloseOrdersDOMEvent.downloadClaimReport);
    DomEventHandler.bindClassEvent("transferorder-row", "click", ".download-revoke-report", BulkCloseOrdersDOMEvent.downloadRevokeReport);
    DomEventHandler.bindClassEvent("transferorder-row", "click", ".download-card-status-report", BulkCloseOrdersDOMEvent.downloadCardStatusReport);
    DomEventHandler.bindClassEvent("transferorder-row", "click", ".download-file-report", BulkCloseOrdersDOMEvent.downloadFileReport);
};

export
default {
    init: init,
    moment: moment
}
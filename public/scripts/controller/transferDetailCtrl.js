import TransferDetailsDomEvents from "../dom-events/transferDetailsDomEvents";
import DomEvents from "../common/domEventHandler";
import OrderSvc from '../service/ordersService';
import Util from '../common/util';
import Table from "../widgets/table";
import Constants from "../common/constants";
import CacheDataStorage from '../common/cacheDataStorage';

export
default {
    init: function () {
        let revokeFunds = $('#payoutstable').data('revoke')
        let isProgramManagerRW = $('#payoutstable').data('pmrw');
   
        let tableTemplate = "/template/tables/transfer-detail-payouts.ejs";
        let rowTemplate = "/template/tables/transfer-details-payout-row.ejs"; 

        const country_configs = CacheDataStorage.getItem(Constants.COUNTRY_CONFIGS);
        if (country_configs) {
            const tablewidgetData = _.get(country_configs, 'transferDetailsCtrl.init');
            if (tablewidgetData && tablewidgetData.tableTemplate && tablewidgetData.rowTemplate) {
                tableTemplate = tablewidgetData.tableTemplate;
                rowTemplate = tablewidgetData.rowTemplate;
            }
        }

        $("#payoutstable").tablewidget({
            source: OrderSvc.getPayouts,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: tableTemplate,
            rowTemplate: rowTemplate,
            extraParam: {
                startDate: Util.getUrlParameter("startDate"),
                endDate: Util.getUrlParameter("endDate"),
                statusFilters: '',
                orderId: window.location.pathname.split('/').pop(),
                compareDates: $('#compare-date').data('compare-date'),
                revokeFunds: revokeFunds,
                isProgramManagerRW: isProgramManagerRW
            },
            searchSelector: $("#employeeSearch"),
            searchButton: $("#searchEmployeeBtn"),
            filterSelect: $("#statusFilter"),
            dateSearch: $('#tranferPeriod'),
            payoutFilter: $('#payoutFilters')
        });

        $("#tranferPeriod").datepicker({
            format: "m-yyyy",
            viewMode: "months",
            minViewMode: "months",
            autoclose: true
        });

        
        Table.init();
        DomEvents.bindEvent("payoutstable", "click", ".payoutOutCancelBtn", TransferDetailsDomEvents.confirmCancelPayout);
        DomEvents.bindEvent("payoutstable", "click", ".payoutOutRevokeBtn", TransferDetailsDomEvents.confirmRevokePayout);
        DomEvents.bindEvent("cancelPayoutConfirm", "click", ".reset", TransferDetailsDomEvents.resetCancelPayout);
        DomEvents.bindEvent("cancelPayoutConfirm", "click", ".confirm", TransferDetailsDomEvents.cancelPayout);
        DomEvents.bindEvent("revokePayoutConfirm", "click", ".reset", TransferDetailsDomEvents.resetRevokePayout);
        DomEvents.bindEvent("revokePayoutConfirm", "click", ".confirm", TransferDetailsDomEvents.revokePayout);
        DomEvents.bindEvent("payoutstable", "click", ".select-chkbox", TransferDetailsDomEvents.rowSelect);
        DomEvents.bindEvent("payoutstable", "click", ".selectall", TransferDetailsDomEvents.selectAllChkbox);
        DomEvents.bindEvent("bulk-action-section", "click", ".action-revoke", TransferDetailsDomEvents.confirmRevokePayout);
        DomEvents.bindEvent("bulk-action-section", "click", ".clear", TransferDetailsDomEvents.cancelBulkRevokePayout);
        DomEvents.bindEvent("payoutstable", "click", ".view-details", TransferDetailsDomEvents.viewOrderHistoryList);        
    },

    cancelPayout: function (orderId, payoutId, cancelAllScheduledPayouts) {
        return OrderSvc.cancelPayout(orderId, payoutId, cancelAllScheduledPayouts);
    },

    revokePayout: function (orderId, payoutId, remarks) {
        return OrderSvc.revokePayout(orderId, payoutId, remarks);
    },
    downloadOrderFile: function (url) {
        return OrderSvc.downloadOrderFile(url);
    },
    downloadOrderReport: function (url) {
        return OrderSvc.downloadOrderReport(url);
    }
}



$('#revokeOptions').on("change", function () {

    if ($('#revokeOptions option:selected').text() == "others (please specify)") {

        $('#revokeOthersSpecifyOption').show();
    }
    else {
        $('#revokeOthersSpecifyOption').hide();
    }
});

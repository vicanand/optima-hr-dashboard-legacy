import DomEventHandler from '../common/domEventHandler';
import CorpDetailsDOMEvents from '../dom-events/corpDetailsDOMEvents';

export
default {
    init: function() { 
        DomEventHandler.bindEvent('completeSetup', 'click', CorpDetailsDOMEvents.complteSetup);
        DomEventHandler.bindClassEvent('input-item', 'keyup change', CorpDetailsDOMEvents.validateItem);
        DomEventHandler.bindEvent('companyCountry', 'change', CorpDetailsDOMEvents.validateCountry);
    }
}

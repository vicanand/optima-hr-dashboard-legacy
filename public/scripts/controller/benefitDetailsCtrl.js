import BenefitSvc from "../service/benefitSvc";
import BenifitDomEvents from "../dom-events/benefitDetailsDOMEvent";
import Chart from "../../lib/chart.js/src/chart";
import Constants from "../common/constants";
import DomEvents from "../common/domEventHandler";
import EmployeeSvc from "../service/employeeSvc";
import OrderSvc from '../service/ordersService';
import TopMerchantsTemplate from "../../template/section/benefits/top-merchants.ejs";
import Util from '../common/util';
import DomEventHandler from '../common/domEventHandler';
import Storage from '../common/webStorage';
import CacheDataStorage from '../common/cacheDataStorage';

let _loadTimeHistogramGraph = function(graphId, graphData, graphLabels, graphType, bgColor) {
    var ctx = document.getElementById(graphId);
    var myChart = new Chart(ctx, {
        type: graphType,
        data: {
            labels: graphLabels,
            datasets: [{
                data: graphData,
                backgroundColor: bgColor,
                borderWidth: 0
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false
                    }
                }],
                yAxes: [{
                    gridLines: {
                        display: false
                    }
                }]
            }
        }
    });
};

let drawMarkersMap = function(locationData) {
    google.charts.load('visualization', '1', {
        'packages': ['geochart']
    });
    google.charts.setOnLoadCallback(function() {
        var output = locationData.map(function(obj) {
            return Object.keys(obj).sort().map(function(key) {
                return obj[key];
            });
        });
        drawRegionsMap(output);
    });
};

function drawRegionsMap(locationData) {
    var heading = [
        ['CITY', 'AVG']
    ];
    locationData = heading.concat(locationData);
    var data = google.visualization.arrayToDataTable(locationData);
    var options = {
        region: 'IN',
        displayMode: 'markers',
        colorAxis: {
            colors: ['#633ea5']
        }
    };

    const country_configs = CacheDataStorage.getItem(Constants.COUNTRY_CONFIGS);
    if (country_configs) {
        const regionConfig = _.get(country_configs, 'benefitDetailsCtrl.drawRegionsMap.region');
        if (regionConfig) {
            options.region = regionConfig;
        }
    }

    var chart = new google.visualization.GeoChart(document.getElementById('spendLocations'));
    chart.draw(data, options);
}

export
default {
    init: function() {
        var _this = this;
        _this.loadDatePicker();
        //DomEventHandler.bindEvent('payoutAdd', 'click', BenifitDomEvents.callAddPayout);


        DomEvents.bindEvent("transferOrderTable", "click", ".addPayout", BenifitDomEvents.callAddPayout);
        DomEvents.bindEvent("benifitsEmployees", "click", ".closeCardBtn", BenifitDomEvents.closeCard);
        DomEvents.bindEvent("transferOrderTable", "click", ".cancelOrderBtn", BenifitDomEvents.confirmCancel);
        DomEvents.bindEvent("cancelOrderConfirm", "click", ".reset", BenifitDomEvents.resetCancel);
        DomEvents.bindEvent("cancelOrderConfirm", "click", ".confirm", BenifitDomEvents.cancelOrder);
        DomEvents.bindEvent("cancelOrderSuccess", "click", ".success", BenifitDomEvents.cancelOrderSuccess);
        DomEvents.bindEvent('fundingAccountID', 'change', BenifitDomEvents.fundingAccountIDChange);
        DomEvents.bindEvent('newfundingaccname', 'keyup', BenifitDomEvents.newFundingAccountName);
        DomEvents.bindEvent('programName', 'keyup', BenifitDomEvents.programNameChange);
        DomEvents.bindEvent('createProgram', 'click', BenifitDomEvents.changeAndSetupProgram);
        DomEvents.bindEvent('reload-funding-changes', 'click', BenifitDomEvents.reload);
        DomEvents.bindClassEvent('benefit-new-transfer-order','click',BenifitDomEvents.benefitTransferOrder);
        DomEvents.bindEvent('addPayOrderIdButton', 'click', BenifitDomEvents.moveSingleToAddPayout);
        DomEvents.bindEvent('addRecurringOrderIdButton', 'click', BenifitDomEvents.moveToMultipleAddPayout);

        DomEventHandler.bindEvent('cardLogo', 'change', BenifitDomEvents.uploadCardProgramLogo);

        let currentUrl = window.location.href;
        if (currentUrl.indexOf('#') !== -1) {
            var selectedId = currentUrl.substr(currentUrl.lastIndexOf('#'), currentUrl.length);
        }

        $("document").ready(function() {
            $(selectedId).trigger('click');
            $('.tabContainer').css('display', 'block');
        });

        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });

        $('.nav.nav-tabs > li > a').click(function(e){
            var data = $(this).attr("id");
            if(history.pushState) {
                history.pushState(null, null, '#'+data);
            }
            else {
                window.location.hash = '#'+data;
            }
            e.preventDefault();
            $(this).tab('show');
        });

        BenefitSvc.getTopMerchants('GET_TOP_MERCHANTS').then(function(topMerchantsData) {
            $('#merchantGraphLoader').hide();
            topMerchantsData.aggregations.totalUniqueMerchants = Util.formatNumber(topMerchantsData.aggregations.totalUniqueMerchants);
            for (var i = 0; i < topMerchantsData.aggregations.topLocations.length; i++) {
                topMerchantsData.aggregations.topLocations[i].count = parseFloat((topMerchantsData.aggregations.topLocations[i].count / topMerchantsData.aggregations.total) * 100).toFixed(2) + '%';
            }
            DomEvents.renderMyTemplate("merchantsWrpr", TopMerchantsTemplate, topMerchantsData);
        }, function(respErr) {
            console.log(respErr.status == 401);
            if (respErr.status == 401) {
                Util.renewAuth();
            }
            console.log(respErr);
            $('#merchantGraphLoader').hide();
            $('#merchantGraphError').show();
        });

        BenefitSvc.getTopMerchants('GET_SPEND_CHANNELS').then(function(spendChannelsData) {
            console.log(spendChannelsData);

            let data = [];
            let labels = [];
            $('#spendDataLoader').hide();
            $('#spendChannelsWrpr').show();

            let appLabel;
            const country_configs = CacheDataStorage.getItem(Constants.COUNTRY_CONFIGS);
            if (country_configs) {
                appLabel = _.get(country_configs, 'benefitDetailsCtrl.spendChannels.AppLabel');
            }

            for (var i = 0; i < spendChannelsData.aggregations.spendChannels.length; i++) {
                let totalCount = spendChannelsData.aggregations.total;
                labels[i] = Constants.SPEND_CHANNELS_TYPE[spendChannelsData.aggregations.spendChannels[i].channelType];
                
                if (appLabel && labels[i] == 'App') {
                    labels[i] = appLabel;
                }
                if (totalCount) {
                    data[i] = parseFloat((spendChannelsData.aggregations.spendChannels[i].count / totalCount) * 100).toFixed(2);
                } else {
                    data[i] = 0;
                }
            }
            if (data.length < 2 && !data[0]) {
                $('#spendChannelsWrpr').hide();
                $('#spendChannelsEmpty').show();
            } else {
                _this.loadGraph("spendChannels", data, labels, 'doughnut');
            }
        }, function(respErr) {
            console.log(respErr.status == 401);
            if (respErr.status == 401) {
                Util.renewAuth();
            }
            $('#spendDataLoader').hide();
            $('#spendChannelsGraphErr').show();
        });

        BenefitSvc.getTopMerchants('GET_TIME_HISTOGRAM').then(function(spendTimeData) {
            let data = [];
            let bgColor = [];
            let labels = [];
            $('#spendTimesLoader').hide();
            $('#spendTimesWrpr').show();
            for (var i = 0; i < spendTimeData.aggregations.timeHistograms.length; i++) {
                labels[i] = Util.timeStampToTime(spendTimeData.aggregations.timeHistograms[i].timeStamp);
                data[i] = spendTimeData.aggregations.timeHistograms[i].count;
                bgColor[i] = '#633ea5';
            }
            if (data.length == 0) {
                $('#spendTimesWrpr').hide();
                $('#spendTimesEmpty').show();
            } else {
                _loadTimeHistogramGraph("spendTimes", data, labels, 'bar', bgColor);
            }
        }, function(respErr) {
            $('#spendTimesLoader').hide();
            $('#spendTimesGraphErr').show();
        });

        BenefitSvc.getTopMerchants('GET_TOP_LOCATIONS').then(function(topLocationData) {
            var topLocationsArr = topLocationData.aggregations.topLocations;
            $('#spendLocationsLoader').hide();
            $('#spendLocations').show();
            $('.spend-location .footer-note').show();

            if (topLocationsArr.length > 0) {
                drawMarkersMap(topLocationsArr);
            } else {
                $('#spendLocations').hide();
                $('#spendLocationsEmpty').show();
                $('.spend-location .footer-note').hide();
            }

        }, function(respErr) {
            $('#spendLocationsLoader').hide();
            $('#spendLocationsGraphErr').show();
        });
    },
    loadDatePicker: function() {
        $('#reportDate,#orderDate,#beneficiarySortDate').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
        });
    },
    closeCard: function(companyId, employeeId) {
        return EmployeeSvc.closeCard(companyId, employeeId);
    },
    cancelOrder: function(orderId) {
        return OrderSvc.cancelOrder(orderId);
    },
    loadGraph: function(graphId, graphData, graphLabels, graphType) {
        var ctx = document.getElementById(graphId);
        var myChart = new Chart(ctx, {
            type: graphType,
            data: {
                labels: graphLabels,
                datasets: [{
                    data: graphData,
                    backgroundColor: [
                        '#633ea5',
                        '#00b898',
                        '#eb3b8c'
                    ],
                    borderWidth: 0
                }]
            },
            options: {
                legend: {
                    display: true
                }
            }
        });
    }
}
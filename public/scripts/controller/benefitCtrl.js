import BenefitDOMEvents from '../dom-events/benefitDOMEvents';
import DomEventHandler from '../common/domEventHandler';

export
default {
    init: function() {
        DomEventHandler.bindEvent('fundingAccountID', 'change', BenefitDOMEvents.fundingAccountIDChange);
        DomEventHandler.bindEvent('newfundingaccname', 'keyup', BenefitDOMEvents.newFundingAccountName);
        DomEventHandler.bindEvent('setupLtaCheckbox', 'change', BenefitDOMEvents.setupLtaCheckbox);
        DomEventHandler.bindEvent('programName', 'keyup', BenefitDOMEvents.programNameChange);
        DomEventHandler.bindEvent('createProgram', 'click', BenefitDOMEvents.createProgram);

        DomEventHandler.bindEvent('finishProgramSetup', 'click', BenefitDOMEvents.createCustomProgram);
        DomEventHandler.bindEvent('showConfirmModal', 'click', BenefitDOMEvents.showConfirmModal);


        DomEventHandler.bindEvent('scheduleYearDone', 'click', BenefitDOMEvents.scheduleYearDone);
        DomEventHandler.bindEvent('closureDate', 'change', BenefitDOMEvents.closureDate);
        DomEventHandler.bindEvent('scheduleYear', 'click', BenefitDOMEvents.scheduleYear);
        DomEventHandler.bindEvent('closedCards', 'click', BenefitDOMEvents.showClosedCards);
        DomEventHandler.bindEvent('activeProgramsTab', 'click', BenefitDOMEvents.loadActiveCards); // for active programs load more button
        DomEventHandler.bindEvent('closedProgramsTab', 'click', BenefitDOMEvents.loadClosedCards); // for closed programs load more button
        DomEventHandler.bindEvent('close_Me', 'click', BenefitDOMEvents.close_Me);
        DomEventHandler.bindClassEvent('active-card-programs', 'click', BenefitDOMEvents.benefitSelected);
        DomEventHandler.bindClassEvent('inactive-card-programs', 'click', BenefitDOMEvents.benefitSelected);
        BenefitDOMEvents.saveCompanyInfoToSession();
        BenefitDOMEvents.refreshCorpAccountsInfo();

        BenefitDOMEvents.initProgramSetupEvents();

        $('#setupProgramModal').on('show.bs.modal', function(e) {
            if (e.relatedTarget != undefined) {
              $('#programType').html(e.relatedTarget.dataset.programname);
              $('#programName').val(e.relatedTarget.dataset.programname);
              $('#createProgram').attr('data-programType', e.relatedTarget.dataset.programtype);
                if(e.relatedTarget.dataset.programtype.toLowerCase() == "lta"){
                    $('#setupLtaCheckboxDv').show();
                }
                else{$('#setupLtaCheckboxDv').hide();}
            }
        });

    },
    loadDatePicker: function() {
        var dateToday = new Date();
        dateToday.setDate(dateToday.getDate() + 20);
        $('#closureDate').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            startDate: dateToday,
            endDate: "27/03/2017"
        });
    }
}
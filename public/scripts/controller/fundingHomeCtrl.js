import DomEventHandler from '../common/domEventHandler.js';
import FundingDOMEvents from '../dom-events/fundingHomeDOMEvent.js';
import Storage from '../common/webStorage';
import Constants from '../common/constants';

export
default {
    init: function() {
        $('#transferFundsModal').on('show.bs.modal', function(e) {
            $('#transferFundsModal').children().find('select').val('select');
            $('#transferFundsModal').children().find('select').removeClass('input-has-error');

            $('#transferFundsModal').children().find('input').val("");
            $('#transferFundsModal').children().find('input').removeClass('input-has-error');
            $('#transferFundsModal').children().find('.error-message').html('');
            $('#fundTransferBtn').prop('disabled', true);
            $('#fundTransferBtn').addClass('btn-disabled');

            let accno = e.relatedTarget.dataset.accno;
            let xaccno = '*****';
            xaccno = xaccno.concat(accno.substr(accno.length-4));
            $('#accDetails').data('accname', e.relatedTarget.dataset.accname)
            $('#accDetails').data('accbalpaise', e.relatedTarget.dataset.accbalpaise);
            $('#accDetails').data('curraccno', accno);
            $('#accDetails').data('accountid', e.relatedTarget.dataset.accountid);
            $('#accDetails').html(e.relatedTarget.dataset.accname +" "+ xaccno +"<span class='bal-info'> - Current Balance: </span>" +e.relatedTarget.dataset.accbal);
            $('.transferToAccs').show();
            $('#transferTo option[value=' + e.relatedTarget.dataset.accountid +']').hide();
        });
        this.registerDOMEvents();
    },
    init1: function(){
        $('#fundTransferBtn').off('click').on('click', function(){
            FundingDOMEvents.transferFunds();
        })
    },
    registerDOMEvents: function() {
        DomEventHandler.bindEvent('openNewFundAcc', 'click', FundingDOMEvents.openNewFundAcc);
        DomEventHandler.bindEvent('routeToAddFund', 'click', FundingDOMEvents.routeToAddFund);
        DomEventHandler.bindEvent('recipient-name', 'keyup', FundingDOMEvents.enableOpenAccBtn);
        DomEventHandler.bindEvent('openAddFundModal', 'click', FundingDOMEvents.clearInpVal);
        DomEventHandler.bindEvent('trans-history', 'click', FundingDOMEvents.captureFundID);
        DomEventHandler.bindClassEvent('addFundsOnHome', 'click', FundingDOMEvents.addFundsOnHome);
        DomEventHandler.bindClassEvent('show-program-funded', 'click', FundingDOMEvents.showProgramsFundedModal);
        DomEventHandler.bindEvent('transferTo', 'change', FundingDOMEvents.activateFundsTransfer);
        DomEventHandler.bindEvent('transferAmount', 'keyup', FundingDOMEvents.activateFundsTransfer);
        DomEventHandler.bindEvent('transferAmount', 'keydown', FundingDOMEvents.restrictInvalidAmount);
        //DomEventHandler.bindEvent('fundTransferBtn', 'click', FundingDOMEvents.transferFunds);
        DomEventHandler.bindEvent('responseBtn', 'click', FundingDOMEvents.reloadPage);
    }

}


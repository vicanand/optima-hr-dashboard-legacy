import DomEventHandler from '../common/domEventHandler';
import ReportCentreDOMEvent from '../dom-events/reportDetailsDOMEvent';

export default{
    init: function(){
        DomEventHandler.bindClassEvent('reportDownloadType', 'click', ReportCentreDOMEvent.downloadReport);
    }
}

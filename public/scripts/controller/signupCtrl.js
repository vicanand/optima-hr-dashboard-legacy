import DomEventHandler from '../common/domEventHandler';
import SignupDOMEvents from '../dom-events/signupDOMEvents';
export
default {
    init: function() {
        let $city = $('#city');
        var autocomplete = new google.maps.places.Autocomplete($('#city')[0], {
            types: ['(cities)'],
            componentRestrictions: { country: 'in' }
        });
        DomEventHandler.bindEvent('createAccount', 'click', SignupDOMEvents.createAccount);
        DomEventHandler.bindClassEvent('signup__form___input', 'change', SignupDOMEvents.inputChange);
        DomEventHandler.bindClassEvent('signup__form___textarea', 'change', SignupDOMEvents.inputChange);
        DomEventHandler.bindEvent('panNo', 'change', SignupDOMEvents.panNoChange);
        DomEventHandler.bindEvent('noOfEmployees', 'change', SignupDOMEvents.noOfEmployeesChange);
        DomEventHandler.bindEvent('cinImageFile', 'change', SignupDOMEvents.uploadCINFile);
        DomEventHandler.bindEvent('panImageFile', 'change', SignupDOMEvents.uploadPANFile);
        DomEventHandler.bindEvent('gstinImageFile', 'change', SignupDOMEvents.uploadGSTINFile);
        DomEventHandler.bindEvent('code', 'change', SignupDOMEvents.codeChange);
        DomEventHandler.bindEvent('emailVerifyBtn', 'click', SignupDOMEvents.verifyEmail);
        DomEventHandler.bindEvent('resendCode', 'click', SignupDOMEvents.resendCode);
        DomEventHandler.bindEvent('submitNow', 'click', SignupDOMEvents.signUpSubmit);
        DomEventHandler.bindEvent('signUpTandC', 'change', SignupDOMEvents.acceptTandC);
    }
}
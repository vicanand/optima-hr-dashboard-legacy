import DomEventHandler from '../common/domEventHandler';
import ReportCentreDOMEvent from '../dom-events/reportCentreDOMEvent';

export
default {
    init: function() {
        console.log('REPORT CONTROLLER INITIALIZED');
        DomEventHandler.bindClassEvent('reportModal', 'click', ReportCentreDOMEvent.reportModal);
        DomEventHandler.bindEvent('generateReport', 'click', ReportCentreDOMEvent.generateReport);
        // DomEventHandler.bindEvent('downloadReport', 'click', ReportCentreDOMEvent.downloadReport);
        DomEventHandler.bindEvent('tryAgainBtn', 'click', ReportCentreDOMEvent.hideModal);
        DomEventHandler.bindClassEvent('new-error-button','click',ReportCentreDOMEvent.hideMe);
        DomEventHandler.bindEvent('filterReportOnState', 'change', ReportCentreDOMEvent.loadTable);
        DomEventHandler.bindClassEvent('generatedReportTab', 'click', ReportCentreDOMEvent.loadTable);
    },
    loadDatePicker: function() {
        var todayDate = new Date();
        $("#start_date").datepicker({
            format: "dd M yyyy",
            autoclose: true,
        }).on('changeDate', function(selected) {
            var startDate = new Date(selected.date.valueOf());
            $('#end_date').datepicker('setStartDate', startDate);
        });

        $("#end_date").datepicker({
            format: "dd M yyyy",
            autoclose: true,
            endDate: todayDate
        }).on('changeDate', function(selected) {
            var enddate = new Date(selected.date.valueOf());
            $('#start_date').datepicker('setEndDate', enddate);
        });
    }
}

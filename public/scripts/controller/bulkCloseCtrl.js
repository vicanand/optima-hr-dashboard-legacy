import Constant from '../common/constants';
import DomEventHandler from '../common/domEventHandler';
import BulkCloseDOMEvent from '../dom-events/bulkCloseDOMEvent';
import Storage from '../common/webStorage';

let dropFlag = 0;
let _initDropZone = function(){
    var uploadDetails = $('#uploadDetails').val();
    uploadDetails = JSON.parse(uploadDetails);
    $.Dropzone.options.dragDrop = {
        addRemoveLinks: true,
        paramName: "file",
        previewsContainer: null,
        // clickable: true,
        acceptedFiles: '.csv,.xls,.xlsx',
        // error: true,
        dictDefaultMessage: '',
        uploadMultiple: false,
        maxFiles: 1,
        previewTemplate: document.getElementById('preview-template').innerHTML,
        url: uploadDetails.postAction,
        headers : {
            "Cache-Control": "",
            "X-Requested-With": "",
            "X-File-Name" : ""
        },
        accept: function(file, done) {
            dropFlag = 1;
            console.log('uploaded');
            done();
        },
        init: function() {
            var uploadBtn = $('#dragDrop .browse-wrap');
            uploadBtn.removeClass('btn-disabled');
            this.on('addedfile', function(file) {
                console.log(file);
                if (this.files[1] != null) {
                    this.removeFile(this.files[0]);
                }
            });
            this.on("uploadprogress", function(file, progress) {
                console.log("File progress", progress);
            });
            this.on('canceled', resetForm);
            this.on('removedfile', resetForm);
            this.on('success', function(respData){
                console.log(respData);
                BulkCloseDOMEvent.validationForTransfer();
                let key = respData.xhr.response.split('<Key>')[1].split('</Key>')[0];
                let etag = respData.xhr.response.split('<ETag>')[1].split('</ETag>')[0].replace('"','').replace('"','');
                let fileUrl = respData.xhr.response.split('<Location>')[1].split('</Location>')[0] + '?key=' + key + '&etag=' + etag;
                Storage.set(Constant.STORAGE_FILE_URL, fileUrl);

            });
            this.on('error', function(file, message) {
                var errorEle = $('.dz-error-message');
                errorEle.find('span')
                    .text(Constant.FILE_UPLOAD_ERROR);
                errorEle.show();
                //  alert(message);
                // this.removeFile(file);
            });
        },
        sending: function(file, xhr, formData) {
            Storage.set(Constant.STORAGE_FILE_TYPE, file.type);
            switch (file.type) {
                case "text/csv":
                    $(".fileType").attr("src", "/images/group-12.png");
                    break;
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    $(".fileType").attr("src", "/images/group-xlsx.png");
                    break;
                case "application/vnd.ms-excel":
                    $(".fileType").attr("src", "/images/group-xls.png");
                    break;
            }
            Storage.set(Constant.STORAGE_FILE_NAME, file.name);
            formData.append("key", uploadDetails.key);
            formData.append("acl", uploadDetails.acl);
            formData.append("x-amz-credential", uploadDetails["xAmzCredential"]);
            formData.append("x-amz-algorithm", uploadDetails["xAmzAlgorithm"]);
            formData.append("success_action_status", uploadDetails.successActionStatus);
            formData.append("x-amz-date", uploadDetails["xAmzDate"]);
            formData.append("policy", uploadDetails.policy);
            formData.append("x-amz-signature", uploadDetails["xAmzSignature"]);
            formData.append('content-type', file.type);
        }
    };
};

let resetForm = function() {
    dropFlag = 0;
    $('#create-order').attr('disabled', 'disabled').addClass('btn-disabled');
    $('#companies').val('0');
};

let init = function() {
    _initDropZone();
    DomEventHandler.bindEvent('companies', 'change', BulkCloseDOMEvent.validationForTransfer);
    DomEventHandler.bindEvent('create-order', 'click', BulkCloseDOMEvent.initiateTransfer);
    DomEventHandler.bindEvent('termsConditions', 'change', BulkCloseDOMEvent.validationForTransfer);
    DomEventHandler.bindEvent('retryBtn', 'click', BulkCloseDOMEvent.initiateTransfer);
    DomEventHandler.bindEvent('uploadAgain', 'click', BulkCloseDOMEvent.uploadAgain);
    DomEventHandler.bindEvent('uploadAgainFile', 'click', BulkCloseDOMEvent.uploadAgainFile);
    DomEventHandler.bindEvent('continueValid', 'click', BulkCloseDOMEvent.continueValid);
    DomEventHandler.bindEvent('createBulkTransferOrder', 'click', BulkCloseDOMEvent.createBulkTransferOrder);
    DomEventHandler.bindEvent('retryBulkOrderBtn', 'click', BulkCloseDOMEvent.createBulkTransferOrder);
    DomEventHandler.bindEvent('uploadAgainRetryBtn', 'click', BulkCloseDOMEvent.uploadAgain);
    /*DomEventHandler.bindEvent('uploadStep', 'click', BulkCloseDOMEvent.gotoUploadStep);*/

};

let setFlag = function() {
    return dropFlag;
};



export
default {
    setFlag: setFlag,
    init: init
}
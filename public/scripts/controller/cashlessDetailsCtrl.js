import DomEvents from "../common/domEventHandler";
import Constants from "../common/constants";
import BenifitDomEvents from "../dom-events/benefitDetailsDOMEvent";
import EmployeeSvc from "../service/employeeSvc";
import BenefitSvc from "../service/benefitSvc";
import OrderSvc from '../service/ordersService';
import TopMerchantsTemplate from "../../template/section/benefits/top-merchants.ejs";
import Chart from "../../lib/chart.js/src/chart";
import Util from '../common/util';

export
default {
    init: function() {
        var _this = this;
        _this.loadDatePicker();
        DomEvents.bindEvent("benifitsEmployees", "click", ".closeCardBtn", BenifitDomEvents.closeCard);

        DomEvents.bindEvent("transferOrderTable", "click", ".cancelOrderBtn", BenifitDomEvents.confirmCancel);

        DomEvents.bindEvent("cancelOrderConfirm", "click", ".reset", BenifitDomEvents.resetCancel);

        DomEvents.bindEvent("cancelOrderConfirm", "click", ".confirm", BenifitDomEvents.cancelOrder);

        DomEvents.bindEvent("cancelOrderSuccess", "click", ".success", BenifitDomEvents.cancelOrderSuccess);

        DomEvents.bindClassEvent('transferorder-row','click','.downloadOrderFile',BenifitDomEvents.downloadOrderFile);  
       
        DomEvents.bindClassEvent('transferorder-row','click','.downloadOrderReport',BenifitDomEvents.downloadOrderReport);        

        BenefitSvc.getTopMerchants('GET_TOP_MERCHANTS').then(function(topMerchantsData) {
            $('#merchantGraphLoader').hide();
            topMerchantsData.aggregations.totalUniqueMerchants = Util.formatNumber(topMerchantsData.aggregations.totalUniqueMerchants);
            for (var i = 0; i < topMerchantsData.aggregations.topLocations.length; i++) {
                topMerchantsData.aggregations.topLocations[i].count = parseFloat((topMerchantsData.aggregations.topLocations[i].count / topMerchantsData.aggregations.total) * 100).toFixed(2) + '%';
            }
            DomEvents.renderMyTemplate("merchantsWrpr", TopMerchantsTemplate, topMerchantsData);
        }, function(respErr) {
            $('#merchantGraphLoader').hide();
            $('#merchantGraphError').show();
        });
    },
    loadDatePicker: function() {
        $('#reportDate,#orderDate,#beneficiarySortDate').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
        });
    },
    closeCard: function(companyId, employeeId) {
        return EmployeeSvc.closeCard(companyId, employeeId);
    },
    cancelOrder: function(orderId) {
        return OrderSvc.cancelOrder(orderId);
    },
    loadGraph: function(graphId, graphData, graphLabels, graphType) {
        var ctx = document.getElementById(graphId);
        var myChart = new Chart(ctx, {
            type: graphType,
            data: {
                labels: graphLabels,
                datasets: [{
                    data: graphData,
                    backgroundColor: [
                        '#633ea5',
                        '#00b898',
                        '#eb3b8c'
                    ],
                    borderWidth: 0
                }]
            },
            options: {
                legend: {
                    display: true
                }
            }
        });
    }
}

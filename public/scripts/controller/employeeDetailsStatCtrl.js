import DomEventHandler from '../common/domEventHandler.js';
import BeneficiaryDOMEvents from '../dom-events/cardProgramIndividualBeneficiaryDomEvent.js';


export
default {
    init: function() {
        $('#employee-program-details').on('click', '#schedule-table-id', function () {
          //  let cardId = $(this).data("card-id"); //cardTransferReportTable-5809588924436242000
          //  let text =$(this).text();
           // alert(cardId);
            BeneficiaryDOMEvents.showHideScheduledTable();
       });
       $('#waiting-amount-id').off('click').on('click', '#waiting-table-id', function () {
          BeneficiaryDOMEvents.showHideWaitingTable();
       });
        $('#employee-program-details').on('click', '#canceled-table-id', function () {
            BeneficiaryDOMEvents.showHideCanceledTable();
       });
    }
}
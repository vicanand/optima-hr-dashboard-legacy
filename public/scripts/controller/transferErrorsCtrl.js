import NewTransferSvc from '../service/newTransferSvc';
import DomEventHandler from '../common/domEventHandler';
import TransferErrorsDOMEvent from '../dom-events/transferErrorsDOMEvent';
import TableWidget from '../widgets/table-widget';
import Storage from '../common/webStorage';
import Constant from '../common/constants';

export
default {
    init: function() {
        TableWidget.init();
        $("#transferErrors").tablewidget({
            source: NewTransferSvc.getOrderSummary,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: "/template/tables/transfer-errors.ejs",
            rowTemplate: "/template/tables/transfer-errors-row.ejs",
            extraParam: {
            }
        });
        DomEventHandler.bindClassEvent('errors-entries-type', 'click', TransferErrorsDOMEvent.showDetails);
        DomEventHandler.bindEvent('cancelOrder', 'click', TransferErrorsDOMEvent.cancelOrder);
        DomEventHandler.bindEvent('initiateTransferOrder', 'click', TransferErrorsDOMEvent.initiateTransferOrder);
    },
}

import DomEventHandler from '../common/domEventHandler';
import CashlessDOMEvents from '../dom-events/cashlessDOMEvents';

export
default {
    init: function() {
        console.log('BENEFIT CONTROLLER INITIALIZED TEST');
        $('#setupProgramModal').on('show.bs.modal', function(e) {
            $('#programType').html(e.relatedTarget.dataset.programname);
            $('#programName').val(e.relatedTarget.dataset.programname);
            $('#createProgram').attr('data-programType', e.relatedTarget.dataset.programtype);
        });
        DomEventHandler.bindEvent('programName', 'keyup', CashlessDOMEvents.programNameChange);
        DomEventHandler.bindEvent('createProgram', 'click', CashlessDOMEvents.createProgram);
        DomEventHandler.bindEvent('scheduleYearDone', 'click', CashlessDOMEvents.scheduleYearDone);
        DomEventHandler.bindEvent('closureDate', 'change', CashlessDOMEvents.closureDate);
        DomEventHandler.bindEvent('scheduleYear', 'click', CashlessDOMEvents.scheduleYear);
        DomEventHandler.bindEvent('close_Me','click',CashlessDOMEvents.close_Me);
    },
    loadDatePicker: function() {
        var dateToday = new Date();
        dateToday.setDate(dateToday.getDate() + 20);
        $('#closureDate').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            startDate: dateToday,
            endDate: "27/03/2017"
        });
    }
}

import Constant from '../common/constants';
import ServiceConnector from "../common/serviceConnector";
import DomEventHandler from '../common/domEventHandler';
import cardorderDomEvent from '../dom-events/cardorderDOMEvent';
import Storage from '../common/webStorage';
import CardorderSvc from '../service/cardorderSvc';
import moment from '../../lib/moment/moment';



let dropFlag = 0;
let _initDropZone = function () {


    var uploadDetails = $('#uploadDetails').val();
    uploadDetails = JSON.parse(uploadDetails);
    $.Dropzone.options.dragDrop = {
        addRemoveLinks: true,
        paramName: "file",
        previewsContainer: null,
        // clickable: true,
        acceptedFiles: '.csv,.xls,.xlsx',
        // error: true,
        dictDefaultMessage: '',
        uploadMultiple: false,
        maxFiles: 1,
        previewTemplate: document.getElementById('preview-template').innerHTML,
        url: uploadDetails.postAction,
        headers: {
            "Cache-Control": "",
            "X-Requested-With": "",
            "X-File-Name": ""
        },
        accept: function (file, done) {
            dropFlag = 1;
            console.log('uploaded');
            done();
        },
        init: function () {
            var uploadBtn = $('#dragDrop .browse-wrap');
            uploadBtn.removeClass('btn-disabled');
            this.on('addedfile', function (file) {
                console.log(file);
                if (this.files[1] != null) {
                    this.removeFile(this.files[0]);
                }
            });
            this.on("uploadprogress", function (file, progress) {
                console.log("File progress", progress);
            });
            this.on('canceled', resetForm);
            this.on('removedfile', resetForm);
            this.on('success', function (respData) {
                console.log(respData);
                cardorderDomEvent.validateFile();
                let key = respData.xhr.response.split('<Key>')[1].split('</Key>')[0];
                let etag = respData.xhr.response.split('<ETag>')[1].split('</ETag>')[0].replace('"', '').replace('"', '');
                let fileUrl = respData.xhr.response.split('<Location>')[1].split('</Location>')[0] + '?key=' + key + '&etag=' + etag;
                Storage.setSessionCollection('fileurl', fileUrl);
                Storage.setSessionCollection('key', uploadDetails.key);

            });
            this.on('error', function (file, message) {
                var errorEle = $('.dz-error-message');
                errorEle.find('span')
                    .text(Constant.FILE_UPLOAD_ERROR);
                errorEle.show();
                //  alert(message);
                // this.removeFile(file);
            });
        },
        sending: function (file, xhr, formData) {
            Storage.set('filename', file.name);
            Storage.set('filetype', file.type);
            switch (file.type) {
                case "text/csv":
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-12.png");
                    break;
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xlsx.png");
                    break;
                case "application/vnd.ms-excel":
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                    break;
                default:
                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                    break;
            }
            formData.append("key", uploadDetails.key);
            formData.append("acl", uploadDetails.acl);
            formData.append("x-amz-credential", uploadDetails["xAmzCredential"]);
            formData.append("x-amz-algorithm", uploadDetails["xAmzAlgorithm"]);
            formData.append("success_action_status", uploadDetails.successActionStatus);
            formData.append("x-amz-date", uploadDetails["xAmzDate"]);
            formData.append("policy", uploadDetails.policy);
            formData.append("x-amz-signature", uploadDetails["xAmzSignature"]);
            formData.append('content-type', file.type);
        }
    };
    $('#resetFilter').hide();
};

let resetForm = function () {
    dropFlag = 0;
    $('#verify-order1, #verify-order2').attr('disabled', 'disabled').addClass('btn-disabled');
};


let init = function () {
    _initDropZone();
    DomEventHandler.bindClassEvent('rewardGift', 'click', cardorderDomEvent.selectProgramCode);
    DomEventHandler.bindClassEvent('personalization', 'click', cardorderDomEvent.personalization);
    DomEventHandler.bindEvent('verify-order1', 'click', cardorderDomEvent.createNxtBulk);
    DomEventHandler.bindEvent('verify-order2', 'click', cardorderDomEvent.createNxtManual);
    DomEventHandler.bindClassEvent('address', 'click', cardorderDomEvent.selectaddress);
    DomEventHandler.bindClassEvent('newAddDv', 'click', cardorderDomEvent.adressPopup);
    DomEventHandler.bindEvent('addcardcostAdd', 'click', cardorderDomEvent.getNewAddress);

    //DomEventHandler.bindClassEvent('cardAction', 'click', cardorderDomEvent.cardActions);
    DomEventHandler.bindClassEvent('confirmActioncall', 'click', cardorderDomEvent.takeCardAction);

    DomEventHandler.bindEvent('orderCompleted', 'click', cardorderDomEvent.orderCreate);
    DomEventHandler.bindEvent('orderCompletedNew', 'click', cardorderDomEvent.orderCreate);


    // DomEventHandler.bindEvent('duplicateEntries', 'click', cardorderDomEvent.duplicateEnrty);
    DomEventHandler.bindEvent('rewardTemplateSelected', 'click', cardorderDomEvent.showBenefits);
    // DomEventHandler.bindEvent('rewardManually', 'click', cardorderDomEvent.showManually);
    // DomEventHandler.bindEvent('rewardBulkUpload', 'click', cardorderDomEvent.showBenefitsBulk);

    DomEventHandler.bindClassEvent('addFundsSG', 'click', cardorderDomEvent.addFundsSG);
    DomEventHandler.bindEvent('routeToAddFund', 'click', cardorderDomEvent.routeToAddFund);
    DomEventHandler.bindClassEvent('done-redirect', 'click', cardorderDomEvent.redirectUrl);
    DomEventHandler.bindEvent('uploadStep', 'click', cardorderDomEvent.showManuallyOnStep);
    DomEventHandler.bindEvent('chooseStep', 'click', cardorderDomEvent.showChooseDesign);
    DomEventHandler.bindEvent('verifyStep', 'click', cardorderDomEvent.showVerifyAndPay);

    // maintaining state
    (function (window) {
        // exit if the browser implements that event
        if ("onhashchange" in window) {
            return;
        }

        var location = window.location,
            oldURL = location.href,
            oldHash = location.hash;
        // check the location hash on a 100ms interval
        setInterval(function () {
            var newURL = location.href,
                newHash = location.hash;
            // if the hash has changed and a handler has been bound...
            if (newHash != oldHash && typeof window.onhashchange === "function") {
                // execute the handler
                window.onhashchange({
                    type: "hashchange",
                    oldURL: oldURL,
                    newURL: newURL
                });

                oldURL = newURL;
                oldHash = newHash;
            }
        }, 10);
    })(window);

    $(window).on('hashchange', function () {
        var urlHash = window.location.hash;
        if (urlHash == '#Step2') {
            $('#chooseDesign').hide();
            $('#createOrder').hide();
            //$('#createOrder').hide();
            $('#uploadStep').addClass('active');
            $('#uploadStep').removeClass('done');
            $('#verifyStep').removeClass('active');
            $('#verifyStep').removeClass('done');
            $('#virtualGiftContainer').show();
            // if(Storage.getSessionCollection('bulk') == true){
            //     $('#addBenefits').hide();
            //     $('#addBenefitsBulk').show();
            // }else{
            //     $('#addBenefits').show();
            //     $('#addBenefitsBulk').hide();
            // }

        } else if (urlHash == '#orderHistory') {
            $(urlHash).trigger("click");
            $('#createOrder').hide();
        } else if (urlHash == '#disbursedHistory') {
            $(urlHash).trigger("click");
            $('#createOrder').hide();
        } else if (urlHash == '#cardOrder') {
            $(urlHash).trigger("click");
            $('#chooseDesign').show();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#fileErrorWrpr').hide();
            $('#virtualGiftContainer').show();
            $('#createOrder').hide();
            $('#chooseStep').removeClass('done');
            $('#uploadStep').removeClass('active');
            $('#uploadStep').removeClass('done');
            Storage.setSessionCollection('bulk', false);
            Storage.setSessionCollection('manual', true);
        } else if (urlHash == '#Step3Manual') {
            $('#chooseDesign').hide();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#fileErrorWrpr').hide();
            $('#virtualGiftContainer').show();
            $('#createOrder').show();
        } else if (urlHash == '#Step3Bulk') {
            $('#chooseDesign').hide();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#fileErrorWrpr').hide();
            $('#virtualGiftContainer').show();
            $('#createOrder').show();
        } else {
            var targetUrl = 'cards';
            window.history.pushState({
                url: "" + targetUrl + ""
            }, targetUrl);
            window.location.hash = targetUrl;
            Storage.setSessionCollection('bulk', false);
            Storage.setSessionCollection('manual', true);
            $('#virtualGiftContainer').show();
            $('#chooseDesign').show();
            $('#addBenefits').hide();
            $('#addBenefitsBulk').hide();
            $('#chooseStep').removeClass('done');
            $('#uploadStep').removeClass('active');
            $('#uploadStep').removeClass('done');
            $('#createOrder').hide();
        }
    });


    // $('body').on('click', '.append-amount', function () {
    //     let amount = $(this).data('amount');
    //     let id = $(this).data('id');
    //     $('.input-amount-validate' + id).val(amount);
    // });


    $('body').on('click', '.addFundsSG', function () {
        let accountId = $(this).attr('data-fundingaccountids');
        Storage.set('ACCOUNT_ID', accountId);
        let corporateId = $("meta[name='corpID']").attr("content");

        let authToken = Storage.get('authToken');
        CardorderSvc.getAccDetails(authToken, corporateId, accountId).then(function (fundRespData) {
            let fundingAcc = fundRespData;
            Storage.setSessionCollection('fundingAccountName', fundingAcc.name);
            Storage.setSessionCollection('fundingAccountBalance', (fundingAcc.balance) / 100);
            Storage.setSessionCollection('ifiID', (fundingAcc.ifiID));
            cardorderDomEvent.addFundsSG();
        }, function (respErr) {
            console.log(respErr);
        });

    });

    // $('body').on('change','.inputFieldValidate',function(){
    //     $('#rewardSingleSelected').removeAttr('disabled').removeClass('btn-disabled');
    // });  
    $('body').on('change keyup keypress focus', '#referenceName', cardorderDomEvent.validateFile);
    $('body').on('change keyup keypress focus', '.manualValidate', cardorderDomEvent.validationForManualTransfer);
    // $('.hdfcAmountValidate').change(amountchange);

    $('.addUserFields').keyup(function () {
        let $name = $('#name');
        let $phoneNumber = $('#phoneNumber');
        let $email = $('#email');
        let $address = $('#address');
        let $zipcode = $('#zipcode');
        let $city = $('#city');
        let $state = $('#state');
        let $country = $('#country');

        let email = $email.val();
        let name = $name.val();
        let phoneNumber = $phoneNumber.val();
        let address = $address.val();
        let zipcode = $zipcode.val();
        let state = $state.val();
        let city = $city.val();
        let country = $country.val();
        let emailisValid, nameisValid, phoneNumberisValid, addressisValid, cityisValid, stateisValid, countryisValid, zipcodeisValid;

        var emailRegex = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;

        if (name == '' && $(this).attr('id') == 'name') {
            $name.addClass('input-has-error');
            $name.siblings('.error-message').html('Please enter your name').show();
            nameisValid = false;
        } else {
            $name.removeClass('input-has-error');
            $name.siblings('.error-message').html('Please enter your reference name').hide();
            nameisValid = true;
        };

        if (phoneNumber == '' && $(this).attr('id') == 'phoneNumber') {
            $phoneNumber.addClass('input-has-error');
            $('.error-message-phone').html('Please enter your phone number').show().css({
                'color': '#e74c3c',
                'opacity': 1
            });
            phoneNumberisValid = false;
        } else if (phoneNumber != '' && phoneNumber.length < 10) {
            $phoneNumber.addClass('input-has-error');
            $('.error-message-phone').html('Please enter valid 10 digit phone number').show().css({
                'color': '#e74c3c',
                'opacity': 1
            });
            phoneNumberisValid = false;
        } else {
            $phoneNumber.removeClass('input-has-error');
            $('.error-message-phone').hide();
            phoneNumberisValid = true;
        };
        if (email == '' && $(this).attr('id') == 'email') {
            $email.addClass('input-has-error');
            $email.siblings('.error-message').html('Please enter email Id').show();
            emailisValid = false;
        } else if (!emailRegex.test(email)) {
            $email.addClass('input-has-error');
            $email.siblings('.error-message').html('Please enter valid email id').show();
            emailisValid = false;
        } else {
            $email.removeClass('input-has-error');
            $email.siblings('.error-message').hide();
            emailisValid = true;
        };
        if (address == '' && $(this).attr('id') == 'address') {
            addError('Please enter your address');
        } else {
            $address.removeClass('input-has-error');
            $address.siblings('.error-message').hide();
            addressisValid = true;
            // if (address != '') {
            //     var fullAddressArray = $('#address').val().replace(/\n/g, '/n').split('/n')
            //     if (fullAddressArray.length <= 4) {
            //         for (var i = 0; i < fullAddressArray.length; i++) {
            //             if (fullAddressArray[i].length >= 50) {
            //                 addError('Please enter maximum of 50 characters in each line');
            //                 return;
            //             } else {
            //                 $address.removeClass('input-has-error');
            //                 $address.siblings('.error-message').hide();
            //                 addressisValid = true;
            //             }
            //         }
            //     } else {
            //         addError('Please enter your address');
            //     }
            // }
        };

        function addError(error) {
            $address.addClass('input-has-error');
            $address.siblings('.error-message').html(error).show();
            addressisValid = false;
        }

        if (city == '' && $(this).attr('id') == 'city') {
            $city.addClass('input-has-error');
            $city.siblings('.error-message').html('Please enter your city').show();
            cityisValid = false;
        } else {
            $city.removeClass('input-has-error');
            $city.siblings('.error-message').hide();
            cityisValid = true;
        };

        if (state == '' && $(this).attr('id') == 'state') {
            $state.addClass('input-has-error');
            $state.siblings('.error-message').html('Please enter your state').show();
            stateisValid = false;
        } else {
            $state.removeClass('input-has-error');
            $state.siblings('.error-message').hide();
            stateisValid = true;
        };

        if (country == '') {
            $country.addClass('input-has-error');
            $country.siblings('.error-message').html('Please enter your country').show();
            countryisValid = false;
        } else {
            $country.removeClass('input-has-error');
            $country.siblings('.error-message').hide();
            countryisValid = true;
        };

        if (zipcode == '' && $(this).attr('id') == 'zipcode') {
            $zipcode.addClass('input-has-error');
            $zipcode.siblings('.error-message').html('Please enter your pincode').show();
            zipcodeisValid = false;
        } else if (zipcode.slice(0, 1) == '-') {
            $zipcode.addClass('input-has-error');
            $zipcode.siblings('.error-message').html('Please enter valid pincode').show();
            zipcodeisValid = false;
        } else if (zipcode != '' && zipcode.length < 6) {
            $zipcode.addClass('input-has-error');
            $zipcode.siblings('.error-message').html('Please enter valid 6 digit pincode').show();
            zipcodeisValid = false;
        } else {
            $zipcode.removeClass('input-has-error');
            $zipcode.siblings('.error-message').hide();
            zipcodeisValid = true;
        };

        if ((emailisValid == true) &&
            (nameisValid == true) &&
            (phoneNumberisValid == true) &&
            (addressisValid == true) &&
            (cityisValid == true) &&
            (stateisValid == true) &&
            (countryisValid == true) &&
            (zipcodeisValid == true)) {
            $('.addcardcostAdd').removeClass('btn-disabled');
            $('.addcardcostAdd').prop('disabled', false);
        } else {
            $('.addcardcostAdd').addClass('btn-disabled');
            $('.addcardcostAdd').prop('disabled', true);
        }

    });

};
let setFlag = function () {
    return dropFlag;
};
let initDatePicker = function () {
    var start = moment().subtract(29, 'days').startOf('month');
    var end = moment();

    function cb(start, end) {
        $('input[name="daterange"]').val('');
    }
    $('input[name="daterange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

    $('input[name="daterange"]').daterangepicker({
        "autoApply": true,
        locale: {
            format: 'YYYY-MM-DD',
            "customRangeLabel": "Choose From Calendar",
        },
        "opens": "left",
        maxDate: new Date(),
        startDate: start,
        endDate: end,
        ranges: {
            'All': [moment().subtract(12, 'month').startOf('month'), moment()],
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(0, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
    cb(start, end);
};

export
default {
    init: init,
    setFlag: setFlag,
    resetForm: resetForm,
    initDatePicker: initDatePicker,
    //validationForCustomOrder:validationForCustomOrder
};
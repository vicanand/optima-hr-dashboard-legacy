import AccessCtrlSvc from '../service/accessCtrlSvc';
import DomEventHandler from '../common/domEventHandler.js';
import UserAccessDOMEvents from '../dom-events/userAccessDOMEvent.js';
import UserAccessV2DOMEvents from '../dom-events/userAccessV2DOMEvent.js';
import TableWidget from '../widgets/table-widget';
import Dropdown from '../widgets/dropdown';
import Constants from '../common/constants';
import Meta from '../common/metaStorage';

export
default {
    init: function() {
        $('#addUserModal').on('show.bs.modal', function(e) {
            $('#addUserModal').children().find('#addNewUserBtn').prop('disabled', true);
            $('#addUserModal').children().find('#addNewUserBtn').addClass('btn-disabled');
            $('#addUserModal').children().find('#addUserLoader').hide();
            $('#addUserModal').children().find('input:text').val("");
            $('#addUserModal').children().find('addUserLoader').hide();
            $('#addUserModal').children().find('input').removeClass('input-has-error');
            $('#addUserModal').children().find('.error-message').html('');
            $('#addUserModal').children().find('.valid-img').hide();
            $('#addUserModal').children().find('#adminYes').prop("checked", true);
            $('#addUserModal').children().find('.access-checkbox:radio').prop("checked", false);
            $('#addUserModal').children().find('.role-select-wrap').hide();
        });

        $('#changeStatusModal').on('show.bs.modal', function(e) {
            $('#changeStatusBtn').data("status", e.relatedTarget.dataset.status);
            $('#changeStatusBtn').data("name", e.relatedTarget.dataset.name);
            $('#changeStatusBtn').data("accountid", e.relatedTarget.dataset.accountid);
            // $('#modalUserName').html(e.relatedTarget.dataset.name);
            if(e.relatedTarget.dataset.status == 'active'){
                let question = "Are you sure you want to suspend <strong>"+e.relatedTarget.dataset.name+"</strong>?";
                $('#changeStatusQuote').html(question);
                $('#userStatusMsg').show();
                $('#changeStatusBtn').html("Yes, Suspend this user");
            }
            else if(e.relatedTarget.dataset.status == 'suspended'){
                let question = "Are you sure you want to restore <strong>"+e.relatedTarget.dataset.name+"</strong>?";
                $('#changeStatusQuote').html(question);
                $('#userStatusMsg').hide();
                $('#changeStatusBtn').html("Yes, Restore Access");
            }
        });
        if (Meta.getMetaElement(Constants.COUNTRY_VAR) === 'isSodexoPhpSm') {
            $('#addUserModal').on('show.bs.modal', function(e) {
                $("#addUserFormV2")[0].reset();
                $('#chooseCompany').multiselect('reset');
                $('#userName').removeAttr("data-touched");
                $('#employeeID').removeAttr("data-touched");
                $('#password').removeAttr("data-touched");
            });
            $('#editPrivilegesModal').on('show.bs.modal', function(e) {
                $('#changeAccessBtn').data("accountid", e.relatedTarget.dataset.accountid);
                $('#changeAccessBtn').data("companyids", e.relatedTarget.dataset.companyids);
                $('#accessTypeEdit').data('access-type', e.relatedTarget.dataset.role);
                // Reset form
                $("#editPrevilegeForm")[0].reset();
                $('#editPrivilegesModal').children().find('#changeAccessBtn').prop('disabled', true);
                $('#editPrivilegesModal').children().find('#changeAccessBtn').addClass('btn-disabled');
                $('#editPrivilegesModal').children().find('#changePrivilegeLoader').hide();
                $('#editPrivilegesModal').children().find('select').removeClass('input-has-error');
                $('#editPrivilegesModal').children().find('.error-message').html('');
                // Set default selected
                $('#accessTypeEdit option').filter(function(){
                    return this.value === e.relatedTarget.dataset.role;
                }).prop('selected', true);
            });
            $('#changePasswordModal').on('show.bs.modal', function(e) {
                $('#changePasswordBtnV2').data("accountid", e.relatedTarget.dataset.accountid);
                // Reset form
                $("#changePasswordForm")[0].reset();
                $('#changePasswordModal').children().find('#changePasswordBtnV2').prop('disabled', true);
                $('#changePasswordModal').children().find('#changePasswordBtnV2').addClass('btn-disabled');
                $('#changePasswordModal').children().find('#changePasswordLoader').hide();
                $('#changePasswordModal').children().find('input').removeClass('input-has-error');
                $('#changePasswordModal').children().find('.error-message').html('');
            });
            $('#chooseCompany').multiselect({
                columns: 1,
                search: true,
                selectAll: true,
                nonSelectedText: 'Select companies',
                maxHeight:true,
                texts: {
                    placeholder: 'Select companies',
                    nonSelectedText: 'Select companies',
                    search: 'Search companies'
                }
            });
            this.registerDOMEventsV2();
        } else {
            this.registerDOMEvents();
        }
    },



    registerDOMEvents: function() {
        DomEventHandler.bindEvent('addNewUserBtn', 'click', UserAccessDOMEvents.addUser);
        // DomEventHandler.bindEvent('email', 'keyup', UserAccessDOMEvents.validateExistingUser);
        DomEventHandler.bindEvent('email', 'input', UserAccessDOMEvents.clearConfirmEmailField);
        DomEventHandler.bindEvent('email', 'blur', UserAccessDOMEvents.validateEmail);
        DomEventHandler.bindEvent('confirmEmail', 'keyup', UserAccessDOMEvents.validateConfirmEmail);
        DomEventHandler.bindEvent('userName', 'blur', UserAccessDOMEvents.validateUserName);
        DomEventHandler.bindClassEvent('addUserFields', 'keyup', UserAccessDOMEvents.validateFields);
        DomEventHandler.bindEvent('changeStatusBtn', 'click', UserAccessDOMEvents.changeStatus);
        DomEventHandler.bindClassEvent('reload-btn', 'click', UserAccessDOMEvents.reloadPage);
        DomEventHandler.bindClassEvent('admin-access-radio', 'change', UserAccessDOMEvents.roleSelectShow);
        DomEventHandler.bindClassEvent('access-checkbox', 'change', UserAccessDOMEvents.onRoleCheck);
        DomEventHandler.bindClassEvent('access-checkbox', 'click', UserAccessDOMEvents.toggleRadioBtn);
    },
    registerDOMEventsV2: function() {
        DomEventHandler.bindEvent('addNewUserBtn', 'click', UserAccessV2DOMEvents.addUser);
        DomEventHandler.bindClassEvent('__generate-password', 'click', UserAccessV2DOMEvents.generatePassword);
        DomEventHandler.bindClassEvent('addUserFields', 'keyup', UserAccessV2DOMEvents.validateFields);
        DomEventHandler.bindEvent('changeStatusBtn', 'click', UserAccessV2DOMEvents.changeStatus);
        DomEventHandler.bindClassEvent('reload-btn', 'click', UserAccessV2DOMEvents.reloadPage);
        DomEventHandler.bindEvent('changePasswordBtnV2', 'click', UserAccessV2DOMEvents.changePassword);
        DomEventHandler.bindEvent('changeAccessBtn', 'click', UserAccessV2DOMEvents.changePrivilege);
        DomEventHandler.bindEvent('accessTypeEdit', 'change', UserAccessV2DOMEvents.validateChangeAccessType);
        DomEventHandler.bindEvent('chooseCompany', 'change', UserAccessV2DOMEvents.validateFields);
        DomEventHandler.bindEvent('changePassword', 'keyup', UserAccessV2DOMEvents.validateChangePassword);
        DomEventHandler.bindEvent('accessType', 'change', UserAccessV2DOMEvents.accessTypeChange);
        DomEventHandler.bindEvent('userName', 'blur', UserAccessV2DOMEvents.setTouched);
        DomEventHandler.bindEvent('employeeID', 'blur', UserAccessV2DOMEvents.setTouched);
        DomEventHandler.bindEvent('password', 'blur', UserAccessV2DOMEvents.setTouched);
    }
}   
import DomEventHandler from '../common/domEventHandler';
import SignupDOMEvents from '../dom-events/sodexoVnSignupDOMEvent';
export
default {
        init: function () {
            SignupDOMEvents.getCities();
            DomEventHandler.bindEvent('createAccount', 'click', SignupDOMEvents.createAccount);
            DomEventHandler.bindEvent('bill_province', 'change', SignupDOMEvents.filterCitiesForBillAddress);
            DomEventHandler.bindEvent('comm_province', 'change', SignupDOMEvents.filterCitiesForCommAddress);
            DomEventHandler.bindEvent('sameAsBillingAddrs', 'change', SignupDOMEvents.copyBillingToCommAddrs);
            DomEventHandler.bindClassEvent('signup__form___input', 'change', SignupDOMEvents.inputChange);
        }
}
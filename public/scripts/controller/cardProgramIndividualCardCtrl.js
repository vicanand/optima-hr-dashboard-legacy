import DomEventHandler from '../common/domEventHandler.js';
import BeneficiaryDOMEvents from '../dom-events/cardProgramIndividualBeneficiaryDomEvent.js';
import cardProgramIndividualBeneficiarySvc from '../service/cardProgramIndividualBeneficiarySvc.js';
import employeeBenefits from '../dom-events/employee-benefits-dom-events';

export
default {
    init: function() {
        $('body').off('click').on('click', '.cardDetailsLink', function() {
            let cardId = $(this).data("card-id"); //cardTransferReportTable-5809588924436242000
            let text = $(this).text();
            // alert(cardId);
            BeneficiaryDOMEvents.showHideCardTable(cardId, text);
        });
        $(document).on('click', '.downloadStatement', function() {
            let cardprogramid = $(this).data('cardprogramid');
            let userID = $(this).data('userid');
            let cardID = $(this).data('card-id');
            let cardStatus = $(this).data('cardstatus');
            sessionStorage.setItem('d-cardprogramid', cardprogramid);
            sessionStorage.setItem('d-card-id', $(this).data('card-id'));
            sessionStorage.setItem('d-producttype', $(this).data('producttype'));
            sessionStorage.setItem('d-cardname', $(this).data('cardname'));
            sessionStorage.setItem('d-cardstatus', cardStatus);
            
            $('#beneficiaryStatement').modal('show');
            $('#tranferPeriod').val('');
            $('#downloadStatement').attr('disabled', true).addClass('submit-btn-disabled');
            $('#downloadStatementSingle').attr('disabled', true).addClass('submit-btn-disabled');
            $('#cardProgramIndividualBeneficiary').modal('hide');
            $("#employee-benefit-name").text((sessionStorage.getItem('d-beneficiary-name')));
            $("#showDownloadStatementError").hide();
        });

        $('#beneficiaryStatement').off('click').on('click', '#downloadStatementSingle', function() {
            employeeBenefits.downloadStatement();
        });
        DomEventHandler.bindClassEvent('statement-btn-single', 'change', employeeBenefits.downloadStatementBtn);
    }
}
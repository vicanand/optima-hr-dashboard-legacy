import escalationDomEvents from "../../dom-events/expense-escalations-dom-events.js";
import DomEvents from "../../common/domEventHandler";
import EscalationSvc from '../../service/expense/escalations-svc';
import Util from '../../common/util';
import Table from '../../widgets/table-widget';
import FilterWidget from '../../widgets/filter-widget';

export
default {
    init: function() {
        Table.init();
        FilterWidget.init();
        let params = {
            programID: $('#transferId').val(),
            pageNumber: 1,
            pageSize: 10
        }
        EscalationSvc.getEscalatedClaims(params).then(function(respData) {
            var numberOfEscalations = respData.numberOfEscalations || 0,
                totalAmount = 0,
                scheduledAmount = 0,
                scheduledCount = 0,
                approvedAmount = 0,
                approvedCount = 0,
                declinedAmount = 0,
                declinedCount = 0;
            numberOfEscalations = Util.formatNumber(respData.numberOfEscalations);
            if (respData.netValueOfEscalations && respData.netValueOfEscalations.amount) {
                totalAmount = Util.formatINR(respData.netValueOfEscalations.amount / 100);
            }
            if (respData.netEscalationsByState && respData.netEscalationsByState["APPROVED"]) {
                approvedAmount = Util.formatINR(respData.netEscalationsByState["APPROVED"].netAmount.amount / 100);
                approvedCount = Util.formatNumber(respData.netEscalationsByState["APPROVED"].numberOfEscalations);
            }
            if (respData.netEscalationsByState && respData.netEscalationsByState["PENDING"]) {
                scheduledAmount = Util.formatINR(respData.netEscalationsByState["PENDING"].netAmount.amount / 100);
                scheduledCount = Util.formatNumber(respData.netEscalationsByState["PENDING"].numberOfEscalations);
            }
            if (respData.netEscalationsByState && respData.netEscalationsByState["DECLINED"]) {
                declinedAmount = Util.formatINR(respData.netEscalationsByState["DECLINED"].netAmount.amount / 100);
                declinedCount = Util.formatNumber(respData.netEscalationsByState["DECLINED"].numberOfEscalations);
            }

            if (scheduledCount != "0") {
                $('#escalations .new_number')
                    .removeClass('hide')
                    .text(scheduledCount);
            }
            $('#totalEscalations .filter-no').text(numberOfEscalations);
            $('#totalEscalations .filter-amount span').text(totalAmount);
            $('#scheduledEscalations .filter-no').text(scheduledCount);
            $('#scheduledEscalations .filter-amount span').text(scheduledAmount);
            $('#approvedEscalations .filter-no').text(approvedCount);
            $('#approvedEscalations .filter-amount span').text(approvedAmount);
            $('#declinedEscalations .filter-no').text(declinedCount);
            $('#declinedEscalations .filter-amount span').text(declinedAmount);
        });

        $("#payoutstable").tablewidget({
            source: EscalationSvc.getEscalatedClaims,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: "/template/section/expense/details/tab-content/tables/expense-escalations.ejs",
            rowTemplate: "/template/section/expense/details/tab-content/tables/expense-escalations-row.ejs",
            extraParam: {
                statusFilters: '',
                programID: $('#transferId').val()
            },
            searchSelector: $("#searchEscalation"),
            searchButton: $("#searchEscalationBtn"),
            payoutFilter: $('#escalationFilters')
        });

        DomEvents.bindEvent("payoutstable", "click", ".claimDeclineBtn", escalationDomEvents.declineClaimConfirm);
        DomEvents.bindEvent("payoutstable", "click", ".claimViewDetailsBtn", escalationDomEvents.approveClaimConfirm);
        DomEvents.bindEvent("payoutstable", "click", ".claimApproveBtn", escalationDomEvents.approveClaimConfirm);
        DomEvents.bindEvent("escalationProgramApprove", "click", ".approved", escalationDomEvents.approveClaim);
        DomEvents.bindEvent("escalationProgramDecline", "click", ".decline", escalationDomEvents.declineClaim);
        DomEvents.bindEvent("escalationProgramSucess", "click", ".done", escalationDomEvents.reload);
        DomEvents.bindEvent("escalationProgramSucess", "click", ".done", escalationDomEvents.reload);
    }
}

import DomEventHandler from '../common/domEventHandler.js';
import AddFundDOMEvents from '../dom-events/addfundDOMEvent.js';


export
default {
    init: function() {
        AddFundDOMEvents.addAccName();
        this.loadDatePicker();
        DomEventHandler.bindEvent('submitReq', 'click', AddFundDOMEvents.addFundsToAcc);
        DomEventHandler.bindEvent('inlineRadio1', 'click', AddFundDOMEvents.changeTransferTxt);
        DomEventHandler.bindEvent('inlineRadio2', 'click', AddFundDOMEvents.changeTransferTxt);
        DomEventHandler.bindEvent('transactionRefNumber', 'keyup', AddFundDOMEvents.validationRequest);
        DomEventHandler.bindEvent('transactionDate', 'change', AddFundDOMEvents.validationRequest);
        DomEventHandler.bindEvent('transactionAmount', 'keyup', AddFundDOMEvents.validationRequest);

        DomEventHandler.bindEvent('transactionRefNumber', 'keydown', AddFundDOMEvents.validationRequest);
        DomEventHandler.bindEvent('orderId', 'keydown', AddFundDOMEvents.validationRequest);
        DomEventHandler.bindEvent('POText', 'keydown', AddFundDOMEvents.validationRequest);
        DomEventHandler.bindEvent('POAttachment', 'input', AddFundDOMEvents.uploadAttachmentFile);
        DomEventHandler.bindEvent('paymentProof', 'input', AddFundDOMEvents.uploadAttachmentFile);

        DomEventHandler.bindEvent('transactionAmount', 'input', AddFundDOMEvents.validateAmount);
        DomEventHandler.bindEvent('viewAuthorizedBanksLink', 'click', AddFundDOMEvents.showAuthorizedBankAccnts);
    },
    loadDatePicker: function() {
        $('#transactionDate').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
        });
    },
    loadTrasferType: function() {
        AddFundDOMEvents.changeTransferTxt();
    }
}

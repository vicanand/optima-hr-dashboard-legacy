import Util from '../common/util';
import DomEventHandler from '../common/domEventHandler';
import DocumentDomEvents from '../dom-events/documentDom-Events';

export
default {
    init: function() {
        DocumentDomEvents.initFilterSelect();
        DomEventHandler.bindEvent("documentsTable", "click", ".click-doc-row", DocumentDomEvents.clickDocRow);
        DomEventHandler.bindEvent("documentsTable", "click", ".previewBtn", DocumentDomEvents.previewModal);
        DomEventHandler.bindEvent("loadMoreDocsBtn", "click", DocumentDomEvents.loadMoreDocs);
        DomEventHandler.bindEvent("searchDocs", "keyup", DocumentDomEvents.searchDocs);
        DomEventHandler.bindEvent("searchDocsBtn", "click", DocumentDomEvents.searchDocsOnFilter);
        DomEventHandler.bindEvent("programType", "change", DocumentDomEvents.searchDocsOnFilter);
        DomEventHandler.bindEvent("statusType", "change", DocumentDomEvents.searchDocsOnFilter); 
        DomEventHandler.bindEvent("selectedCompanyDropdown", "change", DocumentDomEvents.searchDocsOnFilterCompanyChange);
        DomEventHandler.bindEvent("companyType", "change", DocumentDomEvents.searchDocsOnFilter); 
        DomEventHandler.bindEvent("downloadDocuments", "click", DocumentDomEvents.downloadDocuments); 
        DomEventHandler.bindEvent("download-doc-btn", "click", DocumentDomEvents.processDocuments); 
        DomEventHandler.bindEvent("download-done-btn", "click", DocumentDomEvents.closeModal); 
        DomEventHandler.bindEvent("download-error-btn", "click", DocumentDomEvents.downloadDocuments);
        DomEventHandler.bindEvent("otherDocuments", "click", DocumentDomEvents.showOtherDocuments);
        DomEventHandler.bindEvent("allClaims", "click", DocumentDomEvents.showClaimDetails);
        DomEventHandler.bindEvent("documentType", "change", DocumentDomEvents.searchDocsOnFilter); 
        DomEventHandler.bindClassEvent("company-options", "click", DocumentDomEvents.searchDocsOnFilterCompanyChange);
        DomEventHandler.bindEvent("docStoreList", "click", ".docGroupLink", DocumentDomEvents.openDocDetails);
    }
}
/**
 * Created by kk on 09/11/16.
 */

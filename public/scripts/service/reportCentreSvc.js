import Urls from "../common/urls";
import ServiceConnector from "../common/serviceConnector";
import moment from "moment";
import Util from "../common/util";
import Storage from "../common/webStorage"

export default {
  getAvailableReports: function(product) {
    // let url = Urls.getOMSApiUrl().GET_AVAILABLE_REPORTS
    let url = Urls.getOMSApiUrl()
      .GET_AVAILABLE_REPORTS_FOR_PRODUCTS.replace(
        ":authToken:",
        Storage.get('authToken')
      )
      .replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
      .replace(":product:", product);
    let inputObj = {};
    let headers = {
      "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
  },
  getAllReportList: function(params) {
    let deferred = $.Deferred();
    let pageNo = Number(params.pageNumber) - 1;
    let pageSize = params.pageSize;

    let url =
      "/report-centre/report-list?pageNo=" + pageNo + "&pageSize=" + pageSize;
    let inputObj = params.extraParams;
    let headers = {
      "Content-Type": "application/json"
    };
    var urlSource = new URL(window.location);
    var productSource = urlSource.searchParams.get("product") || "";

    ServiceConnector.post(inputObj, url, headers).then(
      function(respData) {
        let responseJson = respData;
        let records = {};
        records.reports = responseJson.reports.map(function(item) {
          if (!item.url) {
            item.url = "";
          }
          if (!item.reportState) {
            item.reportState = "-";
          }
          if (!item.reportDisplayName) {
            item.reportDisplayName = "-";
          }
          if (!item.downloadFormat) {
            item.downloadFormat = "";
          }
          if (!item.category) {
            item.category = false;
          }
          if (item.userParameters) {
            item.userParameters.startDate = item.userParameters.START_DATE
              ? moment(parseInt(item.userParameters.START_DATE[0])).format(
                  "DD MMM YYYY"
                )
              : null;
            item.userParameters.endDate = item.userParameters.END_DATE
              ? moment(parseInt(item.userParameters.END_DATE[0])).format(
                  "DD MMM YYYY"
                )
              : null;
            item.userParameters.fundingAccountNames = item.userParameters
              .FUNDING_ACCOUNT_NAMES
              ? item.userParameters.FUNDING_ACCOUNT_NAMES
              : [];
            item.userParameters.companies = item.userParameters.COMPANY_IDS
              ? item.userParameters.COMPANY_IDS
              : [];
            item.userParameters.productType = item.userParameters.PRODUCT_TYPE
              ? item.userParameters.PRODUCT_TYPE
              : [];
            item.userParameters.companies = item.userParameters.COMPANIES
              ? item.userParameters.COMPANIES
              : [];
            item.userParameters.cardprograms = item.userParameters.CARD_PROGRAMS
              ? item.userParameters.CARD_PROGRAMS.indexOf(null) >= 0
                ? []
                : item.userParameters.CARD_PROGRAMS
              : [];
            item.userParameters.closeCardOrderID = item.userParameters
              .CLOSE_CARD_ORDER_ID
              ? item.userParameters.CLOSE_CARD_ORDER_ID.join(",")
              : null;
          }

          if (item.createdAt >= 1535135400000) {
            item.showView = true;
          } else {
            item.showView = false;
          }
          if (item.reportID.indexOf("YEAR_END_") >= 0) {
            item.yearEndReport = true;
            // item.createdAtTime = 'Financial Year 2019 - 2020'
            item.extraParam = "Financial Year 2019 - 2020";
          } else {
            item.yearEndReport = false;
          }
          if (item.createdAt) {
            item.createdAtTime = moment(item.createdAt).format("H:mm");
            item.createdAt = moment(item.createdAt).format("DD MMM YYYY");
          } else {
            item.createdAt = "-";
            item.createdAtTime = "-";
          }
          item.productSource = productSource;
          return item;
        });

        responseJson.records = records.reports;
        responseJson.totalRecords = responseJson.noOfReports;
        console.log("responseJson", responseJson);
        deferred.resolve(responseJson);
      },
      function(error) {
        if (error.status === 401) {
          Util.renewAuth();
        }
        deferred.reject();
      }
    );
    return deferred.promise();
  },

  getReportById: function(corpId, reportId) {
    let url = Urls.getOMSApiUrl()
      .GET_REPORT_BY_ID.replace(
        ":authToken:",
        Storage.get('authToken')
      )
      .replace(":corporateId:", corpId)
      .replace(":reportId:", reportId);
    let inputObj = {};
    let headers = {
      "Content-Type": "application/json"
    };
    return ServiceConnector.get(url, inputObj, headers);
  }
};

import ServiceConnector from "../common/serviceConnector";
import Urls from '../common/urls';
import Util from '../common/util';
import Constants from '../common/constants';
import Storage from '../common/webStorage';
import CacheDataStorage from '../common/cacheDataStorage';

let getCorpAccounts = function (params) {
    let url = Urls.getOMSApiUrl().GET_CORP_ACCOUNTS
    .replace(":corpID:", $("meta[name='corpID']").attr("content"))
    .replace(":authToken:", Storage.get('authToken'));
    let deferred = $.Deferred();
    
    let countryName = $("meta[name='countryVar']").attr("content")
    let isSodexoPh = countryName == 'isSodexoPh' ? true : false
    let companyID = $("meta[name='companyID']").attr("content")
    // If country is phillipine use getCorpAccountsV2 api and add company param
    if(isSodexoPh){
        url = Urls.getOMSApiUrl().GET_CORP_ACCOUNTSV2
        .replace(":corpID:", $("meta[name='corpID']").attr("content"))
        .replace(":authToken:", Storage.get('authToken'));
        url += "&companyID=" + companyID + '&pageNo='+params.pageNumber+'&pageSize='+params.pageSize
    }
    ServiceConnector.get(url, {}).then(function (respData) {
        let responseObj = {};
        let corpAccList = [];
        let loggedUser = $('#currUser').html();
        if(isSodexoPh){
            
            responseObj.records = respData.entries;
            responseObj.totalCount = respData.totalEntries;
            responseObj.records = _.map(respData.entries, function (account) {
                
                if (account.account.email == loggedUser) {
                    account.account.loggedUser = true;
                } else {
                    account.account.loggedUser = false;
                }
                corpAccList.push(account.account.email);
                
                return account.account
            });
        }else{
            responseObj.records = respData.accounts;
            responseObj.totalCount = respData.accounts.length;
            _.map(respData.accounts, function (account) {
                if (account.email == loggedUser) {
                    account.loggedUser = true;
                } else {
                    account.loggedUser = false;
                }
                corpAccList.push(account.email);
            });
        }
        Storage.setCollection(Constants.STORAGE_CORP_ACCOUNTS_INFO, responseObj.records);
        Storage.setCollection(Constants.STORAGE_CORP_ACCOUNTS, corpAccList);
        deferred.resolve(responseObj);
    }, function (respError) {
        if (respError.status === 401) {
            Util.renewAuth();
        }
        deferred.reject();
    });

    return deferred.promise();
};

// SODEXO_PHILIPPINES_SM api
let getCorpAccountsV2 = function (params) {
    let companyID = $("meta[name='companyID']").attr("content")
    const parseRole = function(role) {
        let displayRole = '';
        switch(role) {
            case 'TREASURY':
            displayRole = 'Branch Treasury';
                break;
            case 'OPERATOR':
            displayRole = 'BCO Customer Service Agent';
                break;
            case 'PHP_COMPANY_ADMIN':
            displayRole = 'BCO Manager/Supervisor';
                break;
            case 'ADMIN':
            displayRole = 'Sodexo';
                break;
            case 'ACCOUNTANT':
            displayRole = 'Branch Accounting';
                break;
            case 'PHP_CORP_ADMIN':
            displayRole = 'Sm Corporate Admin';
                break;
            default:
            displayRole = role;
        }
        return displayRole;
    }
    const getSuspendAccess = function(addAccountForEmployee, role) {
        let status = null;
        if (role === "TREASURY" || role === "ADMIN" || role === "ACCOUNTANT" || role === "PHP_COMPANY_ADMIN") {
            status = addAccountForEmployee && addAccountForEmployee.indexOf('own') !== -1;
        }
        return status;
    }
    let url = Urls.getOMSApiUrl().GET_CORP_ACCOUNTSV2
        .replace(":corpID:", $("meta[name='corpID']").attr("content"))
        .replace(":authToken:", Storage.get('authToken'));
        if (params) {
            if (params.pageNumber) { url += "&pageNo=" + Number(params.pageNumber); }
            if (params.pageSize) { url += "&pageSize=" + params.pageSize; }
            if (params.searchQuery) { 
                url += "&searchKey=" + (/\d/.test(params.searchQuery) ? "EMAIL" : "NAME");
                url += "&searchValue=" + params.searchQuery;
            }
            if (params.filterValue) { url += "&statusValue=" + params.filterValue; }
        }
        let rbac = CacheDataStorage.getItem(Constants.ROLES_CONFIG);
        if (rbac && rbac['getCorpAccounts_'+companyID] && rbac['getCorpAccounts_'+companyID].indexOf('own') !== -1) {
            url += "&companyID=" + companyID;
        }
    let loggedUser = $('#currUser').html();
    let deferred = $.Deferred();
    ServiceConnector.get(url, {}).then(function (respData) {
        let responseObj = {};
        let corpAccList = [];
        let rbac = CacheDataStorage.getItem(Constants.ROLES_CONFIG);
        let comapnies = CacheDataStorage.getItem(Constants.COMPANIES);
        const addAccountForEmployee = rbac && rbac['addAccountForEmployee_'+companyID];
        _.map(respData.entries, function (item) {
            item.account.loggedUser = item.account.email == loggedUser;

            item['disableChangePassword'] = rbac && !rbac['changeCorpAccountPassword_'+companyID];
            item['disableEditPrivilege'] = null;
            item['disableSuspendUser'] = null;
            item['stores'] = 'NA'; item['displayRoles'] = 'NA';
            if (comapnies && comapnies.length && item.companyUsers && item.companyUsers.length) {
                let stores = []; let displayRoles = []; let companyIDs = [];
                for (let i=0; i<item.companyUsers.length; i++) {
                    const found = comapnies.find((c_item) => c_item.company.companyID == item.companyUsers[i].companyID);
                    if (found && found.company && found.company.companyName) {
                        stores.push(found.company.companyName);
                    }
                    const role = item.companyUsers[i].attributes.role || item.account.attributes.role;
                    // Only Sodexo admin can edit previlegies
                    item['disableEditPrivilege'] = addAccountForEmployee && addAccountForEmployee.indexOf('own') !== -1;
                    // Only Sodexo user can suspend user's with role : [TREASURY, ADMIN, ACCOUNTANT]
                    item['disableSuspendUser'] = getSuspendAccess(addAccountForEmployee, role);
                    const parsedRole = parseRole(role);
                    if (role) {
                        displayRoles.push({role: role, displayRole: parsedRole});
                    }
                    item['employeeId'] = item.companyUsers[i].attributes.employeeId || item.account.attributes.employeeId;
                    if (item.companyUsers[i].companyID) {
                        companyIDs.unshift(item.companyUsers[i].companyID);
                    }
                }
                item['displayRoles'] = displayRoles;
                item['stores'] = stores.sort();
                item['companyIDs'] = companyIDs;
            }
            corpAccList.push(item.account.email);
        });
        responseObj.records = respData.entries;
        responseObj.totalCount = respData.totalEntries;
        Storage.setCollection(Constants.STORAGE_CORP_ACCOUNTS, corpAccList);
        deferred.resolve(responseObj);
    }, function (respError) {
        if (respError.status === 401) {
            Util.renewAuth();
        }
        deferred.reject();
    });

    return deferred.promise();
};

let addCorpAccount = function (requestPayload) {
    let url = Urls.getOMSApiUrl().ADD_CORP_ACCOUNT
        .replace(":authToken:", Storage.get('authToken'));

    let deferred = $.Deferred();
    ServiceConnector.post(requestPayload, url).then(function (respData) {
        deferred.resolve(respData);
        // return respData;
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
};

let givePrivilegeToUser = function (requestPayload) {
    let url = Urls.getOMSApiUrl().GIVE_PRIVELEGES_TO_USER
    .replace(":authToken:", Storage.get('authToken'));

    let deferred = $.Deferred();
    ServiceConnector.post(requestPayload, url).then(function (respData) {
        deferred.resolve(respData);
        // return respData;
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
};

let addAccountForUser = function (requestPayload) {
    let url = Urls.getOMSApiUrl().ADD_ACCOUNT_FOR_USER
    .replace(":authToken:", Storage.get('authToken'));

    let deferred = $.Deferred();
    ServiceConnector.post(requestPayload, url).then(function (respData) {
        deferred.resolve(respData);
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        deferred.reject(error);
    });
    return deferred.promise();
};

let createUserWithDetails = function (requestPayload) {
    let url = Urls.getOMSApiUrl().CREATE_USER_WITH_DETAILS
    .replace(":authToken:", Storage.get('authToken'));

    let deferred = $.Deferred();
    ServiceConnector.post(requestPayload, url).then(function (respData) {
        deferred.resolve(respData);
        // return respData;
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
};

let changeAccountStatus = function (requestPayload) {
    let url = Urls.getOMSApiUrl().CHANGE_ACCOUNT_STATUS
        .replace(":authToken:", Storage.get('authToken'));
    if (requestPayload.status === 'suspended') {
        url = Urls.getOMSApiUrl().REMOVE_CORP_ACCOUNT
            .replace(":authToken:", Storage.get('authToken'));
    }
    let deferred = $.Deferred();
    ServiceConnector.post(requestPayload, url).then(function (respData) {
        deferred.resolve(respData);
        // return respData;
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
};

let addAccountForEmployee = function (requestPayload) {
    let url = Urls.getOMSApiUrl().ADD_ACCOUNT_FOR_EMPLOYEE
        .replace(":authToken:", Storage.get('authToken'));

    let deferred = $.Deferred();
    ServiceConnector.post(requestPayload, url).then(function (respData) {
        deferred.resolve(respData);
        // return respData;
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
};

let changeCorpAccountPassword = function (requestPayload) {
    let url = Urls.getOMSApiUrl().CHANGE_CORP_ACCOUNT_PASSSWORD
        .replace(":authToken:", Storage.get('authToken'));

    let deferred = $.Deferred();
    ServiceConnector.post(requestPayload, url).then(function (respData) {
        deferred.resolve(respData);
        // return respData;
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
};

let changeCorpAccountRole = function (requestPayload) {
    let url = Urls.getOMSApiUrl().CHANGE_CORP_ACCOUNT_ROLE
        .replace(":authToken:", Storage.get('authToken'));

    let deferred = $.Deferred();
    ServiceConnector.post(requestPayload, url).then(function (respData) {
        deferred.resolve(respData);
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        deferred.reject(error);
    });
    return deferred.promise();
};

export
default {
    getCorpAccounts: getCorpAccounts,
    addCorpAccount: addCorpAccount,
    changeAccountStatus: changeAccountStatus,
    addAccountForEmployee: addAccountForEmployee,
    changeCorpAccountPassword: changeCorpAccountPassword,
    changeCorpAccountRole: changeCorpAccountRole,
    getCorpAccountsV2: getCorpAccountsV2,
    givePrivilegeToUser: givePrivilegeToUser,
    createUserWithDetails: createUserWithDetails,
    addAccountForUser: addAccountForUser
}
import Storage from '../common/webStorage';
import Constant from '../common/constants';
import ServiceConnector from '../common/serviceConnector';
import Urls from '../common/urls';
import Utils from '../common/util';
import Constants from '../common/constants';
import Env from '../common/env';
import moment from '../../lib/moment/moment'

let createOrder = function(inputObj) {
    let url = Urls.getOMSApiUrl().CLOSE_BULK_CARDS.replace(":authToken:", Storage.get('authToken'));
    return ServiceConnector.post(inputObj, url);
};
let actionOnBulkOrder = function(inputObj){
    let url = Urls.getOMSApiUrl().ACTION_ON_CLOSE_BULK_ORDER.replace(":authToken:", Storage.get('authToken'));
    return ServiceConnector.post(inputObj, url);
};
let getCloseOrders = function(param) {
    let deferred = $.Deferred();
    let authToken = Storage.get('authToken'),
        corpID = $("meta[name='corpID']").attr("content");
    let url = Urls.getOMSApiUrl().GET_CLOSE_ORDERS
                .replace(":CORP_ID:", corpID)
                .replace(":authToken:", authToken) +
                "&pageNumber=" + param.pageNumber +
                "&pageSize=" + param.pageSize +
                "&closeCardOrderStatus=" + param.filterValue +
                "&startDate=" + param.startDate +
                "&endDate=" + param.endDate +
                "&closeCardOrderID=" + param.searchQuery;  
    ServiceConnector.get(url).then(function(respData) {
        console.log(respData);  
        respData.closeCardOrderList = respData.closeCardOrderList.map(function(item) {
            let createdDate = new Date(item.createdAt);
            item["closeStatus"] = Constants.BULK_CLOSE_ORDERS_STATUS_LABELS[item.status];
            item.amount = Utils.formatINR(item.amount);
            if(Env.CURR_ENV === 'STAGE'){
                item.fileUrl = "https://api.stage.zeta.in/zeta.in/corpben/1.0/downloadBulkOrderFile?corpID="+
                                corpID + "&token=" +
                                authToken + "&orderID=" +
                                item.orderID;
            }
            else{
                item.fileUrl = "https://api.gw.zetapay.in/zeta.in/corpben/1.0/downloadBulkOrderFile?corpID="+
                                corpID + "&token=" +
                                authToken + "&orderID=" +
                                item.orderID;
            }
            item.csvUrl = Urls.getCorpApiUrl().GET_REPORT_ORDER+
                            item.bulkOrderID+"/payouts/download?token="+authToken+"&corpID="+corpID;
            item.createdOn = Utils.dateFormat(createdDate.getDate(),createdDate.getMonth()+1,createdDate.getFullYear());
        
        // item.isDownloadAvailable = (moment().diff(moment(new Date(createdDate)), 'hours' )>= 1);
        var currentTime= moment();
        var orderCreatedTime= moment(new Date(createdDate));
        var convertedTime= orderCreatedTime.add(5.30, 'hours');
        item.isDownloadAvailable =  (currentTime.diff(convertedTime, 'hours'));
        return item;
    });
        let responseJson = {
            totalCount: respData.closeOrderCount,
            records: respData.closeCardOrderList
        };
        console.log(responseJson);
        deferred.resolve(responseJson);
    }, function (error) {
        if (error.status === 401) {
            Utils.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
};

let getBulkOrderSummary = function(bulkOrderID){
    let deferred = $.Deferred();
    let authToken = Storage.get('authToken'),
        corpID = $("meta[name='corpID']").attr("content");
    let url = Urls.getOMSApiUrl().GET_BULK_ORDER_SUMMARY
        .replace(':authToken:', authToken)
        .replace(':CORP_ID:', corpID)
        .replace(':BULK_ORDER_ID:', bulkOrderID);
    ServiceConnector.get(url).then(function(respData) {
        deferred.resolve(respData);
    }, function (error) {
        if (error.status === 401) {
            Utils.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
};

let getCompanies = function(){
    let deferred = $.Deferred();
    let authToken = Storage.get('authToken'),
        corpID = $("meta[name='corpID']").attr("content");
    let url = Urls.getOMSApiUrl().GET_COMPANY_SHORTCODES
        .replace(':authToken:', authToken)
        .replace(':CORP_ID:', corpID);
    ServiceConnector.get(url).then(function(respData) {
        deferred.resolve(respData);
    }, function (error) {
        if (error.status === 401) {
            Utils.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
};

let _processIndividualBulkOrders = function(respData,authToken,corpID,deferred){
    let companies = Storage.getCollection(Constant.STORAGE_COMPANY_SHORTCODES);
    respData.orderList = respData.orderList.map(function(item) {
        item["status"] = Constants.INDIVIDUAL_BULK_TRANSFER_ORDERS_STATUS_LABELS[item.orderStatus];
        item.amount = Utils.formatINR(item.amount);
        if (!item.orderId) {
            item.orderId = 'NA';
        }
        if(Env.CURR_ENV === 'STAGE'){
            item.fileUrl = "https://mv-stage.dash.zeta.in/account/orders/" +
                            +item.orderID + "/downloadUploadedFile?token=" + authToken + "&corpID=" + corpID;
        }
        else{
            item.fileUrl = "https://corp.zetaapps.in/account/orders/" +
                            +item.orderID + "/downloadUploadedFile?token=" + authToken + "&corpID=" + corpID;
        }
        item.csvUrl = Urls.getCorpApiUrl().GET_REPORT_ORDER+
                        item.orderID+"/payouts/download?token="+authToken+"&corpID="+corpID;
        item.performaUrl = Urls.getCorpApiUrl().GET_PERFORMA_INVOICE.replace(':orderId', item.orderId) +
                    "?token=" + authToken + "&corpID=" + corpID;
        item.invoiceUrl = Urls.getCorpApiUrl().GET_INVOICE.replace(':orderId', item.orderId) + "?token=" + authToken + "&corpID=" + corpID;
        let selectedComp = companies.filter(function(company){
            return company.companyID == item.companyID;
        });
        item['companyShortCode'] = selectedComp[0].companyShortCode;
        return item;
    });
    let responseJson = {
        totalCount: respData.totalBulkOrders,
        records: respData.orderList
    };
    console.log(responseJson);
    deferred.resolve(responseJson);
}

let getIndividualBulkOrders = function(param) {
    let deferred = $.Deferred();
    let authToken = Storage.get('authToken'),
        corpID = $("meta[name='corpID']").attr("content");
    let url = Urls.getOMSApiUrl().LIST_INDIVIDUAL_ORDERS_IN_BULK_ORDER
                .replace(":CORP_ID:", corpID)
                .replace(":BULK_ORDER_ID:", param.bulkOrderId)
                .replace(":authToken:", authToken) +
                "&pageNumber=" + param.pageNumber +
                "&pageSize=" + param.pageSize +
                "&orderStatus=" + param.filterValue +
                "&orderID=" + param.searchQuery;
    
    ServiceConnector.get(url).then(function(respData) {
        if(!Storage.getCollection(Constant.STORAGE_COMPANY_SHORTCODES)){
            getCompanies().then(function(companiesData) {
                Storage.setCollection(Constant.STORAGE_COMPANY_SHORTCODES,companiesData.companies);
                _processIndividualBulkOrders(respData,authToken,corpID,deferred);
            },function (error) {
                if (error.status === 401) {
                    Utils.renewAuth();
                }
                deferred.reject(error);
                // return respError;
            });
        }
        else{
            _processIndividualBulkOrders(respData,authToken,corpID,deferred);
        }
    }, function(error) {
        if (error.status === 401) {
            Utils.renewAuth();
        }
        deferred.reject();
    });
    return deferred.promise();
}

let getErrorRows  = function(param) {
    let deferred = $.Deferred();
    let authToken = Storage.get('authToken'),
        corpID = $("meta[name='corpID']").attr("content");
    let url = Urls.getOMSApiUrl().GET_ERROR_ROWS.replace(":CORP_ID:", corpID)
        .replace(":bulkOrderID:", param.bulkOrderID)
        .replace(":authToken:", authToken) + "&pageNumber=" + param.pageNumber + "&pageSize=" + param.pageSize;
    ServiceConnector.get(url).then(function(respData) {
        let $headerRow = '<tr>';
        for(let i = 0;i<respData.columnHeaders.length;i++){
            $headerRow += '<th>' + respData.columnHeaders[i] + '</th>';
        }
        
        
        $headerRow += '<th>Reasons</th></tr>';
        if($('#errorRowsTable thead tr').length < 2){
            $('#errorRowsTable thead').append($headerRow);
        }

        

        let records = respData.rows.map(function(errorEntry) {
            let item = {
                'headers' : [],
                'rows' : []
            };
            errorEntry.row.forEach(function(errorEntryRow,index) {
                item.rows.push({
                    "name" : errorEntryRow
                });
                if(errorEntry.errors[index]){
                    item.rows[index]["error"] = errorEntry.errors[index]
                }
            });
            for(let i = 0;i<respData.columnHeaders.length;i++){
                item.headers.push({
                    'name': respData.columnHeaders[i]
                });
            }
            return item;
        });

        let responseJson = {
            totalCount: Storage.get(Constant.STORAGE_INVALID_BULK_ENTRIES),
            records: records
        };
        console.log(responseJson);
        deferred.resolve(responseJson);

    }, function (error) {
        if (error.status === 401) {
            Utils.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
};

let claimReport = function(orderID){
    let inputObj = {
        "corpID": $("meta[name='corpID']").attr("content"),
        "reportParameters": {
            "reportID": "CLOSE_CARD_ALL_ALLOWANCE_REPORT",
            "reportContentType": "CSV",
            "parameters": {
                "CLOSE_CARD_ORDER_ID": []
            }
        }
    };
    inputObj.reportParameters.parameters.CLOSE_CARD_ORDER_ID.push(orderID);
    let url = Urls.getOMSApiUrl().GET_CLOSE_CARD__ORDER_REPORT.replace(":authToken:", Storage.get('authToken'));
    return ServiceConnector.post(inputObj, url);
};

let revokeReport = function(orderID){
    let inputObj = {
        "corpID": $("meta[name='corpID']").attr("content"),
        "reportParameters": {
            "reportID": "CLOSE_CARD_REVOKE_REPORT",
            "reportContentType": "CSV",
            "parameters": {
                "CLOSE_CARD_ORDER_ID": []
            }
        }
    };
    inputObj.reportParameters.parameters.CLOSE_CARD_ORDER_ID.push(orderID);
    let url = Urls.getOMSApiUrl().GET_CLOSE_CARD__ORDER_REPORT.replace(":authToken:", Storage.get('authToken'));
    return ServiceConnector.post(inputObj, url);
};

let cardStatusReport = function(orderID){
    let inputObj = {
        "corpID": $("meta[name='corpID']").attr("content"),
        "reportParameters": {
            "reportID": "CLOSE_CARD_STATUS_REPORT",
            "reportContentType": "CSV",
            "parameters": {
                "CLOSE_CARD_ORDER_ID": []
            }
        }
    };
    inputObj.reportParameters.parameters.CLOSE_CARD_ORDER_ID.push(orderID);
    let url = Urls.getOMSApiUrl().GET_CLOSE_CARD__ORDER_REPORT.replace(":authToken:", Storage.get('authToken'));
    return ServiceConnector.post(inputObj, url);
};

let fileReport = function(closeOrderID){
    let url = Urls.getOMSApiUrl().GET_CLOSE_FILE_REPORT
    .replace(":corpID:", $("meta[name='corpID']").attr("content"))
    .replace(":closeOrderID:", closeOrderID)
    .replace(":authToken:", Storage.get('authToken'));
    return ServiceConnector.get(url);
};

export default {
    createOrder : createOrder,
    actionOnBulkOrder : actionOnBulkOrder,
    getCloseOrders : getCloseOrders,
    getBulkOrderSummary : getBulkOrderSummary,
    getCompanies : getCompanies,
    getIndividualBulkOrders : getIndividualBulkOrders,
    getErrorRows :getErrorRows,
    claimReport : claimReport,
    revokeReport : revokeReport,
    cardStatusReport:cardStatusReport,
    fileReport:fileReport
}

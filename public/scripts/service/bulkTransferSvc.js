import Storage from '../common/webStorage';
import Constant from '../common/constants';
import ServiceConnector from '../common/serviceConnector';
import Urls from '../common/urls';
import Utils from '../common/util';
import Constants from '../common/constants';
import Env from '../common/env';




let createOrder = function(inputObj) {
    let url = Urls.getOMSApiUrl().CREATE_BULK_ORDER.replace(":authToken:", Storage.get('authToken'));
    return ServiceConnector.post(inputObj, url);
};
let actionOnBulkOrder = function(inputObj){
    let url = Urls.getOMSApiUrl().ACTION_ON_BULK_ORDER.replace(":authToken:", Storage.get('authToken'));
    return ServiceConnector.post(inputObj, url);
};
let getBulkOrders = function(param) {
    let deferred = $.Deferred();
    let authToken = Storage.get('authToken'),
        corpID = $("meta[name='corpID']").attr("content");
    let url = Urls.getOMSApiUrl().GET_BULK_ORDERS
                .replace(":CORP_ID:", corpID)
                .replace(":authToken:", authToken) +
                "&pageNumber=" + param.pageNumber +
                "&pageSize=" + param.pageSize +
                "&bulkOrderStatus=" + param.filterValue +
                "&programCode=" + param.secondaryFilterValue +
                "&bulkOrderID=" + param.searchQuery +
                "&startDate=" + param.startDate +
                "&endDate=" + param.endDate;
                 
    ServiceConnector.get(url).then(function(respData) {
        respData.bulkOrders = respData.bulkOrders.map(function(item) {
            let createdDate = new Date(item.createdOn);
            item["status"] = Constants.BULK_TRANSFER_ORDERS_STATUS_LABELS[item.bulkOrderStatus];
            item.amount = Utils.formatINR(item.amount);
            if(Env.CURR_ENV === 'STAGE'){
                item.fileUrl = "https://api.stage.zeta.in/zeta.in/corpben/1.0/downloadBulkOrderFile?corpID="+
                                corpID + "&token=" +
                                authToken + "&bulkOrderID=" +
                                item.bulkOrderID;
            }
            else{
                item.fileUrl = "https://api.gw.zetapay.in/zeta.in/corpben/1.0/downloadBulkOrderFile?corpID="+
                                corpID + "&token=" +
                                authToken + "&bulkOrderID=" +
                                item.bulkOrderID;
            }
            item.csvUrl = Urls.getCorpApiUrl().GET_REPORT_ORDER+
                            item.bulkOrderID+"/payouts/download?token="+authToken+"&corpID="+corpID;
            item.createdOn = Utils.dateFormat(createdDate.getDate(),createdDate.getMonth()+1,createdDate.getFullYear());
            if(item.bulkOrderStatus == "failed"){
                if(item.statusMessage == undefined){
                    item.statusMessage = '{"errorCode":"INTERNAL_ERROR","data":{},"message":"INTERNAL_ERROR"}';  
                } 
            }
            return item;
        });
        let responseJson = {
            totalCount: respData.totalRequestCount,
            records: respData.bulkOrders
        };
        console.log('this is response', responseJson);
        deferred.resolve(responseJson);
    }, function (error) {
        if (error.status === 401) {
            Utils.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
};

let getBulkOrderSummary = function(bulkOrderID){
    let deferred = $.Deferred();
    let authToken = Storage.get('authToken'),
        corpID = $("meta[name='corpID']").attr("content");
    let url = Urls.getOMSApiUrl().GET_BULK_ORDER_SUMMARY
        .replace(':authToken:', authToken)
        .replace(':CORP_ID:', corpID)
        .replace(':BULK_ORDER_ID:', bulkOrderID);
    ServiceConnector.get(url).then(function(respData) {
        deferred.resolve(respData);
    }, function (error) {
        if (error.status === 401) {
            Utils.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
};

let getCompanies = function(){
    let deferred = $.Deferred();
    let authToken = Storage.get('authToken'),
        corpID = $("meta[name='corpID']").attr("content");
    let url = Urls.getOMSApiUrl().GET_COMPANY_SHORTCODES
        .replace(':authToken:', authToken)
        .replace(':CORP_ID:', corpID);
    ServiceConnector.get(url).then(function(respData) {
        deferred.resolve(respData);
    }, function (error) {
        if (error.status === 401) {
            Utils.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
};

let _processIndividualBulkOrders = function(respData,authToken,corpID,deferred){
    let companies = Storage.getCollection(Constant.STORAGE_COMPANY_SHORTCODES);
    respData.orderList = respData.orderList.map(function(item) {
        item["status"] = Constants.INDIVIDUAL_BULK_TRANSFER_ORDERS_STATUS_LABELS[item.orderStatus];
        item.amount = Utils.formatINR(item.amount);
        if (!item.orderID) {
            item.orderID = 'NA';
        }
        if(Env.CURR_ENV === 'STAGE'){
            item.fileUrl = "https://mv-stage.dash.zeta.in/account/orders/" +
                            +item.orderID + "/downloadUploadedFile?token=" + authToken + "&corpID=" + corpID;
        }
        else{
            item.fileUrl = "https://corp.zetaapps.in/account/orders/" +
                            +item.orderID + "/downloadUploadedFile?token=" + authToken + "&corpID=" + corpID;
        }
        item.csvUrl = Urls.getCorpApiUrl().GET_REPORT_ORDER+
                        item.orderID+"/payouts/download?token="+authToken+"&corpID="+corpID;
        item.performaUrl = Urls.getCorpApiUrl().GET_PERFORMA_INVOICE.replace(':orderId', item.orderID) +
                    "?token=" + authToken + "&corpID=" + corpID;
        item.invoiceUrl = Urls.getCorpApiUrl().GET_INVOICE.replace(':orderId', item.orderID) + "?token=" + authToken + "&corpID=" + corpID;
        let selectedComp = companies.filter(function(company){
            return company.companyID == item.companyID;
        });
        item['companyShortCode'] = selectedComp[0].companyShortCode;
        return item;
    });
    let responseJson = {
        totalCount: respData.totalBulkOrders,
        records: respData.orderList
    };
    deferred.resolve(responseJson);
};

let getIndividualBulkOrders = function(param) {
    let deferred = $.Deferred();
    let authToken = Storage.get('authToken'),
        corpID = $("meta[name='corpID']").attr("content");
    let url = Urls.getOMSApiUrl().LIST_INDIVIDUAL_ORDERS_IN_BULK_ORDER
                .replace(":CORP_ID:", corpID)
                .replace(":BULK_ORDER_ID:", param.bulkOrderId)
                .replace(":authToken:", authToken) +
                "&pageNumber=" + param.pageNumber +
                "&pageSize=" + param.pageSize +
                "&orderStatus=" + param.filterValue +
                "&orderID=" + param.searchQuery;
    
    ServiceConnector.get(url).then(function(respData) {
        if(!Storage.getCollection(Constant.STORAGE_COMPANY_SHORTCODES)){
            getCompanies().then(function(companiesData) {
                Storage.setCollection(Constant.STORAGE_COMPANY_SHORTCODES,companiesData.companies);
                _processIndividualBulkOrders(respData,authToken,corpID,deferred);
            },function(companiesErr){
                if (companiesErr.status === 401) {
                    Utils.renewAuth();
                }
                deferred.reject();
            });
        }
        else{
            _processIndividualBulkOrders(respData,authToken,corpID,deferred);
        }
    }, function (error) {
        if (error.status === 401) {
            Utils.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
};

let getErrorRows  = function(param) {
    let deferred = $.Deferred();
    let authToken = Storage.get('authToken'),
        corpID = $("meta[name='corpID']").attr("content");
    let url = Urls.getOMSApiUrl().GET_ERROR_ROWS.replace(":CORP_ID:", corpID)
        .replace(":bulkOrderID:", param.bulkOrderID)
        .replace(":authToken:", authToken) + "&pageNumber=" + param.pageNumber + "&pageSize=" + param.pageSize;
    ServiceConnector.get(url).then(function(respData) {
        //console.log('this is table response data',respData);
        let $headerRow = '<tr>';
        for(let i = 0;i<respData.columnHeaders.length;i++){
            if(respData.columnHeaders[i]=='name'){
                //$headerRow += '<th id="sortByName">' + respData.columnHeaders[i].replace(/[A-Z]/g, "") + '</th>';
                $headerRow += '<th id="sortByName">' + respData.columnHeaders[i].replace(/([a-z])([A-Z])/g, '$1 $2') + '</th>';
            }else{
                // $headerRow += '<th>' + respData.columnHeaders[i].replace(/[A-Z]/g, " ") + '</th>';
                $headerRow += '<th>' + respData.columnHeaders[i].replace(/([a-z])([A-Z])/g, '$1 $2') + '</th>';
            }
            
        }
        $headerRow += '<th class="text-right">Reasons</th></tr>';
        if($('#errorRowsTable thead tr').length < 2){
            $('#errorRowsTable thead').append($headerRow);
        } 

        let records = respData.rows.map(function(errorEntry) {
            //console.log('this is table errorEntry data',errorEntry);
            let item = {
                'headers' : [],
                'rows' : []
            };
            errorEntry.row.forEach(function(errorEntryRow,index) {
                item.rows.push({
                    "name" : errorEntryRow
                });
                if(errorEntry.errors[index]){
                    item.rows[index]["error"] = errorEntry.errors[index]
                }
                //console.log(item.rows);  
            });
            for(let i = 0;i<respData.columnHeaders.length;i++){
                item.headers.push({
                    "name" : respData.columnHeaders[i]
                })
            } 

            return item;
        });
        let responseJson = {
            totalCount: Storage.get(Constant.STORAGE_INVALID_BULK_ENTRIES),
            records: records
        };
        console.log(responseJson);
        deferred.resolve(responseJson);

    }, function (error) {
        if (error.status === 401) {
            Utils.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
};

let downloadReport = function(bulkOrderID){
    let url = Urls.getOMSApiUrl().BULK_DOWNLOAD_REPORT
            .replace(":authToken:", Storage.get('authToken'))
            .replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
            .replace(":bulkOrderID:", bulkOrderID);
    return ServiceConnector.get(url);
}

let downloadBulkOrderFile = function(bulkOrderID){ 
    let url = Urls.getOMSApiUrl().DONWLOAD_BULK_ORDER_FILE
            .replace(":authToken:", Storage.get('authToken'))
            .replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
            .replace(":BULK_ORDER_ID:", bulkOrderID);
    return ServiceConnector.get(url);
}

export default {
    createOrder : createOrder,
    actionOnBulkOrder : actionOnBulkOrder,
    getBulkOrders : getBulkOrders,
    getBulkOrderSummary : getBulkOrderSummary,
    getCompanies : getCompanies,
    getIndividualBulkOrders : getIndividualBulkOrders,
    getErrorRows :getErrorRows,
    downloadReport : downloadReport,
    downloadBulkOrderFile : downloadBulkOrderFile
}

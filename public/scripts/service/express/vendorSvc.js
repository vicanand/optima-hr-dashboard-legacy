import ServiceConnecter from "../../common/serviceConnector";

export default {
    getVendorsTrends: function (params) {
        let requestParams = {
            fromDate: params.extraParams.fromDate,
            toDate: params.extraParams.toDate,
            order: params.extraParams.order,
            orderBy: params.extraParams.orderBy,
            businessID:params.extraParams.businessID,
            stores: params.extraParams.stores                  
        };


        return ServiceConnecter.post(requestParams,'/express/getVendorsTrends?' ).then(function (res) {

            let vendorTrends = _.map(res.vendorTrends, function (item) {
                item.totalSum = item.totalSum / 100;
                return item;
            });

            return {
                records: vendorTrends
            };
        });
    },

    getVendors: function (requestParam) {
        return ServiceConnecter.post(requestParam, '/express/getVendors').then(function (vendors) {

            vendors.vendorDetails = _.map(vendors.vendorDetails, function (item) {
                item.rating = (item.ratingSum / item.ratingCount).toFixed(0);
                return item;
            });

            console.log(vendors);



            return vendors;
        });
    },

    getItemTrends: function (params) {

        let requestParams = {
            businessID: params.extraParams.businessID,
            fromDate: params.extraParams.fromDate.format('YYYY-MM-DD'),
            toDate: params.extraParams.toDate.format('YYYY-MM-DD'),
            order: params.extraParams.order,
            orderBy: params.extraParams.orderBy,
            storeID: params.extraParams.storeID
        };

        return ServiceConnecter.get('/express/getItemTrends?' + $.param(requestParams)).then(function (respData) {
            return {
                records: respData.itemTransactionDetails
            };
        });
    },

    getItemConsumptions: function (params) {

        let requestParams = {
            businessID: params.businessID,
            fromDate: params.fromDate.format('YYYY-MM-DD'),
            toDate: params.toDate.format('YYYY-MM-DD'),
            storeID: params.storeID
        };


        // requestParams.businessID = 122789;
        // requestParams.storeID = 1;
        // requestParams.fromDate = "2017-03-01";
        // requestParams.toDate = "2017-03-31";


        return ServiceConnecter.get('/express/getItemConsumptions?' + $.param(requestParams));
    }
};



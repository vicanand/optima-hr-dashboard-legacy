import ServiceConnector from '../../common/serviceConnector';

let employeeSvc = {

    getEmployeeStats: function (params) {
        return ServiceConnector.get('/express/getEmployeeSnapshotStats?' + $.param(params));
    },
    
   

    getPaymentPreference: function (params) {
         return ServiceConnector.post(params,'/express/getPaymentPreference').then(function (respData) {
            //TODO: This is a hack for supporting other payment                
            return ServiceConnector.post(params,'express/getCafeStatistics').then(function(employeeStats){
                console.log('employeeStats: ', employeeStats);                                                                          
                let payData = {};                  
                var employeeTotalTransactions = employeeStats['successfulTransactions'] ||0 ;
                var paytmTransactions = respData.payTMTransactionCount || 0;
                var cashTransactions = respData.cashTransactionCount || 0;
                var superTagTransactions = respData.superTagTransactionCount || 0;
                var superIdTransactions = respData.superIDTransactionCount || 0;
                var qrTransactions = respData.QRcodeTransactionCount || 0;
                var zetaTransactions = respData.zetaCodeTransactionCount || 0;
                var kioskTransactions= respData.kioskTransactionCount || 0;
                var creditTransactions = respData.debitCreditTransactionCount || 0;
                var edenTransactions = respData.edenredTransactionCount || 0;
                var sodexoTransactions = respData.sodexoTransactionCount || 0;
                var totalTransactionsCount = employeeTotalTransactions - paytmTransactions - cashTransactions - superTagTransactions - superIdTransactions - qrTransactions - zetaTransactions - kioskTransactions - creditTransactions - edenTransactions - sodexoTransactions;
                var employeeTotalSum = employeeStats['totalSales'] || 0;
                var paytmSum = respData.payTMTransactionSum || 0;
                var cashSum = respData.cashTransactionSum || 0;
                var superTagSum = respData.superTagTransactionSum || 0;
                var superIdSum = respData.superIDTransactionSum || 0;
                var qrSum = respData.QRcodeTransactionSum || 0;
                var zetaSum = respData.zetaCodeTransactionSum || 0;
                var kioskSum= respData.kioskTransactionSum || 0;
                var creditSum = respData.debitCreditTransactionSum || 0;
                var edenSum = respData.edenredTransactionSum || 0;
                var sodexoSum = respData.sodexoTransactionSum || 0;
                var totalTransactionSum = employeeTotalSum - paytmSum - cashSum - superTagSum - superIdSum - qrSum - zetaSum - kioskSum - creditSum - edenSum - sodexoSum;

                            payData["zeta"] = {
                                'Super Tag': {
                                    count: respData.superTagTransactionCount,
                                    sum: respData.superTagTransactionSum,
                                    color: '#190F27'
                                },
                                'Super Id': {
                                    count: respData.superIDTransactionCount,
                                    sum: respData.superIDTransactionSum,
                                    color: '#FFBE00'
                                },
                                'QR Code / Shop Id': {
                                    count: respData.QRcodeTransactionCount,
                                    sum: respData.QRcodeTransactionSum,
                                    color: '#F0308C'
                                },
                                'Zeta Code': {
                                    count: respData.zetaCodeTransactionCount,
                                    sum: respData.zetaCodeTransactionSum,
                                    color: '#E74C3C'
                                },
                                'Kiosk': {
                                    count: respData.kioskTransactionCount,
                                    sum: respData.kioskTransactionSum,
                                    color: '#6335AA'
                                }
                            };
                
                
                
                            payData["nonZeta"] = {
                                'Paytm': {
                                    count: respData.payTMTransactionCount,
                                    sum: respData.payTMTransactionSum,
                                    color: '#00B9F5'
                                },
                                'Cash': {
                                    count: respData.cashTransactionCount,
                                    sum: respData.cashTransactionSum,
                                    color: '#9B9B9B'
                                },
                                'Credit/Debit Card':{
                                    count: respData.debitCreditTransactionCount || 0,
                                    sum: respData.debitCreditTransactionSum || 0,
                                    color:'#b15ddc'
                                },
                                'Edenred':{
                                    count: respData.edenredTransactionCount || 0,
                                    sum: respData.edenredTransactionSum || 0,
                                    color:'#168643'
                                },
                                'Sodexo Transactions':{
                                    count: respData.sodexoTransactionCount || 0,
                                    sum: respData.sodexoTransactionSum || 0,
                                    color:'#1b00ff  '
                                },
                                'Other Payments':{            
                                   count:totalTransactionsCount,
                                   sum:totalTransactionSum,                        
                                   color:'#31f500'                        
                                }
                            };                  
                
                            // console.log(payData);
                           return payData;
            });
           
        });
    },

    getEmployeeInsightsTransactionDetails: function (params) {
        return ServiceConnector.get('/express/getEmployeeInsightsTransactionDetails?' + $.param(params)).then(function (respData) {

            let zeroTransaction = _.countBy(respData.transactionDetails, {transactionCount: 0});

            if (!zeroTransaction.false || zeroTransaction.false == 0) {
                respData.transactionDetails = [];
            }
            // debugger;



            let transactionDetails = _.map(respData.transactionDetails, function (item) {
                item.totalSum = item.totalSum / 100;
                item.averageSpent = item.averageSpent / 100;
                item.dateFormatted = moment(item.date).format('DD MMM, YYYY');
                item.shortDateFormatted = moment(item.date).format('DD MMM');
                return item;
            });

            return transactionDetails;
        });
    },

    getUniqueTransactionsTable: function (params) {
        let requestParams = {
            businessID: params.extraParams.office,
            storeID: params.extraParams.store,
            fromDate: params.extraParams.startDate.format('YYYY-MM-DD'),
            toDate: params.extraParams.endDate.format('YYYY-MM-DD')
        };

        return employeeSvc.getEmployeeInsightsTransactionDetails(requestParams).then(function (respData) {
            return {
                records: respData
            };
        });
    }
};

export default employeeSvc;
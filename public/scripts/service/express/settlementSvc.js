import ServiceConnector from '../../common/serviceConnector';


let _this = {
    getSettlements: function (params) {
        return ServiceConnector.get('/express/getSettlementsForCounter?' + $.param(params)).then(function (respData) {
            return respData;
        });
    },

    getSettlementsTable: function (requestParams) {

        let params = {
            toDate: requestParams.extraParams.endDate.format('YYYYMMDD'),
            fromDate: requestParams.extraParams.startDate.format('YYYYMMDD'),
            businessID: requestParams.extraParams.office
            //storeID: requestParams.extraParams.store
        };


        return _this.getSettlements(params).then(function (respData) {


            let settlements = _.map(respData.fragments, function (transaction) {
                transaction.txnEndDate =[];

                if (!(transaction.bankAccount && transaction.bankAccount.accountNumber)) {
                    transaction.bankAccount = '-';
                }

                if (!transaction.zetaReceiptID) {
                    transaction.zetaReceiptID = '-';
                }

                if (!transaction.txnStartDate) {
                    transaction.txnStartDate = '-';
                }
                else {
                    transaction.txnStartDate = moment(transaction.txnStartDate, 'YYYYMMDD').format('Do MMM, YYYY');
                }


               
                if (!transaction.bankTransferSuccessDate) {
                    transaction.bankTransferSuccessDate = '-';
                }
                else {
                    transaction.bankTransferSuccessDate = moment(transaction.bankTransferSuccessDate, 'YYYYMMDD').format('Do MMM, YYYY');
                }
                if (!transaction.bankTransferMode) {
                    transaction.bankTransferMode = '-';
                }
                if (!(transaction.attributes && transaction.attributes.zetaFeeChargePercentage)) {
                    transaction.attributes = {zetaFeeChargePercentage: '-'};
                }
                if (!(transaction.bankRefNumber)) {
                    transaction.bankRefNumber = '-';              
                }
                if(!(transaction.settlementDate)){
                    transaction.settlementDate = '-';
                }          
                if(transaction.clearances.length > 1){
                    var keys = {}
                        for(var i = 0; i < transaction.clearances.length; i++){
                            keys[transaction.clearances[i].saleDate] = true;
                        }
                        var dates = Object.keys(keys);
                        transaction.txnEndDate = dates;  
                }else{
                        if(transaction.clearances[0].saleDate.split('_').length > 1){
                            var date = transaction.clearances[0].saleDate.split('_');
                            transaction.txnEndDate.push(date.pop());
                        } else {
                            transaction.txnEndDate.push(transaction.clearances[0].saleDate);
                        }
                    
                }                     
                console.log('transaction', transaction);
                return transaction;
            });

            let response = {
                records: settlements
            };

            return response;
        });
    },

    getSettlementMetrics: function (requestParams) {
        return _this.getSettlementsSnap(requestParams);
    },

    getSettlementsSnap: function (params) {
        return ServiceConnector.get('/express/getSettlementMetrics?' + $.param(params)).then(function (respData) {
            return respData;
        });
    }


};


export default _this;
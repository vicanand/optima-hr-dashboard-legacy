import ServiceConnector from '../../common/serviceConnector';

const _this = {

    getSalesMetrics(params) {
        return ServiceConnector.get('/express/getSalesTransactionsMetrics?' + $.param(params));
    },
    getCafeStatistics(params){
        return ServiceConnector.post(params,'/express/getCafeStatistics' );
    },
    getVendorInsightsSnapShot(params){
        console.log('getVendorInsightsSnapShot: ', params);   
       
        return ServiceConnector.get('/express/getVendorInsightsSnapshot?' + $.param(params));
    },

    getSalesTable(params) {
        let requestParams = {
            businessID: params.extraParams.office,
            fromDate: params.extraParams.startDate.format('YYYY-MM-DD'),
            toDate: params.extraParams.endDate.format('YYYY-MM-DD'),    
            storeID: params.extraParams.store                                                       
        };

        return _this.getSalesMetrics(requestParams).then(function (respData) {

            let transactions = [];

            _.forEach(respData.transactionDetails, function (item, key) {

                transactions.push({
                    amount: item.totalSum / 100,
                    transactionCount: item.transactionCount,
                    // uniqueUsers: respData.dateWiseUniqueUsers[key],
                    date: item.date,
                    formatedDate: moment(item.date, 'YYYY-MM-DD').format('Do MMM, YYYY'),
                    shortFormatedDate: moment(item.date, 'YYYY-MM-DD').format('Do MMM')
                });
            });

            return {
                records: transactions
            };
        });

    },

    getFootfall(requestParam = {}) {

        let params = {
            storeID: requestParam.store,
            date: requestParam.date.format('YYYY-MM-DD'),
            businessID: requestParam.office
        };

        return ServiceConnector.get('/express/getSalesFootfall?' + $.param(params));
    }
};



export default _this;
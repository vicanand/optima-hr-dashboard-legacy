import ServiceConnector from '../../common/serviceConnector';
import Utils from '../../common/util';

export default {
    getQuickshopInfo: function (params) {
        let deferred = $.Deferred();

        let requestParams = {
            store: params.office + ':' + params.store,
        };


        ServiceConnector.get('/express/getQuickshopInfo?' + $.param(requestParams)).then(function (respData) {
            deferred.resolve(JSON.parse(Utils.htmlDecoder(respData)));
        }, function (error) {

            let errObj = JSON.parse(Utils.htmlDecoder(error.responseText));

            if (errObj.type == 'QuickshopNotFoundException') {
                deferred.resolve(null);
            }
            else {
                deferred.reject(error);
            }
        });

        return deferred.promise();
    },

    getQuickshopInfoDate: function (params) {
        let deferred = $.Deferred();

        let requestParams = {
            businessID: params.office,
            storeID: params.store,
            date: params.date
        };


        ServiceConnector.get('/express/getQuickshopForDate?' + $.param(requestParams)).then(function (respData) {
            deferred.resolve(JSON.parse(Utils.htmlDecoder(respData)));
        }, function (error) {
            let errObj = JSON.parse(Utils.htmlDecoder(error.responseText));

            if (errObj.type == 'QuickshopNotFoundException') {
                deferred.resolve(null);
            }
            else {
                deferred.reject(error);
            }
        });

        return deferred.promise();
    },


    getListOfUploadedMenus: function (params) {
        let deferred = $.Deferred();

        let requestParams = {
            businessID: params.office,
            storeID: params.store,
        };


        // requestParams.businessID = 137953;


        ServiceConnector.get('/express/getListOfUploadedMenus?' + $.param(requestParams)).then(function (respData) {
            deferred.resolve(respData);
        }, function (error) {
            let errObj = JSON.parse(error.responseText);

            if (errObj.type == 'QuickshopNotFoundException') {
                deferred.resolve(null);
            }
            else {
                deferred.reject(error);
            }
        });

        return deferred.promise();
    }
};
import ServiceConnecter from "../../common/serviceConnector";
import StoresSvc from './storesSvc';
import Utils from '../../common/util';

export default {
    getStoreTrends: function (params) {
        return ServiceConnecter.post(params,'/express/getStoreTrends?').then(function (respData) {

            let storeTransactionDetails = respData.storeTransactionDetails.map(item => {

                item.city = item.city || '-';
                item.totalSum = item.totalSum / 100;
                return item;
            });

            return storeTransactionDetails;
        });
    },


    getTopTenStores: function (params) {
        let requestParams = {
            fromDate: params.extraParams.fromDate,
            toDate: params.extraParams.toDate,
            order: params.extraParams.order,
            orderBy: params.extraParams.orderBy,
            businessID:params.extraParams.businessID,
            stores: params.extraParams.stores
        };

        return StoresSvc.getStoreTrends(requestParams).then(function (res) {
            return {
                records: res
            };
        });
    },

    getStoresTable: function (params) {

        let requestParams = {
            businessID: params.extraParams.businessID
        };

        return ServiceConnecter.get('/express/getStoresByBusiness?' + $.param(requestParams)).then(function (respData) {

            let businessData = JSON.parse(Utils.htmlDecoder(respData));

            const stores = businessData.storeDetails.map(item => {

                if (!item.personInCharge) {
                    item.personInCharge = "NA";
                }

                return item;
            });

            return {
                records: stores,
                // totalRecords: respData.storeDetails.length
            };

        });
    }
};


import ServiceConnector from "../../common/serviceConnector";
import Urls from '../../common/urls';
import Util from '../../common/util';
import Meta from '../../common/metaStorage';
import Storage from '../../common/webStorage';

let alreadyDone = false;
let alreadyBeneficiaryDone = false;

function makeFilter(filterBy, id, list) {   
            let rewardList;
            if(id == 'transferOrderStatus'){
                rewardList = JSON.parse(localStorage.getItem('allAwards_points'));  
                var m = rewardList.map(a => `<option value="${a}">${a}</option>`);
                m = [...new Set(m)]
                m.unshift(`<option value="ALL">All</option>`);
                m.unshift(`<option value="ALL" disabled selected>${filterBy}</option>`);              
                alreadyDone = true;
            } else {
                rewardList = list;
                var m = rewardList.map(a => `<option value="${a}">${a}</option>`);
                m = [...new Set(m)]
                m.unshift(`<option value="">All</option>`);
                m.unshift(`<option value="ALL" disabled selected>${filterBy}</option>`);
                alreadyBeneficiaryDone = true;
            }                                       
            $('#'+id).html(m);                              
}

function getRandomColor() {
            var colors = ['#682ecf','#4700ec','blue','green','#02020a','#b73e2f','#d01593','#0c8ef1','#f10cbc','#36f10c'];
            return colors[Math.floor(Math.random() * 10)];
  }

function getInitialsForName(string = ""){
    const words = string.split(" ");
    return (
        words[0].charAt(0) +
        (words.length > 1 ? words[words.length - 1].charAt(0) : "")
    );
};

function getSetupRewardList(param) {
    let deferred = $.Deferred();
    let programID = $('#getProgramID').val();
    let authToken = Storage.get('authToken');
    let corpID = $("meta[name='corpID']").attr("content");
    let companyID = $("meta[name='companyID']").attr("content");
    let url = Urls.getOMSApiUrl().LIST_PROGRAM_DETAILS
        .replace(':companyID:', companyID)
        .replace(':corpID:', corpID)
        .replace(":token:", authToken)+
        "&filters.pageNumber=" + (param.pageNumber -1) +
        "&filters.pageSize=10";

    ServiceConnector.get(url).then(function (resp) {
        let respData = resp.programDetail.filter(data => data.programID == programID ? data.badgeDetails : '');
        // if(respData.length == 0){
        //     param.pageNumber++;
        //   return getSetupRewardList(param)
        // }
        let records = {};
        let totalCounts = 0;
        console.log('Policy setup', respData);
        if (respData[0] && respData[0].badgeDetails.length > 0) {
            append();
            records = respData[0].badgeDetails.map(function (item) {
                item.programNames = respData[0].name;
                return item;
            });
            totalCounts = respData[0].badgeDetails.length;
        } else {
            append();
            records = [];
            totalCounts = 0;
        }

        function append() {
            $('#currencyName').val(respData[0].currencyName);
            $('#currencySymbol').attr('src', respData[0].currencySymbol);
            $('#description').val(respData[0].description);
            $('#programName').val(respData[0].name);
            $('#conversionRate').val('1 ' + respData[0].currencyName + ' = ' + respData[0].conversionRate + ' ' + respData[0].baseRealCurrency);
        }

        let responseJson = {
            records: records,
            totalCount: totalCounts
        };
        initGAEvent('event', 'Point based Gift Card', 'click', 'List Order, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());
        deferred.resolve(responseJson);
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        initGAEvent('event', 'Point based Gift Card', 'click', 'List Order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', error=' + error + ',timestamp=' + new Date().toLocaleString());
        deferred.reject(error);

    });
    return deferred.promise();
};

 export default {

    getSetupRewardList:getSetupRewardList,

    loadActiveProgramsList:function(pageNo){
        let deferred = $.Deferred();
        let programID = $('#getProgramID').val();
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyID = $("meta[name='companyID']").attr("content");
        let url = Urls.getOMSApiUrl().LIST_PROGRAM_DETAILS
            .replace(':companyID:', companyID)
            .replace(':corpID:', corpID)
            .replace(":token:", authToken) +
            "&filters.pageNumber=" + pageNo +
            "&filters.pageSize=10";

        ServiceConnector.get(url).then(function (responseJson) {                    
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }            
            deferred.reject(error);
        });
        return deferred.promise();
    },



    getRewardIssuedList: function (param) {
        let deferred = $.Deferred();
        let programID = $('#getProgramID').val();
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyID = $("meta[name='companyID']").attr("content");     
        let toDate = param.endDate == "" ? param.startDate : param.endDate; 
        let url = Urls.getOMSApiUrl().GET_FILE_UPLOADER_ORDER_HISTORY
            .replace(':companyID:', companyID)
            .replace(':corpID:', corpID)
            .replace(':programID:', programID)
            .replace(':fileUploadOrderID:', param.searchQuery)
            .replace(':fileUploadOrderStatus:',param.filterValue)
            .replace(":token:", authToken) +
            "&filters.pageNumber=" + (param.pageNumber - 1) +
            "&filters.pageSize=" + param.pageSize +
            "&filters.sqlSortOrder=desc" +            
            "&filters.fromDate=" + param.startDate +
            "&filters.toDate=" + toDate;
            let header = {
                'X-Zeta-AuthToken': authToken
            }
        ServiceConnector.get(url, header).then(function (resp) {
            let records = {};
            let totalCounts = 0;

            records = resp.fileUploadOrderList.map(function (item) {
                item.createdAt = Util.epochToIstDate(item.createdAt);
                item.orderAmount = item.orderAmount ? Util.formatINR(item.orderAmount.amount/100) : 0;
                item.status = item.status.toLowerCase();                               
                return item;
            });
            totalCounts = resp.totalFileUploadOrders;
            let responseJson = {
                records: records,
                totalCount: totalCounts
            };
            initGAEvent('event', 'Point based Gift Card', 'click', 'List Order, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());          
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            initGAEvent('event', 'Point based Gift Card', 'click', 'List Order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', error=' + error + ',timestamp=' + new Date().toLocaleString());
            deferred.reject(error);

        });
        return deferred.promise();
    },

    getRewardSinglePayoutList: function (param) {
        let deferred = $.Deferred();
        let programID = $('#getProgramID').val();
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyID = $("meta[name='companyID']").attr("content");      
        let fileUploadOrderID =  $('#get-oreder-id').html().trim();
        let url = Urls.getOMSApiUrl().GET_ISSUANCE_HISTORY
            .replace(':companyID:', companyID)
            .replace(':corpID:', corpID)
            .replace(':programID:', programID)
            .replace(":token:", authToken)
            .replace(":fileUploadOrderID:", fileUploadOrderID)+
            "&filters.pageNumber=" + (param.pageNumber - 1) +
            "&filters.pageSize=" + param.pageSize +
            "&filters.sqlSortOrder=desc" +
            "&filters.searchString=" + param.searchQuery;            
            let header = {
                'X-Zeta-AuthToken': authToken
            };
        ServiceConnector.get(url, header).then(function (resp) {
            let records = {};
            let totalCounts = 0;
            if(resp.employeeDetailList){
                records = resp.employeeDetailList.map(function (item) {
                    item.updatedAt = Util.epochToIstDate(item.updatedAt); 
                    return item;
                });
            } else {
                records = [];
            }
            totalCounts = resp.issuanceCount;
            let responseJson = {
                records: records,
                totalCount: totalCounts
            };
            initGAEvent('event', 'Point based Gift Card', 'click', 'List Order, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());          
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            initGAEvent('event', 'Point based Gift Card', 'click', 'List Order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', error=' + error + ',timestamp=' + new Date().toLocaleString());
            deferred.reject(error);

        });
        return deferred.promise();
    },

    getRevokeRequests: function (param) {
        let deferred = $.Deferred();
        let programID = $('#getProgramID').val();
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyID = $("meta[name='companyID']").attr("content");
        let searchString = param.searchQuery == "" ? "" : "&filters.searchString="+ param.searchQuery;
        let url = Urls.getOMSApiUrl().GET_REVOKE_REQUESTS
            .replace(":token:", authToken) 
            .replace(':companyID:', companyID)
            .replace(':corpID:', corpID)
            .replace(':programID:', programID)+
            '&&filters.pageNumber=' + (param.pageNumber - 1) +
            '&&filters.pageSize=10' +
            (searchString != "" ? searchString : '');

        ServiceConnector.get(url).then(function (resp) {
            let records = {};
            let totalCounts = 0;
            records = resp.revokeBadgeDetails.map(function (item) {
                item.revokeBadge.createdAt = Util.epochToIstDate(item.revokeBadge.createdAt);
                return item;
            });
            
            totalCounts = resp.count;
            let responseJson = {
                records: records,
                totalCount: totalCounts
            };
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            initGAEvent('event', 'Point based Gift Card', 'click', 'List Order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', error=' + error + ',timestamp=' + new Date().toLocaleString());
            deferred.reject(error);

        });
        return deferred.promise();
    },

    getRevokeTransactionHistory: function (param) {
        let deferred = $.Deferred();
        let programID = $('#getProgramID').val();
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyID = $("meta[name='companyID']").attr("content");
        let url = Urls.getOMSApiUrl().GET_REVOKE_TRANSACTION_HISTORY
            .replace(":token:", authToken) 
            .replace(':companyID:', companyID)
            .replace(':corpID:', corpID)
            .replace(':programID:', programID)+
            '&&filters.pageNumber=' + (param.pageNumber - 1) +
            '&&filters.pageSize=10';

        ServiceConnector.get(url).then(function (resp) {
            let records = {};
            let totalCounts = 0;
            records = resp.transactions.map(function (item) {
                item.timestamp = Util.epochToIstDate(item.timestamp);
                return item;
            });
            totalCounts = resp.totalRecords;
            let responseJson = {
                records: records,
                totalCount: totalCounts
            };
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            initGAEvent('event', 'Point based Gift Card', 'click', 'List Order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', error=' + error + ',timestamp=' + new Date().toLocaleString());
            deferred.reject(error);

        });
        return deferred.promise();
    },

    getRewardActivity: function (param) {
        let deferred = $.Deferred();
        let programID = $('#getProgramID').val();
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyID = $("meta[name='companyID']").attr("content");
        let isSearch = param.searchQuery === '' ? '' : '&filters.searchString=' + param.searchQuery;
        let isFilter = param.filterValue === 'ALL' || param.filterValue === '' ? '' : '&badgeName=' + param.filterValue;
        let url = Urls.getOMSApiUrl().GET_REWARD_LIST
            .replace(':companyID:', companyID)
            .replace(':corpID:', corpID)
            .replace(':programId:', programID)
            .replace(":token:", authToken) +
            '&&filters.pageNumber=' + (param.pageNumber - 1) +
            '&&filters.pageSize=10' + isSearch + isFilter;

        ServiceConnector.get(url).then(function (resp) {
            let records = {};
            let totalCounts = 0;
            records = resp.rewardList.map(function (item) {
                item.initial1 = item.beneficiary.name ? getInitialsForName(item.beneficiary.name) : 'NA';
                item.initialColor1 = getRandomColor();
                item.initial2 = item.issuer.name ? getInitialsForName(item.issuer.name) : 'NA';
                item.initialColor2 = getRandomColor();
                item.iconImage = 'https://card-program-files.s3.amazonaws.com/hrDashboard/images/profileDp.png';
                item.badgeName = 'dummy name';
                item.managericon = 'https://card-program-files.s3.amazonaws.com/hrDashboard/images/profileDp.png';
                return item;
            });
            totalCounts = resp.count;
            let responseJson = {
                records: records,
                totalCount: totalCounts
            };
            initGAEvent('event', 'Point based Gift Card', 'click', 'List Order, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());
            alreadyDone == false ?  makeFilter('Filter by Reward', 'transferOrderStatus') : '';
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            initGAEvent('event', 'Point based Gift Card', 'click', 'List Order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', error=' + error + ',timestamp=' + new Date().toLocaleString());
            deferred.reject(error);

        });
        return deferred.promise();
    },




    getRewardIssuedSummary: function (param) {
        let deferred = $.Deferred();
        let programID = $('#getProgramID').val();
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyID = $("meta[name='companyID']").attr("content");
        let toDate = param.endDate == "" ? param.startDate : param.endDate;
        let url = Urls.getOMSApiUrl().GET_REWARD_ISSUED_SUMMARY
            .replace(':programID:', programID)
            .replace(":token:", authToken) +
            "&companyID=" + companyID +
            "&corpID=" + corpID +
            "&filters.pageNumber=" + (param.pageNumber - 1) +
            "&filters.pageSize=" + param.pageSize +
            "&filters.sqlSortOrder=desc" +
            "&filters.searchString=" + param.searchQuery +
            "&filters.fromDate=" + param.startDate +
            "&filters.toDate=" + toDate;

        ServiceConnector.get(url).then(function (respData) {
            let records = {};
            console.log(respData);
            if(respData.totalBeneficiaryCount !== 0 ) {
                records = respData.employeeDetailList.map(function (item) {
                    item.initial = item.employee.name ? getInitialsForName(item.employee.name) : 'NA';
                    item.initialColor = getRandomColor();
                    item.managerName = item.employee.name ? item.employee.name : 'NA';
                    item.managerEmail = item.employee.emailID ? item.employee.emailID : 'NA';
                    item.businessUnit = item.employee.businessUnit ? item.employee.businessUnit : 'NA'
                    item.rewardsIssued = item.rewardsIssued;
                    item.designation = item.employee.designation;
                    item.isRevokeInProcess = item.isRevokeInProcess || false;
                    item.buID = item.employee.buID;
                    item.managericon = 'https://card-program-files.s3.amazonaws.com/hrDashboard/images/profileDp.png'
                    return item;
                });
                let allAwards = [];
                for(let k in respData.employeeDetailList[0].rewardsIssuedCountMap){
                    allAwards.push(k);
                }
                localStorage.setItem('allAwards_points',JSON.stringify(allAwards));
            } else {
                records = [];
            }
            records.totalRewardsIssued = respData.totalRewardsIssued;
            records.totalRewardsRemaining = respData.totalRewardsRemaining;
            records.totalRewardsRewarded = respData.totalRewardsRewarded;
            records.totalRewardsExpired = respData.totalRewardsExpired || 0;
            records.totalRewardsRevoked = respData.totalRewardsRevoked;
            let totalCounts = respData.totalBeneficiaryCount;
            $('#totalRewardsIssued').text(records.totalRewardsIssued);
            $('#totalRewardsRemaining').text(records.totalRewardsRemaining);
            $('#totalRewardsRewarded').text(records.totalRewardsRewarded);
            $('#totalRewardsExpired').text(records.totalRewardsExpired + records.totalRewardsRevoked);
            let responseJson = {
                records: records,
                totalCount: totalCounts
            };
            initGAEvent('event', 'Point based Gift Card', 'click', 'List Order, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            initGAEvent('event', 'Point based Gift Card', 'click', 'List Order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', error=' + error + ',timestamp=' + new Date().toLocaleString());
            deferred.reject({
                records: {},
                totalCount: 0
            });
        });
        return deferred.promise();
    },

    getBeneficiary: function (param) {
        let deferred = $.Deferred();
        let programID = $('#getProgramID').val();
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyID = $("meta[name='companyID']").attr("content"); 
        let buName = param.filterValue == "" ? "" : "&buName="+ param.filterValue;
        let searchString = param.searchQuery == "" ? "" : "&filters.searchString="+ param.searchQuery;
        let url = Urls.getOMSApiUrl().GET_BENEFICIARY
            .replace(':programID:', programID)
            .replace(":token:", authToken) 
            .replace(":corpID:", corpID)
            .replace(":companyID:", companyID) +     
            (buName != "" ? buName : '') +     
            (searchString != "" ? searchString : '') +
            "&filters.pageNumber=" + (param.pageNumber - 1) +
            "&filters.pageSize=" + param.pageSize +
            "&filters.sqlSortOrder=desc";                     

        ServiceConnector.get(url).then(function (respData) {
            let records = {};
            if(respData.count !== 0 ) {
                records = respData.beneficiaries.map(function (item) {
                    item.initial = item.name ? getInitialsForName(item.name) : 'NA';
                    item.initialColor = getRandomColor();
                    item.employeeName = item.name ? item.name : 'NA';
                    item.employeeEmail = item.emailID ? item.emailID : 'NA';
                    item.businessUnit = item.businessUnit ? item.businessUnit : 'NA'
                    item.designation = item.designation;                    
                    item.buID = item.buID;
                    item.teamID = item.teamID;
                    item.employeeID = item.externalEmployeeID;  
                    item.profileImage = item.profileImage; 
                    item.others = item;                       
                    return item;
                });                
            } else {
                records = [];
            }            

            let totalCounts = respData.count;             
            let responseJson = {
                records: records,
                totalCount: totalCounts
            };
            alreadyBeneficiaryDone == false ?  makeFilter('Filter by BU NAME','beneficiary-filter', respData.bu) : '';
            initGAEvent('event', 'Point based Gift Card', 'click', 'List Order, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());            
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            initGAEvent('event', 'Point based Gift Card', 'click', 'List Order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', error=' + error + ',timestamp=' + new Date().toLocaleString());
            deferred.reject({
                records: {},
                totalCount: 0
            });
        });
        return deferred.promise();
    },

    orderVerifyIssuePoint: function (filename, fileurl, refName, fundingAccountID) {
        let deferred = $.Deferred(),
            programID = $('#programID').val().trim(),
            authToken = Storage.get('authToken'),
            corpID = $("meta[name='corpID']").attr("content"),
            companyID = $("meta[name='companyID']").attr("content");
        let url = Urls.getOMSApiUrl().VERIFY_BUDGET
            .replace(':corpID:', corpID)
            .replace(":companyID:", companyID)
            .replace(':programID:', programID)
            .replace(":fileUrl:", fileurl.replace('&','%26'))
            .replace(':fileName:', filename)
            .replace(":refName:", '')
            .replace(':fundingAccountID:', sessionStorage.getItem('fundingaccountid').trim())
            .replace(":expireAt:", 1894883164000)
        let header = {
            'X-Zeta-AuthToken': authToken
        }
        ServiceConnector.get(url, header).then(function (responseJson) {
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            $('#createOrderModalShow').modal('show');
            deferred.reject(error);
        });
        return deferred.promise();
    },

    upsertEmployees: function (filename, fileurl, refName, fundingAccountID) {
        let deferred = $.Deferred(),
            programID = $('#programID').val().trim(),
            authToken = Storage.get('authToken'),
            corpID = $("meta[name='corpID']").attr("content"),
            companyID = $("meta[name='companyID']").attr("content");
        let url = Urls.getOMSApiUrl().UPSERT_EMPLOYEES;
        let input = {
            "corpID": corpID,
            "companyID": companyID,
            "programID": programID,
            "fileUrl": fileurl,
            "fileName": filename,
            "refName": ''
        }
        let header = {
            'X-Zeta-AuthToken': authToken
        }
        ServiceConnector.post(input, url, header).then(function (respData) {
            deferred.resolve(respData);
        }, function (error) {       
            if (error.status === 401) {
                Util.renewAuth();
            } 
            deferred.reject(error);
        });
        return deferred.promise();
    },

    createBudgetSvc: function (requestID) {
        let authToken = Storage.get('authToken'),
            companyID = $("meta[name='companyID']").attr("content"),
            programID = $('#programID').val(),
            corpID = $("meta[name='corpID']").attr("content");
        let inputObj = {
            "corpID": corpID,
            "companyID": companyID,
            "programID": programID,
            "requestID": requestID
        }
        let url = Urls.getOMSApiUrl().CREATE_BUDGET;
        let headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };
        return ServiceConnector.post(inputObj, url, headers);
    },
    revokebadges:function(issuerEmployeeID, programID, badgeCountMap){
        let authToken = Storage.get('authToken'),
            companyID = $("meta[name='companyID']").attr("content"),
            corpID = $("meta[name='corpID']").attr("content");
        let inputObj = {
            "corpID": corpID,
            "companyID": companyID,
            "programID": programID,
            "issuerEmployeeID": issuerEmployeeID,
            "revokeReason":"MANUAL_EXPIRY",
            "badgeCountMap":null
        }
        let url = Urls.getOMSApiUrl().REVOKE_BADGES;
        let headers = {
            "Content-Type": "application/json",
            "X-Zeta-AuthToken": authToken
        };
        return ServiceConnector.post(inputObj, url, headers);
    }



}
import Constants from '../../common/constants';
import ServiceConnector from "../../common/serviceConnector";
import Urls from '../../common/urls';
import Util from '../../common/util';
import Storage from '../../common/webStorage';


export default {   
    getProductDeatils: function(productId) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_PRODUCT_DETAILS
            .replace(":productId:", productId)
            .replace(":corporateId:", $("meta[name='corpID']").attr("content"))
            .replace(":companyId:", $("meta[name='companyID']").attr("content"))
            .replace(":token:", Storage.get('authToken'))

        ServiceConnector.get(url).then(function(respData) {
            deferred.resolve(respData);
        }, function() {
            deferred.reject();
        });
        return deferred.promise();
    },
    
    getListProduct:function(){
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_LIST_PRODUCT.replace(':spotlightType:','RETAIL_SELECT_GIFTING');
        ServiceConnector.get(url).then(function(respData) {
            deferred.resolve(respData);
        }, function() {
            deferred.reject();
        });
        return deferred.promise();
    },

    // addProductRequest:function(productId){
    //     let deferred = $.Deferred();
    //     let url = Urls.getOMSApiUrl().GET_ADDPRODUCT_REQUEST
    //     .replace(":productId:", productId)
    //     .replace(":spotlightType:", "SELECT_GIFTING")
    //     .replace(":corporateId:", $("meta[name='corpID']").attr("content"))
    //     .replace(":companyId:", $("meta[name='companyID']").attr("content"))
    //     .replace(":token:", Storage.get('authToken'));

    // ServiceConnector.get(url).then(function(respData) {
    //     deferred.resolve(respData);
    // }, function() {
    //     deferred.reject();
    // });
    // return deferred.promise();
    // },

    orderVerify: function(bodyObj) {
        bodyObj.companyId = $("meta[name='companyID']").attr("content");
        bodyObj.corporateId = $("meta[name='corpID']").attr("content");
        let url = Urls.getOMSApiUrl().SELECT_GIFT_ORDER_VERIFY
        return ServiceConnector.post(bodyObj, url);
    },

    orderCreate: function(inputObj) {
        inputObj.companyId = $("meta[name='companyID']").attr("content");
        inputObj.corporateId = $("meta[name='corpID']").attr("content");
        let url = Urls.getOMSApiUrl().SELECT_GIFT_ORDER_CREATE
        return ServiceConnector.post(inputObj, url);

    },

    getIfiDetails: function(authToken, corporateId, ifiID) {
        let url = Urls.getOMSApiUrl().GET_IFIS_ACCOUNTS.replace(':corpID:', corporateId)
        .replace(':ifiID:', ifiID)
        .replace(':type:', "ZETA_SETTLED")
        .replace(':authToken:', authToken);
        return ServiceConnector.get(url);

    },

    getAccDetails : function(authToken, corpID, accountId) {
        let url = Urls.getOMSApiUrl().GET_FUNDING_ACCOUNT
            .replace(':authToken:', authToken)
            .replace(':corpID:', corpID)
            .replace(':accountID:', accountId);
        let inputObj = {};
        let headers = {
            "Content-Type": "application/json"
        };
        return ServiceConnector.get(url, inputObj, headers);
    },

    getErrorRows: function(params) {
        let deferred = $.Deferred();
        let authToken = Storage.get('authToken'),
            corpID = $("meta[name='corpID']").attr("content");
        let $headerRow = '<tr>';
        for (let i = 0; i < respErr.columnHeaders.length; i++) {
            $headerRow += '<th>' + respData.columnHeaders[i] + '</th>';
        }
        $headerRow += '<th class="text-right">Reasons</th></tr>';
        if ($('#errorRowsTable thead tr').length < 2) {
            $('#errorRowsTable thead').append($headerRow);
        }

        let records = respErr.rows.map(function(errorEntry) {
            let item = {
                'rows': []
            };
            errorEntry.row.forEach(function(errorEntryRow, index) {
                item.rows.push({
                    "name": errorEntryRow
                });
                if (errorEntry.errors[index]) {
                    item.rows[index]["error"] = errorEntry.errors[index]
                }
            });
            return item;
        });
        deferred.resolve(responseJson);
        return deferred.promise();
    },
    generateGift: function(inputObj) {
        let dataGiftParam = Util.getParameterByName('claimResource');
        
        let url = Urls.getOMSApiUrl().GET_SELECT_GITFTING_GENERATE.replace(":claimResource:", dataGiftParam)
        let header = {
            "token": Storage.get('authToken'),
        }
        return ServiceConnector.get(url);
    },
    getSelectGiftOrderList: function(param) {
        let deferred = $.Deferred();
       // var fileUrlsGet;
        let authToken = Storage.get('authToken'),
            companyId = $("meta[name='companyID']").attr("content"),
            corpID = $("meta[name='corpID']").attr("content");
        let url = Urls.getOMSApiUrl().GET_SELECT_GITFTING_ORDERLIST
        if (param.filterValue != 'ALL' && param.filterValue != '') {
            var orderStatusFilter = {
                "orderStatus": param.filterValue
            }
        } else {
            var orderStatusFilter = "";
        }
        
        let input = {
            "corporateId": corpID,
            "companyId": companyId,
            "token": authToken,
            "pageNumber": param.pageNumber,
            "pageSize": param.pageSize,
            "listOrderStatus": param.filterValue,
            "searchQuery": param.searchQuery,
            "filterBy": orderStatusFilter,
            "spotlightType":"SELECT_GIFTING",
        }
        let header = {
            "Content-Type": "application/json",
            'token': authToken
        }
        
        ServiceConnector.post(input, url).then(function(respData) {
            console.log(respData);
            respData.listOrders = respData.orders.map(function(item) {
                let createdDate = new Date(item.createdAt);
                item["status"] = Constants.SELECT_GIFTING_STATUS_LABELS[item.status];
                item.amount = (item.orderAmount / 100);
               
                item.discountAmount = ((item.orderAmount * (item.discount / 100)) / 100);
                item.discountAmount = (item.discountAmount - Math.floor(item.discountAmount)) !== 0 ? item.discountAmount.toFixed(2):item.discountAmount;
                item.createdOn = Util.dateFormat(createdDate.getDate(), createdDate.getMonth() + 1, createdDate.getFullYear());
                item.brandName = Util.htmlDecoder(item.brandName);
                item.addBalance = item.addBalance;
                item.fundingAccountId = item.fundingAccountId;
                let urlInput = Urls.OMS_API_URL.GET_SELECT_GITFTING_CSVURL +  
                     "&corporateId=" + item.corporateId + "&companyId=" + item.companyId + 
                    "&spotlightType=" + item.spotlightType + "&orderId=" + item.orderId + 
                     "&token=" + authToken ;
                let urlPInvoice = Urls.OMS_API_URL.GET_SELECT_GITFTING_PINVOICEURL +  
                "&corporateId=" + item.corporateId + "&companyId=" + item.companyId + 
                "&spotlightType=" + item.spotlightType + "&orderId=" + item.orderId + 
                "&token=" + authToken ;
                let urlInvoice = Urls.OMS_API_URL.GET_SELECT_GITFTING_INVOICEURL +  
                "&corporateId=" + item.corporateId + "&companyId=" + item.companyId + 
                "&spotlightType=" + item.spotlightType + "&orderId=" + item.orderId + 
                "&token=" + authToken ;
                item.fileUrl = urlInput;
                item.fileInvoiceUrl = urlInvoice;
                item.filePInvoiceUrl = urlPInvoice;
                
                return item;
                
             });
             
            //let totalCount = $("#totalOrderCount").data('totalordercount');
            let totalCounts = respData.totalRecords;
            if(respData.totalRecords >0){
                Storage.setSessionCollection('totalCountsSG', totalCounts);
            }
            
            let totalcountbe = Storage.getSessionCollection('totalCountsSG');
            let responseJson = {
                records: respData.listOrders,
                totalCount: totalcountbe, 
            };
            deferred.resolve(responseJson);
        }, function() {
            deferred.reject();
        });
        return deferred.promise();
    },

    setFundAcc: function(inputObj) {
        let authToken = Storage.get('authToken');
        console.log(authToken);
        let url = Urls.getOMSApiUrl().ADD_FUND_ACCOUNT.replace(":authToken:", authToken)
        let headers = {
            "Content-Type": "application/json",
        };
        return ServiceConnector.post(inputObj, url);
    },
    

}
import Env from '../../common/env';
import ServiceConnector from "../../common/serviceConnector";
import Urls from '../../common/urls';
import Util from '../../common/util';
import Storage from '../../common/webStorage';
import Meta from '../../common/metaStorage';


export default {   

    getTemplateByIdDetails:function(programId) {
    let deferred = $.Deferred();
         let url = Urls.getOMSApiUrl().GET_TEMLATEBYID_DETAILS
            .replace(":programId:",programId )
            .replace(":corporateID:", $("meta[name='corpID']").attr("content"))
            .replace(":companyID:", $("meta[name='companyID']").attr("content"))
            .replace(":token:", Storage.get('authToken'));

     ServiceConnector.get(url).then(function(respData) {
         deferred.resolve(respData);
     }, function() {
         deferred.reject();
     });
     return deferred.promise();
    },

    orderCreate:function(fundingAccountId,fileName,key,programId) {
        let deferred = $.Deferred();
        let body = {
            "fileName":fileName,
            "poNumber":"",
            "companyID": $("meta[name='companyID']").attr("content"),
            "corpID":$("meta[name='corpID']").attr("content"),
            "type":"reward",
            "key":key,
            "location": Storage.getSessionCollection('storage-fileurl'),
            "allowedMerchants":[], 
            "programID":programId,
            "fundingAccountID":fundingAccountId
        };
        let header = {};
        let url = Urls.getOMSApiUrl().CREATE_ORDER
        // let url = Urls.getCorpApiUrl().ORDER_CREATE
                .replace(":corpID:", $("meta[name='corpID']").attr("content"))
                .replace(":COMPANY_ID:", $("meta[name='companyID']").attr("content"))
                .replace(":token:", Storage.get('authToken'));
    
         ServiceConnector.post(body, url, header).then(function(respData) {
             deferred.resolve(respData);
         }, function() {
             deferred.reject();
         });
         return deferred.promise();
        },
    

     createCustomCard:function(cardName,bg,fundingAccountID,message,greeting){
        let deferred = $.Deferred();

        let body = {
            "companyID": $("meta[name='companyID']").attr("content"),
            "corpID": $("meta[name='corpID']").attr("content"),
            "cardProgram": { 
                "cardName": cardName,
                "productType": "Reward",
                "cardDesign": {
                    "bg": bg
                },
                "notificationConfiguration": {
                    "templateGroupSuffix": "REWARDGIFTS",
                    "issueNotificationForRegisteredUser": false,
                    "issueNotificationEnabled": false
                }
            },
            "fundingAccountID": fundingAccountID,
            "productType": "reward",
            "message":message,
            "greeting":greeting,
            "corpProductType":"SPOTLIGHT"
        }
        let header = {};
         let url = Urls.getOMSApiUrl().CREATE_CUSTOM_CARD
         .replace(":token:", Storage.get('authToken'));

         ServiceConnector.post(body, url, header).then(function(respData) {
            deferred.resolve(respData);
        }, function() {
            deferred.reject();
        });
        return deferred.promise();
       },


    // orderVerify: function(bodyObj) {
    //     bodyObj.companyId = $("meta[name='companyID']").attr("content");
    //     bodyObj.corporateId = $("meta[name='corpID']").attr("content");
    //     let url = Urls.getOMSApiUrl().SELECT_GIFT_ORDER_VERIFY
    //     return ServiceConnector.post(bodyObj, url);
    // },

    

    // orderCreate: function(inputObj) {
    //     inputObj.companyId = $("meta[name='companyID']").attr("content");
    //     inputObj.corporateId = $("meta[name='corpID']").attr("content");
    //     let url = Urls.getOMSApiUrl().SELECT_GIFT_ORDER_CREATE
    //     return ServiceConnector.post(inputObj, url);

    // },

    // getIfiDetails: function(authToken, corporateId, ifiID) {
    //     let url = Urls.getOMSApiUrl().GET_IFIS_ACCOUNTS.replace(':corpID:', corporateId)
    //     .replace(':ifiID:', ifiID)
    //     .replace(':type:', "ZETA_SETTLED")
    //     .replace(':authToken:', authToken);
    //     return ServiceConnector.get(url);

    // },

    // getAccDetails : function(authToken, corpID, accountId) {
    //     let url = Urls.getOMSApiUrl().GET_FUNDING_ACCOUNT
    //         .replace(':authToken:', authToken)
    //         .replace(':corpID:', corpID)
    //         .replace(':accountID:', accountId);
    //     let inputObj = {};
    //     let headers = {
    //         "Content-Type": "application/json"
    //     };
    //     return ServiceConnector.get(url, inputObj, headers);
    // },

    // getErrorRows: function(params) {
    //     let deferred = $.Deferred();
    //     let authToken = Storage.get('authToken'),
    //         corpID = $("meta[name='corpID']").attr("content");
    //     let $headerRow = '<tr>';
    //     for (let i = 0; i < respErr.columnHeaders.length; i++) {
    //         $headerRow += '<th>' + respData.columnHeaders[i] + '</th>';
    //     }
    //     $headerRow += '<th class="text-right">Reasons</th></tr>';
    //     if ($('#errorRowsTable thead tr').length < 2) {
    //         $('#errorRowsTable thead').append($headerRow);
    //     }

    //     let records = respErr.rows.map(function(errorEntry) {
    //         let item = {
    //             'rows': []
    //         };
    //         errorEntry.row.forEach(function(errorEntryRow, index) {
    //             item.rows.push({
    //                 "name": errorEntryRow
    //             });
    //             if (errorEntry.errors[index]) {
    //                 item.rows[index]["error"] = errorEntry.errors[index]
    //             }
    //         });
    //         return item;
    //     });
    //     deferred.resolve(responseJson);
    //     return deferred.promise();
    // },

    getRewardOrderList: function(param) {
        let deferred = $.Deferred();
       // var fileUrlsGet;
       var orderStatusFilter;
        let authToken = Storage.get('authToken'),
            companyId = $("meta[name='companyID']").attr("content"),
            corpID = $("meta[name='corpID']").attr("content");
        let url = Urls.getOMSApiUrl().GET_REWARD_DETAILS.replace(':token:', authToken); 
         if (param.filterValue) {
              orderStatusFilter= param.filterValue;
            }
         else {
              orderStatusFilter = "ALL";
         }
         if (param.startDate=='' || param.startDate.indexOf("Invalid") == -1) {
            param.startDate= null;
          }
        else {
            param.startDate = param.startDate;
        }
       if (param.endDate=='' || param.endDate.indexOf("Invalid") == -1) {
        param.endDate= null;
        }
        else {
            param.endDate = param.endDate;
        }
        
        
        let input = {
            "corpID": corpID,
            "companyID": companyId,
            "token": authToken,
            "pageNo": param.pageNumber,
            "pageSize": param.pageSize,
            "criteria": orderStatusFilter,
            "orderId": param.searchQuery,
            "fromDate":param.startDate,
            "toDate":param.endDate,
            "orderType":"reward",
        }
        let header = {
            "Content-Type": "application/json",
        }
        
        ServiceConnector.post(input, url).then(function(respData) {
            console.log(respData);
            let recData = {};
            recData.orderDetails = respData.orderDetails.map(function(item) {
                 let createdDate = new Date(item.order.createdOn);
                // item["status"] = Constants.SELECT_GIFTING_STATUS_LABELS[item.status];
                 item.createdOn = Util.dateFormat(createdDate.getDate(), createdDate.getMonth() + 1, createdDate.getFullYear());
                item.programID
                     if(Env.CURR_ENV === 'STAGE'){
                        item.fileUrl = "https://mv-stage.dash.zeta.in/account/orders/" +
                                        +item.order.orderID + "/downloadUploadedFile?token=" + authToken + "&corpID=" + corpID;
                        item.inVoiceUrl = "https://mv-stage.dash.zeta.in/account/orders/" +
                                        +item.order.orderID + "/invoice?token=" + authToken + "&corpID=" + corpID;

                        item.orderReportFile =  "https://mv-stage.dash.zeta.in/account/orders/" +
                        +item.order.orderID + "/payouts/download?token=" + authToken + "&corpID=" + corpID;
                        item.performaUrl = Urls.getCorpApiUrl().GET_PERFORMA_INVOICE.replace(':orderId', item.order.orderID) +
                        "?token=" + authToken + "&corpID=" + corpID;  
                    }
                    else{
                        item.fileUrl = "https://corp.zetaapps.in/account/orders/" +
                                        +item.order.orderID + "/downloadUploadedFile?token=" + authToken + "&corpID=" + corpID;
                        item.inVoiceUrl = "https://corp.zetaapps.in/account/orders/" +
                                        +item.order.orderID + "/invoice?token=" + authToken + "&corpID=" + corpID;
                        item.orderReportFile =  "https://corp.zetaapps.in/account/orders/" +
                                        +item.order.orderID + "/payouts/download?token=" + authToken + "&corpID=" + corpID;
                        item.performaUrl = Urls.getCorpApiUrl().GET_PERFORMA_INVOICE.replace(':orderId', item.order.orderID) +
                                        "?token=" + authToken + "&corpID=" + corpID;                                     
                    }                   
                return item;
                
             });
            let totalCount = $("#totalOrderCount").data('totalordercount');
            let responseJson = {
                totalCount: respData.totalOrders, 
                records: respData.orderDetails
            };
            initGAEvent('event', 'Reward Card-Based', 'click', 'List Order, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+',timestamp=' + new Date().toLocaleString());
            deferred.resolve(responseJson);
        }, function(error) {
            initGAEvent('event', 'Reward Card-Based', 'click', 'List Order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', error='+ error +',timestamp=' + new Date().toLocaleString());
            deferred.reject();
        });
        return deferred.promise();
    },


     getProgramBenifitEmployeesV2 : function(params) {
        let deferred = $.Deferred(); 
        let programtype = "reward";  
        //params["secondaryFilterValue"] = params.transferId;
        //params["filterValue"] = $("meta[name='companyID']").attr("content");  
        let item = {};
        let url = Urls.getOMSApiUrl().GET_EMPLOYEESV2_REWARD
        .replace(":programType:", programtype)
        .replace(":corpID:", $("meta[name='corpID']").attr("content"))
        .replace(":companyID:", $("meta[name='companyID']").attr("content"))
        .replace(":token:", Storage.get('authToken')) + "&pageNo=" + (params.pageNumber - 1) + "&pageSize=" + params.pageSize;         
        ServiceConnector.get(url, {}).then(function(respData) {
            item = respData;
              if(item.employees.length > 0 ){
                 for(let i = 0; i< item.employees.length; i++){
                     item.employees[i].activeCardCount = 0;
                     item.employees[i].closedCardCount = 0;
                     if(item.employees[i].cards){
                         for(let j = 0; j< item.employees[i].cards.length; j++){
                          //   set1.add(item.employees[i].cards[j].programID);
                          if(item.employees[i].cards[j].status === 'ACTIVE'){
                             item.employees[i].activeCardCount++;
                          }
                          if(item.employees[i].cards[j].status === 'CLOSED'){
                             item.employees[i].closedCardCount++;
                          }
                         }
                     }
                 }
             }
            let responseObject = {
                totalCount: item.numberOfTotalResults,
                records: item.employees
            };
           
               // Storage.setCollection('employeeDetails',item.employees);
          
            console.log('employee Details',responseObject);
            initGAEvent('event', 'Reward Card-Based', 'click', 'List Beneficiary, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+',timestamp=' + new Date().toLocaleString());
            deferred.resolve(responseObject);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            initGAEvent('event', 'Reward Card-Based', 'click', 'List Beneficiary Error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+',timestamp=' + new Date().toLocaleString());
            deferred.reject(error);
            // return respError;
        });
        return deferred.promise();
    },

     setFundAcc: function(name) {
         let authToken = Storage.get('authToken');
         let inputObj = {
            "corpID":$("meta[name='corpID']").attr("content"),
            "name":name
         }
         let url = Urls.getOMSApiUrl().ADD_ACCOUNTS.replace(":API_KEY:", authToken);
         let headers = {
             "Content-Type": "application/json",
         };
         return ServiceConnector.post(inputObj, url);
     },
    

}
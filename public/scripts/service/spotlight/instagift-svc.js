import ServiceConnector from "../../common/serviceConnector";
import Urls from '../../common/urls';
import Util from '../../common/util';
import Storage from '../../common/webStorage';

export default {
    setupProgram: function(programType, fundingAccountID) {
        let authToken        = Storage.get('authToken');
        let corp_ID          = parseInt($("meta[name='corpID']").attr("content"));
        let company_ID       = parseInt($("meta[name='companyID']").attr("content"));
        let fundingAccount   = fundingAccountID;
        let url = Urls.getOMSApiUrl().SETUP_NEW_PROGRAM
            .replace(":authToken:", authToken);
        let requestJson = {
            "corpID": corp_ID,
            "companyID": company_ID,
            "programType": programType,
            "fundingAccountID":fundingAccount
        }
        return ServiceConnector.post(requestJson, url, {});
    },
    loadCards: function(programID, refName, cardArr) {
        let authToken        = Storage.get('authToken');
        let corp_ID          = parseInt($("meta[name='corpID']").attr("content"));
        let company_ID       = parseInt($("meta[name='companyID']").attr("content"));
        let program_ID       = parseInt(programID);
        let url = Urls.getOMSApiUrl().LOAD_CARDS
            .replace(":authToken:", authToken);
        let requestJson = {
            "corpID": corp_ID,
            "companyID": company_ID,
            "programID": program_ID,
            "refName": refName,
            "cards": cardArr.cardLoads
        }
        return ServiceConnector.post(requestJson, url, {});
    },
    orderCards: function() {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_ORDER_CARDS
            .replace(":corpID:", $("meta[name='corpID']").attr("content"))
            .replace(":COMPANYID:", $("meta[name='companyID']").attr("content"))
            .replace(":authToken:", Storage.get('authToken'));
        ServiceConnector.get(url).then(function(respData) {
            deferred.resolve(respData);
        }, function() {
            deferred.reject();
        });
        return deferred.promise();
    },
    requestDemo: function() {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_ORDER_CARDS
            .replace(":corpID:", $("meta[name='corpID']").attr("content"))
            .replace(":COMPANYID:", $("meta[name='companyID']").attr("content"))
            .replace(":authToken:", Storage.get('authToken'));
        ServiceConnector.get(url).then(function(respData) {
            deferred.resolve(respData);
        }, function() {
            deferred.reject();
        });
        return deferred.promise();
    },
    saveProgramInterest: function(accountID, programType) {
        let account_ID  = parseInt(accountID);
        let authToken   = Storage.get('authToken');
        let corp_ID     = parseInt($("meta[name='corpID']").attr("content"));
        let company_ID  = parseInt($("meta[name='companyID']").attr("content"));

        let url = Urls.getOMSApiUrl().SAVE_PROGRAM_INTEREST
            .replace(":authToken:", authToken);
        let requestJson = {
            "corpID": corp_ID,
            "companyID": company_ID,
            "accountID": account_ID,
            "programType": programType
        }
        return ServiceConnector.post(requestJson, url, {});
    },
    getFundingAccount: function(param) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_FUNDING_ACCOUNTS
            .replace(":pageNo:", param.pageNumber)
            .replace(":pageSize:", param.pageSize)
            .replace(":corpID:", $("meta[name='corpID']").attr("content"))
            .replace(":authToken:", Storage.get('authToken'));
        ServiceConnector.get(url).then(function(respData) {
            deferred.resolve(respData);
        }, function() {
            deferred.reject();
        });
        return deferred.promise();
    },
    getOrders: function(param) {
        let deferred = $.Deferred();
        let programID = $("#transferId").val();
        let url = Urls.getOMSApiUrl().GET_INSTAGIFT_ORDERS
            .replace(":COMPANYID:", $("meta[name='companyID']").attr("content"))
            .replace(":CORPID:", $("meta[name='corpID']").attr("content"))
            .replace(":PROGRAMID:", programID)
            .replace(":API_KEY:", Storage.get('authToken')) + "&pageNumber=" + (param.pageNumber - 1) + "&pageSize=" + param.pageSize + "&sortOrder=DESC" + "&status=" + param.filterValue;
        ServiceConnector.get(url).then(function(respData) {
            let records = {};
            records = respData.orders.map(function(item) {
                item.amount = Util.formatINR(item.totalAmount.amount/100);
                item.date = Util.epochToIstDate(item.updatedOn);
                item.time = Util.epochToIstTime(item.updatedOn);
                return item;
            });
            let totalCounts = (respData.orderCountByStatus.COMPLETED ? respData.orderCountByStatus.COMPLETED:0) + (respData.orderCountByStatus.PROCESSING ? respData.orderCountByStatus.PROCESSING:0)
            let responseJson = {
                records: records,
                totalCount:totalCounts
            };
            deferred.resolve(responseJson);
        }, function() {
            deferred.reject();
        });
        return deferred.promise();
    },
    downloadReport: function(orderID) {
        let authToken   = Storage.get('authToken');
        let corp_ID     = $("meta[name='corpID']").attr("content");
        let company_ID  = $("meta[name='companyID']").attr("content");

        let url = Urls.getOMSApiUrl().DOWNLOAD_REPORTS
            .replace(":token:", authToken);
        let requestJson = {
            "corpID": corp_ID,
            "reportParameters": {
                "reportID": "INSTAGIFT_PAYOUTS_REPORT",
                "reportContentType": "CSV",
                "parameters": {
                  "ORDER_ID": [orderID],
                  "COMPANY_ID": [company_ID],
                  "CORP_ID": [corp_ID]
                }
              }
        }
        return ServiceConnector.post(requestJson, url, {});
    },
    

    getPayouts: function(param) {
        let deferred = $.Deferred();
        let orderId = $("#orderID").val();
        let url = Urls.getOMSApiUrl().GET_INSTAGIFT_PAYOUTS
            .replace(":COMPANYID:", $("meta[name='companyID']").attr("content"))
            .replace(":CORPID:", $("meta[name='corpID']").attr("content"))
            .replace(":ORDER_ID:", orderId)
            .replace(":API_KEY:", Storage.get('authToken')) + "&pageNumber=" + (param.pageNumber - 1) + "&pageSize=" + param.pageSize;
        if (param.statusFilters != undefined && param.statusFilters != '') {
            url = url + "&status=" + param.statusFilters;
        }
        if (param.searchQuery != undefined && param.searchQuery != '') {
            url = url + "&searchString=" + param.searchQuery;
        }
        ServiceConnector.get(url).then(function(respData) {
            let totalPayouts = 0;
            let totalProcessingPayouts = 0;
            let totalSuccessPayouts = 0;
            let totalFailedPayouts = 0;
            let totalRevokedPayouts = 0;
            let totalPayoutsBlnc = 0;
            let totalProcessingPayoutsBlnc = 0;
            let totalSuccessPayoutsBlnc = 0;
            let totalFailedPayoutsBlnc = 0;
            let totalRevokedPayoutsBlnc = 0;
            var totalPayoutkeys = ['PROCESSING', 'SUCCESS', 'FAILED', 'REVOKED'];
            for (let i = 0; i < totalPayoutkeys.length; i++) {
                if (respData.payoutStatsByStatus[totalPayoutkeys[i]]) {
                    totalPayouts += respData.payoutStatsByStatus[totalPayoutkeys[i]].count || 0;
                    totalPayoutsBlnc += respData.payoutStatsByStatus[totalPayoutkeys[i]].amount || 0;
                }
            }
            if (respData.payoutStatsByStatus && respData.payoutStatsByStatus.PROCESSING) {
                totalProcessingPayouts = respData.payoutStatsByStatus.PROCESSING.count;
            }
            if (respData.payoutStatsByStatus && respData.payoutStatsByStatus.SUCCESS) {
                totalSuccessPayouts = respData.payoutStatsByStatus.SUCCESS.count;
            }
            if (respData.payoutStatsByStatus && respData.payoutStatsByStatus.FAILED) {
                totalFailedPayouts = respData.payoutStatsByStatus.FAILED.count;
            }
            if (respData.payoutStatsByStatus && respData.payoutStatsByStatus.REVOKED) {
                totalRevokedPayouts = respData.payoutStatsByStatus.REVOKED.count;
            }
            if (respData.payoutStatsByStatus && respData.payoutStatsByStatus.PROCESSING) {
                totalProcessingPayoutsBlnc = Util.formatINR(respData.payoutStatsByStatus.PROCESSING.amount / 100);
            }
            if (respData.payoutStatsByStatus && respData.payoutStatsByStatus.SUCCESS) {
                totalSuccessPayoutsBlnc = Util.formatINR(respData.payoutStatsByStatus.SUCCESS.amount / 100);
            }
            if (respData.payoutStatsByStatus && respData.payoutStatsByStatus.FAILED) {
                totalFailedPayoutsBlnc = Util.formatINR(respData.payoutStatsByStatus.FAILED.amount / 100);
            }
            if (respData.payoutStatsByStatus && respData.payoutStatsByStatus.REVOKED) {
                totalRevokedPayoutsBlnc = Util.formatINR(respData.payoutStatsByStatus.REVOKED.amount / 100);
            }
            let records = respData.payouts.map(function(item) {
                item.payoutCompleteDate = Util.epochToIstDate(item.updatedOn);
                item.amount = Util.formatINR(item.amount.amount / 100);
                return item;
            });
            let totalCount = 0;
            totalCount = (respData.payoutStatsByStatus.FAILED ? respData.payoutStatsByStatus.FAILED.count:0) + (respData.payoutStatsByStatus.PROCESSING ? respData.payoutStatsByStatus.PROCESSING.count:0) + (respData.payoutStatsByStatus.SUCCESS ? respData.payoutStatsByStatus.SUCCESS.count:0) + (respData.payoutStatsByStatus.REVOKED ? respData.payoutStatsByStatus.REVOKED.count:0);
            let totalPayoutsBlncInr = Util.formatINR(totalPayoutsBlnc / 100);
            let responseJson = {
                totalCount: totalCount,
                records: records,
                totalPayouts: totalPayouts,
                totalProcessingPayouts: totalProcessingPayouts,
                totalSuccessPayouts: totalSuccessPayouts,
                totalFailedPayouts: totalFailedPayouts,
                totalRevokedPayouts: totalRevokedPayouts,
                totalPayoutsBlnc:totalPayoutsBlncInr,
                totalProcessingPayoutsBlnc:totalProcessingPayoutsBlnc,
                totalSuccessPayoutsBlnc:totalSuccessPayoutsBlnc,
                totalFailedPayoutsBlnc:totalFailedPayoutsBlnc,
                totalRevokedPayoutsBlnc:totalRevokedPayoutsBlnc
            };
            deferred.resolve(responseJson);
        }, function() {
            deferred.reject();
        });


        return deferred.promise();
    },

    getResetPinRequests: function(param) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_RESET_PIN_REQUEST
            .replace(":COMPANYID:", $("meta[name='companyID']").attr("content"))
            .replace(":CORPID:", $("meta[name='corpID']").attr("content"))
            .replace(":API_KEY:", Storage.get('authToken')) + "&pageNumber=" + (param.pageNumber - 1) + "&pageSize=" + param.pageSize + "&status=" + param.filterValue;
        ServiceConnector.get(url).then(function(respData) {
            let records = {};
            records = respData.requests.map(function(item) {
                item.date = Util.epochToIstDate(item.updatedOn);
                item.time = Util.epochToIstTime(item.updatedOn);
                return item;
            });
            let responseJson = {
                records: records,
                totalCount:respData.requests.length,
                resetPinCountByStatus: respData.resetPinCountByStatus
            };
            deferred.resolve(responseJson);
        }, function() {
            deferred.reject();
        });
        return deferred.promise();
    },

    approvePinRequest: function(requestID) {
        let url = Urls.getOMSApiUrl().PROCESS_RESETPIN_REQUEST
            .replace(":COMPANYID:", $("meta[name='companyID']").attr("content"))
            .replace(":CORPID:", $("meta[name='corpID']").attr("content"))
            .replace(":API_KEY:", Storage.get('authToken'))
            .replace(":REQUEST_ID:", requestID)
            .replace(":REJECT:", 'ACCEPT')
        return ServiceConnector.post(null, url, {});
    },
    declinePinRequest: function(requestID) {
        let url = Urls.getOMSApiUrl().PROCESS_RESETPIN_REQUEST
            .replace(":COMPANYID:", $("meta[name='companyID']").attr("content"))
            .replace(":CORPID:", $("meta[name='corpID']").attr("content"))
            .replace(":API_KEY:", Storage.get('authToken'))
            .replace(":REQUEST_ID:", requestID)
            .replace(":REJECT:", 'REJECT')
        return ServiceConnector.post(null, url, {});
    },
    cancelOrder: function(orderId) {
        let url = Urls.getCorpApiUrl().CANCEL_ORDER
            .replace(":COMPANY_ID:", $("meta[name='companyID']").attr("content"))
            .replace(":ORDER_ID:", orderId)
        let header = {
            "token": Storage.get('authToken'),
            "corpID": $("meta[name='corpID']").attr("content")
        }
        return ServiceConnector.post(null, url, header);
    },

    cancelPayout: function(orderId, payoutId) {
        let url = Urls.getCorpApiUrl().CANCEL_PAYOUT
            .replace(":COMPANY_ID:", $("meta[name='companyID']").attr("content"))
            .replace(":ORDER_ID:", orderId)
            .replace(":PAYOUT_ID:", payoutId)
        let header = {
            "token": Storage.get('authToken'),
            "corpID": $("meta[name='corpID']").attr("content")
        }
        return ServiceConnector.post(null, url, header);
    },

    revokePayment: function(requestPayload){
        let url = Urls.getOMSApiUrl().REVOKE_PAYMENT
                    .replace(":authToken:", Storage.get('authToken'));
        return ServiceConnector.post(requestPayload, url);
    },

    revokePayout: function(orderId, payoutId) {
        let url = Urls.getCorpApiUrl().REVOKE_PAYOUT
            .replace(":COMPANY_ID:", $("meta[name='companyID']").attr("content"))
            .replace(":ORDER_ID:", orderId)
            .replace(":PAYOUT_ID:", payoutId);
        let header = {
            "token": Storage.get('authToken'),
            "corpID": $("meta[name='corpID']").attr("content")
        }
        return ServiceConnector.post(null, url, header);
    },

    getTopMerchants: function(aggregationType) {
        let url = Urls.getOMSApiUrl().GET_AGGREGATIONS
            .replace(":companyID:", $("meta[name='companyID']").attr("content"))
            .replace(":corpID:", $("meta[name='corpID']").attr("content"))
            .replace(":programID:", $('#transferId').val())
            .replace(":authToken:", Storage.get('authToken'))
            .replace(':aggregationType:', aggregationType);
        let inputObj = {};
        let headers = {
            "Content-Type": "application/json"
        };
        return ServiceConnector.get(url, inputObj, headers);
    }

}
import Env from '../../common/env';
import ServiceConnector from "../../common/serviceConnector";
import Urls from '../../common/urls';
import Util from '../../common/util';
import Meta from '../../common/metaStorage';
import Storage from '../../common/webStorage';


export default {

    createPhysicalCardOrder: function (body) {
        let authToken = Storage.get('authToken');
        let corp_ID = parseInt($("meta[name='corpID']").attr("content"));
        let company_ID = parseInt($("meta[name='companyID']").attr("content"));

        let url = Urls.getOMSApiUrl().CREAT_PHYSICAL_ORDER
            .replace(":authToken:", authToken);
        let requestJson = {
            "programType": 'PHYSICAL_NON_PERSONALISED',
            //"programID": body.programID,
            "corporateID": corp_ID,
            "companyID": company_ID,
            "referenceName": body.referenceName,
            "numberOfCards": parseInt(body.numberOfCards),
            "name": body.name,
            "phoneNumber": body.phoneNumber,
            "address": body.address,
            "pinCode": parseInt(body.zipcode),
            "city": body.city,
            "state": body.state,
            "country": body.country,
            "attributes": {

            }
        };
        return ServiceConnector.post(requestJson, url, {});
    },

    orderVerifyBulk: function (filename, fileurl, key, refName, allowDuplicates) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().CREATE_ISSUE_BULK_ORDER_VERIFY
            .replace(":token:", Storage.get('authToken'));
        let header = {};
        let communicationMode = $('#SpotlightIFIName').val() == 'HDFC' ? "EmailAndMobile" : 'EmailOrMobile';
        let body = {
            "fileName": filename,
            "fileUrl": fileurl,
            "corporateID": $("meta[name='corpID']").attr("content"),
            "companyID": $("meta[name='companyID']").attr("content"),
            "payloadType": "FILE",
            "key": key,
            "refName": refName,
            "communicationMode": communicationMode,
            "spotlightProgram": "PHYSICAL_NON_PERSONALISED",
            "allowDuplicates": allowDuplicates,
        };

        ServiceConnector.post(body, url, header).then(function (respData) {
            deferred.resolve(respData);
        }, function (respErr) {
            console.log('Status', respErr.status);
            $('#showBtnLoader').hide();
            $('#create-order').attr('disable', false).css('background', '#ffbc00');
            if (respErr) {
                if (respErr.status == 500) {
                    $('#createOrderModalSGOops').modal('show');
                } else if (respErr.status == 412) {
                    $('#addBenefitsBulkPhysical').hide();
                    $('#fileErrorWrpr').show();

                    history.pushState('', "", "#errorstate");
                    respErr = JSON.parse(respErr.responseText);
                    if(Object.keys(respErr.attributes).length === 0 && respErr.message === 'INVALID_REQUEST') {
                        $('.file-name').html(localStorage.getItem('filename'));
                        $('.duplicateOr, .duplicateEntries').hide();
                        var dvTable = $("#errorVirtualGiftingTransfers1");
                        dvTable.html("");
                        $('#createOrderModalSGOops').modal('show');
                        deferred.reject();
                    }
                    if (respErr.attributes.orderVerifyErrorStatesSet.length === 1 && respErr.attributes.orderVerifyErrorStatesSet[0].toLowerCase() === 'duplicate') {
                        Storage.set('duplicate', true);
                        $('.duplicateOr, .duplicateEntries').show();
                    } else {
                        Storage.set('duplicate', false);
                        $('.duplicateOr, .duplicateEntries').hide();
                    }
                    if (respErr.attributes.fileProcessingSummary) {

                        if (respErr.attributes.fileProcessingSummary.invalidRows > 0) {
                            $('#uploadAgain').html('UPLOAD AGAIN');
                            $('#card-selection-loader').modal("hide");
                            $("#addBenefitsBulk").hide();
                            $('.file-name').html(localStorage.getItem('filename'));
                            $('#noOfInvalidEntries').html(respErr.attributes.fileProcessingSummary.invalidRows);
                            switch ('text/csv') {
                                case "text/csv":
                                    $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-12.png");
                                    break;
                                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                                    $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xlsx.png");
                                    break;
                                case "application/vnd.ms-excel":
                                    $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                                    break;
                                default:
                                    $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                                    break;
                            }


                            $('#errorVirtualGiftingTransfers').hide();
                            let responseData = respErr.attributes.rows;
                            var ifi = $("meta[name='ifi']").attr("content");
                            //Build an array containing Customer records.
                            var customers = new Array();
                            if (ifi == "HDFC") {
                                customers.push(["Name", "Email", "Amount (₹)", "Transfer Date", "Mobile", "Reason"]);

                                for (var i = 0; i < responseData.length; i++) {
                                    customers.push([Util.htmlEntities(responseData[i].row[0]), Util.htmlEntities(responseData[i].row[1]), Util.htmlEntities(responseData[i].row[2]), Util.htmlEntities(responseData[i].row[3]), Util.htmlEntities(responseData[i].row[4]), allErrors(responseData[i])]);
                                }
                            } else {
                                customers.push(["Name", "Email or Mobile", "Amount (₹)", "Transfer Date", "Reason"]);

                                for (var i = 0; i < responseData.length; i++) {
                                    customers.push([Util.htmlEntities(responseData[i].row[0]), Util.htmlEntities(responseData[i].row[1]), Util.htmlEntities(responseData[i].row[2]), Util.htmlEntities(responseData[i].row[3]), allErrors(responseData[i])]);
                                }
                            }


                            function allErrors(error) {
                                let slash = '<br/>';
                                let errorsReturn = '';
                                error.errors[0] != undefined ? errorsReturn += error.errors[0][0] + slash : '';
                                error.errors[1] != undefined ? errorsReturn += error.errors[1][0] + slash : '';
                                error.errors[2] != undefined ? errorsReturn += error.errors[2][0] + slash : '';
                                error.errors[3] != undefined ? errorsReturn += error.errors[3][0] + slash : '';
                                error.errors[4] != undefined ? errorsReturn += error.errors[4][0] + slash : '';
                                return errorsReturn;
                            }
                            //Create a HTML Table element.
                            var table = $("<table class='custom-table hover-state error-table custom-table-error' />");
                            table[0].border = "0";
                            //Get the count of columns.
                            var columnCount = customers[0].length;
                            //Add the header row.
                            var row = $(table[0].insertRow(-1));
                            for (var i = 0; i < columnCount; i++) {
                                var headerCell = $("<th />");
                                headerCell.html(customers[0][i]);
                                row.append(headerCell);
                            }
                            //Add the data rows.
                            for (var i = 1; i < customers.length; i++) {
                                row = $(table[0].insertRow(-1));
                                for (var j = 0; j < columnCount; j++) {
                                    if (responseData[i - 1].errors[j] == undefined) {
                                        var cell = $("<td class='' />");
                                        cell.html(customers[i][j]);
                                        row.append(cell);
                                    } else {
                                        var cell = $("<td class='error-column' />");
                                        cell.html(customers[i][j]);
                                        row.append(cell);
                                    }
                                }
                            }
                            var dvTable = $("#errorVirtualGiftingTransfers1");
                            dvTable.html("");
                            dvTable.append(table);
                            $('#fileErrorWrpr').show();
                            $('#uploadAgain').attr('data-type', 'bulk')
                            // $('.modal-backdrop').remove()
                            $('body').removeClass('modal-open')
                            $('#createOrderModalSGOops').modal('hide');
                        }
                    }
                } else {
                    $('#createOrderModalSGOops').modal('show');
                }
            }
            initGAEvent('event', 'Physical Gift Card', 'click', 'Verify bulk order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', filename=' + filename + ',fileurl=' + fileurl + ',timestamp=' + new Date().toLocaleString());
            deferred.reject();
        });
        return deferred.promise();
    },

    orderCreateBulk: function (fundingAccountId, fileName, key, programId, fileurl, refName, requestID) {
        let deferred = $.Deferred();
        let communicationMode = $('#SpotlightIFIName').val() == 'HDFC' ? "EmailAndMobile" : 'EmailOrMobile';
        let body = {
            "payloadType": "FILE",
            // "programID": programId,
            "refName": refName,
            "corporateID": $("meta[name='corpID']").attr("content"),
            "companyID": $("meta[name='companyID']").attr("content"),
            "key": key,
            "fileName": fileName,
            "fileUrl": fileurl,
            "communicationMode": communicationMode,
            "fundingAccountID": fundingAccountId,
            "spotlightProgram": "PHYSICAL_NON_PERSONALISED",
            "requestID": requestID,
        };
        let header = {};
        let url = Urls.getOMSApiUrl().CREATE_ISSUE_BULK_ORDER
            .replace(":token:", Storage.get('authToken'));

        ServiceConnector.post(body, url, header).then(function (respData) {
            deferred.resolve(respData);
        }, function (respErr) {
            $('#createOrderModalSGOops').modal('show');
            deferred.reject(respErr);
        });
        return deferred.promise();
    },



    getAccDetails: function (authToken, corpID, accountId) {
        let url = Urls.getOMSApiUrl().GET_FUNDING_ACCOUNT
            .replace(':authToken:', authToken)
            .replace(':corpID:', corpID)
            .replace(':accountID:', accountId);
        let inputObj = {};
        let headers = {
            "Content-Type": "application/json"
        };
        return ServiceConnector.get(url, inputObj, headers);
    },

    getIfiDetails: function (authToken, corporateId, ifiID) {
        let url = Urls.getOMSApiUrl().GET_IFIS_ACCOUNTS.replace(':corpID:', corporateId)
            .replace(':ifiID:', ifiID)
            .replace(':type:', "ZETA_SETTLED")
            .replace(':authToken:', authToken);
        return ServiceConnector.get(url);

    },

    getCardLoadOrders: function (param) {
        let deferred = $.Deferred();
        let programID = $("#transferId").val();
        let companyID = $("meta[name='companyID']").attr("content");
        let searchID = '';
        let searchRefName = '';
        if (Number(param.searchQuery)) {
            searchID = param.searchQuery;
        } else {
            searchRefName = param.searchQuery;
        }

        console.log(param);
        let body = {
            'spotlightProgram': 'PHYSICAL_NON_PERSONALISED',
            'corporateID': $("meta[name='corpID']").attr("content"),
            'companyID': companyID
        };
        let url = Urls.getOMSApiUrl().GET_PHYSICAL_LOADORDERS
            .replace(":API_KEY:", Storage.get('authToken')) + "&pageNumber=" + param.pageNumber + "&pageSize=" + param.pageSize + "&sortOrder=DESC" + "&status=" + param.filterValue + "&cardOrderID=" + searchID + "&referenceName=" + searchRefName + "&fromDate=" + param.startDate + "&toDate=" + param.endDate;
        ServiceConnector.post(body, url).then(function (respData) {
            console.log(respData);
            let records = {};
            records = respData.cardOrders.map(function (item) {
                item.createdAt = Util.epochToIstDate(item.createdAt);
                item.performaUrl = '/companies/' + companyID + '/spotlight/physical/cardcost-proforma-invoice/' + item.id + '/view/';
                return item;
            });
            let totalCounts = respData.totalRecords;
            let responseJson = {
                records: records,
                totalCount: totalCounts
            };
            initGAEvent('event', 'Physical Gift Card', 'click', 'get Card Load Orders, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());
            deferred.resolve(responseJson);
        }, function () {
            initGAEvent('event', 'Physical Gift Card', 'click', 'get Card Load Orders Error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());
            deferred.reject();
        });
        return deferred.promise();
    },

    getCardIssueOrders: function (param) {
        let deferred = $.Deferred();
        let programID = $("#transferId").val();
        let companyID = $("meta[name='companyID']").attr("content");
        let ifiBankName = $("meta[name='ifi']").attr("content");
        let corpID = $("meta[name='corpID']").attr("content");
        let searchID = ''
        let searchRefName = ''
        if (Number(param.searchQuery)) {
            searchID = param.searchQuery
        } else {
            searchRefName = param.searchQuery
        }
        let url = Urls.getOMSApiUrl().GET_PHYSICAL_ORDERS
            .replace(":API_KEY:", Storage.get('authToken'))
            .replace(":COMPANYID:", $("meta[name='companyID']").attr("content"))
            .replace(':CORPID:', $("meta[name='corpID']").attr("content")) + "&pageNumber=" + (param.pageNumber - 1) + "&pageSize=" + param.pageSize + "&sortOrder=DESC" + "&status=" + param.filterValue + "&spotlightType=PHYSICAL_NON_PERSONALISED" + "&orderID=" + searchID + "&refName=" + searchRefName + "&fromDate=" + param.startDate + "&toDate=" + param.endDate;
        ServiceConnector.get(url).then(function (respData) {
            console.log(respData);
            let records = {};
            records = respData.orders.map(function (item) {
                item.createdOn = Util.epochToIstDate(item.createdOn);
                item.totalAmount.amount = Util.formatINR(item.totalAmount.amount / 100);
                item.fileName = 'mohan.csv';
                item.ifiBankName = ifiBankName;
                item.corpID = corpID;
                item.inVoiceUrl = '/companies/' + companyID + '/spotlight/physical/invoice/' + item.orderID + '/view';
                item.performaUrl = '/companies/' + companyID + '/spotlight/physical/proforma-invoice/' + item.orderID + '/view/';
                return item;
            });
            let totalCounts = respData.orderCountByStatus.TOTAL_ORDERS;
            let responseJson = {
                records: records,
                totalCount: totalCounts
            };
            initGAEvent('event', 'Physical Gift Card', 'click', 'get Card Issue Orders, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());
            deferred.resolve(responseJson);
        }, function () {
            initGAEvent('event', 'Physical Gift Card', 'click', 'get Card Issue Orders Error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());
            deferred.reject();
        });
        return deferred.promise();
    },

    getVirtualOrderList: function (param) {

        let deferred = $.Deferred();
        //let programID = $("#transferId").val();
        let ifiBankName = $("meta[name='ifi']").attr("content");
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyID = $("meta[name='companyID']").attr("content");
        let url = Urls.getOMSApiUrl().GET_VIRTUAl_ORDER_DETAILS
            .replace(":companyID:", companyID)
            .replace(":corpID:", corpID)
            //.replace(":PROGRAMID:", 15426)
            .replace(":spotlightType:", 'VIRTUAL_NON_PERSONALISED')
            .replace(":token:", authToken) + "&pageNumber=" + (param.pageNumber - 1) +
            "&pageSize=" + param.pageSize +
            "&sortOrder=DESC" +
            "&status=" + param.filterValue +
            "&orderID=" + param.searchQuery +
            "&fromDate=" + param.startDate +
            "&toDate=" + param.endDate;
        ServiceConnector.get(url).then(function (respData) {
            console.log(respData);
            let records = {};
            records = respData.orders.map(function (item) {
                item.amount = Util.formatINR(item.totalAmount.amount / 100);
                item.date = Util.epochToIstDate(item.createdOn);
                item.time = Util.epochToIstTime(item.createdOn);
                item.designLink = item.designLink;
                item.fileName = 'mohan.csv';
                item.inVoiceUrl = '/companies/' + companyID + '/spotlight/virtual/invoice/' + item.orderID + '/view';
                item.performaUrl = '/companies/' + companyID + '/spotlight/virtual/proforma-invoice/' + item.orderID + '/view/';
                item.ifiBankName = ifiBankName;
                item.corpID = corpID;

                if (Env.CURR_ENV === 'STAGE') {
                    item.fileUrl = "https://mv-stage.dash.zeta.in/account/orders/" +
                        +item.orderID + "/downloadUploadedFile?token=" + authToken + "&corpID=" + corpID;
                    item.orderReportFile = "https://mv-stage.dash.zeta.in/account/orders/" +
                        +item.orderID + "/payouts/download?token=" + authToken + "&corpID=" + corpID;

                } else {
                    item.fileUrl = "https://corp.zetaapps.in/account/orders/" +
                        +item.orderID + "/downloadUploadedFile?token=" + authToken + "&corpID=" + corpID;
                    item.orderReportFile = "https://corp.zetaapps.in/account/orders/" +
                        +item.orderID + "/payouts/download?token=" + authToken + "&corpID=" + corpID;
                }
                return item;
            });
            let totalCounts = respData.orderCountByStatus.TOTAL_ORDERS;
            let responseJson = {
                records: records,
                totalCount: totalCounts
            };
            initGAEvent('event', 'Vitual Gift Card', 'click', 'List Order, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());
            deferred.resolve(responseJson);
        }, function (error) {
            initGAEvent('event', 'Vitual Gift Card', 'click', 'List Order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', error=' + error + ',timestamp=' + new Date().toLocaleString());
            deferred.reject(error);

        });
        return deferred.promise();

    },



    setFundAcc: function (name) {
        let authToken = Storage.get('authToken');
        let inputObj = {
            "corpID": $("meta[name='corpID']").attr("content"),
            "name": name
        }
        let url = Urls.getOMSApiUrl().ADD_ACCOUNTS.replace(":API_KEY:", authToken);
        let headers = {
            "Content-Type": "application/json",
        };
        return ServiceConnector.post(inputObj, url);
    },

    getIndividualOrdersRow: function (params) {

        let deferred = $.Deferred();
        let orderId = $('#get-oreder-id').html();
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyId = $("meta[name='companyID']").attr("content");
        let ifiBankName = $("meta[name='ifi']").attr("content");
        let statusFilter = params.filterValue;
        let searchQuery;
        if (params.searchQuery != "") {
            searchQuery = params.searchQuery;
        } else {
            searchQuery = ""
        }

        let url = Urls.getOMSApiUrl().GET_VIRTUAl_INDIVIDUAL_ORDER.replace(":authToken:", authToken).replace(':corpID:', corpID).replace(':companyID:', companyId).replace(':orderID:', orderId) + '&searchString=' + searchQuery + '&status=' + statusFilter;
        let header = {};
        let records = {};
        let storeCount = 0;
        ServiceConnector.get(url, header).then(function (respData) {
            records = respData.payouts.map(function (item) {
                item.amount = Util.formatINR(item.amount.amount / 100);
                item.date = Util.epochToIstDate(item.updatedOn);
                item.refNo = item.refName;
                item.beneficiaryName = item.name;
                item.ifiBankName = ifiBankName;
                item.corpID = corpID;
                item.orderStatus = item.status.charAt(0).toUpperCase() + item.status.slice(1).toLowerCase();
                // let emailID = item.email != undefined ? item.email : '';
                // let phoneNumber =  item.contact  != undefined ? item.contact : '';
                // item.emailPhone = emailID + '/'  + phoneNumber;
                return item;
            });
            let totalCounts = Number($('#totalPayouts').html()) || 0;
            let responseJson = {
                records: records,
                totalCount: totalCounts
            };
            initGAEvent('event', 'Vitual Gift Card', 'click', 'List Order individual payout, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',orderId=' + orderId + ',timestamp=' + new Date().toLocaleString());
            deferred.resolve(responseJson);
        }, function (error) {
            initGAEvent('event', 'Vitual Gift Card', 'click', 'List Order individual payout error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',orderId=' + orderId + ',timestamp=' + new Date().toLocaleString());
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject(error);
            // return respError;
        });
        return deferred.promise();

    },


    getIndividualOrderSummary: function (orderID) {
        let orderId = orderID;
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyId = $("meta[name='companyID']").attr("content");
        let url = Urls.getOMSApiUrl().GET_VIRTUAl_INDIVIDUAL_ORDER.replace(":authToken:", authToken).replace(':corpID:', corpID).replace(':companyID:', companyId).replace(':orderID:', orderId)
        let header = {};
        return ServiceConnector.get(url, header);
    }


}
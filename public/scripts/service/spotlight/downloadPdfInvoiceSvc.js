import ServiceConnector from "../../common/serviceConnector";
import Urls from '../../common/urls';


let getSelectGiftingInvoice = function (token, companyId, corporateId, orderId, spotlightType, requestFrom) {
  let url;

  if (requestFrom == 'GET_SELECT_GITFTING_INVOICEURL') {
    url = Urls.getOMSApiUrl().GET_SELECT_GITFTING_INVOICEURL.replace(':token:', token).replace(':corpID:', corporateId).replace(':companyID:', companyId).replace(':order:', orderId) + "&spotlightType=SELECT_GIFTING";
  } else if (requestFrom == "cardorder") {
    spotlightType = spotlightType.toLowerCase() == "personalised" ? "SUPER_CARD_PERSONALISED" : "SUPER_CARD_NON_PERSONALISED";
    url = Urls.getOMSApiUrl().GET_CARD_ORDER_INVOICEURL.replace(':token:', token).replace(':corpID:', corporateId).replace(':companyID:', companyId).replace(':order:', orderId).replace(':spotlightProgram:', spotlightType);
  } else if(requestFrom == 'physicalcardcost'){
    url= Urls.getOMSApiUrl().GET_PHYSICAL_CARD_INVOICE.replace(':token:',token).replace(':corpID:',corporateId).replace(':companyID:',companyId).replace(':order:',orderId);
  } else {
    url = Urls.getOMSApiUrl().GET_VIRTUAL_INVOICE.replace(':token:', token).replace(':corpID:', corporateId).replace(':companyID:', companyId).replace(':order:', orderId);
  }


  // + "token="+token+"&="+corporateId+"&companyId="+companyId+"&orderId="+orderId+"&spotlightType=SELECT_GIFTING";

  let inputObj = {
    "token": token,
    "corporateId": corporateId,
    "companyId": companyId,
    "orderId": orderId,
    "spotlightType": spotlightType,
  };
  let headers = {
    "Content-Type": "application/json"
  };
  return ServiceConnector.get(url, headers);
};

let getPointBasedInvoice = function(token, companyId, corporateId ,orderId,programID) {
  let url= Urls.getOMSApiUrl().GET_POINT_BASED_INVOICE
  .replace(':token:',token)
  .replace(':corpID:',corporateId)
  .replace(':companyID:',companyId)
  .replace(':order:',orderId)
  .replace(':programID:', programID);
  let headers = {
      "Content-Type": "application/json"
  };
  return ServiceConnector.get(url, headers);
};



export
default {
  getSelectGiftingInvoice:getSelectGiftingInvoice,
  getPointBasedInvoice:getPointBasedInvoice  
}
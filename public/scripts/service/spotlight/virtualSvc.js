import Env from '../../common/env';
import ServiceConnector from "../../common/serviceConnector";
import Urls from '../../common/urls';
import Util from '../../common/util';
import Meta from '../../common/metaStorage';
import Constant from '../../common/constants';
import Storage from '../../common/webStorage';

export default {

    createNewProgram: function (templateID, email_line_1, email_line_2, cardName, designLink, templateGroupID) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().CREATE_NEW_PROGRAM_VIRTUAL
            .replace(":token:", Storage.get('authToken'));
        let header = {};
        let body = {
            "type": "VIRTUAL_NON_PERSONALISED",
            "companyID": $("meta[name='companyID']").attr("content"),
            "corpID": $("meta[name='corpID']").attr("content"),
            "templateID": templateID,
            "attributes": {
                "email_line_1": email_line_1,
                "email_line_2": email_line_2
            },
            "cardName": cardName,
            "designLink": designLink,
            "templateGroupID": templateGroupID
        };

        ServiceConnector.post(body, url, header).then(function (respData) {
            deferred.resolve(respData);
        }, function () {
            deferred.reject();
        });
        return deferred.promise();
    },

    orderVerifyBulk: function (filename, fileurl, key, allowDuplicates) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().ORDER_VERIFY_BULK_VIRTUAL;
        let token = Storage.get('authToken');
        let header = {};
        let communicationMode = $('#SpotlightIFIName').val() == 'HDFC' ? "EmailAndMobile" : 'EmailOrMobile';
        let body = {
            "token": token,
            "fileName": filename,
            "fileUrl": fileurl,
            "corporateID": $("meta[name='corpID']").attr("content"),
            "companyID": $("meta[name='companyID']").attr("content"),
            "payloadType": "FILE",
            "key": key,
            "communicationMode": communicationMode,
            "spotlightProgram": "VIRTUAL_NON_PERSONALISED",
            "allowDuplicates": allowDuplicates,
        };

        ServiceConnector.post(body, url, header).then(function (respData) {
            deferred.resolve(respData);
        }, function (respErr) {
            respErr = JSON.parse(respErr.responseText);
            if(Object.keys(respErr.attributes).length === 0 && respErr.message === 'INVALID_REQUEST') {
                $('.file-name').html(localStorage.getItem('filename'));
                $('.duplicateOr, .duplicateEntries').hide();
                var dvTable = $("#errorVirtualGiftingTransfers1");
                dvTable.html("");
                $('#createOrderModalFail').modal('show');
                deferred.reject();
            }
            if (respErr.attributes.orderVerifyErrorStatesSet.length === 1 && respErr.attributes.orderVerifyErrorStatesSet[0].toLowerCase() === 'duplicate') {
                Storage.set('duplicateBulk', true);
                $('.duplicateOr, .duplicateEntries').show();
                $('#duplicateEntries').attr('data-url', 'Step3Bulk');
            } else {
                Storage.set('duplicateBulk', false);
                $('.duplicateOr, .duplicateEntries').hide();
            }
            if (respErr.attributes.fileProcessingSummary) {
                if (respErr.attributes.fileProcessingSummary.invalidRows > 0) {
                    $('#virtualGiftContainer').hide();
                    $('#uploadAgain').html('UPLOAD AGAIN');
                    $('#card-selection-loader').modal("hide");
                    $("#addBenefitsBulk").hide();
                    $('.file-details-cntr').show();
                    $('#noOfInvalidEntries').html(respErr.attributes.fileProcessingSummary.invalidRows);
                    $('#fileName').html(Storage.get('filename'));
                    switch (Storage.get('filetype')) {
                        case "text/csv":
                            $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-12.png");
                            break;
                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                            $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xlsx.png");
                            break;
                        case "application/vnd.ms-excel":
                            $("#fileTypeImg").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                            break;
                        default:
                            $(".fileType").attr("src", "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png");
                            break;
                    }


                    $('#errorVirtualGiftingTransfers').hide();
                    var ifi = $("meta[name='ifi']").attr('content');
                    let responseData = respErr.attributes.rows;
                    //Build an array containing Customer records.
                    var customers = new Array();
                    if (ifi == 'HDFC') {
                        customers.push(["Name", "Email", "Amount (₹)", "Transfer Date", "Mobile", "Reason"]);
                        for (var i = 0; i < responseData.length; i++) {
                            customers.push([Util.htmlEntities(responseData[i].row[0]), Util.htmlEntities(responseData[i].row[1]), Util.htmlEntities(responseData[i].row[2]), Util.htmlEntities(responseData[i].row[3]), Util.htmlEntities(responseData[i].row[4]), allErrors(responseData[i])]);
                        }
                    } else {
                        customers.push(["Name", "Email or Mobile", "Amount (₹)", "Transfer Date", "Reason"]);
                        for (var i = 0; i < responseData.length; i++) {
                            customers.push([Util.htmlEntities(responseData[i].row[0]), Util.htmlEntities(responseData[i].row[1]), Util.htmlEntities(responseData[i].row[2]), Util.htmlEntities(responseData[i].row[3]), allErrors(responseData[i])]);
                        }
                    }


                    function allErrors(error) {
                        let slash = '<br/>';
                        let errorsReturn = '';
                        error.errors[0] != undefined ? errorsReturn += error.errors[0][0] + slash : '';
                        error.errors[1] != undefined ? errorsReturn += error.errors[1][0] + slash : '';
                        error.errors[2] != undefined ? errorsReturn += error.errors[2][0] + slash : '';
                        error.errors[3] != undefined ? errorsReturn += error.errors[3][0] + slash : '';
                        error.errors[4] != undefined ? errorsReturn += error.errors[4][0] + slash : '';
                        return errorsReturn;
                    }

                    //Create a HTML Table element.
                    var table = $("<table class='custom-table hover-state error-table custom-table-error' />");
                    table[0].border = "0";

                    //Get the count of columns.
                    var columnCount = customers[0].length;

                    //Add the header row.
                    var row = $(table[0].insertRow(-1));
                    for (var i = 0; i < columnCount; i++) {
                        var headerCell = $("<th />");
                        headerCell.html(customers[0][i]);
                        row.append(headerCell);
                    }

                    //Add the data rows.
                    for (var i = 1; i < customers.length; i++) {
                        row = $(table[0].insertRow(-1));
                        for (var j = 0; j < columnCount; j++) {
                            if (responseData[i - 1].errors[j] == undefined) {
                                var cell = $("<td class='' />");
                                cell.html(customers[i][j]);
                                row.append(cell);
                            } else {
                                var cell = $("<td class='error-column' />");
                                cell.html(customers[i][j]);
                                row.append(cell);
                            }
                        }
                    }

                    var dvTable = $("#errorVirtualGiftingTransfers1");
                    dvTable.html("");
                    dvTable.append(table);
                    $('#fileErrorWrpr').show();
                    $('#uploadAgain').attr('data-type', 'bulk');
                    // $('.modal-backdrop').remove()
                    $('body').removeClass('modal-open')
                    $('#createOrderModalSGOops').modal('hide');
                }
            } else {
                $('#createOrderModalFail').modal('show');
            }
            deferred.reject();
        });
        return deferred.promise();
    },

    orderVerifyManual: function (arrayBeneficiary, allowDuplicates) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().ORDER_VERIFY_BULK_VIRTUAL;
        let header = {};
        let communicationMode = $('#SpotlightIFIName').val() == 'HDFC' ? "EmailAndMobile" : 'EmailOrMobile';
        let body = {
            "token": Storage.get('authToken'),
            "corporateID": $("meta[name='corpID']").attr("content"),
            "companyID": $("meta[name='companyID']").attr("content"),
            "payloadType": "JSON",
            "spotlightProgram": "VIRTUAL_NON_PERSONALISED",
            "beneficiaryList": arrayBeneficiary,
            "communicationMode": communicationMode,
            "allowDuplicates": allowDuplicates
        };

        ServiceConnector.post(body, url, header).then(function (respData) {
            deferred.resolve(respData);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise();
    },

    orderCreateBulk: function (fundingAccountId, fileName, key, programId, fileurl, requestID) {
        let deferred = $.Deferred();
        let communicationMode = $('#SpotlightIFIName').val() == 'HDFC' ? "EmailAndMobile" : 'EmailOrMobile';
        let body = {
            "payloadType": "FILE",
            "programID": programId,
            "corporateID": $("meta[name='corpID']").attr("content"),
            "companyID": $("meta[name='companyID']").attr("content"),
            "key": key,
            "fileName": fileName,
            "fileUrl": fileurl,
            "communicationMode": communicationMode,
            "fundingAccountID": fundingAccountId,
            "spotlightProgram": "VIRTUAL_NON_PERSONALISED",
            "requestID": requestID
        };
        let header = {};
        let url = Urls.getOMSApiUrl().ORDER_CREATE_VIRTUAL
            .replace(":token:", Storage.get('authToken'));

        ServiceConnector.post(body, url, header).then(function (respData) {
            deferred.resolve(respData);
        }, function (respData) {
            deferred.reject(respData);
        });
        return deferred.promise();
    },

    orderCreateManual: function (arrayBeneficiary, fundingAccountId, programId, requestID) {
        let deferred = $.Deferred();
        let communicationMode = $('#SpotlightIFIName').val() == 'HDFC' ? "EmailAndMobile" : 'EmailOrMobile';
        let body = {
            "corporateID": $("meta[name='corpID']").attr("content"),
            "companyID": $("meta[name='companyID']").attr("content"),
            "payloadType": "JSON",
            "spotlightProgram": "VIRTUAL_NON_PERSONALISED",
            "beneficiaryList": arrayBeneficiary,
            "programID": programId,
            "fundingAccountID": fundingAccountId,
            "communicationMode": communicationMode,
            "requestID": requestID,
        };
        let header = {};
        let url = Urls.getOMSApiUrl().ORDER_CREATE_VIRTUAL
            .replace(":token:", Storage.get('authToken'));

        ServiceConnector.post(body, url, header).then(function (respData) {
            deferred.resolve(respData);
        }, function (respData) {
            deferred.reject(respData);
        });
        return deferred.promise();
    },


    getAccDetails: function (authToken, corpID, accountId) {
        let url = Urls.getOMSApiUrl().GET_FUNDING_ACCOUNT
            .replace(':authToken:', authToken)
            .replace(':corpID:', corpID)
            .replace(':accountID:', accountId);
        let inputObj = {};
        let headers = {
            "Content-Type": "application/json"
        };
        return ServiceConnector.get(url, inputObj, headers);
    },

    getIfiDetails: function (authToken, corporateId, ifiID) {
        let url = Urls.getOMSApiUrl().GET_IFIS_ACCOUNTS.replace(':corpID:', corporateId)
            .replace(':ifiID:', ifiID)
            .replace(':type:', "ZETA_SETTLED")
            .replace(':authToken:', authToken);
        return ServiceConnector.get(url);

    },


    getVirtualOrderList: function (param) {

        let deferred = $.Deferred();
        //let programID = $("#transferId").val();
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyID = $("meta[name='companyID']").attr("content");
        let ifiBankName = $("meta[name='ifi']").attr("content");
        if (param.filterValue != '' || param.searchQuery != '' || param.endDate != '') {
            $('#resetFilter').attr('disabled', false)
        }
        let url = Urls.getOMSApiUrl().GET_VIRTUAl_ORDER_DETAILS
            .replace(":companyID:", companyID)
            .replace(":corpID:", corpID)
            //.replace(":PROGRAMID:", 15426)
            .replace(":spotlightType:", 'VIRTUAL_NON_PERSONALISED')
            .replace(":token:", authToken) + "&pageNumber=" + (param.pageNumber - 1) +
            "&pageSize=" + param.pageSize +
            "&sortOrder=DESC" +
            "&status=" + param.filterValue +
            "&orderID=" + param.searchQuery +
            "&fromDate=" + param.startDate +
            "&toDate=" + param.endDate;
        ServiceConnector.get(url).then(function (respData) {
            console.log(respData);
            let records = {};
            records = respData.orders.map(function (item) {
                item.amount = Util.formatINR(item.totalAmount.amount / 100);
                item.date = Util.epochToIstDate(item.createdOn);
                item.time = Util.epochToIstTime(item.createdOn);
                item.designLink = item.designLink;
                item.fileName = 'mohan.csv';
                item.inVoiceUrl = '/companies/' + companyID + '/spotlight/virtual/invoice/' + item.orderID + '/view';
                item.performaUrl = '/companies/' + companyID + '/spotlight/virtual/proforma-invoice/' + item.orderID + '/view/';
                item.ifiBankName = ifiBankName;
                item.corpID = corpID;

                if (Env.CURR_ENV === 'STAGE') {
                    item.fileUrl = "https://mv-stage.dash.zeta.in/account/orders/" +
                        +item.orderID + "/downloadUploadedFile?token=" + authToken + "&corpID=" + corpID;
                    item.orderReportFile = "https://mv-stage.dash.zeta.in/account/orders/" +
                        +item.orderID + "/payouts/download?token=" + authToken + "&corpID=" + corpID;

                } else {
                    item.fileUrl = "https://corp.zetaapps.in/account/orders/" +
                        +item.orderID + "/downloadUploadedFile?token=" + authToken + "&corpID=" + corpID;
                    item.orderReportFile = "https://corp.zetaapps.in/account/orders/" +
                        +item.orderID + "/payouts/download?token=" + authToken + "&corpID=" + corpID;
                }
                return item;
            });
            let totalCounts = respData.orderCountByStatus.TOTAL_ORDERS;
            let responseJson = {
                records: records,
                totalCount: totalCounts
            };
            initGAEvent('event', 'Vitual Gift Card', 'click', 'List Order, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());
            deferred.resolve(responseJson);
        }, function (error) {
            initGAEvent('event', 'Vitual Gift Card', 'click', 'List Order error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ', error=' + error + ',timestamp=' + new Date().toLocaleString());
            deferred.reject(error);

        });
        return deferred.promise();

    },


    getProgramBenifitEmployeesV2: function (params) {
        let deferred = $.Deferred();
        let programtype = "reward";
        let item = {};
        let url = Urls.getOMSApiUrl().GET_EMPLOYEESV2_REWARD
            .replace(":programType:", programtype)
            .replace(":corpID:", $("meta[name='corpID']").attr("content"))
            .replace(":companyID:", $("meta[name='companyID']").attr("content"))
            .replace(":token:", Storage.get('authToken')) + "&pageNo=" + (params.pageNumber - 1) + "&pageSize=" + params.pageSize;
        ServiceConnector.get(url, {}).then(function (respData) {
            item = respData;
            if (item.employees.length > 0) {
                for (let i = 0; i < item.employees.length; i++) {
                    item.employees[i].activeCardCount = 0;
                    item.employees[i].closedCardCount = 0;
                    if (item.employees[i].cards) {
                        for (let j = 0; j < item.employees[i].cards.length; j++) {
                            //   set1.add(item.employees[i].cards[j].programID);
                            if (item.employees[i].cards[j].status === 'ACTIVE') {
                                item.employees[i].activeCardCount++;
                            }
                            if (item.employees[i].cards[j].status === 'CLOSED') {
                                item.employees[i].closedCardCount++;
                            }
                        }
                    }
                }
            }
            let responseObject = {
                totalCount: item.numberOfTotalResults,
                records: item.employees
            };


            console.log('employee Details', responseObject);

            deferred.resolve(responseObject);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject(error);
        });
        return deferred.promise();
    },

    setFundAcc: function (name) {
        let authToken = Storage.get('authToken');
        let inputObj = {
            "corpID": $("meta[name='corpID']").attr("content"),
            "name": name
        }
        let url = Urls.getOMSApiUrl().ADD_ACCOUNTS.replace(":API_KEY:", authToken);
        let headers = {
            "Content-Type": "application/json",
        };
        return ServiceConnector.post(inputObj, url);
    },

    getIndividualOrdersRow: function (params) {
        $('#updateTitle').html($('#SpotlightIFIName').val() == 'HDFC' ? 'Email / Mobile' : 'Email or Mobile')
        let deferred = $.Deferred();
        let orderId = $('#get-oreder-id').html();
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyId = $("meta[name='companyID']").attr("content");
        let ifiBankName = $("meta[name='ifi']").attr("content");
        let statusFilter = params.filterValue;
        let searchQuery;
        if (params.searchQuery != "") {
            searchQuery = params.searchQuery;
        } else {
            searchQuery = ""
        }

        let url = Urls.getOMSApiUrl().GET_VIRTUAl_INDIVIDUAL_ORDER.replace(":authToken:", authToken).replace(':corpID:', corpID).replace(':companyID:', companyId).replace(':orderID:', orderId) + '&searchString=' + searchQuery + '&status=' + statusFilter + "&pageNumber=" + (params.pageNumber - 1) + "&pageSize=" + params.pageSize;;
        let header = {};
        let records = {};
        let storeCount = 0;
        ServiceConnector.get(url, header).then(function (respData) {
            records = respData.payouts.map(function (item) {
                item.amount = Util.formatINR(item.amount.amount / 100);
                item.date = Util.epochToIstDate(item.updatedOn);
                item.refNo = item.refName;
                item.beneficiaryName = item.name;
                item.ifiBankName = ifiBankName;
                item.corpID = corpID;
                item.orderStatus = item.status.charAt(0).toUpperCase() + item.status.slice(1).toLowerCase();
                item.emailorContact = (item.contact != undefined && item.email != undefined) ? item.email + ' / ' + item.contact : item.email != undefined ? item.email : item.contact;
                return item;
            });
            let totalCounts = Number($('#totalPayouts').html()) || 0;
            let responseJson = {
                records: records,
                totalCount: totalCounts
            };
            initGAEvent('event', 'Vitual Gift Card', 'click', 'List Order individual payout, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',orderId=' + orderId + ',timestamp=' + new Date().toLocaleString());
            deferred.resolve(responseJson);
        }, function (error) {
            initGAEvent('event', 'Vitual Gift Card', 'click', 'List Order individual payout error, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',orderId=' + orderId + ',timestamp=' + new Date().toLocaleString());
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject(error);
            // return respError;
        });
        return deferred.promise();

    },


    getIndividualOrderSummary: function (orderID) {
        let orderId = orderID;
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyId = $("meta[name='companyID']").attr("content");
        let url = Urls.getOMSApiUrl().GET_VIRTUAl_INDIVIDUAL_ORDER.replace(":authToken:", authToken).replace(':corpID:', corpID).replace(':companyID:', companyId).replace(':orderID:', orderId)
        let header = {};
        return ServiceConnector.get(url, header);
    },

    revokePayoutsInstaNew: function () {
        $('#revokePayoutConfirm').modal('hide');
        $('#revokePayoutLoader').modal('show');
        let token = Storage.get('authToken');
        let orderID = Storage.get('orderID');
        let payoutID = Storage.get('payoutID');
        let spotlightProgram = Storage.get('program');
        let comment = $('#revokeRemark').val();
        let inputObj = {
            "corpID": $("meta[name='corpID']").attr("content"),
            "companyID": $("meta[name='companyID']").attr("content"),
            "payoutID": payoutID,
            "orderID": orderID,
            "remarks": comment,
            "spotlightProgram": spotlightProgram
        }
        let url = Urls.getOMSApiUrl().REVOKE_API_NEW_INSTA.replace(":token:", token);
        let headers = {
            "Content-Type": "application/json",
        };
        ServiceConnector.post(inputObj, url).then(function (respData) {
            $('#revokePayoutLoader').modal('hide');
            $('#revokePayoutSuccess').modal('show');
        }, function (error) {
            $('#revokePayoutLoader').modal('hide');
            $('#revokePayoutError').modal('show');
        });
    },

    reissePayout: function (name, email, phone, comments, corpID, companyID) {
        let token = Storage.get('authToken');
        let orderID = localStorage.getItem('reissueorderID');
        let payoutID = localStorage.getItem('reissuepayoutID');
        let url = Urls.getOMSApiUrl().REISSUE_API
            .replace(':token:', token)
            .replace(':corpID:', corpID)
            .replace(':name:', name)
            .replace(':phone:', phone)
            .replace(':email:', email)
            .replace(':comments:', comments)
            .replace(':companyID:', companyID)
            .replace(':orderID:', orderID)
            .replace(':payoutID:', payoutID)
        let inputObj = {};
        let headers = {
            "Content-Type": "application/json"
        };
        return ServiceConnector.get(url, inputObj, headers);
    },


}
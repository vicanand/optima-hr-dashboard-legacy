import Urls from "../../common/urls";
import ServiceConnector from "../../common/serviceConnector";
import IFIMap from '../../common/ifiConfig';
import Storage from '../../common/webStorage';

let authToken = Storage.get('authToken')
let corpID = $("meta[name='corpID']").attr("content")
let companyID = $("meta[name='companyID']").attr("content")
let ifiName = $("meta[name='ifi']").attr("content")
let ifiId = IFIMap.getIfiId(ifiName);
let corpbenIfiId = IFIMap.corpbenIfiId(ifiName)
let getInvoices = function(params) {
    let url = Urls.getOMSApiUrl().GET_INVOICES
    // .replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
    .replace(":CORP_ID:", corpID)
    .replace(":TOKEN:", authToken)
    .replace(":PAGE_NO:", params.pageNumber)
    .replace(":PAGE_SIZE:", params.pageSize)
    
    url += "&sortOrder=ASC"

    if(params.filterValue != "ALL" || params.filterValue != ""){
        url += "&status="+params.filterValue;
    }

    let deferred = $.Deferred();    
    ServiceConnector.get(url, {}).then(function(respData){        
        respData['records'] = respData.entries;
        respData['totalCount'] = respData.totalEntries; 
        let programIds = []
        respData.entries.forEach((element, index) => {
            let invoiceDate = new Date(element.invoiceDate).toUTCString().split(" ");
            respData.entries[index].invoiceDate = invoiceDate[1] + " " + invoiceDate[2] + " " + invoiceDate[3]
            programIds.push(element.programId)
        });

        let url1 = Urls.getOMSApiUrl().GET_PROGRAMDETAILSV2
        .replace(":API_KEY:", authToken)

        let inputObj = {
            corpID: corpID,
            programIDs: programIds
        };
        let headers = {
            "Content-Type": "application/json"
        };
        if(programIds.length > 0){
            ServiceConnector.post(inputObj, url1, headers).then(function(programsData){   
                respData.entries.forEach((element, index) => {
                    respData.entries[index].programName = programsData.programs[element.programId] ? programsData.programs[element.programId].cardDesign.cardName : element.programId
                });
        
                deferred.resolve(respData);
            })
        }else{
            deferred.resolve(respData);
        }

        
    }, function(error) {
        deferred.reject(error);
    });
    return deferred.promise();
}

let getCommercials = function(){

    let url = Urls.getOMSApiUrl().GET_INVOICE_CONFIGS
          .replace(":token:", authToken)
          .replace(":companyID:", companyID)
          .replace(":corpID:", corpID)
    return ServiceConnector.get(url, {});
}

let getAllCommercials = function(){
    let url = Urls.getOMSApiUrl().GET_COMMERCIALS
    .replace(":token:", authToken)
    .replace(":ifiID:", corpbenIfiId)

    return ServiceConnector.get(url, {});
}

let getCardMasterDetails = function() {
    let url = Urls.getOMSApiUrl().GET_CARD_MASTER_DETAILS
    .replace(":token:", authToken)
    .replace(":ifiID:", ifiId)

    return ServiceConnector.get(url, {});
}

let updateInvoiceConfig = function(commercialId, addressObj){
    let url = Urls.getOMSApiUrl().UPDATE_INVOICE_CONFIG.replace(":token:", authToken)
    let postObj = {
        "id": commercialId,
        "companyID": companyID,
        "corpID": corpID,
        "billingAddress": {
            "billingAddress": addressObj
        }
    }
    let headers = {
        "Content-Type": "application/json"
    };
    return ServiceConnector.post(postObj, url, headers);
}

export default {
    getInvoices: getInvoices,
    getCommercials: getCommercials,
    getAllCommercials: getAllCommercials,
    getCardMasterDetails: getCardMasterDetails,
    updateInvoiceConfig: updateInvoiceConfig
}
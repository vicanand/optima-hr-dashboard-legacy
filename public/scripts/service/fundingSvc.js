import ServiceConnector from "../common/serviceConnector";
import Urls from '../common/urls';
import Util from "../common/util";
import FundingHomeDOMEvent from '../dom-events/fundingHomeDOMEvent.js';
import Meta from '../common/metaStorage.js';
import Storage from '../common/webStorage'

export default {
    accDetails: '',

    openNewFundAccSvc: function(reqObj) {
        let url = Urls.getOMSApiUrl().ADD_ACCOUNTS.replace(":API_KEY:", Storage.get('authToken'))
        return ServiceConnector.post(reqObj, url);
    },

    addFundstoAcc: function(reqObj) {
        let url = Urls.getOMSApiUrl().ADD_FUNDS.replace(":API_KEY:", Storage.get('authToken'));
        ServiceConnector.post(reqObj, url).then(function() {
            initGAEvent('event', 'Funding Accounts', 'click', 'Submit Fund Addition Request, corpId = ' + Meta.getMetaElement("corpID") + ', compId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', requestParams=('+JSON.stringify(reqObj)+'), timestamp='+new Date().toLocaleString()+', Success');
            FundingHomeDOMEvent.successPopUp();
        }, function(err) {
            initGAEvent('event', 'Funding Accounts', 'click', 'Submit Fund Addition Request, corpId = ' + Meta.getMetaElement("corpID") + ', compId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+', requestParams=('+JSON.stringify(reqObj)+'), timestamp='+new Date().toLocaleString()+', Failed, Reason='+JSON.stringify(err));
            if (err.status === 401) {
                $('#myModalLoaderFund').modal('hide');
                Util.renewAuth();
            } else {
                if(Util.checkGenie()){
                    FundingHomeDOMEvent.FailPopUp(err);
                    $("#myModalOopsFund .heading-sub-text").text("You do not have access to raise fund request");
                } else {
                    FundingHomeDOMEvent.FailPopUp(err);
                }
            }
        });
    },

    transferFunds: function(reqObj){
        let url = Urls.getOMSApiUrl().MOVE_FUNDS_BETWEEN_FAS
                    .replace(":authToken:", Storage.get('authToken'));
        return ServiceConnector.post(reqObj, url);
    },

     getFundingAccount: function(authToken, corpID, pageNo, pageSize) {
        let url = Urls.getOMSApiUrl().GET_FUNDING_ACCOUNTS_REWARD
            .replace(':pageNo:', pageNo)
            .replace(':pageSize:', pageSize)
            .replace(':authToken:', authToken)
            .replace(':corpID:', corpID);
        let inputObj = {};
        let headers = {
            "Content-Type": "application/json"
        };
        return ServiceConnector.get(url, inputObj, headers);
    }
}

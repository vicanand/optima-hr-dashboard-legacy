import Constant from '../common/constants';
import ServiceConnector from "../common/serviceConnector";
import Urls from '../common/urls';
import Util from '../common/util';
import CacheDataStorage from '../common/cacheDataStorage'
import Meta from '../common/metaStorage';
import Storage from '../common/webStorage'

var x,y;
var pgno = 1;
let formatDate = function(transactionDate) {
    if (transactionDate) {
        let dateArr = transactionDate.split(/\.|-/);
        if (transactionDate.match('-')) {
            dateArr = dateArr.reverse();
        }
        transactionDate = Util.dateFormat(dateArr[0], dateArr[1], dateArr[2]);
    }
    return transactionDate;
}

export default {
    getFundsHistory: function (param) {
        let deferred = $.Deferred();
        let url = Urls.getCorpApiUrl().GET_FUNDS_HISTORY.replace(":COMPANY_ID:", $("meta[name='companyID']").attr("content")) + "?pageNo=" + param.pageNumber + "&pageSize=" + param.pageSize;
        let fundHistoryHeader = {
            "token": Storage.get('authToken'),
            "corpID": $("meta[name='corpID']").attr("content")
        };
        ServiceConnector.get(url, fundHistoryHeader).then(function (respData) {
            let records = respData.requests.map(function (item) {
                item["paymentMode"] = (item["paymentMode"]) ? item["paymentMode"] : "NA";
                item.transactionDate = formatDate(item.transactionDate);
                item["paymentMode"] = (item["paymentMode"]) ? item["paymentMode"] : "NA";
                item["transactionRefNumber"] = (item["transactionRefNumber"]) ? item["transactionRefNumber"] : "NA";
                item["transactionDate"] = (item["transactionDate"]) ? item["transactionDate"] : "NA";
                item["amount"] = (item["amount"]) ? Util.formatINR(item["amount"] / 100) : "NA";
                item["status"] = (item["status"]) ? item["status"] : "NA";
                return item;
            });
            let responseJson = {
                totalCount: respData.totalRequests,
                records: records
            }
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            if (error.status === 500) {
                $(".loading-content").hide();
                $(".paging-container").hide();
                $(".table-error-state").show();
            }
            deferred.reject();
        });
        return deferred.promise(); 
    },
    getFundingAccounts: function (param) {
        let deferred = $.Deferred();
        let type = "ZETA_SETTLED";
        let isSodexoPhpSm = $("meta[name='countryVar']").attr("content") == "isSodexoPhpSm" ? true : false;
        // if(isSodexoVn) {
        //     type = "NOT_ZETA_SETTLED"; // if IFI type is PARTNER then we need to use
        // }
        let url = Urls.getOMSApiUrl().GET_FUNDING_ACCOUNTSS
        .replace(":pageNo:", param.pageNumber)
        .replace(":pageSize:", param.pageSize)
        .replace(":corpID:", $("meta[name='corpID']").attr("content"))
        .replace(":type:", type)
        .replace(":authToken:", Storage.get('authToken'));
        let rbac = CacheDataStorage.getItem(Constant.ROLES_CONFIG);
        let passCompanyId = false;
        const country_configs = CacheDataStorage.getItem(Constant.COUNTRY_CONFIGS);
        let getFundingAccountsConfigs;
        if (country_configs) {
            getFundingAccountsConfigs = _.get(country_configs, 'fundsSvc.getFundingAccounts');
            passCompanyId = getFundingAccountsConfigs && getFundingAccountsConfigs.passCompanyId;
        }

        if (((rbac && rbac['getFundingAccounts'] && rbac['getFundingAccounts'].indexOf('own') !== -1) || passCompanyId) && isSodexoPhpSm) {
            url += "&companyID=" + $("meta[name='companyID']").attr("content");
        }
        ServiceConnector.get(url).then(function (respData) {
            if (respData.fundingAccounts) {
                // for (var currAcc = respData.fundingAccounts.length - 1; currAcc >= 0; currAcc--) {
                //     if (respData.fundingAccounts[currAcc].type == 'NOT_ZETA_SETTLED') {
                //         respData.fundingAccounts.splice(currAcc, 1);
                //     }
                // } 

                for (var currIndex = 0; currIndex < respData.fundingAccounts.length; currIndex++) {
                    let companiesFunded = [];
                    // SODEXO_PHILIPPINES_SM Changes -- START
                    respData.fundingAccounts[currIndex].displayCurrencySymbol = Util.currencyNameToSymbol(respData.fundingAccounts[currIndex].currency);
                    // SODEXO_PHILIPPINES_SM Changes -- END

                    for (var currProgram = 0; currProgram < respData.fundingAccounts[currIndex].programsFunded.length; currProgram++) {
                        if (companiesFunded.length > 0) {
                            var newCompany = true;
                            for (var currComp = 0; currComp < companiesFunded.length; currComp++) {
                                if (respData.fundingAccounts[currIndex].programsFunded[currProgram].companyID == companiesFunded[currComp].companyId) {
                                    newCompany = false;
                                    var program = {
                                        "programId": respData.fundingAccounts[currIndex].programsFunded[currProgram].id,
                                        "programType": Constant.CARD_PROGRAM_TYPES[respData.fundingAccounts[currIndex].programsFunded[currProgram].productType],
                                    }
                                    companiesFunded[currComp].programs.push(program);
                                }
                            }
                            if (newCompany) {
                                var company = {
                                    "companyId": respData.fundingAccounts[currIndex].programsFunded[currProgram].companyID,
                                    "companyName": respData.fundingAccounts[currIndex].programsFunded[currProgram].companyName,
                                    "programs": []
                                };
                                var program = {
                                    "programId": respData.fundingAccounts[currIndex].programsFunded[currProgram].id,
                                    "programType": Constant.CARD_PROGRAM_TYPES[respData.fundingAccounts[currIndex].programsFunded[currProgram].productType],
                                }
                                company.programs.push(program);
                                companiesFunded.push(company);
                            }
                        } else {
                            var company = {
                                "companyId": respData.fundingAccounts[currIndex].programsFunded[currProgram].companyID,
                                "companyName": respData.fundingAccounts[currIndex].programsFunded[currProgram].companyName,
                                "programs": []
                            };
                            var program = {
                                "programId": respData.fundingAccounts[currIndex].programsFunded[currProgram].id,
                                "programType": Constant.CARD_PROGRAM_TYPES[respData.fundingAccounts[currIndex].programsFunded[currProgram].productType],
                            }
                            company.programs.push(program);
                            companiesFunded.push(company);
                        }
                        respData.fundingAccounts[currIndex].companiesFunded = companiesFunded;
                    }
                }
            }
            const companyID = parseInt($("meta[name='companyID']").attr("content"));
            let records = respData.fundingAccounts.map(function (item) {
                item["ifiBankName"] = $('#getifiname').data("ifiname");
                item["name"] = item.name;
                item["accountID"] = item.accountID;
                item['programsFunded'] = item.programsFunded;
                item["ifiID"] = item.ifiID;
                item["transferInDetails"] = item.transferInDetails;
                item['balanceInPaise'] = item.balance;
                item['balance'] = Util.formatCurrency(item.balance / 100, item.currency);
                item["ifiID"] = item.ifiID;
                // SODEXO_PHILIPPINES_SM Changes - START
                let disableAddFund = false;
                if (rbac && Object.keys(rbac).length > 1){
                    if(!Util.checkPriviledge(rbac, 'addFunds') && !Util.checkPriviledge(rbac, 'addFundingAccount')){
                        disableAddFund = true;
                    }
                } else if (getFundingAccountsConfigs && _.isFunction(getFundingAccountsConfigs.disableAddFundsByName) && getFundingAccountsConfigs.disableAddFundsByName({name: item.name})) {
                    disableAddFund = true;
                }
                let disableFundTransfer = false;
                
                if (getFundingAccountsConfigs && getFundingAccountsConfigs.hideDropDown && respData.fundingAccounts.length === 1) {
                    disableFundTransfer = true;
                }
                if (rbac && (!rbac['moveFunds_'+companyID] || (rbac['moveFunds_'+companyID] && rbac['moveFunds_'+companyID].indexOf('own') !== -1) && item.companyID !== companyID) || (isSodexoPhpSm && item.name.indexOf('Issuance') != -1)) {
                    disableFundTransfer = true;
                } else if (getFundingAccountsConfigs && _.isFunction(getFundingAccountsConfigs.disableFundTransferByName) && getFundingAccountsConfigs.disableFundTransferByName({name: item.name})) {
                    disableFundTransfer = true;
                }

                // if(isSodexoPhpSm && item.name.indexOf('Issuance') != -1){
                //     disableFundTransfer = false;
                // }
                
                item["displayCurrencySymbol"] = item.displayCurrencySymbol;
                item['disableAddFund'] = disableAddFund;
                item['disableFundTransfer'] = disableFundTransfer;
                item['disableFooterInfo'] = getFundingAccountsConfigs && _.get(getFundingAccountsConfigs, 'disableFooterInfo');
                // SODEXO_PHILIPPINES_SM Changes - END
                return item;
            });
            let responseJson = {
                totalCount: respData.count,
                records: records
            }
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            if (error.status === 500) {
                $(".loading-content").hide();
                $(".paging-container").hide();
                $(".table-error-state").show();
            }
            deferred.reject();
        });
        return deferred.promise();
    },
    getAccountStatement: function (param) {
        let deferred = $.Deferred();
        let today = (new Date()).getTime();
        let before = parseInt(today);
        let after = '';
        let order = 'LATEST_FIRST';
        let url='';
        if(pgno < param.pageNumber){
            before = x;  
             url = Urls.getOMSApiUrl().GET_ACCOUNT_STATEMENT.replace(":COMPANY_ID:", $("meta[name='companyID']").attr("content"))
            .replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
            .replace(":ACCOUNT_ID:", param.accountId)
            .replace(":API_KEY:", Storage.get('authToken')) + "&count=" + param.pageSize + "&before=" + before +  "&order=" + order ;
            console.log(url);
        }
        else if(pgno > param.pageNumber){
            after = y;
             url = Urls.getOMSApiUrl().GET_ACCOUNT_STATEMENT.replace(":COMPANY_ID:", $("meta[name='companyID']").attr("content"))
            .replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
            .replace(":ACCOUNT_ID:", param.accountId)
            .replace(":API_KEY:", Storage.get('authToken')) + "&count=" + param.pageSize + "&after=" + after +  "&order=" + order ;
            console.log(url);
        }
        else{
            pgno = 1;
            before = before;
             url = Urls.getOMSApiUrl().GET_ACCOUNT_STATEMENT.replace(":COMPANY_ID:", $("meta[name='companyID']").attr("content"))
            .replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
            .replace(":ACCOUNT_ID:", param.accountId)
            .replace(":API_KEY:", Storage.get('authToken')) + "&count=" + param.pageSize + "&before=" + before +  "&order=" + order ;
        }
        pgno = param.pageNumber;
        ServiceConnector.get(url, {}).then(function(respData) {
            if(respData.fragments.length > 0){
                if(respData.fragments.length > 9){
                    x = respData.fragments[respData.fragments.length - 1].timestamp;
                    x = parseInt(x);
                    y = respData.fragments[0].timestamp;
                    y = parseInt(y);
                } else {
                    y = respData.fragments[0].timestamp;
                    y = parseInt(y);
                }
            }
            const currencyName = Meta.getMetaElement('currencyName');
            let transactionHistory = respData.fragments.map(function (item) {
                if (item.faTransactionDetails["corpben.fa.transaction-type"] == "InterFAFundMove") {
                    let recordType = item.recordType;
                    if (recordType == "CREDIT") {
                        item.remarks = "Funds transferred from " + item.transactionAttrs["corpben.fa.source-fa-name"];
                    } else if (recordType == "DEBIT") {
                        item.remarks = "Funds transferred to " + item.transactionAttrs["corpben.fa.recipient-fa-name"];
                    }
                }
                if (!item.remarks) {
                    item.remarks = "NA";
                }
                item.amount = Util.formatCurrency(item.amount / 100, currencyName);
                item.newBalance = Util.formatCurrency(item.newBalance / 100, currencyName);
                item.timestamp = Util.timeStampToDate(item.timestamp);
                return item;
            });
            transactionHistory = transactionHistory || [];
            let responseJson = {
                totalCount: respData.response.returnedTransactionsCount ==10 ? 10000 : respData.response.returnedTransactionsCount,
                records: transactionHistory
            };

            //  for(let currIndex=0; currIndex<responseJson.records.length;currIndex++){
            //      responseJson.records[currIndex].amount = Util.formatINR(responseJson.records[currIndex].amount);
            //      let currDate = new Date(responseJson.records[currIndex].timestamp);
            //     responseJson.records[currIndex].timestamp = Util.dateFormat(currDate.getDate(),currDate.getMonth()+1,currDate.getFullYear());
            //  }
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject();
        });
        return deferred.promise();
    },
    getAccountStatementV2: function (param) {
        const deferred = $.Deferred();
        const order = 'LATEST_FIRST';
        let url=''; let start = null; let end = null;
        url = Urls.getOMSApiUrl().GET_ACCOUNT_STATEMENT.replace(":COMPANY_ID:", $("meta[name='companyID']").attr("content"))
            .replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
            .replace(":ACCOUNT_ID:", param.accountId)
            .replace(":API_KEY:", Storage.get('authToken')) + "&count=" + param.pageSize +  "&order=" + order;
        
        const tranferPeriodAccountElem = $('#tranferPeriodAccount');
        if (tranferPeriodAccountElem && tranferPeriodAccountElem.val()) {
            const x = $('#tranferPeriodAccount').val().split(' - ');
            start = Util.dateToepochTime(x[0], '-', 'after').toString();
            end = Util.dateToepochTime(x[1], '-', 'before').toString();
            url += "&before=" + end + "&after=" + start;
        }
        let inputJson = {
            attributes: {
                skipTransactionCount: "false"
            },
            pageNumber: param.pageNumber
        };
        
        ServiceConnector.post(inputJson, url, {}).then(function (respData) {
            const currencyName = Meta.getMetaElement('currencyName');
            let transactionHistory = respData.fragments.map(function (item) {
                if (item.faTransactionDetails["corpben.fa.transaction-type"] == "InterFAFundMove") {
                    let recordType = item.recordType;
                    if (recordType == "CREDIT") {
                        item.remarks = "Funds transferred from " + item.transactionAttrs["corpben.fa.source-fa-name"];
                    } else if (recordType == "DEBIT") {
                        item.remarks = "Funds transferred to " + item.transactionAttrs["corpben.fa.recipient-fa-name"];
                    }
                }
                if (!item.remarks) {
                    item.remarks = "NA";
                }
                item.amount = Util.formatCurrency(item.amount / 100, currencyName);
                item.newBalance = Util.formatCurrency(item.newBalance / 100, currencyName);
                item.timestamp = Util.timeStampToDate(item.timestamp);
                return item;
            });
            transactionHistory = transactionHistory || [];
            let responseJson = {
                totalCount: respData.response.returnedTransactionsCount || 10,
                records: transactionHistory
            };
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject();
        });
        return deferred.promise();
    },
    getLegacyAccountStatement: function(param) {
        let deferred = $.Deferred();
        let url = Urls.getCorpApiUrl().GET_LEGACY_ACCOUNT_STATEMENT.replace(":COMPANY_ID:", $("meta[name='companyID']").attr("content")) + "?pageNo=" + param.pageNumber + "&pageSize=" + param.pageSize;
        let accountStmtHeader = {
            "token": Storage.get('authToken'),
            "corpID": $("meta[name='corpID']").attr("content")
        };
        console.log('url',url);
        ServiceConnector.get(url, accountStmtHeader).then(function (respData) {
            console.log('respData',respData)
            let responseJson = {
                totalCount: respData.totalTransactions,
                records: respData.transactions
            };
            for (let currIndex = 0; currIndex < responseJson.records.length; currIndex++) {
                responseJson.records[currIndex].amount = Util.formatINR(responseJson.records[currIndex].amount);
                let dateArrSplitVal = responseJson.records[currIndex].createdOn.split('T');
                let dateArr = dateArrSplitVal[0].split('-');
                responseJson.records[currIndex].createdOn = Util.dateFormat(dateArr[2], dateArr[1], dateArr[0]);
            }
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            if (error.status === 500) {
                $(".loading-content").hide();
                $(".paging-container").hide();
                $(".table-error-state").show();
            }
            deferred.reject();
        });
        return deferred.promise();
    },
    getEachFundHistory: function (param) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_EACH_FUND_ACCOUNT_DETAIL
            .replace(":ACCOUNT_ID:", param.accountId) + "&pageNo=" + (Number(param.pageNumber) - 1) + "&pageSize=" + param.pageSize;
        // let fundHistoryHeader = {
        //     "token": Storage.get('authToken'),
        //     "corpID": $("meta[name='corpID']").attr("content")
        // };
        let inputJson = {
            "token": Storage.get('authToken'),
            "corpId": $("meta[name='corpID']").attr("content")
        };
        // SODEXO_PHILIPPINES_SM Changes - START
        let start = null; let end = null;
        const tranferPeriodFundElem = $('#tranferPeriodFund');
        if (tranferPeriodFundElem && tranferPeriodFundElem.val()) {
            const x = $('#tranferPeriodFund').val().split(' - ');
            start = Util.dateToepochTime(x[0], '-', 'after').toString();
            end = Util.dateToepochTime(x[1], '-', 'before').toString();
            if (start && end) {
                const current_date = new Date();
                current_date.setHours(0, 0, 0, 0);
                const selected_date = new Date(parseFloat(end));
                selected_date.setHours(0, 0, 0, 0);
                if (current_date.getTime() === selected_date.getTime()) {
                    end = new Date().getTime().toString();
                }
                inputJson.filterBy = {
                    "fromTimestamp" : start,
                    "toTimestamp" : end
                }
            }
        }
        // SODEXO_PHILIPPINES_SM Changes - END
        ServiceConnector.post(inputJson, url, {}).then(function (respData) {
            let records = respData.entries.map(function (item) {
                item["paymentMode"] = (item["transferMode"]) ? item["transferMode"] : "NA";
                item["transactionRefNumber"] = (item["referenceNumber"]) ? item["referenceNumber"] : "NA";
                item.transactionDate = formatDate(item.paymentDetails.transactionDate);
                item["transactionDate"] = (item.transactionDate) ? item.transactionDate : "NA";
                item["amount"] = (item.amount) ? Util.formatCurrency(item.amountInPaise/100, item.amountInBank.currency) : Util.formatCurrency(0, item.amountInBank.currency);
                // item["amount"] = (item.amount) ? Util.formatINR(item.amount) : "NA";
                item["status"] = (item["status"]) ? item["status"] : "NA";
                return item;
            });
            let responseJson = {
                totalCount: respData.totalEntries,
                records: records
            }
            console.log("fund history",responseJson);
            deferred.resolve(responseJson);
        }, function (error) {

            if (error.status === 401) {
                Util.renewAuth();
            }

            deferred.reject();
        });
        return deferred.promise();
    },

    downloadReportPhp:function(name,accountID,startDate,endDate){
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let reportID = name == 'fund' ? 'SM_FUNDING_ADDITION_STATEMENT' : 'SM_FUNDING_ACCOUNT_STATEMENT';

        let url = Urls.getOMSApiUrl().DOWNLOAD_REPORT;
        let requestPayload = {
            "token":authToken,
            "corpID": corpID,
            "reportParameters": {
                "reportID": reportID,
                "reportContentType": "CSV",
                "parameters": {
                    "FUNDING_ACCOUNT_IDS":[accountID],
                    "START_DATE": [startDate],
                    "END_DATE":[endDate],
                    "CORP_ID": [corpID]
                }
            }
        }
        let deferred = $.Deferred();
        ServiceConnector.post(requestPayload, url).then(function (respData) {
            deferred.resolve(respData);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject(error);
        });
        return deferred.promise();
    }
}
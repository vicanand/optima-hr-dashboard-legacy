import DomEventHandler from '../../common/domEventHandler';
import FeedbackService from './feedbackSvc';
import BarGraph from '../../../template/section/feedback/bar-graph.ejs';
import BadBits from '../../../template/section/feedback/bad-bits.ejs';
import GoodBits from '../../../template/section/feedback/good-bits.ejs';
import CustomerComments from '../../../template/section/feedback/comment-feedback.ejs';
import FeedbackModal from '../../controller/feedback/feedbackModal';
import CommentDomEvents from '../../dom-events/feedback/customer-comment';
import Storage from '../../common/webStorage';
import FeedbackController from '../../controller/feedback/feedbackCtrl';


let date_render_all = function(date){
    var authToken = Storage.get('authToken');
    var corpID = $("meta[name='corpID']").attr("content");
    var businessID = $("meta[name='companyID']").attr("content");
    var stores = FeedbackModal.get_stores();
    console.log("from_Date",FeedbackModal.get_from_date());
    console.log("to_Date",FeedbackModal.get_to_date());
    let params = {
        corporateID: corpID,
       // businessID: businessID,
        fromDate: date.fromDate || moment().subtract(30, 'days').format('YYYY-MM-DD'),
        toDate: date.toDate || moment().format('YYYY-MM-DD'),
        stores:stores
    };
    FeedbackService.getCustomerRatings(authToken,params).then(function(ratings){
        var ratings = filterratings(ratings);
        Storage.set('ratings',JSON.stringify(ratings));
        DomEventHandler.renderMyTemplate('rating-graph-container',BarGraph,{ratings:ratings});
        FeedbackController.bar_graph();
        FeedbackService.getCustomerFeedback(authToken,params).then(function(feedback){
             var feedback = feedback;
             DomEventHandler.renderMyTemplate('good-bits-container',GoodBits,feedback);
             DomEventHandler.renderMyTemplate('bad-bits-container',BadBits,feedback);
             params.rating = 5;
             params.pageSize =1;
             FeedbackService.getCustomerComments(authToken,params).then(function(comments){
                 var comments = comments;
                 DomEventHandler.renderMyTemplate('customer-comment-container',CustomerComments,comments);
                 var five = 5;
                 var defaultElement = $(".customer-rating[data-star='" + five +"']").css({"border-bottom":"2px solid #6e4db7","color":"#6e4db7"});
                 DomEventHandler.bindClassEvent('customer-rating','click',CommentDomEvents.starSelected);
             });

        });
         
    });
}


var filterratings = function(params){
    var ratings = params;
    var filteredratings={};
    filteredratings.ratingPercentages=[];
    filteredratings.totalNoOfUsers = params.totalNoOfUsers;
    filteredratings.averageRating = parseInt(params.averageRating).toFixed(1); 
    var starToShow = parseInt(params.averageRating);
    console.log('starToShow: ', starToShow);
    var normalStar = 5 - Math.ceil(starToShow);
    console.log('normalStar: ', normalStar);
    var goldenStar = Math.ceil(starToShow);
    console.log('goldenStar: ', goldenStar);
    var stars=[];
    filteredratings.normalStar = normalStar;
    filteredratings.goldenStar = goldenStar;
    for(var i = 0;i<= ratings.ratingPercentages.length-1;i++){
      stars[ratings.ratingPercentages[i].field]=1;
      stars[ratings.ratingPercentages[i].field]={};
      stars[ratings.ratingPercentages[i].field].percentage = ratings.ratingPercentages[i].percentage;
     
    }
    
    
    
    for(var j=0;j<=stars.length-1;j++){
      if(stars[j] == undefined){
        filteredratings.ratingPercentages[j]={};
        filteredratings.ratingPercentages[j].field=j;
        filteredratings.ratingPercentages[j].percentage=0;
      }else{
        filteredratings.ratingPercentages[j]={};
        filteredratings.ratingPercentages[j].field=j;
        filteredratings.ratingPercentages[j].percentage=stars[j].percentage;
      }
    }
    console.log('filteredratings: ', filteredratings);
    return filteredratings;
    
}

export default {
    date_render_all:date_render_all
}
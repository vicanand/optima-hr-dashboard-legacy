import DomEventHandler from '../../common/domEventHandler';
import FeedbackService from './feedbackSvc';
import BarGraph from '../../../template/section/feedback/bar-graph.ejs';
import BadBits from '../../../template/section/feedback/bad-bits.ejs';
import GoodBits from '../../../template/section/feedback/good-bits.ejs';
import CustomerComments from '../../../template/section/feedback/comment-data-feedback.ejs';
import FeedbackModal from '../../controller/feedback/feedbackModal';
import CommentDomEvents from '../../dom-events/feedback/customer-comment';
import Storage from '../../common/webStorage';
import FeedbackController from '../../controller/feedback/feedbackCtrl';


let render_all = function(store){
    var authToken = Storage.get('authToken');
    var corpID = $("meta[name='corpID']").attr("content");
    var businessID = $("meta[name='companyID']").attr("content");
    var stores = store;
    FeedbackModal.set_processing(false);
    FeedbackModal.set_scroll_flag(true);
    console.log("from_Date",FeedbackModal.get_from_date());
    console.log("to_Date",FeedbackModal.get_to_date());
    let params = {
        corporateID: corpID,
       // businessID: businessID,
        fromDate: FeedbackModal.get_from_date() || moment().subtract(30, 'days').format('YYYY-MM-DD'),
        toDate: FeedbackModal.get_to_date() || moment().format('YYYY-MM-DD HH:mm:ss'),
        stores:stores
    };
    $('#good-bits-loading').css('display','block');
    $('#bad-bits-loading').css('display','block');
    render_data(authToken,params);
};
let date_render_all = function(date){
        var authToken = Storage.get('authToken');
        var corpID = $("meta[name='corpID']").attr("content");
        var businessID = $("meta[name='companyID']").attr("content");
        var stores = FeedbackModal.get_stores();
        FeedbackModal.set_processing(false);
        FeedbackModal.set_scroll_flag(true);
        console.log("from_Date",FeedbackModal.get_from_date());
        console.log("to_Date",FeedbackModal.get_to_date());
        let params = {
            corporateID: corpID,
           // businessID: businessID,
            fromDate: date.fromDate || moment().subtract(30, 'days').format('YYYY-MM-DD'),
            toDate: date.toDate || moment().format('YYYY-MM-DD HH:mm:ss'),
            stores:stores
        };
        $('#good-bits-loading').css('display','block');
        $('#bad-bits-loading').css('display','block');
        render_data(authToken,params);
};

let render_data = function(authToken,params){
    FeedbackService.getCustomerRatings(authToken,params).then(function(ratings){
        var ratings = filterratings(ratings);
        Storage.set('ratings',JSON.stringify(ratings));
        DomEventHandler.renderMyTemplate('rating-graph-container',BarGraph,{ratings:ratings});
        FeedbackController.bar_graph();
        FeedbackService.getCustomerFeedback(authToken,params).then(function(feedback){
             var feedback = feedback;
             $('#good-bits-loading').css('display','none');
             DomEventHandler.renderMyTemplate('good-bits-container',GoodBits,{feedback:feedback});
             $('.good-review-number').change(function(){
                var percentage = $(this).html();
                var element = $(this).parent().next().find('.loader-outer .loader-inner');
                element.css({"width":percentage+"%"});
              });
          
            $('.good-review-number').change();
            $('#bad-bits-loading').css('display','block');
             DomEventHandler.renderMyTemplate('bad-bits-container',BadBits,{feedback:feedback});
             $('.bad-review-number').change(function(){
                var percentage = $(this).html();
                var element = $(this).parent().next().find('.loader-outer .loader-inner');
                element.css({"width":percentage+"%"});
            });
            
            $('.bad-review-number').change();
             params.rating = 5;
             params.pageSize =10;
             FeedbackService.getCustomerComments(authToken,params).then(function(comments){
                 var comments = comments;
                 console.log('comments: ', comments);
                 if (comments.comments.length > 0) {
                    var new_object = JSON.stringify(comments.comments);
                    var new_comments = JSON.parse(new_object);
                    var toDate = new_comments[new_comments.length - 1].timeStamp;
                    console.log('toDate: ', toDate);
                    FeedbackModal.set_last_modified_date(toDate);
                 } else {
                    FeedbackModal.set_scroll_flag(false);
                 }
                 formatTime(comments.comments);
                 DomEventHandler.renderMyTemplate('comment-details-container',CustomerComments,comments);
                 var five = 5;
                 $('.customer-rating').css({"border-bottom":"none","color":"#000000"});
                 var defaultElement = $(".customer-rating[data-star='" + five +"']").css({"border-bottom":"2px solid #6e4db7","color":"#6e4db7"});
                 
             });

        });
         
    });
};


var filterratings = function(params){
    var ratings = params;
    var filteredratings={};
    filteredratings.ratingPercentages=[];
    filteredratings.totalNoOfUsers = params.totalNoOfUsers;
    filteredratings.averageRating = parseFloat(params.averageRating).toFixed(1); 
    console.log('averageRating: ', filteredratings.averageRating);
    var starToShow = parseFloat(params.averageRating).toFixed(1);   
    console.log('starToShow: ', starToShow);
    
    var normalStar = 5 - Math.round(starToShow);
    var goldenStar = Math.round(starToShow);
    var stars=[];
    filteredratings.normalStar = normalStar;
    filteredratings.goldenStar = goldenStar;
    if(ratings.ratingPercentages){
        for(var i = 0;i<= ratings.ratingPercentages.length-1;i++){
            stars[ratings.ratingPercentages[i].field]=1;
            stars[ratings.ratingPercentages[i].field]={};
            stars[ratings.ratingPercentages[i].field].percentage = ratings.ratingPercentages[i].percentage;
           
          }
    }else{
        ratings = {};
        ratings.ratingPercentages =[{field:0},{field:1},{field:2},{field:3},{field:4},{field:5}];
        for(var i = 0;i<= 5;i++){
            
            stars[ratings.ratingPercentages[i].field]=1;
            stars[ratings.ratingPercentages[i].field]={};
            stars[ratings.ratingPercentages[i].field].percentage = 0;
           
          }
    }
    
    for(var j=0;j<=stars.length-1;j++){
      if(stars[j] == undefined){
        filteredratings.ratingPercentages[j]={};
        filteredratings.ratingPercentages[j].field=j;
        filteredratings.ratingPercentages[j].percentage=0;
      }else{
        filteredratings.ratingPercentages[j]={};
        filteredratings.ratingPercentages[j].field=j;
        filteredratings.ratingPercentages[j].percentage=stars[j].percentage;
      }
    }
    console.log('filteredratings: ', filteredratings);
    return filteredratings;
    
}
var formatTime = function(time){
    
    time.forEach(function(item){
        item.timeStamp = moment(item.timeStamp).format('YYYY-MM-DD,hh:mm:ss a');
       
    });
    
}

export default {
    render_all:render_all,
    date_render_all:date_render_all
}
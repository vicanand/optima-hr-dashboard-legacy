import URL from '../../common/urls';
import Storage from '../../common/webStorage';
import ServiceConnector from '../../common/serviceConnector';


var getCustomerRatings = function(authToken,params){
    var url = URL.getOMSApiUrl().GET_CUSTOMER_RATINGS;
    // .replace(':authToken:', authToken);
    var inputObj = params;
    var headers = {
        "Content-Type": "application/json",
        "X-Zeta-AuthToken":authToken
    };
    return ServiceConnector.post(inputObj, url, headers);
}

var getCustomerFeedback = function(authToken,params){
    var url = URL.getOMSApiUrl().GET_CUSTOMER_FEEDBACK;
    // .replace(':authToken:', authToken);
    var inputObj = params;
    var headers = {
        "Content-Type": "application/json",
        "X-Zeta-AuthToken":authToken
    };
    return ServiceConnector.post(inputObj, url, headers);
}

var getCustomerComments = function(authToken,params){
    var url = URL.getOMSApiUrl().GET_CUSTOMER_COMMENTS;
    // .replace(':authToken:', authToken);
    var inputObj = params;
    var headers = {
        "Content-Type": "application/json",
        "X-Zeta-AuthToken":authToken
    };
    return ServiceConnector.post(inputObj, url, headers);
}






module.exports={
    getCustomerRatings:getCustomerRatings,
    getCustomerFeedback:getCustomerFeedback,
    getCustomerComments:getCustomerComments
}
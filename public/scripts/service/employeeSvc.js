import ServiceConnector from "../common/serviceConnector";
import Urls from '../common/urls';
import Util from '../common/util';
import Constants from '../common/constants';
import Storage from '../common/webStorage';

let getBenefitPrograms = function() {
    let companyIDs = $("meta[name='companyIDs']").attr("content").split(',');

    let promises = [];
    for (var i = 0; i < companyIDs.length; i++) {
        let url = Urls.getOMSApiUrl().GET_BENEFIT_PROGRAMS
            .replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
            .replace(":API_KEY:", Storage.get('authToken'))
            .replace(":COMPANY_ID:", companyIDs[i]);
        promises.push(ServiceConnector.get(url, {}));
    }

    return $.when.apply($, promises)
        .then(function() {
            let response = [];
            for (var i = 0; i < arguments.length; i++) {
                response = response.concat(arguments[i].programs);
            }
            return $.Deferred().resolve(response);
        });
}

let getBenefitProgramForCompany = function() {
    let companyID = $("meta[name='companyID']").attr("content");

    let promises = [];
    let url = Urls.getOMSApiUrl().GET_BENEFIT_PROGRAMS
            .replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
            .replace(":API_KEY:", Storage.get('authToken'))
            .replace(":COMPANY_ID:", companyID);
    promises.push(ServiceConnector.get(url, {}));

    return $.when.apply($, promises)
        .then(function() {
            let response = [];
            for (var i = 0; i < arguments.length; i++) {
                response = response.concat(arguments[i].programs);
            }
            return $.Deferred().resolve(response);
        });
}

let fillBenefitPrograms = function(respData, benefitPrograms) {
    
    respData.employees = respData.employees.map(function(item) {
        if (item.benefits) {
            item.benefits = item.benefits.map(function(benItem) {
                for (var index in benefitPrograms) {
                    if (benefitPrograms[index].programID === benItem.programID) {
                        benItem.cardProgram = benefitPrograms[index].cardProgramObject;
                        return benItem;
                    }
                }
                benItem.cardProgram = {
                    cardDesign: {
                        bg: ''
                    },
                    productType: ''
                }
                return benItem;
            });
            return item;
        }
        else{
            item.benefits = [];
            return item;
        }
    });
    return respData;
}

let _processEmployeeData = function(deferred,benefitPrograms,params){
        let url = Urls.getOMSApiUrl().GET_EMPLOYEES.replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
            .replace(":API_KEY:", Storage.get('authToken')) + "&pageNo=" + (Number(params.pageNumber) - 1) + "&pageSize=" + params.pageSize + "&searchTerm=" + params.searchQuery + "&companyID=" + params.filterValue + "&programID=" + params.secondaryFilterValue;
             
        ServiceConnector.get(url, {}).then(function(respData) {
            
            respData = fillBenefitPrograms(respData, benefitPrograms);

           
            let records = respData.employees.map(function(item) {
                
                if (item.benefits) {
                    let cardNameArray = item.benefits.filter(function(item) {
                        if (item.cardProgram.cardName) {
                            return item.cardProgram.cardName;
                        }
                    })
                    item.cards = {};
                    cardNameArray = cardNameArray.map(function(item) {
                        return item.cardProgram.cardName;
                    });
                    item.cards.otherCardlength = cardNameArray.length - 2;
                    item.cards.otherCards = cardNameArray.splice(2);
                }

                if (!item.name) {
                    item.name = "NA";
                }

                if (item.profile && item.profile.emailList && item.email) {
                    
                    for (var i = 0; i < item.profile.emailList.length; i++) {                       
                        if (item.profile.emailList[i].email === item.email) {
                            item.isEmailVerified = item.profile.emailList[i].isVerified;
                        }
                    }
                }
                item.isEmailVerified = item.isEmailVerified || false;

                let emailArray = item.email.split(',');
               
                item.email = {};
                item.email.first = emailArray[0];
                item.email.plusCount = emailArray.length - 1;
                item.email.otherEmails = emailArray.splice(1);
                if (item.profile && item.profile.mobileNumber) {
                    item.phoneNumber = item.profile.mobileNumber;
                } else if (item.phoneNumber == "") {
                    item.phoneNumber = "N/A"
                }

                if (item.profile && item.profile.profilePicURL) {
                    item.profilePicURL = item.profile.profilePicURL;
                } else {
                    item.profilePicURL = "https://card-program-files.s3.amazonaws.com/hrDashboard/images/profileDp.png";
                }

                let businessArrayName = [];
                if (item.benefits) {
                    item.benefits.forEach(function(benItem) {

                        if (benItem.cardProgram && benItem.cardProgram.businessName) {
                            businessArrayName.push(benItem.cardProgram.businessName);
                        }
                    });
                }

                item.businessName = _.uniq(businessArrayName).join(",");

                if (item.businessName == "") {
                    item.businessName = "NA";
                }

                item.supercardstatus = false;

                for (var key in item.superCards) {
                    if (item.superCards[key].state == "ENABLED") {
                        item.supercardstatus = true;
                        break;
                    }
                }
                if (item.profile && item.profile.kycDone) {
                    item.kycdone = item.profile.kycDone;
                } else {
                    item.kycdone = false;
                }
                return item;
            });

            let responseJson = {
                totalCount: respData.numberOfTotalResults,
                records: records
            }
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject(error);
        });
}

let getEmployees = function(params) {
    let deferred = $.Deferred();
    if(params.filterValue){
        getBenefitProgramForCompany().then(function(benefitPrograms) {
            _processEmployeeData(deferred,benefitPrograms,params);
        });
    }
    else{
        getBenefitPrograms().then(function(benefitPrograms) {
            _processEmployeeData(deferred,benefitPrograms,params);
        });
    }
    
    
    return deferred.promise();
};

let mapper = function(params) {
    let arr = [];
    params.employees.map((val) => {
        arr.push(val.profileInfo.zetaUserID);
    })
    return arr;
}

let toggleButtonDetails = function(params) {
    let request = {
        'userIDs': params
    };
    let url = Urls.getAvailFinanceApiUrl().GET_EMPLOYEESV2_STATUS
              .replace(":authToken:", Storage.get('authToken'));
    return ServiceConnector.post(request, url, {});
};

let updateEmployeeData = function(deferred, respData, params, res) {
    let cardPrograms = [];
    let set1 = new Set();
    let benefits = [];
    let cardsTooltip = [];
    let obj;
    let item = {};
    let responseJson = {};
    let isLBSNAA = $('#activateCloseCards')[0].value === 'true';
        item = respData;
        if(item.employees.length > 0 ){
            for(let i = 0; i< item.employees.length; i++){
                item.employees[i].benefits = [];
                item.employees[i].cardsTooltip = [];
                item.employees[i].isLBSNAA = isLBSNAA;
                item.employees[i].highlightCard = null;
                item.employees[i].profileURL = "/beneficiaries/" + item.employees[i].employeeID + '?companyID=' + item.employees[i].companyID;
                item.employees[i].employeeSetupEnabled = params.employeeSetupEnabled;
                if(res) {
                    obj = res.find(val => {
                        return val.userID == item.employees[i].profileInfo.zetaUserID
                    });
                }
                if(obj) {
                    item.employees[i].groupStatus = obj.groupStatus;
                }
                else {
                    item.employees[i].groupStatus  = 'NOT_FOUND';
                }
                if(item.employees[i].cards){
                    for(let j = 0; j< item.employees[i].cards.length; j++){
                        if(item.employees[i].cards[j].status === 'ACTIVE'){
                            set1.add(item.employees[i].cards[j].programID);
                        }
                    }
                }
            }
        }
        if(set1.size > 0){
            for( let a of set1){
                cardPrograms.push(a);
            }
        }
        if(cardPrograms.length){   // fetching active card program details.
            let input= {
                'corpID': $("meta[name='corpID']").attr("content"),
                'programIDs':cardPrograms,
                'detailsRequired':['cardDesign']
            };
            let programurl = Urls.getOMSApiUrl().GET_PROGRAMDETAILSV2
            .replace(":API_KEY:", Storage.get('authToken'));
            ServiceConnector.post(input, programurl, {}).then(function(cardData) {
                let cardProgramData = cardData;
                if(item.employees.length > 0 ){
                    for(let i = 0; i< item.employees.length; i++){
                      //  item.employees[i].benefits = [];
                        if(item.employees[i].cards){
                                for(let j = 0; j< item.employees[i].cards.length; j++){
                                    if(item.employees[i].cards[j].status == 'ACTIVE'){
                                    let temp = item.employees[i].cards[j].programID;
                                    item.employees[i].benefits.push(cardProgramData.programs[temp]);
                                    item.employees[i].cardsTooltip.push(cardProgramData.programs[temp].cardDesign.cardName);
                                    item.employees[i].highlightCard = item.employees[i].cards[j];
                                }
                            }                          
                        }    
                    }
                }
                 responseJson = {
                    totalCount: item.numberOfTotalResults,
                    records: item.employees
                }
                deferred.resolve(responseJson);
            });
        } else {
            responseJson = {
                totalCount: item.numberOfTotalResults,
                records: item.employees
            }
            deferred.resolve(responseJson);
        }
}

let addUserToGroup = function(zetaUserID, userName){
    let url = Urls.getAvailFinanceApiUrl().ADD_USER_STATUS
              .replace(':USER_ID:', zetaUserID)
              .replace(':USER_NAME:', userName)
              .replace(":authToken:", Storage.get('authToken'));
    let headers = {
        "Content-Type": "application/json",
    };
    return ServiceConnector.get(url, headers);
};

let removeUserFromGroup = function(zetaUserID, authToken) {
    let url = Urls.getAvailFinanceApiUrl().REMOVE_USER_STATUS
              .replace(':USER_ID:', zetaUserID)
              .replace(":authToken:", Storage.get('authToken'));
    let headers = {
        "Content-Type": "application/json",
    };
    return ServiceConnector.get(url, headers);
}

let getBenefitEmployeeesV2 = function(deferred, params){
    let url = Urls.getOMSApiUrl().GET_EMPLOYEESV2.replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
        .replace(":API_KEY:", Storage.get('authToken')) + "&pageNo=" + (Number(params.pageNumber) - 1) + "&pageSize=" + params.pageSize + "&searchTerm=" + params.searchQuery + "&companyID=" + params.filterValue + "&programID=" + params.secondaryFilterValue;
         
    ServiceConnector.get(url, {}).then(function(respData) {
        
        if(params.employeeSetupEnabled) {
            toggleButtonDetails(mapper(respData)).then((res) => {
                updateEmployeeData(deferred, respData, params, res);
            }, (err)=> {
                updateEmployeeData(deferred, respData, params);
            })
        }
        else {
            updateEmployeeData(deferred, respData, params);
        }
   
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        return deferred.reject(error);
    });
    return deferred.promise();
}

let getEmployeesV2 = function(params) {
    let deferred = $.Deferred();
        getBenefitEmployeeesV2(deferred,params);

    return deferred.promise();
};

let getIndividualEmployeeDetails = function() {
    let deferred = $.Deferred();
    let empId = window.location.pathname.split('/').pop();
    let url = Urls.getOMSApiUrl().GET_INDIVIDUAL_EMPLOYEE
        .replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
        .replace(":EMPLOYEE_ID:", empId)
        .replace(":API_KEY:", Storage.get('authToken'));

    ServiceConnector.get(url, {}).then(function(respData) {
        let responseJson = {
            records: respData.payouts
        };
        deferred.resolve(responseJson);
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        deferred.reject(error);
    });
    return deferred.promise();
}

let getIndividualEmployeePayout = function(params) {
    let deferred = $.Deferred(),
        urlPath = window.location.href,
        empId = window.location.pathname.split('/').pop(),
        corpID = $("meta[name='corpID']").attr("content"),
        companyID = Util.getParameterByName("companyID", urlPath),
        programId = params.programId,
        compareDates = params.compareDates;
    let url = Urls.getOMSApiUrl().GET_EMPLOYEE_PAYOUTS
        .replace(":CORPID:", $("meta[name='corpID']").attr("content"))
        .replace(":EMPLOYEEID:", empId)
        .replace(":COMPANYID:", companyID)
        .replace(":PROGRAMID:", programId)
        .replace(":API_KEY:", Storage.get('authToken')) + "&pageNo=" + (Number(params.pageNumber) - 1) + "&pageSize=" + params.pageSize;

    ServiceConnector.get(url, {}).then(function(respData) {
        let totalPayouts = 0;
        for (var key in respData.payoutCountByStatus) {
            totalPayouts += respData.payoutCountByStatus[key];
        }
        let records = respData.payouts.map(function(item) {
            item.scheduledOn = Util.timeStampToDate(item.scheduledOn);
            item.amount = Util.formatINR(item.amount / 100);
            item["primaryStatusClass"] = Constants.PAYOUT_STATUS_LABELS_CLASSES[item.payoutStatus];
            item["primaryStatus"] = Constants.PAYOUT_STATUS_LABELS[item.payoutStatus];
            item.compareDates = compareDates;
            return item;
        });


        let responseJson = {
            records: records,
            totalCount: totalPayouts
        };
        deferred.resolve(responseJson);
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        deferred.reject(error);
    });
    return deferred.promise();
}
// v2 api integration of benefit employees for a particular card program
let getProgramBenifitEmployeesV2 = function(params) {
    let deferred = $.Deferred();                                   
    params["secondaryFilterValue"] = params.transferId;
    params["filterValue"] = $("meta[name='companyID']").attr("content");  
    let item = {};
    let url = Urls.getOMSApiUrl().GET_EMPLOYEESV2.replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
        .replace(":API_KEY:", Storage.get('authToken')) + "&pageNo=" + (Number(params.pageNumber) - 1) + "&pageSize=" + params.pageSize + "&searchTerm=" + params.searchQuery + "&companyID=" + params.filterValue + "&programID=" + params.secondaryFilterValue;         
    ServiceConnector.get(url, {}).then(function(respData) {
        item = respData;
        if(item.employees.length > 0 ){
            for(let i = 0; i< item.employees.length; i++){
                item.employees[i].activeCardCount = 0;
                item.employees[i].closedCardCount = 0;
                if(item.employees[i].cards){
                    for(let j = 0; j< item.employees[i].cards.length; j++){
                     if(item.employees[i].cards[j].status === 'ACTIVE'){
                        item.employees[i].activeCardCount++;
                     }
                     if(item.employees[i].cards[j].status === 'CLOSED'){
                        item.employees[i].closedCardCount++;
                     }
                    }
                }
                item.employees[i].isProgramManagerRead = params.isProgramManagerRead
                item.employees[i].isProgramManagerWrite = params.isProgramManagerWrite
            }
        }
        let responseObject = {
            totalCount: item.numberOfTotalResults,
            records: item.employees
        };
       
            Storage.setCollection('employeeDetails',item.employees);
      
        deferred.resolve(responseObject);
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        deferred.reject(error);
    });
    return deferred.promise();
};
// end
let getBenifitEmployees = function(param) {
    let deferred = $.Deferred();                                   
    param["secondaryFilterValue"] = param.transferId;
    param["filterValue"] = $("meta[name='companyID']").attr("content");    


    getEmployees(param).then(function(respData) {
        let recordsArray = respData.records.map(function(item) {
            let companyID = '',
                productType = '';
            item.benefits = _.find(item["benefits"], function(o) {
                return o.programID == param.transferId;
            });

            if (item.benefits && item.benefits.totalPayoutAmount && item.benefits.totalPayoutAmount.amount) {
                item.transfered = item.benefits.totalPayoutAmount.amount;
            } else {
                item.transfered = 0;
            }

            if (item.benefits && item.benefits.totalSpentAmount && item.benefits.totalSpentAmount.amount) {
                item.spentAmount = item.benefits.totalSpentAmount.amount;
            } else {
                item.spentAmount = 0;
            }

            if (item.benefits && item.benefits.reimbursement && item.benefits.reimbursement.totalApprovedAmount && item.benefits.reimbursement.totalApprovedAmount.amount) {
                item.reimbursed = item.benefits.reimbursement.totalApprovedAmount.amount;
            } else {
                item.reimbursed = 0;
            }

            if (item.benefits && item.benefits.reimbursement && item.benefits.reimbursement && item.benefits.reimbursement.totalPaidOutAmount && item.benefits.reimbursement.totalPaidOutAmount.amount) {
                item.totalPaidOutAmount = item.benefits.reimbursement.totalPaidOutAmount.amount;
            } else {
                item.totalPaidOutAmount = 0;
            }

            if (item.benefits && item.benefits.cardState) {
                item.cardState = Constants.EMPLOYEE_STATUS[item.benefits.cardState];
            } else {
                item.cardState = "no active cards";
            }

            if (item.benefits && item.benefits.currentBalance) {
                item.currentBalance = item.benefits.currentBalance.amount;
            } else {
                item.currentBalance = 0;
            }
            if (item.benefits && item.benefits.companyID) {
                companyID = item.benefits.companyID;
            }
            if (item.benefits && item.benefits.cardProgram && item.benefits.cardProgram.productType) {
                if(item.benefits.cardProgram.productType == 'CarMaintenanceAndDriverSalary'){
                    productType = 'car_maintenance_and_driver_salary';
                } 
                else if(item.benefits.cardProgram.productType == 'LTA'){
                    productType = item.benefits.cardProgram.productType;
                }
                else if(item.benefits.cardProgram.productType == 'DriverSalary'){
                    productType = 'driver_salary';
                }
                else{
                    productType = item.benefits.cardProgram.productType.toLowerCase();
                }
            }

            if (item.benefits && item.benefits.cardConfig && item.benefits.cardConfig.config && item.benefits.cardConfig.config.totalEligibility) {
                item.eligibility = item.benefits.cardConfig.config.totalEligibility.amount;
            } else {
                item.eligibility = 0;
            }

            item.remainingEligibility = item.eligibility - item.reimbursed;
            if (item.remainingEligibility < 0) {
                item.remainingEligibility = 0;
            }

            companyID = companyID || $("meta[name='companyID']").attr("content");
            item.profileUrl = "/beneficiaries/" + item.employeeID + '?companyID=' + companyID;
            item.balanceAmount = item.transfered - item.spentAmount;
            item.spentPercentage = (item.spentAmount / item.transfered) * 100;
            item.reiemburssedPercentage = (item.reimbursed / item.transfered) * 100;
            item.transferPercentage = 100 - item.spentPercentage - item.reiemburssedPercentage;
            item['statementUrl'] = Urls.getCorpApiUrl().VIEW_STATEMENT +
                'companies/' + companyID +
                '/employees/' + item.employeeID +
                '/statement?productType=' + productType +
                '&token=' + Storage.get('authToken') +
                '&corpID=' + $("meta[name='corpID']").attr("content");
            item.transfered = Util.formatINR(item.transfered / 100);
            item.spentAmount = Util.formatINR(item.spentAmount / 100);
            item.reimbursed = Util.formatINR(item.reimbursed / 100);
            item.balanceAmount = Util.formatINR(item.balanceAmount / 100);
            item.currentBalance = Util.formatINR(item.currentBalance / 100);
            item.totalPaidOutAmount = Util.formatINR(item.totalPaidOutAmount / 100);
            item.eligibility = Util.formatINR(item.eligibility / 100);
            item.remainingEligibility = Util.formatINR(item.remainingEligibility / 100);
            item.productType = param.productType;
            if($('#activateCloseCards')[0].value === 'true' && item.benefits.cardState === 'ENABLED'){
                item.activateCloseCard = true;
            } else{
                item.activateCloseCard = false;
            }

            return item;
        });

        let responseObject = {
            totalCount: respData.totalCount,
            records: recordsArray
        };
        deferred.resolve(responseObject);
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        deferred.reject(error);
    });
    return deferred.promise();
};


let closeCard = function(companyId, employeeId) {
    let url = Urls.getCorpApiUrl().CANCEL_PROGRAM
        .replace(":COMPANY_ID:", companyId)
        .replace(":EMPLOYEE_ID:", employeeId);
    let inputJson = {
        "token": Storage.get('authToken'),
        "corpID": $("meta[name='corpID']").attr("content"),
        "programId": $('#transferId').val()
    };
    return ServiceConnector.post(inputJson, url, {});
};

let getApproverDependants = function(param) {
    let deferred = $.Deferred(),
        url = Urls.getOMSApiUrl().GET_APPROVER_DEPENDANTS
        .replace(':authToken:', Storage.get('authToken'))
        .replace(':cardID:', param.cardID) + "&corpID=" + $("meta[name='corpID']").attr("content") + "&pageNo=" + (parseInt(param.pageNumber) - 1) + "&pageSize=" + param.pageSize;
    ServiceConnector.get(url, {}).then(function(respData) {
        let records = respData.config.dependents.map(function(item) {
            var dob = item.dob;
            item.age = Util.calculateAge(new Date(dob));
            dob = new Date(dob).getTime();
            item.dob = Util.timeStampToDate(dob);
            item.gender = item.gender.toLowerCase();
            return item;
        });
        respData.records = records;
        deferred.resolve(respData);
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        deferred.reject(error);
    });
    return deferred.promise();
}

let closeCardV2 = function(obj){
    let url = Urls.getOMSApiUrl().CLOSE_CARD.replace(":API_KEY:", Storage.get('authToken'));
    return ServiceConnector.post(obj, url );
}

export default {
    getEmployeesV2: getEmployeesV2,
    getProgramBenifitEmployeesV2: getProgramBenifitEmployeesV2,
    getEmployees: getEmployees,
    getBenifitEmployees: getBenifitEmployees,
    closeCard: closeCard,
    getIndividualEmployeeDetails: getIndividualEmployeeDetails,
    getIndividualEmployeePayout: getIndividualEmployeePayout,
    getApproverDependants: getApproverDependants,
    closeCardV2: closeCardV2,
    addUserToGroup: addUserToGroup,
    removeUserFromGroup: removeUserFromGroup
}

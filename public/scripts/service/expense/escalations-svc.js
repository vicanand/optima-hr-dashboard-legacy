import ServiceConnector from "../../common/serviceConnector";
import Urls from '../../common/urls';
import Util from '../../common/util';
import Storage from '../../common/webStorage'

export default {
    getEscalatedClaims: function(param) {
        let deferred = $.Deferred(),
            searchQuery = param && param.searchQuery || '',
            filterValue = (param.statusFilters) ? [param.statusFilters] : [],
            url = Urls.getOMSApiUrl().GET_ESCALATED_CLAIMS;
        var inputJson = {
            "corpID": $("meta[name='corpID']").attr("content"),
            "token": Storage.get('authToken'),
            "programID": param.programID,
            "escalationStates": filterValue,
            "pageNo": (param.pageNumber - 1),
            "pageSize": param.pageSize
        }
        if (searchQuery !== '') {
            inputJson.searchTerm = searchQuery;
        }
        if (param.userID) {
            inputJson.userID = param.userID;
        }
        ServiceConnector.post(inputJson, url, {}).then(function(respData) {
            let records = respData.listOfEscalations.map(function(aggregate) {
                aggregate.amount = Util.formatINR(aggregate.claimAmount.amount / 100);
                aggregate.status = aggregate.escalationState.toLowerCase();
                aggregate.dateObj = Util.timeStampToDateAndTime(aggregate.updatedAt);
                aggregate.beneficiaryEmailId = aggregate.beneficiaryEmailId.split('\n');
                return aggregate;
            });
            respData.records = records;
            respData.totalCount = respData.numberOfEscalations;
            deferred.resolve(respData);
        }, function() {
            deferred.reject();
        });
        return deferred.promise();
    },
    approveEscalatedClaim: function(programID, claimID) {
        let url = Urls.getOMSApiUrl().APPROVE_ESCALATED_CLAIM;
        let inputJson = {
            "token": Storage.get('authToken'),
            "corpID": $("meta[name='corpID']").attr("content"),
            "programID": programID,
            "claimID": claimID
        }
        return ServiceConnector.post(inputJson, url, {});
    },
    declineEscalatedClaim: function(programID, claimID) {
        let url = Urls.getOMSApiUrl().DECLINE_ESCALATED_CLAIM;
        let inputJson = {
            "token": Storage.get('authToken'),
            "corpID": $("meta[name='corpID']").attr("content"),
            "programID": programID,
            "claimID": claimID
        }
        return ServiceConnector.post(inputJson, url, {});
    }
}

import ServiceConnector from "../../common/serviceConnector";
import Urls from '../../common/urls';
import Util from '../../common/util';
import Constants from '../../common/constants';
import Storage from '../../common/webStorage';
export default {
    getBillsForProgram: function(param) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_BILLS_FOR_PROGRAM,
            stateList = [],
            searchTerm = '';
        if (param && param.filterValue) {
            stateList = param.filterValue.split(',');
        } else {
            stateList = ['APPROVED', 'UNPAID', 'PARTIALLY_PAID', 'PAID', 'UPLOADED', 'DECLINED']
        }
        if (param && param.searchQuery) {
            searchTerm = param.searchQuery
        }
        var offset = (param.pageNumber - 1) * (param.pageSize);
        let inputObj = {
            "token": Storage.get('authToken'),
            "corpID": $("meta[name='corpID']").attr("content"),
            "cardProgramID": param.cardProgramID,
            "limit": param.pageSize,
            "offset": offset,
            "stateList": stateList,
            "userID": param.userID || null
                //"dateRange": {
                //    "fromDateYYYYmmDD": "20160101",
                //    "toDateYYYYmmDD": "20170210"
                //},
                //"userIdentificationVector": searchTerm,
        }

        ServiceConnector.post(inputObj, url).then(function(respData) {
            let records = respData.bills.map(function(aggregate) {
                if (aggregate.value) {
                    aggregate.amount = Util.formatINR(aggregate.value.amount / 100);
                } else {
                    aggregate.amount = '-';
                }
                aggregate.status = aggregate.state ? Constants.REIMBURSEMENT_BILLS_APPROVAL_STATUS[aggregate.state] : '-';
                aggregate.billStatus = aggregate.state ? Constants.REIMBURSEMENT_BILLS_STATUS[aggregate.state] : '-';
                if (aggregate.billStatus === 'Unpaid') {
                    aggregate.billStatus = Constants.ESCALATED_BILLS_STATUS[aggregate.attrs['corpben.escalationState']];
                }
                aggregate.statusClass = aggregate.state ? Constants.DOC_DRIVE_STATUS_CLASS[aggregate.billStatus] : '';
                aggregate.secondaryStatus = aggregate.attrs.declineReason || '';
                aggregate.partialReason = aggregate.attrs.partialReason || '';
                if (aggregate.updatedAt) {
                    aggregate.dateObj = Util.timeStampToDateAndTime(aggregate.updatedAt);
                } else {
                    aggregate.dateObj = {
                        date: '-',
                        time: '-'
                    }
                }
                if (aggregate.userProfile && aggregate.userProfile.emailList.length) {
                    aggregate.emailID = aggregate.userProfile.emailList[0].email;
                } else {
                    aggregate.emailID = '';
                }
                if (aggregate.userProfile && aggregate.userProfile.name) {
                    aggregate.name = aggregate.userProfile.name.firstName + ' ' + aggregate.userProfile.name.lastName;
                } else {
                    aggregate.name = '-';
                }
                if (aggregate.userProfile && aggregate.userProfile.mobileNumber) {
                    aggregate.mobileNumber = aggregate.userProfile.mobileNumber;
                } else {
                    aggregate.mobileNumber = '';
                }
                if (aggregate.attrs.merchantName) {
                    aggregate.merchantName = aggregate.attrs.merchantName.toLowerCase();
                } else {
                    aggregate.merchantName = '-';
                }
                if (aggregate.approvedAmount && aggregate.billStatus !== 'Declined' && aggregate.billStatus !== 'Uploaded' && aggregate.billStatus !== 'Escalated' && aggregate.billStatus !== 'Unpaid') {
                    aggregate.approvedAmount = Util.formatINR(aggregate.approvedAmount / 100);
                } else {
                    aggregate.approvedAmount = '-';
                }

                if (aggregate.attrs.category && aggregate.attrs.subcategory) {
                    aggregate.attrs.category = aggregate.attrs.subcategory;
                    aggregate.attrs.category = aggregate.attrs.category.replace(/([a-z])([A-Z])/g, '$1 $2');
                }
                return aggregate;
            });
            respData.records = records;
            respData.totalCount = respData.count;
            deferred.resolve(respData);
        }, function() {
            deferred.reject();
        });
        return deferred.promise();
    }
}

import ServiceConnector from "../common/serviceConnector";
import Urls from '../common/urls';
import Util from '../common/util';
import Constants from '../common/constants';
import AppLabels from '../common/appLabels';
import Meta from '../common/metaStorage';
import Storage from '../common/webStorage';
import moment from '../../lib/moment/moment';

export default {
    getOrders: function(param) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_AGGREGATE_PAYOUTS.replace(":COMPANYID:", $("meta[name='companyID']").attr("content"))
            .replace(":CORPID:", $("meta[name='corpID']").attr("content"))
            .replace(":PROGRAMID:", $('#transferId').val())
            .replace(":API_KEY:", Storage.get('authToken')) + "&pageNo=" + param.pageNumber + "&pageSize=" + param.pageSize + "&aggregateBy=MONTH&sortOrder=DESC";
        ServiceConnector.get(url).then(function(respData) {
            const currencyName = Meta.getMetaElement('currencyName');
            let records = respData.aggregates.map(function(aggregate) {
                let aggregateByArr = aggregate.aggregationKey.split('-');
                aggregate.aggregationKey = Util.transferPeriodFormat(aggregateByArr[1], aggregateByArr[0]);
                /*if (aggregate.totalAmount) {
                    aggregate.totalAmount.amount = Util.formatINR(aggregate.totalAmount.amount / 100);
                }*/
                aggregate.amount = 0;
                aggregate.totalPayouts = 0;
                aggregate.beneficiaries = 0;
                if(aggregate.aggregationValue){
                    let stats = aggregate.aggregationValue.payoutStatsByStatus;
                    for(let key in stats){
                        let payoutsCount = stats[key].payoutsCount,
                            amount = stats[key].totalAmount.amount;
                        aggregate.totalPayouts += payoutsCount;
                        if(key === 'claimed' || key === 'temporarily_failed'){
                            aggregate.amount += amount;
                            aggregate.beneficiaries += payoutsCount;
                        }
                    }
                }
                aggregate.amount = Util.formatCurrency(aggregate.amount / 100, currencyName);
                aggregate.beneficiaries = Util.formatNumber(aggregate.beneficiaries);
                aggregate.totalPayouts = Util.formatNumber(aggregate.totalPayouts);
                aggregate["startDate"] = new Date(aggregateByArr[0] + '/' + aggregateByArr[1] + '/' + '01').getTime();
                aggregate["endDate"] = new Date(Util.endDateForTrnasfer(aggregateByArr[0], aggregateByArr[1])).getTime();

                return aggregate;
            });

            let responseJson = {
                records: records,
                totalCount:respData.totalOrders
            }
            deferred.resolve(responseJson);
        }, function(error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject();
        });


        return deferred.promise();
    },
    getPayoutStats: function(param){
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_PAYOUT_STATS.replace(":COMPANYID:", $("meta[name='companyID']").attr("content"))
            .replace(":CORPID:", $("meta[name='corpID']").attr("content"))
            .replace(":PROGRAMID:", param.productID)
            .replace(":API_KEY:", Storage.get('authToken')) + "&employeeID=" + param.employeeID;
        ServiceConnector.get(url).then(function (respData) {
            let currencySymbol = $("meta[name='currencySymbol']").attr("content");
            let payoutStats = {
                'waiting':0,
                'successful':0,
                'scheduled':0,
                'revoked': 0
            };
            for (var key in respData.payoutStatsByStatus) {
                var amount = respData.payoutStatsByStatus[key].totalAmount.amount;
                if (amount) {
                    amount = amount / 100;
                    respData.payoutStatsByStatus[key].totalAmount.amount = amount;
                }
            }

            if (respData.payoutStatsByStatus.temporarily_failed || respData.payoutStatsByStatus.temporarily_failed === 0) {
                payoutStats.waiting = Util.formatCurrency(respData.payoutStatsByStatus.temporarily_failed.totalAmount.amount, respData.payoutStatsByStatus.temporarily_failed.totalAmount.currency);
            }

            if (respData.payoutStatsByStatus.claimed || respData.payoutStatsByStatus.claimed === 0) {
                payoutStats.successful = Util.formatCurrency(respData.payoutStatsByStatus.claimed.totalAmount.amount, respData.payoutStatsByStatus.claimed.totalAmount.currency);
            }

            if (respData.payoutStatsByStatus.scheduled || respData.payoutStatsByStatus.scheduled === 0) {
                payoutStats.scheduled = Util.formatCurrency(respData.payoutStatsByStatus.scheduled.totalAmount.amount, respData.payoutStatsByStatus.scheduled.totalAmount.currency);
            }

            if (respData.payoutStatsByStatus.revoked || respData.payoutStatsByStatus.revoked === 0) {
                payoutStats.revoked = Util.formatCurrency(respData.payoutStatsByStatus.revoked.totalAmount.amount, respData.payoutStatsByStatus.revoked.totalAmount.currency);
            }
        
            let responseJson = {
                payoutStatsByStatus: payoutStats
            }

            deferred.resolve(responseJson);
        }, function(error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject();
        });


        return deferred.promise();

    },

    getTransferOrders: function(param) {
        let deferred = $.Deferred();
        let authToken = Storage.get('authToken'),
            corpID = $("meta[name='corpID']").attr("content"),
            companyID = $("meta[name='companyID']").attr("content"),
            ifiBankName = $("meta[name='ifi']").attr("content"),
            compareDates = param.compareDates;
        let url = Urls.getOMSApiUrl().GET_ORDERS
            .replace(":token:", Storage.get('authToken'))
        let header = {};
        let input = {
            "corpID": $("meta[name='corpID']").attr("content"),
            "companyID":  $("meta[name='companyID']").attr("content"),
            "programID":  $('#transferId').val(),
            "criteria": param.filterValue === null ? "ALL" : param.filterValue,
            "pageNo":param.pageNumber,
            "pageSize":param.pageSize
        }

        ServiceConnector.post(input, url, header).then(function(respData) {
            let responseJson = respData;
            let records = {};
            const currencyName = Meta.getMetaElement('currencyName');
            records.orders = responseJson.orderDetails.map(function(item) {
                item["status"] = Constants.TRANSFER_ORDERS_STATUS_LABELS[item["status"]];
                item["statusMessage"]= Constants.PAYOUT_STATUS_MESSAGE_LABEL[item["statusMessage"]] || item["statusMessage"];
                item["isGSTOrder"] = false;
                if(item.amount === null)
                {item.amount=item.amount}
                else{
                    item.amount = Util.formatINR(item.amount);
                }
                var payoutMonth,payoutYear;
                payoutMonth = Number(item.createdOn);
                payoutYear = Number(item.createdOn);
                if (!item.order.orderID) {
                    item.order.orderID = 'NA';
                }
                if (item.order.amount) {
                    item.order.amount = Util.formatCurrency(item.order.amount, currencyName);
                }
                
                if (item.payoutDate) {
                    let dateParts = item.payoutDate.split('T')[0];
                    item.payoutDate = Util.dateFormat(dateParts.split('-')[2], dateParts.split('-')[1], dateParts.split('-')[0]);
                } else {
                    item.payoutDate = "NA";
                }
                if(payoutMonth >= 7 && payoutYear >= 2017){
                    item["isGSTOrder"] = true;
                }
                item.fileUrl = Urls.getOMSApiUrl().GET_ORDER_FILE+"?corpID="+corpID+"&orderID="+item.order.orderID +"&companyID="+companyID+"&token="+authToken;
                let checkGenie =0;
                if(localStorage.getItem('checkGenie')){
                    checkGenie= parseInt(localStorage.getItem('checkGenie'));
                }
                item.csvUrl=Urls.getOMSApiUrl().GET_ORDER_REPORT+"?corpID="+corpID+"&orderID="+item.order.orderID +"&companyID="+companyID+"&token="+authToken;
                item.performaUrl = Urls.getCorpApiUrl().GET_PERFORMA_INVOICE.replace(':orderId', item.order.orderID) +
                    "?token=" + authToken + "&corpID=" + corpID;

                item.invoiceUrl = Urls.getCorpApiUrl().GET_INVOICE.replace(':orderId', item.order.orderID) + "?token=" + authToken + "&corpID=" + corpID;
                item.productType = param.productType;
                item.compareDates = compareDates;
                item.ifiBankName = ifiBankName;
                item.corpID = corpID;
                item.checkGenie = checkGenie;
                item.isProgramManagerRead = param.isProgramManagerRead
                item.isProgramManagerWrite = param.isProgramManagerWrite
                return item;
            });
            responseJson.records = records.orders;
            responseJson.totalCount = responseJson.totalOrders;

            deferred.resolve(responseJson);
        }, function(error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject();
        });


        return deferred.promise();
    },
    getTransferPayouts: function(param) {

        let transferId = param.transferId;

        let deferred = $.Deferred();

        let url = Urls.getCorpApiUrl().GET_ORDER_PAYOUTS.replace(":COMPANY_ID:", $("meta[name='companyID']").attr("content"))
            .replace(":ORDER_ID:", transferId) + "?pageNo=" + param.pageNumber + "&pageSize=" + param.pageSize + "&searchTerm=" + param.searchQuery + "&criteria=" + param.filterValue

        ServiceConnector.get(url, { token: Storage.get('authToken') }).then(function(respData) {


            let records = respData.payouts.map(function(item) {
                item["statusLabel"] = AppLabels.PAYOUT_STATUS_LABELS[item["status"]];
                item.amount = Util.formatINR(item.amount);

                if (item.payoutCompleteDate) {
                    item.payoutCompleteDate = Util.dateFormat(item.payoutCompleteDate.split('T')[0].split('-')[2], item.payoutCompleteDate.split('T')[0].split('-')[1], item.payoutCompleteDate.split('T')[0].split('-')[0]);
                } else {
                    item.payoutCompleteDate = "NA";
                }
                return item;
            });



            let responseJson = {
                totalCount: records,
                records: respData.payouts
            };


            deferred.resolve(responseJson);
        }, function(error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject();
        });


        return deferred.promise();
    },

    getPayouts: function(param) {
        let deferred = $.Deferred(),

            compareDates = param.compareDates;
        let url = Urls.getOMSApiUrl().GET_PAYOUTS.replace(":COMPANYID:", $("meta[name='companyID']").attr("content"))
                .replace(":CORPID:", $("meta[name='corpID']").attr("content"))
                .replace(":PROGRAMID:", $('#programID').val())
                .replace(":API_KEY:", Storage.get('authToken')) + "&pageNo=" + (param.pageNumber - 1) + "&pageSize=" + param.pageSize + "&startDate=" + param.startDate +
            "&endDate=" + param.endDate + "&orderID=" + param.orderId;
        if (param.statusFilters != undefined && param.statusFilters != '') {
            url = url + "&statusFilters=" + param.statusFilters;
        }
        if (param.searchQuery != undefined && param.searchQuery != '') {
            url = url + "&searchTerm=" + param.searchQuery;
        }
        // + "&criteria=" + param.filterValue
        ServiceConnector.get(url).then(function(respData) {
            let totalPayouts = 0;
            let totalScheduledPayouts = 0;
            let totalProcessingPayouts = 0;
            let totalTransferredPayouts = 0;
            let totalWaitingPayouts = 0;
            var totalPayoutkeys = ['scheduled', 'initiated', 'claimed', 'temporarily_failed', 'partially_revoked', 'revoked', 'invalid', 'cancelled', 'failed'];

            if(!param.statusFilters.length || (param.statusFilters.match(/,/g) || []).length > 0){
                sessionStorage.setItem('payouts', JSON.stringify(respData.payoutCountByStatus));
            }

            respData.payoutCountByStatus = JSON.parse(sessionStorage.getItem('payouts'));

            for (let i = 0; i < totalPayoutkeys.length; i++) {
                totalPayouts += respData.payoutCountByStatus[totalPayoutkeys[i]] || 0;
            }

            if (respData.payoutCountByStatus.scheduled != undefined) {
                totalScheduledPayouts += respData.payoutCountByStatus.scheduled;
            }
            if (respData.payoutCountByStatus.initiated != undefined) {
                totalProcessingPayouts += respData.payoutCountByStatus.initiated;
            }
            if (respData.payoutCountByStatus.temporarily_failed != undefined) {
                totalWaitingPayouts += respData.payoutCountByStatus.temporarily_failed;
            }
            if (respData.payoutCountByStatus.claimed != undefined) {
                totalTransferredPayouts += respData.payoutCountByStatus.claimed;
            }
            /*if (respData.payoutCountByStatus.partially_revoked != undefined) {
                totalTransferredPayouts += respData.payoutCountByStatus.partially_revoked;
            }*/
            const currencyName = Meta.getMetaElement('currencyName');
            let records = respData.payouts.map(function(item) {
                item["statusMessage"]= Constants.PAYOUT_STATUS_MESSAGE_LABEL[item["statusMessage"]] || item["statusMessage"];
                item.payoutCompleteDate = Util.timeStampToDate(item.scheduledOn);
                item["revokedAmount"] = (item.amount - item.finalDepositedAmount)/100;
                item.amount = Util.formatCurrency(item.amount / 100, currencyName);
                item.finalDepositedAmount = Util.formatCurrency(item.finalDepositedAmount / 100, currencyName);
                item["primaryStatusClass"] = Constants.PAYOUT_STATUS_LABELS_CLASSES[item.payoutStatus];
                item["primaryStatus"] = Constants.PAYOUT_STATUS_LABELS[item.payoutStatus];
                item["secondaryStatus"] = Constants.PAYOUT_SECONDARY_STATUS_LABELS[item.payoutStatus];
                if (item.payoutStatus == 'revoked') {
                    item["errorMsg"] = 'Revoked Rs. ' + item.amount + ' on ' + Util.timeStampToDate(item.lastStatusChangedOn);
                } else if (item.payoutStatus == 'cancelled') {
                    item["errorMsg"] = 'Cancelled on ' + Util.timeStampToDate(item.lastStatusChangedOn);
                } else {
                    item["errorMsg"] = Constants.PAYOUT_STATUS_MESSAGES[item.payoutStatus];
                }
                item.profileUrl = "/beneficiaries/" + item.employeeID + '?companyID=' + item.companyID;
                item.compareDates = compareDates;
                item.revokeFunds = param.revokeFunds;
                item.isProgramManagerRW = param.isProgramManagerRW
                return item;
            });
            let totalCount = 0;
            $.each(respData.payoutCountByStatus, function(key, value) {
                totalCount += value;
            });
            let responseJson = {
                totalCount: totalCount,
                records: records,
                totalPayouts: totalPayouts,
                totalScheduledPayouts: totalScheduledPayouts,
                totalWaitingPayouts: totalWaitingPayouts,
                totalProcessingPayouts: totalProcessingPayouts,
                totalTransferredPayouts: totalTransferredPayouts,
            };
            console.log(responseJson);
            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject(error);
            // return respError;
        });


        return deferred.promise();
    },

    getEmployeePayouts: function(param) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_EMPLOYEE_PAYOUTS
            .replace(":COMPANYID:", param.companyID)
            .replace(":CORPID:", param.corpID)
            .replace(":PROGRAMID:", param.programID)
            .replace(":API_KEY:", param.token)
            .replace(":EMPLOYEEID:", param.employeeID) + "&pageNo=" + (param.pageNumber - 1) + "&pageSize=" + param.pageSize
        // + "&criteria=" + param.filterValue
        return ServiceConnector.get(url); //.then(function (respData) {
        //     let records = respData.payouts.map(function (item) {
        //         item.payoutCompleteDate = Util.timeStampToDate(item.payoutCompleteDate);
        //         item.amount.amount = Util.formatINR(item.amount.amount/100);
        //         item["primaryStatusClass"] = Constants.PAYOUT_STATUS_LABELS_CLASSES[item.status];
        //         item["primaryStatus"] = Constants.PAYOUT_STATUS_LABELS[item.status];
        //         item["secondaryStatus"] = Constants.PAYOUT_SECONDARY_STATUS_LABELS[item.status];
        //         item["errorMsg"] = Constants.PAYOUT_STATUS_MESSAGES[item.status];
        //         return item;
        //     });
        //     let responseJson = {
        //         totalCount: respData.payoutCountByStatus.claimed,
        //         records: records
        //     };
        //     deferred.resolve(responseJson);
        // }, function () {
        //     deferred.reject();
        // });
        // return deferred.promise();
    },

    cancelOrder: function(orderId) {
        let url = Urls.getOMSApiUrl().CANCEL_ORDER
            .replace(":COMPANY_ID:", $("meta[name='companyID']").attr("content"))
            .replace(":ORDER_ID:", orderId)
        let header = {};
        let input = {
            "token": Storage.get('authToken'),
            "corpID": $("meta[name='corpID']").attr("content"),
            "orderID": orderId,
            "companyID": $("meta[name='companyID']").attr("content")
        }
        return ServiceConnector.post(input, url, header);
    },

    cancelPayout: function(orderId, payoutId, cancelAllScheduledPayouts) {
        let url = Urls.getOMSApiUrl().CANCEL_PAYOUT
            .replace(":token:", Storage.get('authToken'))
        let header = {};
        let input = {
            "payoutID": payoutId,
            "corpID": $("meta[name='corpID']").attr("content"),
            "orderID": orderId,
            "companyID": $("meta[name='companyID']").attr("content"),
            "cancelAllScheduledPayouts" : cancelAllScheduledPayouts
        }
        return ServiceConnector.post(input, url, header);
    },

    revokePayout: function(orderId, payoutId, remarks) {
        let url = Urls.getOMSApiUrl().REVOKE_API
            .replace(":token:", Storage.get('authToken'))
            .replace(":ORDER_ID:", orderId)
            .replace(":PAYOUT_ID:", payoutId);
        let header = {};
        let input = {
            "payoutIDs": payoutId,
            "corpID": $("meta[name='corpID']").attr("content"),
            "orderID": orderId,
            "companyID": $("meta[name='companyID']").attr("content"),
            "remarks": remarks
        }

        return ServiceConnector.post(input, url, header);
    },
    downloadOrderFile: function(url) {
        let header = {
            "token": Storage.get('authToken'),
            "corpID": $("meta[name='corpID']").attr("content")
        }
        return ServiceConnector.get(url);
    },
    downloadOrderReport: function(url) {
        let header = {};
        let input ={}
        return ServiceConnector.post(input, url, header);
    },
    getOrderHistory: function(entityId, entityName = 'payout') {
        let url = Urls.getOMSApiUrl().GET_AUDIT_LOGS
        .replace(":entity_id:", entityId)
        .replace(":entity_name:",entityName)
        .replace(":corp_id:", $("meta[name='corpID']").attr("content"))
        .replace(":company_id:", $("meta[name='companyID']").attr("content"))
        .replace(":token:", Storage.get('authToken'))
        return ServiceConnector.get(url);
    },
    getFormattedOrderDetails: function(orderList) {
        let corpAccounts = Storage.getCollection(Constants.STORAGE_CORP_ACCOUNTS_INFO);
        
        let data = {
            records : this.filterOrderDetails(orderList, corpAccounts)
        }
        return data;
    },
    filterOrderDetails: function(orderList, corpAccounts) {
        for(let i=0; i < orderList.length; i++) {
            if(isNaN(orderList[i].actionBy)) {
                orderList[i]["name"] = "SYSTEM"
                continue;
            }
            orderList[i]["name"] = "USER UNAVAILABLE"
            for(let j=0; j < corpAccounts.length; j++) {
                if(orderList[i].actionBy == corpAccounts[j].zetaUserID) {
                    orderList[i]["name"] = corpAccounts[j].name;
                    break;
                }
            }
            /* convert UTC date to locale time Zone */
             orderList[i]["actionAt"] = moment.utc(orderList[i]["actionAt"]).local().format('Do MMM YYYY LTS');
        }
        return orderList;
    }
}

import ServiceConnector from "../../common/serviceConnector";
import Urls from '../../common/urls';
import Util from '../../common/util';

export default {
    recordEvents: function(eventLogs) {
        let url = Urls.getOMSApiUrl().RECORD_EVENTS;
        let requestJson = {
            "events": eventLogs
        }
        return ServiceConnector.post(requestJson, url, {});
    }
}
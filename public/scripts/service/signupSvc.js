import Urls from "../common/urls";
import ServiceConnector from "../common/serviceConnector";

export default {
    getAssetUploadEndpoint: function() {
        let url = Urls.getOMSApiUrl().GET_ASSET_UPLOAD_ENDPOINT;
        let inputObj = {};
        let headers = {
            "Content-Type": "application/json"
        };
        return ServiceConnector.get(url, inputObj, headers);
    }
}

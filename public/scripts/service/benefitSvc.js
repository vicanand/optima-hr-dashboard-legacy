import Urls from "../common/urls";
import ServiceConnector from "../common/serviceConnector";
import Storage from "../common/webStorage";

let headers = {
    "Content-Type": "application/json"
};

export default {
    getTopMerchants: function(aggregationType) {
        let url = Urls.getOMSApiUrl().GET_AGGREGATIONS
            .replace(":companyID:", $("meta[name='companyID']").attr("content"))
            .replace(":corpID:", $("meta[name='corpID']").attr("content"))
            .replace(":programID:", $('#transferId').val())
            .replace(":authToken:", Storage.get('authToken'))
            .replace(':aggregationType:', aggregationType);
        let inputObj = {};
        
        return ServiceConnector.get(url, inputObj, headers);
    },
    getTopMerchantsInExpense: function(aggregationType) {
        let url = Urls.getOMSApiUrl().GET_AGGREGATIONS
            .replace(":companyID:", $("meta[name='companyID']").attr("content"))
            .replace(":corpID:", $("meta[name='corpID']").attr("content"))
            .replace(":programID:", $('#transferId').val())
            .replace(":authToken:", Storage.get('authToken'))
            .replace(':aggregationType:', aggregationType);
        let inputObj = {};
        return ServiceConnector.get(url, inputObj, headers);
    },
    createCustomCardProgram: function(requestBody){
        let url = Urls.getOMSApiUrl().CREATE_CUSTOM_CARD_PROGRAM
        .replace(":token:", Storage.get('authToken'));
        return ServiceConnector.post(requestBody, url, headers);
    },
    updateCustomCardProgram: function(requestBody){
        let url = Urls.getOMSApiUrl().UPDATE_CUSTOM_CARD_PROGRAM
        .replace(":token:", Storage.get('authToken'));
        return ServiceConnector.post(requestBody, url, headers);
    },
    createFundingAccount: function(requestBody){
        let url = Urls.getOMSApiUrl().ADD_FUND_ACCOUNT
        .replace(":token:", Storage.get('authToken'));
        return ServiceConnector.post(requestBody, url, headers);
    },
    getAssetUploadEndPoint: function () {
        let url = Urls.getOMSApiUrl().GET_ASSET_UPLOAD_ENDPOINT;
        return ServiceConnector.get(url);
    },
    updateCardProgramLogo: function(requestBody) {
        let url = Urls.getOMSApiUrl().UPDATE_CUSTOM_CARD_DESIGN
        .replace(":token:", Storage.get('authToken'));
        return ServiceConnector.post(requestBody, url, headers);
    },
    updateSpendTimeVnd: function(requestBody){
        let url = Urls.getSodexoVnUrl().UPDATE_SPEND_SETTING
        return ServiceConnector.put(requestBody, url, headers);
    },
    getSpendSettingVn: function(cardProgramId){
        let url = Urls.getSodexoVnUrl().GET_SPEND_SETTINGS
        .replace(":cardProgramID:", cardProgramId)
        return ServiceConnector.get(url);
    }
}

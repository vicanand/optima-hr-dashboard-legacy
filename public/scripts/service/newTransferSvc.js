import ServiceConnector from '../common/serviceConnector'
import Urls from '../common/urls'
import Utils from '../common/util'
import Storage from '../common/webStorage'

export default {
    getFileUploadDetails: function () {
        let url = Urls.getOMSApiUrl().GET_FILE_UPLOAD_DETAILS;
        let header = {
            'token': authToken,
            "corpID": corpID
        }
        return ServiceConnector.get(url, header);
    },

    createOrder: function (inputObj) {
        let url;
        if (inputObj.orderType == "recurring") {
            url = Urls.getOMSApiUrl().CREATE_ORDER_RECURRING
        } else {
            url = Urls.getOMSApiUrl().CREATE_ORDER
        }
        url = url
        // url = Urls.getOMSApiUrl().CREATE_ORDER
            .replace(":COMPANY_ID:", $("meta[name='companyID']").attr("content"))
            .replace(":token:", Storage.get('authToken'))
            .replace(":corpID:", $("meta[name='corpID']").attr('content'));
        let header = {}
        return ServiceConnector.post(inputObj, url, header);
    },

    createOrderV2: function (inputObj) {
        let url;
        if (inputObj.orderType == "recurring") {
            url = Urls.getOMSApiUrl().CREATE_ORDER_RECURRING
        } else {
            url = Urls.getOMSApiUrl().CREATE_ORDER_V2
        }
        url = url
        // url = Urls.getOMSApiUrl().CREATE_ORDER
            .replace(":COMPANY_ID:", $("meta[name='companyID']").attr("content"))
            .replace(":token:", Storage.get('authToken'))
            .replace(":corpID:", $("meta[name='corpID']").attr('content'));
        let header = {}
        return ServiceConnector.post(inputObj, url, header);
    },

    addPayout: function (inputObj) {
        let url = Urls.getOMSApiUrl().ADD_PAYOUT
            .replace(":token:", Storage.get('authToken'))
        let header = {}
        return ServiceConnector.post(inputObj, url, header);
    },

    actionOnOrder: function(inputObj){
        let url = Urls.getOMSApiUrl().ACTION_ON_ORDER
            .replace(":token:", Storage.get('authToken'))
        let header = {}
        return ServiceConnector.post(inputObj, url, header);
    },


    getOrderSummary: function () {
        let url = Urls.getOMSApiUrl().GET_ORDER_SUMMARY.replace(":authToken:", Storage.get('authToken'));
        let inputObj = {
            'corporateID': $("meta[name='corpID']").attr('content'),
            "companyID": $("meta[name='companyID']").attr('content'),
            "orderID": $("#orderID").val()
        };
        let deferred = $.Deferred();
        ServiceConnector.post(inputObj, url, {}).then(function (respData) {
            let totalCount = 0, totalAmt = 0;
            respData["orderSummaries"] = [];
            if (respData.scheduledTransferDate) {
                $('#scheduleDateBanner').html(Utils.timeStampToDate(new Date(respData.scheduledTransferDate).getTime()));
            }
            else {
                $('#scheduleDateBanner').text('-');
            }
            if (respData.poNumber && respData.poNumber != '') {
                $('#poOrder').html(respData.poNumber);
            }
            else {
                $('#poOrder').html('-');
            }
            for (var prop in respData.orderDescriptionByStatusMap) {
                if (respData.orderDescriptionByStatusMap.hasOwnProperty(prop)) {
                    respData["orderSummaries"].push({
                        "payoutStatus": prop,
                        "amount": respData.orderDescriptionByStatusMap[prop].payoutAmount,
                        "count": respData.orderDescriptionByStatusMap[prop].payoutCount,
                    });
                }
            };
            let records = respData.orderSummaries.map(function (item) {
                item.amount = +item.amount;
                item.count = +item.count;
                if (item.amount >= 0) {
                    totalAmt += item.amount;
                }
                totalCount += item.count;
                item.amount = Utils.formatINR(item.amount);
                item.count = Utils.formatINR(item.count);
                if (item.payoutStatus == 'valid') {
                    $('#validEntries').html(item.count);
                    $('#validAmt').html(item.amount);
                }
                return item;
            });
            records.push({
                'payoutStatus': 'total',
                'count': Utils.formatINR(totalCount),
                'amount': Utils.formatINR(totalAmt)
            });
            let responseJson = {
                totalCount: respData.orderSummaries.length,
                records: records
            }

            deferred.resolve(responseJson);
        }, function (error) {
            if (error.status === 401) {
                Utils.renewAuth();
            }
            deferred.reject(error);
            // return respError;
        });
        return deferred.promise();
    },
    downloadErrorReport: function(orderID){
        let url = Urls.getOMSApiUrl().DOWNLOAD_ERROR_REPORT
            .replace(":authToken:", Storage.get('authToken'))
            .replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
            .replace(":bulkOrderID:", orderID);
        return ServiceConnector.get(url);
    },
    getErrorRows: function(orderID, pageNumber=1, pageSize=10){
        let url = Urls.getOMSApiUrl().GET_ERROR_ROWS
            .replace(":authToken:", Storage.get('authToken'))
            .replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
            .replace(":bulkOrderID:", orderID);
            url += `&pageNumber=${pageNumber}&pageSize=${pageSize}`
        return ServiceConnector.get(url);
    },
}

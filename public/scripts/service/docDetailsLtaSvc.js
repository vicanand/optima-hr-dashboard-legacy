import Urls from "../common/urls";
import ServiceConnector from "../common/serviceConnector";
import Utils from "../common/util";
import Util from "../common/util";
import Applabels from "../common/appLabels";
import Constant from '../common/constants';


export default {
    getSearchDocs: function(token, docId) {
        let requestJson = {
            "token": token,
            "claimID": docId
        };
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_BILL_DETAILS+ '?token='+token+'&claimID='+docId;
        ServiceConnector.get(url).then(function(respData) {
            deferred.resolve(respData);
        }, function (error) {
            if (error.status === 401) {
                document.location.href = '/logout';
            }
            deferred.reject(error);
            // return respError;
        });
        return deferred.promise();
    }

}

import Urls from "../common/urls";
import ServiceConnector from "../common/serviceConnector";
import Utils from "../common/util";
import Applabels from "../common/appLabels";
import Constant from "../common/constants";
import Storage from "../common/webStorage";

export default {
  getDocs: function (params) {
    var searchQuery = params.searchQuery;
    var attrs = {};
    if (searchQuery != "") {
      if (Utils.validateEmail(searchQuery)) {
        attrs["email"] = [searchQuery];
      } else if (Utils.validateMobile(searchQuery)) {
        attrs["phone"] = [searchQuery];
      } else {
        attrs["name"] = [searchQuery];
      }
    }
    if (params.filterValue != "") {
      attrs["productType"] = [params.filterValue];
    }
    let deferred = $.Deferred();
    let url = Urls.getOMSApiUrl().GET_DOCS;
    // replace(":COMPANY_ID:", 570)
    //     .replace(":CORP_ID:", 342)
    //     .replace(":ACCOUNT_ID:", 836)
    //     .replace(":API_KEY:", Storage.get('authToken'))
    // + "&pageNo=" + param.pageNumber
    // + "&pageSize=" + param.pageSize;
    let endInclusive = params.pageSize * params.pageNumber;
    let startInclusive = endInclusive - params.pageSize + 1;
    let requestJson = {
      corpID: $("meta[name='corpID']").attr("content"),
      token: Storage.get("authToken"),
      docGroupsRequest: {
        owner: params.businessId,
        directoryPath: "biller/claims",
        range: {
          startInclusive: startInclusive,
          endInclusive: endInclusive,
        },
        attrs: attrs,
      },
    };
    ServiceConnector.post(requestJson, url, {}).then(
      function (respData) {
        let responseRecords = respData.docGroupsInfos.map(function (
          item,
          index
        ) {
          if (item.attrs && item.attrs.name) {
            item.name = item.attrs.name;
          } else {
            item.name = "NA";
          }
          if (item.attrs && item.attrs.producttype) {
            item.producttype = item.attrs.producttype;
            item.producttype = Applabels.PROGRAM_TYPES[item.producttype];
          } else {
            item.producttype = "NA";
          }
          item.id = item.docGroupID.split("/")[2];
          item.index = index;
          return item;
        });
        let responseJson = {
          totalCount: respData.docGroupsCount,
          records: responseRecords,
        };
        deferred.resolve(responseJson);
      },
      function (error) {
        if (error.status === 401) {
          Utils.renewAuth();
        }
        deferred.reject(error);
        // return respError;
      }
    );
    return deferred.promise();
  },

  getDoc: function (requestJson) {
    let deferred = $.Deferred();

    ServiceConnector.post(requestJson, Urls.getOMSApiUrl().GET_DOC).then(
      function (respData) {
        let name;
        if (respData.attrs && respData.attrs.name) {
          name = respData.attrs.name;
        } else {
          name = "NA";
        }

        let email;
        if (respData.attrs && respData.attrs.email) {
          email = respData.attrs.email;
        } else {
          email = "NA";
        }

        let phone;
        if (respData.attrs && respData.attrs.phone) {
          phone = respData.attrs.phone;
        } else {
          phone = "NA";
        }

        let modified;
        if (respData.attrs && respData.attrs.updatedAt) {
          modified = respData.attrs.updatedAt;
        } else {
          modified = "NA";
        }

        let docs = respData.docs.map(function (item) {
          item.name = name;
          item.email = email;
          item.phone = phone;
          item.modified = modified;
          let filenameArray = item.data.split("/");

          item.filename = filenameArray[filenameArray.length - 1];

          return item;
        });

        deferred.resolve(_.filter(docs, { mimeType: "image/jpeg" }));
      },
      function (error) {
        if (error.status === 401) {
          Utils.renewAuth();
        }
        deferred.reject(error);
        // return respError;
      }
    );

    return deferred.promise();
  },

  loadMoreDocs: function (startInclusive, endInclusive) {
    let deferred = $.Deferred();
    let phoneno = /^\d{10}$/;
    let shortClaimId = /^[0-9]+-[a-zA-Z0-9]{4}$/;
    let searchTxt = $("#searchDocs").val();
    let programType = $("#programType").val();
    let statusType = "none";
    let docSource = "none";
    if ($(".all-status").is(":visible")) {
      statusType = $("#statusType").val();
    } else {
      docSource = $("#documentType").val();
    }
    let searchTxtArr = [];
    let productTypeArr = [];
    let status = [];
    let corpID = $("meta[name='corpID']").attr("content");
    let companyId = $(".company-dropdown .dropdown-toggle")
      .attr("data-company")
      .split("-")
      .pop();
    let dateRange = $("#timePeriod").val();
    dateRange = dateRange.split("-");
    let startFromDate = $.trim(dateRange[0]);
    let endFromDate = $.trim(dateRange[1]);
    startFromDate = startFromDate.split(" ");
    endFromDate = endFromDate.split(" ");
    dateRange =
      startFromDate[2] +
      "-" +
      Utils.monthNameToMonth(startFromDate[1]) +
      "-" +
      startFromDate[0] +
      " - " +
      endFromDate[2] +
      "-" +
      Utils.monthNameToMonth(endFromDate[1]) +
      "-" +
      endFromDate[0];
    let startDate = Utils.dateToepochTime(dateRange.slice(0, 10), "-", "after");
    let endDate = Utils.dateToepochTime(dateRange.slice(13, 23), "-", "before");
    let directoryPath = [];
    if (statusType != "none") {
      directoryPath.push(
        "biller/claims",
        "optima/proofs/" + corpID + "/" + companyId
      );
    } else {
      directoryPath.push("optima/travel_log/" + corpID + "/" + companyId);
    }
    let requestJson = {
      corpID: $("meta[name='corpID']").attr("content"),
      token: Storage.get("authToken"),
      docGroupsRequest: {
        // "owner" : "138686@business.zeta.in",
        directoryPaths: directoryPath,
        owner:
          $(".company-dropdown .dropdown-toggle")
            .attr("data-company")
            .split("-", 1)[0] + "@business.zeta.in",
        attrs: {},
        range: {
          startInclusive: startInclusive,
          endInclusive: endInclusive,
        },
        startDate: startDate,
        endDate: endDate,
      },
    };
    if (Utils.isEmail(searchTxt)) {
      searchTxtArr[0] = searchTxt;
      requestJson.docGroupsRequest.attrs["email"] = searchTxtArr;
    } else if (searchTxt.match(phoneno)) {
      searchTxtArr[0] = "+91" + searchTxt;
      requestJson.docGroupsRequest.attrs["phone"] = searchTxtArr;
    } else if (searchTxt.match(shortClaimId)) {
      requestJson.docGroupsRequest.isPrefix = false;
      requestJson.docGroupsRequest.docGroupName = searchTxt;
    } else {
      searchTxtArr[0] = searchTxt;
      requestJson.docGroupsRequest.attrs["name"] = searchTxtArr;
    }
    let allProgramsSelected =
      $("#programType option:not(:selected)").length === 0;
    if (allProgramsSelected) {
      //   requestJson.docGroupsRequest.directoryPath.push("optima/proofs/"+corpID+"/"+companyId);
      requestJson.docGroupsRequest.attrs["producttype"] =
        Constant.ALL_DOC_AND_CORPBEN_CARD_PROGRAM_TYPES;
    } else if ($("#programType option:selected").length === 0) {
      requestJson.docGroupsRequest.attrs["producttype"] = [""];
    } else {
      productTypeArr = programType;
      requestJson.docGroupsRequest.attrs["cardprogramid"] = productTypeArr;
    }
    ("/Users/rb/projects/hr/zwe-corporate-hr-dashboard/.git/index.lock");

    for (let i = 0; i < statusType.length; i++) {
      if (
        statusType[i] === "UPLOADED" ||
        statusType[i] === "DECLINED" ||
        statusType[i] === "proof_submitted"
      ) {
        status.push(statusType[i]);
      }
      if (statusType[i] === "APPROVED") {
        status.push(
          "APPROVED",
          "PAID",
          "UNPAID",
          "PARTIALLY_PAID",
          "PARTIALLY_APPROVED"
        );
      }
    }
    if (status.length) {
      requestJson.docGroupsRequest.attrs["state"] = status;
    } else {
      requestJson.docGroupsRequest.attrs["state"] = [""];
    }
    ServiceConnector.post(requestJson, Urls.getOMSApiUrl().GET_DOCS).then(
      function (respData) {
        deferred.resolve(respData);
      },
      function (error) {
        if (error.status === 401) {
          Utils.renewAuth();
        }
        deferred.reject(error);
        // return respError;
      }
    );
    return deferred.promise();
  },
  getSearchDocs: function (inputJson) {
    let deferred = $.Deferred();
    ServiceConnector.post(inputJson, Urls.getOMSApiUrl().GET_DOCS).then(
      function (respData) {
        deferred.resolve(respData);
      },
      function (error) {
        if (error.status === 401) {
          Utils.renewAuth();
        }
        deferred.reject(error);
        // return respError;
      }
    );
    return deferred.promise();
  },
  getDocsDownloadStatus: function (inputJson) {
    let deferred = $.Deferred();
    ServiceConnector.post(inputJson, Urls.getOMSApiUrl().DOWNLOAD_DOCS).then(
      function (respData) {
        deferred.resolve(respData);
      },
      function (error) {
        if (error.status === 401) {
          Utils.renewAuth();
        }
        deferred.reject(error);
        // return respError;
      }
    );
    return deferred.promise();
  },
  getActiveCardPrograms: function (companyID) {
    let url = Urls.getOMSApiUrl()
      .GET_ACTIVE_BENEFIT_PROGRAMS.replace(
        ":authToken:",
        Storage.get("authToken")
      )
      .replace(":corpID:", $("meta[name='corpID']").attr("content"))
      .replace(":companyID:", companyID)
      .replace(":programStatus:", "ACTIVE");
    let inputObj = {};
    let headers = {
      "Content-Type": "application/json",
    };
    let deferred = $.Deferred();
    //   let url = Urls.getOMSApiUrl().GET_BILL_DETAILS+ '?token='+token+'&claimID='+docId;   // GET_ACTIVE_BENEFIT_PROGRAMS
    ServiceConnector.get(url).then(
      function (respData) {
        deferred.resolve(respData);
      },
      function (error) {
        if (error.status === 401) {
          document.location.href = "/logout";
        }
        deferred.reject(error);
        // return respError;
      }
    );
    return deferred.promise();
  },
  getActiveCardProgramsPegination: function (companyID, pageNumber, pageSize) {
    let url = Urls.getOMSApiUrl()
      .GET_ACTIVE_BENEFIT_PROGRAMS_PAGINATION.replace(
        ":authToken:",
        Storage.get("authToken")
      )
      .replace(":corpID:", $("meta[name='corpID']").attr("content"))
      .replace(":companyID:", companyID)
      .replace(":programStatus:", "ACTIVE")
      .replace(":pageSize:", pageSize)
      .replace(":pageNumber:", pageNumber);
    let inputObj = {};
    let headers = {
      "Content-Type": "application/json",
    };
    let deferred = $.Deferred();
    //   let url = Urls.getOMSApiUrl().GET_BILL_DETAILS+ '?token='+token+'&claimID='+docId;   // GET_ACTIVE_BENEFIT_PROGRAMS
    ServiceConnector.get(url).then(
      function (respData) {
        deferred.resolve(respData);
      },
      function (error) {
        if (error.status === 401) {
          document.location.href = "/logout";
        }
        deferred.reject(error);
        // return respError;
      }
    );
    return deferred.promise();
  },
  getActiveCardProgramsPaginationPost: function (
    companyID,
    pageNumber,
    pageSize
  ) {
    let inputObj = {
      token: Storage.get("authToken"),
      corpID: $("meta[name='corpID']").attr("content"),
      companyID: companyID,
      programStatus: "ACTIVE",
      pageNumber: pageNumber,
      pageSize: pageSize,
      productTypes: Constant.DOC_CORPBEN_PROGRAM_TYPES,
    };
    let headers = {
      "Content-Type": "application/json",
    };
    let deferred = $.Deferred();
    ServiceConnector.post(
      inputObj,
      Urls.getOMSApiUrl().GET_ACTIVE_BENEFIT_PROGRAMS_PAGINATION_POST
    ).then(
      function (respData) {
        deferred.resolve(respData);
      },
      function (error) {
        if (error.status === 401) {
          document.location.href = "/logout";
        }
        deferred.reject(error);
        // return respError;
      }
    );
    return deferred.promise();
  },
};

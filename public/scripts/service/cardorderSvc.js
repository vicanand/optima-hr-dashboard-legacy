import ServiceConnector from "../common/serviceConnector";
import Urls from "../common/urls";
import Util from "../common/util";
import Storage from "../common/webStorage";
import Env from '../common/env';

export default {
    orderVerifyBulk: function (filename, fileurl, key, refName, cardMasterID, payloadType, addressFlag) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().ORDER_VERIFY_BULK_CARDCOST.replace(
            ":token:",
            Storage.get('authToken')
        );
        let header = {};
        let body = {
            spotlightProgram: "SUPER_CARD_PERSONALISED",
            corpID: $("meta[name='corpID']").attr("content"),
            companyID: $("meta[name='companyID']").attr("content"),
            payloadType: payloadType,
            fileName: filename,
            fileUrl: fileurl,
            key: key,
            cardMasterID: cardMasterID,
            refName: refName
        };

        ServiceConnector.post(body, url, header).then(
            function (respData) {
                deferred.resolve(respData);
            },
            function (respErr) {
                respErr = JSON.parse(respErr.responseText);
                // if (respErr.attributes.errorSet.length === 1) {
                //     Storage.set('duplicateBulk', true);
                //     $('.duplicateOr, .duplicateEntries').show();
                //     $('#duplicateEntries').attr('data-url', 'Step3Bulk');
                // } else {
                //     Storage.set('duplicateBulk', false);
                //     $('.duplicateOr, .duplicateEntries').hide();
                // }
                if (respErr.attributes.fileProcessingSummary) {
                    if (respErr.attributes.fileProcessingSummary.invalidRows > 0) {
                        $("#virtualGiftContainer").hide();
                        $("#uploadAgain").html("UPLOAD AGAIN");
                        $("#card-selection-loader").modal("hide");
                        $("#addBenefitsBulk").hide();
                        $(".file-details-cntr").show();
                        $("#noOfInvalidEntries").html(
                            respErr.attributes.fileProcessingSummary.invalidRows
                        );
                        $("#fileName").html(Storage.get("filename"));
                        switch (Storage.get("filetype")) {
                            case "text/csv":
                                $("#fileTypeImg").attr(
                                    "src",
                                    "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-12.png"
                                );

                                break;
                            case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                                $("#fileTypeImg").attr(
                                    "src",
                                    "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xlsx.png"
                                );
                                break;
                            case "application/vnd.ms-excel":
                                $("#fileTypeImg").attr(
                                    "src",
                                    "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png"
                                );
                                break;
                            default:
                                $(".fileType").attr(
                                    "src",
                                    "https://card-program-files.s3.amazonaws.com/hrDashboard/images/group-xls.png"
                                );
                                break;
                        }

                        $("#errorVirtualGiftingTransfers").hide();
                        var ifi = $("meta[name='ifi']").attr("content");
                        let responseData = respErr.attributes.rows;
                        //Build an array containing Customer records.
                        var customers = new Array();
                        if (ifi == "HDFC") {
                            customers.push([
                                "Name",
                                "Email",
                                "Amount (₹)",
                                "Transfer Date",
                                "Mobile",
                                "Reason"
                            ]);
                            for (var i = 0; i < responseData.length; i++) {
                                customers.push([
                                    responseData[i].row[0],
                                    responseData[i].row[1],
                                    responseData[i].row[2],
                                    responseData[i].row[3],
                                    responseData[i].row[4],
                                    allErrors(responseData[i])
                                ]);
                            }
                        } else {
                            customers.push([
                                "Name",
                                "Email or Mobile",
                                "Amount (₹)",
                                "Transfer Date",
                                "Reason"
                            ]);
                            for (var i = 0; i < responseData.length; i++) {
                                customers.push([
                                    responseData[i].row[0],
                                    responseData[i].row[1],
                                    responseData[i].row[2],
                                    responseData[i].row[3],
                                    allErrors(responseData[i])
                                ]);
                            }
                        }

                        function allErrors(error) {
                            let slash = "<br/>";
                            let errorsReturn = "";
                            error.errors[0] != undefined ?
                                (errorsReturn += error.errors[0][0] + slash) :
                                "";
                            error.errors[1] != undefined ?
                                (errorsReturn += error.errors[1][0] + slash) :
                                "";
                            error.errors[2] != undefined ?
                                (errorsReturn += error.errors[2][0] + slash) :
                                "";
                            error.errors[3] != undefined ?
                                (errorsReturn += error.errors[3][0] + slash) :
                                "";
                            error.errors[4] != undefined ?
                                (errorsReturn += error.errors[4][0] + slash) :
                                "";
                            return errorsReturn;
                        }

                        //Create a HTML Table element.
                        var table = $(
                            "<table class='custom-table hover-state error-table custom-table-error' />"
                        );
                        table[0].border = "0";

                        //Get the count of columns.
                        var columnCount = customers[0].length;

                        //Add the header row.
                        var row = $(table[0].insertRow(-1));
                        for (var i = 0; i < columnCount; i++) {
                            var headerCell = $("<th />");
                            headerCell.html(customers[0][i]);
                            row.append(headerCell);
                        }

                        //Add the data rows.
                        for (var i = 1; i < customers.length; i++) {
                            row = $(table[0].insertRow(-1));
                            for (var j = 0; j < columnCount; j++) {
                                if (responseData[i - 1].errors[j] == undefined) {
                                    var cell = $("<td class='' />");
                                    cell.html(customers[i][j]);
                                    row.append(cell);
                                } else {
                                    var cell = $("<td class='error-column' />");
                                    cell.html(customers[i][j]);
                                    row.append(cell);
                                }
                            }
                        }

                        var dvTable = $("#errorVirtualGiftingTransfers1");
                        dvTable.html("");
                        dvTable.append(table);
                        $("#fileErrorWrpr").show();
                        $("#uploadAgain").attr("data-type", "bulk");
                        // $('.modal-backdrop').remove()
                        $("body").removeClass("modal-open");
                        $("#createOrderModalSGOops").modal("hide");
                    }
                } else {
                    $("#createOrderModalFail").modal("show");
                }
                deferred.reject();
            }
        );
        return deferred.promise();
    },

    orderVerifyManual: function (benefitSuperCardOrderBeneficiary, refName, cardMasterID, payloadType) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().ORDER_VERIFY_BULK_CARDCOST.replace(
            ":token:",
            Storage.get('authToken')
        );
        let header = {};
        let body = {
            spotlightProgram: "SUPER_CARD_NON_PERSONALISED",
            corpID: $("meta[name='corpID']").attr("content"),
            companyID: $("meta[name='companyID']").attr("content"),
            payloadType: payloadType,
            superCardOrderBeneficiary: benefitSuperCardOrderBeneficiary,
            refName: refName,
            cardMasterID: cardMasterID
        };

        ServiceConnector.post(body, url, header).then(
            function (respData) {
                deferred.resolve(respData);
            },
            function (error) {
                $("#createOrderModalFail").modal("show");
                deferred.reject(error);
            }
        );
        return deferred.promise();
    },

    orderCreateBulk: function (source, requestID, fundingAccountId) {
        let deferred = $.Deferred();
        let body = {
            corpID: $("meta[name='corpID']").attr("content"),
            companyID: $("meta[name='companyID']").attr("content"),
            fundingAccountID: fundingAccountId,
            requestID: requestID,
            source: source
        };
        let header = {};
        let url = Urls.getOMSApiUrl().ORDER_CREATE_CARDCOST.replace(
            ":token:",
            Storage.get('authToken')
        );

        ServiceConnector.post(body, url, header).then(
            function (respData) {
                deferred.resolve(respData);
            },
            function (respData) {
                deferred.reject(respData);
            }
        );
        return deferred.promise();
    },

    resetPin: function (newpin, nonce, superCardID) {
        let deferred = $.Deferred();
        let body = {
            'newPin': newpin,
            'nonce': nonce
        };
        let header = {
            'X-Zeta-AuthToken': Storage.get('authToken')
        };
        let ifi;
        if (Env.CURR_ENV.toLowerCase() == 'stage' || Env.CURR_ENV.toLowerCase() == 'preprod' || Env.CURR_ENV.toLowerCase() == 'localhost') {
            ifi = '140793';
        } else {
            ifi = '156699';
        }
        let url = Urls.getresetPinUrl().RESET_PIN
            .replace(":ifi:", ifi)
            .replace(":superCardID:", superCardID);

        ServiceConnector.post(body, url, header).then(
            function (respData) {
                deferred.resolve(respData);
            },
            function (respData) {
                deferred.reject(respData);
            }
        );
        return deferred.promise();
    },

    orderCreateManual: function (source, requestID, fundingAccountId) {
        let deferred = $.Deferred();
        let body = {
            corporateID: $("meta[name='corpID']").attr("content"),
            companyID: $("meta[name='companyID']").attr("content"),
            fundingAccountID: fundingAccountId,
            requestID: requestID,
            source: source
        };
        let header = {};
        let url = Urls.getOMSApiUrl().ORDER_CREATE_VIRTUAL.replace(
            ":token:",
            Storage.get('authToken')
        );

        ServiceConnector.post(body, url, header).then(
            function (respData) {
                deferred.resolve(respData);
            },
            function (respData) {
                deferred.reject(respData);
            }
        );
        return deferred.promise();
    },

    getAccDetails: function (authToken, corpID, accountId) {
        let url = Urls.getOMSApiUrl()
            .GET_FUNDING_ACCOUNT.replace(":authToken:", authToken)
            .replace(":corpID:", corpID)
            .replace(":accountID:", accountId);
        let inputObj = {};
        let headers = {
            "Content-Type": "application/json"
        };
        return ServiceConnector.get(url, inputObj, headers);
    },

    getIfiDetails: function (authToken, corporateId, ifiID) {
        let url = Urls.getOMSApiUrl()
            .GET_IFIS_ACCOUNTS.replace(":corpID:", corporateId)
            .replace(":ifiID:", ifiID)
            .replace(":type:", "ZETA_SETTLED")
            .replace(":authToken:", authToken);
        return ServiceConnector.get(url);
    },

    getcardOrderList: function (param) {
        let deferred = $.Deferred();
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyID = $("meta[name='companyID']").attr("content");
        if (param.filterValue != "" || param.searchQuery != "" || param.endDate != "") {
            $("#resetFilter").attr("disabled", false);
        }
        let isSearch = param.searchQuery === '' ? '' : '&filters.searchString=' + param.searchQuery;
        let isFilter = param.filterValue === 'ALL' || param.filterValue === '' ? '' : '&status=' + param.filterValue;

        let url =
            Urls.getOMSApiUrl()
            .GET_CARD_ORDER_DETAILS.replace(":companyID:", companyID)
            .replace(":corpID:", corpID)
            .replace(":token:", authToken) +
            "&&filters.sqlSortOrder=ASC" +
            "&&filters.fromDate=" + param.startDate +
            "&&filters.toDate=" + param.endDate +
            '&&filters.pageNumber=' + (param.pageNumber - 1) +
            '&&filters.pageSize=10' + isSearch + isFilter;
        ServiceConnector.get(url).then(
            function (respData) {
                let records = {};
                records = respData.superCardOrders.map(function (item) {
                    item.totalAmount = Util.formatINR(item.totalAmount / 100);
                    item.date = Util.epochToIstDate(item.createdAt);
                    item.time = Util.epochToIstTime(item.createdAt);
                    item.corpID = corpID;
                    item.cardType =
                        item.type == "SUPER_CARD_NON_PERSONALISED" ?
                        "Non Personalised" :
                        "Personalised";
                    return item;
                });
                let totalCounts = respData.count;
                let responseJson = {
                    records: records,
                    totalCount: totalCounts
                };

                deferred.resolve(responseJson);
            },
            function (error) {

                deferred.reject(error);
            }
        );
        return deferred.promise();
    },

    getdisbursedOrderList: function (param) {
        let deferred = $.Deferred();
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyID = $("meta[name='companyID']").attr("content");
        if (param.filterValue != "" || param.searchQuery != "" || param.endDate != "") {
            $("#resetFilter").attr("disabled", false);
        }
        let isSearch = param.searchQuery === '' ? '' : '&filters.searchString=' + param.searchQuery;
        let isFilter = param.filterValue === 'ALL' || param.filterValue === '' ? '' : '&status=' + param.filterValue;
        let url =
            Urls.getOMSApiUrl()
            .GET_DISBURSED_CARD_ORDER_DETAILS.replace(":companyID:", companyID)
            .replace(":corpID:", corpID)
            .replace(":token:", authToken) +
            "&&filters.sqlSortOrder=ASC" +
            "&&filters.fromDate=" + param.startDate +
            "&&filters.toDate=" + param.endDate +
            '&&filters.pageNumber=' + (param.pageNumber - 1) +
            '&&filters.pageSize=10' + isSearch + isFilter;
        // "&status=" + param.filterValue +

        ServiceConnector.get(url).then(
            function (respData) {
                let records = {};
                records = respData.superCardPayouts.map(function (item) {
                    item.date = Util.epochToIstDate(item.createdOn);
                    item.time = Util.epochToIstTime(item.createdOn);
                    item.maskedCardNumber = item.maskedCardNumber != null ? item.maskedCardNumber : 'N/A';
                    item.embossingText1 = item.embossingText1 == "null" ? 'N/A' : item.embossingText1 == "" ? 'N/A' : item.embossingText1;
                    item.type =
                        item.type == "SUPER_CARD_NON_PERSONALISED" ?
                        "Non Personalised" :
                        "Personalised";
                    item.corpID = corpID;
                    return item;
                });
                let totalCounts = respData.count;
                let responseJson = {
                    records: records,
                    totalCount: totalCounts
                };

                deferred.resolve(responseJson);
            },
            function (error) {

                deferred.reject(error);
            }
        );
        return deferred.promise();
    },

    getProgramBenifitEmployeesV2: function (params) {
        let deferred = $.Deferred();
        let programtype = "reward";
        let item = {};
        let url =
            Urls.getOMSApiUrl()
            .GET_EMPLOYEESV2_REWARD.replace(":programType:", programtype)
            .replace(":corpID:", $("meta[name='corpID']").attr("content"))
            .replace(":companyID:", $("meta[name='companyID']").attr("content"))
            .replace(":token:", Storage.get('authToken')) +
            "&pageNo=" +
            (params.pageNumber - 1) +
            "&pageSize=" +
            params.pageSize;
        ServiceConnector.get(url, {}).then(
            function (respData) {
                item = respData;
                if (item.employees.length > 0) {
                    for (let i = 0; i < item.employees.length; i++) {
                        item.employees[i].activeCardCount = 0;
                        item.employees[i].closedCardCount = 0;
                        if (item.employees[i].cards) {
                            for (let j = 0; j < item.employees[i].cards.length; j++) {
                                //   set1.add(item.employees[i].cards[j].programID);
                                if (item.employees[i].cards[j].status === "ACTIVE") {
                                    item.employees[i].activeCardCount++;
                                }
                                if (item.employees[i].cards[j].status === "CLOSED") {
                                    item.employees[i].closedCardCount++;
                                }
                            }
                        }
                    }
                }
                let responseObject = {
                    totalCount: item.numberOfTotalResults,
                    records: item.employees
                };

                console.log("employee Details", responseObject);

                deferred.resolve(responseObject);
            },
            function (error) {
                if (error.status === 401) {
                    Util.renewAuth();
                }
                deferred.reject(error);
            }
        );
        return deferred.promise();
    },

    setFundAcc: function (name) {
        let authToken = Storage.get('authToken');
        let inputObj = {
            corpID: $("meta[name='corpID']").attr("content"),
            name: name
        };
        let url = Urls.getOMSApiUrl().ADD_ACCOUNTS.replace(":API_KEY:", authToken);
        let headers = {
            "Content-Type": "application/json"
        };
        return ServiceConnector.post(inputObj, url);
    },

    cardAction: function (typeOfAction, superCardID, email) {
        let authToken = Storage.get('authToken');
        let inputObj = {
            superCardAction: typeOfAction,
            superCardID: superCardID,
            corpID: $("meta[name='corpID']").attr("content"),
            companyID: $("meta[name='companyID']").attr("content"),
            email: email
        };
        let url = Urls.getOMSApiUrl().CARD_ACTIONS;
        let headers = {
            "Content-Type": "application/json",
            "x-zeta-authtoken": authToken
        };
        return ServiceConnector.post(inputObj, url, headers);
    },

    getIndividualOrdersRow: function (params) {
        $("#updateTitle").html(
            $("#SpotlightIFIName").val() == "HDFC" ?
            "Email / Mobile" :
            "Email or Mobile"
        );
        let deferred = $.Deferred();
        let orderId = $("#get-oreder-id").html();
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyId = $("meta[name='companyID']").attr("content");
        let ifiBankName = $("meta[name='ifi']").attr("content");
        let statusFilter = params.filterValue;
        let searchQuery;
        if (params.searchQuery != "") {
            searchQuery = params.searchQuery;
        } else {
            searchQuery = "";
        }

        let url =
            Urls.getOMSApiUrl()
            .GET_VIRTUAl_INDIVIDUAL_ORDER.replace(":authToken:", authToken)
            .replace(":corpID:", corpID)
            .replace(":companyID:", companyId)
            .replace(":orderID:", orderId) +
            "&searchString=" +
            searchQuery +
            "&status=" +
            statusFilter +
            "&pageNumber=" +
            (params.pageNumber - 1) +
            "&pageSize=" +
            params.pageSize;
        let header = {};
        let records = {};
        let storeCount = 0;
        ServiceConnector.get(url, header).then(
            function (respData) {
                records = respData.payouts.map(function (item) {
                    item.amount = Util.formatINR(item.amount.amount / 100);
                    item.date = Util.epochToIstDate(item.updatedOn);
                    item.refNo = item.refName;
                    item.beneficiaryName = item.name;
                    item.ifiBankName = ifiBankName;
                    item.corpID = corpID;
                    item.orderStatus =
                        item.status.charAt(0).toUpperCase() +
                        item.status.slice(1).toLowerCase();
                    item.emailorContact =
                        item.contact != undefined && item.email != undefined ?
                        item.email + " / " + item.contact :
                        item.email != undefined ?
                        item.email :
                        item.contact;
                    return item;
                });
                let totalCounts = Number($("#totalPayouts").html()) || 0;
                let responseJson = {
                    records: records,
                    totalCount: totalCounts
                };

                deferred.resolve(responseJson);
            },
            function (error) {

                if (error.status === 401) {
                    Util.renewAuth();
                }
                deferred.reject(error);
                // return respError;
            }
        );
        return deferred.promise();
    },

    getIndividualOrderSummary: function (orderID) {
        let orderId = orderID;
        let authToken = Storage.get('authToken');
        let corpID = $("meta[name='corpID']").attr("content");
        let companyId = $("meta[name='companyID']").attr("content");
        let url = Urls.getOMSApiUrl()
            .GET_VIRTUAl_INDIVIDUAL_ORDER.replace(":authToken:", authToken)
            .replace(":corpID:", corpID)
            .replace(":companyID:", companyId)
            .replace(":orderID:", orderId);
        let header = {};
        return ServiceConnector.get(url, header);
    },

    revokePayoutsInstaNew: function () {
        $("#revokePayoutConfirm").modal("hide");
        $("#revokePayoutLoader").modal("show");
        let token = Storage.get('authToken');
        let orderID = Storage.get("orderID");
        let payoutID = Storage.get("payoutID");
        let spotlightProgram = Storage.get("program");
        let comment = $("#revokeRemark").val();
        let inputObj = {
            corpID: $("meta[name='corpID']").attr("content"),
            companyID: $("meta[name='companyID']").attr("content"),
            payoutID: payoutID,
            orderID: orderID,
            remarks: comment,
            spotlightProgram: spotlightProgram
        };
        let url = Urls.getOMSApiUrl().REVOKE_API_NEW_INSTA.replace(
            ":token:",
            token
        );
        let headers = {
            "Content-Type": "application/json"
        };
        ServiceConnector.post(inputObj, url).then(
            function (respData) {
                $("#revokePayoutLoader").modal("hide");
                $("#revokePayoutSuccess").modal("show");
            },
            function (error) {
                $("#revokePayoutLoader").modal("hide");
                $("#revokePayoutError").modal("show");
            }
        );
    }

    // reissePayout: function (name, email, phone, comments, corpID, companyID) {
    //     let token = Storage.get('authToken');
    //     let orderID = localStorage.getItem('reissueorderID');
    //     let payoutID = localStorage.getItem('reissuepayoutID');
    //     let url = Urls.getOMSApiUrl().REISSUE_API
    //         .replace(':token:', token)
    //         .replace(':corpID:', corpID)
    //         .replace(':name:', name)
    //         .replace(':phone:', phone)
    //         .replace(':email:', email)
    //         .replace(':comments:', comments)
    //         .replace(':companyID:', companyID)
    //         .replace(':orderID:', orderID)
    //         .replace(':payoutID:', payoutID)
    //     let inputObj = {};
    //     let headers = {
    //         "Content-Type": "application/json"
    //     };
    //     return ServiceConnector.get(url, inputObj, headers);
    // },
};
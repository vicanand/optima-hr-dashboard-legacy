import ServiceConnector from "../common/serviceConnector";
import Urls from '../common/urls';
import Util from '../common/util';
import Storage from '../common/webStorage'

let getProgramClosureSuggestion = function(params) {
    let deferred = $.Deferred();
    let url = Urls.getOMSApiUrl().GET_YEAR_END_CLOSURE
        .replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
        .replace(":COMPANYID:", $("meta[name='companyID']").attr("content"))
        .replace(":CLOSURE_DATE:", params.reportGenerationDate)
        .replace(":API_KEY:", Storage.get('authToken'));

    ServiceConnector.get(url, {}).then(function(respData) {
        deferred.resolve(respData);
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
}

let scheduleProgramClosure = function(obj) {
    var inputJson = [],ajax = [];
    let deferred = $.Deferred();
    var length = $('#card-programs-length').data('card-programs-length');
    for(var currIndex=0; currIndex< length; currIndex++) {
        if(!$('#card-program-object-'+currIndex).data('card-program-'+currIndex)) {
            inputJson[currIndex] = {
                "corpID": $("meta[name='corpID']").attr("content"),
                "programID": $('#card-program-id-'+currIndex).data('card-program-id-'+currIndex),
                "token": Storage.get('authToken'),
                "closureDates": obj
            };
            ajax[currIndex] = ServiceConnector.post(inputJson[currIndex], Urls.getOMSApiUrl().SCHEDULE_PROGRAM_CLOSURE);
        }
    }
    $.when(ajax.join()).then(function(respData) {
        deferred.resolve(respData);
    }, function (error) {
        if (error.status === 401) {
            Util.renewAuth();
        }
        deferred.reject(error);
        // return respError;
    });
    return deferred.promise();
}

export default {
    getProgramClosureSuggestion: getProgramClosureSuggestion,
    scheduleProgramClosure: scheduleProgramClosure
}
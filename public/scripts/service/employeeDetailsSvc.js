import ServiceConnector from "../common/serviceConnector";
import Urls from '../common/urls';
import Util from '../common/util';
import Constants from '../common/constants';
import Storage from '../common/webStorage';

export default {

    getPayoutStats: function(param) {
        let deferred = $.Deferred();
        //  let url ="../images/demoStat.txt"
        let respData = {};
        let url = Urls.getOMSApiUrl().GET_PAYOUT_STATS.replace(":COMPANYID:", $("#selected-company-id").val())
            .replace(":CORPID:", $("meta[name='corpID']").attr("content"))
            .replace(":PROGRAMID:", param.productID)
            .replace(":API_KEY:", Storage.get('authToken')) + "&employeeID=" + param.employeeID;
        ServiceConnector.get(url).then(function(respData) {
            respData = respData;
            let payoutStats = {
                'waiting': 0,
                'successful': 0,
                'scheduled': 0,
                'revoked': 0
            };
            let responseJson = {
                payoutStatsByStatus: respData
            }

            deferred.resolve(responseJson);
        }, function(error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject();
        });


        return deferred.promise();

    },


    getPayouts: function(param) {
        let deferred = $.Deferred(),

            compareDates = param.compareDates;
        let url = Urls.getOMSApiUrl().GET_PAYOUTS.replace(":COMPANYID:", $("#selected-company-id").val())
            .replace(":CORPID:", $("meta[name='corpID']").attr("content"))
            .replace(":PROGRAMID:", $('#programID').val())
            .replace(":API_KEY:", Storage.get('authToken')) + "&pageNo=" + (param.pageNumber - 1) + "&pageSize=" + param.pageSize + "&startDate=" + param.startDate +
            "&endDate=" + param.endDate + "&orderID=" + param.orderId;
        if (param.statusFilters != undefined && param.statusFilters != '') {
            url = url + "&statusFilters=" + param.statusFilters;
        }
        if (param.searchQuery != undefined && param.searchQuery != '') {
            url = url + "&searchTerm=" + param.searchQuery;
        }
        // + "&criteria=" + param.filterValue
        ServiceConnector.get(url).then(function(respData) {
            let totalPayouts = 0;
            let totalScheduledPayouts = 0;
            let totalProcessingPayouts = 0;
            let totalTransferredPayouts = 0;
            let totalWaitingPayouts = 0;
            var totalPayoutkeys = ['scheduled', 'initiated', 'claimed', 'temporarily_failed', 'partially_revoked', 'revoked', 'invalid', 'cancelled', 'failed'];

            if (!param.statusFilters.length || (param.statusFilters.match(/,/g) || []).length > 0) {
                sessionStorage.setItem('payouts', JSON.stringify(respData.payoutCountByStatus));
            }

            respData.payoutCountByStatus = JSON.parse(sessionStorage.getItem('payouts'));

            for (let i = 0; i < totalPayoutkeys.length; i++) {
                totalPayouts += respData.payoutCountByStatus[totalPayoutkeys[i]] || 0;
            }

            console.log(respData);

            if (respData.payoutCountByStatus.scheduled != undefined) {
                totalScheduledPayouts += respData.payoutCountByStatus.scheduled;
            }
            /*if (respData.payoutCountByStatus.stalled != undefined) {
                totalScheduledPayouts += respData.payoutCountByStatus.stalled;
            }
            if (respData.payoutCountByStatus.cancelled != undefined) {
                totalScheduledPayouts += respData.payoutCountByStatus.cancelled;
            }*/
            if (respData.payoutCountByStatus.initiated != undefined) {
                totalProcessingPayouts += respData.payoutCountByStatus.initiated;
            }
            if (respData.payoutCountByStatus.temporarily_failed != undefined) {
                totalWaitingPayouts += respData.payoutCountByStatus.temporarily_failed;
            }
            if (respData.payoutCountByStatus.claimed != undefined) {
                totalTransferredPayouts += respData.payoutCountByStatus.claimed;
            }
            /*if (respData.payoutCountByStatus.partially_revoked != undefined) {
                totalTransferredPayouts += respData.payoutCountByStatus.partially_revoked;
            }*/
            let records = respData.payouts.map(function(item) {
                item["statusMessage"] = Constants.PAYOUT_STATUS_MESSAGE_LABEL[item["statusMessage"]] || item["statusMessage"];
                item.payoutCompleteDate = Util.timeStampToDate(item.scheduledOn);
                item.amount = Util.formatINR(item.amount / 100);
                item["primaryStatusClass"] = Constants.PAYOUT_STATUS_LABELS_CLASSES[item.payoutStatus];
                item["primaryStatus"] = Constants.PAYOUT_STATUS_LABELS[item.payoutStatus];
                item["secondaryStatus"] = Constants.PAYOUT_SECONDARY_STATUS_LABELS[item.payoutStatus];
                if (item.payoutStatus == 'revoked') {
                    item["errorMsg"] = 'Revoked Rs. ' + item.amount + ' on ' + Util.timeStampToDate(item.lastStatusChangedOn);
                } else if (item.payoutStatus == 'cancelled') {
                    item["errorMsg"] = 'Cancelled on ' + Util.timeStampToDate(item.lastStatusChangedOn);
                } else {
                    item["errorMsg"] = Constants.PAYOUT_STATUS_MESSAGES[item.payoutStatus];
                }
                item.profileUrl = "/beneficiaries/" + item.employeeID + '?companyID=' + item.companyID;
                item.compareDates = compareDates;
                return item;
            });
            let totalCount = 0;
            $.each(respData.payoutCountByStatus, function(key, value) {
                totalCount += value;
            });
            let responseJson = {
                totalCount: totalCount,
                records: records,
                totalPayouts: totalPayouts,
                totalScheduledPayouts: totalScheduledPayouts,
                totalWaitingPayouts: totalWaitingPayouts,
                totalProcessingPayouts: totalProcessingPayouts,
                totalTransferredPayouts: totalTransferredPayouts
            };
            console.log(responseJson);
            deferred.resolve(responseJson);
        }, function(error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject(error);
            // return respError;
        });


        return deferred.promise();
    },

    getEmployeePayoutsScheduled: function(param) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_EMPLOYEE_PAYOUTS
            .replace(":COMPANYID:", $("#selected-company-id").val())
            .replace(":CORPID:", $("meta[name='corpID']").attr("content"))
            .replace(":PROGRAMID:", $('#transferId').val())
            .replace(":API_KEY:", Storage.get('authToken'))
            .replace(":EMPLOYEEID:", param.extraParams.employeeID) + "&statusFilters=" + param.extraParams.statusFilters + "&pageNo=" + (param.pageNumber - 1) + "&pageSize=" + param.pageSize
            // + "&criteria=" + param.filterValue
        ServiceConnector.get(url).then(function(respData) {
            console.log(respData);
            let records = respData.payouts.map(function(item) {
                item.date = Util.timeStampToDate(item.scheduledOn);
                item.amount = Util.formatINR(item.amount / 100);
                item.orderId = item.orderID;
                item.referenceNo = item.referenceID;
                return item;
            });
            let responseJson = {
                // totalCount: respData.payoutCountByStatus.scheduled,
                totalRecords: respData.payoutCountByStatus.scheduled,
                records: records
            };
            console.log('response', responseJson);
            deferred.resolve(responseJson);
        }, function() {
            deferred.reject();
        });
        return deferred.promise();
    },
    getEmployeePayoutsWaiting: function(param) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_EMPLOYEE_PAYOUTS
            .replace(":COMPANYID:", $("#selected-company-id").val())
            .replace(":CORPID:", $("meta[name='corpID']").attr("content"))
            .replace(":PROGRAMID:", $('#transferId').val())
            .replace(":API_KEY:", Storage.get('authToken'))
            .replace(":EMPLOYEEID:", param.extraParams.employeeID) + "&statusFilters=" + param.extraParams.statusFilters + "&pageNo=" + (param.pageNumber - 1) + "&pageSize=" + param.pageSize
            // + "&criteria=" + param.filterValue
        ServiceConnector.get(url).then(function(respData) {
            console.log(respData);
            let records = respData.payouts.map(function(item) {
                item.date = Util.timeStampToDate(item.scheduledOn);
                item.amount = Util.formatINR(item.amount / 100);
                item.orderId = item.orderID;
                item.referenceNo = item.referenceID;
                return item;
            });
            let responseJson = {
                totalRecords: respData.payoutCountByStatus.temporarily_failed,
                records: records
            };
            console.log('response', responseJson);
            deferred.resolve(responseJson);
        }, function() {
            deferred.reject();
        });
        return deferred.promise();
    },
    getEmployeePayoutsCanceled: function(param) {
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_EMPLOYEE_PAYOUTS
            .replace(":COMPANYID:", $("#selected-company-id").val())
            .replace(":CORPID:", $("meta[name='corpID']").attr("content"))
            .replace(":PROGRAMID:", $('#transferId').val())
            .replace(":API_KEY:", Storage.get('authToken'))
            .replace(":EMPLOYEEID:", param.extraParams.employeeID) + "&statusFilters=" + param.extraParams.statusFilters + "&pageNo=" + (param.pageNumber - 1) + "&pageSize=" + param.pageSize
            // + "&criteria=" + param.filterValue
        ServiceConnector.get(url).then(function(respData) {
            console.log(respData);
            let records = respData.payouts.map(function(item) {
                item.date = Util.timeStampToDate(item.scheduledOn);
                item.amount = Util.formatINR(item.amount / 100);
                item.orderId = item.orderID;
                item.referenceNo = item.referenceID;
                return item;
            });
            let responseJson = {
                totalRecords: respData.payoutCountByStatus.cancelled,
                records: records
            };
            console.log('response', responseJson);
            deferred.resolve(responseJson);
        }, function() {
            deferred.reject();
        });
        return deferred.promise();
    },

    getBenefitIndividualEmployee: function(params) {
        let deferred = $.Deferred();
        //  console.log("Hello srv 2");
        let cardPrograms = [];
        let set1 = new Set();
        let benefits = [];
        let cardsTooltip = [];
        let item = {};
        let emp = [];
        let finalItem = {};
        // let url = "../images/demoEmployee.txt";
        let url = Urls.getOMSApiUrl().GET_EMPLOYEESV2.replace(":CORP_ID:", $("meta[name='corpID']").attr("content"))
            .replace(":API_KEY:", Storage.get('authToken')) + "&programID=" + params.productID + "&employeeID=" + params.employeeID;
        ServiceConnector.get(url, {}).then(function(respData) {
            item = respData;
            // console.log('item',item);
            emp.push(item.employees[0]);
            if (emp[0].programIDs.length > 0) {
                // cardPrograms = emp[0].programIDs;
                for (let i = 0; i < emp[0].programIDs.length; i++) {
                    cardPrograms[i] = emp[0].programIDs[i];
                }
            } else {
                if (emp[0].cards) {
                    for (let j = 0; j < emp[0].cards.length; j++) {
                        // cardPrograms.push(emp[0].cards[j].programID);
                        set1.add(emp[0].cards[j].programID);
                    }
                }
                if (set1.size > 0) {
                    for (let a of set1) {
                        cardPrograms.push(a);
                    }
                }
            }
            if (cardPrograms.length) { // fetching active card program details.
                let input = {
                    'corpID': $("meta[name='corpID']").attr("content"),
                    'programIDs': cardPrograms,
                    'detailsRequired': ['cardDesign']
                };
                let programurl = Urls.getOMSApiUrl().GET_PROGRAMDETAILSV2
                    .replace(":API_KEY:", Storage.get('authToken'));
                ServiceConnector.post(input, programurl, {}).then(function(cardData) {
                    let cardProgramData = cardData;
                    if (emp.length > 0) {
                        //  item.employees[i].benefits = [];
                        emp[0].benefits = [];
                        emp[0].cardsTooltip = [];
                        if (emp[0].cards) {
                            for (let j = 0; j < emp[0].cards.length; j++) {
                                let temp = emp[0].cards[j].programID;
                                emp[0].benefits.push(cardProgramData.programs[temp]);
                                emp[0].cardsTooltip.push(cardProgramData.programs[temp].cardDesign.cardName);
                            }
                        }
                    }
                    // console.log(item);
                    let responseJson = {
                        employeeWithCardDetails: emp
                    }
                    console.log('final response', responseJson);
                    deferred.resolve(responseJson);
                });
            } else {
                let responseJson = {
                        totalCount: item.numberOfTotalResults,
                        records: item.employees
                    }
                    // console.log('final response',responseJson);
                deferred.resolve(responseJson);
            }
        });
        return deferred.promise();
    },
    // below fun not required
    getBenefitIndividualEmployee111111: function(params) {
        let deferred = $.Deferred();
        //  console.log("Hello srv 2");
        let cardPrograms = [];
        let set1 = new Set();
        let benefits = [];
        let cardsTooltip = [];
        let item = {};
        let emp = [];
        let finalItem = {};
        item = Storage.getCollection("employeeDetails");
        if (item.length > 0) {
            for (let i = 0; i < item.length; i++) {
                if (item[i].employeeID === params.employeeID) {
                    emp.push(item[i]);
                }
            }



            //   item.employees[0].benefits = [];
            //   item.employees[0].cardsTooltip = [];
            //   if(item.employees[0].cards){
            //       for(let j = 0; j< item.employees[0].cards.length; j++){
            //           set1.add(item.employees[0].cards[j].programID);
            //       }
            //   }
        }
        if (emp[0].cards) {
            for (let j = 0; j < emp[0].cards.length; j++) {
                cardPrograms.push(emp[0].cards[j].programID);
            }
        }

        if (cardPrograms.length) { // fetching active card program details.
            let input = {
                'corpID': $("meta[name='corpID']").attr("content"),
                'programIDs': cardPrograms,
                'detailsRequired': ['cardDesign']
            };
            let programurl = Urls.getOMSApiUrl().GET_PROGRAMDETAILSV2
                .replace(":API_KEY:", Storage.get('authToken'));
            ServiceConnector.post(input, programurl, {}).then(function(cardData) {
                let cardProgramData = cardData;
                if (emp.length > 0) {
                    //  item.employees[i].benefits = [];
                    emp[0].benefits = [];
                    emp[0].cardsTooltip = [];
                    if (emp[0].cards) {
                        for (let j = 0; j < emp[0].cards.length; j++) {
                            let temp = emp[0].cards[j].programID;
                            emp[0].benefits.push(cardProgramData.programs[temp]);
                            emp[0].cardsTooltip.push(cardProgramData.programs[temp].cardDesign.cardName);
                        }
                    }
                }
                // console.log(item);
                let responseJson = {
                    employeeWithCardDetails: emp
                }
                console.log('final response', responseJson);
                deferred.resolve(responseJson);
            });
        } else {
            let responseJson = {
                    totalCount: item.numberOfTotalResults,
                    records: item.employees
                }
                // console.log('final response',responseJson);
            deferred.resolve(responseJson);
        }
        return deferred.promise();
    },

    getIndividualEmployeeCardPayout: function(params) {
        let deferred = $.Deferred(),
            corpID = $("meta[name='corpID']").attr("content"),
            compareDates = params.compareDates;
        let url = Urls.getOMSApiUrl().GET_EMPLOYEE_PAYOUTS
            .replace(":CORPID:", $("meta[name='corpID']").attr("content"))
            .replace(":EMPLOYEEID:", params.employeeID)
            .replace(":COMPANYID:", $("#selected-company-id").val())
            .replace(":PROGRAMID:", $('#transferId').val())
            .replace(":API_KEY:", Storage.get('authToken')) + "&cardID=" + params.cardID + "&pageNo=" + (Number(params.pageNumber) - 1) + "&pageSize=" + params.pageSize;

        ServiceConnector.get(url, {}).then(function(respData) {
            let totalPayouts = 0;
            for (var key in respData.payoutCountByStatus) {
                totalPayouts += respData.payoutCountByStatus[key];
            }
            let records = respData.payouts.map(function(item) {
                item.scheduledOn = Util.timeStampToDate(item.scheduledOn);
                item.amount = Util.formatINR(item.amount / 100);
                item["primaryStatusClass"] = Constants.PAYOUT_STATUS_LABELS_CLASSES[item.payoutStatus];
                item["primaryStatus"] = Constants.PAYOUT_STATUS_LABELS[item.payoutStatus];
                item.compareDates = compareDates;
                return item;
            });


            let responseJson = {
                records: records,
                totalCount: totalPayouts
            };
            deferred.resolve(responseJson);
        }, function(error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject(error);
            // return respError;
        });
        return deferred.promise();
    },
    getTransactionAndClaimBills: function(statement1, statement2, url1, url2) {
        let deferred = $.Deferred();
        let allData = []
        ServiceConnector.post(statement1, url2).then(function(respData) {
                var allBillerData = [];
                allBillerData.push(respData)
                let resp = respData;
                let count = statement1.limit;
                if (count < respData.count) {
                    function billerProgramTranasactionService() {
                        let billerObj = statement1;
                        statement1.offset = count;
                        ServiceConnector.post(billerObj, url2).then(function(respData) {
                            count += respData.bills.length;
                            for (var i = 0; i < respData.bills.length; i++) {
                                allBillerData[0].bills.push(respData.bills[i])
                            }
                            if (count < respData.count - 1) {
                                billerProgramTranasactionService()
                            } else {
                                allData.push(allBillerData);
                                getStatements()
                            }
                        }, function(error) {
                            if (error.status === 401) {
                                Util.renewAuth();
                            }
                        })
                    }
                    billerProgramTranasactionService()
                } else {
                    allData.push(allBillerData);
                    getStatements()
                }
                function getStatements(){
                ServiceConnector.get(url1).then(function(respData) {                       
                        var allResponseData = [];
                        allResponseData.push(respData)
                        let resp = respData;
                        let nextOff = resp.nextOffset;
                        if (nextOff != undefined) {
                            function cardProgramTranasactionService() {
                                let url = url1.replace(new RegExp("([?&]" + 'offset' + "(?=[=&#]|$)[^#&]*|(?=#|$))"), "&" + 'offset' + "=" + encodeURIComponent(nextOff))
                                ServiceConnector.get(url).then(function(respData) {
                                    allResponseData[0].balanceAfterPeriod.amount = respData.balanceAfterPeriod.amount;
                                    // allResponseData[0].balanceBeforePeriod.amount += respData.balanceBeforePeriod.amount;
                                    allResponseData[0].credits.amount += respData.credits.amount;
                                    allResponseData[0].debits.amount += respData.debits.amount;

                                    for (var i = 0; i < respData.transactions.length; i++) {
                                        allResponseData[0].transactions.push(respData.transactions[i])
                                    }
                                    if (respData.nextOffset == undefined) {
                                        nextOff = undefined
                                        allData.push(allResponseData);
                                        deferred.resolve(allData);
                                    } else {
                                        nextOff = respData.nextOffset;
                                        cardProgramTranasactionService()
                                    }
                                }, function(error) {
                                    if (error.status === 401) {
                                        Util.renewAuth();
                                    }
                                })
                            }
                            cardProgramTranasactionService()
                        } else {
                            allData.push(allResponseData);
                            deferred.resolve(allData);
                        }
                    }, function(error) {
                        if (error.status === 401) {
                            Util.renewAuth();
                        }
                    })
                }                   
            },
            function(error) {
                if (error.status === 401) {
                    Util.renewAuth();
                }
                deferred.reject(error);
                return respError;
            });
        return deferred.promise();
    },
    getCardEligibility: function(corpId, cardId){
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_CARD_ELIGIBILITY
            .replace(":authToken:", "")
            .replace(":corpID:", corpId)
            .replace(":cardID:", cardId);
        ServiceConnector.get(url).then(function(responseJson){
            deferred.resolve(responseJson);
        }, function(error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject(error);
            return respError;
        });

        return deferred.promise();
    },
    getBillsForCardProgram: function(request, url){
        let deferred = $.Deferred();
        let header = {}
        ServiceConnector.post(request, url, header).then(function(responseJson){
            deferred.resolve(responseJson);
        }, function(error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject(error);
            return respError;
        });

        return deferred.promise();
    },
    getTransactionsForCardId: function(request, url){
        let deferred = $.Deferred();
        let header = {}
        ServiceConnector.post(request, url, header).then(function(responseJson){
            deferred.resolve(responseJson);
        }, function(error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject(error);
            return respError;
        });

        return deferred.promise();
    },
    getEmployeeImage: function(imageUrl){
        let deferred = $.Deferred();
        let url = '/baseimage?url='+imageUrl;
        ServiceConnector.get(url).then(function(responseJson){
            deferred.resolve(responseJson);
        }, function(error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject(error);
            return respError;
        });

        return deferred.promise();
    },
    getCardProgramDetails: function(url){
        let inputObj = {};
        let headers = {
            "Content-Type": "application/json"
        };
        return ServiceConnector.get(url, inputObj, headers);
    },
    getEmployeeStatement: function(params){
        let deferred = $.Deferred();
        let url = '/employeestatement?params='+JSON.stringify(params);
        ServiceConnector.get(url, {}).then(function(responseJson){
            console.log('es_response',responseJson);
            deferred.resolve(responseJson);
        }, function(error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject(error);
            return error;
        });

        return deferred.promise();
    },
    getAssestUploadEndPoint: function(authToken, corpID){
        let deferred = $.Deferred();
        let url = Urls.getOMSApiUrl().GET_ASSET_UPLOAD_ENDPOINT; 
        let header = {
            "Content-Type": "application/json"
        };
        ServiceConnector.get(url, header).then(function(response){
            deferred.resolve(response);
        }, function(error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject(error);
            return error;
        });

        return deferred.promise();
    },
    getSignedDocument: function(documentUrl){
        let deferred = $.Deferred();
        let url = '/signPdf' + '?url=' + documentUrl;
        ServiceConnector.get(url, {}).then(function(response){
            deferred.resolve(response);
        }, function(error) {
            if (error.status === 401) {
                Util.renewAuth();
            }
            deferred.reject(error);
            return error;
        });

        return deferred.promise();
    }
}

import DropdownTemplate from "../../template/widgets/dropdown.ejs";

class MultipleDropdown {
    constructor(element, options) {
        this.element = element;
        this.data = options.data;
        this.options = options;
        this.selectedItem = this.data[0];
        this.label = this.options.label || 'Select';

        this.data = _.map(this.data, function (item) {
            item.checked = false;
            return item;
        });


        this.render();
        this.setSelectedLabel();
    }

    render() {
        const renderTemplate = _.template(DropdownTemplate);
        $(this.element).html(renderTemplate({
            items: this.data,
            isMultiple: this.options.isMultiple,
            label: this.label
        }));
        // this.setSelectedValue(this.data[0].label);
        this.registerDomEvents();
    }

    registerDomEvents() {
        $(this.element).find('.dropdown--item > input').on('click', (e) => {
            this.setChecked($(e.target).attr('data-index'), $(e.target).is(':checked'));

            if (!this.options.isMultiple) {
                $(this.element).find('.dropdown--list').removeClass('dropdown--list--active');
            }
        });

        $(this.element).find('.dropdown--picker').on('click', () => {
            $(this.element).find('.dropdown--list').addClass('dropdown--list--active');
        });

        $(document).on('click', (e) => {
            if ($(e.target).closest(this.element).length === 0) {
                $(this.element).find('.dropdown--list').removeClass('dropdown--list--active');
            }
        });
    }

    setChecked(index, status) {
        if (this.options.isMultiple) {
            this.data[index].checked = status;
        }
        else {
            this.selectedItem = this.data[index];
        }
        this.triggerOnChange();
    }

    reset(data) {
        this.data = data;
        this.render();
        this.selectedItem = data[0];
        this.setSelectedLabel();
    }

    triggerSingleItem(item) {
        this.selectedItem = item;
        if (this.options.onChange) {
            this.options.onChange(item.value);
        }
    }

    getValue() {
        if (this.options.isMultiple) {
            let selectedArray = _.filter(this.data, function (item) {
                return item.checked == true;
            });

            selectedArray = _.map(selectedArray, function (item) {
                return item.value;
            });

            return selectedArray;
        }
        else {
            if (this.selectedItem && this.selectedItem.value) {
                return this.selectedItem.value;
            }
        }
        return null;
    }

    setSelectedLabel() {
        let selectedDom = $(this.element).find('.dropdown--label');
        if (this.options.isMultiple) {
            let selectedArray = _.filter(this.data, function (item) {
                return item.checked == true;
            });

            if (selectedArray.length == 0) {
                selectedDom.html(this.options.defaultLabel);
            }
            else if (selectedArray.length == 1) {
                selectedDom.html(selectedArray[0].label);
            }
            else if (selectedArray.length == this.data.length) {
                selectedDom.html(this.options.allSelectedLabel);
            }
            else {
                selectedDom.html(`${selectedArray.length} ${this.options.pluralLabel}`)
            }
        }
        else {
            if (this.selectedItem) {
                selectedDom.html(this.selectedItem.label);
            }
        }
    }

    triggerOnChange() {
        this.setSelectedLabel();
        if (this.options.onChange) {
            if (this.options.isMultiple) {
                let selectedArray = _.filter(this.data, function (item) {
                    return item.checked == true;
                });

                selectedArray = _.map(selectedArray, function (item) {
                   
                    return item.value;
                });

                this.options.onChange(selectedArray);
            }
            else {
                this.options.onChange(this.selectedItem.value);
            }
        }
    }
}

export default MultipleDropdown;

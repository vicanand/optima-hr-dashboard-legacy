import DropdownTemplate from "../../template/widgets/dropdown.ejs";

export default {
  init: function () {
    $.widget('custom.dropdown', {

      options: {
        label: 'Select',
        defaultSelected: 'All',
      },

      selectedItems: function () {
        return this.getSelectedItems().map(item => item.value);
      },

      _create: function () {
        this.arrayElements = [];

        this.element.find('option').each((element, item) => {
          this.arrayElements.push({
            label: $(item).html(),
            value: $(item).val(),
            checked: $(item).attr('selected') == 'selected'
          });

        });

        const template = _.template(DropdownTemplate);
        let newElement = $(template({
          items: this.arrayElements,
          label: this.options.label,
          selected: this.options.defaultSelected
        }));

        this.newElement = newElement;
        this.element.replaceWith(newElement);

        newElement.find('.dropdown--picker').click(function () {
          newElement.find('.dropdown--list').addClass('dropdown--list--active');
        });

        $(document).on('click', function (e) {
          if ($(e.target).closest(newElement).length === 0) {
            newElement.find('.dropdown--list').removeClass('dropdown--list--active');
          }
        });


        newElement.find('.dropdown--item input').click((element) => {
          const itemIndex = $(element.target).attr('data-index');
          this.arrayElements[itemIndex].checked = $(element.target).is(":checked");
          this.setSelectedValue();
          this._trigger('onSelected', null, [this.getSelectedItems().map(item => item.value)]);
        });
      },


      setSelectedValue: function () {
        if (this.getSelectedItems().length == 0 || this.getSelectedItems().length == this.arrayElements.length) {
          this.newElement.find('.dropdown--value').html(this.options.defaultSelected);
        }
        else if (this.getSelectedItems().length == 1) {
          this.newElement.find('.dropdown--value').html(this.getSelectedItems()[0].label);
        }
        else {
          this.newElement.find('.dropdown--value').html(this.getSelectedItems().length + ' ' + this.options.multipleItemLabel);
        }
      },

      getSelectedItems: function () {
        let filteredElements = this.arrayElements.filter(function (item) {
          return item.checked == true;
        });
        return filteredElements;
      }
    });

  }
};

import MasterTable from '../../template/tables/express/masterTable.ejs';
import Component from '../controller/express/component';
import Util from '../common/util';

class Table extends Component{
    constructor(element, options, extraParams = {}) {
        super();
        this.element = element;
        this.options = {
            source: null,
            pageSize: 10,
            pageNumber: 1,
        };

        _.forEach(options, (item, key) => {
            this.options[key] = item;
        });


        this.totalRecords = 0;
        this.isFirstTimeLoaded = true;
        this.currentPage = this.options.pageNumber;
        this.totalPages = 0;
        this.isLoading = false;
        this.extraParams = extraParams;
        this.tableRecords = [];


        this.dividePieces();
        this.renderMaster();
        this.renderHeaders();
        this.renderRows();
        this.loadData();
        this.renderLoading();
        this.registerDomEvents();
    }


    registerDomEvents() {
        $(this.element).find('.reload--buttom').on('click', () => {
            $(this.element).find('.table--footer').addClass('table--footer__active');
            this.loadData();
        });
    }

    triggerRefresh(params = {}) {

        _.forEach(params, (param, key) => {
            this.extraParams[key] = param;
        });

        this.resetTable();
        this.loadData();
    }


    loadData() {
        let requestJson = {
            pageNumber: this.currentPage,
            pageSize: this.options.pageSize,
            extraParams: this.extraParams
        };

        this.isLoading = true;
        this.renderLoading();
        if (this.options.onloadStarted) {
            this.options.onloadStarted();
        }

        this.hideEmptyState();
        this.hideErrorState();


        if (this.tableRecords.length == 0) {
            $(this.element).find('.table--footer').addClass('table--footer__active');
        }


        this.options.source(requestJson).then((recordsData) => {

            if (this.options.onLoadCompleted) {
                this.options.onLoadCompleted(recordsData);
            }

            if (recordsData.records.length > 0) {
                this.tableRecords = recordsData.records;
                this.renderRows(recordsData.records);
                if (this.isFirstTimeLoaded) {
                    this.totalRecords = recordsData.totalRecords;
                    this.totalPages = Math.ceil(this.totalRecords / this.options.pageSize);
                    this.isFirstTimeLoaded = false;
                }

                this.renderPaging();
            }
            else {
                $(this.element).find('tbody').html('');
                this.showEmptyState();
            }


            this.isLoading = false;
            this.hideLoader();

            $(this.element).find('.table--footer').removeClass('table--footer__active');
        }, (error) => {
            if (this.options.onLoadFailed) {
                this.options.onLoadFailed(error);
            }
            this.isLoading = false;
            this.hideLoader();
            $(this.element).find('.table--footer').removeClass('table--footer__active');
            this.showErrorState();
            this.tableRecords = [];
        });
    }


    showEmptyState() {
        $(this.element).find('.table--empty--state').addClass('table--empty--state__active');
    }

    hideEmptyState() {
        $(this.element).find('.table--empty--state').removeClass('table--empty--state__active');
    }

    showErrorState() {
        $(this.element).find('tbody').html('');
        $(this.element).find('.table--error--state').addClass('table--error--state__active');
    }

    hideErrorState() {
        $(this.element).find('.table--error--state').removeClass('table--error--state__active');
    }

    renderLoading() {
        let templateObj = $(this.options.tableTemplate);
        const columnCount = templateObj.find('thead > tr > th').length;
        let columns = '';
        let rows = '';

        for (let i = 0; i < columnCount; i++) {
            columns += `
                <td></td>
            `;
        }

        for (let i = 0; i < 3; i++) {
            rows += `
                <tr>${columns}</tr>
            `;
        }

        $(this.element).find('.table--footer').html(rows);
    }

    renderPaging() {

        if (this.totalPages > 1) {
            let pageRows = [];

            let startIndex = 1,
                totalIndex = this.totalPages;

            if (this.totalPages > 5) {

                if (this.currentPage == this.totalPages) {
                    totalIndex = this.totalPages;
                    startIndex = totalIndex - 4;
                }
                else if (this.currentPage == this.totalPages - 1) {
                    totalIndex = this.totalPages;
                    startIndex = totalIndex - 4;
                }
                else if (this.currentPage > 3) {
                    startIndex = this.currentPage - 2;
                    totalIndex = this.currentPage + 2;
                }
                else {
                    totalIndex = 5;
                }

            }


            for (let i = startIndex; i <= totalIndex; i++) {
                let linkClass = '';
                if (i == this.currentPage) {
                    linkClass = 'paging--link__active';
                }
                pageRows.push(`
        <li><a class="paging--link ${linkClass}" href="javascript:void(0)" data-page="${i}">${i}</a></li>
      `);
            }

            const pagingTemplate = `
       <ul class="paging--list">
        <li><a class="paging--nav paging--prev" href="javascript:void(0)"></a></li>
        ${pageRows.join('')}
        <li><a class="paging--nav paging--next" href="javascript:void(0)"></a></li>
      </ul>
    `;

            $(this.element).find('.express--paging').html(pagingTemplate);


            $(this.element).find('.paging--next').click(() => {
                if (this.currentPage < this.totalPages) {
                    this.currentPage++;
                    this.changePage();
                }
            });

            $(this.element).find('.paging--prev').click(() => {
                if (this.currentPage > 1) {
                    this.currentPage--;
                    this.changePage();
                }
            });

            $(this.element).find('.paging--link').click((e) => {
                this.currentPage = Number($(e.target).attr('data-page'));
                this.changePage();
            });
        }
        else {
            $(this.element).find('.express--paging').html("");
        }

    }

    changePage() {
        if (!this.isLoading) {
            this.loadData();
            this.renderPaging();
        }
    }

    renderMaster() {
        const template = _.template(MasterTable);
        $(this.element).html(template());
    }

    renderHeaders() {
        $(this.element).find('thead > tr').html(this.tableHeadColumns);
    }

    renderRows(data) {
        let templateData = _.map(data, (item) => {
            return this.tableBodyColumnsTemplate(item);
        });

        $(this.element).find('tbody').html(templateData.join(''));
    }



    dividePieces() {
        let templateObj = $(this.options.tableTemplate);
        this.tableHeadColumns = templateObj.find('thead > tr').html();
        this.tableBodyColumnsTemplate = _.template(_.unescape(templateObj.find('tbody').html()), {
            imports: {
                getInitialFromTheName: this.getInitialFromTheName,
                formatINR: this.formatINRwithDecimal,
                productsInUse: this.productsInUse,
                htmlDecoder: Util.htmlDecoder
                
            }
        });
    }

    getInitialFromTheName(name) {
        let itemInitials;
        let itemNames = name;
        let itemNameArr = itemNames.split(' ');
        let itemInitial1 = itemNameArr[0][0];

        itemInitials = itemInitial1;
        if (itemNameArr[1]) {
            let itemInitial2 = itemNameArr[1][0];
            itemInitials += itemInitial2;
        }

        return itemInitials;
    }



    showLoader() {
        $(this.element).find('.express--loading').addClass('express--loading__active');
    }

    hideLoader() {
        $(this.element).find('.express--loading').removeClass('express--loading__active');
    }

    resetTable() {
        this.isFirstTimeLoaded = true;
        this.currentPage = this.options.pageNumber;
        this.isLoading = false;
    }
}

export default Table;

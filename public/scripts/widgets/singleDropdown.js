import Template from '../../template/section/express/masterSingleDropdown.ejs';

class Dropdown {
    constructor(element, options) {
        this.element = element;
        this.data = options.data;
        this.callback = options.onSelected;
        this.render();
    }

    render() {
        const renderTemplate = _.template(Template);
        $(this.element).html(renderTemplate({list: this.data}));
        this.setSelectedValue(this.data[0].label);
        this.selectedItem = this.data[0];
        this.registerDomEvents();
    }


    registerDomEvents() {
        $(this.element).find('.single-dropdown__list__link').on('click', (e) => {
            const itemIndex = $(e.currentTarget).attr('data-index'),
                selectedItem = this.data[itemIndex];
            this.selectedItem = selectedItem;
            this.setSelectedValue(selectedItem.label);
            this.callback(selectedItem.value);
            $(this.element).find('.single-dropdown__list').removeClass('single-dropdown__list--active');
        });

        $(this.element).find('.single-dropdown__link').on('click', (e) => {
            $(this.element).find('.single-dropdown__list').addClass('single-dropdown__list--active');
        });


        $(document).on('click', (e) => {
            if ($(e.target).closest(this.element).length === 0) {
                $(this.element).find('.single-dropdown__list').removeClass('single-dropdown__list--active');
            }
        });
    }

    getValue() {
        return this.selectedItem.value;
    }

    setSelectedValue(label) {
        $(this.element).find('.single-dropdown__link').html(label);
    }
};

export default Dropdown;

let $renderingElement;
let options;

let _search = function() {
    let searchTxt = $(this).val().toLowerCase();
    $('.option-list li').each(function(k, v) {
        if (k != 0) {
            if ($(v).find('label').text().toLowerCase().indexOf(searchTxt) == -1) {
                $(v).hide();
            } else {
                $(v).show();
            }
        }
    })
};

let _optionSelected = function(event) {
    let selectedCount = $('li.option-item input:checked').length;
    $('.total-selected span').html(selectedCount);
    if (selectedCount == event.data.length) {
        $('#select_all').prop('checked', true);
    } else {
        $('#select_all').prop('checked', false);
    }
};

let _selectAll = function(event) {
    if ($(this).is(':checked')) {
        $('li.option-item input').prop('checked', true);
        _optionSelected(event);
    } else {
        _clearAll(event);
    }
};

let _clearAll = function(event) {
    console.log('df');
    $('li.option-item input').prop('checked', false);
    $('#select_all').prop('checked', false);
    _optionSelected(event);
};

let _apply = function(event) {
    let selectedCount = $('li.option-item input:checked').length;
    if (selectedCount > 1) {
        $('.filter-ctrl').html(selectedCount + ' ' + options.filterLabel + ' Selected');
    } else {
        $('.filter-ctrl').html($('li.option-item input:checked').val());
    }
    $('.filter-options-cntr').hide();

    console.log("Called");
};

let _optionClicked = function(event) {
    $('.filter-ctrl').html($(this).find('label').html());
    $($renderingElement).parent().find('.filter-options-cntr').hide();
}

let _registerDomEvent = function() {
    $($renderingElement).parent().find('.search-box').on('keyup', _search);
    $($renderingElement).parent().find('li.option-item input').on('change', options.source, _optionSelected);
    $('#select_all').on('change', options.source, _selectAll);
    $('.clear-all').on('click', options.source, _clearAll);
    $('.apply').on('click', this, _apply);
    if (!options.isMultiple) {
        $($renderingElement).parent().find('li.option-item').on('click', this, _optionClicked);
    }
    $($renderingElement).click(function() {
        console.log("Called");
        $('.filter-options-cntr').show();
    });
};

let selectedVal = function() {
    return ($('li.option-item input:checked').map(function() {
        return this.value;
    }));
};


let init = function() {
    $.widget('custom.filterDropdown', {
        _create: function() {
            $renderingElement = $(this.element).parent();
            options = this.options;
            let _self = this;
            // Grab the template
            $.get('/template/filter-template.ejs', function(template) {
                // Compile the EJS template.
                var func = ejs.compile(template);
                var html = func(options);


                $renderingElement.find('.filter-options-cntr').html(html);
                $renderingElement.find('.filter-options-cntr').hide();

                if (!options.isMultiple) {
                    $renderingElement.find('.multiselect-options').remove();
                    $renderingElement.find('#select_all').parents('li').remove();
                }
                _registerDomEvent();
            });

        }
    });
}

export
default {
    init: init
}

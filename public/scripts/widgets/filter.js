import Template from "../../template/section/filter-template.ejs"

export default {
    init: function() {
        $.widget("custom.filterDropdown", {
            _create: function() {
                var _this = this;

                this._renderFilter();
            },


            _renderFilter: function() {
                this.element.hide();
                var templateObj = ejs.compile(Template);
                $(templateObj()).insertAfter(this.element);


                this._focusable(this.element.siblings(".filter-widget"));
            }

        });
    }
}

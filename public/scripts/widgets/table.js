import Util from '../common/util';

let _renderTable = function($renderingElement, options, reset) {
    $renderingElement.find(".custom-table").show();
    $renderingElement.find(".loading-content").show();
    $renderingElement.find(".main-content").hide();
    $renderingElement.find(".table-empty-state").hide();
    $renderingElement.find(".table-error-state").hide();
    let searchVal = options.searchSelector ? options.searchSelector.val() : '',
    filterVal = options.filterSelect ? options.filterSelect.val() : '';
    let requestJson = {
        pageNumber: options.pageNumber,
        pageSize: options.pageSize,
        searchQuery: searchVal,
        filterValue: filterVal,
        secondaryFilterValue: options.secondaryFilterValue
    };
    let dataPromise = options.source;
    if (options.extraParam != undefined) {
        for (var key in options.extraParam) {
            requestJson[key] = options.extraParam[key];
        }
    }
    if (typeof dataPromise == 'function') {
        dataPromise(requestJson).then(function(respData) {
            _processData($renderingElement, options, respData, reset);
        }, function(error) {
            $renderingElement.find(".loading-content").hide();
            $renderingElement.find(".paging-container").hide();
            $renderingElement.find(".table-error-state").show();
        });
    } else {
        _processData($renderingElement, options, dataPromise, reset);
    }
};

let _processData = function($renderingElement, options, respData, reset) {
    for (let currIndex = 0; currIndex < respData.records.length; currIndex++) {
        respData.records[currIndex].balanceAmount = Util.formatINR(respData.records[currIndex].balanceAmount);
    }
    let templateArray = respData.records.map(function(item) {
        return options.emptyRow(item);
    });
    $renderingElement.find(".main-content").html(templateArray.join(""));
    $renderingElement.find(".loading-content").hide();
    if (respData.records.length === 0) {
        $renderingElement.find(".table-empty-state").show();
    } else {
        $renderingElement.find(".main-content").show();
    }

    if (reset) {
        let totalPages = 0;
        if(respData.totalCount % options.pageSize == 0){
            totalPages = respData.totalCount / options.pageSize;
        }
        else{
            totalPages = parseInt(respData.totalCount / options.pageSize) + 1;
        }
        options["totalPages"] = totalPages;
        $.get("/template/tables/pagination-template.ejs", function(template) {
            let pagingTemplate = ejs.compile(template);
            $renderingElement.find(".paging-container").html(pagingTemplate({
                "totalPages": totalPages
            }));

            _setPageNumberActive($renderingElement, options, 1);
        });

        if (respData.totalCount > options.pageSize) {
            $renderingElement.find(".paging-container").show();
        } else {
            $renderingElement.find(".paging-container").hide();
        }
    } else {
        _setPageNumberActive($renderingElement, options, options.pageNumber);
    }

    if (options.payoutFilter != undefined) {
        if ($('#totalPayouts').parent().hasClass('active')) {
            $('#totalPayouts').html(respData.totalPayouts);
            $('#scheduledPayouts').html(respData.totalScheduledPayouts);
            $('#pendingPayouts').html(respData.totalPendingPayouts);
            $('#transferredPayouts').html(respData.totalTransferredPayouts);
            $('#failedPayouts').html(respData.totalFailedPayouts);
        }
    }

}

/*let _search = function($renderingElement, options) {
    if (options.payoutFilter != undefined) {
        options.payoutFilter.find('.filter-wrpr').removeClass('active');
        $('#totalPayouts').parent().addClass('active');
    }
    searchQuery = $(options.searchSelector).val();
    _renderTable($renderingElement, options, true);
};*/

let _filterSearch = function($renderingElement, options, filterBy) {
    options.extraParam.statusFilters = filterBy;
    _renderTable($renderingElement, options, true);
};

let _dateSearch = function($renderingElement, options) {
    if (options.payoutFilter != undefined) {
        options.payoutFilter.find('.filter-wrpr').removeClass('active');
        $('#totalPayouts').parent().addClass('active');
        statusFilters = '';
    }
    let trnaferPerioDate = $('#tranferPeriod').val().split('-');
    options.dateSearch.val(Util.monthtoMonthName(options.dateSearch.val()));
    options.extraParam.startDate = new Date(trnaferPerioDate[1] + '/' + trnaferPerioDate[0] + '/' + '01').getTime();
    options.extraParam.endDate = new Date(Util.endDateForTrnasfer(trnaferPerioDate[1], trnaferPerioDate[0])).getTime();
    searchQuery = $(options.searchSelector).val();
    _renderTable($renderingElement, options, true);
};

let _dropDown = function($renderingElement, options) {
    options.filterValue = $(options.filterSelect).val();
    _renderTable($renderingElement, options, true);
};

let _secondDropDown = function($renderingElement, options) {
    options.secondaryFilterValue = $(this).val();
    _renderTable($renderingElement, options, true);
};

let _registerDomEvents = function($renderingElement, options) {
    $renderingElement.on("click", ".paging-item", function() {
        pagingClick(this, $renderingElement, options);
    });
    $renderingElement.on("click", ".go-prev", function() {
        _goPrevPage($renderingElement, options);
    });
    $renderingElement.on("click", ".go-next", function() {
        _goNextPage($renderingElement, options);
    });
    $renderingElement.on("click", ".reload-btn", function() {
        _reloadData($renderingElement, options);
    });

    $renderingElement.on("click", ".go-last", function() {
        _goLastPage($renderingElement, options);
    });

    $renderingElement.on("click", ".go-first", function() {
        _goFirstPage($renderingElement, options);
    });

    /*if (options.searchSelector != undefined) {
        options.searchSelector.keyup(function(e) {
            if ($(e.target).val() == '') {
                _search();
            }
            if (e.keyCode == 13) {
                _search();
            }
        });
    }*/

    if (options.dateSearch != undefined) {
        options.dateSearch.change(function(e) {
            _dateSearch();
        });
    }

    if (options.benfitFilterItems != undefined) {
        options.benfitFilterItems.find('.filter-wrpr').click(function(e) {
            options.benfitFilterItems.find('.filter-wrpr').removeClass('active');
            $(this).addClass('active');
            let totalNoOfRows = parseInt($(this).find('.filter-no').html());
            if (totalNoOfRows == 0) {
                if ($renderingElement.find(".main-content").find('tr').length == 0) {
                    $renderingElement.find(".table-error-state").hide();
                } else {
                    $renderingElement.find(".main-content").hide();
                }
                $renderingElement.find(".table-empty-state").show();
                $renderingElement.find(".pagination-container").hide();
            } else {
                if ($renderingElement.find(".main-content").find('tr').length == 0) {
                    $renderingElement.find(".table-error-state").show();
                } else {
                    $renderingElement.find(".main-content").show();
                }
                $renderingElement.find(".table-empty-state").hide();
                $renderingElement.find(".pagination-container").show();
            }
        });
    }

    if (options.payoutFilter != undefined) {
        options.payoutFilter.find('.filter-wrpr').click(function(e) {
            options.payoutFilter.find('.filter-wrpr').removeClass('active');
            $(this).addClass('active');
            pageNumber = 0;
            _filterSearch($(this).data('filter-val'));
        });
    }


    /*if (options.searchButton != undefined) {
        options.searchButton.on("click", _search);

    }*/

    if (options.filterSelect != undefined) {
        options.filterSelect.on("change", function() { _dropDown($renderingElement, options) });
    }

    if (options.secondaryFilterSelect != undefined) {
        options.secondaryFilterSelect.on("change", function() {
            _secondDropDown($renderingElement, options);
        });
    }
};

let _reloadData = function($renderingElement, options) {
    _renderTable($renderingElement, options, true);
};

let _goPrevPage = function($renderingElement, options) {
    if (options.pageNumber != 1) {
        options.pageNumber--;
        _renderTable($renderingElement, options);
    }
};

let _goNextPage = function($renderingElement, options) {
    if (options.totalPages != options.pageNumber) {
        options.pageNumber++;
        _renderTable($renderingElement, options);
    }
};

let _goLastPage = function($renderingElement, options) {
        options.pageNumber = options.totalPages;
        _renderTable($renderingElement, options);
};

let _goFirstPage = function($renderingElement, options) {
     options.pageNumber = 1;
    _renderTable($renderingElement, options);
};

let _setPageNumberActive = function($renderingElement, options, pageNo) {
    options.pageNumber = pageNo;
    $renderingElement.find(".paging-item").removeClass("active");
    $renderingElement.find("[data-pageno='" + options.pageNumber + "']").addClass("active");
    let numbersForVisible = [];
    let totalPages = options.totalPages;
    let numbersToShow = 5;
    if(totalPages > 100){
        numbersToShow = 12
    } else if(totalPages > 50){
        numbersToShow = 10
    } else if(totalPages > 25){
        numbersToShow = 8
    }
    
    if (pageNo == 1 || pageNo == 2) {
        for(var i=1;i<=numbersToShow; i++){
            numbersForVisible.push(i);
        }
        
    } else if (pageNo == totalPages || pageNo == totalPages - 1) {
        numbersForVisible.push(totalPages);
        for(var i=1;i<numbersToShow; i++){
            numbersForVisible.push(totalPages-i);
        }
        
    } else {
        numbersForVisible.push(pageNo);
        for(var i=(Math.ceil(numbersToShow/2) - numbersToShow); i<= parseInt(numbersToShow/2); i++){
            if(i > 0){    
                numbersForVisible.push(Number(pageNo)+i);
            }
            if(i < 0){
                numbersForVisible.unshift(Number(pageNo)+i)
            }
        }
    }
    $renderingElement.find(".paging-item").hide();
    numbersForVisible.forEach(function(num) {
        $renderingElement.find("[data-pageno='" + num + "']").show();
    });
}

let pagingClick = function(ele, $renderingElement, options) {
    let pageNo = $(ele).attr("data-pageno");
    if (pageNo != options.pageNumber) {
        _setPageNumberActive($renderingElement, options, pageNo);
        _renderTable($renderingElement, options);
    }
}

let init = function() {
    $.widget('custom.table', {
        _create: function() {
            let $renderingElement = $(this.element);
            let _this = this;
            let dataPromise = _this.options.source;
            let options = this.options;
            _registerDomEvents($renderingElement, options);
            $.get(this.options.tableTemplate, function(template) {
                // Compile the EJS template.
                let tableTemplate = ejs.compile(template);
                $renderingElement.html(tableTemplate({
                    pageSize: _this.options.pageSize,
                    // pageNumber: _this.options.pageNumber
                    // pageNumbers:
                }));
                // console.log(_this);
                $.get(_this.options.rowTemplate, function(template) {
                    options.emptyRow = ejs.compile(template);
                    _renderTable($renderingElement, options, true);
                });
            });
        },
        _destroy: function() {
            // remove generated elements
            this.element.find("custom-table").remove();
        },
    });
}


export default {
    init: init
}

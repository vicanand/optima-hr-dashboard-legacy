import Util from '../common/util';

let emptyRow;
let $renderingElement;
let options;
let pageNumber;
let dataPromise;
let totalPages = 0;
let searchQuery = "";
let filterValue = "";
let secondaryFilterValue = "";
let startDate = "";
let endDate = "";
let statusFilters = "";

let _renderTable = function (reset) {
    $renderingElement.find(".custom-table").show();
    $renderingElement.find(".loading-content").show();
    $renderingElement.find(".main-content").hide();
    $renderingElement.find(".table-empty-state").hide();
    $renderingElement.find(".table-error-state").hide();
    let requestJson = {
        pageNumber: pageNumber || options.pageNumber,
        pageSize: options.pageSize,
        searchQuery: searchQuery,
        filterValue: filterValue || options.companyID || '',
        secondaryFilterValue: secondaryFilterValue || options.programID || '',
        startDate: startDate,
        endDate: endDate
    };

    if (options.extraParam != undefined) {
        for (var key in options.extraParam) {
            requestJson[key] = options.extraParam[key];
        }
    }
    if (typeof dataPromise == 'function') {
        dataPromise(requestJson).then(function (respData) {
            _processData(respData, reset);
        }, function (error) {
            $renderingElement.find(".loading-content").hide();
            $renderingElement.find(".paging-container").hide();
            $renderingElement.find(".table-error-state").show();
        });
    } else {
        _processData(dataPromise, reset);
    }
    $('#resetFilters').removeClass('disableReset');
    $('#resetFilter').show();
};

let _processData = function (respData, reset) {
    if (respData.totalCount % options.pageSize == 0) {
        totalPages = respData.totalCount / options.pageSize;
    } else {
        totalPages = parseInt(respData.totalCount / options.pageSize) + 1;
    }
    for (let currIndex = 0; currIndex < respData.records.length; currIndex++) {
        respData.records[currIndex].balanceAmount = Util.formatINR(respData.records[currIndex].balanceAmount);
    }
    let templateArray = respData.records.map(function (item) {
        return emptyRow(item);
    });
    $renderingElement.find(".main-content").html(templateArray.join(""));
    $renderingElement.find(".loading-content").hide();
    if (respData.records.length === 0) {
        $renderingElement.find(".table-empty-state").show();
    } else {
        $renderingElement.find(".main-content").show();
    }

    if (reset) {
        if (respData.totalCount % options.pageSize == 0) {
            totalPages = respData.totalCount / options.pageSize;
        } else {
            totalPages = parseInt(respData.totalCount / options.pageSize) + 1;
        }
        options["totalPages"] = totalPages;
        $.get("/template/tables/pagination-template.ejs", function (template) {
            let pagingTemplate = ejs.compile(template);
            $renderingElement.find(".paging-container").html(pagingTemplate({
                "totalPages": totalPages
            }));

            _setPageNumberActive(1);
        });

        if (respData.totalCount > options.pageSize) {
            $renderingElement.find(".paging-container").show();
        } else {
            $renderingElement.find(".paging-container").hide();
        }
    } else {
        _setPageNumberActive(pageNumber);
    }

    if (options.payoutFilter != undefined) {
        $('#totalPayouts').html(respData.totalPayouts);
        $('#scheduledPayouts').html(respData.totalScheduledPayouts);
        $('#transferredPayouts').html(respData.totalTransferredPayouts);
        $('#waitingPayouts').html(respData.totalWaitingPayouts);

        $('#processingPayouts').html(respData.totalProcessingPayouts);
        $('#successPayouts').html(respData.totalSuccessPayouts);
        $('#failedPayouts').html(respData.totalFailedPayouts);
        $('#revokedPayouts').html(respData.totalRevokedPayouts);

        $('#totalPayoutsAmount').html(respData.totalPayoutsBlnc);
        $('#processingPayoutsAmount').html(respData.totalProcessingPayoutsBlnc);
        $('#successPayoutsAmount').html(respData.totalSuccessPayoutsBlnc);
        $('#failedPayoutsAmount').html(respData.totalFailedPayoutsBlnc);
        $('#revokedPayoutsAmount').html(respData.totalRevokedPayoutsBlnc);
    }
}

let _search = function () {
    if (options.payoutFilter != undefined) {
        options.payoutFilter.find('.filter-wrpr').removeClass('active');
        $('#totalPayouts').parent().addClass('active');
    }
    searchQuery = $(options.searchSelector).val();
    _renderTable(true);
};

let _startAndEndDate = function () {
    let fullDate = $(options.startDate).val();
    if (fullDate == "") {
        startDate = "";
        endDate = "";
    } else {
        fullDate = fullDate.replace(/\//g, '-');
        startDate = fullDate.slice(0, 10);
        endDate = fullDate.slice(13, 23);
        if (startDate === endDate) {
            endDate = '';
        }
    }
    pageNumber = options.pageNumber || 0;
    _renderTable(true);
}

let _filterSearch = function (filterBy) {
    options.extraParam.statusFilters = filterBy;
    _renderTable(true);
};

let _dateSearch = function () {
    if (options.payoutFilter != undefined) {
        options.payoutFilter.find('.filter-wrpr').removeClass('active');
        $('#totalPayouts').parent().addClass('active');
        statusFilters = '';
    }
    let trnaferPerioDate = $('#tranferPeriod').val().split('-');
    options.dateSearch.val(Util.monthtoMonthName(options.dateSearch.val()));
    options.extraParam.startDate = new Date(trnaferPerioDate[1] + '/' + trnaferPerioDate[0] + '/' + '01').getTime();
    options.extraParam.endDate = new Date(Util.endDateForTrnasfer(trnaferPerioDate[1], trnaferPerioDate[0])).getTime();
    searchQuery = $(options.searchSelector).val();
    _renderTable(true);
};

let _dropDown = function () {
    filterValue = $(this).val();
    _renderTable(true);
};

let _secondDropDown = function () {
    secondaryFilterValue = $(this).val();
    _renderTable(true);
};

let _resetAllFilters = function () {
    searchQuery = "";
    startDate = "";
    endDate = "";
    filterValue = "";
    secondaryFilterValue = "";
    $('#searchBox').val("");
    $('#searchDisBox').val("");
    $('#transferOrderCode').val('');
    $('#transferOrderStatus').val('ALL');
    $('#transferDisOrderStatus').val('ALL');
    $('#tranferPeriod').val('');
    $('#tranferDisPeriod').val('');
    $('#tranferPeriodIssue').val('');
    $('#searchBoxIssue').val("");
    $('#transferOrderStatusIssue').val('ALL');
    _renderTable(true);
    $('#resetFilters').addClass('disableReset');
    $('#resetFilter').hide();
}

let _registerDomEvents = function () {
    $renderingElement.on("click", ".paging-item", pagingClick);
    $renderingElement.on("click", ".go-prev", _goPrevPage);
    $renderingElement.on("click", ".go-next", _goNextPage);
    $renderingElement.on("click", ".reload-btn", _reloadData);
    $renderingElement.on("click", ".go-last", _goLastPage);
    $renderingElement.on("click", ".go-first", _goFirstPage);

    if (options.searchSelector != undefined) {
        options.searchSelector.keyup(function (e) {
            if ($(e.target).val() == '') {
                _search();
            }
            if (e.keyCode == 13) {
                _search();
            }
        });
    }

    if (options.startDate != undefined) {
        options.startDate.on('change', function (e) {
            console.log($(e.target).val());
            if ($(e.target).val() != '') {
                _startAndEndDate();
            }
            if (e.keyCode == 13) {
                _startAndEndDate();
            }
            _renderTable(true);
        });
    }

    // if(options.applyDateInput != undefined) {
    //     options.applyDateInput.on('click', function(e){
    //         _startAndEndDate();
    //     });
    // }

    // if(options.clearDate != undefined){
    //     options.clearDate.on('click', function(e){
    //         // _clearStartAndEndDate();
    //         _startAndEndDate();
    //     });
    // } 


    if (options.dateSearch != undefined) {
        options.dateSearch.change(function (e) {
            _dateSearch();
        });
    }

    if (options.benfitFilterItems != undefined) {
        options.benfitFilterItems.find('.filter-wrpr').click(function (e) {
            options.benfitFilterItems.find('.filter-wrpr').removeClass('active');
            $(this).addClass('active');
            let totalNoOfRows = parseInt($(this).find('.filter-no').html());
            if (totalNoOfRows == 0) {
                if ($renderingElement.find(".main-content").find('tr').length == 0) {
                    $renderingElement.find(".table-error-state").hide();
                } else {
                    $renderingElement.find(".main-content").hide();
                }
                $renderingElement.find(".table-empty-state").show();
                $renderingElement.find(".pagination-container").hide();
            } else {
                if ($renderingElement.find(".main-content").find('tr').length == 0) {
                    $renderingElement.find(".table-error-state").show();
                } else {
                    $renderingElement.find(".main-content").show();
                }
                $renderingElement.find(".table-empty-state").hide();
                $renderingElement.find(".pagination-container").show();
            }
        });
    }

    if (options.payoutFilter != undefined) {
        options.payoutFilter.find('.filter-wrpr').click(function (e) {
            options.payoutFilter.find('.filter-wrpr').removeClass('active');
            $(this).addClass('active');
            pageNumber = 0;
            _filterSearch($(this).data('filter-val'));
        });
    }


    if (options.searchButton != undefined) {
        options.searchButton.on("click", _search);

    }


    if (window.location.pathname == '/beneficiaries') {
        filterValue = options.filterSelect.val();
    }

    if (options.filterSelect != undefined) {
        options.filterSelect.on("change", _dropDown);
    }

    if (options.secondaryFilterSelect != undefined) {
        options.secondaryFilterSelect.on("change", _secondDropDown);
    }
};

let _reloadData = function () {
    _renderTable(true);
};

let _goPrevPage = function () {
    if (pageNumber != 1) {
        pageNumber--;
        _renderTable();
    }
};

let _goNextPage = function () {
    if (totalPages != pageNumber) {
        pageNumber++;
        _renderTable();
    }
};

let _goLastPage = function() {
    pageNumber = totalPages;
    _renderTable();
};

let _goFirstPage = function() {
    pageNumber = 1;
    _renderTable();
};

let _setPageNumberActive = function (pageNo) {
    pageNumber = pageNo;
    $renderingElement.find(".paging-item").removeClass("active");
    $renderingElement.find("[data-pageno='" + pageNumber + "']").addClass("active");
    let numbersForVisible = [];
    let numbersToShow = 5;
    if(totalPages > 100){
        numbersToShow = 12
    } else if(totalPages > 50){
        numbersToShow = 10
    } else if(totalPages > 25){
        numbersToShow = 8
    }
    if (pageNo == 1 || pageNo == 2) {
        for(var i=1;i<=numbersToShow; i++){
            numbersForVisible.push(i);
        }
    } else if (pageNo == totalPages || pageNo == totalPages - 1) {
        numbersForVisible.push(totalPages);
        
        for(var i=1;i<numbersToShow; i++){
            numbersForVisible.push(totalPages-i);
        }
    } else {
        numbersForVisible.push(pageNo);
        
        for(var i=(Math.ceil(numbersToShow/2) - numbersToShow); i<= parseInt(numbersToShow/2); i++){
            if(i > 0){    
                numbersForVisible.push(Number(pageNo)+i);
            }
            if(i < 0){
                numbersForVisible.unshift(Number(pageNo)+i)
            }
        }

    }
    $renderingElement.find(".paging-item").hide();
    numbersForVisible.forEach(function (num) {
        $renderingElement.find("[data-pageno='" + num + "']").show();
    });
}

let pagingClick = function () {
    let pageNo = $(this).attr("data-pageno");
    if (pageNo != pageNumber) {
        _setPageNumberActive(pageNo);
        _renderTable();
    }
}

let init = function () {
    $.widget('custom.tablewidget', {
        _create: function () {
            $renderingElement = $(this.element);
            let _this = this;

            dataPromise = _this.options.source;
            options = this.options;
            _registerDomEvents();
            $.get(this.options.tableTemplate, function (template) {
                // Compile the EJS template.
                var tableTemplate = ejs.compile(template);
                $renderingElement.html(tableTemplate({
                    pageSize: _this.options.pageSize,
                    // pageNumber: _this.options.pageNumber
                    // pageNumbers:
                }));
                // console.log(_this);
                $.get(_this.options.rowTemplate, function (template) {
                    emptyRow = ejs.compile(template);
                    _renderTable(true);
                });
            });
        },
        _destroy: function () {
            // remove generated elements
            this.element.find("custom-table").remove();
            pageNumber = options.pageNumber || 0;
        },
    });
}


export default {
    init: init,
    resetAllFilters: _resetAllFilters
}
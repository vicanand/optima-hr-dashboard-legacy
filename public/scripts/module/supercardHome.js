import ServiceConnector from "../common/serviceConnector";
import Filter from '../widgets/filter';
import Table from '../widgets/table';
import TableWidget from '../widgets/table-widget';
import CardorderDOMEvent from '../dom-events/cardorderDOMEvent';
import Util from '../common/util';
import Storage from '../common/webStorage';
import daterangepicker from '../../../node_modules/bootstrap-daterangepicker/daterangepicker';
import moment from '../../lib/moment/moment';
import DomEventHandler from '../common/domEventHandler';
// import ellipsis from '../common/ellipsis';
import Meta from '../common/metaStorage';
import CardorderCtrl from '../controller/cardorderCtrl';
import CardorderSvc from '../service/cardorderSvc';
import FilterWidget from '../widgets/filter-widget';
import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';

(function () {
  HeaderController.init();
  SidebarController.init();
  CardorderCtrl.init();
  Table.init();
  TableWidget.init();
  Filter.init();
  FilterWidget.init();
  CardorderCtrl.initDatePicker();

  // var start = moment().subtract(29, 'days');
  // var end = moment();

  // function cb(start, end) {
  //   $('input[name="daterange"]').val('');
  // }
  // $('input[name="daterange"]').on('cancel.daterangepicker', function (ev, picker) {
  //   $(this).val('');
  // });

  // $('input[name="daterange"]').daterangepicker({
  //   "autoApply": true,
  //   locale: {
  //     format: 'YYYY-MM-DD',
  //     "customRangeLabel": "Choose From Calendar",
  //   },
  //   "opens": "left",
  //   maxDate: new Date(),
  //   startDate: start,
  //   endDate: end,
  //   ranges: {
  //     'Today': [moment(), moment()],
  //     'Yesterday': [moment().subtract(1, 'days'), moment().subtract(0, 'days')],
  //     'Last 7 Days': [moment().subtract(6, 'days'), moment()],
  //     'Last 30 Days': [moment().subtract(29, 'days'), moment()],
  //     'This Month': [moment().startOf('month'), moment().endOf('month')],
  //     'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  //   }
  // }, cb);
  // cb(start, end);

  // $("#transferDisbursedOrderTable").tablewidget({
  //   source: CardorderSvc.getdisbursedOrderList,
  //   pageSize: 10,
  //   pageNumber: 1,
  //   tableTemplate: "/template/section/supercard/tab-content/tables/transfer-disbursed-orders.ejs",
  //   rowTemplate: "/template/section/supercard/tab-content/tables/transfer-disbursed-orders-row.ejs",
  //   searchSelector: $("#searchDisBox"),
  //   searchButton: $("#searchDisBtn"),
  //   filterSelect: $("#transferDisOrderStatus"),
  //   startDate: $('#tranferDisPeriod')
  // });


  // $("#transferOrderTable").tablewidget({
  //   source: CardorderSvc.getcardOrderList,
  //   pageSize: 10,
  //   pageNumber: 1,
  //   tableTemplate: "/template/section/supercard/tab-content/tables/transfer-orders.ejs",
  //   rowTemplate: "/template/section/supercard/tab-content/tables/transfer-orders-row.ejs",
  //   searchSelector: $("#searchBox"),
  //   searchButton: $("#searchBtn"),
  //   filterSelect: $("#transferOrderStatus"),
  //   startDate: $('#tranferPeriod')
  // });




  setTimeout(() => {
    $('#resetFilter').hide();
  }, 1000);

  $('#uploadAgain').on('click', function () {
    Storage.set('duplicateManual', false);
    Storage.set('duplicateBulk', false);
    let type = $(this).data('type');
    $('#fileErrorWrpr').hide();
    if (type == 'bulk') {

      $('#card-selection-loader').modal('hide');
      // $('#errorVirtualGiftingTransfers').load('#fileErrorMain');
      $('#virtualGiftContainer').show();
      $('#chooseDesign').hide();
      $('#addBenefitsBulk').show();
      $('.dz-remove')[0].click();
      // $('.modal-backdrop').remove()
      // $('body').removeClass('modal-open')
    } else {
      $('#virtualGiftContainer').show();
      $('#chooseDesign').hide();
      $('#addBenefits').show('slow');
    }
  });

  $("select#fundingAccountID").change(function () {
    var newBalance = $('#fundingAccountID option:selected').attr('data-balance');
    var fundingaccountname = $('#fundingAccountID option:selected').attr('data-name');
    var id = $('#fundingAccountID option:selected').val();
    $('#balanceFund').html(newBalance);
    Storage.setSessionCollection('fundingaccountid', id);
    Storage.setSessionCollection('fundingaccountbalance', newBalance);
    Storage.setSessionCollection('fundingaccountname', fundingaccountname);
    $('#createOrderDone').attr('data-fundingaccountids', id);
    // if (newBalance < 1) {
    //   $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').show();
    //   $('.placeOrderEnd').hide();
    // } else {
    $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').hide();
    $('.placeOrderEnd').show();
    // }
  });

  function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], {
      type: "text/csv"
    });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
  }

  function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = $('#errorVirtualGiftingTransfers1')[0].querySelectorAll("table tr");

    for (var i = 0; i < rows.length; i++) {
      var row = [],
        cols = rows[i].querySelectorAll("td, th");

      for (var j = 0; j < cols.length; j++) {

        row.push('"' + cols[j].innerText.replace(/\n/g, ", ") + '"');
      }
      csv.push(row.join(","));
    }

    // Download CSV
    download_csv(csv.join("\n"), filename);
  }

  $('#downloadSGReport').on("click", function () {
    var html = $('#errorVirtualGiftingTransfers1')[0].innerHTML;
    export_table_to_csv(html, "error-report.csv");
  });

  $('body').on("click", '.cardAction', function () {
    var typeOfAction = $(this).attr('data-action');
    var superCardID = $(this).attr('data-superCardID');
    var email = $(this).attr('data-email');
    var type = $(this).attr('data-type');
    Storage.setSessionCollection('typeOfAction', typeOfAction);
    Storage.setSessionCollection('superCardID', superCardID);
    Storage.setSessionCollection('email', email);
    if (typeOfAction.toLowerCase() == 'reset_pin') {
      typeOfAction = "reset pin for";
    } else if (typeOfAction.toLowerCase() == "reissuance") {
      typeOfAction = "re-issue";
    }
    var text = `Are you sure you want to ${typeOfAction.toLowerCase()} this card ?`;
    $('.cardActionText').text(text);
    if (type == "Non Personalised" && typeOfAction == "ACTIVATE") {
      $('.email').show();
    }
    $('#actionConfirm').modal('show');

  });


  /// new shit
  (function () {
    $('.sub-container').hide();
    let currentUrl = window.location.href;
    if (currentUrl.indexOf('#') !== -1) {
      var selectedId = currentUrl.substr(currentUrl.lastIndexOf('#'), currentUrl.length);
    }
    $('.nav.nav-tabs > li > a').click(function (e) {
      var data = $(this).attr("id");
      var previousActive = localStorage.getItem('currentActive');
      localStorage.setItem('currentActive', data);
      if (history.pushState) {
        history.pushState(null, null, '#' + data);

      } else {
        window.location.hash = '#' + data;
      }
      if (previousActive !== localStorage.getItem('currentActive')) {
        $('.sub-container').show();
        renderTable(localStorage.getItem('currentActive'));
        destroyTable(previousActive);
      } else {
        renderTable(previousActive);
        // destroyTable(previousActive);
      }
    });
    window.location.hash == "" ? $('#cardOrder').parent().addClass('active') : '';
    $("document").ready(function () {
      if ($("#progress-top").length === 0) {
        $("body").append($("<div><b></b><i></i></div>").attr("id", "progress-top"));
        $("#progress-top").width("101%").delay(800).fadeOut(1000, function () {
          $(this).remove();
        });
      }
      $(selectedId).trigger('click');
      $('.tabContainer').css('display', 'block');
      if (!window.location.hash) {
        $('#overview').parent().addClass('active');
      }
    });
    setTimeout(function () {
      $('html,body').animate({
        scrollTop: 0
      }, 'fast');
      $('.sub-container').show();
    }, 350);

    DomEventHandler.bindClassEvent("resetFilter", "click", TableWidget.resetAllFilters);
  })();

  function destroyTable(prev) {
    try {
      switch (prev) {
        case 'orderHistory':
          $("#transferOrderTable").tablewidget('destroy');
          TableWidget.resetAllFilters();
          $('#searchBox').val('');
          $('#transferOrderStatus').val('ALL');
          $('#tranferPeriod').val('');
          break;
        case 'disbursedHistory':
          $("#transferDisbursedOrderTable").tablewidget('destroy');
          TableWidget.resetAllFilters();
          $('#searchDisBox').val('');
          $('#transferDisOrderStatus').val('ALL');
          $('#tranferDisPeriod').val('');
          break;
        default:
          break;
      }
    } catch (e) {
      //$('#cardOrder').trigger('click');
    }
  };

  function renderTable(currentHash) {
    switch (currentHash) {
      case 'orderHistory':
        $("#transferOrderTable").tablewidget({
          source: CardorderSvc.getcardOrderList,
          pageSize: 10,
          pageNumber: 1,
          tableTemplate: "/template/section/supercard/tab-content/tables/transfer-orders.ejs",
          rowTemplate: "/template/section/supercard/tab-content/tables/transfer-orders-row.ejs",
          searchSelector: $("#searchBox"),
          searchButton: $("#searchBtn"),
          filterSelect: $("#transferOrderStatus"),
          startDate: $('#tranferPeriod')
        });
        break;
      case 'disbursedHistory':
        $("#transferDisbursedOrderTable").tablewidget({
          source: CardorderSvc.getdisbursedOrderList,
          pageSize: 10,
          pageNumber: 1,
          tableTemplate: "/template/section/supercard/tab-content/tables/transfer-disbursed-orders.ejs",
          rowTemplate: "/template/section/supercard/tab-content/tables/transfer-disbursed-orders-row.ejs",
          searchSelector: $("#searchDisBox"),
          searchButton: $("#searchDisBtn"),
          filterSelect: $("#transferDisOrderStatus"),
          startDate: $('#tranferDisPeriod')
        });
        break;
      default:
        // $('#cardOrder').trigger('click');
        break;
    }
  }

  // new shit end



})();
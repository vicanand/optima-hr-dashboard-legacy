import Table from '../widgets/table';
import EmployeeProgramIndividualCardDetailsController from '../controller/employeeDetailsStatCtrl.js';

(function() {
    Table.init();
    EmployeeProgramIndividualCardDetailsController.init();
})();
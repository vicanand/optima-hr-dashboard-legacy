import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import BulkNewTransferController from '../controller/bulkNewTransferCtrl';

(function() {
    HeaderController.init();
    SidebarController.init();
    BulkNewTransferController.init();
})();

import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import DocumentController from '../controller/documentCtrl';
import TableWidget from '../widgets/table-widget';
import DocumentDomEvents from "../dom-events/documentDom-Events";
import moment from '../../lib/moment/moment';
import Utils from '../common/util';

(function() {
    HeaderController.init();
    SidebarController.init();
    DocumentController.init();
    TableWidget.init();

    $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('input[name="daterange"]').daterangepicker({
        "autoApply": true,
        locale: {
            format: 'DD MMM YYYY',
            "customRangeLabel": "Choose From Calendar",
          },
        "opens": "left",
        "startDate": '01 Apr 2020',
        "endDate": '31 Mar 2021',
        maxDate: '31 Mar 2021',
        ranges: {
            'Today': [moment(), moment()],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'FY 2018-19': ['01 Apr 2018', '31 Mar 2019'],
            'FY 2019-20': ['01 Apr 2019', '31 Mar 2020'],
            'FY 2020-21': ['01 Apr 2020', '31 Mar 2021']
        }     
    }, function(start, end) {
        console.log("A new date selection was made: " + start.format('DD MMM YYYY') + ' to ' + end.format('DD MMM YYYY'));
        let searchTxt = $('#searchDocs').val();
        let programType = $('#programType').val();
         let dateRange = start.format('DD MMM YYYY')+'-'+end.format('DD MMM YYYY');
         let statusType ='none';
         let status = 'none';
         let docSource ='none';
         if($('.all-status').is(':visible')){  
             statusType = $('#statusType').val();
             status = $("#statusType option:selected").text();
         } else {
             docSource = $('#documentType').val();
         }
         $('#docStoreList').empty();
         DocumentDomEvents.searchByDateRange(searchTxt, programType, statusType, dateRange, docSource);
      });
    
    
    $(".dropdown img.flag").addClass("flagvisibility");
    
                $(".dropdown dt").on('click', '#initial-allPrograms', function(event) {  //program-ul
                    event.preventDefault();
                    event.stopImmediatePropagation();
                    event.stopPropagation();
                    $(".dropdown dd ul").toggle();
                  //  alert($('.dropdown dd ul li .gradient').length);
                 //   alert($('.dropdown .gradient').length);
                 let option = '<li class="list gradient" style="height:16px;top: 15px;margin-left: 10px;margin-right: 10px;"></li>';
                    if($('.dropdown .gradient').length >1){
                        $(".program-ul").append(option);
                        DocumentDomEvents.getCompanyPrograms();
                    }
                });
                //$(".dropdown dd ul li a").click(function()
                
                $('.dropdown dd ul').on('click', '.list', function(event) {
                    event.preventDefault();
                    var text = $(this).html();
                    $(".dropdown dt a span").html(text);
                    $(".dropdown dd ul").hide();
                    DocumentDomEvents.searchDocsOnFilter();
                  //  $("#result").html("Selected value is: " + getSelectedValue("sample"));
                });
                            
                function getSelectedValue(id) {
                    return $("#" + id).find("dt a span.value").html();
                }
    
                $(document).bind('click', function(e) {
                    var $clicked = $(e.target);
                    if (! $clicked.parents().hasClass("dropdown"))
                        $(".dropdown dd ul").hide();
                });
    
    
                $("#flagSwitcher").click(function() {
                    $(".dropdown img.flag").toggleClass("flagvisibility");
                });
                // infinite scroll on program dropdown 
                // $(".program-ul").scroll(function() {   
                //     if($(".program-ul").scrollTop()  == $(".program-ul").height()) {
                //         alert("bottom!");
                //     }
                //  });
                 $(".program-ul").scroll(function (event) {
                    if(($('.dropdown .gradient').length == 0) || ($('.dropdown .gradient').length ==3)){
                    var $this = $(this);
                    var height = this.scrollHeight - $this.height(); // Get the height of the div
                    var scroll = $this.scrollTop(); // Get the vertical scroll position

                    var isScrolledToEnd = (scroll+4 >= height);

                    $(".scroll-pos").text(scroll);
                    $(".scroll-height").text(height);

                    if (isScrolledToEnd) {
                        localStorage.setItem('flag', 0);
                        let scrollFlag =0;
                        if(localStorage.getItem('scrollFlag')){
                            scrollFlag= parseInt(localStorage.getItem('scrollFlag'));
                        }
                        if(scrollFlag == 0){
                            var additionalContent = GetMoreContent(); // Get the additional content
                            $this.append(additionalContent); // Append the additional content                         
                        } 
                        // if(typeof additionalContent !== "undefined")  {
                        //     localStorage.setItem('scrollFlag', 0);  
                        //   } 
                    }
                }
                });
                function GetMoreContent() {
                  DocumentDomEvents.getCompanyPrograms();
                }
                
                // for image slider.
                var checkitem = Utils.imageCarousel;
    checkitem();
    $("#bills-carousel").on("slid.bs.carousel", "", checkitem);

})();

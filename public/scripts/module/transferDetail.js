import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import TransferDetail from "../controller/transferDetailCtrl";
import TableWidget from '../widgets/table-widget';
import FilterWidget from '../widgets/filter-widget';
import Meta from '../common/metaStorage';


(function() {
var newString = location.href;
if(newString.search("spotlight") > 0){
    $('#programID').val(location.href.split('spotlight/')[1].split('/')[0]);
}
else{
    $('#programID').val(location.href.split('optima/')[1].split('/')[0]);
}
   
    TableWidget.init();
    HeaderController.init();
    SidebarController.init();
    TransferDetail.init();
    FilterWidget.init();



    initGAEvent('event', 'Single Order Details', 'click', 'Single Order Details, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+',timestamp=' + new Date().toLocaleString());


    // $('#criteriaFilter').filterDropdown({
    //     'isMultiple' : false,
    //     'source' : ['Atmos Energy Corporation','Avaya Inc.','BellSouth Corporation','Boston Scientific Corporation','Bemis Company, Inc.','Cisco Systems Inc.','Capital One Financial Corp.'],
    //     'filterLabel' : 'Companies',
    //     'selected' : ''
    // });
}());

import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import Table from '../widgets/table';
import CardProgramIndividualBeneficiaryController from '../controller/cardProgramIndividualBeneficiaryCtrl.js';

(function() {
    HeaderController.init();
    SidebarController.init();
    Table.init();
    CardProgramIndividualBeneficiaryController.init();
})();

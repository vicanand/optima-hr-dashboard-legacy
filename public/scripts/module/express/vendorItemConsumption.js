import MenuFilters from '../../controller/express/menuFilters';
import VendorDropDown from '../../controller/express/vendorDropdownCtrl';
import ItemsConsumption from '../../controller/express/itemsConsumption';

(function () {

    let itemConsumption, isFirstLoaded = true;
    new VendorDropDown();

    new MenuFilters(null, {
        onChange: (filters) => {
            if (isFirstLoaded) {
                isFirstLoaded = false;
                itemConsumption = new ItemsConsumption('#allItemsConsumption', {filters: filters});
            }
            else {
                itemConsumption.triggerRefresh(filters);
            }
        }
    });


}());
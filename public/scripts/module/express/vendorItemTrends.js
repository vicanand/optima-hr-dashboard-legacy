import MenuFilters from '../../controller/express/menuFilters';
import VendorDropDown from '../../controller/express/vendorDropdownCtrl';
import TopTenItemTrends from '../../controller/express/topTenItemTrends';

(function () {

    let itemTrends, isFirstLoaded = true;
    new VendorDropDown();

    new MenuFilters(null, {
        onChange: (filters) => {
            if (isFirstLoaded) {
                isFirstLoaded = false;
                itemTrends = new TopTenItemTrends('#topTenItemTrends', {filters: filters});
            }
            else {
                itemTrends.triggerRefresh(filters);
            }
        }
    });


}());
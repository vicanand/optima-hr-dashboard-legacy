import AllVendors from '../../controller/express/allVendors';
import VendorsFilter from '../../controller/express/vendorsFilters';

(function () {
    let vendorslist = new AllVendors();
    new VendorsFilter(null, {
        onChange: function (data) {
            vendorslist.triggerLoad(data);
        }
    });

}());

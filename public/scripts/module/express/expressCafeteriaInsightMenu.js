import MenuComp from "../../controller/express/menuComp";
import VendorDropDown from '../../controller/express/vendorDropdownCtrl';
import HeaderController from '../../controller/headerCtrl';
import SidebarController from '../../controller/sidebarCtrl';

(function () {
    new MenuComp();
    new VendorDropDown();
    HeaderController.init();
    SidebarController.init();
}());








import SettlementsTable from '../../controller/express/settlementsTable';
import MenuFilters from '../../controller/express/menuFilters';
import SnapShop from '../../controller/express/vendorSettlementSnap';
import VendorDropDown from '../../controller/express/vendorDropdownCtrl';

(function () {
    new VendorDropDown();
    let isFirstLoad = true;
    let snap, settlement;


    new MenuFilters(null, {
        onChange: (filters) => {

            if (isFirstLoad) {
                isFirstLoad = false;
                snap = new SnapShop(null, {filters: filters});
                settlement = new SettlementsTable(null, {filters: filters});
            }
            else {
                snap.triggerReload(filters);
                settlement.triggerLoad(filters);
            }
        }
    });


}());

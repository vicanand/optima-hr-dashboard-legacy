import VendorSalesCtrl from '../../controller/express/vendorsSalesCtrl';
import SalesAmtAndTransaction from '../../controller/express/salesAmtAndTransaction';
import VendorSalesSnap from '../../controller/express/vendorSalesSnap';
import SalesFootfall from '../../controller/express/salesFootfall';
import MenuFilters from '../../controller/express/menuFilters';
import VendorDropDown from '../../controller/express/vendorDropdownCtrl';

(function () {
    new VendorDropDown();
    let isFirstLoaded = true;
    let salesStats, vendorsStats, footfall;


    new MenuFilters(null, {
        onChange: (filters) => {
            if (isFirstLoaded) {
                vendorsStats = new VendorSalesSnap(null, {filters: filters});
                salesStats = new SalesAmtAndTransaction(null, {filters: filters});
                footfall = new SalesFootfall(null, {filters: filters});
            }
            else {
                vendorsStats.triggerReload(filters);
                salesStats.triggerReload(filters);
                footfall.triggerReload(filters);
            }
        }
    });
}());

import EmployeeCtrl from '../../controller/express/employeeCtrl';
import EmployeeUniTans from '../../controller/express/EmployeeUniTans';
import AvgCustomerSpend from '../../controller/express/avgCustomerSpend';
import EmployeeSnapshot from '../../controller/express/employeeSnapshot';
import PaymentPreference from '../../controller/express/paymentPreferences';

let isFirstLoad = true;
let employeeUniTrans, avgSpend,paymentPreference;

(function () {
    new EmployeeSnapshot(null, {
        filterChange: (filters) => {
            if (isFirstLoad) {
                isFirstLoad = false;
                employeeUniTrans = new EmployeeUniTans($("#uniqueTransactions"), {
                    filters: filters
                });
                avgSpend = new AvgCustomerSpend(null, {
                    filters: filters
                });
               
               
            }
            else {
                employeeUniTrans.triggerUpdate(filters);
                avgSpend.triggerRefresh(filters);
                                     
            }
        }
    });
}());

import Table from '../widgets/table';
import CardProgramIndividualRevokeFund from '../controller/cardProgramIndividualRevokefundCtrl.js';

(function() {
    Table.init();
    CardProgramIndividualRevokeFund.init();
})();
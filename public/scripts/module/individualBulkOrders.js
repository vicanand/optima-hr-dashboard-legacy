import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import TableWidget from '../widgets/table-widget';
import bulkTransferSvc from '../service/bulkTransferSvc';
import Filter from "../widgets/filter";
import Constant from '../common/constants';

(function() {
    HeaderController.init();
    SidebarController.init();

    TableWidget.init();
    Filter.init();
    let bulkOrderID = window.location.pathname.split('/').pop();
    $('#bulkOrderID').html(bulkOrderID);

    bulkTransferSvc.getBulkOrderSummary(bulkOrderID).then(function(respData){
        if(respData.scheduledTransferDate){
            var scheduledTransferDate = respData.scheduledTransferDate.split(',')[0].split(' ')[1];
            var scheduledTransferMonth = respData.scheduledTransferDate.split(',')[0].split(' ')[0];
            var scheduledTransferYear = respData.scheduledTransferDate.split(',')[1].trim().split(' ')[0];
            $('#scheduleDate').html(scheduledTransferDate + ' ' + scheduledTransferMonth + ' ' + scheduledTransferYear);
        }
        else{
            $('#scheduleDate').html('-');
        }
        $('#totalBeneficiaries').html(respData.totalScheduledBeneficiaries);
        $('#totalAmount').html(respData.totalAmount);
        $('#programCode').html(respData.programShortCode);
        $('#bulkTransferOrderStatus').html(Constant.BULK_TRANSFER_ORDERS_STATUS_LABELS[respData.bulkOrderStatus]);
    },function(respErr){
        console.log(respErr);
    });

    $("#transferOrderTable").tablewidget({
        source: bulkTransferSvc.getIndividualBulkOrders,
        pageSize: 10,
        pageNumber: 1,
        tableTemplate: "/template/section/bulk-action/tables/individual-transfer-orders.ejs",
        rowTemplate: "/template/section/bulk-action/tables/individual-transfer-orders-row.ejs",
        searchSelector: $("#searchBox"),
        searchButton: $("#searchBtn"),
        filterSelect: $("#transferOrderStatus"),
        secondaryFilterSelect: $("#programsBenifits"),
        extraParam: {
            bulkOrderId: bulkOrderID
        },
    });
})();

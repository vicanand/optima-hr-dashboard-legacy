import TableWidget from '../widgets/table-widget';
import FilterWidget from '../widgets/filter-widget';
import FundsService from '../service/fundsSvc';


(function() {
    TableWidget.init();
    FilterWidget.init();


    $("#fundsHistory").tablewidget({
        source: FundsService.getFundsHistory,
        pageSize: 10,
        pageNumber: 1,
        tableTemplate: "/template/tables/funds-history-legacy.ejs",
        rowTemplate: "/template/tables/funds-history-legacy-row.ejs",
        extraParam: {
            transferId: 3119
        }
        // searchSelector: $("#searchBox"),
        // filterSelect: $("#filter")
    });
    
     $("#accountStatement").tablewidget({
            source: FundsService.getLegacyAccountStatement,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: "/template/tables/account-statement-legacy.ejs",
            rowTemplate: "/template/tables/account-statement-legacy-row.ejs",
            extraParam: {
                transferId: 3119
            }
            // searchSelector: $("#searchBox"),
            // filterSelect: $("#filter")
        });

    $('#fundsTab').click(function(e) {

        $("#accountStatement").tablewidget("destroy");
        $("#fundsHistory").tablewidget({
            source: FundsService.getFundsHistory,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: "/template/tables/funds-history-legacy.ejs",
            rowTemplate: "/template/tables/funds-history-legacy-row.ejs",
            extraParam: {
                transferId: 3119
            }
            // searchSelector: $("#searchBox"),
            // filterSelect: $("#filter")
        });
    });

    $('#accTab').click(function(e) {
        $("#fundsHistory").tablewidget("destroy");
        $("#accountStatement").tablewidget({
            source: FundsService.getLegacyAccountStatement,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: "/template/tables/account-statement-legacy.ejs",
            rowTemplate: "/template/tables/account-statement-legacy-row.ejs",
            extraParam: {
                transferId: 3119
            }
            // searchSelector: $("#searchBox"),
            // filterSelect: $("#filter")
        });
    });





}());

import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import TransferErrorsController from '../controller/transferErrorsCtrl';

(function() {
    HeaderController.init();
    SidebarController.init();
    TransferErrorsController.init();
})();

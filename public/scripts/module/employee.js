import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import EmployeeController from '../controller/employeesCtrl';
import TableWidget from '../widgets/table-widget';
import EmployeeSvc from '../service/employeeSvc';
import Filter from "../widgets/filter";
import Storage from "../common/webStorage";
import Constants from "../common/constants";
import CacheDataStorage from '../common/cacheDataStorage';

(function() {
    HeaderController.init();
    SidebarController.init();
    EmployeeController.init();
    Filter.init();
    TableWidget.init();

    var employeeSetupEnabled = $('#employee-Setup-Enabled').val() == 'true' ? true : false;
    var tableTemplateURL = '/template/tables/employees/employees.ejs';
    var rowTemplateURL = '/template/tables/employees/employees-row.ejs';

    if(employeeSetupEnabled) {
        tableTemplateURL = '/template/tables/employees/employees-status.ejs';
    }

    const country_configs = CacheDataStorage.getItem(Constants.COUNTRY_CONFIGS);
    if (country_configs) {
        const tablewidgetData = _.get(country_configs, 'employee.tablewidgetData');
        if (tablewidgetData && tablewidgetData.tableTemplateURL && tablewidgetData.rowTemplateURL) {
            tableTemplateURL = tablewidgetData.tableTemplateURL;
            rowTemplateURL = tablewidgetData.rowTemplateURL;
        }
    }

    $("#get-employees-widget").tablewidget({
        source: EmployeeSvc.getEmployeesV2,
        pageSize: 10,
        pageNumber: 1,
        tableTemplate: tableTemplateURL,
        rowTemplate: rowTemplateURL,
        searchSelector: $("#searchBox"),
        searchButton: $("#searchBtn"),
        filterSelect: $("#companiesDropDown"),
        secondaryFilterSelect: $("#programsBenifits"),
        extraParam: {
            employeeSetupEnabled: employeeSetupEnabled
        }
    });
})();



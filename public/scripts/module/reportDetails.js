import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import ReportDetailsCtrl from '../controller/reportDetailsCtrl';

(function() {
    HeaderController.init();
    SidebarController.init();
    ReportDetailsCtrl.init();
})();
import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import CashIncentiveTransferDetail from "../controller/cashIncentiveTransferDetailCtrl";
import TableWidget from '../widgets/table-widget';
import FilterWidget from '../widgets/filter-widget';


(function() {
    TableWidget.init();
    HeaderController.init();
    SidebarController.init();
    CashIncentiveTransferDetail.init();
    FilterWidget.init();






    // $('#criteriaFilter').filterDropdown({
    //     'isMultiple' : false,
    //     'source' : ['Atmos Energy Corporation','Avaya Inc.','BellSouth Corporation','Boston Scientific Corporation','Bemis Company, Inc.','Cisco Systems Inc.','Capital One Financial Corp.'],
    //     'filterLabel' : 'Companies',
    //     'selected' : ''
    // });
}());

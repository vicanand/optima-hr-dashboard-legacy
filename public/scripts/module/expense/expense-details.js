import HeaderController from '../../controller/headerCtrl';
import SidebarController from '../../controller/sidebarCtrl';
import Chart from "../../../lib/chart.js/src/chart";
import Table from '../../widgets/table';
import TableWidget from '../../widgets/table-widget';
import OrderSvc from '../../service/ordersService';
import FilterWidget from '../../widgets/filter-widget';
import EmployeeSvc from '../../service/employeeSvc';
import reportAmounts from '../../../template/section/benefit-details/transfer-report/amounts.ejs';
import ReimbursementSvc from '../../service/expense/reimbursement-bills-svc';
import ReimbursementAmounts from '../../../template/section/expense/details/tab-content/reimbursement-amounts.ejs';
import Util from '../../common/util';
import DomEvents from "../../common/domEventHandler";
import BenefitSvc from "../../service/benefitSvc";
import TopMerchantsTemplate from "../../../template/section/expense/home/top-merchants.ejs";
import EscalationSvc from '../../service/expense/escalations-svc';

HeaderController.init();
SidebarController.init();
Table.init();
TableWidget.init();
FilterWidget.init();

let reimbursementAmountsTemplate = ejs.compile(ReimbursementAmounts);
let reportAmountsTemplate = ejs.compile(reportAmounts);
let productType = $("#productType").val().toLowerCase();
let ordersTableTemplate = "";

BenefitSvc.getTopMerchantsInExpense('GET_TOP_MERCHANTS').then(function(topMerchantsData) {
    topMerchantsData.aggregations.totalUniqueMerchants = Util.formatNumber(topMerchantsData.aggregations.totalUniqueMerchants);
    for (var i = 0; i < topMerchantsData.aggregations.topLocations.length; i++) {
        topMerchantsData.aggregations.topLocations[i].count = parseFloat((topMerchantsData.aggregations.topLocations[i].count / topMerchantsData.aggregations.total) * 100).toFixed(2) + '%';
    }
    $('#merchantGraphLoader').hide();
    DomEvents.renderMyTemplate("merchantsWrpr", TopMerchantsTemplate, topMerchantsData);
}, function(respErr) {
    $('#merchantGraphLoader').hide();
    $('#merchantGraphError').show();
});

let statsParam = {
    productID: $('#programID').val(),
    pageNumber: 0,
    pageSize: 12,
    searchQuery: ""
};

OrderSvc.getPayoutStats(statsParam).then(function(respData) {
    reportAmountsTemplate = reportAmountsTemplate(respData.payoutStatsByStatus);
    $('#reportAmounts').html(reportAmountsTemplate);
});

$("#expenseEmployees").tablewidget({
    source: EmployeeSvc.getBenifitEmployees,
    pageSize: 10,
    pageNumber: 1,
    tableTemplate: "/template/section/expense/details/tab-content/tables/expense-employee.ejs",
    rowTemplate: "/template/section/expense/details/tab-content/tables/expense-employee-row.ejs",
    extraParam: {
        transferId: $("#transferId").val(),
        productType: productType
    },
    searchSelector: $("#employeeSearch"),
    searchButton: $("#employeeSearchBtn")
});


var cardProgram = [];
cardProgram[0] = $('#cardProgramID').val();

var transferReport = $('#transferReport');

/*function spendCategoryChart() {
    let data = [300, 50, 100];
    let labels = ["Gym Fees", "Spectacles", "Domiciliary"];
    BenefitDetailsController.loadGraph("spendCategory", data, labels, 'doughnut');
}

function claimCharts() {
    var ctx = document.getElementById('claimCharts');
    var data = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
            fill: true,
            backgroundColor: "rgba(99,62,165,0.7)",
            borderColor: "rgba(255,255,255,0)",
            pointBackgroundColor: "rgba(255,255,255,0)",
            lineTension: .6,
            pointBorderWidth: 0,
            data: [0, 10, 80, 100, 50, 0],
            showLine: true,
            spanGaps: false,
            steppedLine: false
        }]
    };
    var options = {
        scales: {
            xAxes: [{
                gridLines: {
                    display: false,
                    offsetGridLines: false
                }
            }]
        }
    }
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: options
    });
}
spendCategoryChart();
claimCharts();*/


$("#expenseReimburse").table({
    source: ReimbursementSvc.getBillsForProgram,
    pageSize: 10,
    pageNumber: 1,
    tableTemplate: '/template/section/expense/details/tab-content/tables/expense-reimburse.ejs',
    rowTemplate: '/template/section/expense/details/tab-content/tables/expense-reimburse-row.ejs',
    extraParam: {
        productType: $("#productType").val().toLowerCase(),
        statusFilters: $('#transferOrderStatus').val(),
        cardProgramID: $('#cardProgramID').val()
    },
    searchSelector: $("#employeeReimburseSearch"),
    searchButton: $("#employeeReimburseSearchBtn"),
    benfitFilterItems: $('#reimbursementStatusType'),
    filterSelect: $('#reimbursementStatusType')
});

function checkAmount(n) {
    if (n > 0) {
        return n / 100;
    } else {
        return 0;
    }
}

var reimburseAggregates = function(reimburseParam, escalatedParams) {
    var req1 = ReimbursementSvc.getBillsForProgram(reimburseParam);
    var req2 = EscalationSvc.getEscalatedClaims(escalatedParams);

    $.when(req1, req2).done(function(respData, escData) {
        var approvedAmount = (parseInt(respData.amountPerBillState['APPROVED']) || 0) +
            (parseInt(respData.amountPerBillState['PAID']) || 0) + (parseInt(respData.amountPerBillState['PARTIALLY_PAID']) || 0),
            approvedCount = (parseInt(respData.countPerBillState['APPROVED']) || 0) +
            (parseInt(respData.countPerBillState['PAID']) || 0) + (parseInt(respData.countPerBillState['PARTIALLY_PAID']) || 0),
            amountObj, escAmount = 0,
            escCount = 0,
            reimburseAmount = 0,
            reimburseCount = 0,
            pendingAmount = 0,
            pendingCount = 0,
            declinedAmount = 0,
            declinedCount = 0;
        if (escData.netEscalationsByState && escData.netEscalationsByState["PENDING"]) {
            escAmount = escData.netEscalationsByState["PENDING"].netAmount.amount;
            escCount = escData.netEscalationsByState["PENDING"].numberOfEscalations;
        }
        pendingAmount = respData.amountPerBillState['UPLOADED'] || 0;
        pendingCount = respData.countPerBillState['UPLOADED'] || 0;
        approvedAmount = approvedAmount || 0;
        approvedCount = approvedCount || 0;
        declinedAmount = respData.amountPerBillState['DECLINED'] || 0;
        declinedCount = respData.countPerBillState['DECLINED'] || 0;
        reimburseAmount = pendingAmount + approvedAmount + declinedAmount + escAmount;
        reimburseCount = pendingCount + approvedCount + declinedCount + escCount;
        amountObj = {
            pending: {
                amount: pendingAmount,
                count: pendingCount
            },
            approved: {
                amount: approvedAmount,
                count: approvedCount
            },
            declined: {
                amount: declinedAmount,
                count: declinedCount
            },
            escalated: {
                amount: escAmount,
                count: escCount
            },
            reimbursementsClaimed: {
                amount: reimburseAmount,
                count: reimburseCount
            }
        }

        $('#noOfClaims').text(reimburseCount);

        var labels,
            data = [checkAmount(amountObj.pending.amount), checkAmount(amountObj.escalated.amount), checkAmount(amountObj.approved.amount), checkAmount(amountObj.declined.amount)];
        labels = ['Pending', 'Escalated', 'Approved', 'Declined'];

        if (amountObj.pending.amount || amountObj.approved.amount || amountObj.declined.amount) {
            reimbursementsChart(data, labels);
        } else {
            $('#reimbursementsDataLoader').hide();
            $('#reimbursementsEmpty').show();
        }

        for (var key in amountObj) {
            amountObj[key].amount = Util.formatINR(amountObj[key].amount / 100);
            amountObj[key].count = Util.formatINR(amountObj[key].count);
        }
        $('#reimbursementsAmount').text(amountObj.approved.amount);
        var reimbursementAmountHtml = reimbursementAmountsTemplate(amountObj);
        $('#reimbursementAmounts').html(reimbursementAmountHtml);
    });
}

var reimburseParam = {
    filterValue: $('#reimbursementStatusType').val(),
    pageSize: 10,
    pageNumber: 1,
    cardProgramID: $('#cardProgramID').val()
};

let escalatedParams = {
    programID: $('#transferId').val(),
    pageNumber: 1,
    pageSize: 10
}

reimburseAggregates(reimburseParam, escalatedParams);

var loadGraph = function(graphId, graphData, graphLabels, graphType) {
    var ctx = document.getElementById(graphId);
    var myChart = new Chart(ctx, {
        type: graphType,
        data: {
            labels: graphLabels,
            datasets: [{
                data: graphData,
                backgroundColor: [
                    '#ffbc00',
                    '#ff8b00',
                    '#00b898',
                    '#e54d42'
                ],
                borderWidth: 0
            }]
        },
        options: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return '₹ ' + Util.formatINR(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index]);
                    }
                }
            }
        }
    });
}

function reimbursementsChart(data, labels) {
    loadGraph("reimbursementsChart", data, labels, 'doughnut');
    $('#reimbursementsDataLoader').hide();
    $('#reimbursementsWrpr').show();
}

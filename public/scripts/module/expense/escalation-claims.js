import HeaderController from '../../controller/headerCtrl';
import SidebarController from '../../controller/sidebarCtrl';
import TransferDetail from "../../controller/expense/escalations-detail-ctrl";
import TableWidget from '../../widgets/table-widget';
import FilterWidget from '../../widgets/filter-widget';

(function() {
    TableWidget.init();
    HeaderController.init();
    SidebarController.init();
    TransferDetail.init();
    FilterWidget.init();
}());

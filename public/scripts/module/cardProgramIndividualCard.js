import Table from '../widgets/table';
import CardProgramIndividualCardDetailsController from '../controller/cardProgramIndividualCardCtrl.js';
import daterangepicker from '../../../node_modules/bootstrap-daterangepicker/daterangepicker';
import moment from '../../lib/moment/moment';

(function() {
    Table.init();
    CardProgramIndividualCardDetailsController.init();


    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('input[name="daterange"]').val('');
    }
    $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('input[name="daterange"]').daterangepicker({
        "autoApply": true,
        locale: {
            format: 'YYYY-MM-DD',
            "customRangeLabel": "Choose From Calendar",
        },
        "opens": "right",
        "drops": "up",
        maxDate: new Date(),
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);
})();
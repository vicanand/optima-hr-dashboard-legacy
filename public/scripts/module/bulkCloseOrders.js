import DomEventHandler from '../common/domEventHandler';
import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import EmployeeController from '../controller/employeesCtrl';
import TableWidget from '../widgets/table-widget';
import BulkCloseOrdersController from '../controller/bulkCloseOrdersCtrl';
import BulkCloseSvc from '../service/bulkCloseSvc';
import Filter from "../widgets/filter";
import daterangepicker from '../../../node_modules/bootstrap-daterangepicker/daterangepicker';
import moment from '../../lib/moment/moment';

(function() {
    HeaderController.init();
    SidebarController.init();
    EmployeeController.init();
    TableWidget.init();
    Filter.init();

    var start = moment().subtract(29, 'days');
    console.log(start);
    var end = moment();

    function cb(start, end) {
        $('input[name="daterange"]').val('');
    }
    $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('input[name="daterange"]').daterangepicker({
        "autoApply": true,
        locale: {
            format: 'YYYY-MM-DD',
            "customRangeLabel": "Choose From Calendar",
        },
        "opens": "left",
        maxDate: new Date(),
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(2, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

    $("#transferOrderTable").tablewidget({
        source: BulkCloseSvc.getCloseOrders,
        pageSize: 10,
        pageNumber: 1,
        tableTemplate: "/template/section/bulk-close/tables/transfer-orders.ejs",
        rowTemplate: "/template/section/bulk-close/tables/transfer-orders-row.ejs",
        filterSelect: $("#transferOrderStatus"),
        searchSelector: $("#searchBox"),
        searchButton: $("#searchBtn"),
        startDate: $('#tranferPeriod')
    });
    BulkCloseOrdersController.init();
    DomEventHandler.bindEvent("resetFilters", "click", TableWidget.resetAllFilters);

})();
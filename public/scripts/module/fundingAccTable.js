
import FundingHomeController from '../controller/fundingHomeCtrl';
import TableWidget from '../widgets/table-widget';
import FilterWidget from '../widgets/filter-widget';
import FundsService from '../service/fundsSvc';

(function() {
    //FundingHomeController.init();
    TableWidget.init();
    FilterWidget.init();



    $("#fundingAccounts").tablewidget({
        source: FundsService.getFundingAccounts,
        pageSize: 10,
        pageNumber: 1,
        tableTemplate: "/template/tables/funding-accounts.ejs",
        rowTemplate: "/template/tables/funding-accounts-row.ejs"
        // extraParam: {
        //     transferId: 3119
        // }
        // searchSelector: $("#searchBox"),
        // filterSelect: $("#filter")
    });



})();
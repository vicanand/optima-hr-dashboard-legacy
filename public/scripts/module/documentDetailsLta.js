import HeaderController from '../controller/headerCtrl';
//import SidebarController from '../controller/sidebarCtrl';
import DocumentDetailsLtaController from '../controller/documentDetailsLtaCtrl';
import TableWidget from '../widgets/table-widget';
import FilterWidget from '../widgets/filter-widget';
import DocStoreService from '../service/docStoreSvc';


(function() {
    HeaderController.init();
    //SidebarController.init();
    DocumentDetailsLtaController.init();
    TableWidget.init();
    // $("#documentsTable").tablewidget({
    //     source: DocStoreService.getDocs,
    //     pageSize: 10,
    //     pageNo: 1,
    //     tableTemplate: "/template/tables/document.ejs",
    //     rowTemplate: "/template/tables/document-row.ejs",
    //     searchSelector: $("#searchDocs"),
    //     searchButton: $("#searchDocsBtn"),
    //     filterSelect: $("#programType"),
    //     extraParam: {
    //         businessId : $("#businessId").val()+"@business.zeta.in",
    //         // businessId:"138686@business.zeta.in"
    //     }
    // });
    

})();

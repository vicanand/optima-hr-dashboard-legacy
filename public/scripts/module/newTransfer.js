import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import NewTransferController from '../controller/newTransferCtrl';

(function() {
    HeaderController.init();
    SidebarController.init();
    NewTransferController.init();
})();

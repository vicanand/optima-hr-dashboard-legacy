import FundHistoryCtrl from '../controller/fundHistoryCtrl';
import HeaderController from '../controller/headerCtrl';
(function() {
    FundHistoryCtrl.init();
    HeaderController.init();
}());
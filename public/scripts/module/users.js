import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import UserAccessController from '../controller/userAccessCtrl';
import TableWidget from '../widgets/table-widget';
import AccessCtrlSvc from '../service/accessCtrlSvc';

(function() {
    HeaderController.init();
    SidebarController.init();
    UserAccessController.init();
    TableWidget.init();
    
    $("#get-users-widget").tablewidget({
        source: AccessCtrlSvc.getCorpAccounts,
        pageSize: 10,
        pageNumber: 1,
        tableTemplate: "/template/tables/users.ejs",
        rowTemplate: "/template/tables/users-row.ejs",
    });
})();

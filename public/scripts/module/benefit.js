import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import BenefitController from '../controller/benefitCtrl';

(function() {
    HeaderController.init();
    SidebarController.init();
    BenefitController.init();
    BenefitController.loadDatePicker();
})();

import HeaderController from '../controller/headerCtrl';
import DomEventHandler from '../common/domEventHandler';
//import SidebarController from '../controller/sidebarCtrl';
import BenefitDetailsController from '../controller/benefitDetailsCtrl';
import TableWidget from '../widgets/table-widget';
import Table from '../widgets/table';
import OrderSvc from '../service/ordersService';
import FilterWidget from '../widgets/filter-widget';
import EmployeeSvc from '../service/employeeSvc';
import BenefitSvc from '../service/benefitSvc';
import transferDetailsDomEvents from "../dom-events/transferDetailsDomEvents";
import benefitDetailsDomEvents from "../dom-events/benefitDetailsDOMEvent";
import reportAmounts from '../../template/section/benefit-details/transfer-report/amounts.ejs';
import ReimbursementSvc from '../service/expense/reimbursement-bills-svc';
import ReimbursementAmounts from '../../template/section/benefit-details/reimbursement-report/amounts.ejs';
import TransferOrderHistory from "../../template/Modal/tranfer-order-history.ejs";
import Util from '../common/util';
import Constants from '../common/constants';
import moment from '../../lib/moment/moment';
import CacheDataStorage from '../common/cacheDataStorage';
import Meta from '../common/metaStorage';

HeaderController.init();
//SidebarController.init();
BenefitDetailsController.init();
Table.init();
TableWidget.init();
FilterWidget.init();
if($('#programSetupEnabled').val() == 'true'){
    const productName = $("#productType").val().toLowerCase();
    const country = Meta.getMetaElement('countryVar');
    if(country == 'isSodexoVn' && productName == 'sodexo_cafeteria'){
        const cardProgramId = $("#cardProgramID").val();
        BenefitSvc.getSpendSettingVn(cardProgramId).then(function(spendData) {
            let timeSlice = spendData.allowedTimeSlices;
            var allowedTimeSlice = [];
            var selectedSpendDays = [];
            if (timeSlice && timeSlice.length > 0) {
                timeSlice.forEach(function (timeString) {
                    allowedTimeSlice.push(timeString.split(" ")[1])
                })
                if (timeSlice[0].split(" ")[4] == "1-7" || timeSlice[0].split(" ")[4] == "*") {
                    selectedSpendDays = Constants.DAYS
                } else if (timeSlice[0].split(" ")[4]) {
                    selectedSpendDays = timeSlice[0].split(" ")[4].split(",");
                }
                $('#timeSliceContainer').data('timeslices', allowedTimeSlice);
                $('#allowspendsContainer').data('allowedspenddays', selectedSpendDays);
            }
            benefitDetailsDomEvents.initProgramSetup();
        },function(error) {
            benefitDetailsDomEvents.initProgramSetup();
        });
    }else{
        benefitDetailsDomEvents.initProgramSetup();
    }
}

var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('input[name="daterange1"]').val('');
    }
    $('input[name="daterange1"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
 
    $('input[name="daterange1"]').daterangepicker({
        "autoApply": true,
        locale: {
            format: 'DD MMM YYYY',
            "customRangeLabel": "Choose From Calendar",
        },
        "opens": "right",
        "drops": "up",
        maxDate: new Date(),
        startDate: start,
        endDate: end,
        ranges: {
            // 'Today': [moment(), moment()],
            // 'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            // 'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'FY 2018': ['01 Apr 2017', '31 Mar 2018']
        }
    }, cb);

    cb(start, end);



let reportAmountsTemplate = ejs.compile(reportAmounts);
let reimbursementAmountsTemplate = ejs.compile(ReimbursementAmounts);
let productType = $("#productType").val().toLowerCase();
let ordersTableTemplate = "";
let ordersRowTemplate = "";


let params = {
    productType: productType,
    pageNumber: 0,
    pageSize: 20,
    searchQuery: ""
};


OrderSvc.getOrders(params).then(function(respData) {
    let data = [],
        labels = [],
        transfers = [],
        months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        amountObj = {},
        transfersObj = {},
        aggregationKey = respData.records[0].aggregationKey.split(' '),
        year1 = parseInt(aggregationKey[1]),
        year2 = year1 - 1,
        month = aggregationKey[0],
        index = months.indexOf(month) + 1,
        months_year1 = months.slice(0, index),
        months_year2 = months.slice(index);
    $(respData.records).each(function(index, value) {
        let item = value,
            amount = item.amount.replace(/,/g, ''),
            year;
        aggregationKey = item.aggregationKey.split(' ');
        month = aggregationKey[0];
        year = aggregationKey[1];
        amountObj[month + ' ' + year] = amount;
        transfersObj[month + ' ' + year] = item.beneficiaries;
    });
    $.each(months_year2, function(index, value) {
        let month = value,
            year = year2.toString(),
            amount = amountObj[month + " " + year] || 0,
            transfer = transfersObj[month + " " + year] || 0;
        labels.push(month + " '" + year.slice('-2'));
        data.push(amount);
        transfers.push(transfer);
    });
    $.each(months_year1, function(index, value) {
        let month = value,
            year = year1.toString(),
            amount = amountObj[month + " " + year] || 0,
            transfer = transfersObj[month + " " + year] || 0;
        labels.push(month + " '" + year.slice('-2'));
        data.push(amount);
        transfers.push(transfer);
    });

    if (data.length && !checkAllZero(data) && !checkAllZero(transfers)) {
        $('.report-graph').show();
        reportChart(data, labels, transfers);
    }

});

function checkAllZero(arr) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] != 0) {
            return false;
        }
    }
    return true;
}

function fetchHistoryData(params) {
    return OrderSvc.getOrderHistory(params.orderId, "order")
    .then(response => {
        let finalData = OrderSvc.getFormattedOrderDetails(response.auditDataList)
        if(finalData.records && finalData.records.length > 0) {
            $('.updated-by-name').text(finalData.records[0].name);
        } else {
            $('.latest-order-update').hide();
        }
        return finalData;
    })
}

let statsParam = {
    productID: $('#transferId').val(),
    pageNumber: 0,
    pageSize: 12,
    searchQuery: ""
};

OrderSvc.getPayoutStats(statsParam).then(function(respData) {
    reportAmountsTemplate = reportAmountsTemplate(respData.payoutStatsByStatus);
    $('#reportAmounts').html(reportAmountsTemplate);
});


ordersRowTemplate = "/template/tables/transfer-orders-row.ejs";

if (productType.indexOf('gift') === -1 && productType.indexOf('reward') === -1) {
    ordersTableTemplate = '/template/tables/transfer-orders.ejs';
} else {
    ordersTableTemplate = '/template/tables/gift-transfer-orders.ejs';
}

let rb_access_data = $('#rb-access-data').val();
if(rb_access_data){
    rb_access_data = JSON.parse(rb_access_data);
}
const isProgramManagerRead = Util.isProgramManagerRead(rb_access_data);
const isProgramManagerWrite = Util.isProgramManagerWrite(rb_access_data);

const country_configs = CacheDataStorage.getItem(Constants.COUNTRY_CONFIGS);
if (country_configs) {
    const tablewidgetData = _.get(country_configs, 'benefitDetailsModule.start');
    if (tablewidgetData && tablewidgetData.ordersTableTemplate && tablewidgetData.ordersRowTemplate) {
        ordersTableTemplate = tablewidgetData.ordersTableTemplate;
        ordersRowTemplate = tablewidgetData.ordersRowTemplate;
    }
}


$("#transferOrderTable").table({
    source: OrderSvc.getTransferOrders,
    pageSize: 10,
    pageNumber: 1,
    tableTemplate: ordersTableTemplate,
    rowTemplate: ordersRowTemplate,
    extraParam: {
        productType: $("#productType").val().toLowerCase(),
        statusFilters: $('#transferOrderStatus').val(),
        compareDates: $('#compare-date-overview').data('compare-date'),
        isProgramManagerRead: isProgramManagerRead,
        isProgramManagerWrite: isProgramManagerWrite
    },
    searchSelector: $("#employeeSearch"),
    searchButton: $("#searchEmployeeBtn"),
    benfitFilterItems: $('#transferOrderStatus'),
    filterSelect: $('#transferOrderStatus')
});
DomEventHandler.bindClassEvent("transferorder-row", "click", ".downloadOrderFile", transferDetailsDomEvents.downloadOrderFile);
DomEventHandler.bindClassEvent("transferorder-row", "click", ".downloadOrderReport", transferDetailsDomEvents.downloadOrderReport);


$("#ordersTable").table({
    source: OrderSvc.getOrders,
    pageSize: 20,
    pageNumber: 0,
    tableTemplate: '/template/tables/product-orders.ejs',
    rowTemplate: '/template/tables/product-order-row.ejs',
    extraParam: {
        productType: $("#productType").val().toLowerCase(),
        statusFilters: $('#transferOrderStatus').val()
    },
    searchSelector: $("#employeeSearch"),
    searchButton: $("#searchEmployeeBtn"),
    benfitFilterItems: $('#transferOrderStatus'),
    filterSelect: $('#transferOrderStatus')
});

$('#transferOrderTable').on('click.acivity', '.view-details', function(event) {
    DomEventHandler.renderMyTemplate('showOrderHistoryListModal', TransferOrderHistory, {});
    $('#showOrderHistoryListModal').modal('show');
    const orderId = $(this).closest('tr').data('orderid');
    $("#orderHistory").table({
        source: fetchHistoryData,
        tableTemplate: "/template/tables/transfer-order-history.ejs",
        rowTemplate: "/template/tables/transfer-order-history-row.ejs",
        extraParam: {
            orderId: orderId
        },
    });
});

$('#transferOrderTable').on('click.acivity', '.orders-row td:not(.file-url,.position-dropdown)', function(event) {
    let orderId = $(this).closest('.orders-row').data('orderid');
    let programID = $('#transferOrderTable').data('programid');
    window.location.href = programID + '/transfers/' + orderId;
});

$('#transferOrderTable').on('click.acivity', '.orders-row', function(event) {
    let orderId = $(this).closest('tr').data('orderid');
    let programID = $('#transferOrderTable').data('programid');
    window.location.href = programID + '/transfers/' + orderId;
});

$('#transferOrderTable').on('click.acivity', '.error-row', function(event) {
    let orderStatus = $(this).closest('tr').data('order-status');
    let orderId = $(this).closest('tr').data('orderid');
    let programID = $('#transferOrderTable').data('programid');
    if (orderStatus == Constants.TRANSFER_ORDERS_STATUS_LABELS['error']) {
        window.location.href = programID + '/transfers/' + orderId + '/errors';
    } else {
        window.location.href = programID + '/transfers/' + orderId;
    }
});

$('#ordersTable').on('click.acivity', '.order-row', function(event) {
    let programID = $('#ordersTable').data('programid'),
        startDate = $(this).data('start-date'),
        endDate = $(this).data('end-date');
    window.location.href = programID + '/transfers?startDate=' + startDate + "&endDate=" + endDate;
});

$("#benifitsEmployees").tablewidget({
    source: EmployeeSvc.getProgramBenifitEmployeesV2,
    pageSize: 10,
    pageNumber: 1,
    tableTemplate: "/template/tables/benifitEmployeeTable.ejs",
    rowTemplate: "/template/tables/beniftEmployeeTable-row.ejs",
    extraParam: {
        transferId: $("#transferId").val(),
        productType: productType,
        isProgramManagerRead: isProgramManagerRead,
        isProgramManagerWrite: isProgramManagerWrite
    },
    searchSelector: $("#employeeSearch"),
    searchButton: $("#employeeSearchBtn"),
    benfitFilterItems: $('#filterCntr')
        // filterSelect: $("#filter")
});

//
$("#expenseReimburse").table({
    source: ReimbursementSvc.getBillsForProgram, //OrderSvc.getOrders,
    pageSize: 5,
    pageNumber: 1,
    tableTemplate: '/template/section/benefit-details/tables/expense-reimburse.ejs',
    rowTemplate: '/template/section/benefit-details/tables/expense-reimburse-row.ejs',
    extraParam: {
        productType: $("#productType").val().toLowerCase(),
        statusFilters: $('#transferOrderStatus').val(),
        cardProgramID: $('#cardProgramID').val()
    },
    searchSelector: $("#employeeReimburseSearch"),
    searchButton: $("#employeeReimburseSearchBtn"),
    benfitFilterItems: $('#reimbursementStatusType'),
    filterSelect: $('#reimbursementStatusType')
});

var reimburseAggregates = function(param) {
    ReimbursementSvc.getBillsForProgram(param).then(function(respData) {
        var approvedAmount = (parseInt(respData.amountPerBillState['APPROVED']) || 0) +
            (parseInt(respData.amountPerBillState['PAID']) || 0) + (parseInt(respData.amountPerBillState['PARTIALLY_PAID']) || 0),
            approvedCount = (parseInt(respData.countPerBillState['APPROVED']) || 0) +
            (parseInt(respData.countPerBillState['PAID']) || 0) + (parseInt(respData.countPerBillState['PARTIALLY_PAID']) || 0),
            amountObj = {
                pending: {
                    amount: respData.amountPerBillState['UPLOADED'] / 100 || 0,
                    count: respData.countPerBillState['UPLOADED'] || 0
                },
                approved: {
                    amount: approvedAmount / 100 || 0,
                    count: approvedCount || 0
                },
                declined: {
                    amount: respData.amountPerBillState['DECLINED'] / 100 || 0,
                    count: respData.countPerBillState['DECLINED'] || 0
                }
            }
        var data = [],
            labels;
        data = [amountObj.pending.amount, amountObj.approved.amount, amountObj.declined.amount];
        labels = ['Pending', 'Approved', 'Declined'];
        if (amountObj.pending.amount || amountObj.approved.amount || amountObj.declined.amount) {
            reimbursementsChart(data, labels);
        } else {
            $('#reimbursementsDataLoader').hide();
            $('#reimbursementsEmpty').show();
        }
        for (var key in amountObj) {
            amountObj[key].amount = Util.formatINR(amountObj[key].amount);
            amountObj[key].count = Util.formatINR(amountObj[key].count);
        }
        var reimbursementAmountHtml = reimbursementAmountsTemplate(amountObj);
        $('#reimbursementAmounts').html(reimbursementAmountHtml);
    });
}

var aggregateParam = {
    filterValue: $('#reimbursementStatusType').val(),
    pageSize: 5,
    pageNumber: 1,
    cardProgramID: $('#cardProgramID').val()
};
reimburseAggregates(aggregateParam);

var loadGraph = function(graphId, graphData, graphLabels, graphType) {
    var ctx = document.getElementById(graphId);
    var myChart = new Chart(ctx, {
        type: graphType,
        data: {
            labels: graphLabels,
            datasets: [{
                data: graphData,
                backgroundColor: [
                    '#ffbc00',
                    '#00b898',
                    '#e54d42'
                ],
                borderWidth: 0
            }]
        },
        options: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        return '₹ ' + Util.formatINR(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index]);
                    }
                }
            }
        }
    });
}

function reimbursementsChart(data, labels) {
    loadGraph("reimbursementsChart", data, labels, 'doughnut');
    $('#reimbursementsDataLoader').hide();
    $('#reimbursementsWrpr').show();
}

var cardProgram = [];
cardProgram[0] = $('#cardProgramID').val();

var transferReport = $('#transferReport');

function reportChart(data, labels, transfers) {
    let ifiBankName = $("meta[name='ifi']").attr("content");
    transfers = transfers.map(transfer => {
        if(typeof transfer === 'string' ) {
            transfer = transfer.replace(/,/g, '')
        }
        return transfer;
    });
    var maxTransferVal = Math.max(...transfers),
        dataset = {
            labels: labels,
            datasets: [{
                type: "line",
                label: "Total Beneficiaries",
                backgroundColor: "#ff69b4",
                borderColor: "#ff69b4",
                data: transfers,
                borderWidth: 1,
                yAxisID: 'y-axis-1',
                fill: false,
                pointBorderColor: "#ff69b4",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "#ff69b4",
                pointHoverBorderColor: "#ff69b4",
                pointHoverBorderWidth: 2,
                pointRadius: 5,
                pointHitRadius: 1,
                spanGaps: false,
                lineTension: 0
            }, {
                type: "bar",
                label: "Amount Transferred",
                backgroundColor: '#633ea5',
                borderColor: '#b7b9d0',
                data: data,
                borderWidth: 1,
                yAxisID: 'y-axis-0'

            }]
        };
    if (ifiBankName == 'Sodexo_Philippines') {
        dataset.datasets.pop()
    }    
    var rightAxesMaxVal = maxTransferVal * 2;
    rightAxesMaxVal = Math.ceil(rightAxesMaxVal / 20) * 20;
    let chartData = {
        type: 'bar',
        data: dataset,
        options: {
            scales: {
                yAxes: [{
                    beginAtZero: true,
                    id: "y-axis-1",
                    position: 'right',
                    gridLines: {
                        display: false,
                        offsetGridLines: false
                    },
                    ticks: {
                        max: rightAxesMaxVal,
                        min: 0,
                        beginAtZero: true,
                        userCallback: function(value, index, values) {
                            if (value !== rightAxesMaxVal) {
                                return Util.formatNumber(value);
                            }
                        }
                    }
                }, {
                    beginAtZero: true,
                    id: "y-axis-0",
                    position: 'left',
                    gridLines: {
                        display: false,
                        offsetGridLines: false
                    },
                    ticks: {
                        beginAtZero: true,
                        userCallback: function(value, index, values) {
                            return Util.formatINR(value);
                        }
                    }
                }]
            }
        }
    };
    if (ifiBankName == 'Sodexo_Philippines') {
        chartData.options.scales.yAxes.pop();
    }
    
    var chartInstance = new Chart(transferReport, chartData);
}

export default {
    reimbursementsChart: reimbursementsChart
}
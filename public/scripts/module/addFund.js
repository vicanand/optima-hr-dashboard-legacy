import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import AddFundController from '../controller/addFundCtrl.js';

(function() {
    HeaderController.init();
    SidebarController.init();
    AddFundController.init();
    
})();

import HeaderController from '../controller/headerCtrl';
import AdminController from '../controller/adminCtrl';
import SidebarController from '../controller/sidebarCtrl';

(function() {
    HeaderController.init();
    SidebarController.init();
    AdminController.init();
})();

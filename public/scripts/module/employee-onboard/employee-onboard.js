import HeaderController from '../../controller/headerCtrl';
import SidebarController from '../../controller/sidebarCtrl';


(function(){
    HeaderController.init();
    SidebarController.init();
})();
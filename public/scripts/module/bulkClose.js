import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import BulkCloseController from '../controller/bulkCloseCtrl';

(function() {
    HeaderController.init();
    SidebarController.init();
    BulkCloseController.init();
})();

import HeaderController from '../controller/headerCtrl';
import CorpDetailsController from '../controller/corpDetailsCtrl';

(function() {
    HeaderController.init();
    CorpDetailsController.init();
})();

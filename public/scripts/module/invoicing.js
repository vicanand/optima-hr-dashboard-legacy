import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import InvoicingController from '../controller/invoicingCtrl';
import TableWidget from '../widgets/table';
import InvoicingSvc from '../service/invoicing/invoicingSvc.js';

(function() {
    HeaderController.init();
    SidebarController.init();
    InvoicingController.init();
    // TableWidget.init();    
    // $("#invoicesListTable").table({
    //     source: InvoicingSvc.getInvoices,
    //     pageSize: 10,
    //     pageNumber: 1,
    //     status:'',
    //     tableTemplate: "/template/section/invoices/invoicesTable.ejs",
    //     rowTemplate: "/template/section/invoices/invoicesTableRow.ejs",
    //     extraParam: {
    //     },
    //     filterSelect: $('#filterInvoicesOnState')
    // });
})();
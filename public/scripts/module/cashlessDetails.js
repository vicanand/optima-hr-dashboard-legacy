import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import CashlessDetailsCtrl from '../controller/cashlessDetailsCtrl';
import TableWidget from '../widgets/table-widget';
import Table from '../widgets/table';
import OrderSvc from '../service/ordersService';
import FilterWidget from '../widgets/filter-widget';
import EmployeeSvc from '../service/employeeSvc';
import benefitDetailsDomEvents from "../dom-events/transferDetailsDomEvents";
import reportAmounts from '../../template/section/benefit-details/transfer-report/cashless-amounts.ejs';
import Util from '../common/util';
import Constants from '../common/constants';
import env from '../common/env';

HeaderController.init();
SidebarController.init();
CashlessDetailsCtrl.init();
Table.init();
TableWidget.init();
FilterWidget.init();

let reportAmountsTemplate = ejs.compile(reportAmounts);
let productType = $("#productType").val().toLowerCase();
let currencyType = $('#currencyType').val();
let ordersTableTemplate = "";
let productOrdersTableTemplate = "";
let productOrdersRowTableTemplate = "";
let beneficiariesTableTemplate = "";
let beneficiariesRowTableTemplate = "";
const deleteCard = {}


let params = {
    productType: productType,
    pageNumber: 0,
    pageSize: 20,
    searchQuery: ""
};


OrderSvc.getOrders(params).then(function(respData) {
    let data = [],
        labels = [],
        transfers = [],
        months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        amountObj = {},
        transfersObj = {},
        aggregationKey = respData.records[0].aggregationKey.split(' '),
        year1 = parseInt(aggregationKey[1]),
        year2 = year1 - 1,
        month = aggregationKey[0],
        index = months.indexOf(month) + 1,
        months_year1 = months.slice(0, index),
        months_year2 = months.slice(index);
    $(respData.records).each(function(index, value) {
        let item = value,
            amount = item.amount.replace(/,/g, ''),
            year;
        aggregationKey = item.aggregationKey.split(' ');
        month = aggregationKey[0];
        year = aggregationKey[1];
        amountObj[month + ' ' + year] = amount;
        transfersObj[month + ' ' + year] = item.beneficiaries;
    });
    $.each(months_year2, function(index, value) {
        let month = value,
            year = year2.toString(),
            amount = amountObj[month + " " + year] || 0,
            transfer = transfersObj[month + " " + year] || 0;
        labels.push(month + " '" + year.slice('-2'));
        data.push(amount);
        transfers.push(transfer);
    });
    $.each(months_year1, function(index, value) {
        let month = value,
            year = year1.toString(),
            amount = amountObj[month + " " + year] || 0,
            transfer = transfersObj[month + " " + year] || 0;
        labels.push(month + " '" + year.slice('-2'));
        data.push(amount);
        transfers.push(transfer);
    });

    if (data.length && !checkAllZero(data) && !checkAllZero(transfers)) {
        $('.report-graph').show();
        reportChart(data, labels, transfers);
    }

});

function checkAllZero(arr) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] != 0) {
            return false;
        }
    }
    return true;
}

let statsParam = {
    productID: $('#transferId').val(),
    pageNumber: 0,
    pageSize: 12,
    searchQuery: ""
};

OrderSvc.getPayoutStats(statsParam).then(function(respData) {
    respData.payoutStatsByStatus['currency'] = currencyType;
    reportAmountsTemplate = reportAmountsTemplate(respData.payoutStatsByStatus);
    $('#reportAmounts').html(reportAmountsTemplate);
});

if(currencyType == 'INR'){
    ordersTableTemplate = '/template/tables/cashless-rupee-transfer-orders.ejs';
    productOrdersTableTemplate = '/template/tables/cashless-rupee-product-orders.ejs';
    productOrdersRowTableTemplate = '/template/tables/cashless-rupee-product-orders-row.ejs';
    beneficiariesTableTemplate = '/template/tables/cashless-rupee-benifitEmployeeTable.ejs';
    beneficiariesRowTableTemplate = '/template/tables/cashless-rupee-benifitEmployeeTable-row.ejs';
}
else{
    ordersTableTemplate = '/template/tables/cashless-token-transfer-orders.ejs';
    productOrdersTableTemplate = '/template/tables/cashless-token-product-orders.ejs';
    productOrdersRowTableTemplate = '/template/tables/cashless-token-product-orders-row.ejs';
    beneficiariesTableTemplate = '/template/tables/cashless-token-benifitEmployeeTable.ejs';
    beneficiariesRowTableTemplate = '/template/tables/cashless-token-benifitEmployeeTable-row.ejs';
}


$("#transferOrderTable").table({
    source: OrderSvc.getTransferOrders,
    pageSize: 10,
    pageNumber: 1,
    tableTemplate: ordersTableTemplate,
    rowTemplate: '/template/tables/transfer-orders-row.ejs',
    extraParam: {
        productType: $("#productType").val().toLowerCase(),
        statusFilters: $('#transferOrderStatus').val(),
        compareDates: $('#compare-date-overview').data('compare-date')
    },
    searchSelector: $("#employeeSearch"),
    searchButton: $("#searchEmployeeBtn"),
    benfitFilterItems: $('#transferOrderStatus'),
    filterSelect: $('#transferOrderStatus')
});


$(document).on('click', '#delete-card-btn', function(e){
    $('#delete-card-modal').modal('show');

    deleteCard["cardID"] = this.dataset['cardid'];
    deleteCard["employeeID"] = this.dataset['employeeid'];
    deleteCard["userID"] = this.dataset['userid'];
    let programId = $('#transferOrderTable').data('programid');
    deleteCard["programIDToCloseBehaviourMap"] = {};
    deleteCard["programIDToCloseBehaviourMap"][programId] = "MOVE_TO_FUNDING_ACCOUNT";
    deleteCard["reason"] = "closeCard";
    deleteCard["remarks"] = "towards coupons expiry payment";
    deleteCard["corpID"] = $("meta[name='corpID']").attr("content");
    deleteCard["companyID"] = $("meta[name='companyID']").attr("content");

})

$(document).on('click', '#delete-card', function(){
    EmployeeSvc.closeCardV2(deleteCard).then(respData => {
        employeeTable();
        $('#delete-card-modal').modal('hide');
    })
});



function getEmployeesList(){
    let status = null;

    return function(){
        if(status){
            $("#benifitsEmployees").tablewidget('destroy');
        }
        status = $("#benifitsEmployees").tablewidget({
            source: EmployeeSvc.getBenifitEmployees,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: beneficiariesTableTemplate,
            rowTemplate: beneficiariesRowTableTemplate,
            extraParam: {
                transferId: $("#transferId").val(),
                productType: productType
            },
            searchSelector: $("#employeeSearch"),
            searchButton: $("#employeeSearchBtn"),
            benfitFilterItems: $('#filterCntr')
                // filterSelect: $("#filter")
        });
    }
}

function getEmployeesListLBSNAA(){
    let status = null;
    let LoaderTemplate = beneficiariesTableTemplate = '/template/tables/cashless-rupee-benifitEmployeeLBSNAATable.ejs';

    let dataTemplate = beneficiariesRowTableTemplate = '/template/tables/cashless-rupee-benifitEmployeeLBSNAATable-row.ejs';

    return function(){
        if(status){
            $("#benifitsEmployees").tablewidget('destroy');
        }
        status = $("#benifitsEmployees").tablewidget({
            source: EmployeeSvc.getEmployeesV2,//getBenifitEmployees,
            pageSize: 10,
            pageNumber: 1,
            companyID: $("meta[name='companyID']").attr("content"),
            programID: $('#transferOrderTable').data('programid'),
            tableTemplate: LoaderTemplate,
            rowTemplate: dataTemplate,
            extraParam: {
                transferId: $("#transferId").val(),
                productType: productType
            },
            searchSelector: $("#employeeSearch"),
            searchButton: $("#employeeSearchBtn"),
            benfitFilterItems: $('#filterCntr')
        });
    }
}

let id = $('#transferOrderTable').data('programid');
let LBSNAA_CUG = [];
if(env.CURR_ENV.indexOf('STAGE') > -1){
    LBSNAA_CUG = [25687, 25680, 25685, 25768, 25769, 25770, 25774, 25775, 25776];
} else if(env.CURR_ENV.indexOf('PREPROD') > -1){
    LBSNAA_CUG = [37628, 37627, 37626, 37652];
} else if(env.CURR_ENV.indexOf('PROD') > -1){
    LBSNAA_CUG = [37115];
}

if($('#activateCloseCards')[0].value === 'true' && LBSNAA_CUG.indexOf(id) > -1){
    var employeeTable = getEmployeesListLBSNAA();
    employeeTable();
} else{
    var employeeTable = getEmployeesList();
    employeeTable();
}

$("#ordersTable").table({
    source: OrderSvc.getOrders,
    pageSize: 20,
    pageNumber: 0,
    tableTemplate: productOrdersTableTemplate,
    rowTemplate: productOrdersRowTableTemplate,
    extraParam: {
        productType: $("#productType").val().toLowerCase(),
        statusFilters: $('#transferOrderStatus').val()
    },
    searchSelector: $("#employeeSearch"),
    searchButton: $("#searchEmployeeBtn"),
    benfitFilterItems: $('#transferOrderStatus'),
    filterSelect: $('#transferOrderStatus')
});

$('#transferOrderTable').on('click.acivity', '.orders-row td:not(.file-url,.position-dropdown)', function(event) {
    let orderId = $(this).closest('.orders-row').data('orderid');
    let programID = $('#transferOrderTable').data('programid');
    window.location.href = programID + '/transfers/' + orderId;
});

$('#transferOrderTable').on('click.acivity', '.orders-row', function(event) {
    let orderId = $(this).closest('tr').data('orderid');
    let programID = $('#transferOrderTable').data('programid');
    window.location.href = programID + '/transfers/' + orderId;
});

$('#transferOrderTable').on('click.acivity', '.error-row', function(event) {
    let orderStatus = $(this).closest('tr').data('order-status');
    let orderId = $(this).closest('tr').data('orderid');
    let programID = $('#transferOrderTable').data('programid');
    if(orderStatus == Constants.TRANSFER_ORDERS_STATUS_LABELS['error']){
        window.location.href = programID + '/transfers/' + orderId + '/errors';
    }
    else{
        window.location.href = programID + '/transfers/' + orderId;
    }
});

$('#ordersTable').on('click.acivity', '.order-row', function(event) {
    let programID = $('#ordersTable').data('programid'),
        startDate = $(this).data('start-date'),
        endDate = $(this).data('end-date');
    window.location.href = programID + '/transfers?startDate=' + startDate + "&endDate=" + endDate;
});






var cardProgram = [];
cardProgram[0] = $('#cardProgramID').val();

var transferReport = $('#transferReport');

function reportChart(data, labels, transfers) {
    var maxTransferVal = Math.max(...transfers),
        dataset = {
            labels: labels,
            datasets: [{
                type: "line",
                label: "Total Beneficiaries",
                backgroundColor: "#ff69b4",
                borderColor: "#ff69b4",
                data: transfers,
                borderWidth: 1,
                yAxisID: 'y-axis-1',
                fill: false,
                pointBorderColor: "#ff69b4",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "#ff69b4",
                pointHoverBorderColor: "#ff69b4",
                pointHoverBorderWidth: 2,
                pointRadius: 5,
                pointHitRadius: 1,
                spanGaps: false,
                lineTension: 0
            }, {
                type: "bar",
                label: "Amount Transferred",
                backgroundColor: '#633ea5',
                borderColor: '#b7b9d0',
                data: data,
                borderWidth: 1,
                yAxisID: 'y-axis-0'

            }]
        };
    var rightAxesMaxVal = maxTransferVal * 2;
    rightAxesMaxVal = Math.ceil(rightAxesMaxVal / 20) * 20;
    var chartInstance = new Chart(transferReport, {
        type: 'bar',
        data: dataset,
        options: {
            scales: {
                yAxes: [{
                    beginAtZero: true,
                    id: "y-axis-1",
                    position: 'right',
                    gridLines: {
                        display: false,
                        offsetGridLines: false
                    },
                    ticks: {
                        max: rightAxesMaxVal,
                        min: 0,
                        beginAtZero: true,
                        userCallback: function(value, index, values) {
                            if (value !== rightAxesMaxVal) {
                                return Util.formatNumber(value);
                            }
                        }
                    }
                }, {
                    beginAtZero: true,
                    id: "y-axis-0",
                    position: 'left',
                    gridLines: {
                        display: false,
                        offsetGridLines: false
                    },
                    ticks: {
                        beginAtZero: true,
                        userCallback: function(value, index, values) {
                            return Util.formatINR(value);
                        }
                    }
                }]
            }
        }
    });
}

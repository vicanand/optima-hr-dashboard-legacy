import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import BenefitDetailsController from '../controller/employeesCtrl';
import Table from '../widgets/table';
import OrderSvc from '../service/ordersService';
import FilterWidget from '../widgets/filter-widget';
import EmployeeSvc from '../service/employeeSvc';
import EmployeeTrnsfrOdrRow from '../../template/tables/employee-transfer-report-row.ejs';
import Chart from "../../lib/chart.js/src/chart";
import reportAmounts from '../../template/section/benefit-details/transfer-report/amounts.ejs';
import Constants from '../common/constants';
import ReimbursementSvc from '../service/expense/reimbursement-bills-svc';
import ReimbursementAmounts from '../../template/section/employee-details/reimbursement-amounts.ejs';
import Util from '../common/util';
import Storage from '../common/webStorage';
import approver from '../../template/section/employee-details/approver.ejs';
import EscalationSvc from '../service/expense/escalations-svc';
import DomEvents from "../common/domEventHandler";
import EmployeeBenefitsDomEvents from "../dom-events/employee-benefits-dom-events";
import daterangepicker from '../../../node_modules/bootstrap-daterangepicker/daterangepicker';
import moment from '../../lib/moment/moment';
import CacheDataStorage from '../common/cacheDataStorage';

(function() {
    HeaderController.init();
    SidebarController.init();
    BenefitDetailsController.init();
    FilterWidget.init();
    Table.init();

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('input[name="daterange1"]').val('');
    }
    $('input[name="daterange1"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('input[name="daterange1"]').daterangepicker({
        "autoApply": true,
        locale: {
            format: 'DD MMM YYYY',
            "customRangeLabel": "Choose From Calendar",
        },
        "opens": "right",
        "drops": "up",
        maxDate: new Date(),
        startDate: start,
        endDate: end,
        ranges: {
            // 'Today': [moment(), moment()],
            // 'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            // 'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'FY 2018': ['01 Apr 2017', '31 Mar 2018'],
            'FY 2019': ['01 Apr 2018', '31 Mar 2019']
        }
    }, cb);

    cb(start, end);



    let reportAmountsTemplate = ejs.compile(reportAmounts);
    let reimbursementAmountsTemplate = ejs.compile(ReimbursementAmounts);
    let approverTemplate = ejs.compile(approver);
    // dynamic horizontal menu with more features
    let _alignMenu = function() {
        var w = 0;
        var mw = $("#horizontal").width() - 160;
        var i = -1;
        var menuhtml = '';
        $.each($("#horizontal").children(), function() {
            i++;
            w += $(this).outerWidth(true);
            if (mw < w) {
                menuhtml += $('<div>').append($(this).clone()).html();
                $(this).remove();
            }
        });
        $("#horizontal").append(
            '<li  style="position:relative;" href="#" class="hideshow">' +
            '<a href="javascript:void(0)" class="active-more">More ' +
            '<span style="font-size:13px"><img id="more-down-img" src="../../../images/down-arrow-black.svg"></span>' +
            '</a><ul class="more-dropdown-menu">' + menuhtml + '</ul></li>');
        $("#horizontal li.hideshow ul").css("top",
            $("#horizontal li.hideshow").outerHeight(true) + "px");
        $("#horizontal li.hideshow").click(function() {
            $(this).children("ul").toggle();
        });
        /* Anything that gets to the document
        will hide the dropdown */
        $(document).click(function() {
            $(".hideshow ul").hide();
        });

        /* Clicks within the dropdown won't make
           it past the dropdown itself */
        $(".hideshow").click(function(e) {
            e.stopPropagation();
        });
        //  For more release program --- also comment employeeCtrl file cardProgramIndividualBeneficiary()
        // $('#horizontal li').on('click', '.programTab', function (e) { // $("span").closest("ul")
        // e.stopPropagation();
        //     var className = $(this).closest('ul').attr('class');
        //     if(className=='more-dropdown-menu'){
        //         $('.hideshow .active-more').css("color", "#6e4db7");
        //         $('#more-down-img').attr('src','../../../images/down-arrow-purple.svg');
        //     }
        //     $('.beneficiary li.active').removeClass('active');
        //     $("#horizontal li").removeClass('active');
        //     $(this).parent().addClass('active');

        //     let programId = $(this).attr('id');
        //     EmployeeBenefitsDomEvents.cardProgramIndividualBeneficiary(programId);
        //  });
        $('#horizontal li.hideshow').on('click', '.programTab', function() {
            $('.beneficiary li.active').removeClass('active');
            $("#horizontal li").removeClass('active');
            $(this).parent().addClass('active');
            $('.hideshow .active-more').css("color", "#6e4db7");
            $('#more-down-img').attr('src', '../../../images/down-arrow-purple.svg');
            let programId = $(this).attr('id');
            EmployeeBenefitsDomEvents.cardProgramIndividualBeneficiary(programId);
        });
        if (menuhtml == '') {
            $("#horizontal li.hideshow").hide();
        } else {
            $("#horizontal li.hideshow").show();
        }
    };
    
    _alignMenu();

    let initSodexo = function () {
        let ele = $('.programTab')[0];
        $('.beneficiary li.active').removeClass('active');
        $('.tab_default_0').removeClass('active');
        $("#horizontal li").removeClass('active');
        $(ele).parent().addClass('active');
        $('.hideshow .active-more').css("color", "#273895");
        $('#more-down-img').attr('src', '../../../images/down-arrow-purple.svg');
        let programId = ele.id;
        EmployeeBenefitsDomEvents.cardProgramIndividualBeneficiary(programId);
    }

    const country_configs = CacheDataStorage.getItem(Constants.COUNTRY_CONFIGS);
    if (country_configs && _.get(country_configs, 'employeeBenefits.initSodexo')) {
        initSodexo();
    }

    $(window).resize(function() {
        $("#horizontal").append($("#horizontal li.hideshow ul").html());
        $("#horizontal li.hideshow").remove();
        _alignMenu();
    });

    // end of menu

    let _loadGraph = function(graphId, graphData, graphLabels, colors, graphType) {
        var ctx = document.getElementById(graphId);
        var myChart = new Chart(ctx, {
            type: graphType,
            data: {
                labels: graphLabels,
                datasets: [{
                    data: graphData,
                    backgroundColor: colors,
                    borderWidth: 0
                }]
            },
            options: {
                legend: {
                    display: true
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItems, data) {
                            return '₹ ' + Util.formatINR(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index]);
                        }
                    }
                }
            }
        });
    };

    let data = [],
        labels = [],
        colors = [];
    $('#spendDataLoader').hide();
    $('#cardsTransferWrpr').show();
    $('.employee-program-details .graph-wrpr .card-info-hidden').each(function() {
        let transferAmount = $(this).data('transferamount').toString();
        if (transferAmount) {
            var cardname;
            data.push(transferAmount.replace(',', ''));
            labels.push($(this).data('cardname'));
            cardname = $(this).data('cardname').toLowerCase();
            colors.push($(this).data('color'));
        }
    });
    if (data.length < 1 || (data.length === 1 && data[0] === '0')) {
        $('#cardsTransferEmpty').show();
        $('#cardsTransferWrpr').hide();
    } else {
        _loadGraph("cardsTransferAmount", data, labels, colors, 'doughnut');
    }


    $('.empReportAmounts').each(function() {
        var ele = $(this);
        let params = {
            productID: ele.data('program-id'),
            pageNumber: 0,
            pageSize: 10,
            employeeID: ele.data('emp-id')
        };
        OrderSvc.getPayoutStats(params).then(function(respData) {
            let reportAmounts = reportAmountsTemplate(respData.payoutStatsByStatus);
            ele.html(reportAmounts);
        });
    });


    $('.employee-program-details .employee-report-table').each(function(index, val) {
        $(val).table({
            source: EmployeeSvc.getIndividualEmployeePayout,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: "/template/tables/employee-transfer-report.ejs",
            rowTemplate: "/template/tables/employee-transfer-report-row.ejs",
            extraParam: {
                programId: $(val).data('programid'),
                compareDates: $(val).closest('.media-list-container').find('#compare-date').val()
            }
        });
    });
    var cardProgrammeId =  $('#cardProgrammeId').val(); 

    $("#reimbursementTable").table({
        source: ReimbursementSvc.getBillsForProgram,
        pageSize: 10,
        pageNumber: 1,
        tableTemplate: '/template/section/employee-details/tables/employee-reimburse.ejs',
        rowTemplate: '/template/section/employee-details/tables/employee-reimburse-row.ejs',
        extraParam: {
            productType: $("#productType").val(),
            statusFilters: $('#transferOrderStatus').val(),
            cardProgramID: $("#reimbursementTable").closest(".media-list-container").find("#cardProgramID").val(),
            userID: $('#userID').val()
        },
        searchSelector: $("#employeeReimburseSearch"),
        searchButton: $("#employeeReimburseSearchBtn"),
        benfitFilterItems: $('#reimbursementStatusType'),
        filterSelect: $('#reimbursementStatusType')
    });

    var cardID = $("#reimbursementTable").closest(".media-list-container").find("#cardID").val();
    // $("#dependantsTable").table({
    //     source: EmployeeSvc.getApproverDependants,
    //     pageSize: 10,
    //     pageNumber: 1,
    //     tableTemplate: '/template/section/employee-details/tables/employee-dependents.ejs',
    //     rowTemplate: '/template/section/employee-details/tables/employee-dependents-row.ejs',
    //     extraParam: {
    //         cardID: cardID
    //     }
    // });
 
    var approverSvcParam = {
        cardID: cardID
    };
    EmployeeSvc.getApproverDependants(approverSvcParam).then(function(respData) {
        var approverEle = $('#approverDetails'),
            approverHtml = approverTemplate(respData.config);
        approverEle.html(approverHtml);
    });

    var reimburseAggregates = function(reimburseParam, escalatedParams) {
        var req1 = ReimbursementSvc.getBillsForProgram(reimburseParam);
        var req2 = EscalationSvc.getEscalatedClaims(escalatedParams);

        $.when(req1, req2).done(function(respData, escData) {
            var approvedAmount = (parseInt(respData.amountPerBillState['APPROVED']) || 0) +
                (parseInt(respData.amountPerBillState['PAID']) || 0) + (parseInt(respData.amountPerBillState['PARTIALLY_PAID']) || 0),
                approvedCount = (parseInt(respData.countPerBillState['APPROVED']) || 0) +
                (parseInt(respData.countPerBillState['PAID']) || 0) + (parseInt(respData.countPerBillState['PARTIALLY_PAID']) || 0),
                amountObj, escAmount = 0,
                escCount = 0,
                reimburseAmount = 0,
                reimburseCount = 0,
                pendingAmount = 0,
                pendingCount = 0,
                declinedAmount = 0,
                declinedCount = 0;
            if (escData.netEscalationsByState && escData.netEscalationsByState["PENDING"]) {
                escAmount = escData.netEscalationsByState["PENDING"].netAmount.amount;
                escCount = escData.netEscalationsByState["PENDING"].numberOfEscalations;
            }
            pendingAmount = respData.amountPerBillState['UPLOADED'] || 0;
            pendingCount = respData.countPerBillState['UPLOADED'] || 0;
            approvedAmount = approvedAmount || 0;
            approvedCount = approvedCount || 0;
            declinedAmount = respData.amountPerBillState['DECLINED'] || 0;
            declinedCount = respData.countPerBillState['DECLINED'] || 0;
            reimburseAmount = pendingAmount + approvedAmount + declinedAmount + escAmount;
            reimburseCount = pendingCount + approvedCount + declinedCount + escCount;
            amountObj = {
                pending: {
                    amount: pendingAmount,
                    count: pendingCount
                },
                approved: {
                    amount: approvedAmount,
                    count: approvedCount
                },
                declined: {
                    amount: declinedAmount,
                    count: declinedCount
                },
                escalated: {
                    amount: escAmount,
                    count: escCount
                },
                reimbursementsClaimed: {
                    amount: reimburseAmount,
                    count: reimburseCount
                }
            }

            for (var key in amountObj) {
                amountObj[key].amount = Util.formatINR(amountObj[key].amount / 100);
                amountObj[key].count = Util.formatINR(amountObj[key].count);
            }
            var reimbursementAmountHtml = reimbursementAmountsTemplate(amountObj);
            $('#reimbursementAmounts').html(reimbursementAmountHtml);
        });
    }

    var reimburseParam = {
        filterValue: $('#reimbursementStatusType').val(),
        pageSize: 10,
        pageNumber: 1,
        // cardProgramID: $('#cardProgramID').val(),
        cardProgramID: $('#cardProgrammeId').val(),
        userID: $('#userID').val()
    };

    let escalatedParams = {
        programID: $('#transferId').val(),
        pageNumber: 1,
        pageSize: 10,
        userID: $('#userID').val()
    }

    reimburseAggregates(reimburseParam, escalatedParams);
    // v2 implementation start
})();
import OrderSvc from '../service/ordersService';
import Constants from '../common/constants';
import Util from '../common/util';
import TableWidget from '../widgets/table-widget';
import Storage from '../common/webStorage'

(function() {
    TableWidget.init();
    var totalCards = $(".payoutstable").length;
    // for(var i=0; i<totalCards; i++){
    //     let $currEle = $('#payoutsTable'+i);
    let orderPromArr = [];
    for (let currIndex = 0; currIndex < totalCards; currIndex++) {
        let $currEle = $('#payoutsTable' + currIndex);
        let param = {
            pageSize: 3,
            pageNumber: 1,
            token: Storage.get('authToken'),
            companyID: $("meta[name='companyID']").attr("content"),
            corpID: $("meta[name='corpID']").attr("content"),
            employeeID: $currEle.data('employee-id'),
            programID: $currEle.data('program-id')
        }
        orderPromArr.push(OrderSvc.getEmployeePayouts(param));
    }
    let employeePayouts = [];
    $.when.apply(null, orderPromArr).then(function() {
        if (orderPromArr.length === totalCards) {
            let respBusObj = {};
            for (let currIndex = 0; currIndex < orderPromArr.length; currIndex++) {
                let $currEle = $('#payoutsTable' + currIndex);
                respBusObj = orderPromArr[currIndex].responseJSON;
                if (respBusObj) {
                    employeePayouts.push(respBusObj);
                }
            }
        }
        for (let currIndex = 0; currIndex < employeePayouts.length; currIndex++) {
            let $currEle = $('#payoutsTable' + currIndex);
            let records = employeePayouts[currIndex].payouts.map(function(item) {
                item.payoutCompleteDate = Util.timeStampToDate(item.payoutCompleteDate);
                item.amount.amount = Util.formatINR(item.amount.amount / 100);
                item["primaryStatusClass"] = Constants.PAYOUT_STATUS_LABELS_CLASSES[item.status];
                item["primaryStatus"] = Constants.PAYOUT_STATUS_LABELS[item.status];
                item["secondaryStatus"] = Constants.PAYOUT_SECONDARY_STATUS_LABELS[item.status];
                item["errorMsg"] = Constants.PAYOUT_STATUS_MESSAGES[item.status];
                return item;
            });
            let responseJson = {
                totalCount: employeePayouts[currIndex].payoutCountByStatus.claimed,
                records: records
            };
            $currEle.tablewidget({
                source: responseJson,
                pageSize: 3,
                pageNumber: 1,
                tableTemplate: "/template/tables/transfer-detail-payouts.ejs",
                rowTemplate: "/template/tables/transfer-details-payout-row.ejs",
                extraParam: {}
            });
        }
    });

    /*$currEle.tablewidget({
        source: OrderSvc.getPayouts,
        pageSize: 3,
        pageNumber: 1,
        tableTemplate: "/template/tables/transfer-detail-payouts.ejs",
        rowTemplate: "/template/tables/transfer-details-payout-row.ejs",
        extraParam: {
        }
    });*/
    // }
}());

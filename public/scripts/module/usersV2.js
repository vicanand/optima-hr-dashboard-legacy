import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import UserAccessController from '../controller/userAccessCtrl';
import TableWidget from '../widgets/table-widget';
import AccessCtrlSvc from '../service/accessCtrlSvc';
import Filter from "../widgets/filter";

(function() {
    HeaderController.init();
    SidebarController.init();
    UserAccessController.init();
    Filter.init();
    TableWidget.init();
    $("#get-users-widget-V2").tablewidget({
        source: AccessCtrlSvc.getCorpAccountsV2,
        pageSize: 10,
        pageNumber: 1,
        tableTemplate: "/template/tables/usersV2/users.ejs",
        rowTemplate: "/template/tables/usersV2/users-row.ejs",
        searchSelector: $("#searchBox"),
        searchButton: $("#searchBtn"),
        filterSelect: $("#statusDropDown"),
    });
})();

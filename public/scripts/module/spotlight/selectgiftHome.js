import Filter from '../../widgets/filter';
import SelectGiftController from '../../controller/spotlight/selectgiftCtrl';
import SelectGiftSvc from '../../service/spotlight/selectgiftSvc';
import SpotlightController from '../../controller/spotlight/home-ctrl';
import Table from '../../widgets/table';
import TableWidget from '../../widgets/table-widget';
import selectGiftDOMEvents from '../../dom-events/spotlight/selectgiftDOMEvents';
import Util from '../../common/util';  
import Storage from '../../common/webStorage';

(function() {
    SelectGiftController.init();
    Table.init();
    TableWidget.init();
    Filter.init();
    $("#orderListTable").tablewidget({
        source: SelectGiftSvc.getSelectGiftOrderList,
        pageSize: 10,
        pageNumber: 1,
        tableTemplate: "../../../../template/section/spotlight/selectgifting/table/transfer-orders.ejs",
        rowTemplate: "../../../../template/section/spotlight/selectgifting/table/transfer-orders-row.ejs",
        searchSelector: $("#searchBox"),
        searchButton: $("#searchBtn"),
        filterSelect: $("#transferOrderStatus")
    });

    $('.popularbrandNxt').on('click',function(){
        let url = window.location.pathname;
        var brandNameX = true;
        let productId = $(this).data('brandid');
        let brandName = $(this).data('brandname');
        let brandLogoUrl = $(this).data('logourl');
        let brandIconUrl = $(this).data('iconurl');
        let discount = $(this).data('discount');
        let purchaseMin = $(this).data('purchasemin');
        let purchaseMax = $(this).data('purchasemax');
        var denomination = $(this).data('denomination');
        let purchasetype = $(this).data('purchasetype');
        if(denomination != undefined){
            var arraydenom = denomination.split(',');
            for (var i in arraydenom){
                arraydenom[i] = arraydenom[i]/100;
            }
            arraydenom = arraydenom.toString().replace(/,/g , ", ");
            }
        Storage.setSessionCollection('productId', productId);
        Storage.setSessionCollection('brandName', brandName);
        Storage.setSessionCollection('discount', discount);
        Storage.setSessionCollection('brandLogoUrl', brandLogoUrl);
        Storage.setSessionCollection('brandIconUrl', brandIconUrl);
        Storage.setSessionCollection('purchaseMin', purchaseMin/100);
        Storage.setSessionCollection('purchaseMax', purchaseMax/100);
        Storage.setSessionCollection('denomination', arraydenom);
        Storage.setSessionCollection('purchasetype', purchasetype);
        window.location.href = "" + url + "/selectgifting/newgiftorder?brand="+brandNameX;
    });
    
    let value = Util.getParameterByName('brand');
    if(value == 'true'){
        $('div[data-brandid = ' + Storage.getSessionCollection('productId') + ']').addClass('active');
        $('#chooseStep').addClass('done');
        $('#uploadStep').addClass('active');
        $('#chooseShortcodeCntr').hide();
        $('#fileErrorWrpr').hide();
        $('#chooseTxt').html();
        $('.denominationCont').hide();
        $('.denominationMin').text(Storage.getSessionCollection('purchaseMin'));
        $('.denominationMax').text(Storage.getSessionCollection('purchaseMax'));
        $('.brandLogoSg').attr('src', Storage.getSessionCollection('brandLogoUrl'));
        if( Storage.getSessionCollection('purchasetype').toLowerCase() != 'range'){
            $('.denominationCont').show();
            $('.denominationCont1').hide();
            $('.denomination').text(Storage.getSessionCollection('denomination'));
        }
        else{
            $('.denominationCont').hide();
            $('.denominationCont1').show();
           }
        $('#uploadFileCntr').show();

    }


function download_csv(csv, filename) {
  var csvFile;
  var downloadLink;

  // CSV FILE
  csvFile = new Blob([csv], {type: "text/csv"});

  // Download link
  downloadLink = document.createElement("a");

  // File name
  downloadLink.download = filename;

  // We have to create a link to the file
  downloadLink.href = window.URL.createObjectURL(csvFile);

  // Make sure that the link is not displayed
  downloadLink.style.display = "none";

  // Add the link to your DOM
  document.body.appendChild(downloadLink);

  // Lanzamos
  downloadLink.click();
}

function export_table_to_csv(html, filename) {
 var csv = [];
 var rows = $('#errorRowsTable')[0].querySelectorAll("table tr");
 
  for (var i = 0; i < rows.length; i++) {
    var row = [], cols = rows[i].querySelectorAll("td, th");
    
      for (var j = 0; j < cols.length; j++) {
      
          row.push('"' + cols[j].innerText.replace(/\n/g, ", ") + '"');
          }
      csv.push(row.join(","));
 }

  // Download CSV
  download_csv(csv.join("\n"), filename);
}

$('#downloadSGReport').on("click", function () {
  // alert('called'); 
  var html =$('#errorRowsTable')[0].outerHTML;
  //  document.querySelector("table").outerHTML;
 export_table_to_csv(html, "error-report.csv");
});


})();

import Filter from '../../widgets/filter';
import PhysicalCtrl from '../../controller/spotlight/physicalCtrl';
import PhysicalSvc from '../../service/spotlight/physicalSvc';
import Table from '../../widgets/table';
import TableWidget from '../../widgets/table-widget';
import Storage from '../../common/webStorage';
import daterangepicker from '../../../../node_modules/bootstrap-daterangepicker/daterangepicker';
import moment from '../../../lib/moment/moment';
import DomEventHandler from '../../common/domEventHandler';

import Meta from '../../common/metaStorage';


(function() {
    PhysicalCtrl.init(); 
    Table.init();
    TableWidget.init();
    Filter.init();
    
    var start = moment().subtract(29, 'days');
    var end = moment();
    
    function cb(start, end) {
        $('input[name="daterange"]').val('');
    }
    $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('input[name="daterange"]').daterangepicker({
        "autoApply": true,
        locale: {
            format: 'YYYY-MM-DD',
            "customRangeLabel": "Choose From Calendar",
          },
        "opens": "left",
        maxDate: new Date(),
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(0, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }     
    }, cb);
    cb(start, end); 

    $('#cardOrderHistory').click(function() {       
        $("#transferOrderTable").tablewidget({
            source: PhysicalSvc.getCardLoadOrders,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: "../../../../template/section/spotlight/physical/tab-content/tables/transfer-orders.ejs",
            rowTemplate: "../../../../template/section/spotlight/physical/tab-content/tables/transfer-orders-row.ejs",
            searchSelector: $("#searchBox"),
            searchButton: $("#searchBtn"),
            filterSelect: $("#transferOrderStatus"),
            startDate: $("#tranferPeriod"),
        });
    });

    
    $("#issueOrderHistory").click(function(){   
        $("#transferOrderTableIssue").tablewidget({
            source: PhysicalSvc.getCardIssueOrders,
            pageSize: 10,
            pageNumber: 1,
            tableTemplate: "../../../../template/section/spotlight/physical/tab-content/tables/transfer-orders-issue.ejs",
            rowTemplate: "../../../../template/section/spotlight/physical/tab-content/tables/transfer-orders-issue-row.ejs",
            searchSelector: $("#searchBoxIssue"),
            searchButton: $("#searchBtnIssue"),
            filterSelect: $("#transferOrderStatusIssue"),
            startDate: $("#tranferPeriodIssue"),
        });
    });
   

    DomEventHandler.bindClassEvent("resetFilter", "click", TableWidget.resetAllFilters);



    $('#uploadAgain').on('click',function(){
        $('#addBenefitsBulkPhysical').show();
        $('#fileErrorWrpr').hide();  
        history.pushState('', "", "#upload");
    });   

    $("select#fundingAccountID").change(function(){
      var newBalance = $('#fundingAccountID option:selected').attr('data-balance');
      var fundingaccountname = $('#fundingAccountID option:selected').attr('data-name');
      var id = $('#fundingAccountID option:selected').val();
      $('#balanceFund').html(newBalance);
      Storage.setSessionCollection('fundingaccountid',id);
      Storage.setSessionCollection('fundingaccountbalance',newBalance);
      Storage.setSessionCollection('fundingaccountname',fundingaccountname);
      $('#createOrderDone').attr('data-fundingaccountids',id);
        if(newBalance < 1 ){
            $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').show();
            $('.placeOrderEnd').hide();
        }
        else{
            $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').hide();
            $('.placeOrderEnd').show();
        }
    });  


    $('#fundingAccountID').on('change',function(){
        var newBalance = $('#fundingAccountID option:selected').attr('data-balance');
        $('#fundingAccountID option:selected').val() == 0 ? $('.fundCurrntBalance').hide() : $('.fundCurrntBalance').show();
        if(newBalance < Math.floor(Number(localStorage.getItem('totalAmount')/100))){
            $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').show();
            $('.placeOrderEnd').hide();
        }
        else{
            $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').hide();
            $('.placeOrderEnd').show();
        }
        if (($('#createGiftCheckbox').is(':checked')) && ($('#fundingAccountID option:selected').val() != 0)) {
            $('#orderCompleted').removeAttr('disabled').removeClass('btn-disabled').css('background','#FFCB39');
            $('#orderCompletedNew').removeAttr('disabled').removeClass('btn-disabled').css('background','#FFCB39');
            $('.fundCurrntBalance').show()
            $('#balanceFund').html(newBalance);
        } else {
            $('#orderCompleted').attr('disabled', 'disabled').addClass('btn-disabled').css('background','#ccc');     
            $('#orderCompletedNew').attr('disabled', 'disabled').addClass('btn-disabled').css('background','#ccc');   
            }
    });
  
    
    $('#createGiftCheckbox').on('click', function() {
        if (($(this).is(':checked')) && ($('#fundingAccountID option:selected').val() != 0)) {
            $('#orderCompleted').removeAttr('disabled').removeClass('btn-disabled').css('background','#FFCB39');
            $('#orderCompletedNew').removeAttr('disabled').removeClass('btn-disabled').css('background','#FFCB39');
        } else {
            $('#orderCompleted').attr('disabled', 'disabled').addClass('btn-disabled').css('background','#ccc');     
            $('#orderCompletedNew').attr('disabled', 'disabled').addClass('btn-disabled').css('background','#ccc');
        }
    });   
    setTimeout(()=>{
        $('#resetFilter').hide();
    },1000)
    setTimeout(()=>{
        $('.resetFilter').hide();
    },700)

    function download_csv(csv, filename) {
        var csvFile;
        var downloadLink;
    
        // CSV FILE
        csvFile = new Blob([csv], {type: "text/csv"});
    
        // Download link
        downloadLink = document.createElement("a");
    
        // File name
        downloadLink.download = filename;
    
        // We have to create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);
    
        // Make sure that the link is not displayed
        downloadLink.style.display = "none";
    
        // Add the link to your DOM
        document.body.appendChild(downloadLink);
    
        // Lanzamos
        downloadLink.click();
    }
    
    function export_table_to_csv(html, filename) {
       var csv = [];
       var rows = $('#errorVirtualGiftingTransfers1')[0].querySelectorAll("table tr");
       
        for (var i = 0; i < rows.length; i++) {
          var row = [], cols = rows[i].querySelectorAll("td, th");
          
            for (var j = 0; j < cols.length; j++) {
            
                row.push('"' + cols[j].innerText.replace(/\n/g, ", ") + '"');
                }
            csv.push(row.join(","));
       }
    
        // Download CSV
        download_csv(csv.join("\n"), filename);
    }
    
    $('#downloadSGReport').on("click", function () {
        // alert('called'); 
        var html =$('#errorVirtualGiftingTransfers1')[0].innerHTML;
        //  document.querySelector("table").outerHTML;
       export_table_to_csv(html, "error-report.csv");
    });

    initGAEvent('event', 'Physical Gift Card', 'click', 'Home, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+',timestamp=' + new Date().toLocaleString());
})();

import Filter from '../../widgets/filter';
import FilterWidget from '../../widgets/filter-widget';
import PointbasedCtrl from '../../controller/spotlight/pointbasedCtrl';
import PointbasedSvc from '../../service/spotlight/pointbasedSvc';
import TableWidget from '../../widgets/table-widget';
import HeaderController from '../../controller/headerCtrl';
import SidebarController from '../../controller/sidebarCtrl';
import Meta from '../../common/metaStorage';
import Storage from '../../common/webStorage';


(function () {
    HeaderController.init();
    SidebarController.init();
    PointbasedCtrl.init();
    PointbasedCtrl.initDatePicker();
    TableWidget.init();
    FilterWidget.init();
    Filter.init();

    function renderTable(currentHash) {
        switch (currentHash) {
            case 'cardOrderHistory':
                $("#transferOrderTable").tablewidget({
                    source: PointbasedSvc.getRewardActivity,
                    pageSize: 10,
                    pageNumber: 1,
                    tableTemplate: "../../../../../../../template/section/spotlight/pointbased/tab-content/tables/transfer-orders-issue.ejs",
                    rowTemplate: "../../../../../../../template/section/spotlight/pointbased/tab-content/tables/transfer-orders-issue-row.ejs",
                    searchSelector: $("#searchBoxReward"),
                    searchButton: $("#searchBtnReward"),
                    filterSelect: $("#transferOrderStatus"),
                    startDate: $("#tranferPeriodHistory"),
                });
                break;
            case 'issuance':
                $("#rewarded-history-table-render").tablewidget({
                    source: PointbasedSvc.getRewardIssuedList,
                    pageSize: 10,
                    pageNumber: 1,
                    tableTemplate: "../../../../../../../template/section/spotlight/pointbased/reward-issuance-history-table/rewards-issued.ejs",
                    rowTemplate: "../../../../../../../template/section/spotlight/pointbased/reward-issuance-history-table/rewards-issued-row.ejs",
                    searchSelector: $("#searchissuanceBox"),
                    searchButton: $("#searchissuanceBtn"),
                    filterSelect: $("#issuance-filter"),
                    startDate: $("#issuancetranferPeriod"),
                });
                break;
            case 'overview':
                $("#rewarded-table-render").tablewidget({
                    source: PointbasedSvc.getRewardIssuedSummary,
                    pageSize: 10,
                    pageNumber: 1,
                    tableTemplate: "../../../../../../../template/section/spotlight/pointbased/reward-issued-table/rewards-issued.ejs",
                    rowTemplate: "../../../../../../../template/section/spotlight/pointbased/reward-issued-table/rewards-issued-row.ejs",
                    searchSelector: $("#searchBox"),
                    searchButton: $("#searchBtn"),                    
                    startDate: $("#tranferPeriod"),
                });
                break;
            case 'policies':
                    var tableTemplateUrl = '../../../../../../../template/section/spotlight/pointbased/reward-setup-table/reward-setup-table.ejs';
                    if(localStorage.getItem('SELECTED_CORP_ID') == '17573' || localStorage.getItem('SELECTED_CORP_ID') == '96' || localStorage.getItem('SELECTED_CORP_ID') == '4803'){
                        tableTemplateUrl = '../../../../../../../template/section/spotlight/pointbased/reward-setup-table/reward-setup-zeta-table.ejs'
                    }
                $("#rewarded-setup-table-render").tablewidget({
                    source: PointbasedSvc.getSetupRewardList,
                    pageSize: 100,
                    pageNumber: 1,
                    tableTemplate: tableTemplateUrl,
                    rowTemplate: "../../../../../../../template/section/spotlight/pointbased/reward-setup-table/reward-setup-table-row.ejs",
                    searchSelector: $("#searchBox"),
                    searchButton: $("#searchBtn"),                    
                    startDate: $("#tranferPeriod"),
                });
                break;            
            case 'beneficiary':
                $("#beneficiary-table-render").tablewidget({
                    source: PointbasedSvc.getBeneficiary,
                    pageSize: 10,
                    pageNumber: 1,
                    tableTemplate: "../../../../../../../template/section/spotlight/pointbased/beneficiarytable/beneficiary.ejs",
                    rowTemplate: "../../../../../../../template/section/spotlight/pointbased/beneficiarytable/beneficiary-row.ejs",
                    searchSelector: $("#searchBeneficiaryBox"),
                    searchButton: $("#searchBeneficiaryBtn"),                    
                    filterSelect: $("#beneficiary-filter") 
                });
                break;            
            case 'revokeRequest':
                    var tableTemplateUrl = "../../../../../../../template/section/spotlight/pointbased/revokeRequestTable/revoke-request.ejs";
                    if(localStorage.getItem('SELECTED_CORP_ID') == '17573' || localStorage.getItem('SELECTED_CORP_ID') == '96' || localStorage.getItem('SELECTED_CORP_ID') == '4803'){
                        tableTemplateUrl = "../../../../../../../template/section/spotlight/pointbased/revokeRequestTable/revoke-zeta-request.ejs"
                    }
                $("#revoke-request-table-render").tablewidget({
                    source: PointbasedSvc.getRevokeRequests,
                    pageSize: 10,
                    pageNumber: 1,
                    tableTemplate: tableTemplateUrl,
                    rowTemplate: "../../../../../../../template/section/spotlight/pointbased/revokeRequestTable/revoke-request-row.ejs",
                    searchSelector: $("#searchRevokeBox"),
                    searchButton: $("#searchRevokeBtn"),                    
                });
                break;                        
            case 'revokeTransactionHistory':
                    var tableTemplateUrl = "../../../../../../../template/section/spotlight/pointbased/revokeTransactionTable/revoke-transaction.ejs";
                    if(localStorage.getItem('SELECTED_CORP_ID') == '17573' || localStorage.getItem('SELECTED_CORP_ID') == '96' || localStorage.getItem('SELECTED_CORP_ID') == '4803'){
                        tableTemplateUrl = "../../../../../../../template/section/spotlight/pointbased/revokeTransactionTable/revoke-zeta-transaction.ejs"
                    }
                $("#revoke-transaction-table-render").tablewidget({
                    source: PointbasedSvc.getRevokeTransactionHistory,
                    pageSize: 10,
                    pageNumber: 1,
                    tableTemplate: tableTemplateUrl,
                    rowTemplate: "../../../../../../../template/section/spotlight/pointbased/revokeTransactionTable/revoke-transaction-row.ejs",                 
                });
                break;        
                default:
                        $('#overview').trigger('click');
                break;
        }
    }


    function destroyTable(prev) {
        try {
            switch (prev) {
                case 'cardOrderHistory':
                    $("#transferOrderTable").tablewidget('destroy');
                    TableWidget.resetAllFilters();
                    $('#searchBoxReward').val('');
                    break;
                case 'issuance':
                    $("#rewarded-history-table-render").tablewidget('destroy');
                    TableWidget.resetAllFilters();
                    $('#searchissuanceBox').val('');
                    break;
                case 'overview':
                    $("#rewarded-table-render").tablewidget('destroy');
                    TableWidget.resetAllFilters();
                    $('#searchBox').val('');
                    break;
                case 'policies':
                    $("#rewarded-setup-table-render").tablewidget('destroy');
                    TableWidget.resetAllFilters();
                    break;
                case 'beneficiary':
                    $("#beneficiary-table-render").tablewidget('destroy');                     
                     $('#searchBeneficiaryBox').val('')
                     TableWidget.resetAllFilters();
                    break;
                case 'revokeRequest':
                    $("#revoke-request-table-render").tablewidget('destroy');                     
                        $('#searchRevokeBox').val('')
                        TableWidget.resetAllFilters();
                    break; 
                case 'revokeTransactionHistory':
                    $("#revoke-transction-table-render").tablewidget('destroy');                     
                        TableWidget.resetAllFilters();
                    break;    
                default:
                    break;
            }
        } catch (e) {
            $('#overview').trigger('click');
        }
    }

    (function () {
        $('.sub-container').hide();
        let currentUrl = window.location.href;
        if (currentUrl.indexOf('#') !== -1) {
            var selectedId = currentUrl.substr(currentUrl.lastIndexOf('#'), currentUrl.length);
        }
        $('.nav.nav-tabs > li > a').click(function (e) {
            var data = $(this).attr("id");
            var previousActive = localStorage.getItem('currentActive');
            console.log('previousActive', previousActive);
            localStorage.setItem('currentActive', data);
            if (history.pushState) {
                history.pushState(null, null, '#' + data);

            } else {
                window.location.hash = '#' + data;
            }
            if (previousActive !== localStorage.getItem('currentActive')) {
                $('.sub-container').show();
                renderTable(localStorage.getItem('currentActive'));
                destroyTable(previousActive);
            } else {
                renderTable(previousActive);
                // destroyTable(previousActive);
            }
        });
        window.location.hash == "" ? $('#overview').parent().addClass('active') : '';
        $("document").ready(function () {
            if ($("#progress-top").length === 0) {
                $("body").append($("<div><b></b><i></i></div>").attr("id", "progress-top"));
                $("#progress-top").width("101%").delay(800).fadeOut(1000, function () {
                    $(this).remove();
                });
            }
            $(selectedId).trigger('click');
            $('.tabContainer').css('display', 'block');
            if (!window.location.hash) {
                $('#overview').parent().addClass('active');
            }
            $('body').on('click', '#revokeBadges', function() {
                let issuerEmployeeID = $(this).attr('data-employeeid');
                let programID = $(this).attr('data-programid');
                let badgeCountMap = JSON.parse($(this).attr('data-remainbatch'));
                Storage.set('issuerEmployeeID', issuerEmployeeID);
                Storage.set('programID', programID);
                Storage.set('badgeCountMap', badgeCountMap);             
                $('#revokeBadgePayoutModal').modal('show');
            });
        });
        setTimeout(function () {
            $('html,body').animate({ scrollTop: 0 }, 'fast');  
            $('.sub-container').show();
        }, 350);  

    })();

    initGAEvent('event', 'point based Gift Card', 'click', 'Home, corpId = ' + Meta.getMetaElement("corpID") + ', companyId=' + Meta.getMetaElement('companyID') + ', userId=' + Meta.getMetaElement('userID') + ',timestamp=' + new Date().toLocaleString());
})();
//import HeaderController from '../../controller/headerCtrl';
import SpotlightController from '../../controller/spotlight/home-ctrl';
import Meta from '../../common/metaStorage';


(function() {
    //HeaderController.init();
    SpotlightController.init();

    //for home page tabs

$(document).ready(function(){
    $('#sptIR').fadeOut();
    $('#sptGCV').fadeIn();
    $('.sptGCV').on('click',function(){
        $('.sptTabsBtn').removeClass('active');
        $(this).addClass('active');
        $('#sptIR').fadeOut(300,"linear");
        $('#sptGCV').fadeIn(600,"linear");
        initGAEvent('event', 'Spotlight Home', 'click', 'Gift Cards and Vouchers - Tab visited, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+',timestamp=' + new Date().toLocaleString());
    });
    $('.sptIR').on('click',function(){
        $('.sptTabsBtn').removeClass('active');
        $(this).addClass('active');
        $('#sptGCV').fadeOut(300,"linear");
        $('#sptIR').fadeIn(600,"linear");
        initGAEvent('event', 'Spotlight Home', 'click', 'Rewards and Recognition - Tab visited, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+',timestamp=' + new Date().toLocaleString());
    });
  });


})();

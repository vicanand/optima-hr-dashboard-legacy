import HeaderController from '../../controller/headerCtrl';
import SidebarController from '../../controller/sidebarCtrl';
import TransferDetail from "../../controller/transferDetailCtrl";
import TableWidget from '../../widgets/table-widget';
import FilterWidget from '../../widgets/filter-widget';


(function() {
    TableWidget.init();
    HeaderController.init();
    SidebarController.init();
    TransferDetail.init();
    FilterWidget.init();






    // $('#criteriaFilter').filterDropdown({
    //     'isMultiple' : false,
    //     'source' : ['Atmos Energy Corporation','Avaya Inc.','BellSouth Corporation','Boston Scientific Corporation','Bemis Company, Inc.','Cisco Systems Inc.','Capital One Financial Corp.'],
    //     'filterLabel' : 'Companies',
    //     'selected' : ''
    // });
}());

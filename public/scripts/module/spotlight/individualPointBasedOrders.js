import HeaderController from '../../controller/headerCtrl';
import SidebarController from '../../controller/sidebarCtrl';
import TableWidget from '../../widgets/table-widget';
import PointbasedSvc from '../../service/spotlight/pointbasedSvc';
import Filter from "../../widgets/filter";
import Storage from '../../common/webStorage';
import DomEventHandler from '../../common/domEventHandler';
import IndividualVirtualDOMEvents from '../../dom-events/spotlight/individual-virtual-dom-event';

(function () {
  HeaderController.init();
  SidebarController.init();
  TableWidget.init();
  Filter.init();

  $("#individual-order").tablewidget({
    source: PointbasedSvc.getRewardSinglePayoutList,
    pageSize: 10,
    pageNumber: 1,
    tableTemplate: "/template/section/spotlight/pointbased/individual-order-table/individual-transfer-table.ejs",
    rowTemplate: "/template/section/spotlight/pointbased/individual-order-table/individual-transfer-table-row.ejs",
    searchSelector: $("#searchissuanceBox"),
    searchButton: $("#searchissuanceBtn"),
    filterSelect: $("#issuance-filter"),
    secondaryFilterSelect: $("#programsBenifits")
  });

  $(function () {
    $('[data-toggle="popover"]').popover();
  });
  //  DomEventHandler.bindClassEvent('confirm', 'click',  virtualSvc.revokePayoutsInstaNew);
  //  DomEventHandler.bindEvent('reissuePayoutDOM', 'click', IndividualVirtualDOMEvents.reissue);        
  //  DomEventHandler.bindEvent('email', 'blur', IndividualVirtualDOMEvents.validateEmail);     
  //  DomEventHandler.bindEvent('userName', 'blur', IndividualVirtualDOMEvents.validateUserName);
  //  DomEventHandler.bindEvent('mobile', 'blur', IndividualVirtualDOMEvents.mobileValidation);     
  //  DomEventHandler.bindClassEvent('reload-btn', 'click', IndividualVirtualDOMEvents.reloadPage);



})();
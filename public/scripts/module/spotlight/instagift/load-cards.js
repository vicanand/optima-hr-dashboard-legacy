import HeaderController from '../../../controller/headerCtrl';
import SidebarController from '../../../controller/sidebarCtrl';
import LoadCardsController from '../../../controller/spotlight/instagift/load-cards-ctrl';


(function() {
    HeaderController.init();
    SidebarController.init();
    LoadCardsController.init();
})();


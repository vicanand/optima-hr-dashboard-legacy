import HeaderController from '../../../controller/headerCtrl';
import SidebarController from '../../../controller/sidebarCtrl';
import TransferOrdersController from '../../../controller/spotlight/instagift/transfer-orders-ctrl';
import Table from '../../../widgets/table';
import InstagiftSvc from '../../../service/spotlight/instagift-svc';
import FilterWidget from '../../../widgets/filter-widget';
import benefitDetailsDomEvents from "../../../dom-events/transferDetailsDomEvents";
import Util from '../../../common/util';

HeaderController.init();
SidebarController.init();
TransferOrdersController.init();
Table.init();
FilterWidget.init();

$("#transferOrderTable").table({
    source: InstagiftSvc.getOrders,
    pageSize: 10,
    pageNumber: 1,
    tableTemplate: '/template/section/spotlight/instagift/tab-content/tables/transfer-orders.ejs',
    rowTemplate: '/template/section/spotlight/instagift/tab-content/tables/transfer-orders-row.ejs',
    extraParam: {
        productType: $("#productType").val().toLowerCase(),
        statusFilters: $('#transferOrderStatus')
    },
    filterSelect: $('#transferOrderStatus')
});

(function () {
    let params = {
        pageSize: 1,
        pageNumber: 1
    }
    InstagiftSvc.getResetPinRequests(params).then(function(res) {
        if (res.resetPinCountByStatus.PROCESSING) {
            $("#requestNumber").html(res.resetPinCountByStatus.PROCESSING + " PENDING REQUESTS");
            $(".request").show();
        }
    });
})();

$('#transferOrderTable').on('click', '.orders-row td:not(.file-url)', function(event) {
    let orderId = $(this).closest('.orders-row').data('orderid');
    let programID = $('#transferOrderTable').data('programid');
    let newurl = window.location.href;
    newurl = newurl.substring(0, newurl.indexOf('#'));
    console.log(newurl);
    let url = newurl + '/transfers/' + orderId;
    window.location.href = url;
});

$('#transferOrderTable').on('click', '.download-report', function(event) {
    var _self = this;
    $(_self).addClass('report-loader');
    var orderID = _self.dataset.reportorderid;
    InstagiftSvc.downloadReport(orderID).then(function(responseData) {
        $(_self).removeClass('report-loader');
        if (responseData && responseData.state == "SUCCESS") {
            window.open(Util.htmlDecoder(responseData.url), "_self");
        }
    }, function(respErr) {
        $(_self).removeClass('report-loader');
        $("#Errorstate").modal('show');
    });
});



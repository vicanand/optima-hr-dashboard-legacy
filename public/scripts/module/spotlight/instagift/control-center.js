import HeaderController from '../../../controller/headerCtrl';
import SidebarController from '../../../controller/sidebarCtrl';
import ResetPinController from "../../../controller/spotlight/instagift/reset-pin-request";
import InstagiftSvc from '../../../service/spotlight/instagift-svc';
import TableWidget from '../../../widgets/table';
import FilterWidget from '../../../widgets/filter-widget';


(function() {
    TableWidget.init();
    HeaderController.init();
    SidebarController.init();
    ResetPinController.init();
    FilterWidget.init();

    $("#resetPinRequestTable").table({
		source: InstagiftSvc.getResetPinRequests,
		pageSize: 10,
		pageNumber: 1,
		tableTemplate: "/template/section/spotlight/instagift/control-center/tables/reset-pin-request.ejs",
		rowTemplate: "/template/section/spotlight/instagift/control-center/tables/reset-pin-request-row.ejs",
		extraParam: {
		    statusFilters: ''
		},
		filterSelect: $("#statusFilter")
	});
}());

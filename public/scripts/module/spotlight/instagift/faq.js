import HeaderController from '../../../controller/headerCtrl';
import SidebarController from '../../../controller/sidebarCtrl';
import ServiceConnector from '../../../common/serviceConnector';
import AnalyticsSvc from '../../../service/analytics/analytics-svc';


$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  	let target = $(e.target);
  	let faqID = target[0].dataset.faqid;
  	let eventName   = "faq.click";
	let userID      = $("meta[name='userID']").attr("content");
	let corp_ID     = $("meta[name='corpID']").attr("content");
	let company_ID  = $("meta[name='companyID']").attr("content");
	let eventLogs   = [
	    {
	        "serviceName": "instagift",
	        "eventName": eventName,
	        "userID_longValue": userID,
	        "corpID_longValue": corp_ID,
	        "companyID_longValue": company_ID,
	        "faqID": faqID
	    }
	];
	AnalyticsSvc.recordEvents(eventLogs);
});
import HeaderController from '../../controller/headerCtrl';
import SidebarController from '../../controller/sidebarCtrl';
import TableWidget from '../../widgets/table-widget';
import virtualSvc from '../../service/spotlight/virtualSvc';
import Filter from "../../widgets/filter";
import Storage from '../../common/webStorage';
import DomEventHandler from '../../common/domEventHandler';
import IndividualVirtualDOMEvents from '../../dom-events/spotlight/individual-virtual-dom-event';

(function() {
    HeaderController.init();
    SidebarController.init();
    TableWidget.init();
    Filter.init();
  
    $("#transferOrderTable").tablewidget({
        source: virtualSvc.getIndividualOrdersRow,
        pageSize: 10,
        pageNumber: 1,
        tableTemplate: "/template/section/spotlight/virtual/individual-order-table/individual-transfer-orders.ejs",
        rowTemplate: "/template/section/spotlight/virtual/individual-order-table/individual-transfer-orders-row.ejs",
        searchSelector: $("#searchBox"),
        searchButton: $("#searchBtn"),
        filterSelect: $("#transferOrderStatus"),
        secondaryFilterSelect: $("#programsBenifits")       
    });

    $("#transferOrderTablePhysical").tablewidget({
        source: virtualSvc.getIndividualOrdersRow,
        pageSize: 10,
        pageNumber: 1,
        tableTemplate: "/template/section/spotlight/physical/individual-order-table/individual-transfer-orders.ejs",
        rowTemplate: "/template/section/spotlight/physical/individual-order-table/individual-transfer-orders-row.ejs",
        searchSelector: $("#searchBox"),
        searchButton: $("#searchBtn"),
        filterSelect: $("#transferOrderStatus"),
        secondaryFilterSelect: $("#programsBenifits")       
    });

     DomEventHandler.bindClassEvent('confirm', 'click',  virtualSvc.revokePayoutsInstaNew);
     DomEventHandler.bindEvent('reissuePayoutDOM', 'click', IndividualVirtualDOMEvents.reissue);        
     DomEventHandler.bindEvent('email', 'blur', IndividualVirtualDOMEvents.validateEmail);     
     DomEventHandler.bindEvent('userName', 'blur', IndividualVirtualDOMEvents.validateUserName);
     DomEventHandler.bindEvent('mobile', 'blur', IndividualVirtualDOMEvents.mobileValidation);     
     DomEventHandler.bindClassEvent('reload-btn', 'click', IndividualVirtualDOMEvents.reloadPage);
     
     $('body').on('keyup', '#revokeRemark', function() {
        var remark = $('#revokeRemark').val();
        if(remark.length >0){
            $('.confirm').removeAttr('disabled').removeClass('btn-disabled');
        }
        else{
            $('.confirm').attr('disabled','disabled').addClass('btn-disabled');
        }
     });


    $('body').on('click', '.revokePaymentLink', function() {
        $('#revokePayoutConfirm').modal('show');
        var orderID = $(this).attr('data-orderid');
        var payoutID= $(this).attr('data-payoutid');
        var program= $(this).attr('data-program');
        Storage.set('payoutID',payoutID);
        Storage.set('orderID',orderID);
        Storage.set('program',program);
       });
    
       $('body').on('click', '.reset', function() {
        $('#revokePayoutConfirm').modal('hide');
        Storage.set('payoutID','');
        Storage.set('orderID','');
        Storage.set('program','');
       });


       //for Reissue DOM 
       $('body').on('click', '.reissuePayout', function() {  
           
        let ifi = $('#SpotlightIFIName').val();

        if(ifi.toLowerCase() === 'hdfc'){
            $('.ifihdfc-show').show()
        }else{
            $('.ifi-other-show').show();
        }

        var orderID = $(this).attr('data-orderid');
        var payoutID= $(this).attr('data-payoutid');
        var program= $(this).attr('data-program');
        $('#show-error-msg').css('opacity',0);   
        $('#userName').val('');
        $('#email').val('');
        $('#mobile').val('');    
        $('#reason').val('');    
        $('#userName').removeClass('input-has-error');
        $('#userName').parents('.form-group').find('.error-message').html('');
        $('#email').removeClass('input-has-error');
        $('#email').parents('.form-group').find('.error-message').html('');
        $('#mobile').removeClass('input-has-error');
        $('#mobile').parents('.form-group').find('.error-message').html('');

        $('#reissuePayoutModalShow').modal('show');
        Storage.set('reissuepayoutID',payoutID);
        Storage.set('reissueorderID',orderID);
        Storage.set('reissueprogram',program);
       });

       $('body').on('click','.reissue-to-list', function() {
        let ifi = $('#SpotlightIFIName').val();
        $('.reissueToEmail').hide()
        $('.reissueToMobile').hide()
        $('.reissueToEmailorMobile').hide();
        
        try {
            let attributes = $(this).data('attributes');
            console.log(attributes);
                $('#reissueToOrderId').html($('#get-oreder-id').html())
                $('#reissueToName').html(attributes[attributes.length-1].name);                      
    
                if(ifi.toLowerCase() === 'hdfc'){
                    $('#reissueToEmail').html(attributes[attributes.length-1].email);
                    $('#reissueToMobile').html(attributes[attributes.length-1].contact);
                    $('.reissueToEmail').show()
                    $('.reissueToMobile').show()
                }else{
                    $('#reissueToEmailorMobile').html(attributes[attributes.length-1].contact ? attributes[attributes.length-1].contact : attributes[attributes.length-1].email ? attributes[attributes.length-1].email : '-');
                    $('.reissueToEmailorMobile').show();
                }
    
                $('#reissueToPayoutSuccess').modal('show');
        } catch(e){
            $('#reissuePayoutError').modal('show');
        }
        })


        


})();

import Filter from '../../widgets/filter';
import RewardCtrl from '../../controller/spotlight/rewardCtrl';
import RewardsSvc from '../../service/spotlight/rewardSvc';
import Table from '../../widgets/table';
import TableWidget from '../../widgets/table-widget';
import Storage from '../../common/webStorage';
import daterangepicker from '../../../../node_modules/bootstrap-daterangepicker/daterangepicker';
import moment from '../../../lib/moment/moment';
import DomEventHandler from '../../common/domEventHandler';
import Meta from '../../common/metaStorage';
 
(function() {
    RewardCtrl.init();
    Table.init();
    TableWidget.init();
    Filter.init();
    $("#transferOrderTableX").tablewidget({
        source: RewardsSvc.getRewardOrderList,
        pageSize: 10,
        pageNumber: 1,
        tableTemplate: "../../../../template/section/spotlight/reward/tab-content/tables/transfer-orders.ejs",
        rowTemplate: "../../../../template/section/spotlight/reward/tab-content/tables/transfer-orders-row.ejs",
        searchSelector: $("#searchBox"),
        searchButton: $("#searchBtn"),
        filterSelect: $("#transferOrderStatus"),
        startDate: $('#tranferPeriod')
    });
  
    $("#transferBenefitTable").table({
        source: RewardsSvc.getProgramBenifitEmployeesV2,
        pageSize: 10,
        pageNumber: 1,
        tableTemplate: "../../../../template/section/spotlight/reward/tab-content/tables/transfer-benefitforders.ejs",
        rowTemplate: "../../../../template/section/spotlight/reward/tab-content/tables/transfer-benefitforders-row.ejs",
        searchSelector: $("#searchBenefitBox"),
        searchButton: $("#searchBenefitBtn"),
        filterSelect: $("#transferOrderStatus1")
    });

    $('#cutomCardName').keyup(function() { 
        var cutomCardName = $('#cutomCardName').val();
        console.log(cutomCardName);
        Storage.setSessionCollection('cutomCardName', cutomCardName);
        $('#cutomCardNameShow').text( Storage.getSessionCollection('cutomCardName'));
        $('#cutomCardNameShowX').text( Storage.getSessionCollection('cutomCardName'));
        validationForCustomOrder();
     });
     
    $('#cutomCardTitle').keyup(function() { 
        var cutomCardTitle = $('#cutomCardTitle').val();
        console.log(cutomCardTitle);
        Storage.setSessionCollection('cutomCardTitle', cutomCardTitle);
        $('#cutomCardTitleShow').text( Storage.getSessionCollection('cutomCardTitle')+ '  &lt;Beneficiary Name&gt; ' );
        validationForCustomOrder();
    });

    
    $('#cutomCardMessage').keyup(function() {
        var cutomCardMessage = $.trim($(this).val());
        console.log(cutomCardMessage);
        Storage.setSessionCollection('cutomCardMessage', cutomCardMessage);
        $('#cutomCardMessageShow').text( Storage.getSessionCollection('cutomCardMessage'));
        validationForCustomOrder();
    });  

    $("select#fundingAccountID").change(function(){
      var newBalance = $('#fundingAccountID option:selected').attr('data-balance');
      var id = $('#fundingAccountID option:selected').val();
      $('#balanceFund').html(newBalance);
      Storage.setSessionCollection('fundingID',id);
        if(newBalance < 1 ){
            $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').show();
            $('.placeOrderEnd').hide();
        }
        else{
            $('.placeOrderEndAddFund, .insuffFund, .insuffFundText').hide();
            $('.placeOrderEnd').show();
        }
    });

     let  validationForCustomOrder = function() {
            console.log($('.templateChoose').hasClass('active'));
           if(($('#cutomCardName').val().length > 0) && ($('.templateChoose').hasClass('active') == true)){
               $('#openEmail').removeAttr('disabled').removeClass('btn-disabled');
               console.log('nextac');
           }
           else{
               $('#openEmail').attr('disabled','disabled').addClass('btn-disabled');
           }
   
           if (($('#cutomCardName').val().length > 0) && ($('#cutomCardTitle').val().length > 0) && ($('#cutomCardMessage').val().length > 0) && ($('.templateChoose').hasClass('active') == true)) {
               $('#createCustomCard').removeAttr('disabled').removeClass('btn-disabled');
               console.log('creste');
           }
           else{
               $('#createCustomCard').attr('disabled','disabled').addClass('btn-disabled');
           }
    };
  
    

    //Date Picker
    var start = moment().subtract(365, 'days');
    console.log(start);
    var end = moment();    
    function cb(start, end) {
        $('input[name="daterange"]').val('');
    }
    $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('input[name="daterange"]').daterangepicker({
        "autoApply": true,
        locale: {
            format: 'YYYY-MM-DD',
            "customRangeLabel": "Choose From Calendar",
          },
        "opens": "left",
        maxDate: new Date(),
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(2, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }     
    }, cb);

    cb(start, end);
    DomEventHandler.bindEvent("resetFilter", "click", TableWidget.resetAllFilters); 
    setTimeout(()=>{
        $('#resetFilter').hide();
    },1000)

    initGAEvent('event', 'Reward - Card-based Home Page', 'click', 'Home, corpId = ' + Meta.getMetaElement("corpID") + ', companyId='+ Meta.getMetaElement('companyID') + ', userId='+Meta.getMetaElement('userID')+',timestamp=' + new Date().toLocaleString());

})();

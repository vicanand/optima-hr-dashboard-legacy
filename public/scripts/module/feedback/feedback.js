import HeaderController from '../../controller/headerCtrl';
import SidebarController from '../../controller/sidebarCtrl';
import FeedbackController from '../../controller/feedback/feedbackCtrl';
(function() {
    HeaderController.init();
    SidebarController.init();
    FeedbackController.cityinit();      
    FeedbackController.officeinit();  
    FeedbackController.storeinit();  
    FeedbackController.datepicker();
    FeedbackController.bar_graph();
    FeedbackController.customer_comment();
    FeedbackController.good_bits();
    FeedbackController.bad_bits();
    FeedbackController.save_stores();
    FeedbackController.scroll();
    FeedbackController.initial_date_capture();
})();
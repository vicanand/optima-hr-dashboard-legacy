import HeaderController from '../controller/headerCtrl';
import SidebarController from '../controller/sidebarCtrl';
import CashlessController from '../controller/cashlessCtrl';

(function() {
   
    HeaderController.init();
    SidebarController.init();
    CashlessController.init();
    CashlessController.loadDatePicker();
})();

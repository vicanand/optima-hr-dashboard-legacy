import HeaderController from '../controller/headerCtrl';
import ReportCentreController from '../controller/reportCentreCtrl';
import SidebarController from '../controller/sidebarCtrl';

(function () {
    HeaderController.init();
    SidebarController.init();
    ReportCentreController.init();
    ReportCentreController.loadDatePicker();

    let currentUrl = window.location.href;
    let selectedId; 

    if (currentUrl.indexOf('#') !== -1) {
        selectedId = currentUrl.substr(currentUrl.lastIndexOf('#')+1, currentUrl.length);
    }

    $("document").ready(function () {
        if (selectedId) {
            $('#'+selectedId).trigger('click');
        }
        $('.tabContainer').css('display', 'block');
    });

    window.onbeforeunload = function () {
        window.scrollTo(0, 0);
    }

    window.onscroll = function () {
        myFunction()
    };

    var navbar = document.getElementById("verticalNavbar"),
        noticeHeight,
        notice = $(".container-fluid.kyc-notice");

    if(notice && notice[0]){
        noticeHeight = notice[0].offsetHeight
    }
    else{
        noticeHeight = 0
    }


    var scrollTop = $(window).scrollTop(),
        elementOffset = $('#verticalNavbar').offset().top,
        distance = (elementOffset - scrollTop) - 91 - noticeHeight;

    function myFunction() {
        if (window.pageYOffset >= distance) {
            navbar.classList.add("sticky")
        } else {
            navbar.classList.remove("sticky");
        }
    }

    // $(document).ready(function () {
    //     var $sections = $('.sectionHeading');
    //     // $(window).scroll(function () {
    //     //     var currentScroll = $(this).scrollTop();
    //     //     var $currentSection;
    //     //     $sections.each(function () {
    //     //         var divPosition = $(this).offset().top;
    //     //
    //     //         if ((divPosition - 100) < currentScroll) {
    //     //             $currentSection = $(this);
    //     //         }
    //     //         if ($($currentSection).length > 0) {
    //     //             var id = $currentSection.attr('id');
    //     //             $('ul.vertical-menubar li').removeClass('active');
    //     //             $("ul.vertical-menubar [data-active='" + id + "']").addClass('active');
    //     //         }
    //     //
    //     //     })
    //     // });
    // });

    $("ul.vertical-menubar li").click(function (event) {
        var id = $(this).data('active');
        $('ul.vertical-menubar li').removeClass('active');
        $(this).addClass('active');
        $('html, body').animate({
            scrollTop: $("#" + id).offset().top - 75 - noticeHeight
        }, 500);
        event.stopPropagation();
    });

    $('.nav.nav-tabs > li > a').click(function (e) {
        var data = $(this).attr("id");
        if (history.pushState) {
            history.pushState(null, null, '#' + data);
        }
        else {
            window.location.hash = '#' + data;
        }
        e.preventDefault();
        $("#filterReportOnState").val($("#filterReportOnState option:first").val());
        $(".vertical-menubar li").removeClass('active');
        $(".vertical-menubar li:first").addClass('active');
        $(this).tab('show');
    });


})(); 

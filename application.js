let express = require('express'),
    config = require('./config');
let app = express();

require('./app/index')(app, config);
app.listen(config.port);
console.log(config.app.name + ' STARTED IN PORT: ' + config.port);
